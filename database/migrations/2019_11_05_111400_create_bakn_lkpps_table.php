<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaknLKPPsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bakn_lkpps', function (Blueprint $table) {
             //foreign key from spphs
             $table->integer('spph_id')->unsigned();
             $table->foreign('spph_id')->references('id')->on('spph_lkpps')->onDelete('cascade');
 
             // data utama
             $table->increments('id');
             $table->string('jenis_kontrak');
             $table->date('tglbakn');
             $table->string('tipe_rapat');
             $table->string('pimpinan_rapat');
             $table->string('peserta_mitra');
             $table->string('peserta_pins');
 
             // data penting
             $table->bigInteger('harga');
             $table->integer('io_id');
 
             //data tambahan
             $table->text('agenda');
             $table->text('dasar_pembahasan');
             $table->text('ruang_lingkup');
             $table->text('lokasi_pekerjaan');
             $table->text('jangka_waktu');
             $table->text('harga_terbilang');
             $table->text('cara_bayar');
             $table->text('lain_lain');
 
             // data wajib
             $table->string('created_by');
             $table->enum('status', ['save_bakn','draft_bakn','done_bakn','disp_bakn']);
             $table->string('handler')->nullable();
             // data file
             $table->string('file')->nullable();
             $table->string('title')->nullable();
             $table->dateTime('upload')->nullable();
             // data lampiran
             $table->string('lampiran')->nullable();
             $table->string('title_lampiran')->nullable();
             $table->dateTime('tgl_lampiran')->nullable();
            // etenxion
            $table->string('approval')->nullable();
            $table->string('end_approval')->nullable();
             // data time
             $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bakn_lkpps');
    }
}
