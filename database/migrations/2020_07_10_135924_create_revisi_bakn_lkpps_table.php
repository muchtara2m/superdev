<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevisiBaknLkppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revisi_bakn_lkpps', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('bakn_id');
            $table->string('spph_id');

            $table->date('tglbakn')->nullable();
            $table->integer('io_id')->nullable();
            $table->bigInteger('harga')->nullable();
            $table->longText('isi')->nullable();

            $table->string('tipe_rapat')->nullable();
            $table->string('pimpinan_rapat')->nullable();
            $table->string('peserta_mitra')->nullable();
            $table->string('peserta_pins')->nullable();
            $table->text('agenda')->nullable();
            $table->text('dasar_pembahasan')->nullable();
            $table->text('ruang_lingkup')->nullable();
            $table->text('lokasi_pekerjaan')->nullable();
            $table->text('jangka_waktu')->nullable();
            $table->text('harga_terbilang')->nullable();
            $table->text('cara_bayar')->nullable();
            $table->text('lain_lain')->nullable();


            $table->enum('status',['save_bakn','draft_bakn','done_bakn']);
            $table->text('file')->nullable();
            $table->text('title')->nullable();
            $table->dateTime('upload')->nullable();
            $table->text('lampiran')->nullable();
            $table->text('title_lampiran')->nullable();
            $table->dateTime('tgl_lampiran')->nullable();
            $table->json('new_approval')->nullable();
            $table->string('approval')->nullable();
            $table->string('end_approval')->nullable();
            $table->boolean('hold')->default(0);
            $table->string('created_by')->nullable();
            $table->timestamps();
        });

        Schema::table('bakn_lkpps', function (Blueprint $table){
            $table->json('new_approval')->after('approval')->nullable();
            $table->boolean('revisi')->default(0);
            $table->longText('isi')->after('tglbakn')->nullable();
            $table->boolean('hold')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revisi_bakn_lkpps');

        Schema::table('bakn_lkpps', function (Blueprint $table) {
            $table->dropColumn('new_approval');
            $table->dropColumn('revisi');
            $table->dropColumn('isi');
            $table->dropColumn('hold');
        });
    }
}
