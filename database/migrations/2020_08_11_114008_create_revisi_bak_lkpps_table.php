<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevisiBakLkppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revisi_bak_lkpps', function (Blueprint $table) {
            $table->increments('id');
             //foreign key from spphs
             $table->unsignedBigInteger('bak_id');
             $table->string('spph_id');
             $table->string('nomor_bak');
             $table->date('tgl_bak');
             $table->double('harga');
             $table->enum('status',['save_bak','done_bak','draft_bak']);
             // text
             $table->longText('isi')->nullable();
             $table->text('ruang_lingkup');
             $table->text('lokasi_pekerjaan');
             $table->text('jangka_waktu');
             $table->text('cara_bayar');
             $table->text('ketentuan');
             // tambahan
             $table->text('lampiran')->nullable();
             $table->text('title_lampiran')->nullable();
             $table->date('tgl_lampiran');
             $table->text('file')->nullable();
             $table->text('title')->nullable();
             $table->date('upload')->nullable();
             $table->string('approval')->nullable();
             $table->json('new_approval')->nullable();
             $table->string('end_approval')->nullable();
             $table->string('created_by');
             $table->boolean('hold')->default(0);
             $table->timestamps();
        });

        Schema::table('bak_lkpps', function (Blueprint $table){
            $table->json('new_approval')->after('approval')->nullable();
            $table->boolean('revisi')->default(0);
            $table->boolean('hold')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revisi_bak_lkpps');

        Schema::table('bak_lkpps', function (Blueprint $table) {
            $table->dropColumn('new_approval');
            $table->dropColumn('revisi');
            $table->dropColumn('hold');
        });
    }
}
