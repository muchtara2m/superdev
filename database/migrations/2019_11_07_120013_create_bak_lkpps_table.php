<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBakLkppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bak_lkpps', function (Blueprint $table) {
            //foreign key from spphs
            $table->integer('spph_id')->unsigned();
            $table->foreign('spph_id')->references('id')->on('spph_lkpps')->onDelete('cascade');
             
            $table->increments('id');
            $table->string('nomor_bak')->unique();
            $table->date('tgl_bak');
            $table->double('harga');
            $table->enum('status',['save_bak','done_bak','draft_bak']);
            // text
            $table->text('ruang_lingkup');
            $table->text('lokasi_pekerjaan');
            $table->text('jangka_waktu');
            $table->text('cara_bayar');
            $table->text('ketentuan');
            // tambahan
            $table->text('lampiran')->nullable();
            $table->text('title_lampiran')->nullable();
            $table->date('tgl_lampiran');
            $table->text('file')->nullable();
            $table->text('title')->nullable();
            $table->date('upload')->nullable();
            $table->string('approval')->nullable();
            $table->string('end_approval')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bak_lkpps');
    }
}
