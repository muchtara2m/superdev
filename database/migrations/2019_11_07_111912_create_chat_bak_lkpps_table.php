<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatBakLkppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_bak_lkpps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_bak_lkpp');
            $table->text('chat');
            $table->string('jabatan');
            $table->string('username');
            $table->integer('queue');
            $table->string('transaksi');
            $table->enum('status',['Approve','Return']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_bak_lkpps');
    }
}
