<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChatBaknLkpp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_bakn_lkpp', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_bakn_lkpp');
            $table->text('chat');
            $table->string('jabatan');
            $table->string('username');
            $table->integer('queue');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_bakn_lkpp');
    }
}
