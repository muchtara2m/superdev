<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KontrakLkpp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kontrak_lkpps', function (Blueprint $table) {
          


            $table->integer('sp3_id')->unsigned()->nullable();
            $table->foreign('sp3_id')->references('id')->on('sp3_lkpps')->onDelete('cascade');
            
            $table->integer('bakn_id')->unsigned()->nullable();
            $table->foreign('bakn_id')->references('id')->on('bakn_lkpps')->onDelete('cascade');
            
            $table->increments('id');
            $table->date('nomor_kontrak')->unique();
            $table->text('isi');
            $table->string('approval')->nullable();
            $table->string('endapproval')->nullable();
            $table->enum('status', ['save_kontrak','draft_kontrak','done_kontrak','disp_kontrak']);
            // data file
            $table->string('file')->nullable();
            $table->string('title')->nullable();
            $table->dateTime('upload')->nullable();
            // data lampiran
            $table->string('lampiran')->nullable();
            $table->string('title_lampiran')->nullable();
            $table->dateTime('tgl_lampiran')->nullable();
            
            $table->tinyInteger('created_by');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kontrak_lkpps');
    }
}
