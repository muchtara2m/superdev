<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTanggalBerkas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('spphs', function (Blueprint $table) {
            //
            $table->date('tanggal_terima_berkas')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('spphs', function (Blueprint $table) {
            //
            $table->dropColumn('tanggal_terima_berkas');
        });
    }
}
