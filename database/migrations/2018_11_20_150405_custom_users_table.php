<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CustomUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('users');
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 50);
            $table->string('name', 255);
            $table->string('email')->unique();
            $table->string('password');
            $table->string('level', 25)->nullable();
            $table->tinyInteger('id_unit')->nullable();
            // $table->string('employee_status', 20);
            $table->string('position', 255)->nullable();
            $table->string('band', 50)->nullable();
            $table->string('permanent_status', 20)->nullable();
            // $table->string('cost_center', 255)->nullable();
            $table->string('phone', 20)->nullable();
            $table->string('telp', 20)->nullable();
            $table->rememberToken();
            $table->tinyInteger('created_by')->nullable();
            $table->tinyInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
