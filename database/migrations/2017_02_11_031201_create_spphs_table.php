<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Faker\Provider\cs_CZ\DateTime;

class CreateSpphsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spphs', function (Blueprint $table) {

            $table->increments('id');
            $table->string('nomorspph',250)->unique();
            $table->date('tglspph');
            $table->string('nomorsph')->nullable();
            $table->dateTime('tglsph');
            $table->text('judul');
            $table->string('pic');
            $table->string('dari');
            $table->string('tembusan');
            $table->string('mitra');
            $table->string('perihal');
            //tambahan
            $table->string('created_by');
            $table->enum('status', ['save_spph', 'draft_spph', 'done_spph']);
            // file
            $table->string('file')->nullable();
            $table->string('title')->nullable();
            $table->dateTime('upload')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spphs');
    }
}
