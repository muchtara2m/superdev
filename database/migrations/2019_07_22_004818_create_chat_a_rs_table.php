<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatARsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_ars', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idAR')->references('id')->on('ars')->onDelete('cascade');
            $table->string('username');
            $table->text('chat');
            $table->enum('status',['Approve','Return']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_a_rs');
    }
}
