<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldBakns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bakns', function (Blueprint $table) {
            $table->text('lampiran')->nullable()->after('upload');
            $table->text('title_lampiran')->nullable()->after('lampiran');
            $table->date('tgl_lampiran')->nullable()->after('title_lampiran');
            $table->string('carabayar')->nullable()->after('harga_terbilang');
            $table->string('approval')->nullable()->after('status');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->date('tanggal_terima_berkas')->nullable();
            $table->date('jangka_waktu_kerja')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bakns', function (Blueprint $table) {
            $table->dropColumn('lampiran');
            $table->dropColumn('title_lampiran');
            $table->dropColumn('tgl_lampiran');
            $table->dropColumn('carabayar');
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
            $table->dropColumn('tanggal_terima_berkas');
            $table->dropColumn('jangka_waktu_kerja');
            $table->dropColumn('approval');
        });
    }
}
