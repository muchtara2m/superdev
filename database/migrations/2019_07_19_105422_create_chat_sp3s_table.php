<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatSp3sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_sp3s', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idsp3')->references('id')->on('sp3s')->onDelete('cascade');
            $table->text('chat');
            $table->integer('queue');
            $table->string('jabatan');
            $table->string('username');
            $table->string('transaksi');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_sp3s');
    }
}
