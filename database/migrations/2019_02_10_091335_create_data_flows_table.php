<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataFlowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_flows', function (Blueprint $table) {
            $table->increments('id');
            $table->string('jabatan');
            $table->string('username');
            $table->bigInteger('min');
            $table->bigInteger('max');
            $table->integer('queue');
            $table->string('transaksi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_flows');
    }
}
