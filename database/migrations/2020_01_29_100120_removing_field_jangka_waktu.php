<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovingFieldJangkaWaktu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bakns', function (Blueprint $table) {
            //
            $table->dropColumn('jangka_waktu_kerja');
            $table->dropColumn('tanggal_terima_berkas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bakns', function (Blueprint $table) {
            //
            $table->date('tanggal_terima_berkas')->nullable();
            $table->date('jangka_waktu_kerja')->nullable();
        });
    }
}
