<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileARsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_a_rs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('spk');
            $table->timestamp('kl');
            $table->timestamp('baut');
            $table->timestamp('bast');
            $table->timestamp('bast2');
            $table->timestamp('baop');
            $table->timestamp('baso');
            $table->timestamp('bapp');
            $table->timestamp('npk');
            $table->timestamp('lpp');
            $table->timestamp('baperub');
            $table->timestamp('suratgm');
            $table->timestamp('top');
            $table->timestamp('barekon');
            $table->timestamp('performansi');
            $table->timestamp('lkpp');
            $table->timestamp('ep');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_a_rs');
    }
}
