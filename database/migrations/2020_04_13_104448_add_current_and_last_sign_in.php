<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCurrentAndLastSignIn extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->timestamp('current_sign_in_at')->nullable()->after('updated_at');
            $table->timestamp('last_sign_in')->nullable()->after('current_sign_in_at');
            $table->timestamp('last_sign_out')->nullable()->after('last_sign_in');
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('current_sign_in_at');
            $table->dropColumn('last_sign_in');
            $table->dropColumn('last_sign_out');
        });
    }
}
