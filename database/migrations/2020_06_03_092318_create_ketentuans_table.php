<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKetentuansTable extends Migration
{
    public function up()
    {
        Schema::create('ketentuans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('dokumen');
            $table->string('jenis');
            $table->longText('isi');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ketentuans');
    }
}
