<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sp3Lkpp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sp3_lkpps', function (Blueprint $table) {
           // relation to data bakn
           $table->integer('bakn_id')->unsigned();
           $table->foreign('bakn_id')->references('id')->on('bakn_lkpps')->onDelete('cascade');
           
           // data utama
           $table->increments('id');
           $table->string('nomor_sp3')->unique();
           $table->date('tanggal_sp3');
           // data penting
           $table->bigInteger('harga');
           //data tambahan
           $table->text('ruang_lingkup');
           $table->text('lokasi_pekerjaan');
           $table->text('jangka_waktu');
           $table->text('harga_terbilang');
           $table->text('cara_bayar');
           $table->text('lain_lain');
           // data wajib
           $table->string('created_by');
           $table->enum('status', ['save_sp3','draft_sp3','done_sp3','disp_sp3']);
           // data file
           $table->string('file')->nullable();
           $table->string('title')->nullable();
           $table->dateTime('upload')->nullable();
           // data lampiran
           $table->string('lampiran')->nullable();
           $table->string('title_lampiran')->nullable();
           $table->dateTime('tgl_lampiran')->nullable();
           // etenxion
           $table->string('approval')->nullable();
           $table->string('end_approval')->nullable();
           // data time
           $table->timestamps();
           $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sp3_lkpps');
    }
}
