<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatBaknsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_bakns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('iBakn')->references('id')->on('bakns')->onDelete('cascade');
            $table->text('chat');
            $table->string('jabatan');
            $table->string('username');
            $table->string('transaksi');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_bakns');
    }
}
