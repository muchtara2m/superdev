<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevisiKontrakLkppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revisi_kontrak_lkpps', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('kontrak_id');
            $table->unsignedBigInteger('bakn_id');
            $table->string('nomor_kontrak');
            $table->date('tanggal_kontrak');
            $table->string('jenis_kontrak');
            $table->longText('isi');
            $table->json('new_approval')->nullable();
            $table->string('approval')->nullable();
            $table->string('endapproval')->nullable();
            $table->enum('status',['draft_kontrak','save_kontrak','done_kontrak']);
            $table->text('file')->nullable();
            $table->text('title')->nullable();
            $table->date('upload')->nullable();
            $table->text('lampiran')->nullable();
            $table->text('title_lampiran')->nullable();
            $table->date('tgl_lampiran')->nullable();
            $table->boolean('hold')->nullable();
            $table->string('created_by');
            
            $table->timestamps();
        });

        Schema::table('kontrak_lkpps', function (Blueprint $table){
            $table->json('new_approval')->after('approval')->nullable();
            $table->boolean('revisi')->default(0);
            $table->boolean('hold')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revisi_kontrak_lkpps');

        Schema::table('kontrak_lkpps', function (Blueprint $table) {
            $table->dropColumn('new_approval');
            $table->dropColumn('revisi');
            $table->dropColumn('hold');
        });
    }
}
