<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class ListPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			$menu_pbs = array(array("Create PBS","pbs_create"),array("PBS","pbs"),array("Status Transaksi","pbs_st_trans"),array("Inprogress","pbs_inp"),array(
			"Selesai","pbs_selesai"),array("Revisi Transaksi","pbs_re_trans"),array("Kalkulator Peminjaman","pbs_kal_pinj"));
			$menu_spph = array(array("Create SPPH","spph_create"),array("List SPPH","spph_list"),array("Draft SPPH","spph_draft"),array("Done SPPH","spph_done"));
			$menu_bakn = array(array("Create BAKN","bakn_create"),array("List BAKN","bakn_list"),array("Draft BAKN","bakn_draft"),array("Done BAKN","bakn_done"));
			$menu_spk = array(array("Create SP3/SPK","spk_create"),array("List SP3/SPK","spk_list"),array("Draft SP3/SPK","spk_draft"),array("Status Transaksi","spk_st_trans"),array("Inprogress SP3/SPK","spk_inp"),array("Done SP3/SPK","spk_done"));
			$menu_kontrak = array(array("Kontrak","kontrak"),array("List Kontrak","kontrak_list"),array("Draft Kontrak","kontrak_draft"),array(
			"Status Transaksi","kontrak_st_trans"),array("Inprogress Kontrak","kontrak_inp"),array("Done Kontrak","kontrak_done"));
			$menu_mdata = array(array("Data Flow","mdata_dflow"),array("Data Karyawan","mdata_dkaryawan"),array("Data Mitra","mdata_dmitra"),array(
			"Data Pasal","mdata_dpasal"),array("Data Pimpinan Rapat","mdata_dpimrap"),array("Data Role","mdata_drole"),array("Data Unit","mdata_dunit"),array(
			"Data Jenis Pasal","mdata_djenpas"),array("Data Cara Bayar","mdata_dcarbay"));
			
			foreach ($menu_pbs as $pbs) {
				Permission::create([
					'name' => $pbs[1],
					'display' => $pbs[0]
				]);
			}
			foreach ($menu_spph as $spph) {
				Permission::create([
					'name' => $spph[1],
					'display' => $spph[0]
				]);
			}
			foreach ($menu_bakn as $bakn) {
				Permission::create([
					'name' => $bakn[1],
					'display' => $bakn[0]
				]);
			}
			foreach ($menu_spk as $spk) {
				Permission::create([
					'name' => $spk[1],
					'display' => $spk[0]
				]);
			}
			foreach ($menu_kontrak as $kontrak) {
				Permission::create([
					'name' => $kontrak[1],
					'display' => $kontrak[0]
				]);
			}
			foreach ($menu_mdata as $mdata) {
				Permission::create([
					'name' => $mdata[1],
					'display' => $mdata[0]
				]);
			}
    }
}
