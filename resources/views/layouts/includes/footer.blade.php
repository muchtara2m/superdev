<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.2.0
  </div>
  <strong>Copyright &copy; 2018 <a href="https://pins.co.id" target="_blank">PT PINS Indonesia</a>.</strong> All rights
  reserved.
</footer>