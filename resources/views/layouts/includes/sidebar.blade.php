<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    
    @auth
    {{-- all for all menu  --}}
    @php
    // PBS LIST MENU
    $menu_pbs = array(
    array("Create PBS","pbs_create"),
    array("PBS","pbs"),
    array("Status Transaksi","pbs_st_trans"),
    array("Inprogress","pbs_inp"),
    array("Selesai","pbs_selesai"),
    array("Revisi Transaksi","pbs_re_trans"),
    array("Kalkulator Peminjaman","pbs_kal_pinj")
    );
    
    // ADMIN LIST MENU
    $menu_mdata = array(
    array("Data Flow","mdata_dflow"),
    array("Data Karyawan","mdata_dkaryawan"),
    array("Data Mitra","mdata_dmitra"),
    array("Data Pasal","mdata_dpasal"),
    array("Data Pimpinan Rapat","mdata_dpimrap"),
    array("Data Role & Permission","mdata_drole"),
    array("Data Unit","mdata_dunit"),
    array("Data Jenis Pasal","mdata_djenpas"),
    array("Data Cara Bayar","mdata_dcarbay")
    );
    //  AR LIST MENU
    $menu_ar = array(
    array("Dashboar AR","ar_dashboard"),
    array("Create","ar_create"),
    array("Update Nilai","ar_nilai"),
    array("Unbill","ar_unbill"),
    array("Unbill SDV","unbill-sdv"),
    array("Unbill Operation","unbill-operation"),
    array("Unbill UBIS","unbill-ubis"),
    array("Ready To Bill","ar_readytobill"),
    array("Bill","ar_bill"),
    array("Paid","ar_paid"),
    array("Paid 100","ar_paid100")
    );
    // DASHBOARD MENU PIPELINE
    $menu_db = array(
    array("Target Revenue Sales & GP","target_rev_sal_gp"),
    array("Pencapaian BAST","pencapaian_bast"),
    array("Revenue SAP","revenue_sap"),
    array("Pencapaian AM","pencapaian_am"),
    array("IFRS Monitoring","ifrs_monitoring"));
    // PIPELINE
    $pipeline = array(
    array("CRM","crm"),
    array("Info PBS/Justifikasi","info_pbs_justi"),
    array("Legal Vendor","legal_vendor"),
    array("Legal Customer","legal_customer"),
    array("Delivery Customer","delivery_customer"),
    array("Delivery Vendor","delivery_vendor"),
    array("Report Management","report_management")
    );

    $ar_dua = array(
    array("List Data","/list-ar-dua"),
    array("Notifikasi ","/ar-dua"),
    array("Dashboard","/ar-dua-dashboard"),
    );

    $dashboard = array(
      array('Dashboard SPPH','/#'),
      array('Dashboard BAKN','/#'),
      array('Dashboard SPK','/#'),
      array('Dashboard SP3','/#'),
      array('Dashboard KONTRAK', '/#'),
      array("Dashboard Performansi ","kontrak_non_performansi"),
      array('Dashboard AR','ar_dashboard'),
      array('Dashboard AR 2','/ar2_dashboard'),
    );


    
    //   declare array for list menu
    $pbs_permissions = $mdata_permissions = $ar_permissions = $pipeline_permissions = $menu_db_permissions = $ar_dua_permissions = $dashboard_permissions= array();
    // push data list sub menu to each menu
    
    // insert list sub menu pbs to menu pbs
    for ($i=0; $i<count($menu_pbs); $i++){
      array_push($pbs_permissions,$menu_pbs[$i][1]);
    }
    
    // insert list sub menu master data to menu master data
    for ($i=0; $i<count($menu_mdata); $i++){
      array_push($mdata_permissions,$menu_mdata[$i][1]);
    }
    
    // insert list sub menu ar to menu AR
    for ($i=0; $i<count($menu_ar); $i++){
      array_push($ar_permissions,$menu_ar[$i][1]);
    }
    
    // insert list sub menudb pipeline ar to  menu db pipeline
    for ($i=0; $i<count($menu_db); $i++){
      array_push($menu_db_permissions,$menu_db[$i][1]);
    }
    
    // insert list sub menu pipeline to menu pipeline
    for ($i=0; $i<count($pipeline); $i++){
      array_push($pipeline_permissions,$pipeline[$i][1]);
    }

     // insert list sub menu pipeline to menu ar dua
    for ($i=0; $i<count($ar_dua); $i++){
      array_push($ar_dua_permissions,$ar_dua[$i][1]);
    }

     // insert list sub menu pipeline to menu all dashboard
    for ($i=0; $i<count($dashboard); $i++){
      array_push($dashboard_permissions,$dashboard[$i][1]);
    }
    @endphp
    {{-- end all for all menu --}}
    
    {{-- ecom for LKPP --}}
    @php
    //SPPH LIST MENU LKPP
    $menu_spph_lkpp = array(
    array("Create SPPH","create-spph-lkpp"),
    array("Draft SPPH","draft-spph-lkpp"),
    array("List SPPH","list-spph-lkpp"),
    array("Done SPPH","done-spph-lkpp"),
    array("All SPPH","all-spph-lkpp")
    );
    
    //BAKN LIST MENU LKPP
    $menu_bakn_lkpp = array(
    array("Create BAKN","create-bakn-lkpp"),
    array("Draft BAKN","draft-bakn-lkpp"),
    array("Status Transaksi","status-transaksi-bakn-lkpp"),
    array("Inprogress","inprogress-bakn-lkpp"),
    array("Upload File","list-bakn-lkpp"),
    array("Done BAKN","done-bakn-lkpp"),
    array("Tracking Document","tracking-document-bakn-lkpp"),
    );
    
    //BAK LIST MENU LKPP
    $menu_bak_lkpp = array(
    array("Create BAK","create-bak-lkpp"),
    array("Draft BAK","draft-bak-lkpp"),
    array("Status Transaksi","status-transaksi-bak-lkpp"),
    array("Inprogress","inprogress-bak-lkpp"),
    array("List BAK","list-bak-lkpp"),
    array("Done BAK","done-bak-lkpp"),
    array("Tracking Document","tracking-bak-lkpp"),
    );
    //SP3 LIST MENU LKPP
    $menu_spk = array(
    array("Create SP3","create-sp3-lkpp"),
    array("Draft SP3","draft-sp3-lkpp"),
    array("Status Transaksi","status-transaksi-sp3-lkpp"),
    array("Inprogress SP3","inprogress-sp3-lkpp"),
    array("List SP3","list-sp3-lkpp"),
    array("Done SP3","done-sp3-lkpp"),
    array("Tracking Document",'tracking-sp3-lkpp')
    );
    
    // KONTRAK LIST MENU
    $menu_kontrak = array(
    array("Create Kontrak","create-kontrak-lkpp"),
    array("Draft Kontrak","draft-kontrak-lkpp"),
    array("Status Transaksi","status-transaksi-kontrak-lkpp"),
    array("Inprogress Kontrak","inprogress-kontrak-lkpp"),
    array("List Kontrak","list-kontrak-lkpp"),
    // array("Upload File","upload-file-kontrak-lkpp"),
    array("Done Kontrak","done-kontrak-lkpp"),
    array("Tracking Document",'tracking-kontrak-lkpp')
    );
    
    //   declare array for list menu
    $spph_permissions_lkpp = $bakn_permissions_lkpp = $bak_permissions_lkpp = $spk_permissions = $kontrak_permissions = array();
    
    // push data list sub menu to each menu
    
    // insert list sub menu spph to menu spph LKPP
    for ($i=0; $i<count($menu_spph_lkpp); $i++){
      array_push($spph_permissions_lkpp,$menu_spph_lkpp[$i][1]);
    }
    
    // insert list sub menu bakn to menu bakn LKPP
    for ($i=0; $i<count($menu_bakn_lkpp); $i++){
      array_push($bakn_permissions_lkpp,$menu_bakn_lkpp[$i][1]);
    }
    
    // insert list sub menu bak to menu bak LKPP
    for ($i=0; $i<count($menu_bak_lkpp); $i++){
      array_push($bak_permissions_lkpp,$menu_bak_lkpp[$i][1]);
    }
    
    // insert list sub menu spk to menu spk LKPP
    for ($i=0; $i<count($menu_spk); $i++){
      array_push($spk_permissions,$menu_spk[$i][1]);
    }
    
    // insert list sub menu kontrak to menu kontrak LKPP   
    for ($i=0; $i<count($menu_kontrak); $i++){
      array_push($kontrak_permissions,$menu_kontrak[$i][1]);
    }
    @endphp
    {{-- end ecom for LKPP --}}
    
    {{-- non for General Support --}}
    @php
    // SPPH LIST MENU
    $menu_spph = array(
    array("Create SPPH","spph_create"),
    array("Draft SPPH","spph_draft"),
    array("List SPPH","spph_list"),
    array("Done SPPH","spph_done")
    );
    // BAKN LIST MENU
    $menu_bakn = array(
    array("Create BAKN","bakn_create"),
    array("Draft BAKN","bakn_draft"),
    array("List BAKN","bakn_list"),
    array("Done BAKN","bakn_done")
    );
    //SP3 LIST MENUNON
    $menu_spk_non = array(
    array("Create SP3/SPK","spk_create_non"),
    array("Draft SP3/SPK","spk_draft_non"),
    array("List SP3/SPK","spk_list_non"),
    array("Status Transaksi","spk_st_trans_non"),
    array("Inprogress SP3/SPK","spk_inp_non"),
    array("Done SP3/SPK","spk_done_non")
    );
    // KONTRAK NON LIST MENU
    $menu_kontrak_non = array(
    array("Kontrak ","kontrak_non"),
    array("List Kontrak ","kontrak_non_list"),
    array("Draft Kontrak ","kontrak_non_draft"),
    array("Status Transaksi ","kontrak_non_st_trans"),
    array("Inprogress Kontrak ","kontrak_non_inprogress"),
    array("Upload File ","kontrak_non_upload"),
    array("Done Kontrak ","kontrak_non_done"),
    array("List Dispatch Kontrak ","kontrak_non_listdisp"),
    array("Tracking Document ","kontrak_non_track"),
    array("Dashboard Performansi ","kontrak_non_performansi"),
    );
    
    //   declare array for list menu
    $spph_permissions = $bakn_permissions = $spk_permissions_non = $kontrak_non_permissions = array();
    
    // push data list sub menu to each menu
    
    // insert list sub menu spph to menu spph
    for ($i=0; $i<count($menu_spph); $i++){
      array_push($spph_permissions,$menu_spph[$i][1]);
    }
    
    // insert list sub menu baknto menu bakn
    for ($i=0; $i<count($menu_bakn); $i++){
      array_push($bakn_permissions,$menu_bakn[$i][1]);
    }
    
    // insert list sub menu spk to menu spk   
    for ($i=0; $i<count($menu_spk_non); $i++){
      array_push($spk_permissions_non,$menu_spk_non[$i][1]);
    }
    
    // insert list sub menu kontrak to menu kontrak   
    for ($i=0; $i<count($menu_kontrak_non); $i++){
      array_push($kontrak_non_permissions,$menu_kontrak_non[$i][1]);
    }
    @endphp
    {{-- end non for general support --}}
    
    
    <ul class="sidebar-menu"  style="white-space:normal" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li hidden>
        <a href="{{ url('/home') }}">
          <i class="fa fa-home"></i> <span>Dashboard</span>
        </a>
      </li>
      @canany($menu_db_permissions)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>DASHBOARD SALES</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can($menu_db[0][1]) <li><a href="{{ url('db_sap') }}"><i class="fa fa-circle-o"></i>{{ $menu_db[0][0] }}</a></li> @endcan
          @can($menu_db[1][1]) <li><a href="{{ url('db_outlook') }}"><i class="fa fa-circle-o"></i>{{ $menu_db[1][0] }}</a></li> @endcan
          @can($menu_db[2][1]) <li><a href="#"><i class="fa fa-circle-o"></i>{{ $menu_db[2][0] }}</a></li> @endcan
          @can($menu_db[3][1]) <li><a href="{{ url('db_am') }}"><i class="fa fa-circle-o"></i>{{ $menu_db[3][0] }}</a></li> @endcan
          @can($menu_db[4][1]) <li><a href="{{ route('db-ifrs') }}"><i class="fa fa-circle-o"></i>{{ $menu_db[4][0] }}</a></li> @endcan
        </ul>

      </li>
      @endcanany
      
      @canany($pipeline_permissions)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>PIPELINE</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="treeview menu-open" style="height: auto;">
            @can($pipeline[0][1])
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> {{ $pipeline[0][0] }}
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('entry-update') }}"><i class="fa fa-circle-o"></i> Entry & Update Status Inisiasi Project</a></li>
                <li><a href="{{ url('entry-outlook') }}"><i class="fa fa-circle-o"></i> Entry Outlook</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Submit Project Won To SDV</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Reporting Outlook & Realisasi</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Report Inisiasi</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Reporting Status Inisiasi</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> List Inisiasi Project status WON</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Reporting Produktivitas AM</a></li>
              </ul>
            </li>
          </li>
          @endcan
          @can($pipeline[1][1])
          <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i>{{ $pipeline[1][0] }}
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i> Entry Data PBS</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Entry Data Justifikasi</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Reporting PBS & Kontrak</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> List PBS</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> List Justifikasi</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> IO - SO</a></li>
            </ul>
          </li> 
          @endcan
          @can($pipeline[2][1])
          <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i>{{ $pipeline[2][0] }}
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i> Entry Kontrak Vendor</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> List SPK/PKS Vendor</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> List KHS Vendor/Create PO</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> List PO Vendor</a></li>
            </ul>
          </li>
          @endcan
          @can($pipeline[3][1])
          <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i>{{ $pipeline[3][0] }}
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i> Entry Kontrak Customer</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> List SPK/PKS Customer</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> List KHS Customer/Create PO</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> List PO Customer</a></li>
            </ul>
          </li>
          @endcan
          @can($pipeline[4][1])
          <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i>{{ $pipeline[4][0] }}
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i> Entry Data Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Update Status Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Update Nilai Pencapaian</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Report Status Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Progress Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Performansi Report</a></li>
            </ul>
          </li>
          @endcan
          @can($pipeline[5][1])
          <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i>{{ $pipeline[5][0] }}
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i> Entry Data Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Update Status Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Update Nilai Pencapaian</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Report Status Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Progress Delivery</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i> Performansi Report</a></li>
            </ul>
          </li>
          @endcan
          @can($pipeline[6][1])
          <li class="treeview">
            <a href="#"><i class="fa fa-circle-o"></i>{{ $pipeline[6][0] }}
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i>Reporting Outlook & Realisasi</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Reporting Status Inisiasi</a></li>
              <li><a href="#"><i class="fa fa-circle-o"></i>Report Inisiasi
                <li><a href="#"><i class="fa fa-circle-o"></i>List Inisiasi Project status WON</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Report Legal Pelurusan Project</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>List SPK/PKS Customer</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>List KHS Customer</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>List PO Customer</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>List SPK/PKS Vendor</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>List KHS Vendor</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>List PO Vendor</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Report PBS & Kontrak</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Report Progress Delivery</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Report Status Delivery</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Report Cash IN</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Report Cash Out</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Report PU Billed</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Report PU Unbilled</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Summarize Cash.IN/OUT dan PU</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Report Residu</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i>Report Aset Project</a></li>
              </ul>
            </li>
            @endcan
          </ul>
        </a>
      </li>
      @endcanany
      
      @canany($pbs_permissions)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>PBS</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can($menu_pbs[0][1]) <li><a href="{{ route('pbs-create') }}"><i class="fa fa-circle-o"></i>{{ $menu_pbs[0][0] }}</a></li> @endcan
          @can($menu_pbs[1][1]) <li><a href="{{ route('index-pbs') }}"><i class="fa fa-circle-o"></i>{{ $menu_pbs[1][0] }}</a></li> @endcan
          @can($menu_pbs[2][1]) <li><a href="{{ route('status-transaksi') }}"><i class="fa fa-circle-o"></i>{{ $menu_pbs[2][0] }}</a></li> @endcan
          @can($menu_pbs[3][1]) <li><a href="{{ route('inprogress') }}"><i class="fa fa-circle-o"></i>{{ $menu_pbs[3][0] }}</a></li> @endcan
          @can($menu_pbs[4][1]) <li><a href="{{ route('selesai') }}"><i class="fa fa-circle-o"></i>{{ $menu_pbs[4][0] }}</a></li> @endcan
          @can($menu_pbs[5][1]) <li><a href="{{ route('revisi-transaksi') }}"><i class="fa fa-circle-o"></i>{{ $menu_pbs[5][0] }}</a></li> @endcan
          @can($menu_pbs[6][1]) <li><a href="{{ route('kalkulator-pinjaman') }}"><i class="fa fa-circle-o"></i>{{ $menu_pbs[6][0] }}</a></li> @endcan
        </ul>
      </li>
      @endcanany
      
      @canany($ar_permissions)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>AR</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can($menu_ar[0][1]) <li><a href="{{ route('db_ar') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[0][0] }}</a></li> @endcan
          @can($menu_ar[1][1]) <li><a href="{{ url('create_ar') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[1][0] }}</a></li> @endcan
          @can($menu_ar[2][1]) <li><a href="{{ url('nilai') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[2][0] }}</a></li> @endcan
          @can($menu_ar[3][1]) <li><a href="{{ url('unbill_ar') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[3][0] }}</a></li> @endcan
          @can($menu_ar[4][1]) <li><a href="{{ url('unbill-sdv') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[4][0] }}</a></li> @endcan
          @can($menu_ar[5][1]) <li><a href="{{ url('unbill-operation') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[5][0] }}</a></li> @endcan
          @can($menu_ar[6][1]) <li><a href="{{ url('unbill-ubis') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[6][0] }}</a></li> @endcan
          @can($menu_ar[7][1]) <li><a href="{{ url('readytobill') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[7][0] }}</a></li> @endcan
          @can($menu_ar[8][1]) <li><a href="{{ url('bill') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[8][0] }}</a></li> @endcan
          @can($menu_ar[9][1]) <li><a href="{{ url('paid') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[9][0] }}</a></li> @endcan
          @can($menu_ar[10][1]) <li><a href="{{ url('paid100') }}"><i class="fa fa-circle-o"></i> {{ $menu_ar[10][0] }}</a></li> @endcan
          
        </ul>
      </li>
      @endcanany {{-- end list AR --}}

      @canany($ar_dua_permissions) {{-- start list AR 2 --}}
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>AR2</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @for($i = 0 ; $i< count($ar_dua) ; $i++)
            @can($ar_dua[$i][1]) <li><a href="{{ url($ar_dua[$i][1]) }}"><i class="fa fa-circle-o"></i>{{ $ar_dua[$i][0] }}</a></li>@endcan
          @endfor
        </ul>
      </li>
      @endcanany {{-- end list ar dua  --}}

      @canany($dashboard_permissions) {{-- start list AR 2 --}}
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>ALL DASHBOARD</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @can($menu_kontrak_non[5][1]) <li><a href="{{ url('/kontrak-non-performansi') }}"><i class="fa fa-circle-o"></i>{{ $dashboard[5][0] }}</a></li> @endcan
          @can($menu_kontrak_non[6][1]) <li><a href="{{ route('db_ar') }}"><i class="fa fa-circle-o"></i>{{ $dashboard[6][0] }}</a></li> @endcan
          
        </ul>
      </li>
      @endcanany {{-- end list ar dua  --}}

      {{-- list menu and sub menu for ecommerce --}}
      @canany($bakn_permissions_lkpp)
      <li class="treeview">
        <a href="#">
          <i class="fa fa-dashboard"></i>
          <span>E - COMMERCE</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="treeview menu-open" style="height: auto;"> <!-- declare list sub menu !-->
            {{-- spph --}}
            <li class="treeview menu-open" style="height: auto;"> <!-- declare list sub menu !-->
              @canany($spph_permissions_lkpp)
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-circle-o"></i>
                  <span>SPPH</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  @for($i = 0 ; $i< count($menu_spph_lkpp) ; $i++)
                  @can($menu_spph_lkpp[$i][1]) <li><a href="{{ url($menu_spph_lkpp[$i][1]) }}"><i class="fa fa-circle-o"></i>{{ $menu_spph_lkpp[$i][0] }}</a></li>@endcan
                  @endfor
                </ul>
              </li>
              @endcanany
            </li>
            {{-- bakn --}}
            <li class="treeview menu-open" style="height: auto;"> <!-- declare list sub menu !-->
              @canany($bakn_permissions_lkpp)
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-circle-o"></i>
                  <span>BAKN</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  @for($i = 0 ; $i< count($menu_bakn_lkpp) ; $i++)
                  @can($menu_bakn_lkpp[$i][1]) <li><a href="{{ url($menu_bakn_lkpp[$i][1]) }}"><i class="fa fa-circle-o"></i>{{ $menu_bakn_lkpp[$i][0] }}</a></li>@endcan
                  @endfor
                </ul>
              </li>
              @endcanany
            </li>
            {{-- bak --}}
            <li class="treeview menu-open" style="height: auto;"> <!-- declare list sub menu !-->
              @canany($bak_permissions_lkpp)
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-circle-o"></i>
                  <span>BAK</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  @for($i = 0 ; $i< count($menu_bak_lkpp) ; $i++)
                  @can($menu_bak_lkpp[$i][1]) <li><a href="{{ url($menu_bak_lkpp[$i][1]) }}"><i class="fa fa-circle-o"></i>{{ $menu_bak_lkpp[$i][0] }}</a></li>@endcan
                  @endfor
                </ul>
              </li>
              @endcanany
            </li>
            {{-- spk lkpp --}}
            <li class="treeview menu-open" style="height: auto;"> <!-- declare list sub menu !-->
              @canany($spk_permissions)
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-circle-o"></i>
                  <span>SP3</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  @for($i = 0 ; $i< count($menu_spk) ; $i++)
                  @can($menu_spk[$i][1]) <li><a href="{{ url($menu_spk[$i][1]) }}"><i class="fa fa-circle-o"></i>{{ $menu_spk[$i][0] }}</a></li>@endcan
                  @endfor
                </ul>
              </li>
              @endcanany
            </li>
            {{-- kontrak lkpp --}}
            <li class="treeview menu-open" style="height: auto;"> <!-- declare list sub menu !-->
              @canany($kontrak_permissions)
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-circle-o"></i>
                  <span>KONTRAK</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  @for($i = 0 ; $i< count($menu_kontrak) ; $i++)
                  @can($menu_kontrak[$i][1]) <li><a href="{{ url($menu_kontrak[$i][1]) }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak[$i][0] }}</a></li>@endcan
                  @endfor
                </ul>
              </li>
              @endcanany
            </li>
          </ul>
        </li>
        @endcanany
        {{-- end list menu and sub menu for ecommerce --}}
        {{-- list menu and sub menu for general support --}}
      @canany($bakn_permissions)
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i>
            <span>GENERAL SUPPORT</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            {{-- spph --}}
            <li class="treeview menu-open" style="height: auto;"> <!-- declare list sub menu !-->
              @canany($spph_permissions)
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-circle-o"></i>
                  <span>SPPH</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  @can($menu_spph[0][1]) <li><a href="{{ url('spph-create') }}"><i class="fa fa-circle-o"></i>{{ $menu_spph[0][0] }}</a></li> @endcan
                  @can($menu_spph[1][1]) <li><a href="{{ url('spph-draft') }}"><i class="fa fa-circle-o"></i>{{ $menu_spph[1][0] }}</a></li> @endcan
                  @can($menu_spph[2][1]) <li><a href="{{ url('spph-index') }}"><i class="fa fa-circle-o"></i>{{ $menu_spph[2][0] }}</a></li> @endcan
                  @can($menu_spph[3][1]) <li><a href="{{ url('spph-done') }}"><i class="fa fa-circle-o"></i>{{ $menu_spph[3][0] }}</a></li> @endcan
                </ul>
              </li>
              @endcanany
            </li>
            {{-- bakn --}}
            <li class="treeview menu-open" style="height: auto;"> <!-- declare list sub menu !-->
              @canany($bakn_permissions)
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-circle-o"></i>
                  <span>BAKN</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  {{-- @can($menu_bakn[0][1]) <li hidden><a href="{{ url('ds-bakn') }}"><i class="fa fa-circle-o"></i>Dashboard BAKN</a></li> @endcan --}}
                  @can($menu_bakn[0][1]) <li><a href="{{ url('bakn-create') }}"><i class="fa fa-circle-o"></i>{{ $menu_bakn[0][0] }}</a></li> @endcan
                  @can($menu_bakn[1][1]) <li><a href="{{ url('bakn-draft') }}"><i class="fa fa-circle-o"></i>{{ $menu_bakn[1][0] }}</a></li> @endcan
                  @can($menu_bakn[2][1]) <li><a href="{{ url('list-bakn') }}"><i class="fa fa-circle-o"></i>{{ $menu_bakn[2][0] }}</a></li> @endcan
                  @can($menu_bakn[3][1]) <li><a href="{{ url('bakn-done') }}"><i class="fa fa-circle-o"></i>{{ $menu_bakn[3][0] }}</a></li> @endcan
                </ul>
              </li>
              @endcanany
            </li>
            {{-- spk.sp3 --}}
            <li class="treeview menu-open" style="height: auto;"> <!-- declare list sub menu !-->
              @canany($spk_permissions_non)
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-circle-o"></i>
                  <span>SP3/SPK</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  @can($menu_spk_non[0][1]) <li><a href="{{ url('create-spk-non') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk_non[0][0] }}</a></li> @endcan
                  @can($menu_spk_non[1][1]) <li><a href="{{ url('list-spk-non') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk_non[1][0] }}</a></li> @endcan
                  @can($menu_spk_non[2][1]) <li><a href="{{ url('draft-spk-non') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk_non[2][0] }}</a></li> @endcan
                  @can($menu_spk_non[3][1]) <li><a href="{{ url('status-spk-non') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk_non[3][0] }}</a></li> @endcan
                  @can($menu_spk_non[4][1]) <li><a href="{{ url('inprogress-spk-non') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk_non[4][0] }}</a></li> @endcan
                  @can($menu_spk_non[5][1]) <li><a href="{{ url('done-spk-non') }}"><i class="fa fa-circle-o"></i>{{ $menu_spk_non[5][0] }}</a></li> @endcan
                </ul>
              </li>
              @endcanany
            </li>
            {{-- kontrak --}}
            <li class="treeview menu-open" style="height: auto;"> <!-- declare list sub menu !-->
              @canany($kontrak_non_permissions)
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-circle-o"></i>
                  <span>KONTRAK</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  {{-- @can($menu_kontrak[0][1]) <li hidden><a href="{{ url('ds-kontrak') }}"><i class="fa fa-circle-o"></i>Dashboard Kontrak</a></li> @endcan --}}
                  @can($menu_kontrak_non[0][1]) <li><a href="{{ url('kontrak-non') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[0][0] }}</a></li> @endcan
                  @can($menu_kontrak_non[1][1]) <li><a href="{{ url('kontrak-non-list') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[1][0] }}</a></li> @endcan
                  @can($menu_kontrak_non[2][1]) <li><a href="{{ url('kontrak-non-draft') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[2][0] }}</a></li> @endcan
                  @can($menu_kontrak_non[3][1]) <li><a href="{{ url('kontrak-non-status') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[3][0] }}</a></li> @endcan
                  @can($menu_kontrak_non[4][1]) <li><a href="{{ url('kontrak-non-inprogress') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[4][0] }}</a></li> @endcan
                  @can($menu_kontrak_non[5][1]) <li><a href="{{ url('kontrak-non-upload') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[5][0] }}</a></li> @endcan
                  @can($menu_kontrak_non[6][1]) <li><a href="{{ url('kontrak-non-done') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[6][0] }}</a></li> @endcan
                  @can($menu_kontrak_non[7][1]) <li><a href="{{ url('kontrak-non-listdisp') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[7][0] }}</a></li> @endcan
                  @can($menu_kontrak_non[8][1]) <li><a href="{{ url('kontrak-non-tracking') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[8][0] }}</a></li> @endcan
                  @can($menu_kontrak_non[9][1]) <li><a href="{{ url('kontrak-non-performansi') }}"><i class="fa fa-circle-o"></i>{{ $menu_kontrak_non[9][0] }}</a></li> @endcan
                </ul>
              </li>
              @endcanany
            </li>
          </ul>
        </li>
        @endcanany
        {{-- end list menu and sub menu for general support --}}
        @canany($mdata_permissions)
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>MASTER DATA</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @can($menu_mdata[0][1]) <li><a href="{{ url('flow') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[0][0] }}</a></li> @endcan
            @can($menu_mdata[1][1]) <li><a href="{{ route('user.index') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[1][0] }}</a></li> @endcan
            @can($menu_mdata[2][1]) <li><a href="{{ url('mitra') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[2][0] }}</a></li> @endcan
            @can($menu_mdata[3][1]) <li><a href="{{ url('pasal') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[3][0] }}</a></li> @endcan
            @can($menu_mdata[4][1]) <li><a href="{{ route('chairman.index') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[4][0] }}</a></li> @endcan
            @can($menu_mdata[5][1]) <li><a href="{{ route('rpmanage.index') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[5][0] }}</a></li> @endcan
            @can($menu_mdata[6][1]) <li><a href="{{ route('unit.index') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[6][0] }}</a></li> @endcan
            @can($menu_mdata[7][1]) <li><a href="{{ url('jenispasal') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[7][0] }}</a></li> @endcan
            @can($menu_mdata[8][1]) <li><a href="{{ url('tatacara') }}"><i class="fa fa-circle-o"></i>{{ $menu_mdata[8][0] }}</a></li> @endcan
          </ul>
        </li>
        @endcanany
        
      </ul>
      @endauth
      
    </section>
    <!-- /.sidebar -->
  </aside>
  