@php
        //SP3 LIST MENU LKPP
        $menu_spk = array(
        array("Create SP3/SPK","spk_create"),
        array("List SP3/SPK","spk_list"),
        array("Draft SP3/SPK","spk_draft"),
        array("Status Transaksi","spk_st_trans"),
        array("Inprogress SP3/SPK","spk_inp"),
        array("Done SP3/SPK","spk_done"));
         
        // KONTRAK LIST MENU
    $menu_kontrak = array(
        array("Kontrak LKPP","kontrak"),
        array("List Kontrak LKPP","kontrak_list"),
        array("Draft Kontrak LKPP","kontrak_draft"),
        array("Status Transaksi LKPP","kontrak_st_trans"),
        array("Inprogress Kontrak LKPP","kontrak_inp"),
        array("Done Kontrak LKPP","kontrak_done"));


    //   declare array for list menu
    $spk_permissions = $kontrak_permissions = array();
  
     // push data list sub menu to each menu

    // insert list sub menu spk to menu spk LKPP
    for ($i=0; $i<count($menu_spk); $i++){
      array_push($spk_permissions,$menu_spk[$i][1]);
    }
    
    // insert list sub menu kontrak to menu kontrak LKPP   
    for ($i=0; $i<count($menu_kontrak); $i++){
      array_push($kontrak_permissions,$menu_kontrak[$i][1]);
    }
   
    
@endphp