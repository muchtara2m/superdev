@push('menu_all')
@php
// PBS LIST MENU
$menu_pbs = array(
    array("Create PBS","pbs_create"),
    array("PBS","pbs"),
    array("Status Transaksi","pbs_st_trans"),
    array("Inprogress","pbs_inp"),
    array("Selesai","pbs_selesai"),
    array("Revisi Transaksi","pbs_re_trans"),
    array("Kalkulator Peminjaman","pbs_kal_pinj"));
   
    // ADMIN LIST MENU
$menu_mdata = array(
    array("Data Flow","mdata_dflow"),
    array("Data Karyawan","mdata_dkaryawan"),
    array("Data Mitra","mdata_dmitra"),
    array("Data Pasal","mdata_dpasal"),
    array("Data Pimpinan Rapat","mdata_dpimrap"),
    array("Data Role & Permission","mdata_drole"),
    array("Data Unit","mdata_dunit"),
    array("Data Jenis Pasal","mdata_djenpas"),
    array("Data Cara Bayar","mdata_dcarbay"));
//  AR LIST MENU
$menu_ar = array(
    array("Dashboar AR","ar_dashboard"),
    array("Create","ar_create"),
    array("Update Nilai","ar_nilai"),
    array("Unbill","ar_unbill"),
    array("Unbill SDV","unbill-sdv"),
    array("Unbill Operation","unbill-operation"),
    array("Unbill UBIS","unbill-ubis"),
    array("Ready To Bill","ar_readytobill"),
    array("Bill","ar_bill"),
    array("Paid","ar_paid"),
    array("Paid 100","ar_paid100"));

//   declare array for list menu
$pbs_permissions = $mdata_permissions = $ar_permissions = array();
// push data list sub menu to each menu

// insert list sub menu pbs to menu pbs
for ($i=0; $i<count($menu_pbs); $i++){
  array_push($pbs_permissions,$menu_pbs[$i][1]);
}

// insert list sub menu master data to menu master data
for ($i=0; $i<count($menu_mdata); $i++){
  array_push($mdata_permissions,$menu_mdata[$i][1]);
}

// insert list sub menu ar to menu AR
for ($i=0; $i<count($menu_ar); $i++){
  array_push($ar_permissions,$menu_ar[$i][1]);
}
@endphp
@endpush

