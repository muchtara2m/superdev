@php
     // SPPH LIST MENU
     $menu_spph = array(
        array("Create SPPH","spph_create"),
        array("List SPPH","spph_list"),
        array("Draft SPPH","spph_draft"),
        array("Done SPPH","spph_done"));
        // BAKN LIST MENU
    $menu_bakn = array(
        array("Create BAKN","bakn_create"),
        array("List BAKN","bakn_list"),
        array("Draft BAKN","bakn_draft"),
        array("Done BAKN","bakn_done"));
          //SP3 LIST MENUNON
    $menu_spk_non = array(
        array("Create SP3/SPK","spk_create_non"),
        array("List SP3/SPK","spk_list_non"),
        array("Draft SP3/SPK","spk_draft_non"),
        array("Status Transaksi","spk_st_trans_non"),
        array("Inprogress SP3/SPK","spk_inp_non"),
        array("Done SP3/SPK","spk_done_non"));
            // KONTRAK NON LIST MENU
    $menu_kontrak_non = array(
        array("Kontrak ","kontrak_non"),
        array("List Kontrak ","kontrak_non_list"),
        array("Draft Kontrak ","kontrak_non_draft"),
        array("Status Transaksi ","kontrak_non_st_trans"),
        array("Inprogress Kontrak ","kontrak_non_inprogress"),
        array("Upload File ","kontrak_non_upload"),
        array("Done Kontrak ","kontrak_non_done"),
        array("List Dispatch Kontrak ","kontrak_non_listdisp"),
        array("Tracking Document ","kontrak_non_track"),
      );


    //   declare array for list menu
    $spph_permissions = $bakn_permissions = $spk_permissions_non = $kontrak_non_permissions = array();
   
    // push data list sub menu to each menu

    // insert list sub menu spph to menu spph
    for ($i=0; $i<count($menu_spph); $i++){
      array_push($spph_permissions,$menu_spph[$i][1]);
    }

    // insert list sub menu baknto menu bakn
    for ($i=0; $i<count($menu_bakn); $i++){
      array_push($bakn_permissions,$menu_bakn[$i][1]);
    }

    // insert list sub menu spk to menu spk   
    for ($i=0; $i<count($menu_spk_non); $i++){
      array_push($spk_permissions_non,$menu_spk_non[$i][1]);
    }

    // insert list sub menu kontrak to menu kontrak   
    for ($i=0; $i<count($menu_kontrak_non); $i++){
      array_push($kontrak_non_permissions,$menu_kontrak_non[$i][1]);
    }
   
@endphp