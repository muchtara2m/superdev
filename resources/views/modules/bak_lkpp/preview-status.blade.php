@extends('layouts.master')

@section('title')
Approval BAK
@endsection

@section('stylesheets')
<!-- css froala editor -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
{{-- smartwizard --}}
<link href="{{ asset('smartwizard/dist/css/smart_wizard.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('smartwizard/dist/css/smart_wizard_theme_circles.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('smartwizard/dist/css/smart_wizard_theme_circles.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>BAK<small>Approval</small></h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">BAK</a></li>
            <li class="active"> Approval </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid" style="border-radius: 5px;border-left: 4px solid#00a7d0 !important;">
                    <div class="box-header with-border">
                        <h4 class="text-center">
                            <strong>
                            @if ($bakLkpp->revisi >=1)
                            REVISI
                            @endif
                             {{ strtoupper($bakLkpp->spph_bak_lkpp->judul) }}</strong>
                        </h4>
                    </div>
                    <div class="box-body ">
                        <div class="row">
                            <div class="col-sm-4">
                                <dl class="dl-horizontal">
                                    <dt>Nomor BAK</dt>
                                    <dd>{{ $bakLkpp->nomor_bak }}</dd>
                                    <dt>Nomor SPPH</dt>
                                    <dd>{{ $bakLkpp->spph_bak_lkpp->nomorspph }}</dd>
                                    <dt>Nomor SPH</dt>
                                    <dd>{{ $bakLkpp->spph_bak_lkpp->nomorsph == NULL ? 'Belum Diinput' : $bakLkpp->spph_bak_lkpp->nomorsph }}</dd>
                                    <dt>Tanggal</dt>
                                    <dd>{{ date('d F Y',strtotime($bakLkpp->tgl_bak)) }}</dd>
                                    <dt>Mitra</dt>
                                    <dd>{{ $bakLkpp->spph_bak_lkpp->mitra_lkpps->perusahaan }}</dd>
                                </dl>
                            </div>
                            <div class="col-sm-2">
                                @if ($bakLkpp->revisi >=1)
                                <div class="row">
                                    <div class="col-xs-2"></div>
                                    <div class="col-xs-10">
                                        <dl>
                                            <dt>Revisi</dt>
                                            @php
                                                $x=0;
                                            @endphp
                                            @foreach ($bakLkpp->revisi_bak_lkpp as $item)
                                                <a target="_blank" href="/preview-revisi-bak-lkpp/{{ $item->id }}">  Revisi Ke - {{$x++ }}</a><br>
                                            @endforeach
                                            <p disabled="true"> Revisi Ke - {{ $bakLkpp->revisi }} </p>

                                        </dl>
                                    </div>
                                </div>
                                @endif
                              
                            </div>
                            <div class="col-sm-3">
                                @php
                                $title_lampiran = ($bakLkpp->lampiran) !=NULL ? json_decode($bakLkpp->title_lampiran, TRUE) : NULL;
                                $lampiran       = ($bakLkpp->lampiran) !=NULL ? json_decode($bakLkpp->lampiran, TRUE) : NULL;
                                $title          = ($bakLkpp->file) !=NULL ? json_decode($bakLkpp->title, TRUE) : NULL ;
                                $file           = ($bakLkpp->file) !=NULL ? json_decode($bakLkpp->file, TRUE) : NULL;
                                @endphp
                                <div class="row">
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>Lampiran BAK</dt>
                                            @if ($title_lampiran != NULL)
                                            @foreach ($title_lampiran as $key => $value)
                                            {{ ($key+1)}}. <a href="{{Storage::url($lampiran[$key])}}">{{$title_lampiran[$key]}}</a><br>
                                            @endforeach
                                            @endif
                                           
                                          </dl>
                                    </div>
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>File BAK</dt>
                                            @if ($title != NULL)
                                            @foreach ($title as $key => $value)
                                            {{ ($key+1)}}. <a
                                                href="{{Storage::url($file[$key])}}">{{$title[$key]}}</a><br>
                                            @endforeach
                                            @endif
                                           
                                          </dl>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                @php
                               // Spph
                                $titleLampiranSpph  = ($bakLkpp->spph_bak_lkpp['lampiran'])!=NULL ? json_decode($bakLkpp->spph_bak_lkpp['title_lampiran'], TRUE) : NULL;
                                $lampiranSpph       = ($bakLkpp->spph_bak_lkpp['lampiran'])!=NULL ? json_decode($bakLkpp->spph_bak_lkpp['lampiran'], TRUE) : NULL;
                                $titleFileSpph      = ($bakLkpp->spph_bak_lkpp['file'])!=NULL ? json_decode($bakLkpp->spph_bak_lkpp['title'], TRUE) : NULL ;
                                $fileSpph           = ($bakLkpp->spph_bak_lkpp['file'])!=NULL ? json_decode($bakLkpp->spph_bak_lkpp['file'], TRUE) : NULL;
                                @endphp
                                <div class="row">
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>Lampiran SPPH</dt>
                                            @if ($titleLampiranSpph != NULL)
                                                @foreach ($titleLampiranSpph as $key => $value)
                                                    {{ ($key+1)}}. <a target="_blank" href="{{Storage::url($lampiranSpph[$key])}}">{{$titleLampiranSpph[$key]}}</a><br>
                                                @endforeach
                                            @endif                                           
                                          </dl>
                                    </div>
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>File SPPH</dt>
                                            @if ($titleFileSpph != NULL)
                                                @foreach ($titleFileSpph as $key => $value)
                                                    {{ ($key+1)}}. <a target="_blank" href="{{Storage::url($fileSpph[$key])}}">{{$titleFileSpph[$key]}}</a><br>
                                                @endforeach
                                            @endif
                                          </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            
                            <div id="smartwizard" style="border-radius: 8px; background-color:white">
                                <ul >
                                    <li><a href="#step-1">Step 1<br /><small>Draft BAK</small></a></li>
                                    <li><a href="#step-2">Step 2<br /><small> SPPH</small></a></li>
                                    <li><a href="#step-3">Step 3<br /><small>Approval</small></a></li>
                                    {{-- <li><a href="#step-4">Step 4<br /><small>Approval</small></a></li> --}}
                                </ul>
                                <div>
                                    <div id="step-1" class="">
                                        <textarea name="" id="" cols="30" rows="10">
                                            {{ $bakLkpp->isi }}
                                        </textarea>
                                    </div>
                                  
                                    <div id="step-2" class="">
                                        @if ($bakLkpp->spph_bak_lkpp->isi != NULL)
                                        <textarea name="" id="" cols="30" rows="10">
                                            {{ $spphLKPP->isi }}
                                        </textarea>
                                        @else
                                            @include('modules.spph_lkpp.inc.spph_preview')
                                        @endif

                                    </div>
                                    <div id="step-3" class="">
                                        <h4>Coment and Approve</h4>
                                        <ul class="timeline">
                                            @foreach ($chats as $isi)
                                            <!-- timeline time label -->
                                            <li class="time-label">
                                                <span class="bg-green">
                                                    {{ date('d M.Y', strtotime($isi->created_at)) }}
                                                </span>
                                            </li>
                                            <!-- /.timeline-label -->
                                            <!-- timeline item -->
                                            <li>
                                                <!-- timeline icon -->
                                                <i class="fa fa-user bg-aqua"></i>
                                                <div class="timeline-item">
                                                    <span class="time">
                                                        <i class="fa fa-clock-o"></i>
                                                        {{ date('H:i:s', strtotime($isi->created_at)) .' - '. Carbon\Carbon::parse($isi->created_at)->diffForHumans()}}
                                                    </span>

                                                    <h3 class="timeline-header"><a
                                                            href="#">{{ $isi->jabatan.' - '.$isi->name }}</a></h3>

                                                    <div class="timeline-body">
                                                        {{ $isi->chat }}
                                                    </div>
                                                 
                                                    <div class="timeline-footer" hidden>
                                                        <p class="bold"> </p>
                                                    </div>
                                                </div>
                                            </li>
                                            <!-- END timeline item -->
                                            @endforeach
                                            <li>
                                                <i class="fa fa-clock-o bg-gray"></i>
                                            </li>
                                        </ul>
                                        <br>
                                        @if($bakLkpp->approval == Auth::user()->username)
                                        <form action="{{ route('lkpp.bak.approve', $bakLkpp->id) }}" class="form-group" method="post">
                                            @csrf
                                            <div class="col-md-12 pad-0">
                                                <div class="col-md-6 pad-0">
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Comment</label>
                                                        <input type="text" class="form-control" name="chat" id="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6" hidden>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">ID Transaksi</label>
                                                        <input type="text" class="form-control" name="idTransaksi"
                                                            value="{{ $bakLkpp->id }}" readonly>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mar-paginate pad-0">
                                                    <button type="submit" class="btn btn-danger" name="status"
                                                        value="Return">Return</button>
                                                    <button type="submit" class="btn btn-primary" name="status"
                                                        id="submit" value="Approve">Approve</button>
                                                </div>
                                            </div>
                                        </form>
                                        @endif
                                    </div>
                        
                    </div>
                </div>
            </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('smartwizard/dist/js/jquery.smartWizard.min.js') }}"></script>
<!-- script function froala -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<script src="{{ asset('froala/js/languages/id.js') }}"></script>
<!-- External -->
<script type="text/javascript">
   $(document).ready(function () {
        $('div#smartwizard').smartWizard();
        $('div#smartwizardcircle').smartWizard();
    });

    $('textarea').froalaEditor({
        // fullPage: true,
        toolbarButtons      : ['print', 'html', 'getPDF', 'fullscreen'],
        charCounterCount    : false,
        key                 : keyFroala,
        height              : 500,
    })
    // removo alert lisensi froala
$("div > a", ".fr-wrapper").css('display', 'none');
</script>
@endsection