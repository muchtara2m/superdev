<div class="modal fade" id="modal-unit-spph">
    <div class="modal-dialog" style="width: fit-content">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">List SPPH</h4>
                </div>
                <div class="modal-body">
                    <table id="spph" class="display table-responsive">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nomor SPPH</th>
                                <th>Judul</th>
                                <th>Tanggal</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($spph as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->nomorspph}}</td>
                                    <td>{{ $item->judul}}</td>
                                    <td>{{ $item->created_at}}</td>
                                    <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->