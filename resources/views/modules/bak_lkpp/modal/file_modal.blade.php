<div class="modal fade" id="modal-file">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Upload Lampiran</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="" enctype="multipart/form-data" id="form-file">
                        @csrf
                        <div class="form-group">
                            <input type="file" name="file[]" id="file" class="form-control" multiple="multiple">
                            <span>*Jika file di return harap upload semua file sebelum return</span>
                        </div>
                        <button type="submit" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i>
                            Submit</button>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->