<table border="1" cellpadding="0" cellspacing="0" style="margin-left57.6pt;border-collapse:collapse;border:none;"
    width="100%">
    <tbody>
        <tr>
            <td style="width: 522pt;border: 1pt solid windowtext;padding: 0cm 5.4pt;height: 71.5pt;vertical-align: top;"
                valign="top" width="100%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;line-height:115%;'>
                    <strong><span style="font-size:15px;line-height:115%;">BERITA ACARA KESEPAKATAN</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;line-height:115%;'>
                    <strong><span style="font-size:15px;line-height:115%;" id="judul">XXXXXXXXXXXX</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;line-height:115%;'>
                    <strong><span style="font-size:15px;line-height:115%;">ANTARA &nbsp; &nbsp;&nbsp;</span></strong>
                </p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;line-height:115%;'>
                    <strong><span style="font-size:15px;line-height:115%;">PT. PINS INDONESIA</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;line-height:115%;'>
                    <strong><span style="font-size:15px;line-height:115%;">&nbsp;DENGAN&nbsp;</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;line-height:115%;'>
                    <strong><span style="font-size:15px;line-height:115%;" id="mitra">MITRA(SUPPLIER)</span></strong>
                </p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;line-height:115%;'>
                    <strong><span style="font-size:15px;line-height:115%;" id="nomor_bak">NOMOR :
                            XXXXXXXXXXXX</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;line-height:115%;'>
                    <strong><span style="font-size:15px;line-height:115%;" id="tanggal_bak">XXXXXXXXXXXXXXX</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;line-height:115%;'>
                    <strong><span style="font-size:15px;line-height:115%;">&nbsp;</span></strong></p>
            </td>
        </tr>
        <tr>
            <td style="width: 522pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(166, 166, 166);padding: 0cm 5.4pt;height: 11.2pt;vertical-align: top;"
                valign="top" width="100%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;line-height:115%;'>
                    <strong><span
                            style="font-size:15px;line-height:115%;color:black;">DASAR&nbsp;</span></strong><strong><span
                            style="font-size:15px;line-height:115%;color:black;">KESEPAKATAN</span></strong></p>
            </td>
        </tr>
        <tr>
            <td style="width: 522pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;padding: 0cm 5.4pt;height: 11.2pt;vertical-align: top;"
                valign="top" width="100%">

                <ol style="margin-bottom:0cm;list-style-type: decimal;">
                    <li style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'>
                        <span
                            style='line-height:115%;font-family:"Times New Roman",serif;font-size:12.0pt;font-family:"Times New Roman",serif;' id="surat_spph">XXXXXXXXXXXX.</span>
                    </li>
                    <li style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'>
                        <span
                            style='line-height:115%;font-family:"Times New Roman",serif;font-size:12.0pt;font-family:"Times New Roman",serif;' id="surat_sph">XXXXXXXXXXXX.
                        </span>
                    </li>
                </ol>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;margin-left:17.1pt;text-align:justify;line-height:115%;'>
                    <span style="font-size:15px;line-height:115%;">&nbsp;</span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 522pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(160, 160, 160);padding: 0cm 5.4pt;vertical-align: top;"
                valign="top" width="100%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;line-height:115%;'>
                    <strong><span style="font-size:15px;line-height:115%;color:black;">KESEPAKATAN</span></strong></p>
            </td>
        </tr>
        <tr>
            <td style="width: 522pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;padding: 0cm 5.4pt;vertical-align: top;"
                valign="top" width="100%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:justify;line-height:115%;'>
                    <span style="font-size:15px;line-height:115%;">PT. PINS Indonesia dan Supplier sepakat atas hal-hal
                        sebagai berikut :</span></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:justify;line-height:115%;'>
                    <span style="font-size:15px;line-height:115%;">&nbsp;</span></p>

                <ol style="margin-bottom: 0cm;">
                    <li style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'>
                        <b><span
                                style='line-height:115%;font-family:"Times New Roman",serif;font-size:12.0pt;font-family:"Times New Roman",serif;'>Ruang
                                Lingkup Pekerjaan</span></b>
                        <br><span style="font-size:15px;line-height:115%;" id="ruang_lingkup">JUDUL</span><span
                            style="font-size:15px;line-height:  115%;">.</span>
                        <br>
                        <br>
                    </li>
                    <li style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'>
                        <b><span
                                style='line-height:115%;font-family:"Times New Roman",serif;font-size:12.0pt;font-family:"Times New Roman",serif;'>Lokasi
                                Pekerjaan</span></b>
                        <br><span style="font-size:15px;line-height:115%;"><span>Barang diserahkan ke Gudang PT
                                PINS Indonesia, Jl. Percetakan Negara No.17A, RT.19/RW.7, Johar Baru, Kota Jakarta
                                Pusat, Daerah Khusus Ibukota Jakarta 10440.</span></span>
                        <br>
                        <br>
                    </li>
                    <li style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'>
                        <b><span
                                style='line-height:115%;font-family:"Times New Roman",serif;font-size:12.0pt;font-family:"Times New Roman",serif;'>Jangka
                                Waktu Pelaksanaan Pekerjaan :&nbsp;</span></b>
                        <br><span style="font-size:15px;line-height:115%;">Penyerahan barang selambat-lambatnya sampai dengan tanggal <span class="fr-class-highlighted">16 Februari
                            2019</span> yang dibuktikan dengan Berita Acara Serah Terima Barang (BASTB).</span>
                        <br>
                        <br>
                    </li>
                    <li style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'>
                        <b><span
                                style='line-height:115%;font-family:"Times New Roman",serif;font-size:12.0pt;font-family:"Times New Roman",serif;'>Harga Pekerjaan
                                :</span></b>
                        <br>
                        <span style="font-size:15px;line-height:115%;">
                            <span id="harga_bak">XXXXXXXXXXXX</span></span>
                        <br>
                        <br>
                    </li>
                    <li style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'>
                        <b><span style='font-family:"Times New Roman",serif;font-size:12.0pt;'>Tatacara Pembayaran
                                :</span></b>
                        <br><span style="font-size:15px;">PT. PINS Indonesia akan melaksanakan pembayaran kepada Supplier secara sekaligus sebesar 
                            100% (seratus persen) dari nilai total berdasarkan Berita Acara Kesepakatan (BAK) terkait atau sebesar <span id="hrg_bak">XXXXXXXXXXXX</span>
                         yang dilampirkan dengan :</span>

                        <ol style="list-style-type: lower-alpha;">
                            <li><span style="font-size:15px;">Lembar asli Berita Acara Uji Terima (BAUT) cq. MGR Catalogue &amp;
                                    Partnership Management dan Supplier;</span></li>
                            <li><span style="font-size:15px;">Lembar asli Berita Acara Serah Terima Barang (BASTB) cq. GM
                                    e-Commerce dan Supplier;</span></li>
                            <li><span style="font-size:15px;">Surat Permohonan Bayar;</span></li>
                            <li><span style="font-size:15px;">Invoice;</span></li>
                            <li><span style="font-size:15px;">Kuitansi Bermaterai;</span></li>
                            <li><span style="font-size:15px;">Faktur pajak (dengan dilampirkan Surat Pemberian Nomer
                                    Seri Faktur Pajak dari Dirjen Pajak terkait);</span></li>
                            <li><span style="font-size:15px;">Copy Berita Acara Kesepakatan (BAK).</span>
                                <br>
                                <br>
                            </li>
                        </ol>
                    </li>
                    <li
                        style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif;'>
                        <b><span style='font-family:"Times New Roman",serif;font-size:12.0pt;'>Ketentuan dan Syarat
                                Tambahan</span></b>

                        <ol style="list-style-type: lower-alpha;">
                            <li
                                style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif;'>
                                <span style='font-family:"Times New Roman",serif;font-size:12.0pt;'>Supplier Menyerahkan
                                    dan/atau menyelesaikan barang dan/atau hasil pekerjaan tepat waktu, tepat jumlah,
                                    sesuai dengan Spesifikasi Teknis yang telah ditetapkan oleh PT. PINS
                                    Indonesia;</span></li>
                            <li
                                style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif;'>
                                <span style='font-family:"Times New Roman",serif;font-size:12.0pt;'>Barang dan/atau
                                    hasil pekerjaan yang diserahkan oleh Supplier tidak bertentangan dengan dan/atau
                                    melanggar hukum dan/atau melanggar Hak milik pihak lain;</span></li>
                            <li
                                style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif;'>
                                <span style='font-family:"Times New Roman",serif;font-size:12.0pt;'>Supplier melampirkan
                                    lampiran serial number perangkat dalam bentuk hardcopy dan softcopy;</span></li>
                            <li
                                style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif;'>
                                <span style='font-family:"Times New Roman",serif;font-size:12.0pt;'><span
                                        class="fr-class-highlighted">Supplier melampirkan bukti foto serah terima barang di
                                        lokasi;</span></span>
                            </li>
                            <li
                            style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif;'>
                            <span style='font-family:"Times New Roman",serif;font-size:12.0pt;'><span
                                    class="fr-class-highlighted">Supplier memberikan garansi perangkat selama 1 tahun yang dilengkapi dengan kartu garansi dari Distributor resmi di Indonesia;</span></span>
                            </li>
                            <li
                            style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif;'>
                            <span style='font-family:"Times New Roman",serif;font-size:12.0pt;'><span
                                    class="fr-class-highlighted">Supplier bersedia dikenakan Sanksi Denda Keterlambatan sebesar 1‰ (satu per mil) per hari keterlambatan dikalikan nilai barang dan/ atau hasil pekerjaan yang belum diterima, dengan maksimal denda sebesar 5% (lima persen). Apabila jumlah sanksi denda keterlambatan telah mencapai 5% (lima persen) dari nilai total barang dan/ atau hasil pekerjaan berdasarkan BAK ini, PT. PINS Indonesia berhak membatalkan sisa barang dan/atau sisa pekerjaan yang belum diterima;</span></span>
                            </li>
                            
                        </ol>
                    </li>
                </ol>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;'>
                    <span style="font-size:15px;">&nbsp;</span></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:justify;'>
                    <span style="font-size:15px;">Demikian Berita Acara Kesepakatan ini dibuat sebagai dasar pelaksanaan pekerjaan.</span></p>
                <br>
                <table align="left" border="1" cellpadding="0" cellspacing="0"
                    style="border-collapse:collapse;border:none;margin-left:6.75pt;margin-right:6.75pt;" width="100%">
                    <tbody>
                        <tr>
                            <td style="width: 50%;border-top: 1pt dotted windowtext;border-left: none;border-bottom: none;border-right: 1pt dotted windowtext;padding: 0cm 5.4pt;height: 98.35pt;vertical-align: top;"
                                valign="top" width="50.892857142857146%">

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">PT. PINS Indonesia,</span></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">&nbsp;</span></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">&nbsp;</span></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">&nbsp;</span></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">&nbsp;</span></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">&nbsp;</span></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">&nbsp;</span></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><u><span style="font-size:15px;">Hernadi Yoga Adhitya
                                                Tama</span></u></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">GM e-Commerce</span></strong></p>
                            </td>
                            <td style="width: 50%;border-right: none;border-bottom: none;border-left: none;border-image: initial;border-top: 1pt dotted windowtext;padding: 0cm 5.4pt;height: 98.35pt;vertical-align: top;"
                                valign="top" width="49.107142857142854%">

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;" id="mitra_perusahaan">MITRA</span></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">&nbsp;</span></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">&nbsp;</span></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">&nbsp;</span></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">&nbsp;</span></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">&nbsp;</span></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">&nbsp;</span></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><u><span style="font-size:15px;" id="direktur">DIREKTUR</span></u></strong>
                                </p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">Direktur Utama</span></strong></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
