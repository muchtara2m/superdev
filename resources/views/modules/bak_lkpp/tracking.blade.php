@extends('layouts.master')

@section('title')
Tracking BAK
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<!-- Moment --> 
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAK <small>Tracking Document</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">BAK</a></li>
            <li class="active"> Tracking </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i>Tracking</h3>
                        <div class="pull-right box-tools">
                            <a href="/export-spph-lkpp" class="btn btn-info"><i class="fa fa-download"></i> </a>
                        </div>
                    </div>
                    <div style="text-align:center">
                        <div>
                            <select name="bulan" id="bulan" class="form-group" style="width:10%;height:34px">
                            </select>
                            <select name="tahun" id="tahun" class="form-group" style="width:10%;height:34px">
                            </select>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="tracking" class="display" width="100%" data-url="/lkpp/bak/tracking">
                            <thead>
                                <tr id="kepala">
                                    <th rowspan="2" style="text-align:center">No</th>
                                    <th rowspan="2" style="text-align:center">Key</th>
                                    <th rowspan="2" style="text-align:center">No.BAK</th>
                                    <th rowspan="2" style="text-align:center">Judul</th>
                                    <th rowspan="2" style="text-align:center">Tgl.BAK</th>
                                    <th rowspan="2" style="text-align:center">Mitra</th>
                                    <th rowspan="2" style="text-align:center">Harga</th>
                                    <th rowspan="2" style="text-align:center">Tgl.Buat</th>
                                    <th rowspan="2" style="text-align:center">Pembuat</th>
                                    <th colspan="3" style="text-align:center">Approval</th>
                                    <th rowspan="2" style="text-align:center">Posisi</th>
                                    <th rowspan="2" style="text-align:center">Action</th>
                                </tr>
                                <tr style=" white-space: nowrap">
                                    <th style="text-align:center">Creator</th>
                                    <th style="text-align:center">Mgr</th>
                                    <th style="text-align:center">GM</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<!-- External -->
<script src="{{ asset('js/web/main/js/bak_lkpp/tracking.js')}}"></script>

@endsection