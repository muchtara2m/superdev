@extends('layouts.master')


@section('title')
Approve BAK
@endsection

@section('stylesheets')
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<!-- smartwizard -->
<link href="{{ asset('smartwizard/dist/css/smart_wizard.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('smartwizard/dist/css/smart_wizard_theme_circles.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('smartwizard/dist/css/smart_wizard_theme_circles.min.css') }}" rel="stylesheet" type="text/css" />
<!-- DataTable -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAK <small>Approve</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">BAK</a></li>
            <li class="active">Approve </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Approve BAK</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i
                                class="fa fa-arrow-left"></i></button>
                    </div>
                    <div class="box-body">
                        <div class="form-group">        
                            <!--extend file dokumen-->
                                @php
                                // Spph
                                $titleLampiranSpph  = ($bak->spph_bak_lkpp['lampiran'])    !=NULL ? json_decode($bak->spph_bak_lkpp['title_lampiran'], TRUE) : NULL;
                                $lampiranSpph       = ($bak->spph_bak_lkpp['lampiran'])    !=NULL ? json_decode($bak->spph_bak_lkpp['lampiran'], TRUE) : NULL;
                                $titleFileSpph      = ($bak->spph_bak_lkpp['file'])        !=NULL ? json_decode($bak->spph_bak_lkpp['title'], TRUE) : NULL ;
                                $fileSpph           = ($bak->spph_bak_lkpp['file'])        !=NULL ? json_decode($bak->spph_bak_lkpp['file'], TRUE) : NULL;
                                // Bakn
                                $titleLampiranBak  = ($bak->lampiran)  !=NULL ? json_decode($bak->title_lampiran, TRUE) : NULL;
                                $lampiranBak       = ($bak->lampiran)  !=NULL ? json_decode($bak->lampiran, TRUE) : NULL;
                                $titleFileBak      = ($bak->file)      !=NULL ? json_decode($bak->title, TRUE) : NULL;
                                $fileBak           = ($bak->file)      !=NULL ? json_decode($bak->file, TRUE) : NULL;
                                @endphp
                            <table class="display">
                                <thead>
                                    <tr>
                                        <th>Lampiran SPPH</th>
                                        <th>File SPPH</th>
                                        <th>Lampiran BAK</th>
                                        <th>File BAK</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            @if ($titleLampiranSpph != NULL)
                                            @foreach ($titleLampiranSpph as $key => $value)
                                                {{ ($key+1)}}. <a target="_blank" href="{{Storage::url($lampiranSpph[$key])}}">{{$titleLampiranSpph[$key]}}</a><br>
                                            @endforeach
                                        @endif
                                        </td>
                                        <td>
                                            @if ($titleFileSpph != NULL)
                                            @foreach ($titleFileSpph as $key => $value)
                                                {{ ($key+1)}}. <a target="_blank" href="{{Storage::url($fileSpph[$key])}}">{{$titleFileSpph[$key]}}</a><br>
                                            @endforeach
                                        @endif
                                        </td>
                                        <td>
                                            @if ($titleLampiranBak != NULL)
                                            @foreach ($titleLampiranBak as $key => $value)
                                                {{ ($key+1)}}. <a target="_blank" href="{{Storage::url($lampiranBak[$key])}}">{{$titleLampiranBak[$key]}}</a><br>
                                            @endforeach
                                        @endif
                                        </td>
                                        <td>
                                           @if ($titleFileBak != NULL)
                                            @foreach ($titleFileBak as $key => $value)
                                                {{ ($key+1)}}. <a target="_blank" href="{{Storage::url($fileBak[$key])}}">{{$titleFileBak[$key]}}</a><br>
                                            @endforeach
                                        @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <td>

                            </td>
                        </div>
                            <div class="form-groupp">
                                <div id="smartwizard">
                                    <ul>
                                        <li><a href="#step-1">Step 1<br /><small>Draft BAK</small></a></li>
                                        <li><a href="#step-2">Step 2<br /><small>SPPH</small></a></li>
                                        <li><a href="#step-3">Step 3<br /><small>Approval</small></a></li>
                                    </ul>
                                    <div id="isi">
                                        <div id="step-1" class="">
                                            @include('modules.bak_lkpp.inc.bak_preview')
                                        </div>
                                        <div id="step-2" class="">
                                            @include('modules.spph_lkpp.inc.spph_preview')
                                        </div>
                                        <div id="step-3" class="">
                                                <div class="col-12">
                                                    <h4>Coment and Approve</h4>
                                                    <ul class="timeline">
                                                        @foreach ($chats as $isi)
                                                        <!-- timeline time label -->
                                                        <li class="time-label">
                                                            <span class="bg-green">
                                                                {{ date('d M.Y', strtotime($isi->created_at)) }}
                                                            </span>
                                                        </li>
                                                        <!-- /.timeline-label -->

                                                        <!-- timeline item -->
                                                        <li>
                                                            <!-- timeline icon -->
                                                            <i class="fa fa-user bg-aqua"></i>
                                                            <div class="timeline-item">
                                                                <span class="time">
                                                                    <i class="fa fa-clock-o"></i>
                                                                    {{ date('H:i:s', strtotime($isi->created_at)) }}
                                                                </span>

                                                                <h3 class="timeline-header"><a
                                                                        href="#">{{ $isi->jabatan.' - '.$isi->name }}</a>
                                                                </h3>

                                                                <div class="timeline-body">
                                                                    {{ $isi->chat }}
                                                                </div>

                                                                <div class="timeline-footer">
                                                                    {{-- <a class="btn btn-primary btn-xs">...</a> --}}
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <!-- END timeline item -->

                                                        @endforeach
                                                        <li>
                                                            <i class="fa fa-clock-o bg-gray"></i>
                                                        </li>
                                                    </ul>
                                                </div> <!-- End div col-12-->

                                                @if($bak->approval == Auth::user()->username || Auth::user()->level=='administrator')

                                                    <div class="col-12 ">
                                                        <form   action="/chat-approval-bak-lkpp"
                                                                class="form-group" 
                                                                method="post">
                                                            @csrf
                                                            <div class="col-md-12">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="exampleInputEmail1">Comment</label>
                                                                        <input  type="text" 
                                                                                class="form-control" 
                                                                                name="chat"
                                                                                id="chat"
                                                                                required>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6" hidden>
                                                                    <div class="form-group">
                                                                        <label for="id">ID Transaksi</label>
                                                                        <input type="text" class="form-control"
                                                                            name="idBak" value="{{ $bak->id }}"
                                                                            readonly>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12 mar-paginate pad-0">
                                                                    <button type="submit" 
                                                                            class="btn btn-danger"
                                                                            name="status" 
                                                                            value="Return">
                                                                            Return
                                                                    </button>
                                                                    <button type="submit" 
                                                                            class="btn btn-primary"
                                                                            name="status" 
                                                                            id="submit" 
                                                                            value="Approve">
                                                                        Approve
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div> <!-- End div col-12-->
                                                @endif
                                        </div><!-- End div step 3-->
                                    </div><!-- End div isi-->
                                </div><!-- End div smartwizard-->
                            </div><!-- End div form-group-->
                        </div><!-- End div box-body-->
                    </div>
                </div>
            </div><!-- /.row -->
        </section><!-- /.content -->
    </div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- DataTable -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<!-- Initialize the editor. -->
<script src="{{ asset('froala/js/languages/id.js') }}"></script>
{{-- smart wizard --}}
<script type="text/javascript" src="{{ asset('smartwizard/dist/js/jquery.smartWizard.min.js') }}"></script>

<script>
    $(document).ready(function () {
        $('div#smartwizard').smartWizard();
        $('div#smartwizardcircle').smartWizard();
    });
    $('textarea').froalaEditor({
        toolbarButtons: ['container'],
        toolbarButtons: ['getPDF', 'print'],
        charCounterCount: false,
        key: '{{ env("KEY_FROALA") }}',
    })
</script>
@endsection