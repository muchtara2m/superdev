@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "BAK";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
{{--  Tagify  --}}
<link rel="stylesheet" href="{{ asset('tagify/dist/tagify.css') }}">
@endsection


@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAK
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">BAK</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Create BAK</h3>
                    </div>
                    <!-- /.box-header -->
                    <br>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    
                    {{-- start form  --}}
                    <form method="post" action="{{ url('store-bak-lkpp') }}" enctype="multipart/form-data" id="form">
                        
                        @csrf
                        <div class="box-body">
                            <div class="form-group col-md-12 igroup">
                                <label for="nomorspph">Select SPPH</label><a id="spphpreview" target="_blank" href="#"><i class="fa fa-fw fa-file-code-o"></i></a>
                                <input type="text" name="nmrspph" class="form-control" data-toggle="modal" data-target="#modal-unit-spph"  placeholder="Pilih SPPH" id="nmrspph" value="{{ old('nmrspph') }}">
                            </div>
                            <input type="hidden" id="spphid" name="spphid" value="{{ old('spphid') }}">
                            
                            <div class="form-group col-md-4 igroup">
                                <label for="tglbakn">Tanggal BAK</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datejos" name="tglbak" id="tgl_bak" value="{{ old('tglbak') }}">
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="harga">Harga</label>
                                <div class="input-group col-md-12">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control" onkeyup="formatAngka(this,'.')" name="harga" placeholder="1000000" id="harganum" value="{{ old('harga') }}" >
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="harga">No. BAK</label>                                
                                <input type="text" name="no_bak" id="no_bak" class="form-control" value="{{ old('no_bak') }}">
                            </div>
                            
                            {{-- hasil pembahasan --}}
                            <div class="col-md-12" style="margin-top:5%">
                                <div class="form-group">
                                    <h4 class="divider-title center" style="text-align:center">HASIL PEMBAHASAN</h4>
                                    <hr>
                                </div>
                                <div class="form-group">
                                    <textarea id="agenda" rows="10" cols="80" name="isi" placeholder="Type Something" class="bakn">
                                        @if (old('isi') == null)
                                            @include('modules.bak_lkpp.modal.preview_bak')   
                                        @else
                                            {{ old('isi') }}
                                        @endif
                                    </textarea>
                                </div>
                                
                                <div class="form-group">
                                    <label for="">File Lampiran</label>
                                    <input type="file" name="lampiran[]" class="form-control" multiple="multiple">                                    
                                </div>
                                <div class="form-group">
                                    <label for="">Chat</label><span>*require for submit </span>
                                    <input type="text"  name="chat"class="form-control">
                                </div>
                            </div>
                            {{-- end hasil pembahasan --}}
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" name="status" value="draft_bak" class="btn btn-primary" style="width: 7em;"><i class="fa fa-check"></i> Save</button>
                            <button type="submit" name="status" value="save_bak" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    {{-- end form --}}
                </div>
                <!-- /.box -->
                @include('modules.bak_lkpp.modal.spph_modal')
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<!-- Data Table -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- clockpicker -->
<script src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Tagify -->
<script src="{{ asset('tagify/dist/jQuery.tagify.min.js') }}"></script>
<script src="{{ asset('tagify/dist/tagify.min.js') }}"></script>
<script src="{{ asset('js/bak_lkpp.js') }}"></script>
<script>
    // froala
 $('.bakn').froalaEditor({
    placeholderText: '',
    charCounterCount: false,
    key: '{{ env("KEY_FROALA") }}',
});
    // ajax set tgl_bak
    $(document).ready(function(){
        $(document).change(function(){
            var nomorspph=$('#nmrspph').val();
            var token = $("input[name='_token']").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'get',
                url:"{{ url('data-spph-lkpp') }}",
                data: {nomorspph:nomorspph, _token:token},
                dataType:'json',
                success:function(data){
                    const datespph = new Date(data.options['tglspph']);
                    const monthspph = datespph.toLocaleString('id-ID', {month:'long'});
                    const tglspph = datespph.getDate()+' '+monthspph+' '+datespph.getFullYear();
                    const datesph = new Date(data.options['tglsph']);
                    const monthsph = datesph.toLocaleString('id-ID', { month: 'long' });
                    const tglsph = datesph.getDate()+' '+monthsph+' '+datesph.getFullYear();

                    // judul
                    $('#judul').html(data.options['judul'].toUpperCase());
                    $('#surat_spph').html(`Surat
                            Permintaan Penawaran Harga (SPPH) nomor: ${data.options['nomorspph']} tanggal ${tglspph} permintaan penawaran harga.`);
                    $('#surat_sph').html(`Surat
                            dari Supplier tanggal ${tglsph} perihal Penawaran Harga.`);
                    $('#ruang_lingkup').html(data.options['judul']);
                    $('#mitra_perusahaan').html(data.options['mitra_lkpps']['perusahaan']);
                    $('#mitra').html(data.options['mitra_lkpps']['perusahaan'].toUpperCase());
                    $('#direktur').html(data.options['mitra_lkpps']['direktur']);
                },
                error:function(){
                    
                }
            });
        });
    });
    
</script>
@endsection
