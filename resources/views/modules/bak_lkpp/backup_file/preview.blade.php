@extends('layouts.master')

@section('title')
Preview BAK
@endsection

@section('stylesheets')
{{-- css froala editor --}}
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<style>
      /* .bottomright {
      position  : absolute;
      bottom    : 5mm;
      right     : 16px;
      font-size : 18px; */
    }
    @page {
    size            : A4;
    margin-top      : 2cm;
    margin-bottom   : 2cm;
    margin-left     : 30mm; 
    margin-right    : 20mm;
 }
  @media print {
    /* .bottomright{
        position    : fixed;
        bottom      : 0;
    } */
     body {
        margin-top      : 20mm; 
        margin-bottom   : 20mm; 
        margin-left     : 30mm; 
        margin-right    : 20mm;
        }
    }
</style>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAK <small>Preview</small>
        </h1>
        
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">BAK</a></li>
            <li class="active"> Preview </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Preview BAK</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                       @include('modules.bak_lkpp.inc.bak_preview')
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
{{-- script function froala --}}
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('froala/js/plugins/word_paste.min.js') }}"></script>
<script src="{{ asset('froala/js/languages/id.js') }}"></script>
<script type="text/javascript">
    
    $('textarea').froalaEditor({
        fullPage: true,
        // toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo', 'getPDF'],
        toolbarButtons :['print', 'html','getPDF'],
        charCounterCount: false,
        language: 'id',
        key: '{{ env("KEY_FROALA") }}',
    })</script>
    @endsection
    