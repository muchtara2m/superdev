@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "BAK";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
{{--  Tagify  --}}
<link rel="stylesheet" href="{{ asset('tagify/dist/tagify.css') }}">
@endsection


@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAK
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">BAK</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Create BAK</h3>
                    </div>
                    <!-- /.box-header -->
                    <br>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    
                    {{-- start form  --}}
                    <form method="post" action="{{ url('update-bak-lkpp',$bak->id) }}" enctype="multipart/form-data" id="form">
                        
                        @csrf
                        <div class="box-body">
                            <div class="form-group col-md-12 igroup">
                                <label for="nomorspph">Select SPPH</label><a id="spphpreview" target="_blank" href="#"><i class="fa fa-fw fa-file-code-o"></i></a>
                                <input type="text" name="nmrspph" class="form-control" id="nmrspph" value="{{ $bak->spph_bak_lkpp['nomorspph'] }}">
                            </div>
                            <input type="hidden" id="spphid" name="spphid" value="{{ $bak->spph_id }}">
                            
                            <div class="form-group col-md-4 igroup">
                                <label for="tglbakn">Tanggal BAK</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datejos" name="tglbak" id="tgl_bak" value="{{ $bak->tgl_bak }}">
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="harga">Harga</label>
                                <div class="input-group col-md-12">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control" onkeyup="formatAngka(this,'.')" name="harga" placeholder="1000000" id="harganum" value="{{ $bak->harga }}" >
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="harga">No. BAK</label>                                
                                <input type="text" name="no_bak" id="no_bak" class="form-control" value="{{ $bak->nomor_bak }}">
                            </div>
                            
                            {{-- hasil pembahasan --}}
                            <div class="col-md-12" style="margin-top:5%">
                                <div class="form-group">
                                    <h4 class="divider-title center" style="text-align:center">HASIL PEMBAHASAN</h4>
                                    <hr>
                                </div>
                                <div class="form-group">
                                    <textarea id="agenda" rows="10" cols="80" name="isi" placeholder="Type Something" class="bakn">
                                        {{ $bak->isi }}    
                                    </textarea>
                                </div>
                                
                                <div class="form-group">
                                    <label for="">File Lampiran</label>
                                    <input type="file" name="lampiran[]" class="form-control" multiple="multiple">
                                    <td>
                                        @php
                                        $flampiran = json_decode($bak->lampiran, TRUE);
                                        $tlampiran = json_decode($bak->title_lampiran, TRUE);
                                        $i=1;
                                        @endphp
                                        @if($flampiran != NULL)
                                        @foreach ($tlampiran as $keylampiran =>$vallampiran)
                                        {{ $i++.'. ' }}<a href="{{ Storage::url($flampiran[$keylampiran]) }}">{{ $tlampiran[$keylampiran] }}</a><br>
                                        @endforeach
                                        @endif
                                    </td>                     
                                </div>
                                <div class="form-group">
                                    <label for="">Chat</label><span>*require for submit </span>
                                    <input type="text"  name="chat"class="form-control">
                                </div>
                            </div>
                            {{-- end hasil pembahasan --}}
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" name="status" value="draft_bak" class="btn btn-primary" style="width: 7em;"><i class="fa fa-check"></i> Save</button>
                            <button type="submit" name="status" value="save_bak" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    {{-- end form --}}
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<!-- Data Table -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- clockpicker -->
<script src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Tagify -->
<script src="{{ asset('tagify/dist/jQuery.tagify.min.js') }}"></script>
<script src="{{ asset('tagify/dist/tagify.min.js') }}"></script>
{{-- <script src="{{ asset('js/bak_lkpp.js') }}"></script> --}}
<script>
    // froala
    $('.bakn').froalaEditor({
        placeholderText: '',
        charCounterCount: false,
        key: '{{ env("KEY_FROALA") }}',
    });
    // format currency harga
    function formatAngka(objek, separator) {
        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;
        j = 0;
        for (i = panjang; i > 0; i--) {
            j = j + 1;
            if (((j % 3) == 1) && (j != 1)) {
                c = b.substr(i - 1, 1) + separator + c;
            } else {
                c = b.substr(i - 1, 1) + c;
                formatAngka
            }
        }
        objek.value = c;
    }
    
    function clearDot(number)
    {
        output = number.replace(".", "");
        return output;
    }
    // script datepicker
    $(function () {
        $(".datejos").on("change", function () {
            this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
            .format(this.getAttribute("data-date-format"))
            )
        }).trigger("change")
    });
    $('.datejos').datepicker({
        autoclose: true,
        orientation: "bottom"
    });
    // harga terbilang
    $(document).ready(function () {
        function addCommas(nStr) {
            nStr += '';
            x = nStr.split(',');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            if (x.length == 1) {
                x[1] = "00";
            }
            return x1 + "," + x[1];
        };
        $('#harganum').change(function () {
            var hrg = $('#harganum').val();
            hapusdot = hrg.split('.').join("");
            var blg = terbilangs(hapusdot) + "Rupiah";
            let hasil_format_currency = `Harga pekerjaan adalah sebesar <strong>Rp. ${addCommas($('#harganum').val())},- (${blg})</strong> sudah termasuk PPN 10% (sepuluh persen) dengan rincian sebagai berikut:`;
            let hasil_hrg_bak = `Rp.${addCommas($('#harganum').val())},- (${blg}) sudah termasuk PPN 10% (sepuluh persen) `;
            $('#harga_bak').html(hasil_format_currency);
            $('#hrg_bak').html(hasil_hrg_bak);
            
        });
        
        function terbilangs(n) {
            k = ['', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan'];
            a = [1000000000000000, 1000000000000, 1000000000, 1000000, 1000, 100, 10, 1];
            s = ['Kuadriliun', 'Trilyun', 'Milyar', 'Juta', 'Ribu', 'ratus', 'Puluh', ''];
            var i = 0,
            x = '';
            var j = 0,
            y = '';
            var angka = n;
            //alert(angka);
            var inp_pric = String(angka).split(",");
            while (inp_pric[0] > 0) {
                b = a[i], c = Math.floor(inp_pric[0] / b), inp_pric[0] -= b * c;
                x += (c >= 10 ? terbilangs(c) + "" + s[i] + " " : ((c > 0 && c < 10) ? k[c] + " " + s[i] + " " : ""));
                i++;
                
            }
            var depan = x.replace(new RegExp(/Satu Puluh (\w+)/gi), '$1 belas').replace(new RegExp(/satu (ribu|ratus|puluh|belas)/gi), 'Se\$1');
            
            while (inp_pric[1] > 0) {
                z = a[j], d = Math.floor(inp_pric[1] / z), inp_pric[1] -= z * d;
                y += k[d] + " ";
                j++;
            }
            var belakang = y.replace(new RegExp(/satu Puluh (\w+)/gi), '$1 Belas').replace(new RegExp(/satu (Ribu|Ratus|Puluh|Belas)/gi), 'Se\$1');
            
            if (belakang != '') {
                return depan + "koma" + belakang;
            } else {
                return depan;
            }
            
        };
    });
    // set default no_bak dan tgl_bak
    let get_tgl_bak = document.getElementById('tgl_bak');
    var nomornya = document.getElementById('no_bak');
    var tglnya = nomornya.value.split('/');
    var nilai=[];
    for (var i = 0; i < tglnya.length-1; i++) {
        nilai.push(tglnya[i]) ;
    }
    function useValue() {
        let NameValue = get_tgl_bak.value.split('-');
        // use it
        document.getElementById('no_bak').value = nilai.join('/')+'/'+NameValue[0];
        
    }
    get_tgl_bak.onchange = useValue;
    get_tgl_bak.onblur = useValue;
    
    // change nomor bak
    $('#no_bak').change(function() {
        var nomor = $('#no_bak').val();
        let tgl_baknya = $('#tgl_bak').val();
        let split_tgl = tgl_baknya.split('-');
        const date_bak = new Date(tgl_baknya);
        const month_bak = date_bak.toLocaleString('id-ID', {month:'long'});
        $('#tanggal_bak').html(`TANGGAL ${split_tgl[2]} ${month_bak.toUpperCase()} ${split_tgl[0]}`);
        $('#nomor_bak').html(`NOMOR : ${nomor}`);
    });
    
    
</script>
@endsection
