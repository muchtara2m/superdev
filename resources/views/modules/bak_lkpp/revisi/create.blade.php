@extends('layouts.master')

@section('title')
Revisi BAK
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.4/select2.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css" />
<!-- Include Editor style. -->
<link rel="stylesheet" href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" />
<link rel="stylesheet" href="{{ asset('froala/css/froala_style.min.css') }}" />
<style>
    .capitalize {
  text-transform: capitalize;
}
.select2-container-multi .select2-choices .select2-search-choice {
    padding: 5px 5px 5px 18px;
}
</style>
@endsection


@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAK <small>Revisi {{ $bakLkpp->revisi }}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">BAK</a></li>
            <li class="active">Revisi</li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Revisi BAK</h3>
                    </div>
                    <!-- /.box-header -->
                    <br>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    
                    <!-- start form  -->
                    <form method="post" action="{{ route('lkpp.bak.revisi', $bakLkpp->id) }}" enctype="multipart/form-data" id="form">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="id" value="{{ $bakLkpp->id }}">
                        <input type="hidden" id="url" data-url="/lkpp/spph/not-draft/">
                        
                        <div class="box-body">
                            <div class="form-group col-md-12 igroup">
                                <label for="nomorspph">Select SPPH</label>
                                <a id="spphpreview" target="_blank" href="#"><i class="fa fa-fw fa-file-code-o"></i></a>
                               
                                        <input  class="form-control" 
                                        data-val="true" 
                                        placeholder="Pilih SPPH" 
                                        value="{{ old('nmrspph', $bakLkpp->spph_bak_lkpp['nomorspph']) }}" 
                                        name="nmrspph" 
                                        id="nospph" 
                                        type="hidden" >

                                <input  type="hidden" 
                                        name="spphid" 
                                        id="spphid" 
                                        value="{{ old('spphid', $bakLkpp->spph_id) }}">
                            </div>
                            
                            <div class="form-group col-md-4 igroup">
                                <label for="tglbakn">Tanggal BAK</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input  type="text" 
                                            data-date="" 
                                            data-date-format="yyyy-mm-dd" 
                                            class="form-control datejos" 
                                            name="tglbak" 
                                            id="tgl_bak" 
                                            value="{{ old('tglbak', $bakLkpp->tgl_bak) }}"
                                            >
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="harga">Harga</label>
                                <div class="input-group col-md-12">
                                    <span class="input-group-addon">Rp</span>
                                    <input  type="text" 
                                            class="form-control" 
                                            onkeyup="formatAngka(this,'.')" 
                                            name="harga"  
                                            id="harganum" 
                                            value="{{ old('harga',$bakLkpp->harga) }}" 
                                            >
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="harga">No. BAK</label>                                
                                <input  type="text" 
                                        name="nomor_bak" 
                                        id="no_bak" 
                                        class="form-control"
                                        data-old="{{ old('nomor_bak', $bakLkpp->nomor_bak) }}"
                                        value="{{ old('nomor_bak', $bakLkpp->nomor_bak) }}">
                            </div>
                            
                            {{-- hasil pembahasan --}}
                            <div class="col-md-12" style="margin-top:5%">
                                <div class="form-group">
                                    <h4 class="divider-title center" style="text-align:center">PREVIEW</h4>
                                    <hr>
                                </div>
                                <div class="form-group">
                                    <textarea   id="agenda" 
                                                rows="10" 
                                                cols="80" 
                                                name="isi" 
                                                class="bak">
                                        {{ old('isi', $bakLkpp->isi) }}    
                                    </textarea>
                                </div>
                                
                                <div class="form-group">
                                    <label for="">File Lampiran</label>
                                    <input type="file" name="lampiran[]" class="form-control" multiple="multiple">
                                    <td>
                                        @php
                                        $flampiran = json_decode($bakLkpp->lampiran, TRUE);
                                        $tlampiran = json_decode($bakLkpp->title_lampiran, TRUE);
                                        $i=1;
                                        @endphp
                                        @if($flampiran != NULL)
                                        @foreach ($tlampiran as $keylampiran =>$vallampiran)
                                        {{ $i++.'. ' }}<a href="{{ Storage::url($flampiran[$keylampiran]) }}">{{ $tlampiran[$keylampiran] }}</a><br>
                                        @endforeach
                                        @endif
                                    </td>                     
                                </div>
                                @if ($bakLkpp->hold == true)
                                <button type="submit" 
                                       
                                        class="btn btn-warning"
                                        style="width: 7em;">
                                        Save
                                </button>
                            @else
                                    <div class="form-group">
                                        <label for="">Chat</label><span>*require for submit </span>
                                        <input  type="text"  
                                                name="chat"
                                                class="form-control"
                                                value="{{ old('chat') }}">
                                    </div>
                                </div>
                            </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" 
                                            name="status" 
                                            value="draft_bak" 
                                            class="btn btn-primary" 
                                            style="width: 7em;">
                                            <i class="fa fa-check"></i> 
                                            Save
                                        </button>
                                    <button type="submit" 
                                            name="status" 
                                            value="save_bak" 
                                            class="btn btn-success" 
                                            style="width: 7em;">
                                            <i class="fa fa-check"></i> 
                                            Submit
                                        </button>
                                </div>
                                <!-- /.box-footer -->
                            @endif
                              
                    </form>
                    {{-- end form --}}
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- clockpicker -->
<script src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/4.15.0/lodash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.4/select2.min.js"></script>
<!-- External JS -->
<script src="{{ asset('js/web/main/js/bak_lkpp/edit.js')}}"></script>
@endsection