@extends('layouts.master')

@section('title')
Preview BAK
@endsection

@section('stylesheets')
<!-- css froala editor -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>BAK<small>Preview </small></h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">BAK</a></li>
            <li class="active"> Preview </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid" style="border-radius: 5px;border-left: 4px solid#00a7d0 !important;">
                    <div class="box-header with-border">
                        <h4 class="text-center">
                            <strong>
                                @if ($bakLkpp->revisi >=1)
                                    REVISI 
                                @endif
                                {{ strtoupper($bakLkpp->spph_bak_lkpp->judul) }}
                            </strong>
                        </h4>
                    </div>
                    <div class="box-body ">
                        <div class="row">
                            <div class="col-sm-4">
                                <dl class="dl-horizontal">
                                    <dt>Nomor BAK</dt>
                                    <dd>{{ $bakLkpp->nomor_bak }}</dd>
                                    <dt>Nomor SPPH</dt>
                                    <dd>{{ $bakLkpp->spph_bak_lkpp->nomorspph }}</dd>
                                    <dt>Nomor SPH</dt>
                                    <dd>{{ $bakLkpp->spph_bak_lkpp->nomorsph == NULL ? 'Belum Diinput' : $bakLkpp->spph_bak_lkpp->nomorsph }}</dd>
                                    <dt>Tanggal</dt>
                                    <dd>{{ date('d F Y',strtotime($bakLkpp->tgl_bak)) }}</dd>
                                    <dt>Mitra</dt>
                                    <dd>{{ $bakLkpp->spph_bak_lkpp->mitra_lkpps->perusahaan }}</dd>
                                </dl>
                            </div>
                            <div class="col-sm-2">
                                @if ($bakLkpp->revisi >=1)
                                <div class="row">
                                    <div class="col-xs-2"></div>
                                    <div class="col-xs-10">
                                        <dl>
                                            <dt>Revisi</dt>
                                            @php
                                                $x=0;
                                            @endphp
                                            @foreach ($bakLkpp->revisi_bak_lkpp as $item)
                                                <a target="_blank" href="/preview-revisi-bak-lkpp/{{ $item->id }}">  Revisi Ke - {{$x++ }}</a><br>
                                            @endforeach
                                            <p disabled="true"> Revisi Ke - {{ $bakLkpp->revisi }} </p>

                                        </dl>
                                    </div>
                                </div>
                                @endif
                              
                            </div>
                            <div class="col-sm-6">
                                @php
                                $title_lampiran = ($bakLkpp->lampiran) !=NULL ? json_decode($bakLkpp->title_lampiran, TRUE) : NULL;
                                $lampiran       = ($bakLkpp->lampiran) !=NULL ? json_decode($bakLkpp->lampiran, TRUE) : NULL;
                                $title          = ($bakLkpp->file) !=NULL ? json_decode($bakLkpp->title, TRUE) : NULL ;
                                $file           = ($bakLkpp->file) !=NULL ? json_decode($bakLkpp->file, TRUE) : NULL;
                                @endphp
                                <div class="row">
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>Lampiran BAK</dt>
                                            @if ($title_lampiran != NULL)
                                            @foreach ($title_lampiran as $key => $value)
                                            {{ ($key+1)}}. <a href="{{Storage::url($lampiran[$key])}}">{{$title_lampiran[$key]}}</a><br>
                                            @endforeach
                                            @endif
                                           
                                          </dl>
                                    </div>
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>File BAK</dt>
                                            @if ($title != NULL)
                                            @foreach ($title as $key => $value)
                                            {{ ($key+1)}}. <a
                                                href="{{Storage::url($file[$key])}}">{{$title[$key]}}</a><br>
                                            @endforeach
                                            @endif
                                           
                                          </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-solid" style="border-radius: 8px">
                        <textarea name="" id="" cols="30" rows="10">
                            {{ $bakLkpp->isi }}
                        </textarea>
                       
                </div>
            </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<!-- script function froala -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<script src="{{ asset('froala/js/languages/id.js') }}"></script>
<!-- External -->
<script type="text/javascript">
    $('textarea').froalaEditor({
        // fullPage: true,
        toolbarButtons      : ['print', 'html', 'getPDF', 'fullscreen'],
        charCounterCount    : false,
        key                 : keyFroala,
        height              : 500,
    })
    // removo alert lisensi froala
$("div > a", ".fr-wrapper").css('display', 'none');
</script>
@endsection