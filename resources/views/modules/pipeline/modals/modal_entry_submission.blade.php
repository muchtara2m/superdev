 {{-- modal update tgl closing --}}
 <div class="modal fade" id="modal-submission" style="display: none;">
    <div class="modal-dialog" style="width:fit-content">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Entry Submission</h4>
                </div>
                <div class="modal-body" style="padding-bottom:5%;">
                    <div id="updatestatus" class="ui-dialog-content ui-widget-content" scrolltop="0" scrollleft="0" style="width: auto; min-height: 72px; height: auto;">
                        <div style="height: 450px; overflow: auto">
                            {{-- <form enctype="multipart/form-data" onsubmit="return validatesubmission()" id="form-content-submission" action="/index.php?r=inisiasi/createsubmission&amp;id=6462" method="post">    <fieldset><legend><b>Submit Inisiasi "WIN" to Delivery Unit and Procurement Unit</b></legend> --}}
                                <div>
                                    <table class="item table table-striped">
                                        <tbody><tr>
                                            <td>No Inisiasi</td>
                                            <td>:</td>
                                            <td>2019ENTERPRISESERVICE20116</td>
                                        </tr>
                                        <tr>
                                            <td>No IO/Deskripsi Project</td>
                                            <td>:</td>
                                            <td>20190670 /  Video Conference untuk BPJS Kesehatan</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Project</td>
                                            <td>:</td>
                                            <td>Pekerjaan Pengadaan Renewal Maintenance Multipoint Control Unit (MCU) Video Conference untuk BPJS Kesehatan</td>
                                        </tr>
                                        <tr>
                                            <td>Nilai Project Inisiasi</td>
                                            <td>:</td>
                                            <td>385.796.700</td>
                                        </tr>
                                    </tbody></table>
                                </div>        <hr>
                                <table class="item table table-striped">
                                    <tbody><tr>
                                        <td>Submission To</td>
                                        <td>:</td>
                                        <td>
                                            <select id="submission_to" name="menu" class="form-control submission_to" required="required">
                                                <option value="">== PILIH DELIVERY ==</option>
                                                <option value="10" data-val="SDV2" >SDV2</option>
                                                <option value="5"  data-val="REG.TIMUR">REG. TIMUR</option>
                                                <option value="8"  data-val="REG.BARAT">REG. BARAT</option>
                                                <option value="9"  data-val="SDV1">SDV1</option>
                                                <option value="14" data-val="MILITRAY" >MILITARY</option>
                                                <option value="17" data-val="TELE" >TELE</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>File Submission</td>
                                        <td>
                                            :
                                        </td>
                                        <td>
                                            {{-- <input id="ytfile_submission" type="file" value="" name="Inisiasi[file_submission]"> --}}
                                            <input id="file_submission" class="file_submission" required="required"  type="file"></td>
                                        </tr>
                                        <tr>
                                            <td>Submission Date</td>
                                            <td>:</td>
                                            <td>
                                                <input readonly="readonly" id="tgl_submission" class="tgl_submission form-control" value="2019-10-30" name="Inisiasi[tgl_submission]" type="text">
                                            </td>
                                        </tr>
                                    </tbody></table>
                                    <hr>
                                    <div id="deliveryto">
                                        <span>Disubmit kepada :</span><br>
                                        <table class="items table table-striped">
                                            <tbody><tr>
                                                <td>No</td>
                                                <td>Kepada</td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>
                                                    <input id="deliveryname1" disabled="disabled" type="text"  name="deliveryname1" class="form-control">      
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>
                                                    <input disabled="disabled" id="delivery4" type="text" value="PROCUREMENT" name="delivery4" class="form-control">                        
                                                    <input id="delivery4" type="hidden" value="12" name="delivery[]">  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>3</td>
                                                <td>
                                                    <input disabled="disabled" id="delivery3" type="text" value="ENTERPRISE SERVICE 2" name="delivery3" class="form-control">                        
                                                    <input id="delivery3" type="hidden" value="1" name="delivery[]">                    
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </div>
                                    <hr>
                                    <button class="btn btn-success" id="btn-submit-submission" type="submit" name="yt0"><i class="icon-ok"></i> Submit</button>    </fieldset>
                                {{-- </form>                 --}}
                                
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        {{-- end modal update status --}}