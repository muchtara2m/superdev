 {{-- modal update tgl closing --}}
 <div class="modal fade" id="modal-tgl-closing" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Tanggal Closing</h4>
                </div>
                <div class="modal-body" style="padding-bottom:5%;">
                    <div class="form-group">
                        <label for="tglwin" class="col-sm-2 control-label">Tanggal Closing</label>
                        
                        <div class="col-sm-10">
                            <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datepick" name="tglwin" id="tglclosing" placeholder="0000-00-00">                                
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-submit-closing">Submit</button>                    
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    {{-- end modal update status --}}