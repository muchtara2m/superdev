@extends('layouts.master')

@section('title')
Revenue SAP | Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">
    /* .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    } */
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Revenue SAP
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            {{-- <li><a href="#">SPPH</a></li> --}}
            <li class="active"> Revenue SAP </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Revenue SAP</h3>
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="pbsTable" class="display">
                            <thead>
                                <tr style="white-space:nowrap">
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">UBIS</th>
                                    <th colspan="4" class="text-center">MTD REVENUE</th>
                                    <th colspan="3" class="text-center">YTD REVENUE</th>
                                    <th colspan="3" class="text-center">MTD GROSS PROFIT</th>
                                    <th colspan="3" class="text-center">YTD GROSS PROFIT</th>
                                    <th rowspan="2">TOTAL ACH %</th>
                                </tr>
                                <tr style="white-space:nowrap">
                                    <th>OUTLOOK</th>
                                    <th>TARGET</th>
                                    <th>REAL</th>
                                    <th>ACH %</th>
                                    <th>TARGET</th>
                                    <th>REAL</th>
                                    <th>ACH %</th>
                                    <th>TARGET</th>
                                    <th>By SAP</th>
                                    <th>ACH %</th>
                                    <th>TARGET</th>
                                    <th>By SAP</th>
                                    <th>ACH %</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i=1;
                                @endphp
                                @foreach ($data2 as $item => $val)
                                <tr style="white-space:nowrap">
                                        <td>{{ $val->urutan }}</td>
                                        <td>{{ $val->before_to }}</td>
                                        <td>{{ $val->ubis_outlook_count }}</td>
                                        <td>{{-- $val['ubis_outlook'][0]->total --}}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
        <!-- /.box -->
            </div>
        <!--/.col (right) -->
        </div>
    <!-- /.row -->
    </section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
        
        @endsection
        
        @section('scripts')
        <!-- DataTables -->
        <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript">
            // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
            $(document).ready( function () {
                $('#pbsTable').DataTable({
                    "aLengthMenu": [[15,30, 50, 75, -1], [15,30, 50, 75, "All"]],
                    "iDisplayLength": 15
                });
            } );
        </script>
        @endsection
        