<table id="rolesTable" class="display">
    <thead>
        <tr style="white-space:nowrap">
            <th>No</th>
            <th>IO</th>
            <th>Deskripsi IO</th>
            <th>UBIS</th>
            <th>Vendor</th>
            <th>No. PKS Vendor</th>
            <th>Tanggal Kontrak</th>
            <th>Nilai Kontrak</th>
            <th>Start Layanan</th>
            <th>End Layanan</th>
            <th>Periode Bulan</th>
            <th>TOP OTC</th>
            <th>TOP Recurring</th>
            <th>No. BAST</th>
            <th>Tanggal BAST</th>
        </tr>
    </thead>
    
</table>

@push('scripts')
<script>
    $(function() {
        $('#rolesTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: 'ifrs-vendor-json',
            
            columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},            
            { data: 'no_io'},
            { data: 'io_spk_pks_vendor.deskripsi_project','defaultContent': ""},
            { data: 'mapping_io.ubis_mapping_vendor.before_to','defaultContent': ""},
            { data: 'akun_vendor.deskripsi_vendor','defaultContent': ""},
            { data: 'nomor','defaultContent': ""},
            { data: 'tanggal','defaultContent': ""},
            { data: 'revenue', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )},            
            { data: 'start_layanan','defaultContent': ""},
            { data: 'end_layanan','defaultContent': ""},
            { data: 'periode','defaultContent': ""},
            { data: 'top_toc','defaultContent': ""},
            { data: 'top_recurring','defaultContent': ""},
            { data: 'no_io','defaultContent': ""},
            { data: 'no_io','defaultContent': ""},
            ],
            lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
            ],
            "dom": 'Blfrtip',
            "buttons": [
            'excel', 'csv'
            ],
        });
    });
</script>
@endpush
