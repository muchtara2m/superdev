<table id="permissionsTable" class="display">
    <thead>
        <tr style="white-space:nowrap;width:100%">
            <th rowspan="2">No</th>
            <th rowspan="2">IO</th>
            <th rowspan="2">Deskripsi IO</th>
            <th rowspan="2">UBIS</th>
            <th rowspan="2">Customer</th>
            <th colspan="3" style="text-align:center">SPK</th>
            <th colspan="3" style="text-align:center">KL</th>
            <th rowspan="2">Start Layanan</th>
            <th rowspan="2">End Layanan</th>
            <th rowspan="2">Periode Bulan</th>
            <th rowspan="2">TOP OTC</th>
            <th rowspan="2">TOP Recurring</th>
            <th colspan="2" style="text-align:center">BAST</th>
        </tr>
        <tr style="white-space:nowrap">
            <th style="text-align:center">No. SPK</th>
            <th style="text-align:center">Tanggal SPK</th>
            <th style="text-align:center">Nilai SPK</th>
            <th style="text-align:center">No. KL</th>
            <th style="text-align:center">Tanggal KL</th>
            <th style="text-align:center">Nilai KL</th>
            <th style="text-align:center">No. BAST</th>
            <th style="text-align:center">Tanggal BAST</th>
        </tr>
    </thead>
</table>


@push('scripts2')
<script>
    $(function() {
        $('#permissionsTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: 'ifrs-customer-json',
            columns: [
            { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},            
            { data: 'no_io','defaultContent': ""},
            { data: 'io_spk_pks_customer.deskripsi_project','defaultContent': ""},
            { data: 'mapping_io_customer.ubis_mapping_customer.before_to','defaultContent': ""},
            { data: 'customer_spk_pks.deskripsi_customer','defaultContent': ""},
            { data: 'nomor','defaultContent': ""},
            { data: 'tanggal','defaultContent': ""},
            { data: 'revenue', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )},   
            { data: 'no_io','defaultContent': ""},
            { data: 'no_io','defaultContent': ""},
            { data: 'no_io', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )},               
            { data: 'start_layanan','defaultContent': ""},
            { data: 'end_layanan','defaultContent': ""},
            { data: 'periode','defaultContent': ""},
            { data: 'top_toc','defaultContent': ""},
            { data: 'top_recurring','defaultContent': ""},
            { data: 'no_io','defaultContent': ""},
            { data: 'no_io','defaultContent': ""},
            ],
            lengthMenu: [
            [ 10, 25, 50, -1 ],
            [ '10 rows', '25 rows', '50 rows', 'Show all' ]
            ],
            "dom": 'Blfrtip',
            "buttons": [
            'excel', 'csv'
            ],
        });
    });
</script>
@endpush