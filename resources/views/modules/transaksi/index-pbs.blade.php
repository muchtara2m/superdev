@extends('layouts.master')

@section('title')
  Index PBS | Super Slim
@endsection

@section('stylesheets')
  <!-- DataTables -->
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.3/css/dataTables.responsive.css">
  <style type="text/css">
  	.form-horizontal .form-group {
  		margin-right: unset;
  		margin-left: unset;
  	}
  	tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
  </style>
@endsection

@section('content')

@php 
  $homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      TRANSAKSI
      <!-- <small>Form PBS</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
      <li><a href="#">Transaksi</a></li>
      <li class="active"> PBS </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-ticket"></i> PBS</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive">
          	<table id="pbsTable" class="table table-bordered table-hover">
          	  <thead>
          	    <tr>
          	   	  <th>Row</th>
          	   	  <th>Key Number</th>
          	   	  <th>Kode Unit</th>
          	   	  <th>Nama Unit</th>
          	   	  <th>Nomor IO</th>
          	   	  <th>Deskripsi IO</th>
          	   	  <th>Nama Customer</th>
          	   	  <th>Deskripsi</th>
          	   	  <th>Tgl Dokumen</th>
          	   	  <th>Action</th>
          	    </tr>
          	  </thead>
          	  <tbody>
          	    <tr>
                  <td>1</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                </tr>
                <tr>
                  <td>1</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                  <td>nksjnda</td>
                </tr>
          	  </tbody>
          	</table>
          </div>
          
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
  <!-- DataTables -->
  <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>
  <script type="text/javascript">
  	// document.getElementById("pbsTable_wrapper").style.overflow = "auto";
  	$(document).ready( function () {
  	    $('#pbsTable').DataTable({

  	    });
  	} );
  </script>
@endsection