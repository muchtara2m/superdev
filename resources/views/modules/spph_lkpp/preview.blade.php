@extends('layouts.master')

@section('title')
Preview SPPH
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<!-- css froala editor -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<style>
    @page {
        size            : A4;
        margin-top      : 2cm;
        margin-bottom   : 2cm;
        margin-left     : 30mm; 
        margin-right    : 20mm;
     }
      @media print {
        /* .bottomright{
            position    : fixed;
            bottom      : 0;
        } */
         body {
            margin-top      : 20mm; 
            margin-bottom   : 20mm; 
            margin-left     : 30mm; 
            margin-right    : 20mm;
            }
        }
      
    </style>
@endsection
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>SPPH<small>Preview </small></h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">SPPH</a></li>
            <li class="active"> Preview </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                
                <div class="box box-solid" style="border-radius: 5px;border-left: 4px solid#00a7d0 !important;">
                    <div class="box-header with-border">
                        <h4 class="text-center"><strong>{{ strtoupper($spphLKPP->judul) }}</strong></h4>
                    </div>
                    <div class="box-body ">
                        <div class="row">
                            <div class="col-sm-6">
                                <dl class="dl-horizontal">
                                    <dt>Nomor SPPH</dt>
                                    <dd>{{ $spphLKPP->nomorspph }}</dd>
                                    <dt>Nomor SPH</dt>
                                    <dd>{{ $spphLKPP->nomorsph == NULL ? 'Belum Diinput' : $spphLKPP->nomorsph }}</dd>
                                    <dt>Pembuat</dt>
                                    <dd>{{ $spphLKPP->pic }}</dd>
                                    <dt>Mitra</dt>
                                    <dd>{{ $spphLKPP->mitra_lkpps->perusahaan }}</dd>
                                </dl>
                            </div>

                            <div class="col-sm-6">
                                @php
                                $title_lampiran = ($spphLKPP->lampiran) !=NULL ? json_decode($spphLKPP->title_lampiran, TRUE) : NULL;
                                $lampiran       = ($spphLKPP->lampiran) !=NULL ? json_decode($spphLKPP->lampiran, TRUE) : NULL;
                                $title          = ($spphLKPP->file) !=NULL ? json_decode($spphLKPP->title, TRUE) : NULL ;
                                $file           = ($spphLKPP->file) !=NULL ? json_decode($spphLKPP->file, TRUE) : NULL;
                                @endphp
                                <div class="row">
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>Lampiran</dt>
                                            @if ($title_lampiran != NULL)
                                            @foreach ($title_lampiran as $key => $value)
                                            {{ ($key+1)}}. <a href="{{Storage::url($lampiran[$key])}}">{{$title_lampiran[$key]}}</a><br>
                                            @endforeach
                                            @endif
                                           
                                          </dl>
                                    </div>
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>File</dt>
                                            @if ($title != NULL)
                                            @foreach ($title as $key => $value)
                                            {{ ($key+1)}}. <a
                                                href="{{Storage::url($file[$key])}}">{{$title[$key]}}</a><br>
                                            @endforeach
                                            @endif
                                           
                                          </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-solid" style="border-radius: 8px">
                   
                    {{-- <div class="box-body">  --}}
                        @if ($spphLKPP->isi != NULL)
                        <textarea name="" id="" cols="30" rows="10">
                            {{ $spphLKPP->isi }}
                        </textarea>
                        @else
                        @include('modules.spph_lkpp.inc.spph_preview')
                        @endif
                        
                    {{-- </div> --}}
                </div>
            </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<!-- script function froala -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<script src="{{ asset('froala/js/languages/id.js') }}"></script>
<!-- External -->
<script type="text/javascript">
    $('textarea').froalaEditor({
        // fullPage: true,
        toolbarButtons      : ['print', 'html', 'getPDF', 'fullscreen'],
        charCounterCount    : false,
        key                 : keyFroala,
        height              : 500,
    })
    // removo alert lisensi froala
    $("div > a", ".fr-wrapper").css('display', 'none');
</script>
@endsection