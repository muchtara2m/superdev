@extends('layouts.master')


@section('title')
Edit SPPH 
@endsection
<!-- datetimepicker -->
<link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}">
<!-- select2 -->
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
<!-- datatable -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- css froala editor -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<!-- Moment JS --> 
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>


@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SPPH
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">SPPH</a></li>
            <li class="active"> Edit </li>
        </ol>
    </section>
    <section class="content">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            There was a problem, please check your form carefully.
            <ul>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">  
                    <form method="POST" action="/update-spph-lkpp/{{ $spph->id }}" enctype="multipart/form-data" class="form" id="form">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="id" value="{{ $spph->id }}">
                        <div class="box-body">     
                            <div class="form-group col-sm-4">
                                <label for="tglspph">Tanggal SPPH*</label>
                                <input  type="text" 
                                        data-date="" 
                                        data-date-format="yyyy-mm-dd" 
                                        class="form-control datejos" 
                                        autocomplete="off"
                                        name="tglspph" 
                                        id="tglspph" 
                                        placeholder="Tanggal SPPH" 
                                        value="{{old('tglspph', $spph->tglspph)}}">                                
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="nomorspph">Nomor SPPH*</label>
                                <input  type="text" 
                                        name="nomorspph" 
                                        class="form-control" 
                                        id="nomorspph" 
                                        placeholder="Nomor SPPH" 
                                        data-old="{{ old('nomorspph', $spph->nomorspph) }}"
                                        value="{{ old('nomorspph', $spph->nomorspph) }}">
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="tglsph">Tanggal SPH*</label>
                                <input  type="text" 
                                        data-date="" 
                                        data-date-format="yyyy-mm-dd" 
                                        class="form-control datepick" 
                                        name="tglsph" 
                                        id="tglsph" 
                                        placeholder="Tanggal SPH" 
                                        value="{{ old('tglsph', $spph->tglsph) }}">                                
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="kepada">Mitra*</label>
                                <input  type="hidden" 
                                        name="kepada" 
                                        id="idmitra" 
                                        value="{{ old('kepada', $spph->mitra) }}">
                                <input  type="text" 
                                        class="form-control" 
                                        placeholder="Kepada" 
                                        id="kepada" 
                                        name="perusahaan"
                                        autocomplete="off"
                                        data-toggle="modal" 
                                        data-target="#modal-unit" 
                                        value="{{ old('perusahaan', $spph->mitra_lkpps['perusahaan']) }}">
                            </div>
                            
                            <div class="form-group col-sm-4">
                                <label for="dari">Penanda Tangan*</label>
                                <select name="dari" 
                                        id="dari" 
                                        class="form-control">
                                    @foreach ($user as $item)
                                    <option value="{{ $item->name }}" {{ (old('dari', $spph->dari) == $item->name)? 'selected':'' }} data-position="{{ $item->position }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="judul">Judul*</label>
                                <input  type="text" 
                                        name="judul" 
                                        class="form-control" 
                                        placeholder="Judul" 
                                        value="{{ old('judul', $spph->judul) }}">
                            </div>
                          
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <h4 class="divider-title center" style="text-align:center">PREVIEW</h4>
                                    <hr>
                                </div>
                                <textarea name="isi" class="font">
                                        {{ old('isi', $spph->isi) }}
                                </textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">Lampiran</label>
                                <input  type="file" 
                                        name="lampiran[]" 
                                        class="form-control" 
                                        multiple="multiple" 
                                        value="{{ old('lampiran', $spph->lampiran) }}">  
                                <br>
                                @php
                                if($spph->lampiran != NULL){
                                    $title = json_decode($spph->title_lampiran, TRUE);
                                    $file = json_decode($spph->lampiran, TRUE);
                                    $i=1;
                                    foreach ($title as $key => $value) {
                                        echo $i++.'. <a
                                        href="'.Storage::url($file[$key]).'">'.$title[$key].'</a><br>';
                                    }
                                }
                                @endphp                                  
                            </div>
                            <div class="form-group col-md-12">
                                <button type="submit" 
                                        name="status" 
                                        value="draft_spph" 
                                        class="btn btn-success" 
                                        style="width: 7em;">Save
                                </button>
                                <button type="submit" 
                                        name="status" 
                                        value="save_spph" 
                                        class="btn btn-primary" 
                                        style="width: 7em;">Submit
                                </button>
                            </div>
                            
                        </div>
                        <!-- /.box-body -->
                    </form>
                    
                </div>
            </div>
            @include('modules.spph_lkpp.modal.mitra_modal')
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (right) -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- froala -->
<script src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<!-- datatable -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<!-- datetimepicker -->
<script src="{{ asset('js/jquery.datetimepicker.full.min.js') }}"></script>
<!-- select2  -->
<script src="{{ asset('js/select2.min.js') }}"></script>
<!-- script function date picker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- external script -->
<script src="{{ asset('js/web/main/js/spph_lkpp/create.js') }}"></script>
@endsection
