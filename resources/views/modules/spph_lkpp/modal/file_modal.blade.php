<div class="modal fade" id="modal-upload">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Upload File SPH</h4>
                    <small>Dokumen yang sudah ditanda tangan </small>
                </div>
                <div class="modal-body">
                    <form method="post" action="" enctype="multipart/form-data" id="file">
                        @csrf
                        <div class="form-group">
                            <input type="hidden" name="title" class="form-control">
                            <label for="">Nomor SPH</label>
                            <input type="text" name="nomorsph" id="nomorsph" class="form-control"  placeholder="Nomor SPH">
                        </div>
                        <div class="form-group">
                          <label for="">Pilih File</label>
                          <input type="file" name="file[]" id="multiplefile" class="form-control" multiple="multiple">
                          <small id="helpId" class="text-muted">*Dokumen yang sudah ditandatangan</small>
                        </div>
                        <button type="submit" class="btn btn-primary" style="width: 7em;">
                            Submit</button>
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->