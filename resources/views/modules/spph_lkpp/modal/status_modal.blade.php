<div class="modal fade" id="modal-status">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Change Status</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="" enctype="multipart/form-data" id="status">
                    @csrf
                    <h4 id="before"></h4>

                    <div class="form-group col-md">
                        <select name="status" id="" class="form-control">
                            <option value="save_spph">Save </option>
                            <option value="draft_spph">Draft</option>
                            <option value="done_spph">Done</option>
                        </select>
                    </div>
                    {{-- @method('DELETE') --}}

                    <button type="submit" class="btn btn-success" style="width: 7em;"></i>
                        Submit</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
