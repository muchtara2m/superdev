 <!-- modal -->
 <div class="modal fade" id="modal-unit">
    <div class="modal-dialog" style="width:fit-content">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Data Mitra</h4>
                </div>
                <div class="modal-body">
                    <table id="mitra" class="display table-responsive">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Perusahaan</th>
                                <th>Alamat</th>
                                <th>Direktur</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($mitras as $ios)
                            <tr>
                                <td class="id">{{ $ios['id'] }}</td>
                                <td class="nama">{{$ios['perusahaan']}}</td>
                                <td class="alamat">{{$ios['alamat']}}</td>
                                <td class="direktur">{{$ios['direktur']}}</td>
                                <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    {{--  <button type="button" class="btn btn-primary">Save changes</button>  --}}
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->