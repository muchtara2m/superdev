@extends('layouts.master')


@section('title')
Create SPPH
@endsection
<!-- datetimepicker -->
<link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- select2 -->
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- css froala editor -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<!-- Moment JS --> 
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<style>
@page {
    size            : A4;
    margin-top      : 2cm;
    margin-bottom   : 2cm;
    margin-left     : 30mm; 
    margin-right    : 20mm;
 }
  @media print {
    /* .bottomright{
        position    : fixed;
        bottom      : 0;
    } */
     body {
        margin-top      : 20mm; 
        margin-bottom   : 20mm; 
        margin-left     : 30mm; 
        margin-right    : 20mm;
        }
    }
</style>

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           SPPH
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">SPPH</a></li>
            <li class="active"> Create </li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form method="POST" action="{{ url('store-spph-lkpp') }}" enctype="multipart/form-data" class="form" id="form">
                        @csrf
                        <div class="box-body">      
                            <div class="form-group col-sm-4">
                                <label for="tglspph">Tanggal SPPH*</label>
                                <input  type="text" 
                                        data-date="" 
                                        data-date-format="yyyy-mm-dd" 
                                        class="form-control datejos" 
                                        autocomplete="off"
                                        name="tglspph" 
                                        id="tglspph" 
                                        placeholder="Tanggal SPPH" 
                                        value="{{ old('tglspph') }}">
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="nomorspph">Nomor SPPH*</label>
                                <input  type="text" 
                                        name="nomorspph" 
                                        class="form-control @error('nomorspph') is-invalid @enderror" 
                                        id="nomorspph" 
                                        placeholder="Nomor SPPH" 
                                        data-old="{{ old('nomorspph') }}"
                                        value="{{ old('nomorspph') }}" >
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="tglsph">Tanggal SPH*</label>
                                <input  type="text" 
                                        data-date="" 
                                        data-date-format="yyyy-mm-dd" 
                                        class="form-control datepick @error('tglsph') is-invalid @enderror" 
                                        name="tglsph" 
                                        id="tglsph" 
                                        autocomplete="off"
                                        placeholder="Tanggal SPH" 
                                        value="{{ old('tglsph') }}">
                            </div>

                            <div class="form-group col-sm-4">
                                <label for="kepada">Mitra*</label>
                                <input  type="hidden" 
                                        name="kepada" 
                                        id="idmitra"
                                        value="{{ old('kepada') }}">
                                <input  type="text" 
                                        class="form-control" 
                                        name="perusahaan" 
                                        placeholder="Aybis.corp" 
                                        autocomplete="off"
                                        id="kepada" 
                                        data-toggle="modal" 
                                        data-target="#modal-unit" 
                                        value="{{ old('perusahaan') }}">
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="dari">Penanda Tangan*</label>
                                <select name="dari" 
                                        id="dari" 
                                        class="form-control">
                                            <option selected disabled>Pilih Penanda Tangan</option>
                                            @foreach ($user as $item)
                                            <option value="{{ $item->name }}" 
                                                    {{ (old('dari') == $item->name)? 'selected':'' }} 
                                                    data-position="{{ $item->position }}">
                                                    {{ $item->name }}</option>
                                            @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="judul">Judul*</label>
                                <input  type="text" 
                                        name="judul" 
                                        id="judul"
                                        class="form-control" 
                                        placeholder="Pengadaan Laptop Razer Blade Advanced" 
                                        value="{{ old('judul') }}">
                            </div>
                           
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <h4 class="divider-title center" style="text-align:center">PREVIEW</h4>
                                    <hr>
                                </div>
                                <textarea name="isi" class="font">
                                        @include('modules.spph_lkpp.inc.create')
                                </textarea>
                            </div>
                            <div class="form-group col-md-12">
                                    <label for="">Lampiran</label>
                                    <input  type="file" 
                                            name="lampiran[]" 
                                            class="form-control" 
                                            multiple="multiple">                                    
                                </div>
                            <div class="form-group col-md-12">
                                <button type="submit" 
                                        name="status" 
                                        value="draft_spph" 
                                        class="btn btn-success" 
                                        style="width: 7em;">Save
                                </button>
                                <button type="submit" 
                                        name="status" 
                                        value="save_spph" 
                                        class="btn btn-primary" 
                                        style="width: 7em;">Submit
                                </button>
                            </div>
                            
                        </div>
                        <!-- /.box-body -->
                    </form>
                    
                </div>
            </div>
            @include('modules.spph_lkpp.modal.mitra_modal')
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (right) -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- froala -->
<script src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<!-- datetimepicker -->
<script src="{{ asset('js/jquery.datetimepicker.full.min.js') }}"></script>
<!-- select2  -->
<script src="{{ asset('js/select2.min.js') }}"></script>
<!-- date picker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/web/main/js/spph_lkpp/create.js') }}"></script>
@endsection
