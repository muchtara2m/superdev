@extends('layouts.master')

@section('title')
Done SPPH
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<!-- Moment -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SPPH <small>Done</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">SPPH</a></li>
            <li class="active"> Done </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title"> Done </h3>
                        <div class="pull-right box-tools">
                            <a href="/export-spph-lkpp" class="btn btn-info"><i class="fa fa-download"></i> </a>
                        </div>
                    </div>
                    <div style="text-align:center">
                        <div>
                            <select name="bulan" id="bulan" class="form-group" style="width:10%;height:34px">
                            </select>
                            <select name="tahun" id="tahun" class="form-group" style="width:10%;height:34px">
                            </select>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="done" class="display" data-url="/lkpp/spph/done/">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nomor SPPH</th>
                                    <th>Tanggal SPPH</th>
                                    <th>Nomor SPH</th>
                                    <th>Tanggal SPH</th>
                                    <th>Mitra</th>
                                    <th>Judul</th>
                                    <th>Tanggal Buat</th>
                                    <th>Status Dokumen</th>
                                    <th style="text-align:center"></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    @include('modules.spph_lkpp.modal.file_modal')
                    @include('modules.spph_lkpp.modal.lampiran_modal')
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/sweetalert/js/sweetalert2.all.min.js')}}"></script>
<script src="{{ asset('js/web/main/js/spph_lkpp/done.js')}}"></script>
@endsection