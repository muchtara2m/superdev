@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "SP3";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet"
href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
{{--  Tagify  --}}
<link rel="stylesheet" href="{{ asset('tagify/dist/tagify.css') }}">
@endsection
<style>
    .class1 tbody tr:nth-child(2n) {
        background: #f9f9f9;
    }
    
    .class3 tbody tr td {
        border-color: black;
        border-style: solid;
        border-width: unset;
    }
    
    .class4 tbody tr td {
        border: 1px solid black;
    }
    
    .class2 thead tr th,
    .class2 tbody tr td {
        border-style: dashed;
    }
    
    .class5 tr {
        border: 1px solid black;
    }
    
    /* ol {
        list-style: none;
        counter-reset: my-awesome-counter;
    }
    
    ol li {
        counter-increment: my-awesome-counter;
    }
    
    ol li::before {
        content: counter(my-awesome-counter) ") ";
        color: red;
        font-weight: bold;
    } */
    
</style>

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SP3
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">SP3</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Create SP3</h3>
                    </div>
                    <!-- /.box-header -->
                    <br>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    
                    {{-- start form  --}}
                    <form method="post" action="{{ url('store-SP3-lkpp') }}" enctype="multipart/form-data" id="form">
                        
                        @csrf
                        <div class="box-body">
                            <div class="form-group col-md-12 igroup">
                                <label for="nomorspph">Select SPPH</label><a id="spphpreview" target="_blank"
                                href="#"><i class="fa fa-fw fa-file-code-o"></i></a>
                                <input type="text" name="nmrspph" class="form-control" data-toggle="modal"
                                data-target="#modal-unit-spph" placeholder="Pilih SPPH" id="nmrspph"
                                value="{{ old('nmrspph') }}">
                            </div>
                            <input type="hidden" id="spphid" name="spphid" value="{{ old('spphid') }}">
                            
                            <div class="form-group col-md-4 igroup">
                                <label for="tglSP3n">Tanggal SP3</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" data-date="" data-date-format="yyyy-mm-dd"
                                    class="form-control datejos" name="tanggalsp3" id="tanggalsp3"
                                    value="{{ old('tanggalsp3') }}">
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="harga">Harga</label>
                                <div class="input-group col-md-12">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control" onkeyup="formatAngka(this,'.')" name="harga"
                                    placeholder="1000000" id="harganum" value="{{ old('harga') }}">
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="harga">No. SP3</label>
                                <input type="text" name="nomorsp3" id="nomorsp3" class="form-control"
                                value="{{ old('nomorsp3') }}">
                            </div>
                            
                            {{-- hasil pembahasan --}}
                            <div class="col-md-12" style="margin-top:5%">
                                <div class="form-group">
                                    <h4 class="divider-title center" style="text-align:center">HASIL PEMBAHASAN</h4>
                                    <hr>
                                </div>
                                <div class="form-group">
                                    <textarea rows="10" cols="80" name="isi" placeholder="Type Something" class="isi">
                                        @include('modules.sp3_lkpp.modal.sp3_preview')       
                                    </textarea>
                                </div>
                                <div id="isinya_sementara"> 
                                    
                                </div>
                                <div class="form-group">
                                    <label for="">File Lampiran</label>
                                    <input type="file" name="lampiran[]" class="form-control" multiple="multiple">
                                </div>
                                <div class="form-group">
                                    <label for="">Chat</label><span>*require for submit </span>
                                    <input type="text" name="chat" class="form-control">
                                </div>
                            </div>
                            {{-- end hasil pembahasan --}}
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" name="status" value="draft_SP3" class="btn btn-primary"
                            style="width: 7em;"><i class="fa fa-check"></i> Save</button>
                            <button type="submit" name="status" value="save_SP3" class="btn btn-success"
                            style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    {{-- end form --}}
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
            @include('modules.sp3_lkpp.modal.bakn_modal')
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<!-- Data Table -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- clockpicker -->
<script src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Tagify -->
<script src="{{ asset('tagify/dist/jQuery.tagify.min.js') }}"></script>
<script src="{{ asset('tagify/dist/tagify.min.js') }}"></script>
<script src="{{ asset('js/sp3.js') }}"></script>
<script>
    // froala
    $('.isi').froalaEditor({
        placeholderText: '',
        charCounterCount: false,
        key: '{{ env("KEY_FROALA") }}',
        tableStyles: {
            class1: 'Class 1',
            class2: 'Class 2',
            class3: 'Class 3',
            class4: 'Class 4',
            class5: 'Class 5',
        },
        fontFamily: {
            "Roboto,sans-serif": 'Roboto',
            "Oswald,sans-serif": 'Oswald',
            "Montserrat,sans-serif": 'Montserrat',
            "'Open Sans Condensed',sans-serif": 'Open Sans Condensed',
            "Trebuchet MS": "Trebuchet MS",
            "Helvetica": "helvetica,sans-serif",
            "Arial": "arial,sans-serif",
            "Times New Roman": "times new roman,serif",
            "Comic Sans MS": "comic sans ms,sans-serif",
            "Courier New": "courier new,monospace",
            "Garamond": "garamond,serif",
            "Georgia": "georgia,serif",
            "Lucida Sans Unicode": "lucida sans unicode,serif",
            "Tahoma": "tahoma,sans-serif",
            "Trebuchet MS": "trebuchet ms,sans-serif",
            "Verdana": "verdana,sans-serif"
        },
        fontFamilySelection: true
    });
    
    // script datepicker
    $(function () {
        $(".datejos").on("change", function () {
            this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
            .format(this.getAttribute("data-date-format"))
            )
        }).trigger("change")
    });
    $('.datejos').datepicker({
        autoclose: true,
        orientation: "bottom"
    });
    // function table spph
    $(document).ready(function () {
        $('#bakn').DataTable();
    })
    var tablespph = document.getElementById('bakn');
    for (var i = 1; i < tablespph.rows.length; i++) {
        tablespph.rows[i].onclick = function () {
            document.getElementById("spphid").value = this.cells[0].innerHTML;
            $('#nmrspph').val(this.cells[1].innerHTML);
            
        };
    };
    // set default no_bak dan tgl_bak
    let get_tgl_bak = document.getElementById('tanggalsp3');
    
    function useValue() {
        let NameValue = get_tgl_bak.value.split('-');
        // use it
        document.getElementById('nomorsp3').value = "/HK.810/ECOM/PIN.00.00/" + NameValue[0];
    }
    get_tgl_bak.onchange = useValue;
    get_tgl_bak.onblur = useValue;
    
    // format currency harga
    function formatAngka(objek, separator) {
        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;
        j = 0;
        for (i = panjang; i > 0; i--) {
            j = j + 1;
            if (((j % 3) == 1) && (j != 1)) {
                c = b.substr(i - 1, 1) + separator + c;
            } else {
                c = b.substr(i - 1, 1) + c;
                formatAngka
            }
        }
        objek.value = c;
    }
    
    function clearDot(number) {
        output = number.replace(".", "");
        return output;
    }
    
    $(document).ready(function () {
        $(document).change(function () {
            var id = $('#spphid').val();
            var token = $("input[name='_token']").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: "{{ route('data-bakn') }}",
                data: {
                    id: id,
                    _token: token
                },
                dataType: 'json', //return data will be json
                async: "false",
                success: function (data) {
                    var nmr = data.options['spph_lkpps']['nomorspph'];
                    var tglSP3n = new Date(data.options['tglSP3n']);
                    var thn = tglSP3n.getFullYear();
                    // judul ruang lingkup
                    $('#judul_spph').empty();
                    $('#judul_spph').html(`
                    <p id="judul_spph" style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <span style="font-size:13px;line-height:115%;">${data.options['spph_lkpps']['judul']}
                    </p>
                    `);
                    //harga
                    $('#harga_sp3').empty();
                    $('#harga_sp3').html(` 
                    <p id="harga_sp3"
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:justify;'>
                    <span style="font-size:13px;line-height:115%;">${data.options['harga_terbilang']}</span></p>
                    `);
                    //harga
                    $('#cara_bayar_sp3').empty();
                    $('#cara_bayar_sp3').html(` 
                    <p id="cara_bayar_sp3"
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:justify;'>
                    <span style="font-size:13px;line-height:115%;">${data.options['cara_bayar']}</span></p>
                    `);
                    // syarat ketentuan
                    $('#syarat_ketentuan_sp3').empty();
                    $('#syarat_ketentuan_sp3').html(`
                    <p id="syarat_ketentuan_sp3"
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:justify;'>
                    <span style="font-size:13px;line-height:115%;">${data.options['lain_lain']}</span></p>
                    `);
                    
                    
                },
                error: function () {
                    
                }
            });
            
            
        });
    });
    // change nomor bak
    $('#nomorsp3').change(function () {
        var nomor = $('#nomorsp3').val();
        let tgl_sp3 = $('#tanggalsp3').val();
        let split_tgl = tgl_sp3.split('-');
        const date_bak = new Date(tgl_sp3);
        const month_bak = date_bak.toLocaleString('id-ID', {
            month: 'long'
        });
        $('#tanggal_sp3').html(` ${split_tgl[2]} ${month_bak.toUpperCase()} ${split_tgl[0]}`);
        $('#nomor_sp3').html(nomor);
      
    });
</script>
@endsection
