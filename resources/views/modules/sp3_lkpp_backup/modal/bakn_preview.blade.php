<table border="1" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
            <tr>
                <td rowspan="4" style="width: 117pt;border: 1pt solid windowtext;padding: 0cm 5.4pt;height: 71.5pt;vertical-align: top;" valign="top" width="24.761904761904763%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;margin-left:-5.4pt;'>
                        <br>
                    </p>
                </td>
                <td colspan="3" style="width: 355.5pt;border-top: 1pt solid windowtext;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-image: initial;border-left: none;padding: 0cm 5.4pt;height: 71.5pt;vertical-align: top;" valign="top" width="75.23809523809524%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">BERITA ACARA KLARIFIKASI &amp; NEGOSIASI</span></strong></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">PENGADAAN KOMPUTER UNTUK BAGIAN SARANA DAN PRASARANA KABUPATEN TANJUNG JABUNG BARAT JAMBI MELALUI LKPP&nbsp;</span></strong></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">ANTARA &nbsp;</span></strong></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">PT. PINS INDONESIA</span></strong></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">&nbsp;DENGAN&nbsp;</span></strong></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">PT.&nbsp;</span></strong><strong><span style="font-size:15px;">DUTA TEKNOLOGI NUSANTARA</span></strong></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
                </td>
            </tr>
            <tr>
                <td style="width: 148.5pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="41.77215189873418%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">Tanggal</span></p>
                </td>
                <td colspan="2" style="width: 207pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="58.22784810126582%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">11 Oktober 2019</span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 148.5pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0cm 5.4pt;height: 3.2pt;vertical-align: top;" valign="top" width="41.77215189873418%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">Waktu</span></p>
                </td>
                <td colspan="2" style="width: 207pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0cm 5.4pt;height: 3.2pt;vertical-align: top;" valign="top" width="58.22784810126582%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">16:30&nbsp;</span><span style="font-size:15px;">WIB sampai dengan&nbsp;</span><span style="font-size:15px;">Selesai</span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 148.5pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0cm 5.4pt;height: 3.2pt;vertical-align: top;" valign="top" width="41.77215189873418%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">Tempat</span></p>
                </td>
                <td colspan="2" style="width: 207pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0cm 5.4pt;height: 3.2pt;vertical-align: top;" valign="top" width="58.22784810126582%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">Kantor PT. PINS Indonesia</span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 117pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="24.761904761904763%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">Undangan dari</span></p>
                </td>
                <td colspan="3" style="width: 355.5pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="75.23809523809524%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">PT. PINS Indonesia</span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 117pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;padding: 0cm 5.4pt;height: 3.2pt;vertical-align: top;" valign="top" width="24.761904761904763%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">Tipe Rapat</span></p>
                </td>
                <td colspan="3" style="width: 355.5pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0cm 5.4pt;height: 3.2pt;vertical-align: top;" valign="top" width="75.23809523809524%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><strong><span style="font-size:15px;border:solid windowtext 1.0pt;padding:0cm;">V</span></strong><strong><span style="font-size:15px;">&nbsp;</span></strong><span style="font-size:15px;">Review &nbsp;<strong><span style="border:solid windowtext 1.0pt;padding:0cm;">V</span></strong> <strong>Coordination</strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Briefing &nbsp; <strong><span style="border:solid windowtext 1.0pt;padding:0cm;">V</span></strong> <strong>Decision Making</strong>&nbsp; &nbsp;</span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 117pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;padding: 0cm 5.4pt;height: 3.2pt;vertical-align: top;" valign="top" width="24.761904761904763%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">Pimpinan Rapat</span></p>
                </td>
                <td colspan="3" style="width: 355.5pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0cm 5.4pt;height: 3.2pt;vertical-align: top;" valign="top" width="75.23809523809524%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">GM e-Commerce</span></p>
                </td>
            </tr>
            <tr>
                <td style="width: 117pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;padding: 0cm 5.4pt;height: 24.7pt;vertical-align: top;" valign="top" width="24.761904761904763%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">Peserta</span></p>
                </td>
                <td colspan="2" style="width: 189pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0cm 5.4pt;height: 24.7pt;vertical-align: top;" valign="top" width="40%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">Supplier&nbsp;</span><span style="font-size:15px;">:</span></p>
    
                    <ol style="margin-bottom:0cm;list-style-type: decimal;">
                        <li style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'>Yan Sen</li>
                    </ol>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;margin-left:15.85pt;'><span style="font-size:15px;">&nbsp;</span></p>
                </td>
                <td style="width: 166.5pt;border-top: none;border-left: none;border-bottom: 1pt solid windowtext;border-right: 1pt solid windowtext;padding: 0cm 5.4pt;height: 24.7pt;vertical-align: top;" valign="top" width="35.23809523809524%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">PT. PINS Indonesia :</span></p>
    
                    <ol style="margin-bottom:0cm;list-style-type: decimal;">
                        <li style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">Hernadi Yoga Adhitya Tama</span></li>
                        <li style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">Lucas Zylgwyn</span></li>
                        <li style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'>Wahyudi Tito</li>
                    </ol>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="width: 472.5pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(160, 160, 160);padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="100%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;color:black;">AGENDA</span></strong></p>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="width: 472.5pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;padding: 0cm 5.4pt;height: 11.2pt;vertical-align: top;" valign="top" width="100%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:justify;'><span style="font-size:15px;">Klarifikasi &amp; Negosiasi&nbsp;</span><span style="font-size:15px;">Pengadaan Pengadaan Komputer untuk Bagian Sarana dan Prasarana Kabupaten Tanjung Jabung Barat jambi melalui LKPP.</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:justify;'><span style="font-size:15px;">&nbsp;</span></p>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="width: 472.5pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(166, 166, 166);padding: 0cm 5.4pt;height: 3.5pt;vertical-align: top;" valign="top" width="100%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;color:black;">DASAR KLARIFIKASI</span></strong></p>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="width: 472.5pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="100%">
    
                    <ol style="margin-bottom:0cm;list-style-type: decimal;">
                        <li style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">Surat dari PT. PINS Indonesia No. 4035/LG.220/ECOM/PIN.00.00/2019 tanggal 10 Oktober 2019 perihal permintaan harga.</span></li>
                        <li style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'>Surat dari Supplier tanggal 11 Oktober 2019 perihal penawaran harga.</li>
                    </ol>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;margin-left:12.6pt;text-align:justify;'><span style="font-size:15px;">&nbsp;</span></p>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="width: 472.5pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;background: rgb(160, 160, 160);padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="100%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;color:black;">NEGOSIASI</span></strong></p>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="width: 472.5pt;border-right: 1pt solid windowtext;border-bottom: 1pt solid windowtext;border-left: 1pt solid windowtext;border-image: initial;border-top: none;padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="100%">
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:justify;'><span style="font-size:15px;">PT. PINS Indonesia dan&nbsp;</span><span style="font-size:15px;">Supplier&nbsp;</span><span style="font-size:15px;">melakukan klarifikasi dan negosiasi atas</span><span style="font-size:15px;">&nbsp;</span><span style="font-size:15px;">kesepakatan sebagai berikut:</span></p>
    
                    <ol>
                        <li style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'><span style="font-size:15px;">&nbsp;</span><strong>Ruang Lingkup</strong> :
                            <br><span style="font-size:15px;">Pengadaan Komputer untuk Bagian Sarana dan Prasarana Kabupaten Tanjung Jabung Barat jambi melalui LKPP</span><span style="font-size:15px;">.</span></li>
                        <li style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'><strong>Lokasi Pekerjaan/Pengiriman Barang/Jasa</strong> :
                            <br><span style="font-size:15px;">PT. PINS Indonesia Area Jambi, Plasa Telkom Indonesia Jl. Prof. Dr. Sumantri Brojonegoro No. 54, Sungai Putri, Telanaipura, Jambi.</span></li>
                        <li style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'><strong>Jangka Waktu&nbsp;</strong><strong><span style="font-size:15px;">Pelaksanaan</span></strong><strong>&nbsp;:&nbsp;</strong>
                            <br><span style="font-size:15px;">Jangka waktu pengiriman barang selambat-lambatnya sampai dengan tanggal 10 November 2019.</span></li>
                        <li style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'><strong>Harga:</strong>
                            <br><span style="font-size:15px;">Total harga adalah sebesar <strong>Rp. 18.788.625,- (Delapan Belas Juta Tujuh Ratus Delapan Puluh Delapan Ribu Enam Ratus Dua Puluh Lima Rupiah)&nbsp;</strong>telah termasuk PPN 10% (sepuluh persen), dengan rincian seperti pada tabel berikut :</span></li>
                        <li style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'><strong>Tatacara Pembayaran :</strong>
    
                            <ol style="list-style-type: lower-alpha;">
                                <li style='margin: 0cm 0cm 0.0001pt 20px; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'><span style="font-size:15px;">PT. PINS Indonesia akan melaksanakan pembayaran secara sekaligus 100% (seratus persen) atau sebesar</span><strong>&nbsp;</strong><strong>Rp. 18.788.625,- (Delapan Belas Juta Tujuh Ratus Delapan Puluh Delapan Ribu Enam Ratus Dua Puluh Lima Rupiah)&nbsp;</strong><span style="font-size:15px;">sudah termasuk PPN 10%</span>.</li>
                                <li style='margin: 0cm 0cm 0.0001pt 20px; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'>Dokumen tagihan diserahkan kepada Unit Keuangan PT. PINS Indonesia yang dilampiri dengan dokumen-dokumen sebagai berikut :
    
                                    <ol style="list-style-type: lower-alpha;">
                                        <li style='margin: 0cm 0cm 0.0001pt 40px; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'>Lembar asli Berita Acara Uji Terima (BAUT) yang ditandatangani oleh PT. PINS Indonesia
                                            <br>(c.q MGR Catalogue &amp; Partnership Management) dan Supplier;</li>
                                        <li style='margin: 0cm 0cm 0.0001pt 40px; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'>Lembar Asli Berita Acara Serah Terima Barang (BASTB) yang ditandatangani oleh
                                            <br>PT. PINS Indonesia (c.q GM e-Commerce) dan Supplier;</li>
                                        <li style='margin: 0cm 0cm 0.0001pt 40px; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'>Surat permohonan bayar;</li>
                                        <li style='margin: 0cm 0cm 0.0001pt 40px; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'>Invoice;</li>
                                        <li style='margin: 0cm 0cm 0.0001pt 40px; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'>Kuitansi bermaterai;</li>
                                        <li style='margin: 0cm 0cm 0.0001pt 40px; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'>Faktur Pajak (lembar pertama asli dan copy) dengan melampirkan Surat Pemberian Nomor Seri Faktur Pajak dari Dirjen Pajak terkait;&nbsp;</li>
                                        <li style='margin: 0cm 0cm 0.0001pt 40px; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'>Copy PKS terkait.</li>
                                    </ol>
                                </li>
                                <li>yang telah dinyatakan lengkap akurat benar dan sah oleh Unit Keuangan PT. PINS Indonesia. Dokumen yang belum lengkap akan dikembalikan terlebih dahulu dan akan diproses setelah seluruh dokumen pembayaran tersebut di atas dilengkapi.</li>
                            </ol>
                        </li>
                        <li style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'><strong>Ketentuan dan Syarat Tambahan</strong>
                            <br><span style="font-size:15px;">PT. PINS Indonesia akan menerbitkan Surat Penetapan Pelaksanaan Pekerjaan (SPPP) untuk ditandatangani oleh Supplier sekaligus sebagai dokumen yang menyatakan bahwa Supplier bersedia dan sanggup:</span>
    
                            <ol style="list-style-type: lower-alpha;">
                                <li style='margin: 0cm 0cm 0.0001pt 20px; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'><span style="font-size:15px;">Menyerahkan dan/atau menyelesaikan barang dan/atau hasil pekerjaan tepat waktu, tepat jumlah, sesuai dengan Spesifikasi Teknis yang telah ditetapkan oleh PT. PINS Indonesia;</span></li>
                                <li style='margin: 0cm 0cm 0.0001pt 20px; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'><span style="font-size:15px;">Barang dan/atau hasil pekerjaan yang diserahkan Supplier tidak bertentangan dengan dan/atau melanggar hukum dan/atau melanggar Hak milik pihak lain;</span></li>
                                <li style='margin: 0cm 0cm 0.0001pt 20px; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'><span style="font-size:15px;">Melampirkan lampiran serial number perangkat dalam bentuk hardcopy dan softcopy;</span></li>
                                <li style='margin: 0cm 0cm 0.0001pt 20px; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'><span style="font-size:15px;">Melampirkan bukti foto serah terima barang di lokasi;</span></li>
                                <li style='margin: 0cm 0cm 0.0001pt 20px; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'><span style="font-size:15px;">Memberikan garansi perangkat selama 1 tahun yang dilengkapi dengan kartu garansi;</span></li>
                                <li style='margin: 0cm 0cm 0.0001pt 20px; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'><span style="font-size:15px;">Bersedia dikenakan Sanksi Denda Keterlambatan sebesar 1&permil; (satu per mil) per hari keterlambatan dikalikan nilai barang dan/atau hasil pekerjaan yang belum diterima, dengan maksimal denda sebesar 5% (lima persen). Apabila jumlah sanksi denda keterlambatan telah mencapai 5% (lima persen) dari nilai barang dan/atau hasil pekerjaan berdasarkan PKS.</span></li>
                            </ol>
                        </li>
                    </ol>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">&nbsp;</span></p>
    
                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:justify;'><span style="font-size:15px;">Demikian Berita Acara Klarifikasi dan Negosiasi ini dibuat.</span></p>
                    <div style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span style="font-size:15px;">&nbsp;</span></div>
    
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse;" width="671">
                        <tbody>
                            <tr>
                                <td style="width: 219.6pt;border-top: none;border-bottom: none;border-left: none;border-image: initial;border-right: 1pt dotted windowtext;padding: 0cm 5.4pt;height: 22.5pt;vertical-align: top;" valign="top" width="43.666169895678095%">
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">PT. PINS Indonesia,</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">Wahyudi Tito</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">Lucas Zylgwyn</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">Hernadi Yoga Adhitya Tama</span></strong></p>
                                </td>
                                <td style="width: 10cm;border: none;padding: 0cm 5.4pt;height: 22.5pt;vertical-align: top;" valign="top" width="56.333830104321905%">
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><strong><span style="font-size:15px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;PT. Duta Teknologi Nusantara,</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    
                                    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;margin-left:15.65pt;'><strong><span style="font-size:15px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Yan Sen</span></strong></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="border:none;" width="33.43065693430657%">
                    <br>
                </td>
                <td style="border:none;" width="27.883211678832115%">
                    <br>
                </td>
                <td style="border:none;" width="5.839416058394161%">
                    <br>
                </td>
                <td style="border:none;" width="32.846715328467155%">
                    <br>
                </td>
            </tr>
        </tbody>
    </table>
    
    <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><strong><span style="font-size:15px;">&nbsp;</span></strong></p>
    