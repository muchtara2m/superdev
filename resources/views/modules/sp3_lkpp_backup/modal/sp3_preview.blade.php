<table border="1" cellpadding="0" cellspacing="0" style="border-collapse:collapse;width:auto">
    <tbody>
        <tr>
            <td colspan="3"
                style="width:86.85pt;border-top:silver;border-left:  white;border-bottom:silver;border-right:white;border-style:solid;border-width:  1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt;"
                width="17.261904761904763%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <span style="font-size:13px;line-height:115%;">&nbsp;</span></p>
            </td>
            <td style="width:29.65pt;border-top:solid silver 1.0pt;border-left:  none;border-bottom:solid silver 1.0pt;border-right:solid white 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt;"
                width="5.9523809523809526%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <span style="font-size:13px;line-height:115%;">&nbsp;</span></p>
            </td>
            <td colspan="3"
                style="width:186.9pt;border-top:solid silver 1.0pt;border-left:none;border-bottom:solid silver 1.0pt;border-right:solid white 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:3.85pt;"
                width="37.05357142857143%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <span style="font-size:13px;line-height:115%;">&nbsp;</span></p>
            </td>
            <td style="width: 200.6pt;border-top: 1pt solid white;border-right: 1pt solid white;border-bottom: 1pt solid white;border-image: initial;border-left: none;padding: 0cm 5.4pt;height: 3.85pt;vertical-align: top;"
                valign="top" width="39.732142857142854%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <span style="font-size:13px;line-height:115%;">&nbsp;</span></p>
            </td>
        </tr>
        <tr>
            <td colspan="3"
                style="width: 86.85pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 8.75pt;vertical-align: bottom;"
                valign="bottom" width="17.261904761904763%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <span style="font-size:13px;line-height:115%;">Ref. No.</span></p>
            </td>
            <td colspan="4"
                style="width: 216.55pt;border-top: none;border-left: none;border-bottom: 1pt solid silver;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 8.75pt;vertical-align: bottom;"
                valign="bottom" width="43.00595238095238%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <span style="font-size:13px;line-height:115%;" id="nomor_sp3">XXXXXXXXXXXXXXXXX</p>
            </td>
            <td rowspan="2"
                style="width: 200.6pt;border-top: none;border-left: none;border-bottom: 1pt solid white;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 8.75pt;vertical-align: top;"
                valign="top" width="39.732142857142854%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <span style="font-size:13px;line-height:115%;">&nbsp;</span></p>
            </td>
        </tr>
        <tr>
            <td colspan="3"
                style="width: 86.85pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 12.8pt;vertical-align: bottom;"
                valign="bottom" width="28.641975308641975%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <span style="font-size:13px;line-height:115%;">Tanggal (<em>date</em>)</span></p>
            </td>
            <td colspan="4"
                style="width: 216.55pt;border-top: none;border-left: none;border-bottom: 1pt solid silver;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 12.8pt;vertical-align: bottom;"
                valign="bottom" width="71.35802469135803%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <span style="font-size:13px;line-height:115%;">:&nbsp;</span><span
                        style="font-size:13px;line-height:115%;" id="tanggal_sp3">4 Oktober 2019&nbsp;</span></p>
            </td>
        </tr>
        <tr>
            <td colspan="8"
                style="width:504.0pt;border-top:none;border-left:  solid white 1.0pt;border-bottom:solid silver 1.0pt;border-right:solid white 1.0pt;background:#F3F3F3;padding:0cm 5.4pt 0cm 5.4pt;height:12.75pt;"
                width="100%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:center;'>
                    <strong><span style="font-size:13px;line-height:115%;color:black;">SURAT
                            P</span></strong><strong><span style="font-size:13px;line-height:115%;color:black;">ENETAPAN
                            PELAKSANAAN PEKERJAAN (SPPP)</span></strong></p>
            </td>
        </tr>
        <tr>
            <td colspan="5"
                style="width:229.5pt;border-top:none;border-left:  solid white 1.0pt;border-bottom:solid silver 1.0pt;border-right:solid white 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:3.9pt;"
                width="45.535714285714285%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <em><span style="font-size:13px;line-height:115%;">Referensi:&nbsp;</span></em></p>
            </td>
            <td colspan="3"
                style="width:274.5pt;border-top:none;border-left:  none;border-bottom:solid silver 1.0pt;border-right:solid white 1.0pt;background:#F3F3F3;padding:0cm 5.4pt 0cm 5.4pt;height:3.9pt;"
                width="54.464285714285715%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;margin-right:0cm;margin-left:1.65pt;'>
                    <strong><span style="font-size:13px;line-height:115%;color:black;">Kepada&nbsp;</span></strong><span
                        style="font-size:13px;line-height:115%;color:black;">(<em>to)</em><strong>:</strong></span></p>
            </td>
        </tr>
        <tr>
            <td colspan="5"
                style="width:229.5pt;border-top:none;border-left:  solid white 1.0pt;border-bottom:solid silver 1.0pt;border-right:solid white 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:55.6pt;"
                width="45.535714285714285%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:justify;'>
                    <span style="font-size:13px;line-height:115%;">Berita Acara Klarifikasi &amp;
                        Negosiasi&nbsp;</span><span style="font-size:13px;line-height:115%;" id="judul_spph">Ini Judul dari SPPH</p>
            </td>
            <td colspan="3"
                style="width: 274.5pt;border-top: none;border-left: none;border-bottom: 1pt solid silver;border-right: 1pt solid white;background: rgb(243, 243, 243);padding: 0cm 5.4pt;height: 55.6pt;vertical-align: top;"
                valign="top" width="54.464285714285715%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <strong><span style="font-size:13px;line-height:115%;color:black;" id="direktur_mitra">Ini Direktur Mitranya</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <strong><span style="font-size:13px;line-height:115%;color:black;">Alamat Perusahaan Mitra</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <strong><span style="font-size:13px;line-height:115%;color:black;">Telp : &nbsp;<a
                                href="https://www.google.com/search?q=pt+sistech+kharisma&oq=pt+siste&aqs=chrome.0.0j69i57j0l4.2707j0j4&sourceid=chrome&ie=UTF-8"
                                title="Telepon telepon via Hangouts">(021) 3505668</a></span></strong></p>
            </td>
        </tr>
        <tr>
            <td style="width: 22.5pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 9.25pt;vertical-align: bottom;"
                valign="bottom" width="4.464285714285714%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <span style="font-size:13px;line-height:115%;">&nbsp;</span></p>
            </td>
            <td colspan="7"
                style="width: 481.5pt;border-top: none;border-left: none;border-bottom: 1pt solid silver;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 9.25pt;vertical-align: bottom;"
                valign="bottom" width="95.53571428571429%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <strong><span style="font-size:13px;line-height:115%;">&nbsp;</span></strong></p>
            </td>
        </tr>
        <tr>
            <td style="width: 22.5pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 12pt;vertical-align: bottom;"
                valign="bottom" width="4.464285714285714%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <span style="font-size:13px;line-height:115%;">1.</span></p>
            </td>
            <td colspan="7"
                style="width: 481.5pt;border-top: none;border-left: none;border-bottom: 1pt solid silver;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 12pt;vertical-align: bottom;"
                valign="bottom" width="95.53571428571429%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <strong><span style="font-size:13px;line-height:115%;">Ruang Lingkup
                            Pekerjaan</span></strong><strong><span
                            style="font-size:13px;line-height:115%;">&nbsp;:</span></strong></p>
            </td>
        </tr>
        <tr>
            <td style="width: 22.5pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 13.8pt;vertical-align: bottom;"
                valign="bottom" width="4.464285714285714%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <span style="font-size:13px;line-height:115%;">&nbsp;</span></p>
            </td>
            <td colspan="7"
                style="width: 481.5pt;border-top: none;border-left: none;border-bottom: 1pt solid silver;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 13.8pt;vertical-align: top;"
                valign="top" width="95.53571428571429%">

                <p id="judul_spph" style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <span style="font-size:13px;line-height:115%;">Judul SPPH
                </p>
            </td>
        </tr>
        <tr>
            <td style="width: 22.5pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 14.05pt;vertical-align: bottom;"
                valign="bottom" width="4.464285714285714%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <span style="font-size:13px;line-height:115%;">2.</span></p>
            </td>
            <td colspan="7"
                style="width:481.5pt;border-top:none;border-left:  none;border-bottom:solid silver 1.0pt;border-right:solid white 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:14.05pt;"
                width="95.53571428571429%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <strong><span style="font-size:13px;line-height:115%;">Lokasi Pekerjaan/Pengiriman
                            Barang/Jasa</span></strong><strong><span
                            style="font-size:13px;line-height:115%;">&nbsp;:</span></strong></p>
            </td>
        </tr>
        <tr>
            <td style="width: 22.5pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 16.55pt;vertical-align: bottom;"
                valign="bottom" width="4.464285714285714%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <span style="font-size:13px;line-height:115%;">&nbsp;</span></p>
            </td>
            <td colspan="7"
                style="width:481.5pt;border-top:none;border-left:  none;border-bottom:solid silver 1.0pt;border-right:solid white 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:16.55pt;"
                width="95.53571428571429%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:justify;'>
                    <span style="font-size:13px;line-height:115%;">Gudang PT PINS Indonesia, Jl. Percetakan Negara
                        No.17A, RT.19/RW.7, Johar Baru, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10440.</span>
                </p>
            </td>
        </tr>
        <tr>
            <td style="width:22.5pt;border-top:none;border-left:solid white 1.0pt;border-bottom:solid silver 1.0pt;border-right:solid silver 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.1pt;"
                width="4.464285714285714%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <span style="font-size:13px;line-height:115%;">3.</span></p>
            </td>
            <td colspan="7"
                style="width:481.5pt;border-top:none;border-left:  none;border-bottom:solid silver 1.0pt;border-right:solid white 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.1pt;"
                width="95.53571428571429%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <strong><span style="font-size:13px;line-height:115%;">Jangka
                            Waktu&nbsp;</span></strong><strong><span
                            style="font-size:13px;line-height:115%;">Pelaksanaan Pekerjaan :</span></strong></p>
            </td>
        </tr>
        <tr>
            <td style="width: 22.5pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 15.95pt;vertical-align: bottom;"
                valign="bottom" width="4.464285714285714%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <span style="font-size:13px;line-height:115%;">&nbsp;</span></p>
            </td>
            <td colspan="7"
                style="width:481.5pt;border-top:none;border-left:  none;border-bottom:solid silver 1.0pt;border-right:solid white 1.0pt;padding:0cm 5.4pt 0cm 5.4pt;height:15.95pt;"
                width="95.53571428571429%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:  115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;text-align:justify;'>
                    <span style="font-size:13px;line-height:115%;">Jangka waktu pelaksanaan pekerjaan selambat-lambatnya
                        sampai dengan tanggal 2 Desember 2019.</span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 22.5pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 13.7pt;vertical-align: bottom;"
                valign="bottom" width="4.464285714285714%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <span style="font-size:13px;line-height:115%;">4.</span></p>
            </td>
            <td colspan="7"
                style="width: 481.5pt;border-top: none;border-left: none;border-bottom: 1pt solid silver;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 13.7pt;vertical-align: bottom;"
                valign="bottom" width="95.53571428571429%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <strong><span style="font-size:13px;line-height:115%;">Harga</span></strong><strong><span
                            style="font-size:13px;line-height:115%;">&nbsp;Pekerjaan :</span></strong></p>
            </td>
        </tr>
        <tr>
            <td style="width: 22.5pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 134.2pt;vertical-align: bottom;"
                valign="bottom" width="4.464285714285714%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <span style="font-size:13px;line-height:115%;">&nbsp;</span></p>
            </td>
            <td colspan="7"
                style="width: 481.5pt;border-top: none;border-left: none;border-bottom: 1pt solid silver;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 134.2pt;vertical-align: top;"
                valign="top" width="95.53571428571429%">

                <p id="harga_sp3"
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:justify;'>
                    <span style="font-size:13px;line-height:115%;">Harga pekerjaan adalah sebesar <strong>Rp.
                            149,160,000,- (Seratus Empat Puluh Sembilan Juta Seratus Enam Puluh Ribu
                            Rupiah)&nbsp;</strong>sudah termasuk PPN 10% dengan rincian sebagai berikut :</span></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:justify;'>
                    <span style="font-size:13px;line-height:115%;">&nbsp;</span><span
                        style="font-size:13px;line-height:115%;">&nbsp;</span></p>

                <table border="0" cellpadding="0" cellspacing="0" class="class4" style="width: 100%;">
                    <tbody>
                        <tr>
                            <td class="xl74" height="37"
                                style='color:black;font-size:13px;font-weight:700;font-style:normal;text-decoration:none;font-family:"Trebuchet MS", sans-serif;text-align:center;vertical-align:middle;background:silver;height:28.0pt;width:22pt;'
                                width="3.656998738965952%">No</td>
                            <td class="xl75"
                                style='color:black;font-size:13px;font-weight:700;font-style:normal;text-decoration:none;font-family:"Trebuchet MS", sans-serif;text-align:center;vertical-align:middle;background:silver;width:273pt;'
                                width="45.90163934426229%">Barang</td>
                            <td class="xl74"
                                style='color:black;font-size:13px;font-weight:700;font-style:normal;text-decoration:none;font-family:"Trebuchet MS", sans-serif;text-align:center;vertical-align:middle;background:silver;width:59pt;'
                                width="9.96216897856242%">Jumlah</td>
                            <td class="xl74"
                                style='color: black; font-size: 13px; font-weight: 700; font-style: normal; text-decoration: none; font-family: "Trebuchet MS", sans-serif; text-align: center; vertical-align: middle; background: silver; width: 10.2326%;'
                                width="10.21437578814628%">Satuan</td>
                            <td class="xl76"
                                style='color: black; font-size: 13px; font-weight: 700; font-style: normal; text-decoration: none; font-family: "Trebuchet MS", sans-serif; vertical-align: middle; text-align: center; background: silver; width: 15.6589%;'
                                width="15.636822194199244%">&nbsp;Harga Satuan &nbsp; &nbsp; Kesepakatan&nbsp;</td>
                            <td class="xl75"
                                style='color:black;font-size:13px;font-weight:700;font-style:normal;text-decoration:none;font-family:"Trebuchet MS", sans-serif;text-align:center;vertical-align:middle;background:silver;width:87pt;'
                                width="14.627994955863809%">Harga Total Kesepakatan</td>
                        </tr>
                        <tr>
                            <td class="xl78" height="37"
                                style='color:black;font-size:13px;font-weight:700;font-style:normal;text-decoration:none;font-family:"Trebuchet MS", sans-serif;text-align:center;vertical-align:middle;background:white;height:28.0pt;'>
                                1</td>
                            <td class="xl79"
                                style='color: black; font-size: 13px; font-weight: 400; font-style: normal; text-decoration: none; font-family: "Trebuchet MS", sans-serif; vertical-align: middle; width: 273pt; text-align: left;'>
                                Macbook Air MVFL2ID/A /MVFN2ID/A
                                <br>&nbsp;MBA 13.3/1.6DCi5/8GB/256GB-IND&nbsp;</td>
                            <td class="xl80"
                                style='color:black;font-size:13px;font-weight:400;font-style:normal;text-decoration:none;font-family:"Trebuchet MS", sans-serif;text-align:center;vertical-align:middle;'>
                                8</td>
                            <td class="xl81"
                                style='color: black; font-size: 13px; font-weight: 400; font-style: normal; text-decoration: none; font-family: "Trebuchet MS", sans-serif; vertical-align: middle; text-align: center; background: white; width: 10.2326%;'>
                                &nbsp;unit&nbsp;</td>
                            <td class="xl82"
                                style='color: black; font-size: 13px; font-weight: 400; font-style: normal; text-decoration: none; font-family: "Trebuchet MS", sans-serif; vertical-align: middle; text-align: center; width: 15.6589%;'>
                                &nbsp; &nbsp; &nbsp; &nbsp;16.950.000,00</td>
                            <td class="xl83"
                                style='color:black;font-size:13px;font-weight:400;font-style:normal;text-decoration:none;font-family:"Trebuchet MS", sans-serif;text-align:general;vertical-align:middle;text-align:center;background:white;width:87pt;'>
                                &nbsp; &nbsp; &nbsp; &nbsp; 135.600.000</td>
                        </tr>
                        <tr>
                            <td class="xl91" colspan="5" height="17"
                                style='color: black; font-size: 13px; font-weight: 700; font-style: normal; text-decoration: none; font-family: "Trebuchet MS", sans-serif; text-align: right; vertical-align: middle; background: rgb(217, 217, 217); height: 13pt;'>
                                Total&nbsp;</td>
                            <td class="xl84"
                                style='color:black;font-size:13px;font-weight:700;font-style:normal;text-decoration:none;font-family:"Trebuchet MS", sans-serif;text-align:general;vertical-align:middle;text-align:center;background:#D9D9D9;width:87pt;'>
                                &nbsp; &nbsp; &nbsp;135.600.000</td>
                        </tr>
                        <tr>
                            <td class="xl89" colspan="5" height="17"
                                style='color: black; font-size: 13px; font-weight: 700; font-style: normal; text-decoration: none; font-family: "Trebuchet MS", sans-serif; text-align: right; vertical-align: middle; background: rgb(217, 217, 217); height: 13pt;'>
                                PPN 10%&nbsp;</td>
                            <td class="xl86"
                                style='color: black; font-size: 13px; font-weight: 700; font-style: normal; text-decoration: none; font-family: "Trebuchet MS", sans-serif; vertical-align: middle; background: rgb(217, 217, 217); text-align: center;'>
                                &nbsp; &nbsp; &nbsp; &nbsp;13.560.000</td>
                        </tr>
                        <tr>
                            <td class="xl89" colspan="5" height="17"
                                style='color: black; font-size: 13px; font-weight: 700; font-style: normal; text-decoration: none; font-family: "Trebuchet MS", sans-serif; text-align: right; vertical-align: middle; background: rgb(217, 217, 217); height: 13pt;'>
                                Grand Total&nbsp;</td>
                            <td class="xl87"
                                style='color: black; font-size: 13px; font-weight: 700; font-style: normal; text-decoration: none; font-family: "Trebuchet MS", sans-serif; vertical-align: middle; background: rgb(217, 217, 217); text-align: center;'>
                                &nbsp; &nbsp; &nbsp;149.160.000</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 22.5pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 14.4pt;vertical-align: bottom;"
                valign="bottom" width="4.464285714285714%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <span style="font-size:13px;line-height:115%;">5.</span></p>
            </td>
            <td colspan="7"
                style="width: 481.5pt;border-top: none;border-left: none;border-bottom: 1pt solid silver;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 14.4pt;vertical-align: bottom;"
                valign="bottom" width="95.53571428571429%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <strong><span style="font-size:13px;line-height:115%;">Tata</span></strong><strong><span
                            style="font-size:13px;line-height:115%;">c</span></strong><strong><span
                            style="font-size:13px;line-height:115%;">ara&nbsp;</span></strong><strong><span
                            style="font-size:13px;line-height:115%;">P</span></strong><strong><span
                            style="font-size:13px;line-height:115%;">embayaran</span></strong><strong><span
                            style="font-size:13px;line-height:115%;">&nbsp;:</span></strong></p>
            </td>
        </tr>
        <tr>
            <td style="width: 22.5pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 3.75pt;vertical-align: bottom;"
                valign="bottom" width="4.464285714285714%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <span style="font-size:13px;line-height:115%;">&nbsp;</span></p>
            </td>
            <td colspan="7"
                style="width: 481.5pt;border-top: none;border-left: none;border-bottom: 1pt solid silver;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 3.75pt;vertical-align: bottom;"
                valign="bottom" width="95.53571428571429%">
                <p id="cara_bayar_sp3" style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                        <span style="font-size:13px;line-height:115%;">CARA BAYAR SP3
                    </p>
                
            </td>
        </tr>
        <tr>
            <td style="width: 22.5pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 16.2pt;vertical-align: bottom;"
                valign="bottom" width="4.464285714285714%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <span style="font-size:13px;line-height:115%;">6.</span></p>
            </td>
            <td colspan="7"
                style="width: 481.5pt;border-top: none;border-left: none;border-bottom: 1pt solid silver;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 16.2pt;vertical-align: bottom;"
                valign="bottom" width="95.53571428571429%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <strong><span style="font-size:13px;line-height:115%;">Ketentuan dan Syarat
                            Tambahan</span></strong><strong><span
                            style="font-size:13px;line-height:115%;">&nbsp;:</span></strong></p>
            </td>
        </tr>
        <tr>
            <td style="width: 22.5pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 4.15pt;vertical-align: bottom;"
                valign="bottom" width="4.464285714285714%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <span style="font-size:13px;line-height:115%;">&nbsp;</span></p>
            </td>
            <td colspan="7"
                style="width: 481.5pt;border-top: none;border-left: none;border-bottom: 1pt solid silver;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 4.15pt;vertical-align: bottom;"
                valign="bottom" width="95.53571428571429%">
                <p  style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                        <span style="font-size:13px;line-height:115%;" id="syarat_ketentuan_sp3">Syarat Ketentuan SP3
                    </p>
            </td>
        </tr>
        <tr>
            <td colspan="8"
                style="width: 504pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 6.35pt;vertical-align: bottom;"
                valign="bottom" width="100%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;margin-right:0cm;margin-left:21.6pt;'>
                    <em><span style="font-size:13px;line-height:115%;">Demikian Surat Penetapan ini dibuat sebagai dasar
                            pelaksanaan pekerjaan sebelum ditandatanganinya Perjanjian/Kontrak.</span></em></p>
            </td>
        </tr>
        <tr>
            <td colspan="2"
                style="width: 63pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 10.5pt;vertical-align: bottom;"
                valign="bottom" width="12.5%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;'>
                    <strong><span style="font-size:13px;line-height:115%;">&nbsp;</span></strong></p>
            </td>
            <td colspan="4"
                style="width: 220.5pt;border-top: none;border-left: none;border-bottom: 1pt solid silver;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 10.5pt;vertical-align: bottom;"
                valign="bottom" width="43.75%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:center;'>
                    <strong><span style="font-size:13px;line-height:115%;">PT. Sistech Kharisma.</span></strong></p>
            </td>
            <td colspan="2"
                style="width: 220.5pt;border-top: none;border-left: none;border-bottom: 1pt solid silver;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 10.5pt;vertical-align: bottom;"
                valign="bottom" width="43.75%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;margin-top:2.0pt;text-align:center;'>
                    <strong><span style="font-size:13px;line-height:115%;">PT</span></strong><strong><span
                            style="font-size:13px;line-height:115%;">.</span></strong><strong><span
                            style="font-size:13px;line-height:115%;">&nbsp;</span></strong><strong><span
                            style="font-size:13px;line-height:115%;">PINS Indonesia</span></strong></p>
            </td>
        </tr>
        <tr>
            <td colspan="2"
                style="width: 63pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid white;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 72.55pt;vertical-align: top;"
                valign="top" width="12.5%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <span style="font-size:13px;line-height:115%;">&nbsp;</span></p>
            </td>
            <td colspan="4"
                style="width: 220.5pt;border-top: none;border-left: none;border-bottom: 1pt solid white;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 72.55pt;vertical-align: top;"
                valign="top" width="43.75%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:center;'>
                    <strong><span style="font-size:13px;line-height:115%;">&nbsp;</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:center;'>
                    <strong><span style="font-size:13px;line-height:115%;">&nbsp;</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:center;'>
                    <strong><span style="font-size:13px;line-height:115%;">&nbsp;</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <strong><span style="font-size:13px;line-height:115%;">&nbsp;</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <strong><span style="font-size:13px;line-height:115%;">&nbsp;</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <strong><span style="font-size:13px;line-height:115%;">&nbsp;</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <strong><span style="font-size:13px;line-height:115%;">&nbsp;</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:center;'>
                    <strong><u><span style="font-size:13px;line-height:115%;">John Kurniawan</span></u></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:center;'>
                    <span style="font-size:13px;line-height:115%;">Direktur&nbsp;</span></p>
            </td>
            <td colspan="2"
                style="width: 220.5pt;border-top: none;border-left: none;border-bottom: 1pt solid white;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 72.55pt;vertical-align: top;"
                valign="top" width="43.75%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:center;'>
                    <strong><span style="font-size:13px;line-height:115%;">&shy;</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:center;'>
                    <strong><span style="font-size:13px;line-height:115%;">&nbsp;</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <strong><span style="font-size:13px;line-height:115%;">&nbsp;</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:center;'>
                    <strong><span style="font-size:13px;line-height:115%;">&nbsp;</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:center;'>
                    <strong><span style="font-size:13px;line-height:115%;">&nbsp;</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:center;'>
                    <strong><span style="font-size:13px;line-height:115%;">&nbsp;</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:center;'>
                    <strong><span style="font-size:13px;line-height:115%;">&nbsp;</span></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:center;'>
                    <strong><u><span style="font-size:13px;line-height:115%;">Hernadi Yoga Adhitya
                                Tama</span></u></strong></p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;text-align:center;'>
                    <span style="font-size:13px;line-height:115%;">GM e-Commerce</span></p>
            </td>
        </tr>
        <tr>
            <td colspan="2"
                style="width: 63pt;border-top: none;border-left: 1pt solid white;border-bottom: 1pt solid silver;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 6.4pt;vertical-align: top;"
                valign="top" width="12.5%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <strong><span style="font-size:13px;line-height:115%;">&nbsp;</span></strong></p>
            </td>
            <td colspan="4"
                style="width: 220.5pt;border-top: none;border-left: none;border-bottom: 1pt solid silver;border-right: 1pt solid silver;padding: 0cm 5.4pt;height: 6.4pt;vertical-align: top;"
                valign="top" width="43.75%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <span style="font-size:13px;line-height:115%;">&nbsp;</span></p>
            </td>
            <td colspan="2"
                style="width: 220.5pt;border-top: none;border-left: none;border-bottom: 1pt solid silver;border-right: 1pt solid white;padding: 0cm 5.4pt;height: 6.4pt;vertical-align: top;"
                valign="top" width="43.75%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'>
                    <span style="font-size:13px;line-height:115%;">&nbsp;</span></p>
            </td>
        </tr>
        <tr>
            <td style="border:none;" width="4.457652303120357%">
                <br>
            </td>
            <td style="border:none;" width="8.023774145616642%">
                <br>
            </td>
            <td style="border:none;" width="4.75482912332838%">
                <br>
            </td>
            <td style="border:none;" width="5.943536404160476%">
                <br>
            </td>
            <td style="border:none;" width="22.436849925705793%">
                <br>
            </td>
            <td style="border:none;" width="10.698365527488855%">
                <br>
            </td>
            <td style="border:none;" width="4.011887072808321%">
                <br>
            </td>
            <td style="border:none;" width="39.673105497771175%">
                <br>
            </td>
        </tr>
    </tbody>
</table>

<p style='margin:0cm;margin-bottom:.0001pt;line-height:115%;font-size:11px;font-family:"Trebuchet MS",sans-serif;'><span
        style="font-size:13px;line-height:115%;">&nbsp;</span></p>
