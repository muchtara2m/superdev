@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "EDIT PASAL";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')

<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            EDIT PASAL
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Master Data</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Edit Pasal</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i
                                class="fa fa-arrow-left"></i></button>

                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="{{action('PasalController@update', $id)}}">
                        @csrf
                        @method('PATCH')
                        <div class="box-body">
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">List</label>
                                    <input type="text" class="form-control" name="pasal" value="{{ $pasals['pasal'] }}"
                                        id="exampleInputEmail1">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Judul</label>
                                    <input type="text" class="form-control" name="judul" value="{{ $pasals['judul'] }}"
                                        id="exampleInputEmail1">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Isi</label>
                                    <textarea name="isi">
                                        {{ $pasals['isi'] }}
                                    </textarea>
                                </div>

                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-success" style="width: 7em;"><i
                                            class="fa fa-check"></i>
                                        Save</button>

                                </div>
                                <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<script>
    $(function () {
        $('textarea').froalaEditor({
            charCounterCount: false,
            key: '{{ env("KEY_FROALA") }}',
        })
    });
</script>

@endsection