@extends('layouts.master')

@section('title')
Pasal | Super Slim
@endsection

@section('stylesheets')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            PASAL
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Master Data</a></li>
            <li class="active"> Pasal </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> PASAL</h3>
                        <a href="{{ route('pasal.create') }}" class="btn btn-primary" style="float: right"><strong>Add Pasal</strong></a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="pbsTable" class="display">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Pasal</th>
                                    <th>Judul</th>
                                    <th>Isi</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pasals as $list)
                                <td>{{ $list->no }}</td>
                                <td>{{ $list->pasal }}</td>
                                <td>{{ $list->judul }}</td>
                                <td>{{ mb_strimwidth(strip_tags(html_entity_decode($list->isi, ENT_NOQUOTES)), 0, 300,".....") }}</td>
                                <td class="text-center">
                                        <div class="btn-group  btn-group-sm">
                                            <a href="{{action('PasalController@edit', $list->id)}}">
                                            <button class="btn btn-success btn-xs" type="button"><i class="fa fa-pencil"  title="Edit Pasal"></i></button></a>

                                            <form action="{{ action('PasalController@destroy', $list->id) }}" method="POST" style="display: inline;">
                                              @csrf
                                              @method('DELETE')
                                              <button type="submit" onclick="return confirm('Bener nih mau dihapus..?')" id="delete-btn" class="btn btn-danger btn-xs click-hand" title="Delete Pasal"><i class="fa fa-trash"></i>
                                              </button>
                                            </form>
                                          </div>
                                        </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
    $(document).ready( function () {
        $('#pbsTable').DataTable({

        });
    } );
</script>
@endsection
