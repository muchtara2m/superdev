@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Edit AR";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- Data Table -->
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
{{--  froala  --}}
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />


@endsection
@section('customstyle')
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    .example-modal .modal {
        background: transparent !important;
    }
    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }
    .boxx {
  float: left;
  height: 20px;
  width: 20px;
  margin-bottom: 15px;
  border: 1px solid black;
  clear: both;
}

.red {
  background-color: red;
}

.green {
  background-color: green;
}

.blue {
  background-color: blue;
}
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit AR
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">AR</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Edit AR</h3>
                    </div>
                    @if($chat != NULL)
                    <div class="form-group col-md-12" >
                        <label for="exampleInputEmail1">Comment</label>
                        {{ $ars->id }}
                        <textarea name="" id="chat" >
                            {{ $chat->chat }}
                        </textarea>
                    </div>
                    @endif
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="{{ url('update_ar/'.$ars->id) }}" id="form" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="customer">Customer</label>
                                    <input type="hidden" name="customer" value="{{ $ars->customer_id }}" id="customerid">
                                    <input type="text" class="form-control" title="Customer" data-toggle="modal" data-target="#modal-unit-mitra" readonly placeholder="Pilih Customer" id="customer" value="{{ $ars->customer['nama_customer'] }}">
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="noio">No. IO</label>
                                    <input type="hidden" name="io" value="{{ $ars->io_id }}" id="ioid">
                                    <input type="text" name="desio" id="noio" class="form-control" title="Deskripsi" data-toggle="modal" data-target="#modal-unit" readonly value="{{ $ars->dataio['no_io'] }}">
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="noio">Deskripsi Project</label>
                                    <input type="text" name="desio" id="descio" class="form-control"  readonly value="{{ $ars->dataio['deskripsi'] }}">
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="Currency">Nilai Project</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                        <input type="text" name="nilai_project" id="nilai_project" value="{{ number_format($ars->nilai_project,0,'.','.') }}" onkeyup="formatAngka(this,'.')" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3" hidden>
                                <div class="form-group">
                                    <label for="Currency">Nilai Belum ditagih</label>
                                    <input type="text" class="form-control" name="nilai_belum_ditagih" id="nilai_belum_ditagih" onfocus="formatAngka(this,'.')" disabled>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Deskripsi Project</label>
                                    <textarea class="form-control" id="text" name="uraian">
                                        {{ $ars->uraian }}
                                    </textarea>
                                </div>
                            </div>
                            @if($ars->invoice != NULL)
                            <div class="form-group col-md-12">
                                <div class="form-group" >
                                    <label for="exampleInputEmail1">Deskripsi Invoice</label>
                                    <textarea name="invoice" id="text" class="form-control">
                                        {{ $ars->invoice }}
                                    </textarea>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <h4 class="divider-title">DOKUMEN</h4>
                                    <hr>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="form-group col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="spk"  name="spk" {{ ($ars->spk != NULL && $ars->spk != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="spk" value="spk" name="spk" {{ ($ars->spk == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="SPK" class="form-control" id="spkin" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-green">
                                                        <input type="checkbox" id="kb"  name="kb" {{ ($ars->kb != NULL && $ars->kb != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                    </span>
                                                    <span class="input-group-addon bg-red">
                                                        <input type="checkbox" id="kb" value="kb" name="kb" {{ ( $ars->kb == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                    </span>
                                                    <input type="text" value="KB" class="form-control" id="kbin" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    <div class="form-group col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="kl"  name="kl" {{ ($ars->kl != NULL && $ars->kl != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="kl" value="kl" name="kl" {{ ( $ars->kl == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="KL" class="form-control" id="klin" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="baut"  name="baut" {{ ($ars->baut != NULL && $ars->baut != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="baut" value="baut" name="baut" {{ ( $ars->baut == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="BAUT" readonly class="form-control" id="bautin">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="bast"  name="bast" {{ ($ars->bast != NULL && $ars->bast != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="bast" value="bast"  name="bast" {{ ( $ars->bast == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="BAST" readonly class="form-control" id="bastin">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="baso"  name="baso" {{ ($ars->baso != NULL && $ars->baso != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="baso" value="baso" name="baso" {{ ( $ars->baso == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="BASO PINS" readonly class="form-control" id="basoin">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon bg-green">
                                                        <input type="checkbox" id="basocus"  name="basocus" {{ ($ars->basocus != NULL && $ars->basocus != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                    </span>
                                                    <span class="input-group-addon bg-red">
                                                        <input type="checkbox" id="basocus" value="basocus" name="basocus" {{ ( $ars->basocus == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                    </span>
                                                    <input type="text" value="BASO CUST" readonly class="form-control" id="basoin">
                                                </div>
                                            </div>
                                        </div>
                                    <div class="form-group col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="bapp"  name="bapp" {{ ($ars->bapp != NULL && $ars->bapp != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="bapp" value="bapp" name="bapp" {{ ( $ars->bapp == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="BAPP" readonly class="form-control" id="bappin">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="npk"  name="npk" {{ ($ars->npk != NULL && $ars->npk != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="npk" value="npk"  name="npk" {{ ( $ars->npk == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="NPK" readonly class="form-control" id="npkin">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="lpp"  name="lpp" {{ ($ars->lpp != NULL && $ars->lpp != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="lpp" value="lpp" name="lpp" {{ ( $ars->lpp == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="LPP" readonly class="form-control" id="lppin">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="baperub"  name="baperub" {{ ($ars->baperub != NULL && $ars->baperub != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="baperub" value="baperub"  name="baperub" {{ ( $ars->baperub == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="BA Perub JK WKT" readonly class="form-control" id="bain">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3" hidden>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="suratgm"  name="suratgm" {{ ($ars->suratgm != NULL && $ars->suratgm != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="suratgm" value="suratgm" name="suratgm" {{ ( $ars->suratgm == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="Surat GM Segmen" readonly class="form-control" id="gmin">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3" hidden>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="top"  name="top" {{ ($ars->top != NULL && $ars->top != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="top" value="top"  name="top" {{ ( $ars->top == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="PPN" readonly class="form-control" id="topin">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="barekon"  name="barekon" {{ ($ars->barekon != NULL && $ars->barekon != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="barekon" value="barekon"  name="barekon" {{ ( $ars->barekon == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="BA Rekon" readonly class="form-control" id="barein">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="performansi"  name="performansi" {{ ($ars->performansi != NULL && $ars->performansi != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="performansi" value="performansi" name="performansi" {{ ( $ars->performansi == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="Performansi" readonly class="form-control" id="perfomin">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3" hidden>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="lkpp"  name="lkpp" {{ ($ars->lkpp != NULL && $ars->lkpp != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="lkpp" value="lkpp" name="lkpp" {{ ( $ars->lkpp == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="LKPP" readonly class="form-control" id="lkppin">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3" hidden>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon bg-green">
                                                    <input type="checkbox" id="ep"  name="ep" {{ ($ars->ep != NULL && $ars->ep != '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <span class="input-group-addon bg-red">
                                                    <input type="checkbox" id="ep" value="ep"  name="ep" {{ ( $ars->ep == '1111-11-11 11:11:11')? 'checked':'' }}>
                                                </span>
                                                <input type="text" value="EP" readonly class="form-control" id="epin">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" name="status" value="unbill" class="btn btn-success" style="width: 7em;">Save</button>
                                {{-- <button type="submit" name="status" value="readytobill" onclick="return confirm('Apakah Semua Data Sudah Terpenuhi ?')" class="btn btn-primary" style="width: 7em;"><i class="fa fa-check"></i> Submit</button> --}}
                            </div>
                            <!-- /.box-footer -->
                        </form>
                        <!-- modal IO-->
                        <div class="modal fade" id="modal-unit">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Data Unit</h4>
                                        </div>
                                        <div class="modal-body">
                                            <table id="io" class="display table-responsive">
                                                <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>IO</th>
                                                        <th>Description</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($io as $ios)
                                                    <tr>
                                                        <td>{{ $ios['id'] }}</td>
                                                        <td>{{$ios['no_io']}}</td>
                                                        <td>{{$ios['deskripsi']}}</td>
                                                        <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            <!-- modal Mitra-->
                            <div class="modal fade" id="modal-unit-mitra">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title">Data Customer</h4>
                                            </div>
                                            <div class="modal-body">
                                                <table id="mitra" class="display table-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Perusahaan</th>
                                                            <th>Alamat</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($customer as $list)
                                                        <tr>
                                                            <td>{{ $list['id'] }}</td>
                                                            <td>{{$list['nama_customer']}}</td>
                                                            <td>{{$list['alamat']}}</td>
                                                            <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                            </div>
                            <!-- /.box -->
                        </div>
                        <!--/.col (right) -->
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            
            
            @endsection
            
            @section('scripts')
            <!-- Include Editor JS files. -->
            <script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
            {{-- bahasa indonesia froala --}}
            <script src="{{ asset('froala/js/languages/id.js') }}"></script>
            {{--  datatabe  --}}
            <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
            
            <!-- date-range-picker -->
            <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
            <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
            <!-- bootstrap datepicker -->
            <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
            <!-- iCheck 1.0.1 -->
            <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
            <!-- clockpicker -->
            <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
            <!-- Select2 -->
            <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
            {{--  datatable  --}}
            
            {{-- input format tanggal --}}
            <script>
                function pemberitahuan(){
                    confirm("Apakah anda yakin semua dokumen telah dipenuhi !");
                }
                $(function(){
                    $('#text').froalaEditor({
                        placeholderText: 'Type Something',
                        language: 'id',
                        charCounterCount: false,
                        key: '{{ env("KEY_FROALA") }}',
                    });
                    $('#chat').froalaEditor({
                        key: '{{ env("KEY_FROALA") }}',
                        height: 70,
                        toolbarButtons:['help'],
                        charCounterCount: false,
                        // language: 'id',
                        
                    })
                    $('.rangedate').daterangepicker();
                    $(".datejos").on("change", function() {
                        this.setAttribute(
                        "data-date",
                        moment(this.value, "YYYY-MM-DD")
                        .format( this.getAttribute("data-date-format") )
                        )
                    }).trigger("change")
                    $('.datejos').datepicker({
                        autoclose: true,
                        orientation: "bottom"
                    })
                })
                function tambahHasil(){
                    var nilai_project = $('#nilai_project').val();
                    var project_hapus_dot = nilai_project.split('.').join("");
                    var nilai_invoice = $('#nilai_invoice').val();
                    var invoice_hapus_dot = nilai_invoice.split('.').join("");
                    var hasil = project_hapus_dot - invoice_hapus_dot;
                    $('#nilai_belum_ditagih').val(hasil);
                }
                function formatAngka(objek, separator) {
                    a = objek.value;
                    b = a.replace(/[^\d]/g, "");
                    c = "";
                    panjang = b.length;
                    j = 0;
                    for (i = panjang; i > 0; i--) {
                        j = j + 1;
                        if (((j % 3) == 1) && (j != 1)) {
                            c = b.substr(i - 1, 1) + separator + c;
                        } else {
                            c = b.substr(i - 1, 1) + c;
                            formatAngka
                        }
                    }
                    objek.value = c;
                }
                function clearDot(number)
                {
                    output = number.replace(".", "");
                    return output;
                }
                </script>
                <script>
                    $(document).ready( function () {
                        $('#io').DataTable();
                    })
                    $(document).ready( function () {
                        $('#mitra').DataTable();
                    })
                    var table = document.getElementById('io');
                    var tablemitra = document.getElementById('mitra');
                    
                    for(var i = 1; i < table.rows.length; i++)
                    {
                        table.rows[i].onclick = function()
                        {
                            //  rIndex = this.rowIndex;
                            document.getElementById("ioid").value = this.cells[0].innerHTML;
                            document.getElementById("noio").value = this.cells[1].innerHTML;
                            document.getElementById("descio").value = this.cells[2].innerHTML;
                        };
                    };
                    
                    for(var i = 1; i < tablemitra.rows.length; i++)
                    {
                        tablemitra.rows[i].onclick = function()
                        {
                            //  rIndex = this.rowIndex;
                            document.getElementById("customerid").value = this.cells[0].innerHTML;
                            document.getElementById("customer").value = this.cells[1].innerHTML;
                        };
                    };</script>
                    @endsection
                    