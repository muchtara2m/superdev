@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Create AR";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- Data Table -->
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
{{--  froala  --}}
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />


@endsection
@section('customstyle')
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    .example-modal .modal {
        background: transparent !important;
    }
    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            AR
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">AR</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form AR</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="" id="form" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="customer">Customer</label>
                                    <input type="text" class="form-control" title="Customer" data-toggle="modal" data-target="#modal-unit-mitra" readonly placeholder="Pilih Customer" id="customer">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="noio">No. IO</label>
                                    <input type="text" name="desio" id="noio" class="form-control" title="Deskripsi" data-toggle="modal" data-target="#modal-unit" readonly placeholder="Pilih IO">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="Currency">Currency</label>
                                    <select name="" id="" class="form-control">
                                        <option value="IDR">IDR</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">No KL/WO</label>
                                    <input type="text" class="form-control" name="perusahaan" placeholder="NO KL/WO" id="exampleInputEmail1">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="tglspph">Tanggal BAST</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datejos"  placeholder="Tanggal KL/WO" >
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="tglspph">Tanggal KL/WO</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datejos"  placeholder="Tanggal KL/WO" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nilai Project</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-dollar"></i></span>
                                        <input type="text" onkeyup="formatAngka(this,'.')" placeholder="1.000.00.000" class="form-control">
                                        <span class="input-group-addon">.00</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Bulan Awal Penagihan</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right rangedate" id="blnawal">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="">Bulan Akhir Penagihan</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right rangedate" id="blnawal">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">LPP Yang Sudah Dilengkapi</label>
                                    <input type="text" class="form-control rangedate" name="" id="">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">LPP Yang Belum Dilengkapi</label>
                                    <input type="text" class="form-control rangedate" name="" id="">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Uraian</label>
                                    <textarea class="form-control"></textarea>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <h4 class="divider-title">FILE UPLOAD</h4>
                                <hr>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File SPK</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="spk" value="spk">
                                        </span>
                                        <input type="file" class="form-control" id="spkin">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File KL</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="kl">
                                        </span>
                                        <input type="file" class="form-control" id="klin">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File BAUT</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="baut">
                                        </span>
                                        <input type="file" class="form-control" id="bautin">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File BAST</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="bast">
                                        </span>
                                        <input type="file" class="form-control" id="bastin">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File BAST2</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="bast2">
                                        </span>
                                        <input type="file" class="form-control" id="bast2in">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File BASO</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="baso">
                                        </span>
                                        <input type="file" class="form-control" id="basoin">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File BAPP</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="bapp">
                                        </span>
                                        <input type="file" class="form-control" id="bappin">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File BAOP</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="baop">
                                        </span>
                                        <input type="file" class="form-control" id="baopin">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File NPK</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="npk">
                                        </span>
                                        <input type="file" class="form-control" id="npkin">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File LPP</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="lpp">
                                        </span>
                                        <input type="file" class="form-control" id="lppin">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File BA Perub JK WKT</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="ba">
                                        </span>
                                        <input type="file" class="form-control" id="bain">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File Surat GM Segmen</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="gm">
                                        </span>
                                        <input type="file" class="form-control" id="gmin">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File Term Of Payment</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="top">
                                        </span>
                                        <input type="file" class="form-control" id="topin">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File BA Rekon</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="bare">
                                        </span>
                                        <input type="file" class="form-control" id="barein">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File Performansi</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="perfom">
                                        </span>
                                        <input type="file" class="form-control" id="perfomin">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File LKPP</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="lkpp">
                                        </span>
                                        <input type="file" class="form-control" id="lkppin">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-3">
                                <div class="form-group">
                                    <label for="">File EP</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <input type="checkbox" id="ep">
                                        </span>
                                        <input type="file" class="form-control" id="epin">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <h4 class="divider-title">DETAILS</h4>
                                <hr>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <label for="">Keterangan</label>
                                    <textarea class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group col-md-5">
                                <div class="form-group">
                                    <label for="">Status Doc</label>
                                    <select name="" class="form-control" id="">
                                        <option value="UNBILLED">UNBILLED</option>
                                        <option value="BILLED">BILLED</option>
                                        <option value="RETURN">RETURN</option>
                                        <option value="PENDINF">PENDING</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-5">
                                <div class="form-group">
                                    <label for="">Cara Pembayaran</label>
                                    <select name="" class="form-control" id="carabayar">
                                        <option value="OTC & BULANAN">OTC & BULANAN</option>
                                        <option value="OTC">OTC</option>
                                        <option value="PARSIAL">PARSIAL</option>
                                        <option value="PERBULAN">PERBULAN</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-2" id="tt">
                                <div class="form-group">
                                    <label for="">Bulan</label>
                                    <input type="number" name="" class="form-control" placeholder="24" id="bln" min="1" max="60">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="">Tanggal Input</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datejos"  placeholder="Tanggal Input" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="">Tanggal Clear</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datejos"  placeholder="Tanggal Clear" >
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="">PIC</label>
                                    <select id="role-cm_role" class="form-control" name="" onchange="getRoles(this.value)">
                                        <option selected disabled>PIC</option>
                                        @foreach ($user as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <label for="">DETAILS</label>
                                    <textarea class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- modal IO-->
                    <div class="modal fade" id="modal-unit">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Data Unit</h4>
                                    </div>
                                    <div class="modal-body">
                                        <table id="io" class="display table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>IO</th>
                                                    <th>Description</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($io as $ios)
                                                <tr>
                                                    <td>{{ $ios['id'] }}</td>
                                                    <td>{{$ios['no_io']}}</td>
                                                    <td>{{$ios['deskripsi']}}</td>
                                                    <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                        <!-- modal Mitra-->
                        <div class="modal fade" id="modal-unit-mitra">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title">Data Unit</h4>
                                        </div>
                                        <div class="modal-body">
                                            <table id="mitra" class="display table-responsive">
                                                <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Perusahaan</th>
                                                        <th>Alamat</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($mitra as $list)
                                                    <tr>
                                                        <td>{{ $list['id'] }}</td>
                                                        <td>{{$list['perusahaan']}}</td>
                                                        <td>{{$list['alamat']}}</td>
                                                        <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->


        @endsection

        @section('scripts')
        <!-- Include Editor JS files. -->
        <script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
        {{-- bahasa indonesia froala --}}
        <script src="{{ asset('froala/js/languages/id.js') }}"></script>
        {{--  datatabe  --}}
        <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

        <!-- date-range-picker -->
        <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
        <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
        <!-- bootstrap datepicker -->
        <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
        <!-- iCheck 1.0.1 -->
        <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
        <!-- clockpicker -->
        <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
        <!-- Select2 -->
        <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
        {{--  datatable  --}}

        {{-- input format tanggal --}}
        <script>
            $(function(){
                $('textarea').froalaEditor({
                    placeholderText: 'Type Something',
                    language: 'id',
                    charCounterCount: false,
                    key: '{{ env("KEY_FROALA") }}',
                });
                $('.rangedate').daterangepicker();
                $(".datejos").on("change", function() {
                    this.setAttribute(
                    "data-date",
                    moment(this.value, "YYYY-MM-DD")
                    .format( this.getAttribute("data-date-format") )
                    )
                }).trigger("change")
                $('.datejos').datepicker({
                    autoclose: true,
                    orientation: "bottom"
                })
            })
            $("#form").find(':file').prop('disabled', true);
            $('#spk').click(function(){
                var spk = $(this).is(':checked');
                if(spk == true){
                    $('#spkin').prop('disabled',false);
                }else{
                    $('#spkin').prop('disabled',true);
                }
            })
            $('#kl').click(function(){
                var kl = $(this).is(':checked');
                if(kl == true){
                    $('#klin').prop('disabled',false);
                }else{
                    $('#klin').prop('disabled',true);
                }
            })
            $('#baut').click(function(){
                var baut = $(this).is(':checked');
                if(baut == true){
                    $('#bautin').prop('disabled',false);
                }else{
                    $('#bautin').prop('disabled',true);
                }
            })
            $('#bast').click(function(){
                var bast = $(this).is(':checked');
                if(bast == true){
                    $('#bastin').prop('disabled',false);
                }else{
                    $('#bastin').prop('disabled',true);
                }
            })
            $('#bast2').click(function(){
                var bast2 = $(this).is(':checked');
                if(bast2 == true){
                    $('#bast2in').prop('disabled',false);
                }else{
                    $('#bast2in').prop('disabled',true);
                }
            })
            $('#baso').click(function(){
                var baso = $(this).is(':checked');
                if(baso == true){
                    $('#basoin').prop('disabled',false);
                }else{
                    $('#basoin').prop('disabled',true);
                }
            })
            $('#bapp').click(function(){
                var bapp = $(this).is(':checked');
                if(bapp == true){
                    $('#bappin').prop('disabled',false);
                }else{
                    $('#bappin').prop('disabled',true);
                }
            })
            $('#baop').click(function(){
                var baop = $(this).is(':checked');
                if(baop == true){
                    $('#baopin').prop('disabled',false);
                }else{
                    $('#baopin').prop('disabled',true);
                }
            })
            $('#npk').click(function(){
                var npk = $(this).is(':checked');
                if(npk == true){
                    $('#npkin').prop('disabled',false);
                }else{
                    $('#npkin').prop('disabled',true);
                }
            })
            $('#lpp').click(function(){
                var lpp = $(this).is(':checked');
                if(lpp == true){
                    $('#lppin').prop('disabled',false);
                }else{
                    $('#lppin').prop('disabled',true);
                }
            })
            $('#ba').click(function(){
                var ba = $(this).is(':checked');
                if(ba == true){
                    $('#bain').prop('disabled',false);
                }else{
                    $('#bain').prop('disabled',true);
                }
            })
            $('#gm').click(function(){
                var gm = $(this).is(':checked');
                if(gm == true){
                    $('#gmin').prop('disabled',false);
                }else{
                    $('#gmin').prop('disabled',true);
                }
            })
            $('#top').click(function(){
                var top = $(this).is(':checked');
                if(top == true){
                    $('#topin').prop('disabled',false);
                }else{
                    $('#topin').prop('disabled',true);
                }
            })
            $('#bare').click(function(){
                var bare = $(this).is(':checked');
                if(bare == true){
                    $('#barein').prop('disabled',false);
                }else{
                    $('#barein').prop('disabled',true);
                }
            })
            $('#perfom').click(function(){
                var perfom = $(this).is(':checked');
                if(perfom == true){
                    $('#perfomin').prop('disabled',false);
                }else{
                    $('#perfomin').prop('disabled',true);
                }
            })
            $('#lkpp').click(function(){
                var lkpp = $(this).is(':checked');
                if(lkpp == true){
                    $('#lkppin').prop('disabled',false);
                }else{
                    $('#lkppin').prop('disabled',true);
                }
            })
            $('#ep').click(function(){
                var ep = $(this).is(':checked');
                if(ep == true){
                    $('#epin').prop('disabled',false);
                }else{
                    $('#epin').prop('disabled',true);
                }
            })
            $('#tt').prop('hidden',true);

            $('#carabayar').change(function(){
                $('#bln').prop('hidden',true);
                var carabayar = $(this).val();
                if(carabayar != 'PERBULAN'){
                    $('#tt').prop('hidden',true);
                }else{
                    $('#tt').prop('hidden',false);

                }
            })
            $('select').select2();
            function getRoles(val) {
                $('#role-cm_role_text').val('');
                var data = $('#role-cm_role').select2('data').map(function(elem){ return elem.text} );
                console.log(data);
                $('#role-cm_role_text').val(data);
                $('#role-cm_role').on('select2:unselecting', function (e) {
                    $('#role-cm_role_text').val('');
                });
            }
            function formatAngka(objek, separator) {
                a = objek.value;
                b = a.replace(/[^\d]/g, "");
                c = "";
                panjang = b.length;
                j = 0;
                for (i = panjang; i > 0; i--) {
                    j = j + 1;
                    if (((j % 3) == 1) && (j != 1)) {
                        c = b.substr(i - 1, 1) + separator + c;
                    } else {
                        c = b.substr(i - 1, 1) + c;
                        formatAngka
                    }
                }
                objek.value = c;
            }
            function clearDot(number)
            {
                output = number.replace(".", "");
                return output;
            }</script>
            <script>
                $(document).ready( function () {
                    $('#io').DataTable();
                })
                $(document).ready( function () {
                    $('#mitra').DataTable();
                })
                var table = document.getElementById('io');
                var tablemitra = document.getElementById('mitra');

                for(var i = 1; i < table.rows.length; i++)
                {
                    table.rows[i].onclick = function()
                    {
                        //  rIndex = this.rowIndex;
                        document.getElementById("noio").value = this.cells[1].innerHTML;
                    };
                };
                for(var i = 1; i < table.rows.length; i++)
                {
                    tablemitra.rows[i].onclick = function()
                    {
                        //  rIndex = this.rowIndex;
                        document.getElementById("customer").value = this.cells[1].innerHTML;
                    };
                };</script>
                @endsection
