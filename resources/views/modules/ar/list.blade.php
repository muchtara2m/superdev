@extends('layouts.master')

@section('title')
Unbill AR | Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot {
        display: table-header-group;
    }
    .dt-buttons{
        padding-bottom: 5px;
    }
    #icon{
        text-align:center;
    }
    /* table thead th tbody td{
        :
    } */
    tbody tr{
        white-space: nowrap;
    }
    thead tr th small{
        color: red;
        font-size: 10px
    }
    h3{
        padding-bottom: inherit;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            UNBILL
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">AR</a></li>
            <li class="active"> Unbill </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Unbill</h3>
                        <div style="text-align:center">
                            <div class="row input-daterange">
                                <div class="col-md-4">
                                    <select name="bulan" id="bulan" class="form-control">
                                        
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select name="tahun" id="tahun" class="form-control">
                                        
                                    </select>
                                </div>
                                <div class="col-md-4" hidden>
                                    <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                                    <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="pbsTable" class="display">
                            @csrf
                            <thead>
                                <tr style="white-space: nowrap">
                                    <th rowspan="2">Action</th>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2">IO</th>
                                    <th rowspan="2">Deskripsi IO</th>
                                    <th rowspan="2">Customer</th>
                                    <th rowspan="2">UBIS</th>
                                    <th rowspan="2">Uraian</th>
                                    <th rowspan="2">Nilai Revenue</th>
                                    <th colspan="13" style="text-align: -webkit-center">Status</th>
                                    <th rowspan="2">Status Dokumen</th>
                                </tr>
                                <tr style="white-space: nowrap">
                                    <th>SPK <br> <small>(UBIS)</small></th>
                                    <th>KB <br> <small>(UBIS)</small></th>
                                    <th>KL <br> <small>(LEGAL CONTRACT)</small></th>
                                    <th>BAUT <br> <small>(SDV)</small></th>
                                    <th>BAST <br> <small>(SDV)</small></th>
                                    <th>BASO PINS<br> <small>(SDV)</small></th>
                                    <th>BASO CUST<br> <small>(UBIS)</small></th>
                                    <th>BAPP <br> <small>(SDV)</small></th>
                                    <th>LPP <br> <small>(OPERATION)</small></th>
                                    <th>BA Perub JK WKT/Amandemen <br> <small>(SDV)</small></th>
                                    <th>BA Rekon <br> <small>(OPERATION)</small></th>
                                    <th>Performansi <br> <small>(OPERATION)</small></th>
                                    <th>NPK <br> <small>(TREASURY)</small></th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th class="testing">IO</th>
                                    <th></th>
                                    <th class="testing">Customer</th>
                                    <th class="testing">UBIS</th>
                                    <th class="testing">Uraian</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>                                    
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<script src="{{ asset('js/web/monthAndYear.js') }}"></script>
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script>
   
    dataUnbill();
    $('#bulan').change(function(){
        var bln = $('#bulan').val();
        var thn = $('#tahun').val();
        if(bln != '' &&  thn != '')
        {
            $('#pbsTable').DataTable().destroy();
            dataUnbill(bln, thn);
        }
        else
        {
            alert('Both Date is required');
        }
    });
    $('#tahun').change(function(){
        var bln = $('#bulan').val();
        var thn = $('#tahun').val();
        if(bln != '' &&  thn != '')
        {
            $('#pbsTable').DataTable().destroy();
            dataUnbill(bln, thn);
        }
        else
        {
            alert('Both Date is required');
        }
    });
    $('#pbsTable tfoot .testing').each( function () {
            var title = $(this).text();
            $(this).html( '<input type="text" style="text-align:center;"placeholder="'+title+'" />' );
        } );
    function dataUnbill(bln = '', thn = ''){
        var table = $('#pbsTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url:'{{ url("unbill_ar") }}',
                data:{bln:bln, thn:thn}
            },
            columns: [
            { data: 'action'},
            { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
            { data: 'dataio.no_io','defaultContent': ""},
            { data: 'dataio.deskripsi','defaultContent': ""},
            { data: 'customer.nama_customer','defaultContent': ""},
            { data: 'pembuat.unitnya.nama','defaultContent': ""},            
            { data: 'uraian','defaultContent': ""},
            { data: 'nilai_project', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )},
            { 
                data: 'spk', 
                render: function(data) { 
                    if(data ==  '1111-11-11 11:11:11') {
                        return '<i class="fa fa-minus"></i>';
                    } else if(data != '1111-11-11 11:11:11' && data !==null){
                        return '<i class="fa fa-check"></i>';
                    }else if(data === null){
                        return '<i class="fa fa-close"></i>';
                    }
                },
                defaultContent: ''
            },
            { 
                data: 'kb', 
                render: function(data) { 
                    if(data ==  '1111-11-11 11:11:11') {
                        return '<i class="fa fa-minus"></i>';
                    } else if(data != '1111-11-11 11:11:11' && data !==null){
                        return '<i class="fa fa-check"></i>';
                    }else if(data === null){
                        return '<i class="fa fa-close"></i>';
                    }
                },
                defaultContent: ''
            },
            { 
                data: 'kl', 
                render: function(data) { 
                    if(data ==  '1111-11-11 11:11:11') {
                        return '<i class="fa fa-minus"></i>';
                    } else if(data != '1111-11-11 11:11:11' && data !==null){
                        return '<i class="fa fa-check"></i>';
                    }else if(data === null){
                        return '<i class="fa fa-close"></i>';
                    }
                },
                defaultContent: ''
            },
            { 
                data: 'baut', 
                render: function(data) { 
                    if(data ==  '1111-11-11 11:11:11') {
                        return '<i class="fa fa-minus"></i>';
                    } else if(data != '1111-11-11 11:11:11' && data !==null){
                        return '<i class="fa fa-check"></i>';
                    }else if(data === null){
                        return '<i class="fa fa-close"></i>';
                    }
                },
                defaultContent: ''
            },
            { 
                data: 'bast', 
                render: function(data) { 
                    if(data ==  '1111-11-11 11:11:11') {
                        return '<i class="fa fa-minus"></i>';
                    } else if(data != '1111-11-11 11:11:11' && data !==null){
                        return '<i class="fa fa-check"></i>';
                    }else if(data === null){
                        return '<i class="fa fa-close"></i>';
                    }
                },
                defaultContent: ''
            },
            { 
                data: 'baso', 
                render: function(data) { 
                    if(data ==  '1111-11-11 11:11:11') {
                        return '<i class="fa fa-minus"></i>';
                    } else if(data != '1111-11-11 11:11:11' && data !==null){
                        return '<i class="fa fa-check"></i>';
                    }else if(data === null){
                        return '<i class="fa fa-close"></i>';
                    }
                },
                defaultContent: ''
            },
            { 
                data: 'basocus', 
                render: function(data) { 
                    if(data ==  '1111-11-11 11:11:11') {
                        return '<i class="fa fa-minus"></i>';
                    } else if(data != '1111-11-11 11:11:11' && data !==null){
                        return '<i class="fa fa-check"></i>';
                    }else if(data === null){
                        return '<i class="fa fa-close"></i>';
                    }
                },
                defaultContent: ''
            },
            { 
                data: 'bapp', 
                render: function(data) { 
                    if(data ==  '1111-11-11 11:11:11') {
                        return '<i class="fa fa-minus"></i>';
                    } else if(data != '1111-11-11 11:11:11' && data !==null){
                        return '<i class="fa fa-check"></i>';
                    }else if(data === null){
                        return '<i class="fa fa-close"></i>';
                    }
                },
                defaultContent: ''
            },
            { 
                data: 'lpp', 
                render: function(data) { 
                    if(data ==  '1111-11-11 11:11:11') {
                        return '<i class="fa fa-minus"></i>';
                    } else if(data != '1111-11-11 11:11:11' && data !==null){
                        return '<i class="fa fa-check"></i>';
                    }else if(data === null){
                        return '<i class="fa fa-close"></i>';
                    }
                },
                defaultContent: ''
            },
            { 
                data: 'baperub', 
                render: function(data) { 
                    if(data ==  '1111-11-11 11:11:11') {
                        return '<i class="fa fa-minus"></i>';
                    } else if(data != '1111-11-11 11:11:11' && data !==null){
                        return '<i class="fa fa-check"></i>';
                    }else if(data === null){
                        return '<i class="fa fa-close"></i>';
                    }
                },
                defaultContent: ''
            },
            { 
                data: 'barekon', 
                render: function(data) { 
                    if(data ==  '1111-11-11 11:11:11') {
                        return '<i class="fa fa-minus"></i>';
                    } else if(data != '1111-11-11 11:11:11' && data !==null){
                        return '<i class="fa fa-check"></i>';
                    }else if(data === null){
                        return '<i class="fa fa-close"></i>';
                    }
                },
                defaultContent: ''
            },
            { 
                data: 'performansi', 
                render: function(data) { 
                    if(data ==  '1111-11-11 11:11:11') {
                        return '<i class="fa fa-minus"></i>';
                    } else if(data != '1111-11-11 11:11:11' && data !==null){
                        return '<i class="fa fa-check"></i>';
                    }else if(data === null){
                        return '<i class="fa fa-close"></i>';
                    }
                },
                defaultContent: ''
            },
            { 
                data: 'npk', 
                render: function(data) { 
                    if(data ==  '1111-11-11 11:11:11') {
                        return '<i class="fa fa-minus"></i>';
                    } else if(data != '1111-11-11 11:11:11' && data !==null){
                        return '<i class="fa fa-check"></i>';
                    }else if(data === null){
                        return '<i class="fa fa-close"></i>';
                    }
                },
                defaultContent: ''
            },
            { data: 'status.toUpperCase()'},
            ],
            
        });
        table.columns().every( function () {
            
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                    .search( this.value )
                    .draw();
                }
            } );
        } );
    } 
</script>

@endsection



