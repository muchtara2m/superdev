@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Create AR";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- Data Table -->
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
{{--  froala  --}}
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />


@endsection
@section('customstyle')
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    .example-modal .modal {
        background: transparent !important;
    }
    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            AR
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">AR</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form AR</h3>
                    </div>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="{{ route('insert_ar') }}" id="form" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="customer">Customer</label>
                                    <input type="hidden" name="customer" id="idcustomer">
                                    <select id="role-cm_role" class="form-control" name="customer" onchange="getRoles(this.value)">
                                        <option selected disabled>Pilih Customer</option>
                                        @foreach ($customer as $item)
                                        <option value="{{ $item->id }}">{{ $item->nama_customer }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="noio">No. IO</label>
                                    <input type="hidden" name="io" id="idio">
                                    <input type="text"  id="noio" class="form-control" title="Deskripsi" data-toggle="modal" data-target="#modal-unit"  placeholder="Pilih IO">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="noio">Deskripsi IO</label>
                                    <input type="text" name="desio" id="descio" class="form-control"  readonly >
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="tglbakn">Tanggal End Project</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datejos" name="endproject" id="enddelivery-date" value="{{ old('endproject') }}">
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <div class="form-group">
                                    <label for="Currency">Nilai Project</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">IDR</span>
                                        <input type="text" class="form-control" name="nilai_project" id="nilai_project" placeholder="10000000" onkeyup="formatAngka(this,'.')">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-4" id="belumditagih">
                                <div class="form-group">
                                    <label for="Currency">Nilai Belum ditagih</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">IDR</span>
                                        <input type="text" class="form-control" name="" id="nilai_belum_ditagih" onfocus="formatAngka(this,'.')" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Deskripsi Project</label>
                                    <textarea name="uraian" class="form-control"></textarea>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <h4 class="divider-title">DOKUMEN</h4>
                                <hr>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon primary bg-green">
                                                <input type="checkbox" name="spk" id="spk">
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" name="spk" id="spk" value="spk">
                                            </span>
                                            <input type="text" value="SPK" class="form-control" id="spkin" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon primary bg-green">
                                                <input type="checkbox" name="kl" id="kl">
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" name="kl" id="kl" value="kl">
                                            </span>
                                            <input type="text" value="KL" class="form-control" id="klin" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon primary bg-green">
                                                <input type="checkbox" name="baut" id="baut">
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" name="baut" id="baut" value="baut">
                                            </span>
                                            <input type="text" value="BAUT" readonly class="form-control" id="bautin">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon primary bg-green">
                                                <input type="checkbox" name="bast" id="bast">
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" name="bast" id="bast" value="bast">
                                            </span>
                                            <input type="text" value="BAST" readonly class="form-control" id="bastin">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon primary bg-green">
                                                <input type="checkbox" name="baso" id="baso">
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" name="baso" id="baso" value="baso">
                                            </span>
                                            <input type="text" value="BASO" readonly class="form-control" id="basoin">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon primary bg-green">
                                                <input type="checkbox" name="bapp" id="bapp">
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" name="bapp" id="bapp" value="bapp">
                                            </span>
                                            <input type="text" value="BAPP" readonly class="form-control" id="bappin">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon primary bg-green">
                                                <input type="checkbox" name="npk" id="npk">
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" name="npk" id="npk" value="npk">
                                            </span>
                                            <input type="text" value="NPK" readonly class="form-control" id="npkin">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon primary bg-green">
                                                <input type="checkbox" name="lpp" id="lpp">
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" name="lpp" id="lpp" value="lpp">
                                            </span>
                                            <input type="text" value="LPP" readonly class="form-control" id="lppin">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon primary bg-green">
                                                <input type="checkbox" name="baperub" id="ba">
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" name="baperub" id="ba" value="baperub">
                                            </span>
                                            <input type="text" value="BA Perub JK WKT" readonly class="form-control" id="bain">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon primary bg-green">
                                                <input type="checkbox" name="suratgm" id="gm">
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" name="suratgm" id="gm" value="gm">
                                            </span>
                                            <input type="text" value="Surat GM Segmen" readonly class="form-control" id="gmin">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon primary bg-green">
                                                <input type="checkbox" name="top" id="top">
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" name="top" id="top" value="top">
                                            </span>
                                            <input type="text" value="PPN" readonly class="form-control" id="topin">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon primary bg-green">
                                                <input type="checkbox" name="barekon" id="bare">
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" name="barekon" id="bare" value="bare">
                                            </span>
                                            <input type="text" value="BA Rekon" readonly class="form-control" id="barein">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon primary bg-green">
                                                <input type="checkbox" name="performansi" id="perfom">
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" name="performansi" id="perfom" value="perfom">
                                            </span>
                                            <input type="text" value="Performansi" readonly class="form-control" id="perfomin">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon primary bg-green">
                                                <input type="checkbox" name="lkpp" id="lkpp">
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" name="lkpp" id="lkpp" value="lkpp">
                                            </span>
                                            <input type="text" value="LKPP" readonly class="form-control" id="lkppin">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon primary bg-green">
                                                <input type="checkbox" name="ep" id="ep">
                                            </span>
                                            <span class="input-group-addon bg-red">
                                                <input type="checkbox" name="ep" id="ep" value="ep">
                                            </span>
                                            <input type="text" value="EP" readonly class="form-control" id="epin">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" name="status" value="unbill" class="btn btn-success" style="width: 7em;">Save</button>
                            <button type="submit" name="status" value="readytobill" onclick="return confirm('Apakah Semua Data Sudah Terpenuhi ?')" class="btn btn-primary" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- modal IO-->
                    <div class="modal fade" id="modal-unit">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Data Unit</h4>
                                    </div>
                                    <div class="modal-body">
                                        <table id="io" class="display table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>Id</th>
                                                    <th>IO</th>
                                                    <th>Description</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($io as $ios)
                                                <tr>
                                                    <td>{{ $ios['id'] }}</td>
                                                    <td>{{ (int)$ios['no_io']}}</td>
                                                    <td>{{$ios['deskripsi']}}</td>
                                                    <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


    @endsection

    @section('scripts')
    <!-- Include Editor JS files. -->
    <script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
    {{-- bahasa indonesia froala --}}
    <script src="{{ asset('froala/js/languages/id.js') }}"></script>
    {{--  datatabe  --}}
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <!-- date-range-picker -->
    <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <!-- clockpicker -->
    <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    {{--  datatable  --}}

    {{-- input format tanggal --}}
    <script>
        $(function () {
            $(".datejos").on("change", function () {
                this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DD")
                .format(this.getAttribute("data-date-format"))
                )
            }).trigger("change")
        });
        $('.datejos').datepicker({
            autoclose: true,
            orientation: "bottom"
        });
        $(document).ready(function(){
            $(document).change(function(){
                var io=$('#idio').val();
                var token = $("input[name='_token']").val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type:'post',
                    url:"{{ route('data-io') }}",
                    data: {io:io, _token:token},
                    dataType:'json',//return data will be json
                    success:function(data){
                        // console.log(data.nilai['total']);
                        $('#belumditagih').attr('hidden', true);
                        $('#nilai_project').attr('readonly', false);
                        if( data.options['nilai_project'] != null){
                            var nilaiakhir = data.options['nilai_project'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                            var nilaibelumditagih = data.nilai['total'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                            $('#belumditagih').attr('hidden', false);
                            // console.log(nilaiakhir);
                            $('#nilai_project').val(nilaiakhir);
                            $('#nilai_belum_ditagih').val(nilaibelumditagih);
                            $('#nilai_project').attr('readonly', true);
                        }

                    },
                    error:function(){

                    }
                });
            });
        });</script>
        <script>
            $('select').select2();
            function getRoles(val) {
                $('#role-cm_role_text').val('');
                var data = $('#role-cm_role').select2('data').map(function(elem){ return elem.text} );
                // console.log(data);
                $('#role-cm_role_text').val(data);
                $('#role-cm_role').on('select2:unselecting', function (e) {
                    $('#idcustomer').val('');
                });
            }
            $(function(){
                $('textarea').froalaEditor({
                    placeholderText: 'Type Something',
                    language: 'id',
                    charCounterCount: false,
                    key: '{{ env("KEY_FROALA") }}',
                });

                $('.rangedate').daterangepicker();
                $(".datejos").on("change", function() {
                    this.setAttribute(
                    "data-date",
                    moment(this.value, "YYYY-MM-DD")
                    .format( this.getAttribute("data-date-format") )
                    )
                }).trigger("change")
                $('.datejos').datepicker({
                    autoclose: true,
                    orientation: "bottom"
                })
            })
            function tambahHasil(){
                var nilai_project = $('#nilai_project').val();
                var project_hapus_dot = nilai_project.split('.').join("");
                var nilai_invoice = $('#nilai_invoice').val();
                var invoice_hapus_dot = nilai_invoice.split('.').join("");
                var hasil = project_hapus_dot - invoice_hapus_dot;
                // $('#nilai_belum_ditagih').val(hasil);
            }
            function formatAngka(objek, separator) {
                a = objek.value;
                b = a.replace(/[^\d]/g, "");
                c = "";
                panjang = b.length;
                j = 0;
                for (i = panjang; i > 0; i--) {
                    j = j + 1;
                    if (((j % 3) == 1) && (j != 1)) {
                        c = b.substr(i - 1, 1) + separator + c;
                    } else {
                        c = b.substr(i - 1, 1) + c;
                        formatAngka
                    }
                }
                objek.value = c;
            }
            function clearDot(number)
            {
                output = number.replace(".", "");
                return output;
            }</script>
            <script>
                $(document).ready( function () {
                    $('#io').DataTable();
                })
                var table = document.getElementById('io');
                for(var i = 1; i < table.rows.length; i++)
                {
                    table.rows[i].onclick = function()
                    {
                        //  rIndex = this.rowIndex;
                        document.getElementById("idio").value = this.cells[0].innerHTML;
                        document.getElementById("noio").value = this.cells[1].innerHTML;
                        document.getElementById("descio").value = this.cells[2].innerHTML;
                    };
                };</script>

                @endsection
