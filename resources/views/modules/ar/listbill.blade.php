@extends('layouts.master')

@section('title')
List Bill | Super Slim
@endsection

@section('stylesheets')
<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }

    tfoot {
        display: table-header-group;
    }

    /* table thead th tbody td{
        :
    } */
    tbody tr {
        white-space: nowrap;
    }

    h3 {
        padding-bottom: inherit;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            List Bill
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">AR</a></li>
            <li class="active"> List Bill</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> List Bill</h3>
                        <div style="text-align:center">
                            <div class="row input-daterange">
                                <div class="col-md-4">
                                    <select name="bulan" id="bulan" class="form-control">

                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select name="tahun" id="tahun" class="form-control">

                                    </select>
                                </div>
                                <div class="col-md-4" hidden>
                                    <button type="button" name="filter" id="filter"
                                        class="btn btn-primary">Filter</button>
                                    <button type="button" name="refresh" id="refresh"
                                        class="btn btn-default">Refresh</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="pbsTable" class="display">
                            @csrf
                            <thead>
                                <tr style="white-space: nowrap">
                                    <th>Action</th>
                                    <th>No</th>
                                    <th>IO</th>
                                    <th>Deskripsi IO</th>
                                    <th>Customer</th>
                                    <th>Ubis</th>
                                    <th>Deskripsi Project</th>
                                    <th>Nilai Revenue</th>
                                    <th>Nilai Invoice</th>
                                    <th>Tanggal Input</th>
                                    <th>Tanggal Submit Invoice</th>
                                    <th>PIC</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th class="testing">IO</th>
                                    <th></th>
                                    <th class="testing">Customer</th>
                                    <th class="testing">Ubis</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>

                    </div>
                    <div class="modal fade" id="modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Upload File</h4>
                                </div>
                                <div class="modal-body">
                                    <form method="post" action="" enctype="multipart/form-data">
                                        @csrf
                                        {{-- @method('PATCH') --}}
                                        {{-- <input type="hidden" name="title" class="form-control">
                                            <br>
                                            <input type="file" name="file[]" id="" class="form-control" multiple="multiple">
                                            <br> --}}
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" data-date="" data-date-format="yyyy-mm-dd"
                                                class="form-control datejos" name="tglupload" id="enddelivery-date">
                                        </div>
                                        <br>
                                        <button type="submit" class="btn btn-success" style="width: 7em;"><i
                                                class="fa fa-check"></i>
                                            Submit</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<script src="{{ asset('js/web/monthAndYear.js') }}"></script>
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>

<script>
    // script datepicker
    $(function () {
        $(".datejos").on("change", function () {
            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DD")
                .format(this.getAttribute("data-date-format"))
            )
        }).trigger("change")
    });
    $('.datejos').datepicker({
        autoclose: true,
        orientation: "bottom"
    });
    dataBill();

    $('#bulan').change(function () {
        var bln = $('#bulan').val();
        var thn = $('#tahun').val();
        if (bln != '' && thn != '') {
            $('#pbsTable').DataTable().destroy();
            dataBill(bln, thn);
        } else {
            alert('Both Date is required');
        }
    });
    $('#tahun').change(function () {
        var bln = $('#bulan').val();
        var thn = $('#tahun').val();
        if (bln != '' && thn != '') {
            $('#pbsTable').DataTable().destroy();
            dataBill(bln, thn);
        } else {
            alert('Both Date is required');
        }
    });
    $('#pbsTable tfoot .testing').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" style="text-align:center;"placeholder="' + title + '" />');
    });

    function dataBill(bln = '', thn = '') {
        var table = $('#pbsTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{{ url("bill") }}',
                data: {
                    bln: bln,
                    thn: thn
                }
            },
            columns: [{
                    data: 'action'
                },
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'dataio.no_io',
                    'defaultContent': ""
                },
                {
                    data: 'dataio.deskripsi',
                    'defaultContent': ""
                },
                {
                    data: 'customer.nama_customer',
                    'defaultContent': ""
                },
                {
                    data: 'pembuat.unitnya.nama',
                    'defaultContent': ""
                },
                {
                    data: 'uraian',
                    'defaultContent': ""
                },
                {
                    data: 'nilai_project',
                    render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')
                },
                {
                    data: 'nilai_invoice',
                    render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')
                },
                {
                    data: 'created_at'
                },
                {
                    data: 'tgl_invoice'
                },
                {
                    data: 'pembuat.name',
                    'defaultContent': ""
                },
            ]
        });
        table.columns().every(function () {

            var that = this;
            $('input', this.footer()).on('keyup change', function () {
                if (that.search() !== this.value) {
                    that
                        .search(this.value)
                        .draw();
                }
            });
        });
    };
    // document.getElementById("pbsTable_wrapper").style.overflow = "auto";

    $(document).on("click", ".upload", function () {
        var idbro = $(this).data('id');
        // $(".modal-body #idnya").val( idbro );
        $('form').attr('action', "{{ url('upload')}}/" + idbro);
    });
</script>

@endsection