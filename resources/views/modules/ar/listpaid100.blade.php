@extends('layouts.master')

@section('title')
Paid 100% | Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">

<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot {
        display: table-header-group;
    }
    /* table thead th tbody td{
        :
    } */
    tbody tr {
        white-space: nowrap;
    }
    h3{
        padding-bottom: inherit;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Paid 100%
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">AR</a></li>
            <li class="active">  Paid 100%</li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i>  Paid 100%</h3>
                        <div style="text-align:center">
                            <div class="row input-daterange">
                                <div class="col-md-4">
                                    <select name="bulan" id="bulan" class="form-control">
                                        
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select name="tahun" id="tahun" class="form-control">
                                        
                                    </select>
                                </div>
                                <div class="col-md-4" hidden>
                                    <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                                    <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        
                        <table id="pbsTable" class="display">
                            <thead>
                                <tr style="white-space: nowrap">
                                    <th>Action</th>
                                    <th>No</th>
                                    <th>IO</th>
                                    <th>Deskripsi IO</th>
                                    <th>Customer</th>
                                    <th>Ubis</th>
                                    <th>Deskripsi Project</th>
                                    <th>Nilai Revenue</th>
                                    <th>Tanggal Dibuat</th>
                                    <th>Tanggal Invoice</th>
                                    <th>Tanggal Bukti Bayar</th>
                                    <th>PIC</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th class="testing">IO</th>
                                    <th></th>
                                    <th class="testing">Customer</th>
                                    <th class="testing">UBIS</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                        
                    </div>
                    
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<script src="{{ asset('js/web/monthAndYear.js') }}"></script>
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script>
    
    dataPaid100();
    $('#bulan').change(function(){
        var bln = $('#bulan').val();
        var thn = $('#tahun').val();
        if(bln != '' &&  thn != '')
        {
            $('#pbsTable').DataTable().destroy();
            dataPaid100(bln, thn);
        }
        else
        {
            alert('Both Date is required');
        }
    });
    $('#tahun').change(function(){
        var bln = $('#bulan').val();
        var thn = $('#tahun').val();
        if(bln != '' &&  thn != '')
        {
            $('#pbsTable').DataTable().destroy();
            dataPaid100(bln, thn);
        }
        else
        {
            alert('Both Date is required');
        }
    });
    $('#pbsTable tfoot .testing').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" style="text-align:center;"placeholder="'+title+'" />' );
    } );
    function dataPaid100(bln='',thn=''){
       var table = $('#pbsTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url:'{{ url("paid100") }}',
                data:{bln:bln, thn:thn}
            },
            columns: [
            { data: 'action'},
            { data: 'DT_RowIndex', name: 'DT_RowIndex' , orderable: false, searchable: false},
            { data: 'dataio.no_io','defaultContent': ""},
            { data: 'dataio.deskripsi','defaultContent': ""},
            { data: 'customer.nama_customer','defaultContent': ""},
            { data: 'pembuat.unitnya.nama','defaultContent': ""},
            { data: 'uraian','defaultContent': ""},
            { data: 'nilai_project', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )},
            // { data: 'nilai_invoice', render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )},
            { data: 'created_at'},
            { data: 'tgl_invoice'},
            { data: 'upload'},
            { data: 'pembuat.name','defaultContent': ""},
            ]
        });
        table.columns().every( function () {
            
            var that = this;
            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( that.search() !== this.value ) {
                    that
                    .search( this.value )
                    .draw();
                }
            } );
        } );
    }
    
</script>

@endsection
