@extends('layouts.master')

@section('title')
Revisi {{ ($revisiKontrakLkpp->revisi_kontrak_lkpps->revisi - 1) }}
@endsection

@section('stylesheets')
<!-- css froala editor -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Revisi <small>Ke {{ ($revisiKontrakLkpp->revisi_kontrak_lkpps->revisi - 1) }} </small></h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active"> Preview </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid" style="border-radius: 5px;border-left: 4px solid#00a7d0 !important;">
                    <div class="box-header with-border">
                        <h4 class="text-center"><strong>REVISI {{ strtoupper($spphLKPP->judul) }}</strong></h4>
                    </div>
                    <div class="box-body ">
                        <div class="row">
                            <div class="col-sm-6">
                                <dl class="dl-horizontal">
                                    <dt>Nomor SPPH</dt>
                                    <dd>{{ $spphLKPP->nomorspph }}</dd>
                                    <dt>Nomor SPH</dt>
                                    <dd>{{ $spphLKPP->nomorsph == NULL ? 'Belum Diinput' : $spphLKPP->nomorsph }}</dd>
                                    <dt>Pembuat</dt>
                                    <dd>{{ $user->name }}</dd>
                                    <dt>Tanggal</dt>
                                    <dd>{{ date('d F Y',strtotime($revisiKontrakLkpp->tanggal_kontrak)) }}</dd>
                                    <dt>Mitra</dt>
                                    <dd>{{ $spphLKPP->mitra_lkpps->perusahaan }}</dd>
                                    @if ($baknLKPP->isi != null)
                                        <dt>Metode Bayar</dt>
                                        @php
                                            $i = 1;                                            
                                            $data = explode(",",$baknLKPP->cara_bayar);
                                        @endphp
                                            @foreach ($data as $item)
                                                <dd class="text-justify">{{$i++.'. '.$item }}</dd>                                                
                                            @endforeach
                                    @endif
                                </dl>
                            </div>

                            <div class="col-sm-6">
                                @php
                                $title_lampiran = ($revisiKontrakLkpp->lampiran) !=NULL ? json_decode($revisiKontrakLkpp->title_lampiran, TRUE) : NULL;
                                $lampiran       = ($revisiKontrakLkpp->lampiran) !=NULL ? json_decode($revisiKontrakLkpp->lampiran, TRUE) : NULL;
                                $title          = ($revisiKontrakLkpp->file) !=NULL ? json_decode($revisiKontrakLkpp->title, TRUE) : NULL ;
                                $file           = ($revisiKontrakLkpp->file) !=NULL ? json_decode($revisiKontrakLkpp->file, TRUE) : NULL;
                                @endphp
                                <div class="row">
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>Lampiran</dt>
                                            @if ($title_lampiran != NULL)
                                            @foreach ($title_lampiran as $key => $value)
                                            {{ ($key+1)}}. <a href="{{Storage::url($lampiran[$key])}}">{{$title_lampiran[$key]}}</a><br>
                                            @endforeach
                                            @endif
                                           
                                          </dl>
                                    </div>
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>File</dt>
                                            @if ($title != NULL)
                                            @foreach ($title as $key => $value)
                                            {{ ($key+1)}}. <a
                                                href="{{Storage::url($file[$key])}}">{{$title[$key]}}</a><br>
                                            @endforeach
                                            @endif
                                           
                                          </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-solid" style="border-radius: 8px">
                        <textarea name="" id="" cols="30" rows="10">
                            {{ $revisiKontrakLkpp->isi }}
                        </textarea>
                       
                </div>
            </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<!-- script function froala -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<script src="{{ asset('froala/js/languages/id.js') }}"></script>
<!-- External -->
<script type="text/javascript">
    $('textarea').froalaEditor({
        // fullPage: true,
        toolbarButtons      : ['print', 'html', 'getPDF', 'fullscreen'],
        charCounterCount    : false,
        key                 : keyFroala,
        height              : 500,
    })
    // removo alert lisensi froala
$("div > a", ".fr-wrapper").css('display', 'none');
</script>
@endsection