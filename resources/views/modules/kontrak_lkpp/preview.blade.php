@extends('layouts.master')

@section('title')
Preview Kontrak
@endsection

@section('stylesheets')
<!-- css froala editor -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<style>
    .bottomright {
      position: absolute;
      bottom: 0;
      right: 2mm;
      font-size: 18px;
    }
    @page {
        size            : A4;
        margin-top      : 3cm;
        margin-bottom   : 2cm;
        margin-left     : 30mm; 
        margin-right    : 20mm;
    }
    @media print {
        .bottomright{
        position: fixed;
        padding-top: 5%;
        bottom: 0;
    }
   body {
      margin-top      : 20mm; 
      margin-bottom   : 30mm; 
      margin-left     : 30mm; 
      margin-right    : 20mm;
      }
  }
</style>
@endsection
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Kontrak<small> Preview</small></h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active"> Preview </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid" style="border-radius: 5px;border-left: 4px solid#00a7d0 !important;">
                    <div class="box-header with-border">
                        <h4 class="text-center">
                            <strong>
                            @if ($kontrak->revisi >=1)
                            REVISI
                            @endif
                             {{ strtoupper($kontrak->bakn_lkpp->spph_lkpps->judul) }}</strong>
                        </h4>
                    </div>
                    <div class="box-body ">
                        <div class="row">
                            <div class="col-sm-4">
                                <dl class="dl-horizontal">
                                    <dt>Nomor SPPH</dt>
                                        <dd>{{ $kontrak->bakn_lkpp->spph_lkpps->nomorspph }}</dd>
                                    <dt>Nomor SPH</dt>
                                        <dd>{{ $kontrak->bakn_lkpp->spph_lkpps->nomorsph == NULL ? 'Belum Diinput' : $kontrak->bakn_lkpp->spph_lkpps->nomorsph }}</dd>
                                    <dt>Pembuat</dt>
                                        <dd>{{ $kontrak->user_kontrak_lkpp->name }}</dd>
                                    <dt>Tanggal</dt>
                                        <dd>{{ date('d F Y',strtotime($kontrak->tanggal_kontrak)) }}</dd>
                                    <dt>Mitra</dt>
                                        <dd>{{ $kontrak->bakn_lkpp->spph_lkpps->mitra_lkpps->perusahaan }}</dd>
                                    @if ($kontrak->bakn_lkpp->isi != null)
                                        <dt>Metode Bayar</dt>
                                        @php
                                            $i = 1;  
                                            $x = 0;                                          
                                            $data = explode(",",$kontrak->bakn_lkpp->cara_bayar);
                                        @endphp
                                            @foreach ($data as $item)
                                                <dd class="text-justify">{{$i++.'. '.$item }}</dd>                                                
                                            @endforeach
                                    @endif
                                </dl>
                            </div>
                            <div class="col-sm-2">
                                @if ($kontrak->revisi >=1)
                                <div class="row">
                                    <div class="col-xs-2"></div>
                                    <div class="col-xs-10">
                                        <dl>
                                            <dt>Revisi</dt>
                                            @foreach ($kontrak->revisi_kontrak_lkpps as $item)
                                                <a target="_blank" href="/preview-revisi-kontrak-lkpp/{{ $item->id }}">  Revisi Ke - {{$x++ }}</a><br>
                                            @endforeach
                                            <p disabled="true"> Revisi Ke - {{ $kontrak->revisi }} </p>

                                        </dl>
                                    </div>
                                </div>
                                @endif
                              
                            </div>
                            <div class="col-sm-6">
                                @php
                                $title_lampiran = ($kontrak->lampiran) !=NULL ? json_decode($kontrak->title_lampiran, TRUE) : NULL;
                                $lampiran       = ($kontrak->lampiran) !=NULL ? json_decode($kontrak->lampiran, TRUE) : NULL;
                                $title          = ($kontrak->file) !=NULL ? json_decode($kontrak->title, TRUE) : NULL ;
                                $file           = ($kontrak->file) !=NULL ? json_decode($kontrak->file, TRUE) : NULL;
                                @endphp
                                <div class="row">
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>Lampiran Kontrak</dt>
                                            @if ($title_lampiran != NULL)
                                            @foreach ($title_lampiran as $key => $value)
                                            {{ ($key+1)}}. <a href="{{Storage::url($lampiran[$key])}}">{{$title_lampiran[$key]}}</a><br>
                                            @endforeach
                                            @endif
                                           
                                          </dl>
                                    </div>
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>File Kontrak</dt>
                                            @if ($title != NULL)
                                            @foreach ($title as $key => $value)
                                            {{ ($key+1)}}. <a
                                                href="{{Storage::url($file[$key])}}">{{$title[$key]}}</a><br>
                                            @endforeach
                                            @endif
                                           
                                          </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-solid" style="border-radius: 8px">
                    <textarea name="" id="" cols="30" rows="10">
                        {{ $kontrak->isi }}
                    </textarea>
                </div>
            </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- script function froala -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>


<!-- External -->
<script type="text/javascript">
   $(document).ready(function () {
        $('textarea').froalaEditor({
        // fullPage: true,
        toolbarButtons      : ['print', 'html', 'getPDF', 'fullscreen'],
        charCounterCount    : false,
        key                 : keyFroala,
        height              : 500,
    });
    // removo alert lisensi froala
    $("div > a", ".fr-wrapper").css('display', 'none');
    
});
</script>
@endsection