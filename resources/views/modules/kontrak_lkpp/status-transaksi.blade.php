@extends('layouts.master')

@section('title')
Kontrak Status
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<!-- Moment -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            STATUS
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active">Status</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Status</h3>
                        <div class="pull-right box-tools">
                            <a href="/export-spph-lkpp" class="btn btn-info"><i class="fa fa-download"></i> </a>
                        </div>
                    </div>
                    <div style="text-align:center">
                        <div>
                            <select name="bulan" id="bulan" class="date form-group" style="width:10%;height:34px">
                            </select>
                            <select name="tahun" id="tahun" class="date form-group" style="width:10%;height:34px">
                            </select>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="status" class="display" data-url="/lkpp/kontrak/status" width="100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Key</th>
                                    <th>IO</th>
                                    <th>No.Kontrak</th>
                                    <th>Judul</th>
                                    <th>Tgl.Kontrak</th>
                                    <th>Mitra</th>
                                    <th>Harga</th>
                                    <th>Pembuat</th>
                                    <th>Posisi</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<!-- Sweet Alert -->
<script src="{{ asset('assets/sweetalert/js/sweetalert2.all.min.js')}}"></script>
<!-- External JS -->
<script src="{{ asset('js/web/main/js/kontrak_lkpp/status.js')}}"></script>
@endsection