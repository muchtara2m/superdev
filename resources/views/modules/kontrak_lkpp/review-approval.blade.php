@extends('layouts.master')

@section('title')
Approval Kontrak
@endsection

@section('stylesheets')
<!-- css froala editor -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
{{-- smartwizard --}}
<link href="{{ asset('smartwizard/dist/css/smart_wizard.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('smartwizard/dist/css/smart_wizard_theme_circles.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('smartwizard/dist/css/smart_wizard_theme_circles.min.css') }}" rel="stylesheet" type="text/css" />
<style>
        .bottomright {
      position: absolute;
      bottom: 0;
      right: 2mm;
      font-size: 18px;
    }
  @page {
  size            : A4;
  margin-top      : 3cm;
  margin-bottom   : 2cm;
  margin-left     : 30mm; 
  margin-right    : 20mm;
}
@media print {
    .bottomright{
    position: fixed;
    padding-top: 5%;
    bottom: 0;
  }
   body {
      margin-top      : 20mm; 
      margin-bottom   : 30mm; 
      margin-left     : 30mm; 
      margin-right    : 20mm;
      }
  }
</style>
@endsection
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Kontrak<small> Approval</small></h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active"> Approval </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid" style="border-radius: 5px;border-left: 4px solid#00a7d0 !important;">
                    <div class="box-header with-border">
                        <h4 class="text-center">
                            <strong>
                            @if ($kontrak->revisi >=1)
                            REVISI
                            @endif
                             {{ strtoupper($spphLKPP->judul) }}</strong>
                        </h4>
                    </div>
                    <div class="box-body ">
                        <div class="row">
                            <div class="col-sm-4">
                                <dl class="dl-horizontal">
                                    <dt>Nomor SPPH</dt>
                                    <dd>{{ $spphLKPP->nomorspph }}</dd>
                                    <dt>Nomor SPH</dt>
                                    <dd>{{ $spphLKPP->nomorsph == NULL ? 'Belum Diinput' : $spphLKPP->nomorsph }}</dd>
                                    <dt>Pembuat</dt>
                                    <dd>{{ $kontrak->user_kontrak_lkpp->name }}</dd>
                                    <dt>Tanggal</dt>
                                    <dd>{{ date('d F Y',strtotime($kontrak->tanggal_kontrak)) }}</dd>
                                    <dt>Mitra</dt>
                                    <dd>{{ $spphLKPP->mitra_lkpps->perusahaan }}</dd>
                                    <dt>Harga</dt>
                                    <dd>Rp. {{ number_format($bakn->harga,0,';','.') }}</dd>
                                    @if ($kontrak->bakn_lkpp->isi != null)
                                        <dt>Metode Bayar</dt>
                                        @php
                                            $i = 1;  
                                            $x = 0;                                          
                                            $data = explode(",",$kontrak->bakn_lkpp->cara_bayar);
                                        @endphp
                                            @foreach ($data as $item)
                                                <dd class="text-justify">{{$i++.'. '.$item }}</dd>                                                
                                            @endforeach
                                    @endif
                                </dl>
                            </div>
                            <div class="col-sm-2">
                                @if ($kontrak->revisi >=1)
                                <div class="row">
                                    <div class="col-xs-2"></div>
                                    <div class="col-xs-10">
                                        <dl>
                                            <dt>Revisi</dt>
                                            {{-- @foreach ($kontrak->revisi_kontrak_lkpp as $item)
                                                <a target="_blank" href="/preview-revisi-bakn-lkpp/{{ $item->id }}">  Revisi Ke - {{$x++ }}</a><br>
                                            @endforeach --}}
                                            <p disabled="true"> Revisi Ke - {{ $kontrak->revisi }} </p>

                                        </dl>
                                    </div>
                                </div>
                                @endif
                              
                            </div>
                            <div class="col-sm-3">
                                @php
                                $title_lampiran = ($kontrak->lampiran) !=NULL ? json_decode($kontrak->title_lampiran, TRUE) : NULL;
                                $lampiran       = ($kontrak->lampiran) !=NULL ? json_decode($kontrak->lampiran, TRUE) : NULL;
                                $title          = ($kontrak->file) !=NULL ? json_decode($kontrak->title, TRUE) : NULL ;
                                $file           = ($kontrak->file) !=NULL ? json_decode($kontrak->file, TRUE) : NULL;
                                @endphp
                                <div class="row">
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>Lampiran Kontrak</dt>
                                            @if ($title_lampiran != NULL)
                                            @foreach ($title_lampiran as $key => $value)
                                            {{ ($key+1)}}. <a href="{{Storage::url($lampiran[$key])}}">{{$title_lampiran[$key]}}</a><br>
                                            @endforeach
                                            @endif
                                           
                                          </dl>
                                    </div>
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>File Kontrak</dt>
                                            @if ($title != NULL)
                                            @foreach ($title as $key => $value)
                                            {{ ($key+1)}}. <a
                                                href="{{Storage::url($file[$key])}}">{{$title[$key]}}</a><br>
                                            @endforeach
                                            @endif
                                           
                                          </dl>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                @php
                               // Spph
                                $titleLampiranSpph  = ($bakn->spph_lkpps['lampiran'])!=NULL ? json_decode($bakn->spph_lkpps['title_lampiran'], TRUE) : NULL;
                                $lampiranSpph       = ($bakn->spph_lkpps['lampiran'])!=NULL ? json_decode($bakn->spph_lkpps['lampiran'], TRUE) : NULL;
                                $titleFileSpph      = ($bakn->spph_lkpps['file'])!=NULL ? json_decode($bakn->spph_lkpps['title'], TRUE) : NULL ;
                                $fileSpph           = ($bakn->spph_lkpps['file'])!=NULL ? json_decode($bakn->spph_lkpps['file'], TRUE) : NULL;
                                @endphp
                                <div class="row">
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>Lampiran BAKN</dt>
                                            @if ($titleLampiranSpph != NULL)
                                                @foreach ($titleLampiranSpph as $key => $value)
                                                    {{ ($key+1)}}. <a target="_blank" href="{{Storage::url($lampiranSpph[$key])}}">{{$titleLampiranSpph[$key]}}</a><br>
                                                @endforeach
                                            @endif                                           
                                          </dl>
                                    </div>
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>File BAKN</dt>
                                            @if ($titleFileSpph != NULL)
                                                @foreach ($titleFileSpph as $key => $value)
                                                    {{ ($key+1)}}. <a target="_blank" href="{{Storage::url($fileSpph[$key])}}">{{$titleFileSpph[$key]}}</a><br>
                                                @endforeach
                                            @endif
                                          </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            
                            <div id="smartwizard" style="border-radius: 8px; background-color:white">
                                <ul >
                                    <li><a href="#step-1">Step 1<br /><small>Draft Kontrak</small></a></li>
                                    <li><a href="#step-2">Step 2<br /><small> BAKN</small></a></li>
                                    <li><a href="#step-3">Step 3<br /><small>Approval</small></a></li>
                                    {{-- <li><a href="#step-4">Step 4<br /><small>Approval</small></a></li> --}}
                                </ul>
                                <div>
                                    <div id="step-1" class="">

                                        <textarea name="" id="" cols="30" rows="10">
                                            {{ $kontrak->isi }}
                                        </textarea>
                                    </div>
                                  
                                    <div id="step-2" class="">
                                        @if ($bakn->isi != NULL)
                                        <textarea name="" id="" cols="30" rows="10">
                                            {{ $bakn->isi }}
                                        </textarea>
                                        @else
                                            @include('modules.bakn_lkpp.inc.bakn_preview')
                                        @endif

                                    </div>
                                    <div id="step-3" class="">
                                        <h4>Coment and Approve</h4>
                                        <ul class="timeline">
                                            @foreach ($chats as $isi)
                                            <!-- timeline time label -->
                                            <li class="time-label">
                                                <span class="bg-green">
                                                    {{ date('d M.Y', strtotime($isi->created_at)) }}
                                                </span>
                                            </li>
                                            <!-- /.timeline-label -->
                                            <!-- timeline item -->
                                            <li>
                                                <!-- timeline icon -->
                                                <i class="fa fa-user bg-aqua"></i>
                                                <div class="timeline-item">
                                                    <span class="time">
                                                        <i class="fa fa-clock-o"></i>
                                                        {{ date('H:i:s', strtotime($isi->created_at)) .' - '. Carbon\Carbon::parse($isi->created_at)->diffForHumans()}}
                                                    </span>

                                                    <h3 class="timeline-header"><a
                                                            href="#">{{ $isi->jabatan.' - '.$isi->name }}</a></h3>

                                                    <div class="timeline-body">
                                                        {{ $isi->chat }}
                                                    </div>
                                                 
                                                    <div class="timeline-footer" hidden>
                                                        <p class="bold"> </p>
                                                    </div>
                                                </div>
                                            </li>
                                            <!-- END timeline item -->
                                            @endforeach
                                            <li>
                                                <i class="fa fa-clock-o bg-gray"></i>
                                            </li>
                                        </ul>
                                        <br>
                                        @if($kontrak->approval == Auth::user()->username)

                                        <div class="col-12 ">
                                            <form   action="{{ route('lkpp.kontrak.approve', $kontrak->id) }}"
                                                    class="form-group" 
                                                    method="post">
                                                @csrf
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Comment</label>
                                                            <input  type="text" 
                                                                    class="form-control" 
                                                                    name="chat"
                                                                    id="chat"
                                                                    required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" hidden>
                                                        <div class="form-group">
                                                            <label for="id">ID Transaksi</label>
                                                            <input type="text" class="form-control"
                                                                name="idTransaksi" value="{{ $kontrak->id }}"
                                                                readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 mar-paginate pad-0">
                                                        <button type="submit" 
                                                                class="btn btn-danger"
                                                                name="status" 
                                                                value="Return">
                                                                Return
                                                        </button>
                                                        <button type="submit" 
                                                                class="btn btn-primary"
                                                                name="status" 
                                                                id="submit" 
                                                                value="Approve">
                                                            Approve
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div> <!-- End div col-12-->
                                    @endif
                                </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('smartwizard/dist/js/jquery.smartWizard.min.js') }}"></script>
<!-- script function froala -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<script src="{{ asset('froala/js/languages/id.js') }}"></script>
<!-- External -->
<script type="text/javascript">
   $(document).ready(function () {
        $('div#smartwizard').smartWizard();
        $('div#smartwizardcircle').smartWizard();
    });

    $('textarea').froalaEditor({
        // fullPage: true,
        toolbarButtons      : ['print', 'html', 'getPDF', 'fullscreen'],
        charCounterCount    : false,
        key                 : keyFroala,
        height              : 500,
    })
    // removo alert lisensi froala
$("div > a", ".fr-wrapper").css('display', 'none');
</script>
@endsection