<p
	style='margin: 0cm 0cm 0.0001pt 17.1pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 115%;'>
	<span style="font-size:15px;line-height:115%;">&nbsp;</span><strong><span style="line-height: 1.5;">PERJANJIAN KERJA
			SAMA</span></strong></p>

<p
	style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 2;'>
	<span style="line-height: 1.5;"><strong>TENTANG</strong></span></p>

<p
	style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 2;'>
	<span style="line-height: 1.5;"><strong><span id="judulKontrak"
				style="font-size: 15px;">XXXXXXXXXXXX</span></strong>&nbsp;</span>
</p>

<p
	style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 2;'>
	<span style="line-height: 1.5;"><strong><span style="font-size: 15px;">PT. PINS
				INDONESIA</span></strong>&nbsp;</span>
</p>

<p
	style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 2;'>
	<span style="line-height: 1.5;"><strong><span
				style="font-size: 15px;">&nbsp;DENGAN&nbsp;</span></strong>&nbsp;</span>
</p>

<p
	style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 2;'>
	<span style="line-height: 1.5;"><strong><span class="mitraKontrak"
				style="font-size: 15px;">MITRA</span></strong>&nbsp;</span>
</p>

<p
	style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 1.5;'>
	<span style="line-height: 1.5;"><br></span></p>

<p
	style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 1.5;'>
	<span
		style="line-height: 1.5;"><strong>&nbsp;<u>_________________________________________________________________________________________________________________</u></strong></span>
</p>

<p
	style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 1.5;'>
	<span style="line-height: 1.5;"><br></span></p>

<p
	style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 1.5;'>
	<span style="line-height: 1.5;"><strong><u >Nomor : <span id="nomorKontrak"> XXXXXXXXXXXX</span></u></strong></span></p>

<p
	style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 1.5;'>
	<span style="line-height: 1.5;"><br></span></p>

<p
	style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 1.5;'>
	<span style="line-height: 1.5;"><br></span></p>

<p
	style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
	<span style="line-height: 1.5;">Pada hari ini, <b id="hariTanggalKontrak">{hariTanggalKontrak}</b>, tanggal <b
			id="tanggalTanggalKontrak">{tanggalKontrak}</b>, bulan <b
			id="bulanTanggalKontrak">{bulanTanggalKontrak}</b>, tahun <b
			id="tahunTanggalKontrak">{tahunTanggalKontrak}</b>, <span id="formatTanggalKontrak">(Y-m-d)</span> bertempat di Jakarta, oleh dan antara dua pihak -
		pihak:<br><br></span></p>

<ol style="list-style-type: upper-roman;">
	<li
		style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
		<span style="line-height: 1.5;"><strong>PT. PINS INDONESIA</strong>, perseroran terbatas yang didirikan
			berdasarkan hukum Negara Republik Indonesia, berkedudukan di <span id="alamatPins">{alamatPins}</span>,
			dalam perbuatan hukum ini diwakili secara sah oleh <strong class="namaApproval">{Nama}</strong>,
			Jabatan <span class="jabatanApproval">{Jabatan}</span>, selanjutnya disebut PINS;
			<br>
			<br>
		</span>
	</li>
	<li
		style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
		<span style="line-height: 1.5;"><strong class="mitraKontrak">{Mitra}</strong>, perusahaan yang didirikan
			berdasarkan hukum Negara Republik Indonesia, berkedudukan di <span id="alamatMitra">{alamatMitra}</span>,
			dalam perbuatan hukum ini diwakili secara sah oleh <strong class="namaDirektur">{NamaDirMitra}</strong>,
			Jabatan <strong>Direktur Utama</strong> selanjutnya disebut SUPPLIER.</span>
	</li>
</ol>

<p style="line-height: 1.5; text-align: justify;"><span style="line-height: 1.5;"><br></span></p>

<p style="text-align: justify; line-height: 1.5;"><span style="line-height: 1.5;"><span
			style='color: rgb(65, 65, 65); font-family: "Times New Roman", serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; display: inline !important;'><strong>PINS</strong>
			dan <strong>SUPPLIER</strong> apabila secara bersama - sama untuk selanjutnya disebut &quot;Para Pihak&quot;.</span></span>
</p>

<p style="text-align: justify; line-height: 1.5;"><span style="line-height: 1.5;"><br><span
			style='color: rgb(65, 65, 65); font-family: "Times New Roman", serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;'>Dengan
			terlebih dahulu mempertimbangkan hal - hal sebagai berikut :</span></span>
</p>

<ol style="list-style-type: lower-alpha;">
	<li style="line-height: 1.5; text-align: justify;"><span style="line-height: 1.5;"><span
				style='color: rgb(65, 65, 65); font-family: "Times New Roman", serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;'>Bahwa
				PINS bermasuk menjalin kerjasama dengan SUPPLIER dalam Pekerjaan <span
					class="judulSpph">{judulSPPH}</span>;</span>
		</span>
	</li>
	<li style="line-height: 1.5; text-align: justify;"><span style="line-height: 1.5;"><span
				style='color: rgb(65, 65, 65); font-family: "Times New Roman", serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;'>Bahwa
				PINS telah menyampaikan kepada SUPPLIER surat No. <span id="nomorSpph">{NoSPPHLKPP}</span> tanggal <span
					id="tanggalSpph">{tanggalSPPHLKPP}</span> Perihal Surat Permintaan Penawaran Harga;</span>
		</span>
	</li>
	<li style="line-height: 1.5; text-align: justify;"><span style="line-height: 1.5;"><span
				style='color: rgb(65, 65, 65); font-family: "Times New Roman", serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;'>Bahwa
				SUPPLIER telah menyampaikan kepada PINS surat No. <span id="nomorSPH">{NoSPHLKPP}</span> tanggal <span
					id="tanggalSPH">{tanggalSPH}</span> perihal Surat Penawaran Harga;</span>
		</span>
	</li>
	<li style="line-height: 1.5; text-align: justify;"><span style="line-height: 1.5;"><span
				style='color: rgb(65, 65, 65); font-family: "Times New Roman", serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;'>Bahwa
				Para Pihak telah melaksanakan rapat klarifikasi dan negosiasi pada tanggal <span
					id="tanggalBakn">{tanggalBAKN}</span> yang hasilnya dituangkan dalam Berita Acara Klarifikasi dan
				Negosiasi;</span>
		</span>
	</li>
</ol>

<p style="line-height: 1.5; text-align: justify;"><span
		style='color: rgb(65, 65, 65); font-family: "Times New Roman", serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; float: none; line-height: 1.5; display: inline !important;'>Setelah
		mempertimbangkan hal - hal tersebut di atas, Para Pihak sepakat untuk mengikatkan diri satu kepada yang lain dan
		dituangkan dalam Perjanjian Pekerjaan <span class="judulSpph">{judulSPPH}</span> (selanjutnya disebut &quot;Perjanjian&quot; ) dengan ketentuan
		dan syarat - syarat sebagai berikut :&nbsp;</span></p>

<p style="line-height: 1.5;">
	<br>
</p>

<p style="line-height: 1.5; text-align: center;">
	<div id="isiJenisKontrak" style="text-align: center;">
		<span style="font-family: Times New Roman,Times,serif,-webkit-standard;">{ISI PASAL}</span>
	</div>

<p style="line-height: 1.5; text-align: center;">
	<br>
</p>

<p style="line-height: 1.5; text-align: center;">
	<br>
</p>

<p style="line-height: 1.5; text-align: center;">
	<br>
</p>

<table align="center" border="1" cellpadding="0" cellspacing="0"
	style="border-collapse:collapse;border:none;margin-left:6.75pt;margin-right:6.75pt;margin-bottom:30mm" width="100%">
	<tbody>
		<tr>
			<td style="width: 50%;border-top: 1pt dotted windowtext;border-left: none;border-bottom: none;border-right: 1pt dotted windowtext;padding: 0cm 5.4pt;height: 98.35pt;vertical-align: top;"
				valign="top" width="50.892857142857146%">

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<span style="font-size:15px;"><span style="line-height: 1.5;"><span
								style='color: rgb(65, 65, 65); font-family: "Times New Roman", serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial; display: inline !important; float: none;'><strong>PT.
									PINS INDONESIA</strong></span></span>&nbsp;</span><strong><span
							style="font-size:15px;">,</span></strong></p>

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<strong><span style="font-size:15px;">&nbsp;</span></strong></p>

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<strong><span style="font-size:15px;">&nbsp;</span></strong></p>

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<strong><span style="font-size:15px;">&nbsp;</span></strong></p>

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<strong><span style="font-size:15px;">&nbsp;</span></strong></p>

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<strong><span style="font-size:15px;">&nbsp;</span></strong></p>

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<strong><span style="font-size:15px;">&nbsp;</span></strong></p>

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<u><strong class="namaApproval">{Nama}</strong></u></p>

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<strong class="jabatanApproval">{GM E-Commerce}</strong></p>
			</td>
			<td style="width: 50%;border-right: none;border-bottom: none;border-left: none;border-image: initial;border-top: 1pt dotted windowtext;padding: 0cm 5.4pt;height: 98.35pt;vertical-align: top;"
				valign="top" width="49.107142857142854%">

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<strong class="mitraKontrak">MITRA</strong></p>

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<strong><span style="font-size:15px;">&nbsp;</span></strong></p>

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<strong><span style="font-size:15px;">&nbsp;</span></strong></p>

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<strong><span style="font-size:15px;">&nbsp;</span></strong></p>

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<strong><span style="font-size:15px;">&nbsp;</span></strong></p>

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<strong><span style="font-size:15px;">&nbsp;</span></strong></p>

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<strong><span style="font-size:15px;">&nbsp;</span></strong></p>

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<u><strong class="namaDirektur">DIREKTUR</strong></u></p>

				<p
					style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
					<strong>Direktur Utama</strong></p>
			</td>
		</tr>
	</tbody>
</table>
<br>
<br>
<br>