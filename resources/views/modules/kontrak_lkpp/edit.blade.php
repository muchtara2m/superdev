@extends('layouts.master')

@section('title')
Edit Kontrak
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.4/select2.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css" />
<!-- Include Editor style. -->
<link rel="stylesheet" href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" />
<link rel="stylesheet" href="{{ asset('froala/css/froala_style.min.css') }}" />
<style>
    .bottomright {
      position: absolute;
      bottom: 0;
      right: 2mm;
      font-size: 18px;
    }
  @page {
  size            : A4;
  margin-top      : 3cm;
  margin-bottom   : 2cm;
  margin-left     : 30mm; 
  margin-right    : 20mm;
}
@media print {
    .bottomright{
    position: fixed;
    padding-top: 5%;
    bottom: 0;
  }
   body {
      margin-top      : 20mm; 
      margin-bottom   : 30mm; 
      margin-left     : 30mm; 
      margin-right    : 20mm;
      }
  }
    .capitalize {
  text-transform: capitalize;
}
.select2-container-multi .select2-choices .select2-search-choice {
    padding: 5px 5px 5px 18px;
}
</style>
@endsection


@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Kontrak
            <small>Edit</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        {{-- {{ dd($kontrak) }} --}}
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Edit</h3>
                    </div>
                    <!-- /.box-header -->
                    <br>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    
                    <!-- start form  -->
                    <form method="post" action="{{ route('lkpp.kontrak.update', $kontrak->id) }}}}" enctype="multipart/form-data" id="form">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="id" value="{{ $kontrak->id }}">
                        <input type="hidden" id="jenis_kontrak_sementara" data-old="{{ old('jenis_kontrak') }}" value="{{ $kontrak->jenis_kontrak }}">
                        <input type="hidden" id="url" data-url="{{ url('lkpp/kontrak/spph-bakn',$kontrak->id) }}">
                        <input type="hidden" id="url_name_approval" data-url="{{ url('lkpp/kontrak/name-approval/') }}">
                        
                        <div class="box-body">
                            <div class="form-group col-md-12 igroup">
                                <label for="nomorspph">Select SPPH</label>
                                <a id="spphpreview" target="_blank" href="#" hidden><i class="fa fa-fw fa-file-code-o"></i></a>
                               
                                        <input  class="form-control" 
                                        data-val="true" 
                                        placeholder="Pilih SPPH" 
                                        value="{{ old('nmrspph', $kontrak->bakn_lkpp->spph_lkpps['nomorspph']) }}" 
                                        name="nmrspph" 
                                        id="nospph" 
                                        type="text" >

                                <input  type="hidden" 
                                        name="bakn_id" 
                                        id="bakn_id" 
                                        value="{{ old('bakn_id', $kontrak->bakn_id) }}">
                            </div>
                            
                            <div class="form-group col-md-4 igroup">
                                <label for="tanggal_kontrak">Tanggal Kontrak</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input  type="text" 
                                            data-date="" 
                                            data-date-format="yyyy-mm-dd" 
                                            class="form-control datejos" 
                                            name="tanggal_kontrak" 
                                            id="tanggal_kontrak" 
                                            value="{{ old('tanggal_kontrak', $kontrak->tanggal_kontrak) }}"
                                            >
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="harga">No. Kontrak</label>                                
                                <input  type="text" 
                                        name="nomor_kontrak" 
                                        id="nomor_kontrak" 
                                        class="form-control"
                                        data-old="{{ old('nomor_kontrak', $kontrak->nomor_kontrak) }}"
                                        value="{{ old('nomor_kontrak', $kontrak->nomor_kontrak) }}">
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="jenis kontrak">Jenis Kontrak</label>
                                <select name="jenis_kontrak" class="form-control" id="jenisKontrak">
                                    <option value="">Pilih Jenis Kontrak</option>
                                </select>
                            </div>
                            
                            {{-- hasil pembahasan --}}
                            <div class="col-md-12" style="margin-top:5%">
                                <div class="form-group">
                                    <h4 class="divider-title center" style="text-align:center">PREVIEW</h4>
                                    <a href="/preview-status-bakn-lkpp/{{ $kontrak->bakn_id }}" target="_blank" id="previewBakn">
                                        <h4 class="divider-title center" style="text-align:center">BAKN</h4>
                                    </a>
                                    <hr>
                                </div>
                                <div class="form-group">
                                    <textarea   id="agenda" 
                                                rows="10" 
                                                cols="80" 
                                                name="isi" 
                                                class="bak">
                                        {{ old('isi', $kontrak->isi) }}    
                                    </textarea>
                                </div>
                                
                                <div class="form-group">
                                    <label for="">File Lampiran</label>
                                    <input type="file" name="lampiran[]" class="form-control" multiple="multiple">
                                    <td>
                                        @php
                                        $flampiran = json_decode($kontrak->lampiran, TRUE);
                                        $tlampiran = json_decode($kontrak->title_lampiran, TRUE);
                                        $i=1;
                                        @endphp
                                        @if($flampiran != NULL)
                                        @foreach ($tlampiran as $keylampiran =>$vallampiran)
                                        {{ $i++.'. ' }}<a href="{{ Storage::url($flampiran[$keylampiran]) }}">{{ $tlampiran[$keylampiran] }}</a><br>
                                        @endforeach
                                        @endif
                                    </td>                     
                                </div>
                                @if ($kontrak->hold == true)
                                    <button type="submit" 
                                            class="btn btn-warning"
                                            style="width: 7em;">
                                            Save
                                    </button>
                                @else
                                    <div class="form-group">
                                        <label for="">Chat</label><span>*require for submit </span>
                                        <input  type="text"  
                                                name="chat"
                                                class="form-control"
                                                value="{{ old('chat') }}">
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" 
                                            name="status" 
                                            value="draft_kontrak" 
                                            class="btn btn-primary" 
                                            style="width: 7em;">
                                            Save
                                        </button>
                                    <button type="submit" 
                                            name="status" 
                                            value="save_kontrak" 
                                            class="btn btn-success" 
                                            style="width: 7em;">
                                            Submit
                                        </button>
                                </div>
                                <!-- /.box-footer -->
                            @endif
                    </form>
                    {{-- end form --}}
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- clockpicker -->
<script src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/4.15.0/lodash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.4/select2.min.js"></script>
<!-- External JS -->
<script src="{{ asset('js/web/main/js/kontrak_lkpp/edit.js')}}"></script>
@endsection