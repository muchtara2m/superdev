@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Data Karyawan";
$crmenuname = "Master Data"
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
@endsection

@section('customstyle')
<style type="text/css">
  .form-horizontal .form-group {
    margin-right: unset;
    margin-left: unset;
  }
  .example-modal .modal {
    position: relative;
    top: auto;
    bottom: auto;
    right: auto;
    left: auto;
    display: block;
    z-index: 1;
  }
  .example-modal .modal {
    background: transparent !important;
  }
  .no-bullet {
    padding-left: 0;
    list-style-type: none;
  }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      {{ $crpagename }}
      <!-- <small>Form PBS</small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
      <li><a href="#">{{ $crmenuname }}</a></li>
      <li class="active">{{ $crpagename }} </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- right column -->
      <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-file-text"></i> Form Data Karyawan</h3>
            <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form method="POST" action="{{ route('user.store') }}" enctype="multipart/form-data" class="form">
            @csrf
            @method('POST')
            <div class="box-body">

              @if (count($errors) > 0)
              <div class="alert alert-danger">
                There was a problem, please check your form carefully.
                <ul>
                  @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif

              <div class="form-group col-md-6">
                <label>Nama*</label>
                <input name="name" type="text" class="form-control" value="{{ old('name') }}" autofocus required>
              </div>
              <div class="form-group col-md-6">
                <label>NIK*</label>
                <input name="username" type="text" class="form-control" value="{{ old('username') }}" required>
              </div>
              <div class="form-group col-md-6">
                <label>Email*</label>
                <input name="email" type="email" class="form-control" value="{{ old('email') }}" required>
              </div>
              <div class="form-group col-md-6">
                <label for="level">Level User*</label>
                <select name="level" id="level" class="form-control" required>
                  <option selected disabled>Pilih Level</option>
                  @if (count($roles) > 0)
                  @foreach ($roles as $role)
                      <option value="{{ $role->name }}" @if(old('level') == $role->name) {{ __('selected') }} @endif >{{ $role->display }}</option>
                  @endforeach
                  @endif
                </select>
              </div>
              {{-- <div class="form-group col-md-6">
                <label for="employee_status">Status Karyawan*</label>
                <select name="employee_status" id="employee_status" class="form-control" required>
                  <option selected disabled>Pilih Status</option>
                  <option value="Aktif" @php echo (old('employee_status') == 'Aktif') ? 'selected' : ''; @endphp>Aktif</option>
                  <option value="Non Aktif" @php echo (old('employee_status') == 'Non Aktif') ? 'selected' : ''; @endphp>Non Aktif</option>
                </select>
              </div> --}}
              <div class="form-group col-md-6">
                <label for="unit">Unit*</label>
                <select name="unit" id="unit" class="form-control" required>
                  <option selected disabled>Pilih Unit</option>
                  @if (count($units)>0)
                    @foreach ($units as $unit)
                      <option value="{{ $unit->id }}" @php echo (old('unit') == $unit->id) ? 'selected' : ''; @endphp >{{ $unit->nama }}</option>
                    @endforeach
                  @endif
                </select>
              </div>
              <div class="form-group col-md-6">
                <label for="position">Jabatan</label>
                <input name="position" id="position" type="text" class="form-control" value="{{ old('position') }}">
              </div>
              <div class="form-group col-md-6">
                <label for="band">Band</label>
                <input name="band" id="band" type="text" class="form-control" value="{{ old('band') }}">
              </div>
              <div class="form-group col-md-6">
                <label for="permanent_status">Status</label>
                <select name="permanent_status" id="permanent_status" class="form-control">
                  <option selected disabled>Pilih Status</option>
                  <option value="Organik">Organik</option>
                  <option value="Non Organik">Non Organik</option>
                </select>
              </div>
              {{-- <div class="form-group col-md-6">
                <label for="cost_center">Cost Center</label>
                <input name="cost_center" id="cost_center" type="text" class="form-control" value="{{ old('cost_center') }}">
              </div> --}}
              <div class="form-group col-md-6">
                <label for="phone">No. HP</label>
                <input name="phone" id="phone" type="text" class="form-control" value="{{ old('phone') }}" data-inputmask='"mask": "(+62) 999-9999-9999"' data-mask>
              </div>
              <div class="form-group col-md-6">
                <label for="telp">No. Telp</label>
                <input name="telp" id="telp" type="text" class="form-control" value="{{ old('telp') }}">
              </div>
              <input type="hidden" name="created_by" value="{{ Auth::id() }}">
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
            </div>
            <!-- /.box-footer -->
          </form>
        </div>
        <!-- /.box -->
      </div>
      <!--/.col (right) -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- clockpicker -->
<script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- InputMask -->
<script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('adminlte/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script>
  $(function () {
    $('[data-mask]').inputmask()
  })
</script>
@endsection
