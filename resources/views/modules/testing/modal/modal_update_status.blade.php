 <!-- modal -->
 <div class="modal fade" id="modal-update-status">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                 <h4 class="modal-title" id="judul">Update Status Dokumen </h4>
             </div>
             <div class="modal-body">
                 <form method="POST" id="update_status_ar_dua">
                    <input type="hidden" name="id_dokumen" id="id_dokumen">
                    <div class="form-group">
                        <label>Pilih Status</label>
                           <select class="form-control" id="update_dokumen" name="status_dokumen">
                               <option value="BELUM ADA DOKUMEN" disabled>PILIH</option>
                               <option value="bakn">BAKN</option>
                               <option value="kl">KL</option>
                               <option value="amandemen_kl">AMANDEMEN KL</option>
                               <option value="spk_mitra">SPK MITRA</option>
                               <option value="bakn_mitra">BAKN MITRA</option>
                               <option value="baso_cust">BASO CUST</option>
                               <option value="baut_cust">BAUT CUST</option>
                               <option value="baut_mitra">BAUT MITRA</option>
                               <option value="bast_cust">BAST CUST</option>
                               <option value="bast_mitra">BAST MITRA</option>
                               <option value="sid_ao">SID AO</option>
                               <option value="ba_rekon">BA REKON</option>
                               <option value="bapp">BAPP</option>
                               <option value="bapw">BAPW</option>
                               <option value="surat_segmen">SURAT SEGMEN</option>
                               <option value="lpl">LPL</option>
                               <option value="invoice">INVOICE</option>
                               <option value="billed">BILLED</option>
                               <option value="paid">PAID</option>
                           </select>
                    </div>
                    <div class="form-group">
                        <label for="">Nilai Project</label>
                        <div class="input-group">
                            <span class="input-group-addon">Rp</span>
                            <input type="text" class="form-control" name="nilai_project" id="nilai_project" onkeyup="formatAngka(this,'.')">
                          </div>
                    </div>
                    <div class="form-group">
                        <input type="button" value="Update" class="btn btn-success" id="update_status">
                    </div>
                    
                 </form>
                 
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
             </div>
         </div>
         <!-- /.modal-content -->
     </div>
     <!-- /.modal-dialog -->
 </div>
 <!-- /.modal -->