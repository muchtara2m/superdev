<!doctype html>
<html lang="en">

<head>
    <title>PINS</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
   
    <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <!-- WRAPPER -->
    <div class="wrapper">

        <header class="main-header">

            <!-- Logo -->
            <a href="index2.html" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>IoT</b></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">PINS&nbsp;<b>IoT</b></span>
            </a>

            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <!-- Navbar Right Menu -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="http://127.0.0.1:8000/img/user2-160x160.jpg" class="user-image"
                                    alt="User Image">
                                <span class="hidden-xs">Administrator</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="http://127.0.0.1:8000/img/user2-160x160.jpg" class="img-circle"
                                        alt="User Image">

                                    <p>
                                        Administrator
                                        <small>Member since Nov. 2012</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="http://127.0.0.1:8000/logout" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">
                                            Logout
                                            <form id="logout-form" action="http://127.0.0.1:8000/logout" method="POST"
                                                style="display: none;">
                                                <input type="hidden" name="_token"
                                                    value="hwpsxSJG4a36FNJuOwkpTQOEPdlD3OAtH7d2UhNw"> </form>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>

            </nav>
        </header> <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">MAIN NAVIGATION</li>

                    <li class="treeview ">
                        <a href="#">
                            <i class="fa fa-pie-chart"></i> <span>MASTER DATA</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class=>
                                <a href="data-submenu">
                                    <i class="fa fa-circle-o"></i>
                                    Data Submenu
                                </a>
                            </li>
                            <li class=>
                                <a href="data-role">
                                    <i class="fa fa-circle-o"></i>
                                    Role
                                </a>
                            </li>
                            <li class=>
                                <a href="data-menu">
                                    <i class="fa fa-circle-o"></i>
                                    Data Menu
                                </a>
                            </li>
                            <li class=>
                                <a href="data-user">
                                    <i class="fa fa-circle-o"></i>
                                    Data Users
                                </a>
                            </li>
                            <li class=>
                                <a href="rolemenu">
                                    <i class="fa fa-circle-o"></i>
                                    Role Menu
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="treeview active">
                        <a href="#">
                            <i class="fa fa-send"></i> <span>KL PKS</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class=active>
                                <a href="data-project">
                                    <i class="fa fa-circle-o"></i>
                                    Detail Project
                                </a>
                            </li>
                            <li class=>
                                <a href="data-mitra">
                                    <i class="fa fa-circle-o"></i>
                                    Detail Project Mitra
                                </a>
                            </li>
                            <li class=>
                                <a href="data-progress">
                                    <i class="fa fa-circle-o"></i>
                                    Detail Progress
                                </a>
                            </li>
                            <li class=>
                                <a href="data-pinsmitra">
                                    <i class="fa fa-circle-o"></i>
                                    Detail Progress PINS &amp; Mitra
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside> <!-- jQuery 3 -->
        <script src="http://127.0.0.1:8000/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Detail Progress
                </h1>
                <ol class="breadcrumb">
                    <li><a href="/"><i class="fa fa-pie-chart"></i> Home</a></li>
                    <li class="active">Detail Progress</li>
                </ol>
            </section>

            <section class="content">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#modalCreateProject" style="margin-bottom:15px;">
                                    Add Project
                                </button>
                                <table style="white-space: back-wrap;" id="dataTable1"
                                    class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>No Project</th>
                                            <th>Name Project</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Detail Info</th>
                                            <th>Date BAST</th>
                                            <th>KL Value</th>
                                            <th>Progress Info</th>
                                            <th>Progress Value</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Proj-0001</td>
                                            <td>Borneo Kalimantan</td>
                                            <td>2017-07-01</td>
                                            <td>2018-04-04</td>
                                            <td>Masih sirkulir gaes</td>
                                            <td>2019-05-09</td>
                                            <td>15.366.400.000.00</td>
                                            <td>100%</td>
                                            <td>15.366.400.000.00</td>
                                            <td>
                                                <a href="#" id="getModalEditValue" data-edit-id="1"
                                                    data-no_project="Proj-0001" data-project_name="Borneo Kalimantan"
                                                    data-start_date="2017-07-01" data-end_date="2018-04-04"
                                                    data-project_info="Masih sirkulir gaes" data-date_bast="2019-05-09"
                                                    data-kl_value="15366400000" data-progress_percent="100"
                                                    data-progress_value="15366400000" data-toggle="modal"
                                                    data-target="#modalEditProject" class="btn-xs btn-warning">
                                                    Edit</a>
                                                <a href="/delete-project/1" class="btn-xs btn-danger"
                                                    onclick="return confirm('Are you sure?');"><i
                                                        class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row">2</th>
                                            <td>Proj-0002</td>
                                            <td>Menteng</td>
                                            <td>2016-04-05</td>
                                            <td>2018-12-07</td>
                                            <td>BAST 1 masih sirkulir ttd Telkom</td>
                                            <td>2020-01-02</td>
                                            <td>20.957.300.000.00</td>
                                            <td>100%</td>
                                            <td>20.957.300.000.00</td>
                                            <td>
                                                <a href="#" id="getModalEditValue" data-edit-id="3"
                                                    data-no_project="Proj-0002" data-project_name="Menteng"
                                                    data-start_date="2016-04-05" data-end_date="2018-12-07"
                                                    data-project_info="BAST 1 masih sirkulir ttd Telkom"
                                                    data-date_bast="2020-01-02" data-kl_value="20957300000"
                                                    data-progress_percent="100" data-progress_value="20957300000"
                                                    data-toggle="modal" data-target="#modalEditProject"
                                                    class="btn-xs btn-warning">Edit</a>
                                                <a href="/delete-project/3" class="btn-xs btn-danger"
                                                    onclick="return confirm('Are you sure?');"><i
                                                        class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- /.content-wrapper -->


        <script>
            $('table a').on('click', function () {
                console.log($(this).data('edit-id'));
                $('#modal_edit_id').val($(this).data('id'));
                $('#modal_edit_no_project').val($(this).data('no_project'));
                $('#modal_edit_project_name').val($(this).data('project_name'));
                $('#modal_edit_start_date').val($(this).data('start_date'));
                $('#modal_edit_end_date').val($(this).data('end_date'));
                $('#modal_edit_project_info').val($(this).data('project_info'));
                $('#modal_edit_date_bast').val($(this).data('date_bast'));
                $('#modal_edit_kl_value').val($(this).data('kl_value'));
                $('#modal_edit_progress_percent').val($(this).data('progress_percent'));
                $('#modal_edit_progress_value').val($(this).data('progress_value'));
            });
        </script>
        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.0.0
            </div>
            <strong>Copyright &copy; 2019 <a href="#">ngodingKuy</a>.</strong> All rights
            reserved.
        </footer>

    </div>
    
    <script>
        $(function () {
            $('table').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>
    <script type="text/javascript">
        $(function () {
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            })
        });
    </script>
</body>