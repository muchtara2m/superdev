@extends('layouts.master')

@section('title')
Performansi | Super Slim
@endsection

@section('stylesheets')
<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
{{-- Icon --}}
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">

@endsection
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }

    tfoot {
        display: table-header-group;
    }

    .dt-buttons {
        padding-bottom: 5px;
    }

    #icon {
        text-align: center;
    }

    /* table thead th tbody td{
        :
    } */
    tbody tr {
        white-space: nowrap;
    }

    thead tr th small {
        color: red;
        font-size: 10px
    }

    h3 {
        padding-bottom: inherit;
    }
</style>
<!-- Chart code -->

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            PERFORMANSI
            <!-- <small>Form PBS</small> -->
        </h1>
        <br>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">General Support</a></li>
            <li class="active"> Performansi </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div>
        <br />
        @endif
        <div class="row">
            <div class="col-md-12" style="padding-bottom:2%">
                <div style="text-align:-webkit-center">
                    <select name="bulan" id="bulan" class="form-group" style="width:20%;height:34px" hidden>

                    </select>
                    <select name="tahun" id="tahun" class="form-group" style="width:30%;height:34px">

                    </select>
                    <br>

                </div>
            </div>
            {{-- Dashboard Gauge AR  --}}
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border" style="text-align:center">
                        <h3 class="box-title"><b>DASHBOARD PERFORMANSI</b></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                    class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body table-responsive" style="background:">
                        <div class="row" style="margin:1%">
                            <div class="box box-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Jumlah SPK/PKS MITRA</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                                class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                                class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12 table-responsive">
                                            <table id="jumlah_spk_pks" class="display">
                                                <thead class="text-center">
                                                    <tr style="white-space:nowrap">
                                                        <th rowspan="2">No</th>
                                                        <th rowspan="2">Unit</th>
                                                        <th rowspan="2">PBS</th>
                                                        <th colspan="5" style="text-align:center">Jumlah SPK/PKS MITRA
                                                        </th>
                                                        <th rowspan="2" style="text-align:center">Nilai COGS</th>
                                                        <th rowspan="2" style="text-align:center">Nilai SPK/PKS</th>
                                                        <th colspan="2" style="text-align:center">Efesiensi</th>

                                                        <th rowspan="2">KHS</th>
                                                    </tr>
                                                    <tr style="white-space:nowrap">
                                                        <th>Plan SPK/PKS</th>
                                                        <th>Selesai</th>
                                                        <th>Inprogress</th>
                                                        <th>Batal</th>
                                                        <th>Pending</th>
                                                        {{-- efesiensi --}}
                                                        <th>(Rp)</th>
                                                        <th>(%)</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center" style="white-space:nowrap">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.box-body -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12 table-responsive">
                                            <table id="jumlah_spk_pks" class="display">
                                                <thead class="text-center">
                                                    <tr style="white-space:nowrap">
                                                        <th rowspan="2">No</th>
                                                        <th rowspan="2">Unit</th>
                                                        <th rowspan="2">PBS</th>
                                                        <th colspan="5" style="text-align:center">Jumlah SPK/PKS MITRA
                                                        </th>
                                                        <th rowspan="2" style="text-align:center">Nilai COGS</th>
                                                        <th rowspan="2" style="text-align:center">Nilai SPK/PKS</th>
                                                        <th colspan="2" style="text-align:center">Efesiensi</th>

                                                        <th rowspan="2">KHS</th>
                                                    </tr>
                                                    <tr style="white-space:nowrap">
                                                        <th>Plan SPK/PKS</th>
                                                        <th>Selesai</th>
                                                        <th>Inprogress</th>
                                                        <th>Batal</th>
                                                        <th>Pending</th>
                                                        {{-- efesiensi --}}
                                                        <th>(Rp)</th>
                                                        <th>(%)</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center" style="white-space:nowrap">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /jumlah spk mitra -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>

            </div>
        </div>
    </section>
</div>

</section>
@endsection
@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script>
    url = "{{ url('data-pbs') }}"
    $(document).ready(function () {
        $('table').dataTable();

    });

    fetch(url).then((resp) => resp.json()) // Transform the data into json
        .then(function (data) {
            // Create and append the li's to the ul
            console.log(data);

        })
</script>
<!-- Chart code -->
{{-- <script src="{{ asset('js/chartjs/ds-ar.js') }}"></script> --}}




@endsection