@extends('layouts.master')

@section('title')
List Data AR | Super Slim
@endsection

@section('stylesheets')
<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
{{-- Icon --}}
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<!-- DataTables -->

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
{{-- <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.3/css/dataTables.responsive.css"> --}}
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }

    tfoot {
        display: table-header-group;
    }

    .dt-buttons {
        padding-bottom: 5px;
    }

    #icon {
        text-align: center;
    }

    /* table thead th tbody td{
        :
    } */
    tbody tr {
        white-space: nowrap;
    }

    thead tr th small {
        color: red;
        font-size: 10px
    }

    h3 {
        padding-bottom: inherit;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            LIST DATA
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">AR</a></li>
            <li class="active"> Unbill </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> List Data</h3>
                        <div style="text-align:center">
                            <div class="row input-daterange">
                                <div class="col-md-4">
                                    <select name="tahun" id="tahun" class="form-control">
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select name="bulan" id="bulan" class="form-control">
                                    </select>
                                </div>
                                <div class="col-md-4" hidden>
                                    <button type="button" name="filter" id="filter"
                                        class="btn btn-primary">Filter</button>
                                    <button type="button" name="refresh" id="refresh"
                                        class="btn btn-default">Refresh</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="ar" class="display">
                            {{-- @csrf --}}
                            <thead>
                                <tr style="white-space: nowrap">
                                    <th>No</th>
                                    <th>Customer</th>
                                    <th>Group Template Mitra</th>
                                    <th>Akun</th>
                                    <th>SO</th>
                                    <th>No. Dokumen</th>
                                    <th>Uraian</th>
                                    <th>Project</th>
                                    <th>IO</th>
                                    <th>Posting Date</th>
                                    <th>Amount</th>
                                    <th>PIC</th>
                                    <th>UBIS</th>
                                    <th>Update</th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@include('modules.testing.modal.modal_update_status')
<div id="list_records">
    
</div>
@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/ajax/list_data_ar.js') }}"></script>
<script>
    var url_data = "{{ url('data-new-ar') }}";
    var token = $("input[name='_token']").val();
    
    dataAr();
    $('#tahun').change(function () {
        var bln = $('#bulan').val();
        var thn = $('#tahun').val();
        dataAr(thn,bln);
    });
    $('#bulan').change(function () {
        var bln = $('#bulan').val();
        var thn = $('#tahun').val();
        dataAr(thn,bln);
    });


    $('#ar').on('click', '#update_status', function () {
        let idnya = $(this).data('id');
        let dokumen = $(this).data('dok');
        let nilai = $(this).data('nilai');
        $('#id_dokumen').val(idnya);
        $("#update_dokumen").val(dokumen);
        $('#nilai_project').val(nilai)
        // console.log(idnya, dokumen, nilai);
    });
    $('#update_status').on('click', function () {
        let dokumen = $('#update_dokumen').val();
        let nilai = $('#nilai_project').val();
        let id = $('#id_dokumen').val();
        let data = $('#update_status_ar_dua').serialize();
        // console.log(dokumen,nilai,id);
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        $.ajax({
            url: 'update-dokumen',
            type: 'post',
            data: {
                _token:token,
                dokumen_status:dokumen,
                id_dokumen: id,
                nilai_project: clearDot(nilai),
                },
            success: function(response){
                alert(response);
                dataAr(thn,bln);
            }
        });
    })

    function formatAngka(objek, separator) {
        a = objek.value;
        b = a.replace(/[^\d]/g, "");
        c = "";
        panjang = b.length;
        j = 0;
        for (i = panjang; i > 0; i--) {
            j = j + 1;
            if (((j % 3) == 1) && (j != 1)) {
                c = b.substr(i - 1, 1) + separator + c;
            } else {
                c = b.substr(i - 1, 1) + c;
                // formatAngka
            }
        }
        objek.value = c;
    }

    function clearDot(number) {
        output = number.split(".").join("")
        return output;
    }
</script>

@endsection