@extends('layouts.master')


@section('title')
Edit BAKN
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet"
href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
{{--  Tagify  --}}
<link rel="stylesheet" href="{{ asset('tagify/dist/tagify.css') }}">
@endsection

@section('customstyle')

<style type="text/css">
    table tr:not(:first-child) {
        cursor: pointer;
        transition: all .25s ease-in-out;
    }
    
    table tr:not(:first-child):hover {
        background-color: #ddd;
    }
    
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    
    .example-modal .modal {
        background: transparent !important;
    }
    
    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }
    
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAKN
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">BAKN</a></li>
            <li class="active"> Edit </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Edit BAKN</h3>
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            There was a problem, please check your form carefully.
                            <ul>
                                @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        @if ($chat != NULL)
                        <div class="form-group col-md-12" style="margin-top:2%">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Comment From {{ strtoupper($chat->username) }}</label>
                                <textarea   name="" 
                                            id="chat" 
                                            cols="30" 
                                            rows="10">
                                    {{ $chat->chat }}
                                </textarea>
                            </div>
                        </div>
                        @endif
                    </div>
                    
                    <!-- /.box-header -->
                    {{-- start form  --}}
                    <form   method="post" 
                            action="/update-bakn-lkpp/{{$bakns->id}}" 
                            enctype="multipart/form-data"
                            id="form">
                    @csrf
                    
                    <input  type="hidden" 
                            name="spphid" 
                            id="spphid"
                            value="{{ old('spphid', $bakns->spph_id) }}">

                    <div class="box-body">
                        <div class="form-group col-md-12 igroup">
                            <label for="nomorspph">Select SPPH</label>
                            <a  id="spphpreview" 
                                target="_blank"
                                href="#">
                                <i class="fa fa-fw fa-file-code-o"></i>
                            </a>
                            <div class="input-group col-md-12">
                                <select name="nmrspph" 
                                        id="nospph" 
                                        class="form-control">
                                        @if (!old('nmrspph'))
                                            <option value="{{ $bakns->spph_lkpps['nomorspph'] }}" disabled selected>{{ $bakns->spph_lkpps['nomorspph'] }}</option>                                            
                                        @endif
                                    @foreach ($spph as $item)
                                        <option value="{{ $item->nomorspph }}" {{  ($item->nomorspph == old('nmrspph')) ? 'selected' : ''}}>{{ $item->nomorspph.' - '.$item->judul }}</option>        
                                    @endforeach
                                </select>
                            </div>
                        </div>
                            
                        <div class="form-group col-md-4 igroup">
                            <label for="tglbakn">Tanggal BAKN</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input  type="text" 
                                        data-date="" 
                                        data-date-format="yyyy-mm-dd"
                                        class="form-control datejos" 
                                        name="tglbakn" 
                                        id="enddelivery-date"
                                        value="{{ old('tglbakn', $bakns->tglbakn) }}">
                            </div>
                        </div>
                        <div class="form-group col-md-4 igroup">
                            <label for="harga">Harga</label>
                            <div class="input-group col-md-12">
                                <span class="input-group-addon">Rp</span>
                                <input  type="text" 
                                        class="form-control" 
                                        name="harga" 
                                        onkeyup="formatAngka(this,'.')"
                                        placeholder="1.000.000" 
                                        id="harganum"
                                        value="{{ old('harga', number_format($bakns->harga,0,'.','.') ) }}">
                            </div>
                        </div>
                        <div class="form-group col-md-4 igroup">
                            <label for="pimpinanrapat">Pimpinan Rapat</label>
                            <div class="input-group col-md-12">
                                <select name="pimpinan_rapat" 
                                        id="pimpinan" 
                                        class="form-control" 
                                        title="Pimpinan">
                                        <option selected value="{{ $bakns->pimpinan_rapat }}">{{ $bakns->pimpinan_rapat }}</option>
                                        <option value="Revi Guspa">VP General Support</option>
                                        <option value="Sigit Sumarsono">GM Regional Timur</option>
                                        <option value="Hernadi Yoga Adhitya Tama">GM E-Commerce</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group col-md-4 igroup">
                            <label for="noio">No IO</label>
                            <div class="input-group col-md-12">
                                <input  type="hidden" 
                                        name="io_id" 
                                        id="io_id" 
                                        value="{{ old('io_id',$bakns->io_id) }}">
                                <input  type="text" 
                                        name="noio" 
                                        id="noio" 
                                        class="form-control" 
                                        title="Nomor IO"
                                        readonly 
                                        value="{{ old('noio', $bakns->io_id) }}">
                            </div>
                        </div>
                        <div class="form-group col-md-4 igroup">
                            <label for="desio">Deskripsi IO</label>
                            <div class="input-group col-md-12">
                                <input  type="text" 
                                        name="desio" 
                                        id="desio" 
                                        class="form-control" 
                                        title="Deskripsi"
                                        data-toggle="modal" 
                                        data-target="#modal-unit"
                                        value="{{ old('desio', $bakns->io_lkpp['deskripsi']) }}" 
                                        readonly 
                                        placeholder="Pilih IO">
                            </div>
                        </div>
                                    
                        <div class="form-group col-md-4 igroup">
                            <label class="control-label">Tipe Undangan</label>
                            <div class="col-md-6">
                                <ul class="no-bullet">
                                    <li>
                                        <input  type="checkbox" 
                                                name="tipe_undangan[]" 
                                                value="Review" 
                                                id="invit1"
                                                {{ in_array("Review",$checkbox)? "checked":"" }}>
                                        <label for="minimal-checkbox-1">Review</label>
                                    </li>
                                    <li>
                                        <input  type="checkbox" 
                                                name="tipe_undangan[]" 
                                                value="Coordination"
                                                id="invit2" 
                                                {{ in_array("Coordination",$checkbox)? "checked":"" }}>
                                        <label for="minimal-checkbox-2">Coordination</label>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul class="no-bullet">
                                    <li>
                                        <input  type="checkbox" 
                                                name="tipe_undangan[]" 
                                                value="Briefing" 
                                                id="invit3"
                                                {{ in_array("Briefing",$checkbox)? "checked":"" }}>
                                        <label for="minimal-checkbox-disabled">Briefing</label>
                                    </li>
                                    <li>
                                        <input  type="checkbox" 
                                                name="tipe_undangan[]" 
                                                value="Decision Marking"
                                                id="invit4" 
                                                {{ in_array("Decision Marking",$checkbox)? "checked":"" }}>
                                        <label for="minimal-checkbox-disabled-checked">DecisionMaking</label>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="pesertarapat">Peserta Rapat</label>
                            <div class="form-group">
                                <div class="input-group-addon select-addon">
                                    <span>PT PINS INDONESIA</span>
                                </div>
                                <input  name="peserta_pins" 
                                        class=" input-group tagify" 
                                        id="tags-pins"
                                        placeholder="Pilih Peserta" 
                                        value="{{ old('peserta_pins', $bakns->peserta_pins) }}">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="pesertarapat">Peserta Rapat</label>
                            <div class="form-group" id="mtr">
                                <div class="input-group-addon select-addon">
                                    <span id="mitra">MITRA</span>
                                </div>
                                <input  name="peserta_mitra" 
                                        class=" input-group form-control" 
                                        id="direktur"
                                        placeholder="Pilih Peserta" 
                                        value="{{ old('peserta_mitra', $bakns->peserta_mitra) }}">
                            </div>
                        </div>
                                        
                                        {{-- hasil pembahasan --}}
                        <div class="col-md-12">
                            <div class="form-group">
                                <h4 class="divider-title">HASIL PEMBAHASAN</h4>
                                <hr>
                            </div>
                            <div class="form-group">
                                <label for="agenda">Agenda</label>
                                <textarea   id="agenda" 
                                            rows="10" 
                                            cols="80" 
                                            name="agenda" 
                                            placeholder="Type Something"
                                            class="bakn">{{ old('agenda', $bakns->agenda) }}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="dasarpembahasan">Dasar Pembahasan</label>
                                <textarea   rows="10" 
                                            cols="80" 
                                            id="dasarpembahasan" 
                                            name="dasar_pembahasan"
                                            placeholder="Type Something" 
                                            class="bakn">{{ old('dasarpembahasan', $bakns->dasar_pembahasan) }}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="ruanglingkip">Ruang Lingkup</label>
                                <textarea   id="ruanglingkup" 
                                            rows="10" 
                                            cols="80" 
                                            name="ruang_lingkup"
                                            placeholder="Type Something" 
                                            class="bakn">{{ old('ruang_lingkup', $bakns->ruang_lingkup) }}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="lokasipekerjaan">Lokasi Pekerjaan/Pengiriman Barang/Jasa</label>
                                <textarea   id="cke-lokasi-pekerjaan" 
                                            rows="10" 
                                            cols="80" 
                                            name="lokasi_pekerjaan"
                                            placeholder="Type Something" 
                                            class="bakn">{{ old('lokasi_pekerjaan', $bakns->lokasi_pekerjaan) }}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="jangkawaktu">Jangka waktu Pengiriman Barang</label>
                                <textarea   id="cke-jangka-waktu" 
                                            rows="10" 
                                            cols="80" 
                                            name="jangka_waktu"
                                            placeholder="Type Something" 
                                            class="bakn">{{ old('jangka_waktu', $bakns->jangka_waktu) }}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="harga">Harga</label>
                                <textarea   id="nilai" 
                                            name="harga_terbilang" 
                                            class="bakn">{{ old('harga_terbilang', $bakns->harga_terbilang) }}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="tatacara">Tata Cara Pembayaran</label>
                                @foreach ($bayar as $item)
                                <label>
                                    <input  type="checkbox" 
                                            value="{{ $item->isi }}"
                                            class="carabayar" 
                                            onchange="add()"> {{ $item->jenis }}
                                </label>
                                @endforeach
                                <textarea   rows="10" 
                                            cols="80" 
                                            name="cara_bayar" 
                                            class="bakn" 
                                            id="tatacara">{{ old('cara_bayar', $bakns->cara_bayar) }}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Lain - lain</label>
                                <textarea   rows="10" 
                                            cols="80" 
                                            class="bakn" 
                                            name="lain_lain">{{ old('lain_lain', $bakns->lain_lain) }}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="">File Lampiran</label>
                                <input  type="file" 
                                        name="lampiran[]" 
                                        id="" 
                                        class="form-control" 
                                        multiple="multiple">
                                @php
                                if($bakns->lampiran == NULL){
                                }else{
                                    $title = json_decode($bakns->title_lampiran, TRUE);
                                    $file = json_decode($bakns->lampiran, TRUE);
                                    $i=1;
                                    foreach ($title as $key => $value) {
                                        echo $i++.'. <a
                                        href="'.Storage::url($file[$key]).'">'.$title[$key].'</a><br>';
                                    }
                                }
                                @endphp   
                            </div>
                            <div class="form-group">
                                <label for="">Chat</label><span>*require for submit </span>
                                <input  type="text" 
                                        name="chat" 
                                        class="form-control"
                                        value="{{ old('chat') }}">
                            </div>
                        </div>
                        {{-- end hasil pembahasan --}}
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" 
                            name="status" 
                            value="draft_bakn" 
                            class="btn btn-primary"
                            style="width: 7em;">
                            <i class="fa fa-check"></i> 
                            Save
                    </button>
                    <button type="submit" 
                            name="status" 
                            value="save_bakn" 
                            class="btn btn-success" 
                            style="width: 7em;">
                            <i class="fa fa-check"></i> 
                            Submit
                    </button>
                    @if (Auth::user()->level == 'administrator')
                    <button type="submit" 
                            name="status" 
                            value="save_admin" 
                            class="btn bg-navy" 
                            style="width: 7em;">
                            Save Admin 
                    </button>
                    @endif
                </div>
                    <!-- /.box-footer -->
            </form>
            {{-- end form --}}
        </div>
        <!-- /.box -->
        @include('modules.bakn_lkpp.modal.io_modal')
        @include('modules.bakn_lkpp.modal.spph_modal')
    </div>
    <!--/.col (right) -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<!-- Data Table -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- clockpicker -->
<script src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Tagify -->
<script src="{{ asset('tagify/dist/jQuery.tagify.min.js') }}"></script>
<script src="{{ asset('tagify/dist/tagify.min.js') }}"></script>
<script src="{{ asset('js/web/main/js/bakn_lkpp/edit.js')}}"></script>
<script>
    
    $('#chat').froalaEditor({
        key: '{{ env("KEY_FROALA") }}',
        height: 70,
        toolbarButtons: ['help'],
        charCounterCount: false
    });
    
</script>
@endsection
