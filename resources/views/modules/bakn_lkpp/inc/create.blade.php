<table border="1" cellpadding="0" cellspacing="0" style="width: 100%;">
    <tbody>
        <tr>
            <td rowspan="4" style="width: 25.9259%; padding: 0cm 5.4pt; height: 71.5pt; vertical-align: top;"
                valign="top" width="28.52760736196319%"><img data-fr-image-pasted="true"
                src="{{ asset('images/pinlogo.png') }}" width="190" class="fr-fic fr-dii"
                    style="width: 317px;">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;margin-left:-5.4pt;'>
                    <br>
                </p>
            </td>
            <td colspan="3" style="width: 73.9985%; padding: 0cm 5.4pt; height: 71.5pt; vertical-align: top;"
                valign="top" width="71.47239263803681%">

                <p
                    style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 1.5;'>
                    <strong><span style="font-size:15px;">BERITA ACARA KLARIFIKASI &amp; NEGOSIASI</span></strong></p>

                <p
                    style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 1.5;'>
                    <strong><span style="font-size:15px;" id="judulUtama">{judulSpph}&nbsp;</span></strong></p>

                <p
                    style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 1.5;'>
                    <strong><span style="font-size:15px;">ANTARA &nbsp;</span></strong></p>

                <p
                    style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 1.5;'>
                    <strong><span style="font-size:15px;">PT. PINS INDONESIA</span></strong></p>

                <p
                    style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 1.5;'>
                    <strong><span style="font-size:15px;">&nbsp;DENGAN&nbsp;</span></strong></p>

                <p
                    style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center; line-height: 1.5;'>
                    <strong><span style="font-size:15px;" id="mitraUtama">{namaMitra}</span></strong></p>
            </td>
        </tr>
        <tr>
            <td style="width: 34.316%; padding: 0cm 5.4pt; vertical-align: middle;" valign="top"
                width="36.05150214592275%">

                <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span
                        style="font-size:15px;">Tanggal</span></p>
            </td>
            <td colspan="2" style="width: 39.3802%; padding: 0cm 5.4pt; vertical-align: middle;" valign="top"
                width="63.94849785407725%">

                <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span
                        style="font-size:15px;" id="tanggalBakn">{tanggalBakn}</span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 34.316%; padding: 0cm 5.4pt; height: 3.2pt; vertical-align: middle;" valign="top"
                width="36.05150214592275%">

                <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span
                        style="font-size:15px;">Waktu</span></p>
            </td>
            <td colspan="2" style="width: 39.3802%; padding: 0cm 5.4pt; height: 3.2pt; vertical-align: middle;"
                valign="top" width="63.94849785407725%">

                <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span
                        style="font-size:15px;">10.30&nbsp;WIB sampai dengan&nbsp;selesai</span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 34.316%; padding: 0cm 5.4pt; height: 3.2pt; vertical-align: middle;" valign="top"
                width="36.05150214592275%">

                <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span
                        style="font-size:15px;">Tempat</span></p>
            </td>
            <td colspan="2" style="width: 39.3802%; padding: 0cm 5.4pt; height: 3.2pt; vertical-align: middle;"
                valign="top" width="63.94849785407725%">

                <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span
                        style="font-size:15px;">Kantor PT. PINS Indonesia</span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 25.9259%; padding: 0cm 5.4pt; vertical-align: middle;" valign="top"
                width="28.52760736196319%">

                <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span
                        style="font-size:15px;">Undangan dari</span></p>
            </td>
            <td colspan="3" style="width: 73.9985%; padding: 0cm 5.4pt; vertical-align: top;" valign="top"
                width="71.47239263803681%">

                <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span
                        style="font-size:15px;">PT. PINS Indonesia</span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 25.9259%; padding: 0cm 5.4pt; height: 3.2pt; vertical-align: middle;" valign="top"
                width="28.52760736196319%">

                <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span
                        style="font-size:15px;">Tipe Rapat</span></p>
            </td>
            <td colspan="3" style="width: 73.9985%; padding: 0cm 5.4pt; height: 3.2pt; vertical-align: bottom;"
                valign="top" width="71.47239263803681%">

                <ul class="icheck-list">
                    <li style="display: inline;"><span
                            style="font-family: Times New Roman,Times,serif,-webkit-standard;"><input type="checkbox"
                                class="check" id="minimal-checkbox-1" checked="" value="on">&nbsp;<label
                                for="minimal-checkbox-1">Review&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                &nbsp;</label></span></li>
                    <li style="display: inline;"><span
                            style="font-family: Times New Roman,Times,serif,-webkit-standard;"><input type="checkbox"
                                class="check" id="minimal-checkbox-2" checked="" value="on">&nbsp;<label
                                for="minimal-checkbox-2">Coordination&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                &nbsp;&nbsp;</label></span></li>
                    <li style="display: inline;"><span
                            style="font-family: Times New Roman,Times,serif,-webkit-standard;"><input type="checkbox"
                                class="check" id="minimal-checkbox-3" value="on">&nbsp;<label
                                for="minimal-checkbox-disabled">Briefing.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                &nbsp;</label></span></li>
                    <li style="display: inline;"><span
                            style="font-family: Times New Roman,Times,serif,-webkit-standard;"><input type="checkbox"
                                class="check" id="minimal-checkbox-4" checked="" value="on"></span>
                        <label for="minimal-checkbox-disabled-checked"><span
                                style="font-family: Times New Roman,Times,serif,-webkit-standard;">Decision
                                Making</span></label>
                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td style="width: 25.9259%; padding: 0cm 5.4pt; height: 3.2pt; vertical-align: top;" valign="top"
                width="28.52760736196319%">

                <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span
                        style="font-size:15px;">Pimpinan Rapat</span></p>
            </td>
            <td colspan="3" style="width: 73.9985%; padding: 0cm 5.4pt; height: 3.2pt; vertical-align: top;"
                valign="top" width="71.47239263803681%">

                <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span
                        style="font-size:15px;">GM e-Commerce</span></p>
            </td>
        </tr>
        <tr>
            <td style="width: 25.9259%; padding: 0cm 5.4pt; height: 24.7pt; vertical-align: top;" valign="top"
                width="28.52760736196319%">

                <p style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span
                        style="font-size:15px;">Peserta</span></p>
            </td>
            <td colspan="2" style="width: 34.4671%; padding: 0cm 5.4pt; height: 24.7pt; vertical-align: top;"
                valign="top" width="34.04907975460123%">

                <p
                    style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; line-height: 1.5;'>
                    <span style="font-size:15px;">Supplier :</span></p>

                <ol start="1" style="margin-bottom:0cm;margin-top:0cm;" type="1">
                    <li
                        style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; line-height: 1.5;'>
                        <span style="font-size:15px;" class="direkturMitra">{direkturMitra}</span></li>
                </ol>
            </td>
            <td style="width: 40.2872%; padding: 0cm 5.4pt; height: 24.7pt; vertical-align: top;" valign="top"
                width="37.423312883435585%">

                <p
                    style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; line-height: 1.5;'>
                    <span style="font-size:15px;">PT. PINS Indonesia :</span></p>

                <ol>
                    <li
                        style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; line-height: 1.5;'>
                        <span style="font-size:15px;" class="getNamaGmEcom">{nama GM Ecom}</span></li>
                    <li
                        style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; line-height: 1.5;'>
                        <span style="font-size:15px;">Lucas Zylgwyn</span></li>
                    <li
                        style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; line-height: 1.5;'>
                        <span style="font-size:15px;">{{ Auth::user()->name }}</span></li>
                </ol>
            </td>
        </tr>
        <tr>
            <td colspan="4"
                style="width: 489.05pt;background: rgb(160, 160, 160);padding: 0cm 5.4pt;vertical-align: top;"
                valign="top" width="100%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                    <strong><span style="font-size:15px;color:black;">AGENDA</span></strong></p>
            </td>
        </tr>
        <tr>
            <td colspan="4"
                style="width: 489.05pt; padding: 0cm 5.4pt; height: 11.2pt; vertical-align: top; text-align: justify;"
                valign="top" width="100%">

                <p
                    style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                    <span style="font-size:15px;">Klarifikasi</span><span style="font-size:11px;">&nbsp;</span><span
                        style="font-size:15px;">dan</span><span style="font-size:11px;">&nbsp;</span><span
                        style="font-size:15px;">Negosiasi tentang</span><span style="font-size:11px;">&nbsp;</span>
                        <span style="font-size:15px;" class="judulSpphKedua">{judulSpph}}</span></p>
            </td>
        </tr>
        <tr>
            <td colspan="4"
                style="width: 489.05pt;background: rgb(166, 166, 166);padding: 0cm 5.4pt;height: 6.25pt;vertical-align: top;"
                valign="top" width="100%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                    <strong><span style="font-size:15px;color:black;">DASAR KLARIFIKASI</span></strong></p>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="width: 489.05pt;padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="100%">

                <ol style="margin-bottom:0cm;">
                    <li style="text-align: justify; line-height: 1.5;"><span
                            style='font-size: 15px; font-family: "Times New Roman", Times, serif, -webkit-standard;'>Surat
                            Permintaan Penawaran Harga (SPPH) nomor:</span>
                            <span style="font-family: Times New Roman,Times,serif,-webkit-standard;">
                            <span style="font-size:15px;" id="nomorSpph">{nomorSpph} </span> tanggal
                            <span style="font-size:15px;" id="tanggalSpph">{tanggal SPPH}</span>
                            <span style="font-size:15px;"> perihal Permintaan Penawaran Harga.</span>
                    </li>
                    <li style="text-align: justify; line-height: 1.5;"><span
                            style="font-family: Times New Roman,Times,serif,-webkit-standard;"><span
                                style="font-size:15px;">Surat dari Supplier tanggal </span><span style="font-size:15px;" id="tanggalSph">{tanggal SPH}&nbsp;</span></span><span
                            style='font-size: 15px; font-family: "Times New Roman", Times, serif, -webkit-standard;'> perihal
                            Penawaran Harga.</span></li>
                </ol>
            </td>
        </tr>
        <tr>
            <td colspan="4"
                style="width: 489.05pt;background: rgb(160, 160, 160);padding: 0cm 5.4pt;vertical-align: top;"
                valign="top" width="100%">

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                    <strong><span style="font-size:15px;color:black;">NEGOSIASI</span></strong></p>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="width: 489.05pt;padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="100%">

                <p
                    style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                    <span style="font-size:15px;">PT. PINS Indonesia dan Supplier melakukan klarifikasi dan negosiasi
                        atas kesepakatan sebagai berikut:</span></p>

                <ol>
                    <li
                        style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                        <strong><span style="font-size:15px;">Ruang Lingkup</span></strong><span
                            style="font-size:15px;">&nbsp;<strong>Pekerjaan</strong>:</span>
                        <br><span style="font-size:15px;" class="judulSpphKedua">{judulSpph}</span></li>
                    <li
                        style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                        <strong><span style="font-size:15px;">Lokasi Pekerjaan&nbsp;</span></strong><span
                            style="font-size:15px;">:&nbsp;</span>
                        <br><span style="font-size:15px;" class="fr-class-highlighted">Barang diserahkan ke PT. PINS Indonesia Area Jawa Barat, Jl.
                            Mangga No. 4 Kelurahan Cihapit, Kecamatan Bandung Wetan, Kota Bandung 40114.</span></li>
                    <li
                        style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                        <strong><span style="font-size:15px;">Jangka Waktu Pelaksanaan Pekerjaan :&nbsp;</span></strong>
                        <br><span style="font-size:15px;">Penyerahan barang selambat-lambatnya sampai dengan tanggal <span class="fr-class-highlighted">28
                            April 2020 </span>yang dibuktikan dengan Berita Acara Serah Terima Barang (BASTB).</span></li>
                    <li
                        style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                        <strong><span style="font-size:15px;">Harga :</span></strong>
                        <br><span style="font-size:15px;">Total harga yang sesuai dengan ruang lingkup pekerjaan
                            tersebut diatas adalah sebesar <strong class="spellHarga">{spellHarga}</strong> sudah termasuk PPN 10% (sepuluh
                            persen), dengan rincian sebagai berikut :</span></li>
                    <li
                        style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'>
                        <strong><span style="font-size:15px;">Tatacara Pembayaran :</span></strong>
                        <div id="isiCaraBayar">
                            <ol style="list-style-type: lower-alpha;" >
                                <li
                                    style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                                    <span style="font-size:15px;">PT. PINS Indonesia akan melaksanakan pembayaran kepada
                                        Supplier secara sekaligus sebesar 100% (seratus persen) dari nilai total berdasarkan
                                        Berita Acara Klarifikasi dan Negosiasi (BAKN) terkait atau sebesar <strong>Rp.
                                            200.295.210,- (Dua Ratus Juta Dua Ratus Sembilan Puluh Lima Ribu Dua Ratus
                                            Sepuluh Rupiah)</strong> sudah termasuk PPN 10% (sepuluh persen).</span></li>
                                <li
                                    style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'>
                                    <span style="font-size:15px;">PT PINS Indonesia akan melakukan pembayaran
                                        selambat-lambatnya 14 (empat belas) hari kerja setelah Supplier menyerahkan:</span>
    
                                    <ol>
                                        <li
                                            style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                                            <span style="font-size:15px;">Lembar asli <strong>Berita Acara Uji Terima
                                                    (BAUT)</strong> dan lampiran dokumen surat jalan (Delivery Order) yang
                                                ditandatangani oleh PT. PINS Indonesia (diketahui MGR Catalogue &amp;
                                                Partnership Management) dan Supplier;</span></li>
                                        <li
                                            style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                                            <span style="font-size:15px;">Lembar asli <strong>Berita Acara Serah Terima
                                                    Barang (BASTB)</strong> yang ditandatangani oleh <br>&nbsp;PT. PINS
                                                Indonesia (diketahui GM e-Commerce) dan Supplier;</span></li>
                                        <li
                                            style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'>
                                            <span style="font-size:15px;">Dokumen tagihan dilengkapi dengan :&nbsp;</span>
    
                                            <ol>
                                                <li
                                                    style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                                                    <span style="font-size:15px;">Surat permohonan bayar;</span></li>
                                                <li
                                                    style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                                                    <span style="font-size:15px;">Kuitansi bermaterai;</span></li>
                                                <li
                                                    style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                                                    <span style="font-size:15px;">Invoice;</span></li>
                                                <li
                                                    style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                                                    <span style="font-size:15px;">Faktur pajak (lembar pertama asli dan copy
                                                        (dengan melampirkan Surat Keterangan Pemberian Nomor Seri Faktur
                                                        Pajak dari Direktorat Jenderal Pajak) serta copy Surat Setoran Pajak
                                                        (SSP);, dan</span></li>
                                                <li
                                                    style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                                                    <span style="font-size:15px;">Copy Perjanjian Kerja Sama/
                                                        Kontrak,</span></li>
                                            </ol>
                                        </li>
                                    </ol>
                                </li>
                                <li
                                    style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                                    <span style="font-size:15px;">PT. PINS Indonesia akan melakukan pembayaran kepada
                                        Supplier selambat-lambatnya dalam waktu 14 (empat belas) hari kerja tehitung sejak
                                        diterimanya dokumen-dokumen tagihan oleh Unit Keuangan PT. PINS Indonesia secara
                                        sah, lengkap, dan benar, serta telah memenuhi persyaratan. Dokumen yang belum
                                        lengkap akan dikembalikan terlebih dahulu dan akan diproses setelah seluruh dokumen
                                        pembayaran tersebut diatas dilengkapi.</span></li>
                            </ol>
                        </div>
                       
                    </li>
                    <li
                        style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify;'>
                        <strong><span style="font-size:15px;">Lain-lain:</span></strong>
                        <br><span style="font-size:15px;">PT. PINS Indonesia akan menerbitkan Perjanjian Kerja Sama
                            (PKS) untuk ditandatangani oleh Supplier sekaligus sebagai dokumen yang menyatakan bahwa
                            Supplier bersedia dan sanggup:</span>

                        <ol style="list-style-type: lower-alpha;">
                            <li
                                style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                                <span style="font-size:15px;">Menyerahkan dan/atau menyelesaikan barang dan/atau hasil
                                    pekerjaan tepat waktu, tepat jumlah, sesuai dengan Spesifikasi Teknis yang telah
                                    ditetapkan oleh PT. PINS Indonesia;</span></li>
                            <li
                                style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                                <span style="font-size:15px;">Barang dan/atau hasil pekerjaan yang diserahkan oleh
                                    Supplier tidak bertentangan dengan dan/atau melanggar hukum dan/atau melanggar Hak
                                    milik pihak lain;</span></li>
                            <li
                                style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                                <span style="font-size:15px;">Melampirkan lampiran serial number perangkat dalam bentuk
                                    hardcopy dan softcopy;</span></li>
                            <li
                                style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                                <span style="font-size:15px;">Melampirkan bukti foto serah terima barang di
                                    lokasi;</span></li>
                            <li
                                style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                                <span style="font-size:15px;">Memberikan garansi perangkat selama 1 (satu) tahun yang
                                    dilengkapi dengan kartu garansi dari resmi di Indonesia;</span></li>
                            <li
                                style='margin-top: 0cm; margin-right: 0cm; margin-bottom: 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: justify; line-height: 1.5;'>
                                <span style="font-size:15px;">Bersedia dikenakan Sanksi Denda Keterlambatan sebesar
                                    1&permil; (satu per mil) per hari keterlambatan dikalikan nilai barang dan/ atau
                                    hasil pekerjaan yang belum diterima, dengan maksimal denda sebesar 5% (lima persen).
                                    Apabila jumlah sanksi denda keterlambatan telah mencapai 5% (lima persen) dari nilai
                                    total barang dan/ atau hasil pekerjaan berdasarkan PKS ini, PT. PINS Indonesia
                                    berhak membatalkan sisa barang dan/atau sisa pekerjaan yang belum diterima.</span>
                            </li>
                        </ol>
                    </li>
                </ol>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;text-align:justify;font-size:16px;font-family:"Times New Roman",serif;margin-left:35.3pt;'>
                    <br>
                </p>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:justify;'>
                    <span style="font-size:15px;">Demikian Berita Acara Klarifikasi dan Negosiasi ini dibuat sebagai
                        dasar pelaksanaan pekerjaan sebelum ditandatanganinya Perjanjian/Kontrak.</span></p>
                <div style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'><span
                        style="font-size:9px;">&nbsp;</span></div>

                <p
                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:justify;'>
                    <br>
                </p>

                <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-left: calc(0%);"
                    width="671">
                    <tbody>
                        <tr>
                            <td style="width: 235.85pt;padding: 0cm 5.4pt;height: 107.1pt;vertical-align: top;"
                                valign="top" width="46.865671641791046%">

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">PT. PINS Indonesia,</span></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'>
                                    <br>
                                </p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'>
                                    <br>
                                </p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'>
                                    <br>
                                </p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <br>
                                </p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <br>
                                </p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><u><span style="font-size:15px;" class="getNamaGmEcom">{GM Ecom}</span></u></strong></p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <strong><span style="font-size:15px;">GM e-Commerce</span></strong></p>
                            </td>
                            <td style="width: 267.25pt;padding: 0cm 5.4pt;height: 107.1pt;vertical-align: top;"
                                valign="top" width="53.134328358208954%">

                                <p
                                    style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center;'>
                                    <strong><span style="font-size:15px;" id="namaMitra">{namaMitra},&nbsp;</span></strong>
                                </p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;text-align:center;'>
                                    <br>
                                </p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'>
                                    <br>
                                </p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'>
                                    <br>
                                </p>

                                <p
                                    style='margin:0cm;margin-bottom:.0001pt;font-size:16px;font-family:"Times New Roman",serif;'>
                                    <br>
                                </p>

                                <p
                                    style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center;'>
                                    <br>
                                </p>

                                <p
                                    style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center;'>
                                    <strong><span style="font-size:15px;"><u class="direkturMitra">{direkturMitra}</u></span></strong></p>

                                <p
                                    style='margin: 0cm 0cm 0.0001pt; font-size: 16px; font-family: "Times New Roman", serif; text-align: center;'>
                                    <strong><span style="font-size:15px;">Direktur</span></strong></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>