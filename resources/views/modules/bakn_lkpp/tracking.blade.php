@extends('layouts.master')

@section('title')
Tracking BAKN
@endsection
@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<!-- Momment -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
@endsection

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAKN
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">BAKN</a></li>
            <li class="active"> Tracking </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-ticket"></i>BAKN TRACKING</h3>
                        <div class="pull-right box-tools">
                            <a href="/export-spph-lkpp" class="btn btn-info"><i class="fa fa-download"></i> </a>
                        </div>
                    </div>
                    <div style="text-align:center">
                        <div>
                            <select name="bulan" id="bulan" class="form-group" style="width:10%;height:34px">
                            </select>
                            <select name="tahun" id="tahun" class="form-group" style="width:10%;height:34px">
                            </select>
                            <input type="hidden" id="token" value="{{ csrf_token()}}">
                        </div>
                    </div>
                    <!-- /.box-header --> 
                    <div class="box-body table-responsive">
                            <table name="item" id="tracking" data-url="/lkpp/bakn/tracking/" class="display" style="padding:10px" width="100%">
                            <thead>
                                <tr id="kepala">
                                    <th rowspan="2" style="text-align:center">No</th>
                                    <th rowspan="2" style="text-align:center">Key</th>
                                    <th rowspan="2" style="text-align:center">No. IO</th>
                                    <th rowspan="2" style="text-align:center">No. SPPH</th>
                                    <th rowspan="2" style="text-align:center">Judul</th>
                                    <th rowspan="2" style="text-align:center">Mitra</th>
                                    <th rowspan="2" style="text-align:center">Pembuat SPPH</th>
                                    <th rowspan="2" style="text-align:center">Tanggal Buat BAKN</th>
                                    <th rowspan="2" style="text-align:center">Harga</th>
                                    <th colspan="3" style="text-align:center">Approval</th>
                                    <th rowspan="2" style="text-align:center">Posisi</th>
                                    <th rowspan="2" style="text-align:center"></th>
                                </tr>
                                <tr style=" white-space: nowrap">
                                    <th style="text-align:center">Creator</th>
                                    <th style="text-align:center">Manager </th>
                                    <th style="text-align:center">GM E-Commerce</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/sweetalert/js/sweetalert2.all.min.js')}}"></script>
<script src="{{ asset('js/web/main/js/bakn_lkpp/tracking.js')}}"></script>
@endsection