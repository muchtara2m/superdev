<div class="modal fade" id="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Upload File</h4>
                </div>
                <div class="modal-body">
                    <form method="post" action="" id="file" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="">Pilih File</label>
                            <input type="file" name="file[]" id="multiplefile" class="form-control" multiple="multiple">
                            <small id="helpId" class="text-muted">*Dokumen yang sudah di tanda tangan</small>
                          </div>
                          <button type="submit" class="btn btn-primary" style="width: 7em;">
                              Submit</button>
                          </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->