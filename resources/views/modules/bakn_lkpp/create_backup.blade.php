@extends('layouts.master')

@section('title')
Create BAKN
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<!-- Include Editor style. -->
<link rel="stylesheet" href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('froala/css/froala_style.min.css') }}"/>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- DataTable -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<!-- Tagify  -->
<link rel="stylesheet" href="{{ asset('tagify/dist/tagify.css') }}">

<style>
    .no-bullet {
       padding-left: 0;
       list-style-type: none;
   }
</style>
@endsection


@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAKN
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">BAKN</a></li>
            <li class="active"> Create </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Create BAKN</h3>
                    </div>
                    <!-- /.box-header -->
                    <br>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    {{-- start form  --}}
                    <form method="post" action="/store-bakn-lkpp/" enctype="multipart/form-data" id="form">
                        @csrf
                        <input  type="hidden" 
                                    id="spphid" 
                                    name="spphid" 
                                    value="{{ old('spphid') }}">

                        <div class="box-body">
                            <div class="form-group col-md-12 igroup">
                                <label for="nomorspph">Select SPPH*</label>
                                <a  id="spphpreview" 
                                    target="_blank" 
                                    href="#">
                                    <i class="fa fa-fw fa-file-code-o"></i>
                                </a>
                                <input  type="text" 
                                        name="nmrspph" 
                                        class="form-control" 
                                        data-toggle="modal"
                                        data-target="#modal-unit-spph" 
                                        placeholder="Pilih SPPH" 
                                        id="nmrspph"
                                        autocomplete="off"
                                        value="{{ old('nmrspph') }}">
                            </div>
            
                            <div class="form-group col-md-4 igroup">
                                <label for="tglbakn">Tanggal BAKN*</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input  type="text" 
                                            data-date="" 
                                            data-date-format="yyyy-mm-dd"
                                            class="form-control datejos" 
                                            name="tglbakn" 
                                            id="enddelivery-date"
                                            onchange="getDetailSpph()" 
                                            autocomplete="off"
                                            value="{{ old('tglbakn') }}">
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="harga">Harga*</label>
                                <div class="input-group col-md-12">
                                    <span class="input-group-addon">Rp</span>
                                    <input  type="text" 
                                            class="form-control" 
                                            onkeyup="formatAngka(this,'.')" 
                                            name="harga"
                                            id="harganum" 
                                            autocomplete="off"
                                            value="{{ old('harga') }}">
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="pimpinanrapat">Pimpinan Rapat</label>
                                <div class="input-group col-md-12">
                                    <select name="pimpinan_rapat" 
                                            id="pimpinan" 
                                            class="form-control" 
                                            title="Pimpinan">
                                        <option value="Hernadi Yoga Adhitya Tama"
                                            {{ (old('pimpinan_rapat') == 'Hernadi Yoga Adhitya Tama')? 'selected':'' }}>GM E-Commerce</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="noio">No IO</label>
                                <div class="input-group col-md-12">
                                    <input  type="hidden" 
                                            name="io_id" 
                                            id="io_id">

                                    <input  type="text" 
                                            name="noio" 
                                            id="noio" 
                                            class="form-control" 
                                            title="Nomor IO"
                                            readonly
                                            value="{{ old('noio') }}">
                                </div>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="desio">Deskripsi IO</label>
                                <div class="input-group col-md-12">
                                    <input  type="text" 
                                            name="desio" 
                                            id="desio" 
                                            class="form-control" 
                                            title="Deskripsi"
                                            data-toggle="modal" 
                                            data-target="#modal-unit" 
                                            readonly 
                                            value="{{ old('desio') }}"
                                            placeholder="Pilih IO">
                                </div>
                            </div>

                            <div class="form-group col-md-4 igroup">
                                <label class="control-label">Tipe Undangan</label>
                                <br>
                                <div class="col-md-6">
                                    <ul class="no-bullet">
                                        <li>
                                            <input  type="checkbox" 
                                                    name="tipe_undangan[]" 
                                                    value="Review" 
                                                    id="invit1"
                                                    checked>
                                            <label for="minimal-checkbox-1">Review</label>
                                        </li>
                                        <li>
                                            <input  type="checkbox" 
                                                    name="tipe_undangan[]" 
                                                    value="Coordination"
                                                    id="invit2" 
                                                    checked>
                                            <label for="minimal-checkbox-2">Coordination</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul class="no-bullet">
                                        <li>
                                            <input  type="checkbox" 
                                                    name="tipe_undangan[]" 
                                                    value="Briefing" 
                                                    id="invit3">
                                            <label for="minimal-checkbox-disabled">Briefing</label>
                                        </li>
                                        <li>
                                            <input  type="checkbox" 
                                                    name="tipe_undangan[]" 
                                                    value="Decision Marking"
                                                    id="invit4" 
                                                    checked>
                                            <label for="minimal-checkbox-disabled-checked">Decision Making</label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="pesertarapat">Peserta Rapat*</label>
                                <div class="form-group">
                                    <div class="input-group-addon select-addon">
                                        <span>PT PINS INDONESIA</span>
                                    </div>
                                    @php
                                        $daftar = '';
                                        foreach($peserta as $item){
                                            $daftar .= $item.',';
                                        }
                                    @endphp
                                    <input  name="peserta_pins" 
                                            class=" input-group tagify" 
                                            id="tags-pins"
                                            placeholder="Pilih Peserta" 
                                            value="{{ Auth::user()->name.','.$daftar }}">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="pesertarapat">Peserta Rapat*</label>
                                <div class="form-group" id="mtr">
                                    <div class="input-group-addon select-addon">
                                        <span id="mitra">MITRA</span>
                                    </div>
                                    <input  name="peserta_mitra" 
                                            class="input-group form-control" 
                                            id="direktur"
                                            placeholder="Pilih Peserta" 
                                            value="{{ old('peserta_mitra') }}">
                                </div>
                            </div>

                            {{-- hasil pembahasan --}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h4 class="divider-title">HASIL PEMBAHASAN</h4>
                                    <hr>
                                </div>
                                <div class="form-group">
                                    <label for="agenda">Agenda</label>
                                    <textarea   id="agenda" 
                                                rows="10" 
                                                cols="80" 
                                                name="agenda" 
                                                placeholder="Type Something"
                                                class="bakn">
                                        {{ old('agenda') }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="dasarpembahasan">Dasar Pembahasan</label>
                                    <textarea   rows="10" 
                                                cols="80" 
                                                id="dasarpembahasan" 
                                                name="dasar_pembahasan"
                                                placeholder="Type Something" 
                                                class="bakn">
                                        {{ old('dasar_pembahasan') }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="ruanglingkip">Ruang Lingkup</label>
                                    <textarea   id="ruanglingkup" 
                                                rows="10" 
                                                cols="80" 
                                                name="ruang_lingkup"
                                                placeholder="Type Something" 
                                                class="bakn">
                                        {{ old('ruang_lingkup') }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="lokasipekerjaan">Lokasi Pekerjaan/Pengiriman Barang/Jasa*</label>
                                    <textarea   id="cke-lokasi-pekerjaan" 
                                                rows="10" 
                                                cols="80" 
                                                name="lokasi_pekerjaan"
                                                placeholder="Type Something" 
                                                class="bakn">
                                        {{ old('lokasi_pekerjaan') }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="jangkawaktu">Jangka waktu Pengiriman Barang*</label>
                                    <textarea   id="cke-jangka-waktu" 
                                                rows="10" 
                                                cols="80" 
                                                name="jangka_waktu"
                                                placeholder="Type Something" 
                                                class="bakn">
                                        {{ old('jangka_waktu') }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="harga">Harga</label>
                                    <textarea   id="nilai" 
                                                name="harga_terbilang" 
                                                placeholder="Type Something"
                                                class="bakn">
                                        {{ old('harga_terbilang') }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="tatacara">Tata Cara Pembayaran*</label>
                                    <div>
                                        @foreach ($bayar as $item)
                                        <label>
                                            <input  type="checkbox" 
                                                    value="{{ $item->isi }}" 
                                                    class="carabayar"
                                                    onchange="add()"> {{ $item->jenis }}
                                        </label>
                                        @endforeach
                                    </div>
                                    <textarea   rows="10" 
                                                cols="80" 
                                                name="cara_bayar" 
                                                id="tatacara" 
                                                class="bakn">
                                        {{ old('cara_bayar') }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label >Lain - lain</label>
                                    <textarea   rows="10" 
                                                cols="80" 
                                                name="lain_lain" 
                                                class="bakn">
                                        <p>PT. PINS Indonesia akan menerbitkan Perjanjian/Konrak untuk ditandatangani oleh MITRA sekaligus sebagai dokumen yang menyatakan bahwa MITRA bersedia dan sanggup :</p>
                                        <ol type="a">
                                            <li>Menyerahkan dan/atau melaksanakan barang dan/atau hasil pekerjaan tepat waktu, tepat jumlah, sesuai dengan ketentuan yang telah ditetapkan oleh PT. PINS Indonesia;</li>
                                            <li>Barang dan/atau hasil pekerjaan yang diserahkan MITRA tidak akan bertentangan dengan dan/atau melanggar hukum dan/atau melanggar Hak milik pihak lain;</li>
                                        </ol>
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="">File Lampiran</label>
                                    <input  type="file" 
                                            name="lampiran[]" 
                                            id="" 
                                            class="form-control" 
                                            multiple="multiple">
                                </div>
                                <div class="form-group">
                                    <label for="">Chat</label><span>*require for submit </span>
                                    <input  type="text" 
                                            name="chat" 
                                            class="form-control">
                                </div>
                            </div>
                            {{-- end hasil pembahasan --}}
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" 
                                    name="status" 
                                    value="draft_bakn" 
                                    class="btn btn-primary"
                                    style="width: 7em;">
                                    <i class="fa fa-check"></i> Save
                            </button>
                            <button type="submit" 
                                    name="status" 
                                    value="save_bakn" 
                                    class="btn btn-success"
                                    style="width: 7em;">
                                    <i class="fa fa-check"></i> Submit
                            </button>
                            @if (Auth::user()->level == 'administrator')
                            <button type="submit" 
                                    name="status" 
                                    value="save_admin" 
                                    class="btn bg-navy" 
                                    style="width: 7em;">
                                    Save Admin 
                            </button>
                            @endif
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    {{-- end form --}}
                </div>
                <!-- /.box -->
                @include('modules.bakn_lkpp.modal.io_modal')
                @include('modules.bakn_lkpp.modal.spph_modal')
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<!-- Data Table -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- clockpicker -->
<script src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Tagify -->
<script src="{{ asset('tagify/dist/jQuery.tagify.min.js') }}"></script>
<script src="{{ asset('tagify/dist/tagify.min.js') }}"></script>
<script src="{{ asset('js/web/main/js/bakn_lkpp/create.js')}}"></script>
@endsection