@extends('layouts.master')

@section('title')
Preview BAKN
@endsection

@section('stylesheets')
<!-- css froala editor -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>BAKN<small>Preview </small></h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">BAKN</a></li>
            <li class="active"> Preview </li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid" style="border-radius: 5px;border-left: 4px solid#00a7d0 !important;">
                    <div class="box-header with-border">
                        <h4 class="text-center"><strong>{{ strtoupper($baknLKPP->spph_lkpps->judul) }}</strong></h4>
                    </div>
                    <div class="box-body ">
                        <div class="row">
                            <div class="col-sm-6">
                                <dl class="dl-horizontal">
                                    <dt>Nomor SPPH</dt>
                                    <dd>{{ $baknLKPP->spph_lkpps->nomorspph }}</dd>
                                    <dt>Nomor SPH</dt>
                                    <dd>{{ $baknLKPP->spph_lkpps->nomorsph == NULL ? 'Belum Diinput' : $baknLKPP->spph_lkpps->nomorsph }}</dd>
                                    <dt>Pembuat</dt>
                                    <dd>{{ $baknLKPP->user_bakn_lkpp->name }}</dd>
                                    <dt>Tanggal</dt>
                                    <dd>{{ date('d F Y',strtotime($baknLKPP->tglbakn)) }}</dd>
                                    <dt>Mitra</dt>
                                    <dd>{{ $baknLKPP->spph_lkpps->mitra_lkpps->perusahaan }}</dd>
                                    @if ($baknLKPP->isi != null)
                                        <dt>Metode Bayar</dt>
                                        @php
                                            $i = 1;                                            
                                            $data = explode(",",$baknLKPP->cara_bayar);
                                        @endphp
                                            @foreach ($data as $item)
                                                <dd class="text-justify">{{$i++.'. '.$item }}</dd>                                                
                                            @endforeach
                                    @endif
                                </dl>
                            </div>

                            <div class="col-sm-6">
                                @php
                                $title_lampiran = ($baknLKPP->lampiran) !=NULL ? json_decode($baknLKPP->title_lampiran, TRUE) : NULL;
                                $lampiran       = ($baknLKPP->lampiran) !=NULL ? json_decode($baknLKPP->lampiran, TRUE) : NULL;
                                $title          = ($baknLKPP->file) !=NULL ? json_decode($baknLKPP->title, TRUE) : NULL ;
                                $file           = ($baknLKPP->file) !=NULL ? json_decode($baknLKPP->file, TRUE) : NULL;
                                @endphp
                                <div class="row">
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>Lampiran</dt>
                                            @if ($title_lampiran != NULL)
                                            @foreach ($title_lampiran as $key => $value)
                                            {{ ($key+1)}}. <a href="{{Storage::url($lampiran[$key])}}">{{$title_lampiran[$key]}}</a><br>
                                            @endforeach
                                            @endif
                                           
                                          </dl>
                                    </div>
                                    <div class="col-xs-6">
                                        <dl>
                                            <dt>File</dt>
                                            @if ($title != NULL)
                                            @foreach ($title as $key => $value)
                                            {{ ($key+1)}}. <a
                                                href="{{Storage::url($file[$key])}}">{{$title[$key]}}</a><br>
                                            @endforeach
                                            @endif
                                           
                                          </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-solid" style="border-radius: 8px">
                        @if ($baknLKPP->isi != NULL)
                        <textarea name="" id="" cols="30" rows="10">
                            {{ $baknLKPP->isi }}
                        </textarea>
                        @else
                        @include('modules.bakn_lkpp.inc.bakn_preview')
                        @endif
                </div>
            </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<!-- script function froala -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<script src="{{ asset('froala/js/languages/id.js') }}"></script>
<!-- External -->
<script type="text/javascript">
    $('textarea').froalaEditor({
        // fullPage: true,
        toolbarButtons      : ['print', 'html', 'getPDF', 'fullscreen'],
        charCounterCount    : false,
        key                 : keyFroala,
        height              : 500,
    })
    // removo alert lisensi froala
$("div > a", ".fr-wrapper").css('display', 'none');
</script>
@endsection