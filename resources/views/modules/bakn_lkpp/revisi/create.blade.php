@extends('layouts.master')

@section('title')
Revisi BAKN
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.4/select2.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-css/1.4.6/select2-bootstrap.min.css" />
<!-- Include Editor style. -->
<link rel="stylesheet" href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" />
<link rel="stylesheet" href="{{ asset('froala/css/froala_style.min.css') }}" />
<style>
    .capitalize {
        text-transform: capitalize;
    }

    .select2-container-multi .select2-choices .select2-search-choice {
        padding: 5px 5px 5px 18px;
    }
</style>
@endsection


@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAKN
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">BAKN</a></li>
            <li><a href="#">Revisi</a></li>
            <li class="active">Create </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Edit</h3>
                    </div>
                    <!-- /.box-header -->
                    <br>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    {{-- start form  --}}
                    <form method="post"  action="{{ route('lkpp.bakn.revisi',$baknLKPP->id) }}" enctype="multipart/form-data" id="form">
                        @csrf
                        @method('PUT')
                        <input type="hidden" name="" id="gmEcom" value="{{ \App\User::where('level','gmecom')->first()->name }}">
                        <input type="hidden" id="url" data-url="{{ url('lkpp/bakn/spph',$baknLKPP->id) }}">
                        <input type="hidden" id="url-io" data-url="{{ url('lkpp/bakn/io') }}">

                        <div class="box-body">
                            <div class="row invoice-info">
                                <div class="form-group col-sm-12 igroup">
                                    <label for="nomorspph">Select SPPH</label>
                                    <a  id="spphpreview" 
                                        target="_blank" 
                                        href="#">
                                        <i class="fa fa-fw fa-file-code-o"></i>
                                    </a>
                                    <input  class="form-control" 
                                            data-val="true" 
                                            placeholder="Pilih SPPH" 
                                            value="{{ old('nmrspph', $baknLKPP->spph_lkpps['nomorspph']) }}" 
                                            name="nmrspph" 
                                            id="nospph" 
                                            type="hidden" >
    
                                    <input  type="hidden" 
                                            name="spph_id" 
                                            id="spph_id" 
                                            value="{{ old('spph_id', $baknLKPP->spph_id) }}">
                                </div>

                                <div class="form-group col-sm-4 igroup">
                                    <label for="tglKontrak">Tanggal BAKN</label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input  type="text" 
                                                data-date="" 
                                                data-date-format="yyyy-mm-dd"
                                                class="form-control datejos" 
                                                name="tglbakn" 
                                                id="tglbakn"
                                                value="{{ old('tglbakn', $baknLKPP->tglbakn) }}">
                                    </div>
                                </div>

                                <div class="form-group col-sm-4">
                                    <label for="tglKontrak">Harga</label>
                                       
                                    <div class="input-group col-md-12">
                                        <span class="input-group-addon">Rp</span>
                                        <input  type="text" 
                                                class="form-control" 
                                                onkeyup="formatAngka(this,'.')" 
                                                name="harga"
                                                id="harganum" 
                                                autocomplete="off"
                                                value="{{ old('harga', $baknLKPP->harga) }}">
                                    </div>
                                </div>

                                <div class="form-group col-sm-4 ">
                                    <label for="tglKontrak">No.IO </label>
                                  
                                    <input  class="form-control" 
                                            data-val="true" 
                                            placeholder="Pilih IO" 
                                            value="{{ old('io_id', $baknLKPP->io_id) }}" 
                                            name="io_id" 
                                            id="io_id" 
                                            type="hidden" >

                                    <input  type="hidden" 
                                            name="no_io" 
                                            id="no_io" 
                                            value="{{ old('no_io', $baknLKPP->io_id) }}">
                                </div>
                                <div class="col-sm-6">
                                    <label for="tatacara">Tata Cara Pembayaran*</label>
                                    <div class="row">
                                        @php
                                            if(old('caraBayar') == NULL){
                                                $caraBayar = explode(",",$baknLKPP->cara_bayar);
                                            }else{
                                                $caraBayar = old('caraBayar');
                                            }
                                        @endphp
                                           @foreach ($bayar as $item)
                                            <div class="col-xs-6">
                                                <label>
                                                    <input  type="checkbox" 
                                                            name="caraBayar[]"
                                                            value="{{ $item->jenis }}" 
                                                                {{ in_array($item->jenis, $caraBayar) ? "checked":"" }}
                                                            data-val="{{ $item->isi }}"
                                                            class="carabayar"
                                                            onchange="add()"> {{ $item->jenis }}
                                              </div>
                                            @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="row" style="margin-top: 2%">
                                <div class="form-group">
                                    <h4 class="divider-title center" style="text-align:center">PREVIEW</h4>
                                    <h5 class="divider-title center" style="text-align:center">
                                        <a  href="/preview-bakn-lkpp/{{ $baknLKPP->id }}" target="_blank">Preview BAKN</a>
                                    </h5>
                                    <hr>
                                </div>
                                <div class="col-xs-12">
                                    <textarea   id="agenda" 
                                                rows="10" 
                                                cols="80" 
                                                name="isi" 
                                                class="bak">
                                                @if (old('isi') == null && $baknLKPP->isi != null)
                                                    {{ $baknLKPP->isi }}
                                                @elseif(old('isi') != null)
                                                    {{ old('isi') }}
                                                @elseif($baknLKPP->isi == null)
                                                    @include('modules.bakn_lkpp.inc.create')   
                                                @endif
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="form-group">
                                <label for="">File Lampiran</label>
                                <input  type="file" 
                                        name="lampiran[]" 
                                        id="lampiran" 
                                        class="form-control" 
                                        multiple="multiple">
                                        <td>
                                            @php
                                            $flampiran = json_decode($baknLKPP->lampiran, TRUE);
                                            $tlampiran = json_decode($baknLKPP->title_lampiran, TRUE);
                                            $i=1;
                                            @endphp
                                            @if($flampiran != NULL)
                                            @foreach ($tlampiran as $keylampiran =>$vallampiran)
                                            {{ $i++.'. ' }}<a href="{{ Storage::url($flampiran[$keylampiran]) }}">{{ $tlampiran[$keylampiran] }}</a><br>
                                            @endforeach
                                            @endif
                                        </td>       
                            </div>
                            <div class="form-group">
                                <label for="">Chat</label><span>*require for submit </span>
                                <input  type="text" 
                                        name="chat" 
                                        value="{{ old('chat') }}"
                                        class="form-control">
                            </div>
                            <button type="submit" 
                                    name="status" 
                                    value="draft_bakn" 
                                    class="btn btn-primary"
                                    style="width: 7em;">
                                    Save
                                </button>
                            <button type="submit" 
                                    name="status" 
                                    value="save_bakn" 
                                    class="btn btn-success"
                                    style="width: 7em;">
                                    Submit
                                </button>
                        </div>

                        <!-- /.box-footer -->
                    </form>
                    {{-- end form --}}
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>
<!-- clockpicker -->
<script src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/4.15.0/lodash.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.4/select2.min.js"></script>
<script src="{{ asset('js/web/main/js/bakn_lkpp/edit-new.js')}}"></script>
@endsection