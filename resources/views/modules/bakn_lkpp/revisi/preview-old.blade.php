<textarea id="froala-editor">
    <!DOCTYPE html>
    <html>
    <head>
        <title></title>
    </head>
    <body>
        <table>
            <tbody>
                <tr>
                    <td rowspan="4" colspan="1" style="text-align:center; vertical-align:middle;"><img src="{{ asset('images/pinlogo.png') }}" width="190" /></td>
                    <td colspan="3">
                        <div style="text-align: center;">
                            <strong class="judul">
                                <center>BERITA ACARA KLARIFIKASI & NEGOSIASI</br>
                                    {{ strtoupper($spphLKPP->judul) }}</br>
                                    ANTARA</br>
                                    PT. PINS INDONESIA</br>
                                    DENGAN</br>
                                    <span id="phk2">{{ strtoupper($spphLKPP->mitra_lkpps['perusahaan']) }}</span></center>
                                </strong>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td colspan="3">{{ Carbon\Carbon::parse($revisiBaknLkpp['tglbakn'])->formatLocalized('%d %B %Y') }}</td>
                    </tr>
                    <tr>
                        <td>Waktu</td>
                        <td colspan="3">- WIB</td>
                    </tr>
                    <tr>
                        <td>Tempat</td>
                        <td colspan="3">Kantor PT. PINS INDONESIA</td>
                    </tr>
                    <tr>
                        <td colspan="1">Undangan dari</td>
                        <td colspan="3">PT. PINS INDONESIA</td>
                    </tr>
                    
                    <tr>
                        <td colspan="1">Tipe Rapat</td>
                        <td colspan="3">
                            <ul class="icheck-list">
                                <li style="display: inline;">
                                    <input type="checkbox" class="check" id="minimal-checkbox-1" {{ in_array("Review",$checkbox)? "checked":"" }}>
                                    <label for="minimal-checkbox-1">Review</label>
                                </li>
                                <li style="display: inline;">
                                    <input type="checkbox" class="check" id="minimal-checkbox-2" {{ in_array("Coordination",$checkbox)? "checked":"" }}>
                                    <label for="minimal-checkbox-2">Coordination</label>
                                </li>
                                <li style="display: inline;">
                                    <input type="checkbox" class="check" id="minimal-checkbox-3" {{ in_array("Briefing",$checkbox)? "checked":"" }}>
                                    <label for="minimal-checkbox-disabled">Briefing</label>
                                </li>
                                <li style="display: inline;">
                                    <input type="checkbox" class="check" id="minimal-checkbox-4" {{ in_array("Decision Marking",$checkbox)? "checked":"" }}>
                                    <label for="minimal-checkbox-disabled-checked">Decision
                                        Making</label>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        
                        <tr>
                            <td colspan="1">Pimpinan Rapat</td>
                            <td colspan="3">{{ $revisiBaknLkpp['pimpinan_rapat'] }}</td>
                        </tr>
                        <tr>
                            <td colspan="1">Peserta</td>
                            <td colspan="2"><strong>{{ strtoupper($spphLKPP->mitra_lkpps['perusahaan'])}}:</strong>
                                <ol>
                                    {{ $revisiBaknLkpp->peserta_mitra }}
                                </ol>
                            </td>
                            <td colspan="2">
                                <strong>PT. PINS INDONESIA :</strong>
                                <ol>
                                    @php
                                    $datas = json_decode($revisiBaknLkpp['peserta_pins'], true);
                                    for ($i=0; $i<count($datas); $i++) {
                                        echo '<li>'.$datas[$i]["value"].'</li>';
                                    }
                                    @endphp
                                </ol>
                                
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="tengah" style="background-color: rgb(204, 204, 204);">
                                <strong>
                                    <center>AGENDA</center>
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" >
                                <center>
                                    {{ $revisiBaknLkpp['agenda'] }}
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="tengah" style="background-color: rgb(204, 204, 204);">
                                    <strong>
                                        <center>DASAR PEMBAHASAN</center>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" >
                                    {{ $revisiBaknLkpp['dasar_pembahasan'] }}
                                    
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="tengah" style="background-color: rgb(204, 204, 204);">
                                    <strong>
                                        <center>HASIL - HASIL KLARIFIKASI & NEGOSIASI</center>
                                    </strong>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    SUPPLIER dan PT. PINS Indonesia sepakat melakukan
                                    klarifikasi dan negosiasi dengan hasil sebagai berikut
                                    :
                                </br>
                                <b>1. Ruang Lingkup :</b>
                                {{ $revisiBaknLkpp['ruang_lingkup'] }}
                            </br>
                            <b>2. Lokasi Pekerjaan/ Pengiriman Barang/ Jasa :</b>
                            {{ $revisiBaknLkpp['lokasi_pekerjaan'] }}
                        </br>
                        <b>3. Jangka Waktu Pengiriman Barang :</b>
                        {{ $revisiBaknLkpp['jangka_waktu'] }}
                    </br>
                    <br><b>4. Harga :</b>
                    {{ $revisiBaknLkpp['harga_terbilang'] }}
                </br>
                <b>5. Tatacara Pembayaran :</b>
                
                {{ $revisiBaknLkpp['cara_bayar'] }}
            </br>
            <b>6. Lain - lain :</b>
            {{ $revisiBaknLkpp['lain_lain'] }}
        </br>
    </br>
    Demikian Berita Acara Klarifikasi dan Negosiasi
    ini dibuat.
</td>
</tr>

<tr>
    <td colspan="2" width="50%">
        <p style="text-align:center">{{ $spphLKPP->mitra_lkpps['perusahaan'] }}</p>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <p style="text-align:center">{{ $spphLKPP->mitra_lkpps['direktur'] }}</p>
        
    </td>
    <td colspan="2" width="50%">
        <p style="text-align:center">PT PINS INDONESIA</p>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <p style="text-align:center">{{ $revisiBaknLkpp->pimpinan_rapat }}</p>
    </td>
</tr>
</tbody>
</table>
</body>
</html>

</textarea>