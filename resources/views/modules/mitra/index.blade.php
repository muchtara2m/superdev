@extends('layouts.master')

@section('title')
Create Mitra | Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">

@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAKN & CONTRACT
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Transaksi</a></li>
            <li class="active"> Mitra </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> MITRA</h3>
                        <a href="{{ url('mitra/create') }}" class="btn btn-primary" style="float: right"><strong>Add Mitra</strong></a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="pbsTable" class="display">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Perusahaan</th>
                                    <th>Alamat</th>
                                    <th>Direktur</th>
                                    <th>PIC</th>
                                    <th>Telp</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach($mitras as $mitra)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{$mitra['perusahaan']}}</td>
                                    <td>{{$mitra['alamat']}}</td>
                                    <td>{{$mitra['direktur']}}</td>
                                    <td>{{$mitra['pic']}}</td>
                                    <td>{{$mitra['telp']}}</td>
                                    <td>{{$mitra['email']}}</td>
                                    <td class="text-center">
                                        <div class="btn-group  btn-group-sm">
                                          <a href="{{action('MitraController@edit', $mitra['id'])}}">
                                            <button class="btn btn-success btn-xs" type="button"><i class="fa fa-pencil"  title="Edit Mitra"></i></button></a>

                                            <form action="{{action('MitraController@destroy', $mitra['id'])}}" method="POST" style="display: inline;">
                                              @csrf
                                              @method('DELETE')
                                              <button type="submit" onclick="return confirm('Bener nih mau dihapus?')" id="delete-btn" class="btn btn-danger btn-xs click-hand" title="Delete Data"><i class="fa fa-trash"></i>
                                              </button>
                                            </form>
                                          </div>
                                        </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>

<script type="text/javascript">
    // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
    $(document).ready( function () {
        $('table').DataTable({

        });
    } );
</script>
@endsection
