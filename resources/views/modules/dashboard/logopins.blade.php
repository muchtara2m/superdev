@extends('layouts.master')

@section('title')
  Home
@endsection

@section('stylesheets')
@endsection

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        WELCOME TO KONTRAK
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <img class="home-logo-pins" src="{{ asset('images/logo-trans-min.png') }}" alt="Logo Pins">
            </div>
        </div>
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@section('scripts')
@endsection