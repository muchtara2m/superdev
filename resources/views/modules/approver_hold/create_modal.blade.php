<!-- modal -->
<div class="modal fade" id="modal-add">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"></span></button>
          <h4 class="modal-title">Data Unit</h4>
        </div>
        <div class="modal-body">
         <!-- form start -->
          <form method="POST" action="{{ }}">
            @csrf
            @method('POST')
            
            <div class="form-group">
              <label>Role*</label>
              <select name="role_id" id="role" class="form-control" required>
                <option selected disabled>Choose Role</option>
                @if (count($roles)>0)
                  @foreach ($roles as $role)
                    <option value="{{ $role->id }}" @php echo (old('role') == $role->id)? 'selected' : ''; @endphp>{{ $role->display }}</option>
                  @endforeach
                @endif
              </select>
            </div>
            <div class="form-group">
              <label>Nilai Minimum*</label>
              <input name="min" type="text" class="form-control" value="{{ old('min') }}" autofocus required>
            </div>
            <div class="form-group">
              <label>Nilai Maksimum*</label>
              <input name="max" type="text" class="form-control" value="{{ old('max') }}" autofocus required>
            </div>

            <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary pull-right">Submit</button>
          {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
        </div>
            
          </form>
         <!-- form end -->
        </div>
        
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  