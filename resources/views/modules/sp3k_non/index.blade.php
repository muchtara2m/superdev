@extends('layouts.master')

@section('title')
SPK List | Super Slim
@endsection

@section('stylesheets')
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
{{-- Icon --}}
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<!-- DataTables -->

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
{{-- <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.3/css/dataTables.responsive.css"> --}}
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SPK
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">SPK</a></li>
            <li class="active"> SPK LIST </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> SPK LIST</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_sp3" data-toggle="tab">SPK</a></li>
                        </ul>
                        <div class="tab-content">
                           
                            <!-- /.tab-pane -->
                            <div class="tab-pane active" id="tab_sp3">
                                <div class="box-body table-responsive">
                                    <table id="rolesTable" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>IO</th>
                                                <th>Nomor SPPH</th>
                                                <th>Judul SPPH</th>
                                                <th>Nomor SPK</th>
                                                <th>Tanggal SPK</th>
                                                <th>Harga</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $no=1;
                                            @endphp
                                            @foreach($spk as $list)
                                            <tr>
                                                <td>{{ $no++ }}</td>
                                                <td>{{ $list->bakns->io['no_io'] }}</td>
                                                <td>{{ $list->bakns->spph['nomorspph'] }}</td>
                                                <td>{{ $list->bakns->spph['judul'] }}</td>
                                                <td>{{ $list->nosp3 }}</td>
                                                <td>{{ $list->tglsp3 }}</td>
                                                <td>{{ number_format($list->bakns['harga'],0,'.','.') }}</td>
                                                <td>
                                                    <a href="{{ url('approve-sp3', $list->id) }}" class="fa fa-fw fa-check" title="Approval SP3"></a>
                                                    <a href="#modal" data-id="{{ $list->id }}" data-toggle="modal" title="Upload File" class="upload fa fa-fw fa-file"></a>
                                                    {{--  <a href="{{ url('edit-sp3', $list->id) }}" class="fa fa-fw fa-pencil-square-o" title="Edit SP3"></a>  --}}
                                                    <a href="{{ url('sp3-preview', $list->id) }}" class="fa fa-fw fa-file-code-o" title="Preview SP3"></a>
                                                    <a href = "delete-sp3/{{ $list->id }}" class="fa fa-fw fa-trash" onclick="return confirm('Bener nih mau dihapus?')" title="Delete SP3"></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                </div>
                <!-- /.box -->
                <div class="modal fade" id="modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Upload File</h4>
                                </div>
                                <div class="modal-body">
                                    <form method="post" action="" enctype="multipart/form-data">
                                        @csrf
                                        {{-- @method('PATCH') --}}
                                        <input type="hidden" name="title" class="form-control">
                                        <br>
                                        <input type="file" name="file[]" id="" class="form-control" multiple="multiple">
                                        <br>
                                        <button type="submit" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i>
                                            Submit</button>
                                        </form>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        @endsection

        @section('scripts')
        <!-- DataTables -->
        <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>
        <script type="text/javascript">
            // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
            $(document).ready( function () {
                $('#pbsTable').DataTable({
                });
            } );
            $(document).on("click", ".upload", function () {
                var idbro = $(this).data('id');
                // $(".modal-body #idnya").val( idbro );
                $('form').attr('action',"{{ url('sp3-upload')}}/"+idbro);
            }); </script>
            <script type="text/javascript">
                // document.getElementById("unitTable_wrapper").style.overflow = "auto";
                $(document).ready( function () {
                    $('#rolesTable,#permissionsTable').DataTable({
                    });
                } );
            </script>
            @endsection
