@extends('layouts.master')

@php
$homelink = "/home";
$crmenu = "Master Data";
$crsubmenu = "Create Role";
$submenulink = "/rpmanage";
$cract = "Add Role";
@endphp

@section('title')
{{ $crsubmenu." | SuperSlim" }}
@endsection

@section('stylesheets')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pretty-checkbox@3.0/dist/pretty-checkbox.min.css">
@endsection

@section('customstyle')
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $crsubmenu }}
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li>{{ $crmenu }}</li>
            <li><a href="{{ $submenulink }}">{{ $crsubmenu }}</a></li>
            <li class="active">{{ $cract }} </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Data Role</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="POST" action="{{ route('rpmanage.storeRole') }}" enctype="multipart/form-data" class="form">
                        @csrf
                        @method('POST')
                        <div class="box-body">
                            
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                There was a problem, please check your form carefully.
                                <ul>
                                    @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <div class="row">
                                <div class="form-group col-md-6 col-md-offset-3">
                                    <label>Role name*</label>
                                    <input name="roleName" type="text" class="form-control" autofocus required>
                                </div>
                                <div class="form-group col-md-6 col-md-offset-3">
                                    <label>Role display*</label>
                                    <input name="roleDisplay" type="text" class="form-control" required>
                                </div>
                                <div class="form-group col-md-6 col-md-offset-3">
                                    <div class="box box-info box-solid">
                                        <div class="box-header with-border">
                                            <h3 class="box-title">List of Allowed Menu</h3>
                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                </button>
                                            </div>
                                            <!-- /.box-tools -->
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            Please checks for menu/submenu that belongs to user's role.
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                </div>   
                            </div>
                            
                            <div class="container">
                                <div class="col-md-4">
                                     {{-- Pipeline--}}
                                     <ul class="unstyled-list">
                                            <li><div class="pretty p-icon p-round p-jelly">
                                                <input type="checkbox" id="menu-mdata-p"/>
                                                <div class="state p-primary">
                                                    <i class="icon fa fa-check"></i>
                                                    <label>Pipeline</label>
                                                </div>
                                            </div></li>
                                            <ul class="unstyled-list ckbox-child">
                                                @for ($i = 0; $i < count($pipeline); $i++)
                                                <li><div class="pretty p-icon p-round p-jelly">
                                                    <input type="checkbox" name="menuAccess[]" value="{{ $pipeline[$i][1] }}" class="menu-mdata-c" />
                                                    <div class="state p-primary">
                                                        <i class="icon fa fa-check"></i>
                                                        <label id="testlabel">{{ $pipeline[$i][0] }}</label>
                                                    </div>
                                                </div></li>
                                                @endfor
                                            </ul>
                                        </ul>
                                    {{-- PBS --}}
                                    <ul class="unstyled-list">
                                        <li><div class="pretty p-icon p-round p-jelly">
                                            <input type="checkbox" id="menu-pbs-p"/>
                                            <div class="state p-primary">
                                                <i class="icon fa fa-check"></i>
                                                <label>PBS</label>
                                            </div>
                                        </div></li>
                                        <ul class="unstyled-list ckbox-child">
                                            @for ($i = 0; $i < count($menu_pbs); $i++)
                                            <li><div class="pretty p-icon p-round p-jelly">
                                                <input type="checkbox" name="menuAccess[]" value="{{ $menu_pbs[$i][1] }}" class="menu-pbs-c" />
                                                <div class="state p-primary">
                                                    <i class="icon fa fa-check"></i>
                                                    <label id="testlabel">{{ $menu_pbs[$i][0] }}</label>
                                                </div>
                                            </div></li>
                                            @endfor
                                        </ul>
                                    </ul>
                                    {{-- AR --}}
                                    <ul class="unstyled-list">
                                        <li><div class="pretty p-icon p-round p-jelly">
                                            <input type="checkbox" id="menu-ar-p"/>
                                            <div class="state p-primary">
                                                <i class="icon fa fa-check"></i>
                                                <label>AR</label>
                                            </div>
                                        </div></li>
                                        <ul class="unstyled-list ckbox-child">
                                            @for ($i = 0; $i < count($menu_ar); $i++)
                                            <li><div class="pretty p-icon p-round p-jelly">
                                                <input type="checkbox" name="menuAccess[]" value="{{ $menu_ar[$i][1] }}" class="menu-ar-c" />
                                                <div class="state p-primary">
                                                    <i class="icon fa fa-check"></i>
                                                    <label id="testlabel">{{ $menu_ar[$i][0] }}</label>
                                                </div>
                                            </div></li>
                                            @endfor
                                        </ul>
                                    </ul>
                                    {{-- Master Data --}}
                                    <ul class="unstyled-list">
                                        <li><div class="pretty p-icon p-round p-jelly">
                                            <input type="checkbox" id="menu-mdata-p"/>
                                            <div class="state p-primary">
                                                <i class="icon fa fa-check"></i>
                                                <label>Master Data</label>
                                            </div>
                                        </div></li>
                                        <ul class="unstyled-list ckbox-child">
                                            @for ($i = 0; $i < count($menu_mdata); $i++)
                                            <li><div class="pretty p-icon p-round p-jelly">
                                                <input type="checkbox" name="menuAccess[]" value="{{ $menu_mdata[$i][1] }}" class="menu-mdata-c" />
                                                <div class="state p-primary">
                                                    <i class="icon fa fa-check"></i>
                                                    <label id="testlabel">{{ $menu_mdata[$i][0] }}</label>
                                                </div>
                                            </div></li>
                                            @endfor
                                        </ul>
                                    </ul>
                                </div>
                                <div class="col-md-4">
                                    {{-- spph non --}}
                                    <ul class="unstyled-list">
                                        <li><div class="pretty p-icon p-round p-jelly">
                                            <input type="checkbox" id="menu-spph-p"/>
                                            <div class="state p-primary">
                                                <i class="icon fa fa-check"></i>
                                                <label>SPPH</label>
                                            </div>
                                        </div></li>
                                        <ul class="unstyled-list ckbox-child">
                                            @for ($i = 0; $i < count($menu_spph); $i++)
                                            <li><div class="pretty p-icon p-round p-jelly">
                                                <input type="checkbox" name="menuAccess[]" value="{{ $menu_spph[$i][1] }}" class="menu-spph-c" />
                                                <div class="state p-primary">
                                                    <i class="icon fa fa-check"></i>
                                                    <label id="testlabel">{{ $menu_spph[$i][0] }}</label>
                                                </div>
                                            </div></li>
                                            @endfor
                                        </ul>
                                    </ul>
                                    {{-- bakn non --}}
                                    <ul class="unstyled-list">
                                        <li><div class="pretty p-icon p-round p-jelly">
                                            <input type="checkbox" id="menu-bakn-p"/>
                                            <div class="state p-primary">
                                                <i class="icon fa fa-check"></i>
                                                <label>BAKN</label>
                                            </div>
                                        </div></li>
                                        <ul class="unstyled-list ckbox-child">
                                            @for ($i = 0; $i < count($menu_bakn); $i++)
                                            <li><div class="pretty p-icon p-round p-jelly">
                                                <input type="checkbox" name="menuAccess[]" value="{{ $menu_bakn[$i][1] }}" class="menu-bakn-c" />
                                                <div class="state p-primary">
                                                    <i class="icon fa fa-check"></i>
                                                    <label id="testlabel">{{ $menu_bakn[$i][0] }}</label>
                                                </div>
                                            </div></li>
                                            @endfor
                                        </ul>
                                    </ul>
                                    {{-- spk non --}}
                                    <ul class="unstyled-list">
                                        <li><div class="pretty p-icon p-round p-jelly">
                                            <input type="checkbox" id="menu-spk-non-p"/>
                                            <div class="state p-primary">
                                                <i class="icon fa fa-check"></i>
                                                <label>SP3 & SPK</label>
                                            </div>
                                        </div></li>
                                        <ul class="unstyled-list ckbox-child">
                                            @for ($i = 0; $i < count($menu_spk_non); $i++)
                                            <li><div class="pretty p-icon p-round p-jelly">
                                                <input type="checkbox" name="menuAccess[]" value="{{ $menu_spk_non[$i][1] }}" class="menu-spk-non-c" />
                                                <div class="state p-primary">
                                                    <i class="icon fa fa-check"></i>
                                                    <label id="testlabel">{{ $menu_spk_non[$i][0] }}</label>
                                                </div>
                                            </div></li>
                                            @endfor
                                        </ul>
                                    </ul>
                                    {{-- Kontrak Non --}}
                                    <ul class="unstyled-list">
                                        <li><div class="pretty p-icon p-round p-jelly">
                                            <input type="checkbox" id="menu-kontrak-non-p"/>
                                            <div class="state p-primary">
                                                <i class="icon fa fa-check"></i>
                                                <label>Kontrak</label>
                                            </div>
                                        </div></li>
                                        <ul class="unstyled-list ckbox-child">
                                            @for ($i = 0; $i < count($menu_kontrak_non); $i++)
                                            <li><div class="pretty p-icon p-round p-jelly">
                                                <input type="checkbox" name="menuAccess[]" value="{{ $menu_kontrak_non[$i][1] }}" class="menu-kontrak-non-c" />
                                                <div class="state p-primary">
                                                    <i class="icon fa fa-check"></i>
                                                    <label id="testlabel">{{ $menu_kontrak_non[$i][0] }}</label>
                                                </div>
                                            </div></li>
                                            @endfor
                                        </ul>
                                    </ul>
                                    
                                    
                                    
                                    
                                </div>
                                <div class="col-md-4">
                                    {{-- spph non --}}
                                    <ul class="unstyled-list">
                                        <li><div class="pretty p-icon p-round p-jelly">
                                            <input type="checkbox" id="menu-spph-lkpp-p"/>
                                            <div class="state p-primary">
                                                <i class="icon fa fa-check"></i>
                                                <label>SPPH LKPP</label>
                                            </div>
                                        </div></li>
                                        <ul class="unstyled-list ckbox-child">
                                            @for ($i = 0; $i < count($menu_spph_lkpp); $i++)
                                            <li><div class="pretty p-icon p-round p-jelly">
                                                <input type="checkbox" name="menuAccess[]" value="{{ $menu_spph_lkpp[$i][1] }}" class="menu-spph-lkpp-c" />
                                                <div class="state p-primary">
                                                    <i class="icon fa fa-check"></i>
                                                    <label id="testlabel">{{ $menu_spph_lkpp[$i][0] }}</label>
                                                </div>
                                            </div></li>
                                            @endfor
                                        </ul>
                                    </ul>
                                    {{-- bakn  --}}
                                    <ul class="unstyled-list">
                                        <li><div class="pretty p-icon p-round p-jelly">
                                            <input type="checkbox" id="menu-bakn-lkpp-p"/>
                                            <div class="state p-primary">
                                                <i class="icon fa fa-check"></i>
                                                <label>BAKN LKPP</label>
                                            </div>
                                        </div></li>
                                        <ul class="unstyled-list ckbox-child">
                                            @for ($i = 0; $i < count($menu_bakn_lkpp); $i++)
                                            <li><div class="pretty p-icon p-round p-jelly">
                                                <input type="checkbox" name="menuAccess[]" value="{{ $menu_bakn_lkpp[$i][1] }}" class="menu-bakn-lkpp-c" />
                                                <div class="state p-primary">
                                                    <i class="icon fa fa-check"></i>
                                                    <label id="testlabel">{{ $menu_bakn_lkpp[$i][0] }}</label>
                                                </div>
                                            </div></li>
                                            @endfor
                                        </ul>
                                    </ul>
                                    {{-- bak --}}
                                    <ul class="unstyled-list">
                                            <li><div class="pretty p-icon p-round p-jelly">
                                                <input type="checkbox" id="menu-bak-lkpp-p"/>
                                                <div class="state p-primary">
                                                    <i class="icon fa fa-check"></i>
                                                    <label>BAK LKPP</label>
                                                </div>
                                            </div></li>
                                            <ul class="unstyled-list ckbox-child">
                                                @for ($i = 0; $i < count($menu_bak_lkpp); $i++)
                                                <li><div class="pretty p-icon p-round p-jelly">
                                                    <input type="checkbox" name="menuAccess[]" value="{{ $menu_bak_lkpp[$i][1] }}" class="menu-bak-lkpp-c" />
                                                    <div class="state p-primary">
                                                        <i class="icon fa fa-check"></i>
                                                        <label id="testlabel">{{ $menu_bak_lkpp[$i][0] }}</label>
                                                    </div>
                                                </div></li>
                                                @endfor
                                            </ul>
                                        </ul>
                                    {{-- Kontrak LKPP --}}
                                    <ul class="unstyled-list">
                                        <li><div class="pretty p-icon p-round p-jelly">
                                            <input type="checkbox" id="menu-kontrak-non-p"/>
                                            <div class="state p-primary">
                                                <i class="icon fa fa-check"></i>
                                                <label>Kontrak LKPP</label>
                                            </div>
                                        </div></li>
                                        <ul class="unstyled-list ckbox-child">
                                            @for ($i = 0; $i < count($menu_kontrak); $i++)
                                            <li><div class="pretty p-icon p-round p-jelly">
                                                <input type="checkbox" name="menuAccess[]" value="{{ $menu_kontrak[$i][1] }}" class="menu-kontrak-c" />
                                                <div class="state p-primary">
                                                    <i class="icon fa fa-check"></i>
                                                    <label id="testlabel">{{ $menu_kontrak[$i][0] }}</label>
                                                </div>
                                            </div></li>
                                            @endfor
                                        </ul>
                                    </ul>
                                    
                                    {{-- spk lkpp --}}
                                    <ul class="unstyled-list">
                                        <li><div class="pretty p-icon p-round p-jelly">
                                            <input type="checkbox" id="menu-spk-p"/>
                                            <div class="state p-primary">
                                                <i class="icon fa fa-check"></i>
                                                <label>SP3 & SPK LKPP</label>
                                            </div>
                                        </div></li>
                                        <ul class="unstyled-list ckbox-child">
                                            @for ($i = 0; $i < count($menu_spk); $i++)
                                            <li><div class="pretty p-icon p-round p-jelly">
                                                <input type="checkbox" name="menuAccess[]" value="{{ $menu_spk[$i][1] }}" class="menu-spk-c" />
                                                <div class="state p-primary">
                                                    <i class="icon fa fa-check"></i>
                                                    <label id="testlabel">{{ $menu_spk[$i][0] }}</label>
                                                </div>
                                            </div></li>
                                            @endfor
                                        </ul>
                                    </ul>
                                </div>
                            </div>   
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    
    @endsection
    
    @section('scripts')
    <script type="text/javascript">
        $("#menu-pbs-p").click(function () {
            let items = $('.menu-pbs-c');
            items.not(this).prop('checked', this.checked);
        });
        $("#menu-spph-p").click(function () {
            let items = $('.menu-spph-c');
            items.not(this).prop('checked', this.checked);
        });
        $("#menu-bakn-p").click(function () {
            let items = $('.menu-bakn-c');
            items.not(this).prop('checked', this.checked);
        });
        $("#menu-spk-p").click(function () {
            let items = $('.menu-spk-c');
            items.not(this).prop('checked', this.checked);
        });
        $("#menu-spk-non-p").click(function () {
            let items = $('.menu-spk-non-c');
            items.not(this).prop('checked', this.checked);
        });
        $("#menu-kontrak-p").click(function () {
            let items = $('.menu-kontrak-c');
            items.not(this).prop('checked', this.checked);
        });
        $("#menu-kontrak-non-p").click(function () {
            let items = $('.menu-kontrak-non-c');
            items.not(this).prop('checked', this.checked);
        });
        $("#menu-mdata-p").click(function () {
            let items = $('.menu-mdata-c');
            items.not(this).prop('checked', this.checked);
        });
        $("#menu-ar-p").click(function () {
            let items = $('.menu-ar-c');
            items.not(this).prop('checked', this.checked);
        });
        $("#menu-spph-lkpp-p").click(function () {
            let items = $('.menu-spph-lkpp-c');
            items.not(this).prop('checked', this.checked);
        });
        $("#menu-bakn-lkpp-p").click(function () {
            let items = $('.menu-bakn-lkpp-c');
            items.not(this).prop('checked', this.checked);
        });
        $("#menu-bak-lkpp-p").click(function () {
            let items = $('.menu-bak-lkpp-c');
            items.not(this).prop('checked', this.checked);
        });
        $("#menu-dbsales-p").click(function () {
            let items = $('.menu-dbsales-c');
            items.not(this).prop('checked', this.checked);
        });
        $("#menu-pipeline-p").click(function () {
            let items = $('.menu-pipeline-c');
            items.not(this).prop('checked', this.checked);
        });
    </script>
    @endsection
    