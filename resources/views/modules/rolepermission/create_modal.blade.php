<!-- modal -->
<div class="modal fade" id="modal-add">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"></span></button>
            <h4 class="modal-title">Add Role</h4>
          </div>
          <div class="modal-body">
           <!-- form start -->
            <form method="POST" action="{{ route('rpmanage.store') }}">
              @csrf
              @method('POST')
              
              @if (count($errors) > 0)
              <div class="alert alert-danger">
                There was a problem, please check your form carefully.
                <ul>
                  @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
              @endif

              <div class="form-group">
                <label>Role name*</label>
                <input name="roleName" type="text" class="form-control" value="{{ old('roleName') }}" autofocus required>
              </div>
              <div class="form-group">
                <label>Role display*</label>
                <input name="roleDisplay" type="text" class="form-control" value="{{ old('roleDisplay') }}" autofocus required>
              </div>
              <div class="pretty p-icon p-round p-jelly">
                <input type="checkbox" />
                <div class="state p-primary">
                  <i class="icon fa fa-check"></i>
                  <label>Interested</label>
                </div>
              </div>
  
              <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary pull-right">Submit</button>
            {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
          </div>
              
            </form>
           <!-- form end -->
          </div>
          
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    