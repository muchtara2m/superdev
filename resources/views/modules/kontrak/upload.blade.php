@extends('layouts.master')

@section('title')
Kontrak Upload| Super Slim
@endsection

@section('stylesheets')

<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            KONTRAK UPLOAD
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active"> Kontrak Upload </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            @if (\Session::has('success'))
            <div class="alert alert-success">
                <p>{{ \Session::get('success') }}</p>
            </div><br />
            @endif
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Kontrak</h3>
                    </div>
                    <!-- /.box-header -->
                    <div style="text-align:center;padding-bottom:10px">
                            <div class="row input-daterange">
                                <form method="get" action="/upload-file-kontrak-lkpp" enctype="multipart/form-data">
                                    <div class="col-md-4">
                                        <select name="bulan" id="bulan" class="form-control">
                                            @php
                                            if(request()->get('bulan') == null){
                                                $bln = date('m');
                                                $bulan = date('F',strtotime(date('Y-m-d')));
                                                $thn = date('Y');
                                            }else{
                                                $bln = request()->get('bulan');
                                                $bulan = date('F', mktime(0, 0, 0, request()->get('bulan'), 10));
                                                $thn = request()->get('tahun');
                                            }
                                            @endphp
                                            <option value="{{ $bln }}" selected>{{ $bulan }}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select name="tahun" id="tahun" class="form-control">
                                            <option value="{{ $thn }}" selected>{{ $thn }}</option>
                                        </select>
                                    </div>
                                    <input type="submit" value="Filter" class="btn btn-primary">
                                    
                                    <div class="col-md-4">
                                    </div>
                                </form>
                            </div>
                        </div>
                    <div class="box-body table-responsive">
                        <table id="kontrak" class="display">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>IO</th>
                                    <th>Deskripsi IO</th>
                                    <th>Nomor SPPH</th>
                                    <th>Nomor Kontrak</th>
                                    <th>Judul SPPH</th>
                                    <th>Tangal BAKN</th>
                                    <th>Tanggal Kontrak</th>
                                    <th>Harga</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody style="white-space:nowrap">
                                @php
                                $no=1;
                                @endphp
                                @foreach ($kontrak as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->bakn_lkpp['io_id']== 0 ? 'Tidak Diinput': $item->bakn_lkpp['io_id']  }}</td>
                                    <td>{{ $item->bakn_lkpp->io_lkpp['deskripsi']== 0 ? 'Tidak Diinput': $item->bakn_lkpp->io_lkpp['deskripsi']  }}</td>
                                    <td>{{ $item->bakn_lkpp->spph_lkpps['nomorspph'] }}</td>
                                    <td>{{ $item->nomor_kontrak }}</td>
                                    <td>{{ $item->bakn_lkpp->spph_lkpps['judul'] }}</td>
                                    <td>{{ date('d.F.Y', strtotime($item->bakn_lkpp['tglbakn'])) }}</td>
                                    @if ($item->tanggal_kontrak != null)
                                    <td>{{ date('d.F.Y', strtotime($item->tanggal_kontrak)) }}</td>     
                                    @else
                                    <td></td>                                       
                                    @endif
                                    <td>{{ number_format($item->bakn_lkpp['harga']) }}</td>
                                    <td>
                                        <a href="#modal" data-id="{{ $item->id }}" data-toggle="modal" title="Upload File" class="upload fa fa-fw fa-file"></a>
                                        <a href="{{ url('kontrak-non-print_preview', $item->id)}}" class="fa fa-file-word-o" title="Print Preview"></a>
                                        <a target="_blank" href="{{ url('preview-kontrak-lkpp', $item->id) }}" class="fa fa-fw fa-file-code-o" title="Preview Kontrak"></a>
                                        @if (Auth::user()->level == 'administrator')
                                        <a href ="delete-kontrak-lkpp/{{ $item->id }}" class="fa fa-fw fa-trash"  onclick="return confirm('Bener nih mau dihapus?')" title="Delete Kontrak"></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @include('modules.bakn_lkpp.modal.file_modal')
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<script src="{{ asset('js/web/monthAndYear.js') }}"></script>
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
    $(document).ready( function () {
        $('#kontrak').DataTable({
            scrollY: "50vh",
        // scrollX: true,
        scrollCollapse: true,
        });
    } );

    $(document).on("click", ".upload", function () {
        var idbro = $(this).data('id');
        // $(".modal-body #idnya").val( idbro );
        $('form').attr('action',"{{ url('file-kontrak-lkpp')}}/"+idbro);
    }); 
</script>
@endsection
