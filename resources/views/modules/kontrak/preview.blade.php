@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Kontrak";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Kontrak
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Preview Kontrak</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
                    </div>
                    <div class="box-body">
                        <div class="box-body">
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <textarea name="isi"  cols="180" rows="10">
                                        {{ $kontrak->isi }}
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <!-- /.box -->
                </div>
                
            </div>
            <!-- /.row -->
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>

<script>
    
    $('textarea').froalaEditor({
        toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'inlineClass', 'clearFormatting', '|', 'emoticons', 'fontAwesome', 'specialCharacters', 'paragraphFormat', 'lineHeight', 'paragraphStyle', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', 'insertHR', 'selectAll', 'help', 'html', 'fullscreen', 'undo', 'redo', 'getPDF', 'print'],
        placeholderText: '',
        documentReady: true,
        key: '{{ env("KEY_FROALA") }}',
    })
    
</script>
@endsection
