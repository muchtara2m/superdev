@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Edit Kontrak";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Kontrak
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                There was a problem, please check your form carefully.
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Edit Kontrak</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i
                            class="fa fa-arrow-left"></i></button>
                        @if ($chat != NULL)
                        <div class="form-group col-md-12" style="margin-top:2%">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Comment From {{ strtoupper($chat->username) }}</label>
                                {{-- {{ $bakns->id }} --}}
                                <textarea name="" id="chat" cols="30" rows="10">
                                    {{ $chat->chat }}
                                </textarea>
                            </div>
                        </div>
                        @endif
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="{{ url('update-kontrak-lkpp',$kontraks->id) }}"
                        enctype="multipart/form-data" id="form">
                        @csrf
                        <div class="box-body">
                            <div class="box-body">
                                <div class="col-md-12">
                                    <div class="form-group col-md-6 igroup">
                                        <label for="exampleInputEmail1">Nomor Kontrak</label>
                                        <input type="text" class="form-control" name="nokontrak" id="nmrkontrak"
                                            value="{{ $kontraks->nomor_kontrak }}">
                                    </div>
                                    <div class="form-group col-md-6 igroup">
                                        <label for="tglbakn">Tanggal Kontrak</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" data-date="" data-date-format="yyyy-mm-dd"
                                                class="form-control datejos" name="tglkontrak" id="enddelivery-date"
                                                value="{{ $kontraks->tanggal_kontrak }}">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 igroup">
                                        <label for="jenis_kontrak">Jenis Kontrak</label>
                                        <select name="jenis_kontrak" id="jenis_kontrak" class="form-control">
                                            @foreach ($jenis as $item)
                                            <option value="{{ $item->id }}" {{ $kontraks->jenis_kontrak == $item->id ? 'selected':'' }}>{{ $item->jenis_pasal }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div id="centang">

                                </div>
                                <div class="form-group col-md-12">
                                    <div class="form-group">
                                        <textarea name="isi" id="isi">
                                            {{ $kontraks->isi }}
                                        </textarea>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Lampiran</label>
                                            <br>*if you want submit this field is required
                                            <input type="file" name="lampiran[]" id="" class="form-control"
                                                multiple="multiple">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Comment</label>
                                            <br>*if you want submit this field is required
                                            <input type="text" name="chat" class="form-control"
                                                value="{{ old('chat') }}">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success" name="status"
                                    value="draft_kontrak">Save</button>
                                <button type="submit" class="btn btn-primary" name="status"
                                    value="save_kontrak">Submit</button>
                            </div>
                        </div>
                        <!-- /.box-body -->

                    </form>
                </div>
                <!-- /.box -->
            </div>

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>

<!-- Initialize the editor. -->
<script>
      $('#chat').froalaEditor({
        key: '{{ env("KEY_FROALA") }}',
        height: 70,
        toolbarButtons: ['help'],
        charCounterCount: false
    });
    function alertData() {
        confirm("Perhatikan Semua Form, jika tidak sesuai maka data yang tertulis otomatis di reset !");
    }

    function terbilangs(angka) {
        k = ['', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan'];
        a = [1000000000000000, 1000000000000, 1000000000, 1000000, 1000, 100, 10, 1];
        s = ['Kuadriliun', 'Trilyun', 'Milyar', 'Juta', 'Ribu', 'Ratus', 'Puluh', ''];
        var i = 0,
            x = '';
        var j = 0,
            y = '';

        var inp_pric = String(angka).split(",");
        console.log(String(angka));
        while (inp_pric[0] > 0) {
            b = a[i], c = Math.floor(inp_pric[0] / b), inp_pric[0] -= b * c;
            x += (c >= 10 ? terbilangs(c) + "" + s[i] + " " : ((c > 0 && c < 10) ? k[c] + " " + s[i] + " " : ""));
            i++;

        }
        var depan = x.replace(new RegExp(/Satu Puluh (\w+)/gi), '$1 Belas').replace(new RegExp(
            /satu (ribu|ratus|puluh|Belas)/gi), 'Se\$1');

        while (inp_pric[1] > 0) {
            z = a[j], d = Math.floor(inp_pric[1] / z), inp_pric[1] -= z * d;
            y += k[d] + " ";
            j++;
        }
        var belakang = y.replace(new RegExp(/Satu Puluh (\w+)/gi), '$1 Belas').replace(new RegExp(
            /satu (ribu|ratus|puluh|Belas)/gi), 'Se\$1');

        if (belakang != '') {
            return `${depan} koma ${belakang}`;
        } else {
            return depan;
        }

    };
    $(function () {
        $(".datejos").on("change", function () {
            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DD")
                .format(this.getAttribute("data-date-format"))
            )
        }).trigger("change")
    });
    $('.datejos').datepicker({
        autoclose: true,
        orientation: "bottom"
    });
    $('#enddelivery-date').on('change', function () {
        let tanggal = $(this).val();
        var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
        const date = new Date(tanggal);
        const month = date.toLocaleString('id-ID', {
            month: 'long'
        });
        const tgl = date.getDate() + ' ' + month + ' ' + date.getFullYear();
        const day = date.getDay();
        $('#hari').html(days[day]);
        $('#bulan').html(month);
        $('#spell_tanggal').html(terbilangs(date.getDate()));
        $('#spell_tahun').html(terbilangs(date.getFullYear()));
        $('#tgl_lengkap').html(`, ( ${tanggal} ) `);

    });
    $('#nmrkontrak').change(function () {
        var nomor = $('#nmrkontrak').val();
        document.getElementById('nmrnya').innerHTML = "Nomor : " + nomor;
        // $('#nmrnya').froalaEditor('html.set', nomor);
    });

    $('#isi').froalaEditor({
        documentReady: true,
        toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript',
            'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineClass', 'inlineStyle',
            'paragraphStyle', '|',
            'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-',
            'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons',
            'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html',
            '|', 'undo', 'redo', 'getPDF'
        ],
        key: '{{ env("KEY_FROALA") }}',
    })
    $('#submit').click(function (e) {
        var helpHtml = $('div#froala-editor').froalaEditor('html.get'); // Froala Editor Inhalt auslesen
        $.post("{{ action('KontrakController@store') }}", {
            helpHtml: helpHtml
        });
    });
    $('#jenis_kontrak').on('change', function () {
        let val = $(this).find('option:selected').val();
        var token = $("input[name='_token']").val();
        console.log(val);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'get',
            url: "{{ url('jenis-pasal') }}",
            data: {
                id: val,
                _token: token
            },
            dataType: 'json',
            success: function (data) {
                $('#jenis_kontraknya').html(`<p>${data.options['isi_pasal']}</p>`);
                if (data.options['id'] == 13) {
                    $(function () {
                        setTarget();
                        setPasal();
                        // $("#judul,#tgl,#handler").change(setTarget);
                        $("#tgl,#handler").change(setTarget);
                        var idnya = [
                            @foreach($pasals as $tes)
                            $("#pilih{{ $tes->pasal }}").change(setPasal),
                            @endforeach
                        ]

                        function setTarget() {
                            var tmp = $("#tgl").val() + " ";
                            tmp += $("#handler").val() + " ";
                        }

                        function setPasal() {
                            $(document).ready(function () {
                                $('input[name="all"],input[name="title"]').bind(
                                    'click',
                                    function () {
                                        var status = $(this).is(':checked');
                                        $('.dd').attr('checked', status);
                                        var isi = "";
                                        @foreach($pasals as $pasal)
                                        isi += $(
                                            "#pilih{{ $pasal->pasal }}:checked"
                                        ).val() || '';
                                        @endforeach
                                        document.getElementById('akhir')
                                            .innerHTML = isi;
                                        var isi = "";

                                    });
                            });
                            var isi = "";
                            @foreach($pasals as $pasal)
                            isi += $("#pilih{{ $pasal->pasal }}:checked").val() || '';
                            @endforeach
                            document.getElementById('akhir').innerHTML = isi;
                            var isi = "";

                        }
                    });
                    $('#centang').html(`
                    <div class="col-md-12" id="centang">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1" style="font-size: 20px;">Pasal</label>
                                        <br>
                                        <input type="checkbox" name="all" id="all" /> <label for='all'>Pilih Semua
                                        </label>
                                        <br>
                                        @foreach ($pasals as $list)
                                        <label class="checkbox-inline">
                                            <input type="checkbox" class="dd" id="pilih{{ $list->pasal }}" name="pasal"
                                                value="<p><strong>PASAL {{ $list->pasal }}<p>{{ $list->judul }}</strong>{{ $list->isi }}">
                                            {{ $list->judul }}
                                        </label>
                                        @endforeach
                                    </div>
                                </div>
                    `);
                } else {
                    $('#centang').empty();
                }
            },
            error: function () {

            }
        });
    });

</script>
@endsection