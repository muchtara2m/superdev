@extends('layouts.master')

@section('title')
Kontrak | Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            KONTRAK
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active"> Kontrak </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Kontrak</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- /.box-header -->
                    <div style="text-align:center;padding-bottom:10px">
                            <div class="row input-daterange">
                                <form method="get" action="{{ url('inprogress-kontrak-lkpp') }}" enctype="multipart/form-data">
                                    <div class="col-md-4">
                                        <select name="bulan" id="bulan" class="form-control">
                                            @php
                                            if(request()->get('bulan') == null){
                                                $bln = date('m');
                                                $bulan = date('F',strtotime(date('Y-m-d')));
                                                $thn = date('Y');
                                            }else{
                                                $bln = request()->get('bulan');
                                                $bulan = date('F', mktime(0, 0, 0, request()->get('bulan'), 10));
                                                $thn = request()->get('tahun');
                                            }
                                            @endphp
                                            <option value="{{ $bln }}" selected>{{ $bulan }}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select name="tahun" id="tahun" class="form-control">
                                            <option value="{{ $thn }}" selected>{{ $thn }}</option>
                                        </select>
                                    </div>
                                    <input type="submit" value="Filter" class="btn btn-primary">
                                    
                                    <div class="col-md-4">
                                    </div>
                                </form>
                            </div>
                        </div>
                    <div class="box-body table">
                        <table name="item" id="item" class="display">
                            <thead>
                                <tr id="kepala">
                                    <th rowspan="2" style="text-align:center">No</th>
                                    <th rowspan="2" style="text-align:center">No. IO</th>
                                    <th rowspan="2" style="text-align:center">No. SPH</th>
                                    <th rowspan="2" style="text-align:center">No. SPPH</th>
                                    <th rowspan="2" style="text-align:center">No. Kontrak</th>
                                    <th rowspan="2" style="text-align:center">Judul</th>
                                    <th rowspan="2" style="text-align:center">Mitra</th>
                                    <th rowspan="2" style="text-align:center">Tanggal BAKN</th>
                                    <th rowspan="2" style="text-align:center">Tanggal Buat BAKN</th>
                                    <th rowspan="2" style="text-align:center">Tanggal Kontrak</th>
                                    <th rowspan="2" style="text-align:center">Pembuat</th>
                                    <th rowspan="2" style="text-align:center">Tanggal Buat</th>
                                    <th rowspan="2" style="text-align:center">Harga</th>
                                    <th colspan="6" style="text-align:center">Approval</th>
                                    <th rowspan="2" style="text-align:center">Posisi</th>
                                    <th rowspan="2" style="text-align:center">Lampiran</th>
                                    <th rowspan="2" style="text-align:center">Preview</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center">Creator</th>
                                    <th style="text-align:center">Manager</th>
                                    <th style="text-align:center">Manager Catalogue & Partnership Management</th>
                                    <th style="text-align:center">GM E-Commerce</th>
                                    <th style="text-align:center">DIR Operation</th>
                                    <th style="text-align:center">DIR Utama</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                @php
                                $i = 1;
                                @endphp
                                @foreach ($kontrak as $item => $value)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $value->bakn_lkpp['io_id'] == 0 ? 'Tidak Diinput': $value->bakn_lkpp['io_id'] }}</td>
                                    <td>{{ $value->bakn_lkpp->spph_lkpps['nomorsph'] == 0 ? 'Tidak Diinput' : $value->bakn_lkpp->spph_lkpps['nomorsph'] }}</td>
                                    <td>{{ $value->bakn_lkpp->spph_lkpps['nomorspph'] }}</td>
                                    <td>{{ $value->nomor_kontrak }}</td>
                                    <td>{{ $value->bakn_lkpp->spph_lkpps['judul'] }}</td>
                                    <td>{{ $value->bakn_lkpp->spph_lkpps->mitra_lkpps['perusahaan'] }}</td>
                                    <td>{{ date('d.F.Y',strtotime($value->bakn_lkpp['tglbakn'])) }}</td>
                                    <td>{{ date('d.F.Y',strtotime($value->bakn_lkpp['created_at'])) }}</td>
                                    <td>{{ date('d.F.Y',strtotime($value->tanggal_kontrak)) }}</td>
                                    <td>{{ $value->user_kontrak_lkpp['name'] }}</td>
                                    <td>{{ date('d.F.Y',strtotime($value->created_at)) }}</td>
                                    <td>{{ number_format($value->bakn_lkpp['harga']) }}</td>
                                    <td style="white-space:nowrap">
                                        @php
                                    
                                        for ($i=0; $i < count($value->chatnya); $i++) {
                                            if ($value->chatnya[$i]['queue']== 0) {
                                                echo '<br>('.date('d.F.Y H:i', strtotime($value->chatnya[$i]['created_at'])).' - '.$value->chatnya[$i]['status'].')<br>'.$value->chatnya[$i]['name'];
                                            }
                                        }
                                        @endphp
                                    </td>
                                    <td style="white-space:nowrap">
                                        @php
                                     
                                        for ($i=0; $i < count($value->chatnya); $i++) {
                                            if ($value->chatnya[$i]['queue']== 1) {
                                                echo '<br>('.date('d.F.Y H:i', strtotime($value->chatnya[$i]['created_at'])).' - '.$value->chatnya[$i]['status'].')<br>'.$value->chatnya[$i]['name'];
                                            }
                                        }
                                        @endphp
                                    </td>
                                    <td style="white-space:nowrap">
                                        @php
                                        for ($i=0; $i < count($value->chatnya); $i++) {
                                            if ($value->chatnya[$i]['queue']== 2) {
                                                echo '<br>('.date('d.F.Y H:i', strtotime($value->chatnya[$i]['created_at'])).' - '.$value->chatnya[$i]['status'].')<br>'.$value->chatnya[$i]['name'];
                                            }
                                        }
                                        @endphp
                                    </td>
                                    <td style="white-space:nowrap">
                                        @php
                                        for ($i=0; $i < count($value->chatnya); $i++) {
                                            if ($value->chatnya[$i]['queue']== 3) {
                                                echo '<br>('.date('d.F.Y H:i', strtotime($value->chatnya[$i]['created_at'])).' - '.$value->chatnya[$i]['status'].')<br>'.$value->chatnya[$i]['name'];
                                            }
                                        }
                                        @endphp
                                    </td>
                                    <td style="white-space:nowrap">
                                        @php
                                        for ($i=0; $i < count($value->chatnya); $i++) {
                                            if ($value->chatnya[$i]['queue']== 4) {
                                                echo '<br>('.date('d.F.Y H:i', strtotime($value->chatnya[$i]['created_at'])).' - '.$value->chatnya[$i]['status'].')<br>'.$value->chatnya[$i]['name'];
                                            }
                                        }
                                        @endphp
                                    </td>
                                    <td style="white-space:nowrap">
                                            @php
                                            for ($i=0; $i < count($value->chatnya); $i++) {
                                                if ($value->chatnya[$i]['queue']== 5) {
                                                    echo '<br>('.date('d.F.Y H:i', strtotime($value->chatnya[$i]['created_at'])).' - '.$value->chatnya[$i]['status'].')<br>'.$value->chatnya[$i]['name'];
                                                }
                                            }
                                            @endphp
                                        </td>
                                    <td>{{ $value->approval }}</td>
                                    @php
                                    $flampiran = json_decode($value->lampiran, TRUE);
                                    $tlampiran = json_decode($value->title_lampiran, TRUE);
                                    @endphp
                                    <td style="white-space:nowrap">
                                        @php
                                        $x=1;
                                        @endphp
                                        @if($flampiran != NULL)
                                        @foreach ($tlampiran as $keylampiran =>$vallampiran)
                                        {{ $x++.'. ' }}<a href="{{ Storage::url($flampiran[$keylampiran]) }}">{{ $tlampiran[$keylampiran] }}</a><br>
                                        @endforeach
                                        @endif
                                        
                                    </td>
                                    <td style="white-space:nowrap">
                                        <a href="{{ url('preview-spph-lkpp/'.$value->bakn_lkpp['spph_id']) }}">Preview SPPH</a><br>
                                        <a href="{{ url('preview-bakn-lkpp/'.$value->bakn_id) }}">Preview BAKN</a><br>
                                        <a href="{{ url('preview-kontrak-lkpp/'.$value->id) }}">Preview Kontrak</a><br>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<script src="{{ asset('js/web/monthAndYear.js') }}"></script>
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>

<script type="text/javascript">

    // DataTable
    var otable = $('.display').DataTable({
        scrollY: "50vh",
        scrollX: true,
        scrollCollapse: true,
    });

    
    $(document).on("click", ".upload", function () {
        var idbro = $(this).data('id');
        // $(".modal-body #idnya").val( idbro );
        $('form').attr('action',"{{ url('kontrak-non-upload')}}/"+idbro);
    }); </script>
    @endsection
    