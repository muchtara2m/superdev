@if ($jeniskontrak == 'KHS')
<p style="text-align: center;"><strong>KONTRAK HARGA SATUAN (KHS)</strong></p>
@else
<p style="text-align: center;"><strong>PERJANJIAN KERJASAMA</strong></p>
@endif
<p style="text-align: center;"><strong>TENTANG</strong></p>
<p style="text-align: center;"><strong>{{ strtoupper($judul) }}</strong></p>
<p style="text-align: center;"><strong>PT. PINS INDONESIA&nbsp;</strong></p>
<p style="text-align: center;"><strong>DENGAN</strong></p>
<p style="text-align: center;"><strong>{{ strtoupper($perusahaan) }}</strong></p>
<p style="text-align: center;">
    <strong>
        <u>___________________________________________________________________________________________</u>
        {{--  <u>_____________________________________________________________________________</u>  --}}
    </strong>
</p>

<p style="text-align: center;"><u><strong id="nmrnya">Nomor :</strong></u></p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>

<div title="Page 1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
    {{--  <p style="text-align: justify;">Pada hari ini, <strong>{{ Carbon\Carbon::parse($sesok)->formatLocalized('%A') }}</strong>, tanggal <strong>{{ $f->format($tanggal) }}</strong>, bulan <strong>{{ Carbon\Carbon::parse($sesok)->formatLocalized('%B') }}</strong> tahun <strong>{{ $f->format($tahun) }}</strong>, (<strong>{{ $dtnow }}</strong>), bertempat di Jakarta, oleh dan antara pihak-pihak:</p>  --}}
    <p style="text-align: justify;">Pada hari ini, <strong><span id="hari">........................</span></strong>, tanggal <strong><span id="spell_tanggal">.........</span></strong>, bulan <strong><span id="bulannya">..............</span>.</strong> tahun <strong><span id="spell_tahun">.............</span></strong><span id="tgl_lengkap">, (<strong>..........</strong>)</span>, bertempat di Jakarta, oleh dan antara pihak-pihak:</p>


    <ol style="list-style-type: upper-roman;">
        <li style="text-align: justify;"><strong>PT. PINS INDONESIA</strong>, perseroan terbatas yang didirikan berdasarkan hukum Negara Republik Indonesia, berkedudukan di Telkom Landmark Tower lantai 42-43, Jl. Gatot Subroto No.Kav. 52, Kuningan Barat, Mampang Prapatan, Kota Jakarta Selatan Daerah Khusus Ibukota Jakarta 12710, dalam perbuatan hukum ini diwakili secara sah oleh <strong>{{ strtoupper($end->name) }}</strong>, Jabatan <strong>{{ strtoupper($end->position) }}</strong>, selanjutnya disebut PINS;
            <br>
            <br>
        </li>
        <li style="text-align: justify;"><strong>{{$perusahaan }}</strong>, perusahaan yang didirikan berdasarkan hukum Negara Republik Indonesia, berkedudukan di {{ $alamat }}, dalam perbuatan hukum ini diwakili secara sah oleh <strong> {{ $direktur }}</strong>, Jabatan <strong> Direktur Utama</strong>, selanjutnya disebut SUPPLIER.
            <br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</li>
        </ol>
        <p style="text-align: justify;"><strong>PINS</strong> dan <strong>SUPPLIER</strong> apabila secara bersama-sama untuk selanjutnya disebut &rdquo;Para Pihak&rdquo;.</p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        <p style="text-align: justify;">Dengan terlebih dahulu mempertimbangkan hal-hal sebagai berikut :</p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        <ol style="list-style-type: lower-alpha;">
            <li style="text-align: justify;">bahwa PINS bermaksud menjalin kerjasama dengan SUPPLIER dalam Pekerjaan {{ $judul }}.</li>
            <li>
                <p style="text-align: justify;">Bahwa PINS telah menyampaikan kepada SUPPLIER No. {{ $nomorspph }} tanggal {{ date('d F Y', strtotime($tanggalspph)) }} Perihal Surat Permintaan Penawaran Harga;</p>
            </li>
            <li>
                <p style="text-align: justify;">Bahwa SUPPLIER telah menyampaikan kepada PINS surat No. {{ $nomorsph }} tanggal {{ date('d F Y', strtotime($tanggalsph)) }} perihal Penawaran Harga;</p>
            </li>
            <li>
                <p style="text-align: justify;">bahwa Para Pihak telah melaksanakan rapat klarifikasi dan negosiasi pada tanggal {{ date('d F Y', strtotime($tanggalbakn)) }} yang hasilnya dituangkan dalam Berita Acara Klarifikasi dan Negosiasi;</p>
            </li>
        </ol>
        <p style="text-align: justify;">Setelah mempertimbangkan hal-hal tersebut di atas, Para Pihak sepakat untuk mengikatkan diri satu kepada yang lain dan dituangkan dalam Perjanjian Pekerjaan {{ $judul }} (selanjutnya disebut &ldquo;Perjanjian&rdquo;) dengan ketentuan dan syarat-syarat sebagai berikut :</p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        <br>

        <p id="jenis_kontraknya">
            
        </p>
      
        <p id="akhir"></p>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tbody>
                <tr>
                    <td style="width: 226pt;padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="49.42528735632184%">

                        <p style="text-align:center;"><strong><span style='font-size:15px;font-family:"Calibri",sans-serif;'>PT. PINS INDONESIA</span></strong></p>
                    </td>
                    <td style="width: 231.2pt;padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="50.57471264367816%">

                        <p style="text-align:center;"><strong><span style='font-size:15px;font-family:"Calibri",sans-serif;'>PT.&nbsp;</span></strong><strong><span style='font-size:15px;font-family:  "Calibri",sans-serif;'>BISMACINDO PERKASA</span></strong></p>
                    </td>
                </tr>
                <tr>
                    <td style="width: 226pt;padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="49.42528735632184%">

                        <p style="text-align:center;">
                            <br>
                        </p>

                        <p style="text-align:center;">
                            <br>
                        </p>

                        <p style="text-align:center;">
                            <br>
                        </p>

                        <p style="text-align:center;">
                            <br>
                        </p>

                        <p>
                            <br>
                        </p>

                        <p>
                            <br>
                        </p>

                        <p style="text-align:center;"><strong><u><span style='font-size:15px;font-family:"Calibri",sans-serif;'>{{ $end->name }}</span></u></strong></p>

                        <p style="text-align:center;"><span style='font-size:15px;font-family:"Calibri",sans-serif;'>{{ $end->position }}</span></p>
                    </td>
                    <td style="width: 231.2pt;padding: 0cm 5.4pt;vertical-align: top;" valign="top" width="50.57471264367816%">

                        <p style="text-align:center;">
                            <br>
                        </p>

                        <p style="text-align:center;">
                            <br>
                        </p>

                        <p style="text-align:center;">
                            <br>
                        </p>

                        <p style="text-align:center;">
                            <br>
                        </p>

                        <p style="text-align:center;">
                            <br>
                        </p>

                        <p style="text-align:center;">
                            <br>
                        </p>
                        <p style="text-align:center;"><strong><u><span style='font-size:15px;font-family:"Calibri",sans-serif;color:black;'>{{ $direktur }}</span></u></strong></p>
                        <p style="text-align:center;"><span style='font-size:15px;font-family:"Calibri",sans-serif;'>Direktur Utama</span></p>
                    </td>
                </tr>
            </tbody>
        </table>