@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Create Kontrak";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<script src="https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js"></script>
@endsection
@section('customstyle')

<style type="text/css">

    <!-- Froala -->
    hr {
        border-top: 1px solid #000;
    }

    table tr:not(:first-child){
        cursor: pointer;transition: all .25s ease-in-out;
    }
    table tr:not(:first-child):hover{background-color: #ddd;}
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    .example-modal .modal {
        background: transparent !important;
    }
    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }
    .pulse {
        width: 20%;
        --color: #ef6eae;
        --hover: #ef8f6e;
    }
    .pulse:hover,
    .pulse:focus {
        -webkit-animation: pulse 1s;
        animation: pulse 1s;
        box-shadow: 0 0 0 2em rgba(255, 255, 255, 0);
    }

    @-webkit-keyframes pulse {
        0% {
            box-shadow: 0 0 0 0 var(--hover);
        }
    }

    @keyframes pulse {
        0% {
            box-shadow: 0 0 0 0 var(--hover);
        }
    }
    .close:hover,
    .close:focus {
        box-shadow: inset -3.5em 0 0 0 var(--hover), inset 3.5em 0 0 0 var(--hover);
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Kontrak
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Kontrak</h3>
                        {{-- <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button> --}}

                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="{{ action('KontrakController@store') }}" enctype="multipart/form-data">
                        <input type="hidden" name="idbakn" value="{{ $bakns->id }}">
                        @csrf
                        <div class="box-body">
                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    There was a problem, please check your form carefully.
                                    <ul>
                                        @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                            <div class="box-body">
                                <div class="col-md-12" hidden>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Judul</label>
                                        <input type="text" class="form-control" name="judul" value="{{ $bakns->judul }}" id="judul">
                                    </div>
                                </div>
                                <div class="col-md-6" hidden>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nomor BAKN</label>
                                        <input type="text" class="form-control" name="nomorBakn" value="{{ $bakns->nomor_spph }}" id="handler" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6" hidden>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Tanggal</label>
                                        <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datejos" name="tanggal" id="tgl">
                                    </div>
                                </div>
                                <div class="col-md-6" hidden>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Harga</label>
                                        <input type="text" class="form-control" name="harga" value="{{ $bakns->harga }}" id="">

                                    </div>
                                </div>
                                <div class="col-md-6" hidden>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Handler</label>
                                        <input type="text" class="form-control" name="handler" value="{{ $bakns->handler }}" id="" readonly>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1" style="font-size: 20px;">Pasal</label>
                                        <br>
                                        @foreach ($pasals as $list)
                                        <label class="checkbox-inline">
                                            <input type="checkbox" id="pilih{{ $list->pasal }}" name="pasal" value="<p><strong>PASAL {{ $list->pasal }}<p>{{ $list->judul }}</strong>{{ $list->isi }}"> {{ $list->judul }}
                                            </label>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            @php
                                            //date_default_timezone_set('Asia/Jakarta');
                                            setlocale(LC_TIME, "id_ID");
                                            $hari = strftime("%A", strtotime(date('Y-m-d')));
                                            $bulan =strftime("%B", strtotime(date('Y-m-d')));
                                            $dtnow = date('d-m-Y');
                                            $tanggal = date('d');
                                            $tahun = date('Y');
                                            $f = new NumberFormatter("id", NumberFormatter::SPELLOUT);
                                            //echo $f->format($tanggal);

                                            @endphp
                                            <textarea name="isi"  cols="180" rows="10">
                                                <p style="text-align: center;"><strong>PERJANJIAN KERJASAMA</strong></p>
                                                <p style="text-align: center;"><strong>TENTANG</strong></p>
                                                <p style="text-align: center;"><strong>{{ $bakns->judul }}</strong></p>
                                                <p style="text-align: center;"><strong>PT. PINS INDONESIA&nbsp;</strong></p>
                                                <p style="text-align: center;"><strong>DENGAN</strong></p>
                                                <p style="text-align: center;"><strong>{{ $bakns->pihak_kedua }}</strong></p>
                                                <p style="text-align: center;"><u><strong>Nomor :{{$bakns->nomor_spph}}</strong></u></p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>

                                                <div title="Page 1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                                                    <p style="text-align: justify;">Pada hari ini, <strong>{{$hari}}</strong>, tanggal <strong>{{ $f->format($tanggal) }}</strong>, bulan <strong>{{$bulan}}</strong> tahun <strong>{{ $f->format($tahun) }}</strong>, (<strong>{{ $dtnow }}</strong>), bertempat di Jakarta, oleh dan antara pihak-pihak:</p>
                                                    <ol style="list-style-type: upper-roman;">
                                                        <li style="text-align: justify;"><strong>PT. PINS INDONESIA</strong>, perseroan terbatas yang didirikan berdasarkan hukum Negara Republik Indonesia, berkedudukan di Plaza Kuningan, Annex Building Lantai 7, Jalan HR. Rasuna Said Kav. C11 &ndash; C14, Jakarta Selatan 12940, dalam perbuatan hukum ini diwakili secara sah oleh <strong>MOHAMMAD FIRDAUS</strong>, Jabatan <strong>Direktur Utama</strong>, selanjutnya disebut PINS;
                                                            <br>
                                                            <br>
                                                        </li>
                                                        <li style="text-align: justify;"><strong>{{ $bakns->pihak_kedua }}</strong>, perusahaan yang didirikan berdasarkan hukum Negara Republik Indonesia, berkedudukan di {{ $mitra->alamat }}, dalam perbuatan hukum ini diwakili secara sah oleh <strong> {{ $mitra->direktur }}</strong>, Jabatan <strong> Direktur Utama</strong>, selanjutnya disebut SUPPLIER.
                                                            <br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</li>
                                                        </ol>
                                                        <p style="text-align: justify;"><strong>PINS</strong> dan <strong>SUPPLIER</strong> apabila secara bersama-sama untuk selanjutnya disebut &rdquo;Para Pihak&rdquo;.</p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <p style="text-align: justify;">Dengan terlebih dahulu mempertimbangkan hal-hal sebagai berikut :</p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <ol style="list-style-type: lower-alpha;">
                                                            <li style="text-align: justify;">bahwa PINS bermaksud menjalin kerjasama dengan SUPPLIER dalam Pekerjaan {{ $bakns->judul }}.</li>
                                                            <li>
                                                                <p style="text-align: justify;">Bahwa PINS telah menyampaikan kepada SUPPLIER No. {{ $bakns->nomor_spph }} tanggal 9 Oktober 2018 Perihal Surat Permintaan Penawaran Harga;</p>
                                                            </li>
                                                            <li>
                                                                <p style="text-align: justify;">Bahwa SUPPLIER telah menyampaikan kepada PINS surat 10 Oktober 2018 perihal Penawaran Harga;</p>
                                                            </li>
                                                            <li>
                                                                <p style="text-align: justify;">bahwa Para Pihak telah melaksanakan rapat klarifikasi dan negosiasi pada tanggal 31 Oktober 2018 yang hasilnya dituangkan dalam Berita Acara Klarifikasi dan Negosiasi;</p>
                                                            </li>
                                                            <li>
                                                                <p>bahwa PINS telah menyampaikan kepada SUPPLIER Surat Nomor: {{ $bakns->nomor_spph }} tanggal 31 Oktober 2018 Perihal Surat Penetapan Pelaksanaan Pekerjaan &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                                    <br>
                                                                    <br>
                                                                </p>
                                                            </li>
                                                        </ol>
                                                        <p style="text-align: justify;">Setelah mempertimbangkan hal-hal tersebut di atas, Para Pihak sepakat untuk mengikatkan diri satu kepada yang lain dan dituangkan dalam Perjanjian Pekerjaan {{ $bakns->judul }} (selanjutnya disebut &ldquo;Perjanjian&rdquo;) dengan ketentuan dan syarat-syarat sebagai berikut :</p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                        <p>
                                                            <br>
                                                        </p>&nbsp; &nbsp;</div>
                                                        <p id="akhir"></p>
                                                    </textarea>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="">Comment</label>
                                                        <input type="text" name="chat" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input type="text" name="created_by" value="{{ Auth::id() }}" hidden>
                                                        <button type="submit" class="btn btn-success" name="status" value="1">Save</button>
                                                        <button type="submit" class="btn btn-primary" name="status" id="submit" value="2">Submit</button>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- /.box-body -->

                                </form>
                            </div>
                            <!-- /.box -->
                        </div>

                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            @endsection

            @section('scripts')

            <!-- Include Editor JS files. -->
            <script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
            <script src="{{ asset('froala/js/languages/id.js') }}"></script>

            <script>
                $(function(){
                    $(".datejos").on("change", function() {
                        this.setAttribute(
                        "data-date",
                        moment(this.value, "YYYY-MM-DD")
                        .format( this.getAttribute("data-date-format") )
                        )
                    }).trigger("change")
                    $('.datejos').datepicker({
                        autoclose: true,
                        orientation: "bottom"
                    })
                })</script>
                <!-- Initialize the editor. -->
                <script>
                    $(function() {
                        setTarget();
                        setPasal();
                        // $("#judul,#tgl,#handler").change(setTarget);
                        $("#tgl,#handler").change(setTarget);
                        var idnya = [
                        @foreach($pasals as $tes)
                        $("#pilih{{ $tes->pasal }}").change(setPasal),
                        @endforeach
                        ]
                        function setTarget() {
                            // var tmp = $("#judul").val()+" ";
                            // tmp += $("#tgl").val()+" ";
                            // tmp += $("#handler").val()+" ";
                            var tmp = $("#tgl").val()+" ";
                            tmp += $("#handler").val()+" ";
                            // document.getElementById('hasil').innerHTML = tmp;
                        }
                        function setPasal() {
                            var isi = "";
                            @foreach($pasals as $pasal)
                            isi += $("#pilih{{ $pasal->pasal }}:checked").val() || '';
                            @endforeach
                            document.getElementById('akhir').innerHTML = isi;

                        }
                    });
                    $('textarea').froalaEditor({
                        documentReady: true,
                        toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo', 'getPDF'],
                        language: 'id',
                        key:'7D4A4F3I3cA5A4B3F2E4B2D2E3D1A3vxyA-9kB-8cH-7B-22C-16D2eC-9ykI2ytB4tz==',
                    })
                    $('#submit').click(function(e){
                        var helpHtml = $('div#froala-editor').froalaEditor('html.get'); // Froala Editor Inhalt auslesen
                        $.post( "{{ action('KontrakController@store') }}", { helpHtml:helpHtml });
                    });</script>

                    <!-- Data Table -->
                    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
                    <!-- date-range-picker -->
                    <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
                    <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
                    <!-- bootstrap datepicker -->
                    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
                    <!-- iCheck 1.0.1 -->
                    <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
                    <!-- clockpicker -->
                    <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
                    <!-- Select2 -->
                    <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
                    <!-- CK Editor -->
                    {{-- <script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script> --}}

                    @endsection
