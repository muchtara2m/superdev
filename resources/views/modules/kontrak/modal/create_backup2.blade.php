@extends('layouts.master')

@php
if (isset($bakns)) {
    $nomor = $bakns->id;
    $jeniskontrak = $bakns->jenis_kontrak;
    $tanggalbakn = $bakns->tglbakn;
    $lastupdate = $bakns->updated_at;
    $judul = $bakns->spph_lkpps['judul'];
    $nomorspph = $bakns->spph_lkpps['nomorspph'];
    $tanggalspph = $bakns->spph_lkpps['tglspph'];
    $nomorsph = $bakns->spph_lkpps['nomorsph'];
    $tanggalsph = $bakns->spph_lkpps['tglsph'];
    $perusahaan = $bakns->spph_lkpps->mitra_lkpps['perusahaan'];
    $alamat = $bakns->spph_lkpps->mitra_lkpps['alamat'];
    $direktur = $bakns->spph_lkpps->mitra_lkpps['direktur'];
    $harga = $bakns->harga;
}
$homelink = "/home";
$crpagename = "Create Kontrak";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet"
href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Kontrak
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Kontrak</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i
                            class="fa fa-arrow-left"></i></button>
                            <br>
                            <br>
                            <a href="{{ url('preview-bakn-lkpp', $bakns->id) }}" target="_blank"
                                class="btn-round pull-right" title="Preview BAKN">Preview BAKN</a>
                                <td class="btn-round pull-right">
                                    @php
                                    if($bakns->file == NULL){
                                    }else{
                                        $title = json_decode($bakns->title, TRUE);
                                        $file = json_decode($bakns->file, TRUE);
                                        $i=1;
                                        foreach ($title as $key => $value) {
                                            echo $i++.'. <a href="'.Storage::url($file[$key]).'">'.$title[$key].'</a><br>';
                                        }
                                    }
                                    @endphp
                                </td>
                            </div>
                            <!-- /.box-header -->
                            
                            <!-- form start -->
                            <form method="post" action="{{ url('store-kontrak-lkpp') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="box-body">
                                    @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        There was a problem, please check your form carefully.
                                        <ul>
                                            @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    <div class="box-body">
                                        <div class="col-md-12">
                                            <div class="form-group col-md-6 igroup">
                                                <label for="exampleInputEmail1">Nomor Kontrak</label>
                                                <input type="text" class="form-control" name="nokontrak" id="nmrkontrak"
                                                value="{{ old('nokontrak') }}">
                                                <input type="hidden" name="baknid" value="{{ $bakns->id }}">
                                            </div>
                                            <div class="form-group col-md-6 igroup">
                                                <label for="tglbakn">Tanggal Kontrak</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" data-date="" data-date-format="yyyy-mm-dd"
                                                    class="form-control datejos" name="tglkontrak" id="enddelivery-date"
                                                    value="{{ old('tglkontrak') }}">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        @if($jeniskontrak == 'Custom')
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1" style="font-size: 20px;">Pasal</label>
                                                <br>
                                                <input type="checkbox" name="all" id="all" /> <label for='all'>Pilih Semua
                                                </label>
                                                <br>
                                                @foreach ($pasals as $list)
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" class="dd" id="pilih{{ $list->pasal }}" name="pasal"
                                                    value="<p><strong>PASAL {{ $list->pasal }}<p>{{ $list->judul }}</strong>{{ $list->isi }}">
                                                        {{ $list->judul }}
                                                    </label>
                                                    @endforeach
                                                </div>
                                            </div>
                                            @endif
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    @php
                                                    //date_default_timezone_set('Asia/Jakarta');
                                                    setlocale(LC_TIME, "id_ID");
                                                    
                                                    $tglbakn = $tanggalbakn;
                                                    $date1 = str_replace('-', '/', $tglbakn);
                                                    $tomorrow = date('Y-m-d',strtotime($date1 . "+1 days"));
                                                    $hari = date('l', strtotime($tomorrow));
                                                    $bulan = date('F', strtotime($tomorrow));
                                                    
                                                    $dtnow = date('d-m-Y', strtotime($tomorrow));
                                                    $tanggal = date('d', strtotime($tomorrow));
                                                    $tahun = date('Y', strtotime($tomorrow));
                                                    $f = new NumberFormatter("id", NumberFormatter::SPELLOUT);
                                                    //echo $f->format($tanggal);
                                                    $d = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $lastupdate);
                                                    $sesok = $d->addDays(1);
                                                    
                                                    @endphp
                                                    <textarea name="isi" id="isi" cols="180" rows="10">
                                                        @include('modules.kontrak.modal.preview_create')
                                                    </textarea>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="">Lampiran</label>
                                                        <input type="file" name="lampiran[]" id="" class="form-control"
                                                        multiple="multiple">
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="">Comment</label>
                                                        <span>*This field is required for submit</span>
                                                        <input type="text" name="chat" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <input type="text" name="created_by" value="{{ Auth::id() }}" hidden>
                                                        <button type="submit" class="btn btn-success" name="status"
                                                        value="draft_kontrak">Save</button>
                                                        <button type="submit" class="btn btn-primary" name="status"
                                                        value="save_kontrak">Submit</button>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    
                                </form>
                            </div>
                            <!-- /.box -->
                        </div>
                        
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            @endsection
            
            @section('scripts')
            
            <!-- Include Editor JS files. -->
            <script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
            <script src="{{ asset('froala/js/languages/id.js') }}"></script>
            
            <!-- date-range-picker -->
            <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
            <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
            <!-- bootstrap datepicker -->
            <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
            <!-- clockpicker -->
            <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
            <script>
                $(function () {
                    $(".datejos").on("change", function () {
                        this.setAttribute(
                        "data-date",
                        moment(this.value, "YYYY-MM-DD")
                        .format(this.getAttribute("data-date-format"))
                        )
                    }).trigger("change")
                });
                $('.datejos').datepicker({
                    autoclose: true,
                    orientation: "bottom"
                });
                $('#nmrkontrak').change(function () {
                    var nomor = $('#nmrkontrak').val();
                    document.getElementById('nmrnya').innerHTML = "Nomor : " + nomor;
                    
                    console.log(nomor);
                    // $('#nmrnya').froalaEditor('html.set', nomor);
                    
                });
                $(function () {
                    setTarget();
                    setPasal();
                    // $("#judul,#tgl,#handler").change(setTarget);
                    $("#tgl,#handler").change(setTarget);
                    var idnya = [
                    @foreach($pasals as $tes)
                    $("#pilih{{ $tes->pasal }}").change(setPasal),
                    @endforeach
                    ]
                    
                    function setTarget() {
                        var tmp = $("#tgl").val() + " ";
                        tmp += $("#handler").val() + " ";
                    }
                    
                    function setPasal() {
                        $(document).ready(function () {
                            $('input[name="all"],input[name="title"]').bind('click', function () {
                                var status = $(this).is(':checked');
                                $('.dd').attr('checked', status);
                                var isi = "";
                                @foreach($pasals as $pasal)
                                isi += $("#pilih{{ $pasal->pasal }}:checked").val() || '';
                                @endforeach
                                document.getElementById('akhir').innerHTML = isi;
                                var isi = "";
                                
                            });
                        });
                        var isi = "";
                        @foreach($pasals as $pasal)
                        isi += $("#pilih{{ $pasal->pasal }}:checked").val() || '';
                        @endforeach
                        document.getElementById('akhir').innerHTML = isi;
                        var isi = "";
                        
                    }
                });
                $('textarea').froalaEditor({
                    documentReady: true,
                    toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript',
                    'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|',
                    'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-',
                    'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons',
                    'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html',
                    '|', 'undo', 'redo', 'getPDF'
                    ],
                    language: 'id',
                    key: '{{ env("KEY_FROALA") }}',
                })
                $('#submit').click(function (e) {
                    var helpHtml = $('div#froala-editor').froalaEditor('html.get'); // Froala Editor Inhalt auslesen
                    $.post("{{ action('KontrakController@store') }}", {
                        helpHtml: helpHtml
                    });
                });
                
            </script>
            
            <!-- Data Table -->
            <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
            <!-- date-range-picker -->
            <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
            <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
            <!-- bootstrap datepicker -->
            <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
            </script>
            <!-- iCheck 1.0.1 -->
            <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
            <!-- clockpicker -->
            <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
            <!-- Select2 -->
            <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
            <!-- CK Editor -->
            {{-- <script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script> --}}
            
            @endsection
            