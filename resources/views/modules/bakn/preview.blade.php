@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "BAKN";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAKN
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">BAKN</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Preview BAKN</h3>
                        <button onclick="window.history.go(-1); return false;"
                            class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
                    </div>
                    @if($chat != NULL && (Auth::user()->level == 'mgrlegal' || Auth::user()->level == 'administrator') )
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Comment</label>
                        {{ $bakn->id }}
                        <textarea name="" class="chat" id="chat" cols="30" rows="10">
                            {{ $chat->chat }}
                        </textarea>
                    </div>
                    @endif
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="row" style="margin:2%">
                        <div class="col-md-12">
                            <div class="white-box">
                                @include('modules.bakn.inc.bakn_preview')
                            </div>
                        </div>
                    </div>
                    {{-- form  --}}
                    <div class="row">
                        <div class="col-lg-12">
                            @if($bakn->status != 'draft_bakn' && $bakn->handler == NULL)
                            <div class="container" style="padding-left:1%;">
                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    There was a problem, please check your form carefully.
                                    <ul>
                                        @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if ( (Auth::user()->level == 'mgrlegal' || Auth::user()->level == 'administrator')
                                && $bakn->kontraksnon == "[]")
                                <form action="{{ action('BaknController@returnbakn',$bakn->id) }}"
                                    method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Comment</label>
                                        <textarea name="chat" id="chat" class="chat form-control">
                                            {{ old('chat') }}
                                        </textarea>
                                    </div>
                                    <button type="submit" name="approval" value="return" class="btn btn-warning"
                                        style="width: 7em;"><i class="fa fa-refresh" ></i> Return</button>
                                </form>
                                <br>
                                <a href="#modal_unit{{ $bakn->id }}" data-toggle="modal"
                                    data-target="#modal-unit_{{ $bakn->id }}" class="btn btn-primary"
                                    title="Dispatch Kontrak"><i class="fa fa-user-plus"></i> Dispatch</a>

                                @endif
                            </div>
                            @endif
                            <br>
                        </div>
                        @include('modules.bakn.modal.disp_modal')
                    </div>
                </div>
            </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
{{-- signature --}}
<script src="{{ asset('signature/jSignature.min.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- clockpicker -->
<script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>

        $('#froala-editor').froalaEditor({
            // Define new table cell styles.
            toolbarButtons: ['print', 'html'],
            charCounterCount: false,
            fullPage: true,
            toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|',
                'fontFamily', 'fontSize', 'color', 'inlineStyle', 'inlineClass', 'clearFormatting', '|',
                'emoticons', 'fontAwesome', 'specialCharacters', 'paragraphFormat', 'lineHeight',
                'paragraphStyle', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '|',
                'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '-', 'insertHR',
                'selectAll', 'help', 'html', 'fullscreen', '|', 'undo', 'redo', 'getPDF', 'print'
            ],
            key: '{{ env("KEY_FROALA") }}',
        });
        $('.chat').froalaEditor({
            key: '{{ env("KEY_FROALA") }}',
            height: 70,
            toolbarButtons: ['help'],
            charCounterCount: false,

        });
    </script>
@endsection
