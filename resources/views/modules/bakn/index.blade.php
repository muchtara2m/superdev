@extends('layouts.master')

@section('title')
Bakn List | Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAKN LIST
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">BAKN</a></li>
            <li class="active"> BAKN LIST </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> BAKN LIST</h3>
                    </div>
                    <div style="text-align:center;padding-bottom:10px">
                        <div class="row input-daterange">
                            <form method="get" action="{{ route('list.bakn') }}" enctype="multipart/form-data">
                                <div class="col-md-4">
                                    <select name="bulan" id="bulan" class="form-control">
                                        @php
                                        if(request()->get('bulan') == null){
                                        $bln = date('m');
                                        $bulan = date('F',strtotime(date('Y-m-d')));
                                        $thn = date('Y');
                                        }else{
                                        $bln = request()->get('bulan');
                                        $bulan = date('F', mktime(0, 0, 0, request()->get('bulan'), 10));
                                        $thn = request()->get('tahun');
                                        }
                                        @endphp
                                        <option value="{{ $bln }}" selected>{{ $bulan }}</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select name="tahun" id="tahun" class="form-control">
                                        <option value="{{ $thn }}" selected>{{ $thn }}</option>
                                    </select>
                                </div>
                                <input type="submit" value="Filter" class="btn btn-primary">

                                <div class="col-md-4">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="pbsTable" class="display">
                            @csrf
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>IO</th>
                                    <th>Judul SPPH</th>
                                    <th>Nomor SPPH</th>
                                    <th>Mitra</th>
                                    <th>Created By</th>
                                    <th>Date Create</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $no=1;
                                @endphp
                                @foreach($bakns as $list)
                                <tr style="white-space:nowrap">
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $list->io['no_io'] }}</td>
                                    <td>{{ $list->spph['judul'] }}</td>
                                    <td>{{ $list->spph['nomorspph'] }}</td>
                                    <td>{{ $list->spph->mitras['perusahaan'] }}</td>
                                    <td>{{ $list->user['name'] }}</td>
                                    <td>{{ $list->created_at }}</td>
                                    <td>
                                        <a href="#modal" data-id="{{ $list->id }}" data-toggle="modal"
                                            title="Upload File" class="upload fa fa-fw fa-file"></a>
                                        <a href="{{ route('preview.bakn', $list->id) }}" class="fa fa-fw fa-file-code-o"
                                            title="Preview BAKN"></a>
                                        <a href="delete/{{ $list->id }}" class="fa fa-fw fa-trash"
                                            onclick="return confirm('Bener nih mau dihapus?')" title="Delete BAKN"></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
                <!-- /.box -->
                <div class="modal fade" id="modal">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Upload File</h4>
                            </div>
                            <div class="modal-body">
                                <form method="post" action="" enctype="multipart/form-data">
                                    @csrf
                                    {{-- @method('PATCH') --}}
                                    <input type="hidden" name="title" class="form-control">
                                    <br>
                                    <input type="file" name="file[]" id="" class="form-control" multiple="multiple">
                                    <span>*Jika file di return harap upload semua file sebelum return</span>
                                    <br>
                                    <br>
                                    <button type="submit" class="btn btn-success" style="width: 7em;"><i
                                            class="fa fa-check"></i>
                                        Submit</button>
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<script src="{{ asset('js/web/monthAndYear.js') }}"></script>
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    $('#pbsTable').DataTable({
        scrollY : "50vh",
        scrollCollapse : true,
    });
    $(document).on("click", ".upload", function () {
        var idbro = $(this).data('id');
        // $(".modal-body #idnya").val( idbro );
        $('form').attr('action', "{{ url('bakn-upload')}}/" + idbro);
    });
</script>

@endsection