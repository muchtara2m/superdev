 <!-- modal -->
 <div class="modal fade" id="modal-unit">
        <div class="modal-dialog" style="width: fit-content">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">List IO</h4>
                    </div>
                    <div class="modal-body">
                        <table id="io" class="display table-responsive">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>IO</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($io as $ios)
                                    <tr>
                                        <td>{{ $ios['id'] }}</td>
                                        <td>{{$ios['no_io']}}</td>
                                        <td>{{$ios['deskripsi']}}</td>
                                        <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->