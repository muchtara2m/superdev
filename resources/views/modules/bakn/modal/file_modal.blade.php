<div class="modal fade" id="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Upload File</h4>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="" enctype="multipart/form-data">
                            @csrf
                            {{-- @method('PATCH') --}}
                            <input type="hidden" name="title" class="form-control">
                            <br>
                            <input type="file" name="file[]" id="" class="form-control" multiple="multiple">
                            <span>*Jika file di return harap upload semua file sebelum return</span>
                            <br>
                            <br>
                            <button type="submit" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i>
                                Submit</button>
                            </form>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->