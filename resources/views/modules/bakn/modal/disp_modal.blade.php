<div class="modal fade" id="modal-unit_{{ $bakn->id }}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Pilih Tanggung Jawab</h4>
            </div>
            <div class="modal-body">
                <form method="post"
                    action="{{action('KontrakNonController@dispbakn', $bakn->id)}}">
                    @csrf
                    {{-- @method('PATCH') --}}
                    <input type="text" value="{{ $bakn->id }}" hidden>
                    <br>
                    <label for="handler">Penangung Jawab</label>

                    <select name="handler" class="form-control">
                        <option selected disabled>Pilih Penanggung Jawab</option>
                        @foreach ($handlers as $handler)
                        <option value="{{ $handler->username }}">
                            {{ $handler->name }}</option>
                        @endforeach
                    </select>
                    <br>
                    <button type="submit" class="btn btn-success" name="approval" value="approve"
                        style="width: 7em;"><i class="fa fa-check"></i>
                        Submit</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
