@extends('layouts.master')

@section('title')
Status BAKN | Super Slim
@endsection

@section('stylesheets')
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
{{-- Icon --}}
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<!-- DataTables -->

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
{{-- <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.3/css/dataTables.responsive.css"> --}}
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAKN
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">BAKN</a></li>
            <li class="active"> Draft </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Status</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="pbsTable" class="table table-bordered table-hover">
                            @csrf
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>IO</th>
                                    <th>Deskripsi IO</th>
                                    <th>Judul SPPH</th>
                                    <th>Tanggal BAKN</th>
                                    <th>No SPPH</th>
                                    <th>No SPH</th>
                                    <th>Harga</th>
                                    <th>Created By</th>
                                    <th>Date Created</th>
                                    <th>Last Update</th>
                                    @if(Auth::user()->level =='administrator')
                                    <th>Tipe</th>
                                    @endif
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $no=1;
                                @endphp
                                @foreach($bakns as $list)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $list->io['no_io'] }}</td>
                                    <td>{{ $list->io['deskripsi'] }}</td>
                                    <td>{{ $list->spph['judul'] }}</td>
                                    <td>{{ $list->tglbakn }}</td>
                                    <td>{{ $list->spph['nomorspph'] }}</td>
                                    <td>{{ $list->spph['nomorsph'] }}</td>
                                    <td>{{ number_format($list->harga,0,'.','.') }}</td>
                                    <td>{{ $list->user['name'] }}</td>
                                    <td>{{ $list->created_at }}</td>
                                    <td>{{ $list->updated_at }}</td>
                                    @if(Auth::user()->level =='administrator')
                                    <td>{{ $list->tipe }}</td>
                                    @endif
                                    <td>
                                        <a href="{{ url('bakn-edit', $list->id)}}" class="fa fa-fw fa-edit" title="Edit BAKN"></a>
                                        <a href = {{ url('bakn-preview', $list->id) }}" class="fa fa-fw fa-file-code-o" title="Preview BAKN"></a>
                                        <a href = "delete/{{ $list->id }}" class="fa fa-fw fa-trash" onclick="return confirm('Bener nih mau dihapus?')" title="Delete BAKN"></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        </div>

                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @endsection

    @section('scripts')
    <!-- DataTables -->
    <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>
    <script type="text/javascript">
        // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
        $(document).ready( function () {
            $('#pbsTable').DataTable({

            });
        } );</script>

        @endsection
