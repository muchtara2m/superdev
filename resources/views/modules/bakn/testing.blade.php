@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "BAKN";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<!-- Bootstrap Tagsinput -->
<link rel="stylesheet" href="{{ asset('bs-tagsinput/dist/bootstrap-tagsinput.css') }}">
@endsection

@section('customstyle')

<style type="text/css">
    table tr:not(:first-child){
        cursor: pointer;transition: all .25s ease-in-out;
    }
    table tr:not(:first-child):hover{background-color: #ddd;}
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    .example-modal .modal {
        background: transparent !important;
    }
    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAKN
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak & BAKN</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form BAKN</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="{{action('BaknController@update', $bakn->id)}}" enctype="multipart/form-data" class="form">
                        @csrf
                        <div class="box-body">
                            <div class="form-group col-md-12">
                                <div class="col-md-6">
                                    {{-- <input type="text" value="{{ $bakn->id }}"> --}}
                                    <label for="exampleInputEmail1">Nama Projek</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="iodes" name="projectname" value="{{ $bakn['projectname'] }}" placeholder="Nomor IO - Justifikasi" readonly>
                                        <!-- browse button -->
                                        <span class="input-group-btn">
                                            <label class="btn btn-primary" title="Browse" data-toggle="modal" data-target="#modal-unit">
                                                <span class="fa fa-search"></span>
                                                <span class="hidden-xs">Browse</span>
                                            </label>
                                        </span>
                                        <!-- preview button -->
                                        <span class="input-group-btn">
                                            <label class="btn btn-reverse red-ss" title="Preview" >
                                                <a href="/books/a-great-book.pdf" target="_blank"><span class="fa fa-file-pdf-o"></span></a>
                                            </label>
                                        </span>

                                    </div>
                                    <!-- modal -->
                                    <div class="modal fade" id="modal-unit">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Data Unit</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table id="io" class="display table-responsive">
                                                            <thead>
                                                                <tr>
                                                                    <th>IO</th>
                                                                    <th>Description</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($io as $ios)
                                                                <tr>
                                                                    <td>{{$ios['no_io']}}</td>
                                                                    <td>{{$ios['deskripsi']}}</td>
                                                                    <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                        {{--  <button type="button" class="btn btn-primary">Save changes</button>  --}}
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="exampleInputEmail1">Tanggal</label>
                                        <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control pull-right datejos" name="date" id="enddelivery-date" value="{{ $bakn->date }}">

                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="exampleInputEmail1">Tempat</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" value="{{ $bakn['place'] }}" name="place" placeholder="Kantor PT PINS Indonesia">
                                    </div>
                                    <div id="waktu_mulai" class="form-group clockpicker col-md-3">
                                        <label for="exampleInputEmail1">Waktu Mulai</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" value="{{ $bakn['timestart'] }}" name="timestart">
                                    </div>
                                    <div class="form-group clockpicker col-md-3">
                                        <label for="exampleInputEmail1">Waktu Akhir</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" name="endtime" value="{{ $bakn['endtime'] }}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="exampleInputEmail1">Pihak Pertama</label>
                                        <input type="text" class="form-control" value="PT. PINS Indonesia" readonly name="firstholder" value="{{ $bakn['firstholder'] }}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="exampleInputEmail1">Pihak Kedua</label>
                                        <select class="form-control" name="secondholder">
                                            <option selected disabled>{{ $bakn['secondholder'] }}</option>
                                            @foreach($mitra as $mtr)
                                            <option value="{{ $mtr['perusahaan'] }}">{{ $mtr['perusahaan'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="exampleInputEmail1">Nomor SPPH</label>
                                        <strong>
                                            <input type="text" class="form-control" name="spph" value="{{ $bakn['spph'] }}" readonly>
                                        </strong>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="exampleInputEmail1">Tanggal SPPH</label>

                                        <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control pull-right datejos" name="datespph" id="enddelivery-date" value="{{ $bakn->datespph }}">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="exampleInputEmail1">Nomor Penawaran</label>
                                        <strong>
                                            <input type="text" class="form-control"  value="{{ $bakn['offer'] }}" placeholder="1234/PN-WR/1/2018" name="offer">
                                        </strong>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="exampleInputEmail1">Tanggal Penawaran</label>

                                        <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control pull-right datejos" name="dateoffer" id="enddelivery-date" value="{{ $bakn->dateoffer }}">

                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="exampleInputEmail1">Undangan Dari</label>
                                        <input type="text" class="form-control" value="PT. PINS Indonesia" readonly name="invitation" value="{{ $bakn['invitation'] }}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="exampleInputEmail1">Tipe Undangan</label>
                                        <ul class="no-bullet">
                                            <li>
                                                <input type="checkbox" name="invittype[]" value="Review" {{ in_array("Review",$checkbox)? "checked":"" }}>
                                                <label for="minimal-checkbox-1">Review</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" name="invittype[]" value="Coordination" {{ in_array("Coordination",$checkbox)? "checked":"" }}>
                                                <label for="minimal-checkbox-2">Coordination</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" name="invittype[]" value="Briefing" {{ in_array("Briefing",$checkbox)? "checked":"" }}>
                                                <label for="minimal-checkbox-disabled">Briefing</label>
                                            </li>
                                            <li>
                                                <input type="checkbox" name="invittype[]" value="Decision Marking" {{ in_array("Decision Marking",$checkbox)? "checked":"" }}>
                                                <label for="minimal-checkbox-disabled-checked">Decision Making</label>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="exampleInputEmail1">Pimpinan Rapat</label>
                                        <select name="chairman" id="" class="form-control">
                                            <option value="{{ $bakn->chairman }}">{{ $bakn['chairman'] }}</option>
                                            @foreach ($pimpinan as $leader)
                                            <option value="{{ $leader['nama'] }}">{{ $leader['jabatan'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="exampleInputEmail1">Peserta Rapat</label>
                                        <div class="form-group input-group">
                                            <div class="input-group-addon select-addon">
                                                <span>PT PINS INDONESIA</span>
                                            </div>
                                            <input type="text" name="pinsparti" class="input-group form-control tags" id="pinsparti" data-role="tagsinput" value="{{ $bakn['pinsparti'] }}" placeholder="Pilih Peserta">
                                            {{--  <input type="text" value="" data-role="tagsinput"   --}}
                                        </div>
                                        <div class="form-group input-group">
                                            <div class="input-group-addon select-addon">
                                                <span>MITRA</span>
                                            </div>
                                            <input type="text" name="vendorparti" class="input-group form-control " placeholder="Pilih Peserta" value="{{ $bakn['vendorparti'] }}">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="exampleInputEmail1">Harga</label>
                                        <input type="text" class="form-control" name="nilai" placeholder="1000000" id="harganum" value="{{ $bakn->nilai }}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="exampleInputEmail1">Terbilang</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Seratus Ribu Rupiah">
                                    </div>
                                    {{-- <div class="form-group col-md-6">
                                        <label for="exampleInputEmail1">Tata Cara Pembayaran</label>
                                        <select class="form-control">
                                            <option selected disabled>Pilih Pihak Kedua</option>
                                            <option>Pembayaran DP dan bertahap</option>
                                            <option>Pembayaran DP dan Per-progress</option>
                                            <option>Pemb ayaran Perbulan</option>
                                            <option>Pembayaran Sekaligus</option>
                                        </select>
                                    </div> --}}

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <h4 class="divider-title">HASIL PEMBAHASAN</h4>
                                            <hr>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Agenda</label>
                                            <textarea id="cke-agenda" rows="10" cols="80" name="agenda" value="">
                                                {{ $bakn['agenda'] }}
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Dasar Pembahasan</label>
                                            <textarea id="cke-dasar-pembahasan" rows="10" cols="80" name="maindiscus" value="">
                                                {{ $bakn['maindiscus'] }}
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Ruang Lingkup</label>
                                            <textarea id="cke-ruang-lingkup" rows="10" cols="80" name="scope" value="">
                                                {{ $bakn['scope'] }}
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Lokasi Pekerjaan/Pengiriman Barang/Jasa</label>
                                            <textarea id="cke-lokasi-pekerjaan" rows="10" cols="80" name="service" value="">
                                                {{ $bakn['service'] }}
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Jangka waktu Pengiriman Barang</label>
                                            <textarea id="cke-jangka-waktu" rows="10" cols="80" name="periode" value="">
                                                {{ $bakn['periode'] }}
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Harga</label>
                                            <textarea id="cke-harga" rows="10" cols="80" name="price" value="">
                                                {{ $bakn['price'] }}
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Tata Cara Pembayaran</label>
                                            <textarea id="cke-tata-cara" rows="10" cols="80" name="methodpay" value="">
                                                {{ $bakn['methodpay'] }}
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Lain - lain</label>
                                            <textarea id="cke-lain-lain" rows="10" cols="80" name="other" value="{{ $bakn['other'] }}">
                                                {{ $bakn['other'] }}
                                            </textarea>
                                        </div>

                                    </div>
                                    <hr>


                                </div>


                            </div>
                            <!-- /.box-body -->
                            <input type="text" name="update_by" value="{{ Auth::id() }}" hidden>
                            <div class="box-footer">
                                <button type="submit" name="status" value="2" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
                                <input action="action" class="btn" onclick="window.history.go(-1); return false;" type="button" value="Back" />
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @endsection

    @section('scripts')
    <!-- Include Editor JS files. -->
    <script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>

    <!-- Initialize the editor. -->
    <script>
        $(function() {
            setTarget();
            $("#invit1,#invit2,#invit3,#invit4").change(setTarget);

            function setTarget() {
                var tmp = $("#invit1:checked").val() || '';
                tmp += $("#invit2:checked").val() || '';
                tmp += $("#invit3:checked").val() || '';
                tmp += $("#invit4:checked").val() || '';
                $('#targetTextField').val(tmp);
            }
        });

        $('textarea').froalaEditor({
            placeholderText: ''
        })
    </script>
    <!-- Data Table -->
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <!-- date-range-picker -->
    <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <!-- clockpicker -->
    <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- CK Editor -->
    {{-- <script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script> --}}
    <!-- Bootstrap Tagsinput -->
    <script src="{{ asset('bs-tagsinput/dist/bootstrap-tagsinput.js') }}"></script>

    <script type="text/javascript">
        var checkboxes = document.getElementsByName('invittype[]');
        var vals = "";
        for (var i=0, n=checkboxes.length;i<n;i++)
        {
            if (checkboxes[i].checked)
            {
                vals += ","+checkboxes[i].value;
            }
        }
        if (vals) vals = vals.substring(1);
        $(function() {

            $("#tags-input-pins").val();
            $("#tags-input-vendor").val();

            $('.bootstrap-tagsinput').tagsinput({
                confirmKeys: [13, 188]
            });

            $('.bootstrap-tagsinput input').on('keypress', function(e){
                if (e.keyCode == 13){
                    e.keyCode = 188;
                    e.preventDefault();
                };
            });

            /*$('.bootstrap-tagsinput').tagsinput({
                confirmKeys : [9]
            });*/

            $(".datejos").on("change", function() {
                this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DD")
                .format( this.getAttribute("data-date-format") )
                )
            }).trigger("change")
            //Initialize Select2 Elements
            $(".select2").select2();
            // Clockpicker
            $('.clockpicker').clockpicker({
                align: 'left',
                placement: 'bottom',
                autoclose: true,
                'default': 'now'
            });
            $('.datejos').datepicker({
                autoclose: true,
                orientation: "bottom"
            })
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-green',
                radioClass   : 'iradio_flat-green'
            })
            // datatable
            $(document).ready( function () {
                $('#io').DataTable();
            } )
            $( document ).ready(function() {
                function addCommas(nStr) {
                    nStr += '';
                    x = nStr.split(',');
                    x1 = x[0];
                    x2 = x.length > 1 ? '.' + x[1] : '';
                    var rgx = /(\d+)(\d{3})/;
                    while (rgx.test(x1)) {
                        x1 = x1.replace(rgx, '$1' + '.' + '$2');
                    }
                    if (x.length == 1){
                        x[1]="00";
                    }
                    return x1+","+x[1];
                };
                $('#harganum').change(function() {
                    borongvariabel = "<p>Total harga pekerjaan adalah sebesar Rp ";
                        var hrg = $('#harganum').val();
                        // var tes = borongvariabel+terbilangs(hrg)+"Rupiah";
                        var blg = terbilangs(hrg)+"Rupiah";
                        var tes = borongvariabel+addCommas($('#harganum').val())+" ( "+blg+" )"+"</p>";
                        // $('#nilai').val(tes);
                        $('#hargatext').val(tes);
                        // $('#harga_detail_ta').val(tes);
                        document.getElementById('harga_detail_ta').innerHTML = tes;
                        $('#test_harga').val(tes);
                        $('.trblng_hrg').val(tes);
                        $('.dt_hrg').val(hrg);

                    });
                    function terbilangs(n) {
                        k = ['','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan']  ;
                        a = [1000000000000000,1000000000000,1000000000,1000000,1000,100,10,1];
                        s = ['Kuadriliun','Trilyun','Milyar','Juta','Ribu','Ratus','Puluh',''];
                        var i=0, x='';
                        var j=0, y = '';
                        var angka = n;
                        //alert(angka);
                        var inp_pric = String(angka).split(",");
                        while(inp_pric[0]>0){

                            b = a[i], c = Math.floor(inp_pric[0]/b), inp_pric[0] -= b * c;

                            x += (c>=10 ? terbilangs(c) + ""+s[i]+" " : ((c > 0 && c < 10) ? k[c] + " "+s[i]+" " : ""));

                            i++;

                        }
                        var depan =  x.replace(new RegExp(/satu Puluh (\w+)/gi),'$1 belas').replace(new RegExp(/satu (ribu|ratus|puluh|belas)/gi),'Se\$1');

                        while(inp_pric[1]>0){

                            z = a[j], d = Math.floor(inp_pric[1]/z), inp_pric[1] -= z * d;

                            y += k[d]+" ";

                            j++;

                        }
                        var belakang =  y.replace(new RegExp(/satu Puluh (\w+)/gi),'$1 belas').replace(new RegExp(/satu (ribu|ratus|puluh|belas)/gi),'Se\$1');

                        if (belakang != ''){
                            return depan+"koma"+belakang;
                        }
                        else {
                            return depan;
                        }

                    };
                });

            })</script>

            <script>
                var table = document.getElementById('io');

                for(var i = 1; i < table.rows.length; i++)
                {
                    table.rows[i].onclick = function()
                    {
                        //  rIndex = this.rowIndex;
                        document.getElementById("iodes").value = this.cells[1].innerHTML;
                        // document.getElementById("iodes").value = this.cells[1].innerHTML;

                    };
                };</script>


                @endsection
