@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "BAKN";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
{{--  Tagify  --}}
<link rel="stylesheet" href="{{ asset('tagify/dist/tagify.css') }}">
@endsection

@section('customstyle')

<style type="text/css">
    table tr:not(:first-child) {
        cursor: pointer;
        transition: all .25s ease-in-out;
    }

    table tr:not(:first-child):hover {
        background-color: #ddd;
    }

    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }

    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }

    .example-modal .modal {
        background: transparent !important;
    }

    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }

</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAKN
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">BAKN</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Create BAKN</h3>
                    </div>
                    <!-- /.box-header -->
                    <br>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    {{-- start form  --}}
                    <form method="post" action="{{ route('store.bakn') }}" enctype="multipart/form-data" id="form">

                        @csrf
                        <div class="box-body">
                            <div class="form-group col-md-12 igroup">
                                <label for="nomorspph">Select SPPH *</label><a id="spphpreview" target="_blank"
                                    href="#"><i class="fa fa-fw fa-file-code-o"></i></a>
                                <input type="text" name="nmrspph" class="form-control" data-toggle="modal"
                                    data-target="#modal-unit-spph" placeholder="Pilih SPPH" id="nmrspph" readonly
                                    value="{{ old('nmrspph') }}">
                            </div>
                            <input type="hidden" id="spphid" name="spphid" value="{{ old('spphid') }}">
                            <div class="form-group col-md-3 igroup">
                                <label for="jenis_kontrak">Jenis PKS*</label>
                                <div class="input-group col-md-12">
                                    <select name="jenis_kontrak" id="jenis_kontrak" class="form-control">
                                        <option disabled selected>Pilih Jenis Kontrak</option>
                                        @foreach ($jenispasal as $item)
                                        <option value="{{ $item->jenis_pasal}}"
                                            {{   (old('jenis_kontrak') == $item->jenis_pasal)? 'selected':'' }}>
                                            {{ $item->jenis_pasal }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-3 igroup">
                                <label for="tglbakn">Tanggal BAKN*</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" data-date="" data-date-format="yyyy-mm-dd"
                                        class="form-control datejos" name="tglbakn" id="enddelivery-date"
                                        value="{{ old('tglbakn') }}">
                                </div>
                            </div>
                            <div class="form-group col-md-3 igroup">
                                <label for="harga">Harga*</label>
                                <div class="input-group col-md-12">
                                    <span class="input-group-addon">Rp</span>
                                    <input type="text" class="form-control" onkeyup="formatAngka(this,'.')" name="harga"
                                        placeholder="1000000" id="harganum" value="{{ old('harga') }}">
                                </div>
                            </div>
                            <div class="form-group col-md-3 igroup">
                                <label for="pimpinanrapat">Pimpinan Rapat*</label>
                                <div class="input-group col-md-12">
                                    <select name="pimpinan_rapat" id="pimpinan" class="form-control" title="Pimpinan">
                                        <option value="Revi Guspa"
                                            {{ (old('pimpinan_rapat') == 'Revi Guspa')? 'selected':'' }} selected>VP
                                            General
                                            Support</option>
                                        <option value="Sigit Sumarsono"
                                            {{ (old('pimpinan_rapat') == 'Sigit Sumarsono')? 'selected':'' }}>GM
                                            Regional Timur</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-3 igroup">
                                <label for="noio">No IO</label>
                                <div class="input-group col-md-12">
                                    <input type="hidden" name="io_id" id="io_id">
                                    <input type="text" name="noio" id="noio" class="form-control" data-toggle="modal" data-target="#modal-unit" readonly placeholder="Pilih IO"
                                        readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-3 igroup hidden">
                                <label for="desio">Deskripsi IO</label>
                                <div class="input-group col-md-12">
                                    <input type="text" name="desio" id="desio" class="form-control" title="Deskripsi"
                                        >
                                </div>
                            </div>
                            <div class="form-group col-md-3 ">
                                <label>Start Date*</label>

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <input type="text" name="start_date" data-date=""
                                        data-date-format="yyyy-mm-dd" class="form-control pull-right datejos" value="{{ old('start_date') }}">
                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group col-md-3 ">
                                <label>End Date*</label>

                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                    <input type="text" name="end_date" data-date=""
                                        data-date-format="yyyy-mm-dd" class="form-control pull-right datejos" value="{{ old('end_date') }}">
                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group col-md-3 igroup">
                                <label class="control-label">Tipe Undangan</label>
                                <br>
                                <div class="col-md-6">
                                    <ul class="no-bullet">
                                        <li>
                                            <input type="checkbox" name="tipe_undangan[]" value="Review" id="invit1"
                                                checked>
                                            <label for="minimal-checkbox-1">Review</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" name="tipe_undangan[]" value="Coordination"
                                                id="invit2" checked>
                                            <label for="minimal-checkbox-2">Coordination</label>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <ul class="no-bullet">
                                        <li>
                                            <input type="checkbox" name="tipe_undangan[]" value="Briefing" id="invit3"
                                                checked>
                                            <label for="minimal-checkbox-disabled">Briefing</label>
                                        </li>
                                        <li>
                                            <input type="checkbox" name="tipe_undangan[]" value="Decision Marking"
                                                id="invit4">
                                            <label for="minimal-checkbox-disabled-checked">Decision Making</label>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="pesertarapat">Peserta Rapat</label>
                                <div class="form-group">
                                    <div class="input-group-addon select-addon">
                                        <span>PT PINS INDONESIA</span>
                                    </div>
                                    <input name="peserta_pins" class=" input-group tagify" id="tags-pins"
                                        placeholder="Pilih Peserta" value="{{ $peserta.','.Auth::user()->name }}">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="pesertarapat">Peserta Rapat</label>
                                <div class="form-group" id="mtr">
                                    <div class="input-group-addon select-addon">
                                        <span id="mitra">MITRA</span>
                                    </div>
                                    {{-- <input name="peserta_mitra" class=" input-group tagify"
                                                    id="direktur" placeholder="Pilih Peserta">--}}
                                    <input name="peserta_mitra" class=" input-group form-control" id="direktur"
                                        placeholder="Pilih Peserta" value="{{ old('peserta_mitra') }}">
                                </div>
                            </div>

                            {{-- hasil pembahasan --}}
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <h4 class="divider-title">HASIL PEMBAHASAN</h4>
                                    <hr>
                                </div>
                                <div class="form-group">
                                    <label for="agenda">Agenda*</label>
                                    <textarea id="agenda" class="bakn" rows="10" cols="80" name="agenda"
                                        placeholder="Type Something">
                                        {{ old('agenda') }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="dasarpembahasan">Dasar Pembahasan*</label>
                                    <textarea rows="10" class="bakn" cols="80" id="dasarpembahasan" name="dasar_pembahasan"
                                        placeholder="Type Something">
                                        {{ old('dasar_pembahasan') }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="ruanglingkip">Ruang Lingkup*</label>
                                    <textarea id="ruanglingkup" class="bakn" rows="10" cols="80" name="ruang_lingkup"
                                        placeholder="Type Something">
                                        {{ old('ruang_lingkup') }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="lokasipekerjaan">Lokasi Pekerjaan/Pengiriman Barang/Jasa*</label>
                                    <textarea id="cke-lokasi-pekerjaan" class="bakn" rows="10" cols="80" name="lokasi_pekerjaan"
                                        placeholder="Type Something">
                                        {{ old('lokasi_pekerjaan') }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="jangkawaktu">Jangka Waktu Pekerjaan* </label>
                                    <textarea id="cke-jangka-waktu" class="bakn" rows="10" cols="80" name="jangka_waktu"
                                        placeholder="Type Something">
                                        {{ old('jangka_waktu') }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="harga">Harga*</label>
                                    <textarea id="nilai" class="bakn" name="harga_terbilang">
                                        {{ old('harga_terbilang') }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="tatacara">Tata Cara Pembayaran*</label>
                                    <div>
                                        <div class="center">
                                            @foreach ($bayar as $item)
                                            <label>
                                                <input type="checkbox" name="carabayar[]" class="carabayar"
                                                    data-id={{ $item->id }} value="{{ $item->jenis }}" onchange="add()"
                                                    @if(in_array($item->jenis, old('carabayar', []))) checked @endif>
                                                {{$item->jenis }}
                                            </label>
                                            @endforeach
                                        </div>
                                    </div>
                                    <textarea rows="10" cols="80" class="bakn" name="cara_bayar" id="tatacara">
                                        {{ old('cara_bayar') }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Lain - lain</label>
                                    <textarea rows="10" cols="80" class="bakn" name="lain_lain">
                                        <p>PT. PINS Indonesia akan menerbitkan Perjanjian/Konrak untuk ditandatangani oleh MITRA sekaligus sebagai dokumen yang menyatakan bahwa MITRA bersedia dan sanggup :</p>
                                        <ol type="a">
                                            <li>Menyerahkan dan/atau melaksanakan barang dan/atau hasil pekerjaan tepat waktu, tepat jumlah, sesuai dengan ketentuan yang telah ditetapkan oleh PT. PINS Indonesia;</li>
                                            <li>Barang dan/atau hasil pekerjaan yang diserahkan MITRA tidak akan bertentangan dengan dan/atau melanggar hukum dan/atau melanggar Hak milik pihak lain;</li>
                                        </ol>
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Lampiran</label>
                                    <input type="file" name="lampiran[]" multiple class="form-control" id="">
                                </div>
                                <div class="form-group">
                                    <label for="">Chat</label><span>*require for submit </span>
                                    <textarea name="chat" id="chat" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                            {{-- end hasil pembahasan --}}
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" name="status" value="draft_bakn" class="btn btn-primary"
                                style="width: 7em;"><i class="fa fa-check"></i> Save</button>
                            <button type="submit" name="status" value="save_bakn" class="btn btn-success"
                                style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    {{-- end form --}}
                </div>
                <!-- /.box -->
                @include('modules.bakn.modal.io_modal')
                @include('modules.bakn.modal.spph_modal')
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
{{-- bahasa indonesia froala --}}
<script src="{{ asset('froala/js/languages/id.js') }}"></script>
<!-- Data Table -->
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- clockpicker -->
<script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Tagify -->
<script src="{{ asset('tagify/dist/jQuery.tagify.min.js') }}"></script>
<script src="{{ asset('tagify/dist/tagify.min.js') }}"></script>
<script src="{{ asset('js/bakn.js') }}"></script>
<script>
    // script text area\
    $('.bakn').froalaEditor({
        placeholderText: '',
        charCounterCount: false,
        key: '{{ env("KEY_FROALA") }}',
    });
    $('#chat').froalaEditor({
        height: 70,
        toolbarButtons: ['help'],
        charCounterCount: false,
        key: '{{ env("KEY_FROALA") }}',
    })
    $('select').select2();

    function getRoles(val) {
        $('#role-cm_role_text').val('');
        var data = $('#role-cm_role').select2('data').map(function (elem) {
            return elem.text
        });
        console.log(data);
        $('#role-cm_role_text').val(data);
        $('#role-cm_role').on('select2:unselecting', function (e) {
            $('#role-cm_role_text').val('');
        });
    }
    $(document).ready(function () {
        // $(document).change(function () {
        // });
        // onchange
        $('#jenis_kontrak').on('change', function () {
            var nomorspph = $('#nmrspph').val();
            var token = $("input[name='_token']").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: "{{ route('data-spph') }}",
                data: {
                    nomorspph: nomorspph,
                    _token: token
                },
                dataType: 'json', //return data will be json
                success: function (data) {
                    var nmr = data.options['nomorspph'];
                    $('#tipe_bakn').val('NON');
                    var nmr = data.options['nomorspph'];
                    var options = document.forms['form']['jenis_kontrak'].options;

                    $('[name=spphid]').val(data.options['id']);
                    $("#spphpreview").attr("href", "{{ url('spph-preview') }}/" + data
                        .options['id']);
                    $('#mitra').html(data.options['mitras']['perusahaan']);
                    $('#direktur').val(data.options['mitras']['direktur']);
                    const datespph = new Date(data.options['tglspph']);
                    const monthspph = datespph.toLocaleString('id-ID', {
                        month: 'long'
                    });
                    const tglspph = datespph.getDate() + ' ' + monthspph + ' ' + datespph
                        .getFullYear();
                    const datesph = new Date(data.options['tglsph']);
                    const monthsph = datesph.toLocaleString('id-ID', {
                        month: 'long'
                    });
                    const tglsph = datesph.getDate() + ' ' + monthsph + ' ' + datesph
                        .getFullYear();
                    var isinya2 = `<ol style="margin-bottom:0cm;">
                            <li><span>Surat Permintaan Penawaran Harga (SPPH) nomor: ${data.options['nomorspph']} tanggal ${tglspph} perihal permintaan penawaran harga;</span></li>
                            <li><span>Surat dari SUPPLIER nomor: ${data.options['nomorsph']} tanggal ${tglsph} perihal Penawaran Harga.</span></li>
                        </ol>`;
                    $('#dasarpembahasan').froalaEditor('html.set', isinya2);
                    $('#agenda').froalaEditor('html.set',
                        `Klarifikasi & Negosiasi Penawaran Harga dari SUPPLIER tentang ${data.options['judul']}.`
                    );
                    $('#ruanglingkup').froalaEditor('html.set',
                        `${data.options['judul']} dengan rincian sebagaimana tercantum pada point nomor 4.`
                    );
                },
                error: function () {

                }
            });
        });
        // onchange jenis_kontrak
    });
    $.extend({
        caraBayar: function (nilai) {
            // local var
            var token = $("input[name='_token']").val();
            var id = x;
            var isiCaraBayar = null;
            // jQuery ajax
            $.ajax({
                url: "{{ route('cara_bayar') }}",
                type: 'get',
                data: {
                    id: id,
                    _token: token
                },
                dataType: "json",
                async: false,
                success: function (data) {
                    isiCaraBayar = data.options['isi'];
                }
            });
            // Return the response text
            return isiCaraBayar;
        }
    });

</script>

@endsection
