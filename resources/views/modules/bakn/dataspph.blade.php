@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "BAKN";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
{{--  Tagify  --}}
<link rel="stylesheet" href="{{ asset('tagify/dist/tagify.css') }}">
@endsection

@section('customstyle')

<style type="text/css">
    table tr:not(:first-child){
        cursor: pointer;transition: all .25s ease-in-out;
    }
    table tr:not(:first-child):hover{background-color: #ddd;}
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    .example-modal .modal {
        background: transparent !important;
    }
    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAKN
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">BAKN</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Create BAKN</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <br>
                    <div class="container">
                        <div class="col-md">
                            <label for="nomorspph">Select SPPH</label>
                            <select name="nomorspph" id="nospph" class="form-control">
                                <option disabled selected>Nomor SPPH</option>
                                @foreach ($spph as $item)
                                <option value="{{ $item->nomorspph }}">{{ $item->nomorspph }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <form method="post" action="{{ route('bakn.store') }}" enctype="multipart/form-data" id="form">
                        <input type="hidden" name="spphid">
                        @csrf
                        <div class="box-body">
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                There was a problem, please check your form carefully.
                                <ul>
                                    @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            <div class="form-group col-md-12 igroup">
                                <label for="jenis_kontrak">Jenis PKS</label>
                                <div class="input-group col-md-12">
                                    <select name="jenis_kontrak" id="" class="form-control">
                                        <option disabled selected>Pilih Jenis Kontrak</option>
                                        @foreach ($jenispasal as $item)
                                        <option value="{{ $item->jenis_pasal }}">{{ $item->jenis_pasal }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            {{-- data spph --}}
                            <div class="form-group col-md-2 igroup">
                                <label for="nomorspph">Nomor SPPH</label>
                                <input type="text" class="form-control" id="nomorspph" disabled>
                            </div>
                            <div class="form-group col-md-2 igroup">
                                <label for="tglspph">Tanggal SPPH</label>
                                <input type="text" class="form-control" id="tglspph" disabled>
                            </div>
                            <div class="form-group col-md-2 igroup">
                                <label for="nomorsph">Nomor SPH</label>
                                <input type="text" class="form-control" id="nomorsph" disabled>
                            </div>
                            <div class="form-group col-md-2 igroup">
                                <label for="tglsph">Tanggal SPH</label>
                                <input type="text" class="form-control" id="tglsph" disabled>
                            </div>
                            <div class="form-group col-md-4 igroup">
                                <label for="judul">Judul</label>
                                <input type="text" class="form-control" id="judul" disabled>
                            </div>
                            {{-- end dataspph --}}
                            <div class="form-group col-md-6 igroup">
                                <label for="tglbakn">Tanggal BAKN</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datejos" name="tanggal" id="enddelivery-date" value="{{ old('tanggal') }}">
                                </div>
                            </div>
                            {{-- nomor io --}}
                            <div class="form-group col-md igroop">
                                <label for="">Nama Projek & IO</label>

                                <div class="input-group">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="desio" name="deskripsi_io" placeholder="Deskripsi IO" readonly value="{{ old('deskripsi_io') }}">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="noio" name="io" placeholder="Nomor IO" readonly value="{{ old('io') }}">
                                    </div>

                                    <!-- browse button -->
                                    <span class="input-group-btn">
                                        <label class="btn btn-primary" title="Browse" data-toggle="modal" data-target="#modal-unit">
                                            <span class="fa fa-search"></span>
                                            <span class="hidden-xs">Browse</span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            {{-- end io --}}
                            {{-- pimpinan rapat --}}
                            <div class="form-group col-md-6">
                                <label for="pimpinanrapat">Pimpinan Rapat</label>
                                <select name="pimpinan_rapat"  class="form-control">
                                    <option value="">Pilih Pimpinan</option>
                                    <option value="Revi Guspa">VP General Support</option>
                                    <option value="Sigit Sumarsono">GM Regional Timur</option>
                                    <option value="Hernadi Yoga Adhitya Tama">GM E-Commerce</option>
                                </select>
                            </div>
                            {{-- end pimpinan rapat --}}
                            {{-- harga --}}
                            <div class="form-group col-md-6">
                                <label for="harga">Harga </label>
                                <input type="text" class="form-control" name="harga" placeholder="1000000" id="harganum" value="{{ old('harga') }}" >
                                *harga yang dicantumkan sesudah PPN 10%
                            </div>
                            {{-- end harga --}}
                            {{-- peserta --}}
                            <div class="form-group col-md-6">
                                <label for="pesertarapat">Peserta Rapat</label>
                                <div class="form-group">
                                    <div class="input-group-addon select-addon">
                                        <span>PT PINS INDONESIA</span>
                                    </div>
                                    <input name="peserta_pins" class="input-group tagify"
                                    id="tags-pins" placeholder="Pilih Peserta" value="{{ $peserta.','.Auth::user()->name }}">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="pesertarapat">Peserta Rapat</label>
                                <div class="form-group" id="mtr">
                                    <div class="input-group-addon select-addon">
                                        <span id="mitra">MITRA</span>
                                    </div>
                                    <input name="peserta_mitra" class="input-group tagify"
                                    id="direktur" placeholder="Pilih Peserta">
                                </div>
                            </div>
                            {{-- end peserta --}}
                            {{-- tipe rapat --}}
                            <div class="form-group col-md-6">
                                <label class="control-label">Tipe Undangan</label>
                                <ul class="no-bullet">
                                    <li>
                                        <input type="checkbox" name="tipe_undangan[]" value="Review" id="invit1">
                                        <label for="minimal-checkbox-1">Review</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" name="tipe_undangan[]" value="Coordination" id="invit2" >
                                        <label for="minimal-checkbox-2">Coordination</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" name="tipe_undangan[]" value="Briefing" id="invit3">
                                        <label for="minimal-checkbox-disabled">Briefing</label>
                                    </li>
                                    <li>
                                        <input type="checkbox" name="tipe_undangan[]" value="Decision Marking" id="invit4">
                                        <label for="minimal-checkbox-disabled-checked">Decision Making</label>
                                    </li>
                                </ul>
                            </div>
                            {{-- end tipe rapat --}}
                            {{-- hasil pembahasan --}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h4 class="divider-title">HASIL PEMBAHASAN</h4>
                                    <hr>
                                </div>
                                <div class="form-group">
                                    <label for="agenda">Agenda</label>
                                    <textarea id="agenda" rows="10" cols="80" name="agenda" placeholder="Type Something">
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="dasarpembahasan">Dasar Pembahasan</label>
                                    <textarea  rows="10" cols="80" id="dasarpembahasan" name="dasar_pembahasan" placeholder="Type Something">
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="ruanglingkip">Ruang Lingkup</label>
                                    <textarea id="ruanglingkup" rows="10" cols="80" name="ruang_lingkup" placeholder="Type Something">

                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="lokasipekerjaan">Lokasi Pekerjaan/Pengiriman Barang/Jasa</label>
                                    <textarea id="cke-lokasi-pekerjaan" rows="10" cols="80" name="lokasi_pekerjaan" placeholder="Type Something">

                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="jangkawaktu">Jangka waktu Pengiriman Barang</label>
                                    <textarea id="cke-jangka-waktu" rows="10" cols="80" name="jangka_waktu" placeholder="Type Something">
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="harga">Harga</label>
                                    <textarea id="nilai" name="harga_terbilang" >
                                        {{ old('harga_terbilang') }}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="tatacara">Tata Cara Pembayaran</label>
                                    <div>
                                        @foreach ($bayar as $item)
                                        <label>
                                            <input type="checkbox" name="style" value="{{ $item->isi }}" class="carabayar" onchange="add()"> {{ $item->jenis }}
                                        </label>
                                        @endforeach
                                    </div>
                                    <textarea  rows="10" cols="80" name="cara_bayar"  id="tatacara">
                                        {{ old('cara_bayar') }}
                                        <p id="carakasar"></p>
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Lain - lain</label>
                                    <textarea rows="10" cols="80" name="lain_lain">
                                        <p>PT. PINS Indonesia akan menerbitkan Perjanjian/Konrak untuk ditandatangani oleh MITRA sekaligus sebagai dokumen yang menyatakan bahwa MITRA bersedia dan sanggup :</p>
                                        <ol type="a">
                                            <li>Menyerahkan dan/atau melaksanakan barang dan/atau hasil pekerjaan tepat waktu, tepat jumlah, sesuai dengan ketentuan yang telah ditetapkan oleh PT. PINS Indonesia;</li>
                                            <li>Barang dan/atau hasil pekerjaan yang diserahkan MITRA tidak akan bertentangan dengan dan/atau melanggar hukum dan/atau melanggar Hak milik pihak lain;</li>
                                        </ol>
                                    </textarea>
                                </div>
                            </div>
                            {{-- end hasil pembahasan --}}
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" name="status" value="draft_bakn" class="btn btn-primary" style="width: 7em;"><i class="fa fa-check"></i> Save</button>
                        <button type="submit" name="status" value="save_bakn" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
                <!-- modal -->
                <div class="modal fade" id="modal-unit">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Data Unit</h4>
                                </div>
                                <div class="modal-body">
                                    <table id="io" class="display table-responsive">
                                        <thead>
                                            <tr>
                                                <th>IO</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($io as $ios)
                                            <tr>
                                                <td>{{$ios['no_io']}}</td>
                                                <td>{{$ios['deskripsi']}}</td>
                                                <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
{{-- bahasa indonesia froala --}}
<script src="{{ asset('froala/js/languages/id.js') }}"></script>

<!-- Data Table -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- clockpicker -->
<script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Tagify -->
<script src="{{ asset('tagify/dist/jQuery.tagify.min.js') }}"></script>
<script src="{{ asset('tagify/dist/tagify.min.js') }}"></script>

{{-- javascript tambahan --}}
{{-- script textarea and tipe undangan  --}}
<script>
    $('textarea').froalaEditor({
        placeholderText: '',
        language: 'id',
        charCounterCount: false,
        key: '{{ env("KEY_FROALA") }}',
    })</script>
    {{-- end  --}}
    <script>

        $('select').select2();
        function getRoles(val) {
            $('#role-cm_role_text').val('');
            var data = $('#role-cm_role').select2('data').map(function(elem){ return elem.text} );
            console.log(data);
            $('#role-cm_role_text').val(data);
            $('#role-cm_role').on('select2:unselecting', function (e) {
                $('#role-cm_role_text').val('');
            });
        }</script>
        <script type="text/javascript">
            var checkboxes = document.getElementsByName('invittype[]');
            var vals = "";
            for (var i=0, n=checkboxes.length;i<n;i++)
            {
                if (checkboxes[i].checked)
                {
                    vals += ","+checkboxes[i].value;
                }
            }
            if (vals) vals = vals.substring(1);
            $(function() {
                $('.tagify').tagify();
                $(".datejos").on("change", function() {
                    this.setAttribute(
                    "data-date",
                    moment(this.value, "YYYY-MM-DD")
                    .format( this.getAttribute("data-date-format") )
                    )
                }).trigger("change")
                //Initialize Select2 Elements

                // Clockpicker
                $('.clockpicker').clockpicker({
                    align: 'left',
                    placement: 'bottom',
                    autoclose: true,
                    'default': 'now'
                });
                $('.datejos').datepicker({
                    autoclose: true,
                    orientation: "bottom"
                })
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass   : 'iradio_flat-green'
                })

                // fungsi terbilang
                $( document ).ready(function() {
                    function addCommas(nStr) {
                        nStr += '';
                        x = nStr.split(',');
                        x1 = x[0];
                        x2 = x.length > 1 ? '.' + x[1] : '';
                        var rgx = /(\d+)(\d{3})/;
                        while (rgx.test(x1)) {
                            x1 = x1.replace(rgx, '$1' + '.' + '$2');
                        }
                        if (x.length == 1){
                            x[1]="00";
                        }
                        return x1+","+x[1];
                    };
                    $('#harganum').change(function() {
                        borongvariabel = "Total harga pekerjaan adalah sebesar Rp ";
                        var hrg = $('#harganum').val();
                        var blg = terbilangs(hrg)+"Rupiah";
                        var tes = borongvariabel+addCommas($('#harganum').val())+" ( "+blg+" )"+" sudah termasuk PPN 10% dengan rincian sebagaimana terlampir.";
                        $('#hargatext').val(tes);
                        $('#nilai').froalaEditor('html.set', tes);
                        $('#test_harga').val(tes);
                        $('.trblng_hrg').val(tes);
                        $('.dt_hrg').val(hrg);
                    });
                    function terbilangs(n) {
                        k = ['','Satu','Dua','Tiga','Empat','Lima','Enam','Tujuh','Delapan','Sembilan']  ;
                        a = [1000000000000000,1000000000000,1000000000,1000000,1000,100,10,1];
                        s = ['Kuadriliun','Trilyun','Milyar','Juta','Ribu','Ratus','Puluh',''];
                        var i=0, x='';
                        var j=0, y = '';
                        var angka = n;
                        //alert(angka);
                        var inp_pric = String(angka).split(",");
                        while(inp_pric[0]>0){
                            b = a[i], c = Math.floor(inp_pric[0]/b), inp_pric[0] -= b * c;
                            x += (c>=10 ? terbilangs(c) + ""+s[i]+" " : ((c > 0 && c < 10) ? k[c] + " "+s[i]+" " : ""));
                            i++;

                        }
                        var depan =  x.replace(new RegExp(/satu Puluh (\w+)/gi),'$1 belas').replace(new RegExp(/satu (ribu|ratus|puluh|belas)/gi),'Se\$1');

                        while(inp_pric[1]>0){
                            z = a[j], d = Math.floor(inp_pric[1]/z), inp_pric[1] -= z * d;
                            y += k[d]+" ";
                            j++;
                        }
                        var belakang =  y.replace(new RegExp(/satu Puluh (\w+)/gi),'$1 belas').replace(new RegExp(/satu (ribu|ratus|puluh|belas)/gi),'Se\$1');

                        if (belakang != ''){
                            return depan+"koma"+belakang;
                        }
                        else {
                            return depan;
                        }

                    };
                });

            })</script>
            {{-- select io  --}}
            <script>
                // datatable
                $(document).ready( function () {
                    $('#io').DataTable();
                } )
                var table = document.getElementById('io');
                var tablespph = document.getElementById('spph');

                for(var i = 1; i < table.rows.length; i++)
                {
                    table.rows[i].onclick = function()
                    {
                        //  rIndex = this.rowIndex;
                        document.getElementById("io_id").value = this.cells[0].innerHTML;
                        document.getElementById("noio").value = this.cells[1].innerHTML;
                        document.getElementById("desio").value = this.cells[2].innerHTML;
                    };
                };
                // check cara bayar
                var interest = {
                    "styles": {} // use an array to store the styles
                };
                function add(){
                    var sbox = Array.from( document.getElementsByClassName( "carabayar" ) );
                    interest.styles = []; // empty the array before rebuilding it
                    sbox.forEach( function( v ) {
                        if ( v.checked ) {
                            interest.styles.push( v.value );
                        }
                    } );
                    $('#tatacara').froalaEditor('html.set', interest.styles.join( " " ));
                    // console output for demo
                    // console.log( interest.styles ); // array
                    // console.log( interest.styles.join() ); // CSV
                    // console.log( interest.styles.join( " " ) ); // for HTML class attributes
                }</script>
                {{-- ajax spph --}}

                <script type="text/javascript">
                    // var options = document.forms['form']['jenis_kontrak'].options;
                    // for (var i=0, iLen=[options.length-1]; i<iLen; i++) {
                        //     options[i].disabled = true;
                        // }
                        $("select[name='nomorspph']").change(function(){
                            var nomorspph = $(this).val();
                            var token = $("input[name='_token']").val();
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });
                            $.ajax({
                                url: "{{ route('data-spph') }}",
                                method: 'POST',
                                data: {nomorspph:nomorspph, _token:token},
                                success: function(data) {
                                    var nmr = data.options['nomorspph'];
                                    if (nmr.includes("ECOM")) {
                                        // $('option[value="Custom"]').selected;
                                        console.log('lkpp');
                                    }else{
                                        // $('option[value="Custom"]').remove;
                                        console.log('nonlkpp');
                                    }
                                    $('[name=spphid]').val(data.options['id']);
                                    $('#nomorspph').val(data.options['nomorspph']);
                                    $('#tglspph').val(data.options['tglspph']);
                                    $('#nomorsph').val(data.options['nomorsph']);
                                    $('#tglsph').val(data.options['tglsph']);
                                    $('#judul').val(data.options['judul']);
                                    $('#mitra').html(data.options['mitra']);
                                    $('#direktur').html(data.options['direktur']);
                                    // $("span", "tag[title='Pilih'").text(data.options['direktur']);
                                    // $("tag[title='Pilih'").attr("value", data.options['direktur']);
                                    // $("#direktur").attr("value", data.options['direktur']);

                                    const datespph = new Date(data.options['tglspph']);
                                    const monthspph = datespph.toLocaleString('id-ID', {month:'long'});
                                    const tglspph = datespph.getDate()+' '+monthspph+' '+datespph.getFullYear()
                                    const datesph = new Date(data.options['tglsph']);
                                    const monthsph = datesph.toLocaleString('id-ID', { month: 'long' });
                                    const tglsph = datesph.getDate()+' '+monthsph+' '+datesph.getFullYear();
                                    var isinya2 = '<ol style="margin-bottom:0cm;">';
                                        isinya2 +='<li><span>Surat Permintaan Penawaran Harga (SPPH) nomor: '+ data.options['nomorspph']+ ' tanggal '+tglspph+' perihal permintaan penawaran harga;</span></li>';
                                        isinya2 +='<li><span>Surat dari SUPPLIER nomor: '+data.options['nomorsph'] +' tanggal '+tglsph+' perihal Penawaran Harga.</span></li>';
                                        isinya2 +='</ol>';
                                        $('#dasarpembahasan').froalaEditor('html.set', isinya2);
                                        $('#agenda').froalaEditor('html.set', "Klarifikasi & Negosiasi Penawaran Harga dari SUPPLIER tentang "+data.options['judul']+".");
                                        $('#ruanglingkup').froalaEditor('html.set', data.options['judul']+' dengan rincian sebagaimana tercantum pada point nomor 4.');
                                    }
                                });
                            });</script>
                            {{-- end javascript tambahaan --}}
                            @endsection
