@extends('layouts.master')

@section('title')
Bakn Done | Super Slim
@endsection

@section('stylesheets')
<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
{{-- Icon --}}
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }

    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAKN DONE
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">BAKN</a></li>
            <li class="active"> BAKN DONE </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> BAKN DONE</h3>
                    </div>
                    <div style="text-align:center;padding-bottom:10px">
                        <div class="row input-daterange">
                            <form method="get" action="{{ url('bakn-done') }}" enctype="multipart/form-data">
                                <div class="col-md-4">
                                    <select name="bulan" id="bulan" class="form-control">
                                        @php
                                        if(request()->get('bulan') == null){
                                        $bln = date('m');
                                        $bulan = date('F',strtotime(date('Y-m-d')));
                                        $thn = date('Y');
                                        }else{
                                        $bln = request()->get('bulan');
                                        $bulan = date('F', mktime(0, 0, 0, request()->get('bulan'), 10));
                                        $thn = request()->get('tahun');
                                        }
                                        @endphp
                                        <option value="{{ $bln }}" selected>{{ $bulan }}</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select name="tahun" id="tahun" class="form-control">
                                        <option value="{{ $thn }}" selected>{{ $thn }}</option>
                                    </select>
                                </div>
                                <input type="submit" value="Filter" class="btn btn-primary">

                                <div class="col-md-4">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="pbsTable" class="display">
                            @csrf
                            <thead>
                                <tr style="white-space:nowrap">
                                    <th>No</th>
                                    <th>IO</th>
                                    <th>Judul SPPH</th>
                                    <th>Nomor SPPH</th>
                                    <th>Mitra</th>
                                    <th>Harga</th>
                                    <th>Created By</th>
                                    <th>Date Created</th>
                                    <th>Lampiran</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody style=" white-space: nowrap;">
                                @php
                                $no=1;
                                @endphp
                                @foreach($bakns as $list)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    @if ($list->io == null)
                                    <td style="background-color:red"></td>
                                    @else
                                    <td>{{ $list->io['no_io'] }}</td>
                                    @endif
                                    <td>{{ $list->spph['judul'] }}</td>
                                    <td>{{ $list->spph['nomorspph'] }}</td>
                                    <td>{{ $list->spph->mitras['perusahaan'] }}</td>
                                    <td>{{ number_format($list->harga) }}</td>
                                    <td>{{ $list->user['name'] }}</td>
                                    <td>{{ $list->created_at }}</td>
                                    <td>
                                        @php
                                        if($list->file == NULL){
                                        }else{
                                        $title = json_decode($list->title, TRUE);
                                        $file = json_decode($list->file, TRUE);
                                        $i=1;
                                        foreach ($title as $key => $value) {
                                        echo $i++.'. <a href="'.Storage::url($file[$key]).'">'.$title[$key].'</a><br>';
                                        }
                                        }
                                        @endphp
                                    </td>
                                    <td>
                                        <a href="/page-update-io/{{ $list->id }}" title="Update IO" class="upload fa fa-fw fa-book"></a>
                                        <a href={{ url('bakn-preview', $list->id) }}" class="fa fa-fw fa-file-code-o"
                                            title="Preview BAKN"></a>
                                        <a href="delete/{{ $list->id }}" class="fa fa-fw fa-trash"
                                            onclick="return confirm('Bener nih mau dihapus?')" title="Delete BAKN"></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
        @include('modules.bakn.modal.update_io_modal')
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<script src="{{ asset('js/web/monthAndYear.js') }}"></script>
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
    $(document).ready(function () {
        $('#pbsTable').DataTable({

        });
    });
</script>

@endsection