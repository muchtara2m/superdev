@extends('layouts.master')

@section('title')
BAKN SPPH| Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">
    /* .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    } */
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
             BAKN - BAKN
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">BAKN</a></li>
            <li class="active"> BAKN SPPH </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> BAKN SPPH</h3>
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="pbsTable" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nomor SPPH</th>
                                    <th>Mitra</th>
                                    <th>Judul</th>
                                    <th>Perihal</th>
                                    <th>Penanggung Jawab</th>
                                    <th>Tanggal SPPH </th>
                                    <th>Tanggal SPH</th>
                                    <th>Pembuat</th>
                                    <th>Lampiran</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $no=1;
                                @endphp
                                @foreach ($spph as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->nomorspph }}</td>
                                    <td>{{ $item->kepada }}</td>
                                    <td>{{ $item->judul }}</td>
                                    <td>{{ $item->perihal }}</td>
                                    <td>{{ $item->pic }}</td>
                                    <td>{{ $item->created_at }}</td>
                                    <td>{{ $item->tglsph }}</td>
                                    <td>{{ $item->created_by }}</td>
                                    <td>
                                        @php
                                        $title = json_decode($item->file_title, TRUE);
                                        $file = json_decode($item->file, TRUE);
                                        $i=1;
                                        foreach ($title as $key => $value) {
                                            echo $i++.'. <a href="'.Storage::url($file[$key]).'">'.$title[$key].'</a><br>';

                                        }
                                        @endphp
                                    </td>
                                    <td>
                                        <a href="{{ url('bakn-create',$item->id) }}" title="Create BAKN" class="fa fa-fw fa-edit"></a>
                                        <a href="{{ url('spph-preview',$item->id) }}" title="Preview SPPH" class="fa fa-fw fa-file-code-o"></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
    $(document).ready( function () {
        $('#pbsTable').DataTable({

        });
    } );
</script>
@endsection
