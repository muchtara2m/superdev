@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Update IO    ";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
@endsection

@section('customstyle')

<style type="text/css">
    table tr:not(:first-child) {
        cursor: pointer;
        transition: all .25s ease-in-out;
    }

    table tr:not(:first-child):hover {
        background-color: #ddd;
    }

    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }

    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }

    .example-modal .modal {
        background: transparent !important;
    }

    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAKN
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">BAKN</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Update IO</h3>
                    </div>
                    <!-- /.box-header -->
                    <br>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    {{-- start form  --}}
                    <form method="post" action="/update-io/{{$bakn->id}}" enctype="multipart/form-data" id="form">

                        @csrf
                        <div class="box-body">
                            <div class="form-group col-md-3 igroup">
                                <label for="noio">No IO</label>
                                <div class="input-group col-md-12">
                                    <input type="hidden" name="io_id" id="io_id">
                                    <input type="text" name="noio" id="noio" class="form-control" data-toggle="modal"
                                        data-target="#modal-unit" readonly placeholder="Pilih IO" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-3 igroup">
                                <label for="desio">Deskripsi IO</label>
                                <div class="input-group col-md-12">
                                    <input type="text" name="desio" id="desio" class="form-control" title="Deskripsi">
                                </div>
                            </div>
                            {{-- end hasil pembahasan --}}
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" name="status" value="save_bakn" class="btn btn-success"
                                style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    {{-- end form --}}
                </div>
                <!-- /.box -->
                @include('modules.bakn.modal.io_modal')
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<!-- Data Table -->
<script src="{{ asset('/js/jquery.dataTables.min.js') }}"></script>

<script>

$(document).ready(function () {
    $('#io').DataTable();
})
var table = document.getElementById('io');

for (var i = 1; i < table.rows.length; i++) {
    table.rows[i].onclick = function () {
        //  rIndex = this.rowIndex;
        document.getElementById("io_id").value = this.cells[1].innerHTML;
        document.getElementById("noio").value = this.cells[1].innerHTML;
        document.getElementById("desio").value = this.cells[2].innerHTML;
    };
};
</script>
@endsection