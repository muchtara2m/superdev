@extends('layouts.master')

@section('title')
Bakn Done | Super Slim
@endsection

@section('stylesheets')
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
{{-- Icon --}}
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<!-- DataTables -->

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
{{-- <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.3/css/dataTables.responsive.css"> --}}
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
             BAKN DONE
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">BAKN</a></li>
            <li class="active">  BAKN DONE </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> BAKN DONE</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="pbsTable" class="table table-bordered table-hover">
                            @csrf
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>IO</th>
                                    <th>Judul BAKN</th>
                                    <th>Tanggal BAKN</th>
                                    <th>Nomor SPPH</th>
                                    <th>Nomor Penawaran</th>
                                    <th>Harga</th>
                                    <th>Pembuat</th>
                                    <th>Tanggal Buat</th>
                                    <th>Terakhir Sunting</th>
                                    <th>Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $no=1;
                                @endphp
                                @foreach($bakns as $list)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{{ $list->io==0 ? 'Belum diinput' : $list->io }}}</td>
                                    <td>{{ $list->judul }}</td>
                                    <td>{{ $list->tanggal }}</td>
                                    <td>{{ $list->nomor_spph }}</td>
                                    <td>{{ $list->nomor_penawaran }}</td>
                                    <td>{{ number_format($list->harga,0,'.','.') }}</td>
                                    <td>{{ $list->name }}</td>
                                    <td>{{ $list->created_at }}</td>
                                    <td>{{ $list->updated_at }}</td>
                                    <td>
                                        {{-- <a href="{{ url('bakn-edit', $list->id)}}" class="fa fa-fw fa-edit"></a> --}}

                                        <a href="#modal_unit{{ $list->id }}"  data-toggle="modal" data-target="#modal-unit_{{ $list->id }}" class="fa fa-fw fa-file"></a>
                                        <div class="modal fade" id="modal-unit_{{ $list->id }}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Upload File</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                    <form method="post" action="{{action('BaknController@upload', $list->id)}}" enctype="multipart/form-data">
                                                        @csrf
                                                        {{-- @method('PATCH') --}}
                                                        
                                                        <input type="hidden" name="title[]" class="form-control">
                                                        <br>
                                                        <input type="file" name="file[]" id="" class="form-control" multiple="multiple">
                                                        <br>
                                                        <button type="submit" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i>
                                                            Submit</button>
                                                    </form>
                                                    
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <!-- /.modal -->
                                        <a href = "{{ url('bakn-preview', $list->id) }}" class="fa fa-fw fa-file-code-o" title="Preview BAKN"></a>
                                        @if(Auth::user()->level == 1)
                                        <a href = "delete/{{ $list->id }}" class="fa fa-fw fa-trash" onclick="return confirm('Bener nih mau dihapus?')" title="Delete BAKN"></a>
                                        @endif

                                        {{-- @if ($list->handler=='')
                                        <a href="#"  id="test" data-toggle="modal" data-target="#modal-unit" title="Modifier" data-id="{{ $list->id }}" class="fa fa-fw fa-user"></a>
                                        @endif --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        </div>

                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    @endsection

    @section('scripts')
    <!-- DataTables -->
    <script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>
    <script type="text/javascript">
        // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
        $(document).ready( function () {
            $('#pbsTable').DataTable({

            });
        } );</script>

        @endsection
