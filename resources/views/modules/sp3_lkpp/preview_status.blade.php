@extends('layouts.master')
@php
if (isset($sp3)) {
    $res = $sp3;
    $refno = $sp3->nomor_sp3;
    $docdate = $sp3->tanggal_sp3;
    $doctitle = "Surat Penetapan Pelaksanaan Pekerjaan (SPPP)";
}
@endphp
@php
$homelink = "/home";
$crpagename = "Approve SP3";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
{{-- smartwizard --}}
{{-- <link href="{{ asset('smartwizard/dist/css/smart_wizard.css') }}" rel="stylesheet" type="text/css" /> --}}
{{-- <link href="{{ asset('smartwizard/dist/css/smart_wizard_theme_circles.css') }}" rel="stylesheet" type="text/css" /> --}}
<link href="{{ asset('smartwizard/dist/css/smart_wizard_theme_circles.min.css') }}" rel="stylesheet" type="text/css" />

{{-- end smartwizard --}}
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Approve SP3
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">SP3</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Approve SP3</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
                        
                    </div>
                    
                    <div class="box-body">
                        <div class="box-body">
                            <div class="form-group" >
                                
                                <br>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Lampiran BAKN</th>
                                            <th>Lampiran SP3</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                @php
                                                if($sp3->bakn_lkpp['file'] == NULL){
                                                }else{
                                                    $title = json_decode($sp3->bakn_lkpp['title'], TRUE);
                                                    $file = json_decode($sp3->bakn_lkpp['file'], TRUE);
                                                    $i=1;
                                                    foreach ($title as $key => $value) {
                                                        echo $i++.'. <a href="'.Storage::url($file[$key]).'">'.$title[$key].'</a><br>';
                                                    }
                                                }
                                                @endphp
                                            </td>
                                            <td>
                                                @php
                                                if($sp3->lampiran == NULL){
                                                }else{
                                                    $title = json_decode($sp3->title_lampiran, TRUE);
                                                    $file = json_decode($sp3->lampiran, TRUE);
                                                    $i=1;
                                                    foreach ($title as $key => $value) {
                                                        echo $i++.'. <a href="'.Storage::url($file[$key]).'">'.$title[$key].'</a><br>';
                                                    }
                                                }
                                                @endphp
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <td >
                                    
                                </td>
                            </div>
                            <div class="form-groupp">
                                <div id="smartwizard">
                                    <ul>
                                        <li><a href="#step-1">Step 1<br /><small>Draft SP3</small></a></li>
                                        <li><a href="#step-2">Step 2<br /><small> BAKN</small></a></li>
                                        <li><a href="#step-3">Step 2<br /><small>Approval</small></a></li>
                                        {{-- <li><a href="#step-4">Step 4<br /><small>Approval</small></a></li> --}}
                                    </ul>
                                    <div>
                                        <div id="step-1" class="">
                                            <textarea name="" id="" cols="30" rows="10">
                                                @include('modules.sp3_lkpp.modal.inc_preview')
                                            </textarea>
                                        </div>
                                        <div id="step-2" class="">
                                            @include('modules.bakn_lkpp.inc.bakn_preview')
                                        </div>
                                        <div id="step-3" class="">
                                            <h4>Coment and Approve</h4>
                                            <ul class="timeline">
                                                @foreach ($chats as $isi)
                                                <!-- timeline time label -->
                                                <li class="time-label">
                                                    <span class="bg-green">
                                                        {{ date('d M.Y', strtotime($isi->created_at)) }}
                                                    </span>
                                                </li>
                                                <!-- /.timeline-label -->
                                                <!-- timeline item -->
                                                <li>
                                                    <!-- timeline icon -->
                                                    <i class="fa fa-user bg-aqua"></i>
                                                    <div class="timeline-item">
                                                        <span class="time">
                                                            <i class="fa fa-clock-o"></i>
                                                            {{ date('H:i:s', strtotime($isi->created_at)) }}
                                                        </span>
                                                        
                                                        <h3 class="timeline-header"><a href="#">{{ $isi->jabatan.' - '.$isi->name }}</a></h3>
                                                        
                                                        <div class="timeline-body">
                                                            {{ $isi->chat }}
                                                        </div>
                                                        
                                                        <div class="timeline-footer">
                                                            {{-- <a class="btn btn-primary btn-xs">...</a> --}}
                                                        </div>
                                                    </div>
                                                </li>
                                                <!-- END timeline item -->
                                                @endforeach
                                                <li>
                                                    <i class="fa fa-clock-o bg-gray"></i>
                                                </li>
                                            </ul>
                                            <br>
                                            @if($sp3->approval == Auth::user()->username)
                                            <form action="{{ url('chat-approval-sp3-lkpp') }}" class="form-group" method="post">
                                                @csrf
                                                <input type="hidden" value={{ $sp3->id }} name="idKontrak">
                                                <div class="col-md-12 pad-0">
                                                    <div class="col-md-6 pad-0">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Comment</label>
                                                            <input type="text" class="form-control" name="chat"  id="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" hidden>
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">ID Transaksi</label>
                                                            <input type="text" class="form-control" name="idTransaksi" value="{{ $sp3->id }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 mar-paginate pad-0">
                                                        <button type="submit" class="btn btn-danger" name="status" value="Return">Return</button>
                                                        <button type="submit" class="btn btn-primary" name="status" id="submit" value="Approve">Approve</button>
                                                    </div>
                                                </div>
                                            </form>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                
                {{-- </form> --}}
            </div>
            <!-- /.box -->
            
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<!-- smart wizard -->
<script type="text/javascript" src="{{ asset('smartwizard/dist/js/jquery.smartWizard.min.js') }}"></script>


<script>
    $(document).ready(function () {
        $('div#smartwizard').smartWizard();
        $('div#smartwizardcircle').smartWizard();
    });
    $('textarea').froalaEditor({
        // toolbarButtons: ['container'],
        // toolbarButtons: ['getPDF', 'print'],
        toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'inlineClass', 'clearFormatting', '|', 'emoticons', 'fontAwesome', 'specialCharacters', 'paragraphFormat', 'lineHeight', 'paragraphStyle', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', 'insertHR', 'selectAll', 'help', 'html', 'fullscreen', '|', 'undo', 'redo', 'getPDF', 'print'],
        placeholderText: '',
        documentReady: true,
        language: 'id',
        key: '{{ env("KEY_FROALA") }}',
    });
</script>
@endsection
