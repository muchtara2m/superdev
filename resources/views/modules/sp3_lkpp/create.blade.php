@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Create SP3";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
{{--  Tagify  --}}
<link rel="stylesheet" href="{{ asset('tagify/dist/tagify.css') }}">
@endsection


@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SP3
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">SP3/sp3</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Create SP3</h3>
                    </div>
                    <!-- /.box-header -->
                    <br>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="container">

                        <div class="col-md">
                            <label for="nomorspph">Select SPPH</label><a target="_blank" href="#"><i
                                    class="fa fa-fw fa-file-code-o"></i></a>
                            <input type="hidden" id="spph">
                            <input type="text" id="nmr" class="form-control" title="SPPH" data-toggle="modal"
                                data-target="#modal-unit" readonly placeholder="Pilih SPPH">
                        </div>
                    </div>

                    {{-- start form  --}}
                    <form method="post" action="store" enctype="multipart/form-data" id="form">
                        @csrf
                        <input type="hidden" name="baknid" value="{{ old('baknid') }}">
                        <input type="hidden" name="harga_sp3" value="{{ old('harga_sp3') }}">
                        <div class="box-body">
                            <div class="form-group col-md-6 igroup">
                                <label for="jenis_kontrak">No SP3</label>
                                <div class="input-group col-md-12">
                                    <input type="text" name="nosp3" class="form-control" id="nosp3"
                                        value="{{ old('nosp3') }}">
                                </div>
                            </div>
                            <div class="form-group col-md-6 igroup">
                                <label for="tglsp3">Tanggal SP3</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" data-date="" data-date-format="yyyy-mm-dd"
                                        class="form-control datejos" name="tglsp3" id="tglsp3"
                                        value="{{ old('tglsp3') }}">
                                </div>
                            </div>
                            {{-- hasil pembahasan --}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h4 id="jenis" class="divider-title" align="center">JUDUL</h4>
                                    <hr>
                                </div>
                            </div>
                            {{--  isinya  --}}
                            <div class="form-group">
                                <label for="ruanglingkip">Ruang Lingkup</label>
                                <textarea id="ruanglingkup" rows="10" cols="80" name="ruang_lingkup">

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="lokasipekerjaan">Lokasi Pekerjaan/Pengiriman Barang/Jasa :</label>
                                <textarea id="lokasipekerjaan" rows="10" cols="80" name="lokasi_pekerjaan">

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="jangkawaktu">Jangka Waktu Pengiriman Barang : </label>
                                <textarea id="jangkawaktu" rows="10" cols="80" name="jangka_waktu">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="harga">Harga Pekerjaan :</label>
                                <textarea id="hargaterbilang" name="harga_terbilang">

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="tatacara">Tata Cara Pembayaran :</label>
                                <textarea rows="10" cols="80" name="cara_bayar" id="carabayar">

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="lainlain">Lain - lain : </label>
                                <textarea rows="10" cols="80" name="lain_lain" id="lainlain">
                                </textarea>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Lampiran</label>
                                    <input type="file" name="lampiran[]" multiple class="form-control">
                                </div>
                            </div>
                            {{-- end hasil pembahasan --}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Comment</label>
                                    <span>
                                        *this field is required if you want submit
                                    </span>
                                    <input type="text" name="chat" class="form-control">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" name="status" value="draft_sp3" class="btn btn-primary"
                                style="width: 7em;"><i class="fa fa-check"></i> Save</button>
                            <button type="submit" name="status" value="save_sp3" class="btn btn-success"
                                style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    {{-- end form --}}
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
        @include('modules.sp3_lkpp.modal.spph_modal')
</div>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
{{-- bahasa indonesia froala --}}
<script src="{{ asset('froala/js/languages/id.js') }}"></script>

<!-- Data Table -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- clockpicker -->
<script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Tagify -->
<script src="{{ asset('tagify/dist/jQuery.tagify.min.js') }}"></script>
<script src="{{ asset('tagify/dist/tagify.min.js') }}"></script>
<script>
    // set default no_bak dan tgl_bak
    let get_tgl_bak = document.getElementById('tglsp3');

    function useValue() {
        let NameValue = get_tgl_bak.value.split('-');
        // use it
        document.getElementById('nosp3').value = "/HK.810/ECOM/PIN.00.00/" + NameValue[0];
    }
    get_tgl_bak.onchange = useValue;
    get_tgl_bak.onblur = useValue;

    $(document).ready(function () {
        $('#io').DataTable();
    })
    var table = document.getElementById('io');

    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            //  rIndex = this.rowIndex;
            document.getElementById("spph").value = this.cells[0].innerHTML;
            document.getElementById("nmr").value = this.cells[1].innerHTML;
        };
    };
    
    $(function () {
        $(".datejos").on("change", function () {
            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DD")
                .format(this.getAttribute("data-date-format"))
            )
        }).trigger("change")
        $('.datejos').datepicker({
            autoclose: true,
            orientation: "bottom"
        })
    })

    $('textarea').froalaEditor({
        placeholderText: '',
        charCounterCount: false,
        key: '{{ env("KEY_FROALA") }}',
    });


    $(document).ready(function () {
        $(document).change(function () {
            var id = $('#spph').val();
            var token = $("input[name='_token']").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: "{{ route('data-bakn') }}",
                data: {
                    id: id,
                    _token: token
                },
                dataType: 'json', //return data will be json
                async: "false",
                success: function (data) {
                    clearText();
                    var nmr = data.options['spph_lkpps']['nomorspph'];
                    var tglbakn = new Date(data.options['tglbakn']);
                    var thn = tglbakn.getFullYear();
                    $('form').attr('action', "{{ url('store-sp3-lkpp')}}");
                    $('[name=baknid]').val(data.options['id']);
                    $('[name=harga_sp3]').val(data.options['harga']);
                    // addText(data);

                    // function addText(data) {
                        $('#ruanglingkup').froalaEditor('html.insert', data.options['ruang_lingkup'], true);
                        $('#lokasipekerjaan').froalaEditor('html.insert', data.options['lokasi_pekerjaan'], true);
                        $('#jangkawaktu').froalaEditor('html.insert', data.options['jangka_waktu'], true);
                        $('#hargaterbilang').froalaEditor('html.insert', data.options['harga_terbilang'], true);
                        $('#carabayar').froalaEditor('html.insert', data.options['cara_bayar'], true);
                        $('#lainlain').froalaEditor('html.insert', data.options['lain_lain'], true);
                    // }

                    function clearText() {
                        $('#ruanglingkup').froalaEditor('html.set', '');
                        $('#lokasipekerjaan').froalaEditor('html.set', '');
                        $('#jangkawaktu').froalaEditor('html.set', '');
                        $('#hargaterbilang').froalaEditor('html.set', '');
                        $('#carabayar').froalaEditor('html.set', '');
                        $('#lainlain').froalaEditor('html.set', '');
                    }
                },
                error: function (data) {
                    console.log(data);
                }
            });


        });
    });

</script>
@endsection
