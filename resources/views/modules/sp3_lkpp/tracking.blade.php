@extends('layouts.master')

@section('title')
SP3 Tracking | Super Slim
@endsection
@section('stylesheets')
<!-- DataTables -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css"
href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css">
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    
    /* tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    } */
    tfoot {
        display: table-header-group;
    }
    
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SP3
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E - Commerce</a></li>
            <li><a href="#">SP3</a></li>
            <li class="active"> SP3 Tracking</li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> SP3 TRACKING</h3>
                    </div>
                    <!-- /.box-header -->
                    <div style="text-align:center;padding-bottom:10px">
                        <div class="row input-daterange">
                            <form method="get" action="{{ url('kontrak-non-status') }}" enctype="multipart/form-data">
                                <div class="col-md-4">
                                    <select name="bulan" id="bulan" class="form-control">
                                        @php
                                        if(request()->get('bulan') == null){
                                            $bln = date('m');
                                            $bulan = date('F',strtotime(date('Y-m-d')));
                                            $thn = date('Y');
                                        }else{
                                            $bln = request()->get('bulan');
                                            $bulan = date('F', mktime(0, 0, 0, request()->get('bulan'), 10));
                                            $thn = request()->get('tahun');
                                        }
                                        @endphp
                                        <option value="{{ $bln }}" selected>{{ $bulan }}</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select name="tahun" id="tahun" class="form-control">
                                        <option value="{{ $thn }}" selected>{{ $thn }}</option>
                                    </select>
                                </div>
                                <input type="submit" value="Filter" class="btn btn-primary">
                                
                                <div class="col-md-4">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="box-body table-responsive">
                        <table name="item" id="item" class="display">
                            <thead align="center">
                                <tr style=" white-space: nowrap">
                                    <th rowspan="2" style="text-align:center">No</th>
                                    <th rowspan="2" style="text-align:center">IO</th>
                                    <th rowspan="2" style="text-align:center">Nomor SPPH</th>
                                    <th rowspan="2" style="text-align:center">Judul SPPH</th>
                                    <th rowspan="2" style="text-align:center">Nomor SP3</th>
                                    <th rowspan="2" style="text-align:center">Mitra</th>
                                    <th rowspan="2" style="text-align:center">Tanggal SPPH</th>
                                    <th rowspan="2" style="text-align:center">Tanggal Buat SPPH</th>
                                    <th rowspan="2" style="text-align:center">Tanggal BAKN</th>
                                    <th rowspan="2" style="text-align:center">Tanggal Buat BAKN</th>
                                    <th rowspan="2" style="text-align:center">Tanggal SP3</th>
                                    <th rowspan="2" style="text-align:center">Tanggal Buat</th>
                                    <th rowspan="2" style="text-align:center">Pembuat</th>
                                    <th rowspan="2" style="text-align:center">Harga</th>
                                    <th colspan="6" style="text-align:center">Approval</th>
                                    <th rowspan="2" style="text-align:center">Posisi</th>
                                    <th rowspan="2" style="text-align:center">Tanggal Upload File</th>
                                    <th rowspan="2" style="text-align:center">Preview</th>
                                    <th colspan="4" style="text-align:center">Lampiran</th>
                                </tr>
                                <tr>
                                    <th style="text-align:center">Creator</th>
                                    <th style="text-align:center">Manager</th>
                                    <th style="text-align:center">Manager</th>
                                    <th style="text-align:center">GM E-Commerce</th>
                                    <th style="text-align:center">Direktur Operation</th>
                                    <th style="text-align:center">Direktur Utama</th>
                                    <th style="text-align:center">Lampiran SPPH</th>
                                    <th style="text-align:center">Lampiran BAKN</th>
                                    <th style="text-align:center">Lampiran SP3</th>
                                    <th style="text-align:center">File SP3</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th class="testing"></th>
                                    <th class="testing"></th>
                                    <th class="testing"></th>
                                    <th class="testing"></th>
                                    <th class="testing"></th>
                                    <th class="testing"></th>
                                    <th></th>
                                    <th class="testing"></th>
                                    <th></th>
                                    <th class="testing"></th>
                                    <th></th>
                                    <th class="testing"></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody style="text-align:justify;white-space:nowrap">
                                @php
                                $no=1;
                                @endphp
                                @foreach ($sp3 as $item => $val)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $val->bakn_lkpp['io_id'] }}</td>
                                    <td>{{ $val->bakn_lkpp->spph_lkpps['nomorspph'] }}</td>
                                    <td>{{ $val->bakn_lkpp->spph_lkpps['judul'] }}</td>
                                    <td>{{ $val->nomor_sp3 }}</td>
                                    <td>{{ $val->bakn_lkpp->spph_lkpps->mitra_lkpps['perusahaan'] }}</td>
                                    <td>{{ date('d.F.Y', strtotime($val->bakn_lkpp->spph_lkpps['tglspph'])) }}</td>
                                    <td>{{ date('d.F.Y H:i', strtotime($val->bakn_lkpp->spph_lkpps['created_at'])) }}
                                    </td>
                                    <td>{{ date('d.F.Y', strtotime($val->bakn_lkpp['tglbakn'])) }}</td>
                                    <td>{{ date('d.F.Y H:i', strtotime($val->bakn_lkpp['created_at'])) }}</td>
                                    <td>{{ date('d.F.Y', strtotime($val->tanggal_sp3)) }}</td>
                                    <td>{{ date('d.F.Y H:i', strtotime($val->created_at)) }}</td>
                                    <td>{{ $val->user_sp3_lkpp['name'] }}</td>
                                    <td>{{ number_format($val->harga) }}</td>
                                    <td style="text-align:center">
                                        @php
                                        $data[$val->idnya]['id']= $val->id; //idkontrak
                                        for ($i=0; $i < count($val->chatsp3); $i++) {
                                            if ($val->chatsp3[$i]['queue']== 0) {
                                                echo '<br>('.date('d.F.Y H:i', strtotime($val->chatsp3[$i]['created_at'])).'
                                                - '.$val->chatsp3[$i]['status'].')<br>'.$val->chatsp3[$i]['name'];
                                            }
                                        }
                                        @endphp
                                    </td>
                                    <td style="text-align:center">
                                        @php
                                        $data[$val->idnya]['id']= $val->id; //idkontrak
                                        for ($i=0; $i < count($val->chatsp3); $i++) {
                                            if ($val->chatsp3[$i]['queue']== 1) {
                                                echo '<br>('.date('d.F.Y H:i', strtotime($val->chatsp3[$i]['created_at'])).'
                                                - '.$val->chatsp3[$i]['status'].')<br>'.$val->chatsp3[$i]['name'];
                                            }
                                        }
                                        @endphp
                                    </td>
                                    <td style="text-align:center">
                                        @php
                                        $data[$val->idnya]['id']= $val->id; //idkontrak
                                        for ($i=0; $i < count($val->chatsp3); $i++) {
                                            if ($val->chatsp3[$i]['queue']== 2) {
                                                echo '<br>('.date('d.F.Y H:i', strtotime($val->chatsp3[$i]['created_at'])).'
                                                - '.$val->chatsp3[$i]['status'].')<br>'.$val->chatsp3[$i]['name'];
                                            }
                                        }
                                        @endphp
                                    </td>
                                    <td style="text-align:center">
                                        @php
                                        $data[$val->idnya]['id']= $val->id; //idkontrak
                                        for ($i=0; $i < count($val->chatsp3); $i++) {
                                            if ($val->chatsp3[$i]['queue']== 3) {
                                                echo '<br>('.date('d.F.Y H:i', strtotime($val->chatsp3[$i]['created_at'])).'
                                                - '.$val->chatsp3[$i]['status'].')<br>'.$val->chatsp3[$i]['name'];
                                            }
                                        }
                                        @endphp
                                    </td>
                                    <td style="text-align:center">
                                        @php
                                        $data[$val->idnya]['id']= $val->id; //idkontrak
                                        for ($i=0; $i < count($val->chatsp3); $i++) {
                                            if ($val->chatsp3[$i]['queue']== 4) {
                                                echo '<br>('.date('d.F.Y H:i', strtotime($val->chatsp3[$i]['created_at'])).'
                                                - '.$val->chatsp3[$i]['status'].')<br>'.$val->chatsp3[$i]['name'];
                                            }
                                        }
                                        @endphp
                                    </td>
                                    <td style="text-align:center">
                                        @php
                                        $data[$val->idnya]['id']= $val->id; //idkontrak
                                        for ($i=0; $i < count($val->chatsp3); $i++) {
                                            if ($val->chatsp3[$i]['queue']== 5) {
                                                echo '<br>('.date('d.F.Y H:i', strtotime($val->chatsp3[$i]['created_at'])).'
                                                - '.$val->chatsp3[$i]['status'].')<br>'.$val->chatsp3[$i]['name'];
                                            }
                                        }
                                        @endphp
                                    </td>
                                    <td>{{ $val->approval }}</td>
                                    @if ($val['upload'] != null)
                                    <td>{{ date('d.F.Y', strtotime($val['upload'])) }}</td>
                                    @else
                                    <td></td>    
                                    @endif
                                    <td>
                                        <a target="_blank" href="{{ url('preview-spph-lkpp/'.$val->bakn_lkpp['spph_id']) }}">Preview SPPH</a><br>
                                        <a target="_blank" href="{{ url('preview-bakn-lkpp/'.$val->bakn_id) }}">Preview BAKN</a><br>
                                        <a target="_blank" href="{{ url('preview-sp3-lkpp/'.$val->id) }}">Preview SP3</a><br>
                                        @if (Auth::user()->level == 'administrator')
                                        <a href = "delete-sp3-lkpp/{{ $val->id }}" class="fa fa-fw fa-trash" onclick="return confirm('Bener nih mau dihapus?')" title="Delete SP3"></a>                                            
                                        @endif
                                    </td>
                                    @php
                                    $fspph = json_decode($val->bakn_lkpp->spph_lkpps['file'], TRUE);
                                    $tspph = json_decode($val->bakn_lkpp->spph_lkpps['title'], TRUE);
                                    $fbakn = json_decode($val->bakn_lkpp['file'], TRUE);
                                    $tbakn = json_decode($val->bakn_lkpp['title'], TRUE);
                                    $flampiran = json_decode($val->lampiran, TRUE);
                                    $tlampiran = json_decode($val->title_lampiran, TRUE);
                                    $ffile = json_decode($val->file, TRUE);
                                    $tfile = json_decode($val->title, TRUE);
                                    @endphp
                                    <td>
                                        @php
                                        $i=1;
                                        @endphp
                                        @if($fspph != NULL)
                                        @foreach ($tspph as $keyspph =>$valspph)
                                        {{ $i++.'. ' }}<a
                                        href="{{ Storage::url($fspph[$keyspph]) }}">{{ $tspph[$keyspph] }}</a><br>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @php
                                        $i=1;
                                        @endphp
                                        @if($fbakn != NULL)
                                        @foreach ($tbakn as $keybakn =>$valbakn)
                                        {{ $i++.'. ' }}<a
                                        href="{{ Storage::url($fbakn[$keybakn]) }}">{{ $tbakn[$keybakn] }}</a><br>
                                        @endforeach
                                        @endif
                                    </td>
                                    <td>
                                        @php
                                        $x=1;
                                        @endphp
                                        @if($flampiran != NULL)
                                        @foreach ($tlampiran as $keylampiran =>$vallampiran)
                                        {{ $x++.'. ' }}<a
                                        href="{{ Storage::url($flampiran[$keylampiran]) }}">{{ $tlampiran[$keylampiran] }}</a><br>
                                        @endforeach
                                        
                                        @endif
                                    </td>
                                    <td>
                                        @php
                                        if($val->file == NULL){
                                        }else{
                                            $title = json_decode($val->title, TRUE);
                                            $file = json_decode($val->file, TRUE);
                                            $i=1;
                                            foreach ($title as $key => $value) {
                                                echo $i++.'. <a
                                                href="'.Storage::url($file[$key]).'">'.$title[$key].'</a><br>';
                                            }
                                        }
                                        @endphp
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>

<script type="text/javascript">
    // Setup - add a text input to each footer cell
    $('#item tfoot .testing').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" />');
    });
    
    // DataTable
    var otable = $('.display').DataTable();
    
    // Apply the search
    otable.columns().every(function () {
        
        var that = this;
        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that
                .search(this.value)
                .draw();
            }
        });
    });
    
</script>
@endsection
