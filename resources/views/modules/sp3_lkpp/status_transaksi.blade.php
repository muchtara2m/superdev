@extends('layouts.master')

@section('title')
Status SP3 | Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            STATUS SP3
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E-Commerce</a></li>
            <li><a href="#">SP3</a></li>
            <li class="active">Status Transaksi </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button href="#" class="close" data-dismiss="alert" aria-label="close">&times;</button>
                            {{ $message }}
                        </div>
                        @endif
                        
                        <h3 class="box-title"><i class="fa fa-ticket"></i>Status SP3</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="nav-tabs-custom">
                        <div style="text-align:center;padding-bottom:10px">
                            <div class="row input-daterange">
                                <form method="get" action="{{ url('kontrak-non-status') }}" enctype="multipart/form-data">
                                    <div class="col-md-4">
                                        <select name="bulan" id="bulan" class="form-control">
                                            @php
                                            if(request()->get('bulan') == null){
                                                $bln = date('m');
                                                $bulan = date('F',strtotime(date('Y-m-d')));
                                                $thn = date('Y');
                                            }else{
                                                $bln = request()->get('bulan');
                                                $bulan = date('F', mktime(0, 0, 0, request()->get('bulan'), 10));
                                                $thn = request()->get('tahun');
                                            }
                                            @endphp
                                            <option value="{{ $bln }}" selected>{{ $bulan }}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select name="tahun" id="tahun" class="form-control">
                                            <option value="{{ $thn }}" selected>{{ $thn }}</option>
                                        </select>
                                    </div>
                                    <input type="submit" value="Filter" class="btn btn-primary">
                                    
                                    <div class="col-md-4">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="box-body table-responsive">
                            <table id="permissionsTable" class="display">
                                <thead>
                                    <tr style=" white-space: nowrap; text-align:center">
                                        <th style="text-align:center">No</th>
                                        <th style="text-align:center">No IO</th>
                                        <th style="text-align:center">Nomor SPPH</th>
                                        <th style="text-align:center">Nomor SP3</th>
                                        <th style="text-align:center">Judul</th>
                                        <th style="text-align:center">Mitra</th>
                                        <th style="text-align:center">Harga</th>
                                        <th style="text-align:center">Pembuat</th>
                                        <th style="text-align:center">Posisi</th>
                                        <th style="text-align:center">Keterangan</th>
                                    </tr>
                                </thead>
                                <tbody style="white-space:nowrap">
                                    @php
                                    $i=1;
                                    @endphp
                                    @foreach ($sp3 as $item)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $item->bakn_lkpp['io_id'] }}</td>
                                        <td>{{ $item->bakn_lkpp->spph_lkpps['nomorspph'] }}</td>
                                        <td>{{ $item->nomor_sp3 }}</td>
                                        <td>{{ $item->bakn_lkpp->spph_lkpps['judul'] }}</td>
                                        <td>{{ $item->bakn_lkpp->spph_lkpps->mitra_lkpps['perusahaan'] }}</td>
                                        <td>{{ number_format($item->harga) }}</td>
                                        <td>{{ $item->user_sp3_lkpp['name'] }}</td>
                                        <td>{{ $item->approval }}</td>
                                        <td>
                                            <a href="{{ url('preview-status-sp3-lkpp', $item->id)}}" class="fa fa-fw fa-sticky-note-o" title="Approve SP3"></a>
                                            @if (Auth::user()->level == 'administrator')
                                                <a href = "delete-sp3-lkpp/{{ $item->id }}" class="fa fa-fw fa-trash" onclick="return confirm('Bener nih mau dihapus?')" title="Delete SP3"></a>                                            
                                            @endif
                                            @if($item->approval == 'Return' && $item->created_by == Auth::user()->id)
                                            <a href="{{ url('edit-sp3-lkpp', $item->id)}}" class="fa fa-fw fa-edit" title="Edit SP3"></a>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
    $(document).ready( function () {
        $('#rolesTable,#permissionsTable').DataTable({
        });
    } );</script>
    @endsection
    