@extends('layouts.master')

@section('title')
SP3 | Super Slim
@endsection

@section('stylesheets')
<link rel="stylesheet"
href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
{{-- Icon --}}
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<!-- DataTables -->

<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
{{-- <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/responsive/2.2.3/css/dataTables.responsive.css"> --}}
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
    
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SP3
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">E - Commerce</a></li>
            <li><a href="#">SP3</a></li>
            <li class="active"> SP3 Draft</li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> SP3  DRAFT</h3>
                    </div>
                    <!-- /.box-header -->
                    <div style="text-align:center;padding-bottom:10px">
                        <div class="row input-daterange">
                            <form method="get" action="{{ url('kontrak-non-status') }}" enctype="multipart/form-data">
                                <div class="col-md-4">
                                    <select name="bulan" id="bulan" class="form-control">
                                        @php
                                        if(request()->get('bulan') == null){
                                            $bln = date('m');
                                            $bulan = date('F',strtotime(date('Y-m-d')));
                                            $thn = date('Y');
                                        }else{
                                            $bln = request()->get('bulan');
                                            $bulan = date('F', mktime(0, 0, 0, request()->get('bulan'), 10));
                                            $thn = request()->get('tahun');
                                        }
                                        @endphp
                                        <option value="{{ $bln }}" selected>{{ $bulan }}</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select name="tahun" id="tahun" class="form-control">
                                        <option value="{{ $thn }}" selected>{{ $thn }}</option>
                                    </select>
                                </div>
                                <input type="submit" value="Filter" class="btn btn-primary">
                                
                                <div class="col-md-4">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="box-body table-responsive">
                        <table id="rolesTable" class="display">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>IO</th>
                                    <th>Nomor SPPH</th>
                                    <th>Judul SPPH</th>
                                    <th>Nomor SP3</th>
                                    <th>Tanggal SP3</th>
                                    <th>Harga</th>
                                    <th>Lampiran</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $no=1;
                                @endphp
                                @foreach ($sp3 as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->bakn_lkpp['io_id'] }}</td>
                                    <td>{{ $item->bakn_lkpp->spph_lkpps['nomorspph'] }}</td>
                                    <td>{{ $item->bakn_lkpp->spph_lkpps['judul'] }}</td>
                                    <td>{{ $item->nomor_sp3 }}</td>
                                    <td>{{ $item->tanggal_sp3 }}</td>
                                    <td>{{ number_format($item->harga) }}</td>
                                    <td>
                                        @php
                                        $flampiran = json_decode($item->lampiran, TRUE);
                                        $tlampiran = json_decode($item->title_lampiran, TRUE);
                                        @endphp
                                           @php
                                           $x=1;
                                           @endphp
                                           @if($flampiran != NULL)
                                           @foreach ($tlampiran as $keylampiran =>$vallampiran)
                                           {{ $x++.'. ' }}<a
                                               href="{{ Storage::url($flampiran[$keylampiran]) }}">{{ $tlampiran[$keylampiran] }}</a><br>
                                           @endforeach
   
                                           @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('edit-sp3-lkpp/'.$item->id) }}" title="Edit SP3" class="upload fa fa-fw fa-edit"></a>
                                        <a href="{{ url('preview-sp3-lkpp', $item->id) }}" class="fa fa-fw fa-file-code-o" title="Preview SP3"></a>
                                        <a href = "delete-sp3-lkpp/{{ $item->id }}" class="fa fa-fw fa-trash" onclick="return confirm('Bener nih mau dihapus?')" title="Delete SP3"></a>                                            
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </div>
        <!--/.col (right) -->
    </div>
    <!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    // document.getElementById("unitTable_wrapper").style.overflow = "auto";
    $(document).ready(function () {
        $('#rolesTable,#permissionsTable').DataTable({});
    });
    
</script>
@endsection
