@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Create SP3";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
{{--  Tagify  --}}
<link rel="stylesheet" href="{{ asset('tagify/dist/tagify.css') }}">
@endsection

@section('customstyle')

<style type="text/css">
    table tr:not(:first-child) {
        cursor: pointer;
        transition: all .25s ease-in-out;
    }

    table tr:not(:first-child):hover {
        background-color: #ddd;
    }

    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }

    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }

    .example-modal .modal {
        background: transparent !important;
    }

    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }

</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SP3
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">SP3/sp3</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Create SP3</h3>
                        @if ($chat != NULL)
                        <div class="form-group col-md-12" style="margin-top:2%">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Comment From {{ strtoupper($chat->username) }}</label>
                                {{-- {{ $bakns->id }} --}}
                                <textarea name="" id="chat" cols="30" rows="10">
                                    {{ $chat->chat }}
                                </textarea>
                            </div>
                        </div>
                        @endif
                    </div>
                    <!-- /.box-header -->
                    <br>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="container">

                        <div class="col-md">
                            <label for="nomorspph">Select SPPH</label><a target="_blank" href="{{ $sp3->bakn_lkpp['spph_id'] }}"><i
                                    class="fa fa-fw fa-file-code-o"></i></a>
                            <input type="hidden" id="spph">
                            <input type="text" id="nmr" class="form-control" value="{{ $sp3->bakn_lkpp->spph_lkpps['nomorspph'] }}" readonly>
                        </div>
                    </div>

                    {{-- start form  --}}
                    <form method="post" action="{{ url('update-sp3-lkpp',$sp3->id) }}" enctype="multipart/form-data" id="form">
                        @csrf
                        <input type="hidden" name="baknid" value="{{ $sp3->bakn_id }}">
                        <input type="hidden" name="harga_sp3" value="{{ $sp3->harga }}">
                        <div class="box-body">
                            <div class="form-group col-md-6 igroup">
                                <label for="jenis_kontrak">No SP3</label>
                                <div class="input-group col-md-12">
                                    <input type="text" name="nosp3" class="form-control" id="nosp3"
                                        value="{{ $sp3->nomor_sp3 }}">
                                </div>
                            </div>
                            <div class="form-group col-md-6 igroup">
                                <label for="tglsp3">Tanggal SP3</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" data-date="" data-date-format="yyyy-mm-dd"
                                        class="form-control datejos" name="tglsp3" id="tglsp3"
                                        value="{{ $sp3->tanggal_sp3 }}">
                                </div>
                            </div>
                            {{-- hasil pembahasan --}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <h4 id="jenis" class="divider-title" align="center">JUDUL</h4>
                                    <hr>
                                </div>
                            </div>
                            {{--  isinya  --}}
                            <div class="form-group">
                                <label for="ruanglingkip">Ruang Lingkup</label>
                                <textarea id="ruanglingkup" rows="10" cols="80" name="ruang_lingkup" placeholder="Type Something">
                                    {{ $sp3->ruang_lingkup }}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="lokasipekerjaan">Lokasi Pekerjaan/Pengiriman Barang/Jasa :</label>
                                <textarea id="lokasipekerjaan" rows="10" cols="80" name="lokasi_pekerjaan" placeholder="Type Something">
                                    {{ $sp3->lokasi_pekerjaan }}

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="jangkawaktu">Jangka Waktu Pengiriman Barang : </label>
                                <textarea id="jangkawaktu" rows="10" cols="80" name="jangka_waktu" placeholder="Type Something">
                                        {{ $sp3->jangka_waktu }}
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="harga">Harga Pekerjaan :</label>
                                <textarea id="hargaterbilang" name="harga_terbilang">
                                        {{ $sp3->harga_terbilang }}

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="tatacara">Tata Cara Pembayaran :</label>
                                <textarea rows="10" cols="80" name="cara_bayar" id="carabayar">
                                        {{ $sp3->cara_bayar }}

                                </textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Lain - lain : </label>
                                <textarea rows="10" cols="80" name="lain_lain" id="lainlain">
                                        {{ $sp3->lain_lain }}

                                </textarea>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Lampiran</label>
                                    <input type="file" name="lampiran[]" multiple class="form-control">
                                </div>
                            </div>
                            {{-- end hasil pembahasan --}}
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Comment</label>
                                    <span>
                                        *this field is required if you want submit
                                    </span>
                                    <input type="text" name="chat" class="form-control">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" name="status" value="draft_sp3" class="btn btn-primary" style="width: 7em;"><i class="fa fa-check"></i> Save</button>
                            <button type="submit" name="status" value="save_sp3" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    {{-- end form --}}
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
</div>
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
{{-- bahasa indonesia froala --}}
<script src="{{ asset('froala/js/languages/id.js') }}"></script>

<!-- Data Table -->
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- clockpicker -->
<script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<!-- Tagify -->
<script src="{{ asset('tagify/dist/jQuery.tagify.min.js') }}"></script>
<script src="{{ asset('tagify/dist/tagify.min.js') }}"></script>
<script>
 $('#chat').froalaEditor({
        key: '{{ env("KEY_FROALA") }}',
        height: 70,
        toolbarButtons: ['help'],
        charCounterCount: false,
        // language: 'id',

    })
    $('select').select2();

    function getRoles(val) {
        $('#role-cm_role_text').val('');
        var data = $('#role-cm_role').select2('data').map(function (elem) {
            return elem.text
        });
        console.log(data);
        $('#role-cm_role_text').val(data);
        $('#role-cm_role').on('select2:unselecting', function (e) {
            $('#role-cm_role_text').val('');
        });
    }
    $(function () {
        $(".datejos").on("change", function () {
            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DD")
                .format(this.getAttribute("data-date-format"))
            )
        }).trigger("change")
        $('.datejos').datepicker({
            autoclose: true,
            orientation: "bottom"
        })
    })
     // set default no_bak dan tgl_bak
     let get_tgl_bak = document.getElementById('tglsp3');
    var nomornya = document.getElementById('nosp3');
    var tglnya = nomornya.value.split('/');
    var nilai=[];
    for (var i = 0; i < tglnya.length-1; i++) {
        nilai.push(tglnya[i]) ;
    }
    function useValue() {
        let NameValue = get_tgl_bak.value.split('-');
        // use it
        document.getElementById('nosp3').value = nilai.join('/')+'/'+NameValue[0];
        
    }
    get_tgl_bak.onchange = useValue;
    get_tgl_bak.onblur = useValue;
    $('textarea').froalaEditor({
        placeholderText: '',
        language: 'id',
        charCounterCount: false,
        htmlAllowedStyleProps: ['font-family', 'font-size', 'background', 'color', 'width', 'text-align',
            'vertical-align', 'background-color', 'float'
        ],
        key: '{{ env("KEY_FROALA") }}',
    });


</script>
@endsection
