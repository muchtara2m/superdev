<div class="modal fade" id="modal-unit">
    <div class="modal-dialog " style="width:auto">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Data Unit</h4>
            </div>
            <div class="modal-body table-responsive">
                <table id="io" class="display table-responsive">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nomor SPPH</th>
                            <th>Judul</th>
                            <th>Harga</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($bakn as $ios)
                        <tr>
                            <td>{{ $ios['id'] }}</td>
                            <td style="white-space:nowrap">{{$ios->spph_lkpps['nomorspph']}}</td>
                            <td style="white-space:nowrap">{{$ios->spph_lkpps['judul']}}</td>
                            <td>{{ number_format($ios->harga) }}</td>
                            <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->