<p>Ref. No &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : &nbsp; &nbsp;{{ $refno }}</p>
<p>Tanggal (<em>date</em>) &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: &nbsp;
    &nbsp;{{ Carbon\Carbon::parse($docdate)->formatLocalized('%d %B %Y') }}</p>
    <p style="text-align: center; text-transform:uppercase"><strong>{{ $doctitle }}</strong></p>
    <br />
    <span style="display:inline-block; width:50%; vertical-align:top;"><em>Referensi:</em></span>
    <span style="display:inline-block; width:50%; vertical-align:top;">Kepada (<em>to</em>):</span>
    <span style="display:inline-block; width:50%; vertical-align:top;">Berita Acara Klarifikasi & Negosiasi Instalasi
        {{ $res->bakn_lkpp->spph_lkpps['judul']}} tanggal
        {{ Carbon\Carbon::parse($res->tanggal_sp3)->formatLocalized('%d %B %Y') }}</span>
        <span style="display:inline-block; width:50%; vertical-align:top;">Direktur
            {{ $res->bakn_lkpp->spph_lkpps->mitra_lkpps['perusahaan'] }}<br />{{ $res->bakn_lkpp->spph_lkpps->mitra_lkpps['alamat'] }}
        </span>
        
        <p><br /></p>
        <b>1. Ruang Lingkup :</b>
        &nbsp; &nbsp; {{ $res->ruang_lingkup }}
    </br>
    <b>2. Lokasi Pekerjaan/ Pengiriman Barang/ Jasa :</b>
    &nbsp; &nbsp; {{ $res->lokasi_pekerjaan }}
</br>
<b>3. Jangka Waktu Pengiriman Barang :</b>
&nbsp; &nbsp; {{ $res->jangka_waktu }}
</br>
<br><b>4. Harga :</b>
&nbsp; &nbsp; {{ $res->harga_terbilang }}
</br>
<b>5. Tatacara Pembayaran :</b>
&nbsp; &nbsp; {{ $res->cara_bayar }}
</br>
<b>6. Lain - lain :</b>
&nbsp; &nbsp; {{ $res->lain_lain }}
</br>
<p><em>Demikian Surat Penetapan ini dibuat sebagai dasar pelaksanaan pekerjaan sebelum ditandatanganinya
    Perjanjian/Kontrak.</em></p>
    <span style="display:inline-block; width:50%; vertical-align:top;">
        <p style="text-align:center">{{ $res->bakn_lkpp->spph_lkpps->mitra_lkpps['perusahaan'] }}</p>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <p style="text-align:center">{{ $res->bakn_lkpp->spph_lkpps->mitra_lkpps['direktur'] }}</p>
    </span>
    <span style="display:inline-block; width:50%; vertical-align:top;">
        <p style="text-align:center">PT. PINS Indonesia</p>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <p style="text-align:center">{{ $res->bakn_lkpp['pimpinan_rapat'] }}</p>
    </span>
    @if ($sp3->harga >= 1000000000 || $sp3->harga <=10000000000)
    <span style="display:inline-block; width:100%; vertical-align:top;">
            <p style="text-align:center">Menyetujui</p>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <p style="text-align:center">
                <b><u>{{ $dirop['name'] }}</u></b><br>
                {{ $dirop['position'] }} PT PINS Indonesia
            </p>
        </span>
    @endif
    