@extends('layouts.master')
@php
if (isset($sp3)) {
    $res = $sp3;
    $refno = $sp3->nomor_sp3;
    $docdate = $sp3->tanggal_sp3;
    $doctitle = "Surat Penetapan Pelaksanaan Pekerjaan (SPPP)";
}
@endphp
@php
$homelink = "/home";
$crpagename = "Preview SP3/SPK";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" />
<!-- bootstrap datepicker -->
<link rel="stylesheet"
href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />

@endsection
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Preview SP3/SPK
            <!-- <small>Form PBS</small> -->
        </h1>
        @if (\Session::has('Success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('Success') }}</p>
        </div><br />
        @endif
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Preview SP3/SPK</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Preview SP3/SPK</h3>
                        <button onclick="window.history.go(-1); return false;"
                        class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="white-box">
                                <textarea id="froala-editor">
                                  @include('modules.sp3_lkpp.modal.inc_preview')
                                </textarea>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
{{-- bahasa indonesia froala --}}
<script src="{{ asset('froala/js/languages/id.js') }}"></script>
{{-- select2 --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<!-- Initialize the editor. -->
<script>
    $('textarea#froala-editor').froalaEditor({
        // Define new table cell styles.
        toolbarButtons: ['print', 'html'],
        tabSpaces: 4,
        charCounterCount: false,
        fullPage: true,
        toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|',
        'fontFamily', 'fontSize', 'color', 'inlineStyle', 'inlineClass', 'clearFormatting', '|',
        'emoticons', 'fontAwesome', 'specialCharacters', 'paragraphFormat', 'lineHeight',
        'paragraphStyle', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '|',
        'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '-', 'insertHR',
        'selectAll', 'help', 'html', 'fullscreen', '|', 'undo', 'redo', 'getPDF', 'print'
        ],
        key: '{{ env("KEY_FROALA") }}',
    })
    
</script>
<script>
    $('select').select2();
    
    function getRoles(val) {
        $('#role-cm_role_text').val('');
        var data = $('#role-cm_role').select2('data').map(function (elem) {
            return elem.text
        });
        console.log(data);
        $('#role-cm_role_text').val(data);
        $('#role-cm_role').on('select2:unselecting', function (e) {
            $('#role-cm_role_text').val('');
        });
    }
    
</script>
{{-- signature --}}
<script src="{{ asset('signature/jSignature.min.js') }}"></script>
<!-- date-range-picker -->
<script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>
<!-- iCheck 1.0.1 -->
<script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<!-- clockpicker -->
<script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

@endsection
