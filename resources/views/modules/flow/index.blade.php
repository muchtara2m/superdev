@extends('layouts.master')

@section('title')
Flow | Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
</style>
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            FLOW
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Flow</a></li>
            <li class="active"> Flow </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible">
                              <button href="#" class="close" data-dismiss="alert" aria-label="close">&times;</button>
                              {{ $message }}
                            </div>
                            @endif
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Flow</h3>
                        <a href="{{ route('flow.create') }}">
                                <button class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add User</button>
                              </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="pbsTable" class="display">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Jabatan</th>
                                    <th>Username</th>
                                    <th>Minimal</th>
                                    <th>Maximal</th>
                                    <th>Transaksi</th>
                                    <th>Queue</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($dtflow as $item)
                                    <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $item->jabatan }}</td>
                                    <td>{{ $item->username }}</td>
                                    <td>{{ $item->min }}</td>
                                    <td>{{ $item->max }}</td>
                                    <td>{{ $item->transaksi }}</td>
                                    <td>{{ $item->queue }}</td>
                                    <td><a href="#modal_unit{{ $item->id }}"  data-toggle="modal" data-target="#modal-unit_{{ $item->id }}">Edit</a>

                                        <div class="modal fade" id="modal-unit_{{ $item->id }}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title">Pilih Tanggung Jawab</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                {{-- <form method="post" action="{{action('KontrakController@disp', $item->id)}}"> --}}
                                                                    @csrf
                                                                    {{-- @method('PATCH') --}}
                                                                    <input type="text" value="{{ $item->id }}" hidden>
                                                                    <br>
                                                                    <select name="handler"  class="form-control">
                                                                        <option value="">Pilih Approval</option>
                                                                        @foreach ($users as $list)
                                                                        <option value="{{ $list['nama'] }}">{{ $list['username'].' - '.$list->position }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <br>
                                                                    <button type="submit" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i>
                                                                        Submit</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                    <!-- /.modal -->
                                    </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
    $(document).ready( function () {
        $('#pbsTable').DataTable({

        });
    } );</script>

    @endsection
