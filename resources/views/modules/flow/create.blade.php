@extends('layouts.master')

@php
$homelink = "/home";
$crmenu = "Master Data";
$crsubmenu = "Data Unit";
$submenulink = "/unit";
$cract = "Add Data Unit";
@endphp

@section('title')
{{ $crsubmenu." | SuperSlim" }}
@endsection

@section('stylesheets')
@endsection

@section('customstyle')
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
</style>
@endsection
{{-- select2 --}}
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
{{-- datatable --}}
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">

{{-- ajax script src --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>


@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $crsubmenu }}
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li>{{ $crmenu }}</li>
            <li><a href="{{ $submenulink }}">{{ $crsubmenu }}</a></li>
            <li class="active">{{ $cract }} </li>
        </ol>
    </section>
    
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Data Flow</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="POST" action="{{ route('flow.store') }}" enctype="multipart/form-data" class="form-horizontal">
                        @csrf
                        @method('POST')
                        <div class="box-body">
                            <div class="form-group col-md-12">
                                <div class="form-group col-md-12 igroup">
                                    <label for="exampleInputEmail1">Nama Approval</label>
                                    <div class="input-group">
                                        <div class="row col-md-4">
                                            <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="Jabatan" readonly>
                                        </div>
                                        <div class="col-md-8 hidden-sm hidden-xs">
                                            <input type="text" class="form-control" id="username" name="username" placeholder="Nama" readonly>
                                        </div>
                                        
                                        <!-- browse button -->
                                        <span class="input-group-btn">
                                            <label class="btn btn-primary" title="Browse" data-toggle="modal" data-target="#modal-unit">
                                                <span class="fa fa-search"></span>
                                                <span class="hidden-xs">Browse</span>
                                            </label>
                                        </span>
                                        
                                    </div>
                                </div>
                                <div class="form-group col-md-6 ">
                                    <label>Minimal Transaksi</label>
                                    <input name="min" type="text" class="form-control" value="{{ old('nama') }}" onkeyup="formatAngka(this,&quot;.&quot;)" required>
                                </div>
                                <div class="form-group col-md-6 ">
                                    
                                    <label>Maximal Transaksi</label>
                                    <input name="max" type="text" class="form-control" value="{{ old('nama') }}" onkeyup="formatAngka(this,&quot;.&quot;)" required>
                                </div>
                                <div class="form-group col-md-6 "hidden>
                                    
                                    <label>Urutan Flow</label>
                                    <select name="queue" id="" class="form-control">
                                        <option selected disabled>Pilih Urutan</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6 ">
                                        <label>Unit</label>
                                        <select name="unit" id="dari" class="form-control">
                                            @foreach ($units as $item)
                                            <option value="{{ $item->nama }}">{{ $item->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                <div class="form-group col-md-6 ">
                                    <label>Transaksi</label>
                                    <select name="transaksi" id="" class="form-control">
                                        <option selected disabled>Pilih Transaksi</option>
                                        <option value="SP3">SP3</option>
                                        <option value="BAKN">BAKN</option>
                                        <option value="BAK">BAK</option>
                                        <option value="SPK">SPK</option>
                                        <option value="PBS">PBS</option>
                                        <option value="Kontrak">Kontrak</option>
                                        <option value="JUSTIFIKASI">JUSTIFIKASI</option>
                                    </select>
                                </div>
                                {{-- <input value="0" id="nilai_project" class="nilai_project" maxlength="15" required="required" onkeyup="formatAngka(this,&quot;.&quot;)" name="Inisiasi[nilai_project]" type="text" /> --}}
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-success col-md-3" style="width: 7em;"><i class="fa fa-check"></i> Submit</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                    <!-- modal -->
                    <div class="modal fade" id="modal-unit">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title">Data Unit</h4>
                                    </div>
                                    <div class="modal-body">
                                        <table id="flow" class="display table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>Jabatan</th>
                                                    <th>Username</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($users as $list)
                                                <tr>
                                                    <td>{{$list['position']}}</td>
                                                    <td>{{$list['username']}}</td>
                                                    <td><a href="#" class="btn btn-primary " data-dismiss="modal" id="tutup">Select</a></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                        {{--  <button type="button" class="btn btn-primary">Save changes</button>  --}}
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    
    @endsection
    
    @section('scripts')
    {{-- datatable --}}
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    {{-- select2  --}}
    <script src="{{ asset('js/select2.min.js') }}"></script>
    
    <script>
        $(document).ready( function () {
            $('#flow').DataTable();
        } )
        var table = document.getElementById('flow');
        
        for(var i = 1; i < table.rows.length; i++)
        {
            table.rows[i].onclick = function()
            {
                //  rIndex = this.rowIndex;
                document.getElementById("jabatan").value = this.cells[0].innerHTML;
                document.getElementById("username").value = this.cells[1].innerHTML;
                
            };
        };
        function formatAngka(objek, separator) {
            a = objek.value;
            b = a.replace(/[^\d]/g, "");
            c = "";
            panjang = b.length;
            j = 0;
            for (i = panjang; i > 0; i--) {
                j = j + 1;
                if (((j % 3) == 1) && (j != 1)) {
                    c = b.substr(i - 1, 1) + separator + c;
                } else {
                    c = b.substr(i - 1, 1) + c;
                    formatAngka
                }
            }
            objek.value = c;
        }
        
        function clearDot(number)
        {
            output = number.replace(".", "");
            return output;
        }
        // select2
        $('select').select2();
        function getRoles(val) {
            $('#role-cm_role_text').val('');
            var data = $('#role-cm_role').select2('data').map(function(elem){ return elem.text} );
            // console.log(data);
            $('#role-cm_role_text').val(data);
            $('#role-cm_role').on('select2:unselecting', function (e) {
                $('#role-cm_role_text').val('');
            });
        }
    </script>
    @endsection
    