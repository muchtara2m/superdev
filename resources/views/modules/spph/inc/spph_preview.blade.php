<textarea class="froala" style="">
        <!DOCTYPE html>
        <html>
        <head>
            <title></title>
        </head>
        <body>
            
            <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Nomor &nbsp;:&nbsp;</span><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>{{   $spph->nomorspph }}&nbsp;</span></p>
            
            <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'>
                <br>
            </p>
            
            <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Jakarta,&nbsp;</span><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>{{ Carbon\Carbon::parse($spph->tglspph)->formatLocalized('%d %B %Y') }}</p>
    
                
                <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Kepada Yth.</span></strong></p>
                
                <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Direktur {{ $spph->mitras['perusahaan'] }}</span></strong></p>
                
                {{-- <p style=' font-size:13pxfont-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Di Tempat&nbsp;</span></strong></p> --}}
                <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>{{ $spph->mitras['alamat'] }}&nbsp;</span></strong></p>
                <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>{{ $spph->mitras['telp'] }}&nbsp;</span></strong></p>
                
                <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'>
                    <br>
                </p>
                
                <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;margin-left:63.0pt;text-align:justify;text-indent:-63.0pt;'><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Perihal &nbsp; &nbsp; &nbsp; &nbsp;:&nbsp;&nbsp;</span><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>{{ $spph->perihal }}&nbsp;</span></strong></p>
                
                <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;margin-left:63.0pt;text-align:justify;text-indent:-63.0pt;'>
                    <br>
                </p>
                
                <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Dengan hormat,</span></p>
             
                <p style=' font-size:13pxfont-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'>
                    <span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Diinformasikan untuk memenuhi kebutuhan</span>
                    <span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>PT. PINS Indonesia</span>
                    <span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>akan</span>
                    <strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>{{ $spph->judul }},</span></strong>
                    <span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>dengan rincian sebagaimana tertera pada SPPH ini.</span>
                    <span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Kami mengundang Perusahaan Saudara untuk berpartisipasi dalam memenuhi kebutuhan</span>
                    <span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>sebagaimana <strong><em>terlampir</em></strong></span><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>.</span></p>
                      
                            {{ $spph->detail }}
                      
                    
                    <p style=' font-size:13pxfont-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Sehubungan dengan hal tersebut di atas, kami mohon agar Saudara mengirimkan Surat Penawaran Harga (SPH) kepada kami selambat-lambatnya hari</span>
                        <strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>{{ Carbon\Carbon::parse($spph->tglsph)->formatLocalized('%A') }}</span></strong>
                        <span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>tanggal</span>
                        <strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>{{ Carbon\Carbon::parse($spph->tglsph)->formatLocalized('%d %B %Y') }}</span></strong>
                        <span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>jam</span>
                        <strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>{{ Carbon\Carbon::parse($spph->tglsph)->formatLocalized('%H:%M') }} WIB</span></strong>
                        <span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>dan ditujukan kepada:</span><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>&nbsp;</span></p>
                  
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;text-indent:36.0pt;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>{{ $dari->position }}</span></strong></p>
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;text-indent:36.0pt;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>PT.&nbsp;</span></strong><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>PINS Indonesia</span></strong></p>
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;text-indent:36.0pt;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>The Telkom HUB &nbsp;</span></strong></p>
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;text-indent:36.0pt;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Telkom Landmark Tower lantai 42-43 &nbsp;</span></strong></p>
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;text-indent:36.0pt;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Jl. Gatot Subroto No.Kav. 52, Kuningan Barat,</span></strong></p>
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;text-indent:36.0pt;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Mampang Prapatan, Kota Jakarta Selatan</span></strong></p>
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;text-indent:36.0pt;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Daerah Khusus Ibukota Jakarta 12710</span></strong></p>
                    
                        
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Apabila ada hal-hal yang kurang jelas, dapat ditanyakan langsung kepada :</span></p>
                      
                        
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;margin-left:36.0pt;text-align:justify;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Sdr. {{ $pic->name }}</span></strong></p>
                        
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;margin-left:36.0pt;text-align:justify;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>{{ $pic->position }}&nbsp;</span></strong></p>
                        
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;margin-left:36.0pt;text-align:justify;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Email. <a href="mailto:{{ $pic->email }}">{{ $pic->email }}</a></span></strong></p>
                        
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;margin-left:36.0pt;text-align:justify;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Tlp. 021 &ndash; 50820790&nbsp;</span></strong></p>
                        
                      
                        
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Demikian disampaikan, terimakasih atas perhatian dan kerjasamanya.&nbsp;</span></p>
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Hormat Kami,</span></p>
                        
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'><strong><u><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>{{ $spph->dari }}</span></u></strong></p>
                        <br>
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'><strong><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>{{ $spph->position }}</span></strong></p>
                        
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>Tembusan :</span></p>
                        
                        <p style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",serif;text-align:justify;'><span style=' font-size:13px;font-family:"Calibri, Candara, Segoe, Segoe UI, Optima, Arial, sans-serif;",sans-serif;'>{{ $spph->tembusan }}</span></p>
                    </body>
                    </html>
                    
                    
                </textarea>