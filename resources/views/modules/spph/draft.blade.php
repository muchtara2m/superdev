@extends('layouts.master')

@section('title')
Draft SPPH| Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            DRAFT SPPH
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">SPPH</a></li>
            <li class="active"> Draft SPPH </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Draft SPPH</h3>
                    </div>
                    <div style="text-align:center;padding-bottom:10px">
                        <div class="row input-daterange">
                            <form method="get" action="/spph-draft" enctype="multipart/form-data">
                                <div class="col-md-4">
                                    <select name="bulan" id="bulan" class="form-control">
                                        @php
                                        if(request()->get('bulan') == null){
                                        $bln = date('m');
                                        $bulan = date('F',strtotime(date('Y-m-d')));
                                        $thn = date('Y');
                                        }else{
                                        $bln = request()->get('bulan');
                                        $bulan = date('F', mktime(0, 0, 0, request()->get('bulan'), 10));
                                        $thn = request()->get('tahun');
                                        }
                                        @endphp
                                        <option value="{{ $bln }}" selected>{{ $bulan }}</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select name="tahun" id="tahun" class="form-control">
                                        <option value="{{ $thn }}" selected>{{ $thn }}</option>
                                    </select>
                                </div>
                                <input type="submit" value="Filter" class="btn btn-primary">

                                <div class="col-md-4">
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="draft" class="display">
                            <thead>
                                <tr style="white-space:nowrap">
                                    <th>No</th>
                                    <th>Nomor SPPH</th>
                                    <th>Judul</th>
                                    <th>Mitra</th>
                                    <th>Tembusan</th>
                                    <th>Perihal</th>
                                    <th>Penanggung Jawab</th>
                                    <th>Tanggal</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $no=1;
                                @endphp
                                @foreach ($draft as $item)
                                <tr style="white-space:nowrap">
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->nomorspph }}</td>
                                    <td>{{ $item->judul }}</td>
                                    <td>{{ $item->mitras['perusahaan'] }}</td>
                                    <td>{{ $item->tembusan }}</td>
                                    <td>{{ $item->perihal }}</td>
                                    <td>{{ $item->dari }}</td>
                                    <td>{{ date('d.F.Y', strtotime($item->created_at)) }}</td>
                                    <td>
                                        <a href="{{ url('spph-edit',$item->id) }}" class="fa fa-fw fa-edit"></a>
                                        <a href="spph-delete/{{ $item->id }}" class="fa fa-fw fa-trash"
                                            onclick="return confirm('Bener nih mau dihapus?')" title="Delete File"></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<script src="{{ asset('js/web/monthAndYear.js') }}"></script>
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
    $(document).ready(function () {
        $('#draft').DataTable({
            scrollY: "50vh",
            scrollCollapse: true,
        });
    });
</script>
@endsection