@extends('layouts.master')

@php
$homelink = "/home";
$crmenu = "General Support";
$crsubmenu = "Create SPPH";
$submenulink = "spph-create";
// $cract = "Add Data Unit";
@endphp

@section('title')
{{ $crsubmenu." | SuperSlim" }}
@endsection
{{-- datetimepicker --}}
<link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}">
{{-- select2 --}}
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
{{-- datatable --}}
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<!-- bootstrap datepicker -->
<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
{{-- css froala editor --}}
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
{{-- moment script src --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>


@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $crsubmenu }}
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li>{{ $crmenu }}</li>
            <li><a href="{{ $submenulink }}">{{ $crsubmenu }}</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                There was a problem, please check your form carefully.
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="col-md-12">
                <div class="box box-info">
                    <form method="POST" action="{{ route('spph-store') }}" enctype="multipart/form-data" class="form"
                        id="form">
                        @csrf

                        <div class="box-body">
                            <div class="form-group col-sm-12">
                                <label for="tglspph">Tanggal Terima Berkas*</label>
                            <input type="text" data-date="" data-date-format="yyyy-mm-dd" name="tanggal_berkas" class="form-control datejos" id="" placeholder="{{ date('Y-m-d')}}" value="{{ old('tanggal_berkas')}}">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="tglspph">Tanggal SPPH*</label>
                                <input type="text" data-date="" data-date-format="yyyy-mm-dd"
                                    class="form-control datejos" name="tglspph" id="tglspph" placeholder="{{ date('Y-m-d')}}"
                                    value="{{ old('tglspph') }}">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="nomorspph">Nomor SPPH*</label>
                                <input type="text" name="nomorspph" class="form-control" id="nomorspph"
                                    placeholder="Nomor SPPH" value="{{ old('nomorspph') }}">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="tglsph">Tanggal SPH*</label>
                                <input type="text" data-date="" data-date-format="yyyy-mm-dd"
                                    class="form-control datepick @error('title') is-invalid @enderror" name="tglsph"
                                    id="datetimepicker_dark" placeholder="{{ date('Y-m-d H:i:s')}}" value="{{ old('tglsph') }}">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="kepada">Kepada*</label>
                                <input type="hidden" name="kepada" id="idmitra">
                                <input type="text" class="form-control" placeholder="PT XYZ" id="kepada"
                                    data-toggle="modal" data-target="#modal-unit" value="{{ old('kepada') }}">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="judul">Judul*</label>
                                <input type="text" name="judul" class="form-control" placeholder="Pengadaan Laptop Macbook Pro"
                                    value="{{ old('judul') }}">
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="pic">Pembuat*</label>
                                <select name="pembuat" id="pic" class="form-control">
                                    <option value="{{ Auth::user()->name }}" selected>{{ Auth::user()->name }}</option>
                                    @foreach ($user as $item)
                                    <option value="{{ $item->name }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="dari">Penanggung Jawab*</label>
                                <select name="handler" id="dari" class="form-control">
                                    @foreach ($user as $item)
                                    <option value="{{ $item->name }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-sm-6">
                                <label for="tembusan">Tembusan*</label>
                                <select id="role-cm_role" class="form-control" name="" multiple size="4"
                                    onchange="getRoles(this.value)">
                                    @foreach ($tembusan as $item)
                                    <option value="{{ $item->name }}"
                                        {{  (old('tembusan') == $item->name) ? 'selected' : ''}}>{{ $item->name }}
                                    </option>
                                    @endforeach
                                </select>
                                <input type="hidden" id="role-cm_role_text" class="form-control" name="tembusan">
                            </div>
                            <div class="form-group col-md-12" hidden>
                                <label for="perihal">Perihal</label>
                                <input type="text" name="perihal" class="form-control"
                                    value="Surat Permintaan Penawaran Harga (SPPH)">
                            </div>
                            <div class="form-group col-md-12">
                                <label for="perihal">Detail Table</label>
                                <span>*This field for detail list item </span>
                                <textarea name="detail" class="font">

                                </textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label for="">File Lampiran</label>
                                <input type="file" name="lampiran[]" class="form-control" multiple="multiple">
                            </div>
                            <div class="form-group col-md-12">
                                <button type="submit" name="status" value="draft_spph" class="btn btn-success"
                                    style="width: 7em;">Save</button>
                                <button type="submit" name="status" value="save_spph" class="btn btn-primary "
                                    style="width: 7em;">Submit</button>
                            </div>

                        </div>
                        <!-- /.box-body -->
                    </form>

                </div>
            </div>
            @include('modules.spph_lkpp.modal.mitra_modal')
        </div>
        <!--/.col (right) -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
{{-- froala --}}
<script src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
{{-- datatable --}}
<script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
{{-- datetimepicker --}}
<script src="{{ asset('js/jquery.datetimepicker.full.min.js') }}"></script>
{{-- select2  --}}
<script src="{{ asset('js/select2.min.js') }}"></script>
{{-- script function date picker --}}
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}">
</script>
{{-- external script --}}
<script>
    // froala
    $('textarea').froalaEditor({
        placeholderText: '',
        charCounterCount: false,
        heightMin: 200,
        key: '{{ env("KEY_FROALA") }}',
    });
    // jQuery
    // set default nomorspph
    var gettglspph = document.getElementById('tglspph');

    function useValue() {
        var NameValue = gettglspph.value.split('-');
        var year = gettglspph[2];
        console.log(NameValue[0]); // just to show the new value
        // use it
        document.getElementById('nomorspph').value = "/LG.220/PIN.00.00/" + NameValue[0];
    }
    gettglspph.onchange = useValue;
    gettglspph.onblur = useValue;

    // datatable
    $(document).ready(function () {
        $('#mitra').DataTable();
    })
    var table = document.getElementById('mitra');

    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            //  rIndex = this.rowIndex;
            document.getElementById('idmitra').value = this.cells[0].innerHTML;
            document.getElementById("kepada").value = this.cells[1].innerHTML;

        };
    };
    // select2
    $('select').select2();

    function getRoles(val) {
        $('#role-cm_role_text').val('');
        var data = $('#role-cm_role').select2('data').map(function (elem) {
            return elem.text
        });
        // console.log(data);
        $('#role-cm_role_text').val(data);
        $('#role-cm_role').on('select2:unselecting', function (e) {
            $('#role-cm_role_text').val('');
        });
    }
    // datepicker tanggal spph
    $(".datejos").on("change", function () {
        this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
            .format(this.getAttribute("data-date-format"))
        )
    }).trigger("change")

    $('.datejos').datepicker({
        autoclose: true,
        orientation: "bottom"
    })

    // datepicker tanggal sph
    $.datetimepicker.setLocale('id');
    $('.datepick').datetimepicker({
        theme: 'dark'
    })

    $('.timepick').datetimepicker({
        datepicker: false,
        format: 'H:i',
        step: 5
    });

</script>
@endsection
