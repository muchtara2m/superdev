@extends('layouts.master')

@section('title')
Preview SPPH| Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
{{-- css froala editor --}}
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
<style>
    div.froala {
        font-size: 12px;
        font-family: 'Roboto', sans-serif;
    }

</style>
@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            PREVIEW SPPH

            <!-- <small>Form PBS</small> -->
        </h1>

        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">SPPH</a></li>
            <li class="active"> Preview SPPH </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div><br />
        @endif
        <div class="row">

            <!-- right column -->
            <div class="col-md-12">

                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Preview SPPH</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i
                                class="fa fa-arrow-left"></i></button>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        @php
                        if($spph->lampiran == NULL){
                        }else{
                        $title = json_decode($spph->title_lampiran, TRUE);
                        $file = json_decode($spph->lampiran, TRUE);
                        $i=1;
                        foreach ($title as $key => $value) {
                        echo $i++.'. <a href="'.Storage::url($file[$key]).'">'.$title[$key].'</a><br>';
                        }
                        }
                        @endphp
                        <br>
                        @include('modules.spph.inc.spph_preview')
                    </div>


                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
{{-- script function froala --}}
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<script src="{{ asset('froala/js/languages/id.js') }}"></script>
<script type="text/javascript">
    $('textarea').froalaEditor({
        fullPage: true,
        // toolbarButtons :['print', 'html','getPDF'],
        charCounterCount: false,
        key: '{{ env("KEY_FROALA") }}',
    })

</script>
@endsection
