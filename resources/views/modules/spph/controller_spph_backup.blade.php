  public function index(Request $request)
    {
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
                $var = '>=';
                $bln = $request->bulan;
            }else{
                $var = '=';
                $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator'){
            $spphs = Spph::with(['mitras','creator'])
            ->where([
                ["status", "save_spph"]
                ])
                ->whereMonth('created_at',$var,$bln)                                
                ->whereYear('created_at',$thn)
                ->orderBy('updated_at','desc')
                ->get();
            return view('modules.spph.index',compact('spphs'));
        }else {
            $spphs = Spph::with(['mitras','creator'])
            ->where([
                ["status", "save_spph"],
                ['created_by',Auth::user()->id]
                ])
                ->whereMonth('created_at',$var,$bln)                                
                ->whereYear('created_at',$thn)
                ->orderBy('updated_at','desc')
                ->get();
        return view('modules.spph.index',compact('spphs'));
        }

    }
    public function draft(Request $request)
    {
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
                $var = '>=';
                $bln = $request->bulan;
            }else{
                $var = '=';
                $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator'){
            $draft = Spph::with(['creator','mitras'])->where([
                ["status", "draft_spph"]
                ])
                ->whereMonth('created_at',$var,$bln)                                
                ->whereYear('created_at',$thn)
                ->orderBy('updated_at','desc')
                ->get();
            return view('modules.spph.draft', compact('draft'));
        }else {
            $draft = Spph::with(['creator','mitras'])->where([
                ["status", "draft_spph"],
                ['created_by',Auth::user()->id]
                ])
                ->whereMonth('created_at',$var,$bln)                                
                ->whereYear('created_at',$thn)
                ->orderBy('updated_at','desc')
                ->get();
            return view('modules.spph.draft', compact('draft'));
        }

    }

    public function done(Request $request)
    {
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
                $var = '>=';
                $bln = $request->bulan;
            }else{
                $var = '=';
                $bln = $request->bulan;
            }
            $thn = $request->tahun;           
        }
        DB::enableQueryLog();
        if(Auth::user()->level == 'administrator'){
            $done = Spph::with(['creator','mitras'])->where([
                ["status", "done_spph"],
                ])
                ->whereMonth('created_at',$var,$bln)                
                ->whereYear('created_at',$thn)
                ->orderBy('updated_at','desc')
                ->get();
            // dd(DB::getQueryLog());

            return view('modules.spph.done', compact('done'));
        }else{
            $done = Spph::with(['creator','mitras'])->where([
                ["status", "done_spph"],
                ['created_by',Auth::user()->id],
                ])
                ->whereMonth('created_at',$var,$bln)
                ->whereYear('created_at',$thn)
                ->orderBy('updated_at','desc')
                ->get();
                // dd(DB::getQueryLog());
            return view('modules.spph.done', compact('done'));
        }
        
    }

    function selectSpph(Request $request){
        if($request->ajax() && $request->jns != null){
            if($request->jns == 'ecom'){
                $spphs = DB::table('users')->where('id_unit',56)->get();
                $dari = DB::select("SELECT * FROM `users` where  (position like 'gm%' or position like 'vp%' or position like 'mgr%' or position like 'avp%') AND id_unit= 56 ");
                return response()->json(['pembuat'=>$spphs,'handler'=>$dari]);
            }else if($request->jns == 'gs'){
                $spphs = DB::table('users')->where('id_unit',59)->get();
                $dari = DB::select("SELECT * FROM `users` where  (position like 'gm%' or position like 'vp%' or position like 'mgr%' or position like 'avp%') AND id_unit= 59 ");
                return response()->json(['pembuat'=>$spphs,'handler'=>$dari]);
            }
        }
    }

    public function create()
    {
        $mitras = Mitra::all();
        $users = User::all();
        $spphs = Spph::all();
        // $pic = DB::select("SELECT * FROM `users` where  (position like 'mgr%' or position like 'avp%' or position like 'off%') AND (id_unit= 56 OR id_unit=59)");
        // $dari = DB::select("SELECT * FROM `users` where  (position like 'gm%' or position like 'vp%' or position like 'mgr%' or position like 'avp%') AND (id_unit= 56 OR id_unit=59)");
        $tembusan = DB::select("SELECT * FROM `users` where  (position like 'mgr%' or position like 'avp%' or position like 'gm%' or position like 'vp%') AND (id_unit= 44 OR id_unit= 45 OR id_unit= 46 OR id_unit= 47 OR id_unit= 48 OR id_unit=49 OR id_unit=54 OR id_unit=55 OR id_unit=56 OR id_unit=38) ");
        return view('modules.spph.create', compact('users','mitras','spphs','tembusan'));
    }
    public function store(Request $request)
    {
        //
        $customMessages = [
            'required' => ucfirst(':attribute').' field is required.',
            'tglsph.required' => 'Tanggal SPH field is required',
            'handler.required' => 'Penanggung Jawab field is required',
            'pembuat.required' => 'Pembuat field is required',
            'unique' => ':Attribute sudah pernah dibuat',
        ];
        $this->validate($request, [
            'nomorspph' => 'required',
            'nomorspph' => 'unique:spphs,nomorspph',
            'tglspph' => 'required',
            'tglsph' => 'required',
            'kepada' => 'required',
            'handler' => 'required',
            'judul' => 'required',
            'tembusan' => 'required',
            'pembuat' => 'required',
            'status' => 'required',
            'perihal' => 'required',
        ], $customMessages);
            DB::beginTransaction();
            try {
            $spph = new SPPH();
            $spph->nomorspph = $request->input('nomorspph');
            $spph->tglspph = $request->input('tglspph');
            $spph->tglsph = $request->input('tglsph');
            $spph->mitra = $request->input('kepada');
            $spph->dari = $request->input('handler');
            $spph->tembusan = $request->input('tembusan');
            $spph->perihal = $request->input('perihal');
            $spph->pic = $request->input('pembuat');
            $spph->judul = $request->input('judul');
            $spph->status = $request->input('status');
            $spph->created_by = Auth::user()->id;
            $spph->save();
            DB::commit();
            $idnya = $spph->id;
            return redirect('spph-preview/'.$idnya)->with('success', 'Data ('.$spph->nomorspph.') was added successfully');
            } catch (ValidationException $e) {
                DB::rollback();
                return redirect()->back()
                ->withErrors("Something wrong with your form, please check carefully")
                ->withInput();
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->back()
                ->withErrors("Something wrong from the server, please check carefully")
                ->withInput();
            }
          
        }

        public function edit( $id)
        {

            $spph = Spph::with('mitras','creator')->where('id',$id)->first();
            $tembus = explode(",", $spph->tembusan);
            $mitras = Mitra::all();
            $users = User::all();
            $tembus = explode(",",$spph->tembusan);
            $tembusan = DB::select("SELECT * FROM `users` where  (position like 'mgr%' or position like 'avp%' or position like 'gm%' or position like 'vp%') AND (id_unit= 44 OR id_unit= 45 OR id_unit= 46 OR id_unit= 47 OR id_unit= 48 OR id_unit=49) ");
            if(strpos($spph->nomorspph, 'ECOM') !== FALSE) {
                $pic = DB::select("SELECT * FROM `users` where  id_unit= 56");
                $dari = DB::select("SELECT * FROM `users` where  (position like 'gm%' or position like 'vp%' or position like 'mgr%' or position like 'avp%') AND id_unit=56");
                return view('modules.spph.edit', compact('tembus','users','spph','pic','dari','tembus','mitras','tembusan'));
            }else{
                $pic = DB::select("SELECT * FROM `users` where  id_unit= 59");
                $dari = DB::select("SELECT * FROM `users` where  (position like 'gm%' or position like 'vp%' or position like 'mgr%' or position like 'avp%') AND id_unit=59");
                return view('modules.spph.edit', compact('tembus','users','spph','pic','dari','tembus','mitras','tembusan'));

            }


        }

        public function update(Request $request, $id)
        {
            //
            $spph = Spph::find($id);
            $spph->nomorspph = $request->input('nomorspph');
            $spph->tglspph = $request->input('tglspph');
            $spph->tglsph = $request->input('tglsph');
            $spph->mitra = $request->input('kepada');
            $spph->dari = $request->input('dari');
            $spph->tembusan = $request->input('tembusan');
            $spph->perihal = $request->input('perihal');
            $spph->pic = $request->input('pic');
            $spph->judul = $request->input('judul');
            $spph->status = $request->input('status');
            $spph->save();
            if($spph->status == 'draft_spph'){
                return redirect('spph-draft')->with('success', 'Data has been edited');

            }else{
                return redirect('spph-preview/'.$id)->with('success', 'Data ('.$spph->nomorspph.') was added successfully');
            }
        }

        public function destroy( $id)
        {
            
            $spph = Spph::findOrFail($id);
            $spph->delete();
            $path = "public/files/SPPH/".$spph->creator->unitnya['nama']."/".$id;
            Storage::deleteDirectory($path);
            return redirect('spph-done')->with('success','Anda telah berhasil menghapus data');
        }

        public function preview($id)
        {
            $preview = DB::table('spphs')
            ->join('users','spphs.dari','=','users.name')
            ->select('spphs.*', 'users.position')
            ->where('spphs.id',$id)
            ->first();
            $pic = DB::table('spphs')
            ->join('users','spphs.pic','=','users.name')
            ->select('spphs.pic', 'users.*')
            ->where('spphs.id',$id)
            ->first();
            $dari =  DB::table('spphs')
            ->join('users','spphs.dari','users.name')
            ->select('spphs.dari', 'users.*')
            ->where('spphs.id', $id)
            ->first();
            $mitra = DB::table('spphs')
            ->join('mitras','spphs.mitra','=','mitras.id')
            ->select('spphs.mitra','mitras.*')
            ->where('spphs.id',$id)
            ->first();
            return view('modules.spph.preview', compact('preview','mitra','pic','dari'));

        }
        public function lampiran(Request $request, $id)
        {

            $this->validate($request, [
                'file' => 'required',
            ]);
                $unit = Unit::where('id',Auth::user()->id_unit)->first();
                // dd($unit->nama);
                $path= "public/files/SPPH/".$unit->nama."/".$id;
                if($request->file('file'))
                {
                    foreach($request->file('file') as $file)
                    {
                        $name=$file->getClientOriginalName();
                        $file->storeAs($path, $name);
                        $namanya[] = $name;
                        $data[] = $path.'/'.$name;
                    }
                }
                $file = Spph::find($id);

                $file->nomorsph = $request->input('nomorsph');
                $file->status = 'done_spph';
                $file->file=json_encode($data);
                $file->title = json_encode($namanya);
                $file->upload = date('Y-m-d H:i:s');
                $file->save();
                return redirect('spph-done')->with('success', 'Your files has been successfully added');
            }


        }
