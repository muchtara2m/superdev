@extends('layouts.master')

@section('title')
Edit Cara Bayar
@endsection


@section('stylesheets')

<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('customstyle')
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            BAKN
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="/"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Cara Bayar</a></li>
            <li class="active">Edit </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Edit Cara Bayar</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
                    </div>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        There was a problem, please check your form carefully.
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="{{action('CaraBayarController@update', $id)}}">
                        @csrf
                        @method('PATCH')
                        <div class="box-body">
                            <div class="form-group col-md-12">
                                <div class="form-group">
                                    <label for="jenispasal">Jenis Bayar</label>
                                    <input type="text" class="form-control" name="jenis" value="{{ $bayar->jenis }}" >
                                </div>
                                <div class="form-group">
                                    <label for="isipasal">Isi Bayar</label>
                                    <textarea name="isi">
                                        {{ $bayar->isi }}
                                    </textarea>
                                </div>

                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <button type="submit" class="btn btn-success" style="width: 7em;"><i class="fa fa-check"></i>
                                        Submit</button>
                                </div>
                                <!-- /.box-footer -->
                    </form>
                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
    <!-- Include Editor JS files. -->
    <script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
    <!-- Initialize the editor. -->
    <script>
        $('textarea').froalaEditor({
            key: '{{ env("KEY_FROALA") }}',
            height :500,
            charCounterCount :false,
        }); 
        // removo alert lisensi froala
        $("div > a", ".fr-wrapper").css('display', 'none');    
    </script>

@endsection
