@extends('layouts.master')

@section('title')
Kontrak Draft| Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            KONTRAK DRAFT
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active"> Kontrak Draft </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Kontrak</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="nav-tabs-custom">
                        <div style="text-align:center;padding-bottom:10px">
                            <div class="row input-daterange">
                                <form method="get" action="{{ url('kontrak-non-draft') }}"
                                    enctype="multipart/form-data">
                                    <div class="col-md-4">
                                        <select name="bulan" id="bulan" class="form-control">
                                            @php
                                            if(request()->get('bulan') == null){
                                            $bln = date('m');
                                            $bulan = date('F',strtotime(date('Y-m-d')));
                                            $thn = date('Y');
                                            }else{
                                            $bln = request()->get('bulan');
                                            $bulan = date('F', mktime(0, 0, 0, request()->get('bulan'), 10));
                                            $thn = request()->get('tahun');
                                            }
                                            @endphp
                                            <option value="{{ $bln }}" selected>{{ $bulan }}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select name="tahun" id="tahun" class="form-control">
                                            <option value="{{ $thn }}" selected>{{ $thn }}</option>
                                        </select>
                                    </div>
                                    <input type="submit" value="Filter" class="btn btn-primary">

                                    <div class="col-md-4">
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="box-body table-responsive">
                            <table id="kontrak" class="display">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>IO</th>
                                        <th>Deskripsi IO</th>
                                        <th>Nomor SPPH</th>
                                        <th>Nomor Kontrak</th>
                                        <th>Judul SPPH</th>
                                        <th>Mitra</th>
                                        <th>Tangal BAKN</th>
                                        <th>Harga</th>
                                        <th>Lampiran</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $no=1;
                                    @endphp
                                    @foreach ($bakn as $list)
                                    <tr style="white-space:nowrap">
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $list->bakns->io['no_io'] }}</td>
                                        <td>{{ $list->bakns->io['deskripsi'] }}</td>
                                        <td>{{ $list->bakns->spph['nomorspph'] }}</td>
                                        <td>{{ $list->nokontraknon }}</td>
                                        <td>{{ $list->bakns->spph['judul'] }}</td>
                                        <td>{{ $list->bakns->spph->mitras['perusahaan'] }}</td>
                                        <td>{{ $list->bakns->created_at }}</td>
                                        <td>{{ number_format($list->bakns['harga']) }}</td>
                                        <td>
                                            @php
                                            if($list->lampiran == NULL){
                                            }else{
                                            $title = json_decode($list->title_lampiran, TRUE);
                                            $file = json_decode($list->lampiran, TRUE);
                                            $i=1;
                                            foreach ($title as $key => $value) {
                                            echo $i++.'. <a
                                                href="'.Storage::url($file[$key]).'">'.$title[$key].'</a><br>';
                                            }
                                            }
                                            @endphp
                                        </td>
                                        <td>
                                            <a href="{{ url('kontrak-non-edit', $list->id)}}"
                                                class="fa fa-fw fa-edit"></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (right) -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<script src="{{ asset('js/web/monthAndYear.js') }}"></script>
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
    // document.getElementById("pbsTable_wrapper").style.overflow = "auto";
    $(document).ready(function () {
        $('#kontrak').DataTable({
            scrollY: "50vh",
scrollCollapse: true,
        });
    });

</script>
@endsection
