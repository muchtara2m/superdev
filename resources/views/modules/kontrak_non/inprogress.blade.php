@extends('layouts.master')

@section('title')
Inprogress | Super Slim
@endsection

@section('stylesheets')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
@endsection

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            INPROGRESS
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active"> Inprogress </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-info">
                    <div class="box-header with-border">
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button href="#" class="close" data-dismiss="alert" aria-label="close">&times;</button>
                            {{ $message }}
                        </div>
                        @endif
                        <h3 class="box-title"><i class="fa fa-ticket"></i> Inprogress</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- /.box-header -->
                    <div class="nav-tabs-custom">
                        <div style="text-align:center;padding-bottom:10px">
                            <div class="row input-daterange">
                                <form method="get" action="/kontrak-non-inprogress"
                                    enctype="multipart/form-data">
                                    <div class="col-md-4">
                                        <select name="bulan" id="bulan" class="form-control">
                                            @php
                                            if(request()->get('bulan') == null){
                                            $bln = date('m');
                                            $bulan = date('F',strtotime(date('Y-m-d')));
                                            $thn = date('Y');
                                            }else{
                                            $bln = request()->get('bulan');
                                            $bulan = date('F', mktime(0, 0, 0, request()->get('bulan'), 10));
                                            $thn = request()->get('tahun');
                                            }
                                            @endphp
                                            <option value="{{ $bln }}" selected>{{ $bulan }}</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select name="tahun" id="tahun" class="form-control">
                                            <option value="{{ $thn }}" selected>{{ $thn }}</option>
                                        </select>
                                    </div>
                                    <input type="submit" value="Filter" class="btn btn-primary">

                                    <div class="col-md-4">
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="box-body table-responsive">
                            <table name="item" id="item" class="display" width="100%" cellspacing="0">
                                <thead>
                                    <tr id="kepala">
                                        <th rowspan="2" style="text-align:center">No</th>
                                        <th rowspan="2" style="text-align:center">No. IO</th>
                                        <th rowspan="2" style="text-align:center">No. SPH</th>
                                        <th rowspan="2" style="text-align:center">No. SPPH</th>
                                        <th rowspan="2" style="text-align:center">No. Kontrak</th>
                                        <th rowspan="2" style="text-align:center">Judul</th>
                                        <th rowspan="2" style="text-align:center">Mitra</th>
                                        <th rowspan="2" style="text-align:center">Tanggal BAKN</th>
                                        <th rowspan="2" style="text-align:center">Tanggal Unggah</th>
                                        <th rowspan="2" style="text-align:center">Tanggal Disposisi</th>
                                        <th rowspan="2" style="text-align:center">Pembuat</th>
                                        <th rowspan="2" style="text-align:center">Tanggal Buat</th>
                                        @if(Auth::user()->level == 'administrator' || Auth::user()->level =='mgrlegal')
                                        <th rowspan="2" style="text-align:center">Harga</th>
                                        @endif
                                        <th colspan="6" style="text-align:center">Approval</th>
                                        <th rowspan="2" style="text-align:center">Posisi</th>
                                        <th rowspan="2" style="text-align:center">Tanggal Upload File</th>
                                        <th rowspan="2" style="text-align:center">Preview</th>
                                    </tr>
                                    <tr style=" white-space: nowrap">
                                        <th style="text-align:center">Admin Kontrak</th>
                                        <th style="text-align:center">AVP Legal Contract</th>
                                        <th style="text-align:center">Manager Procurement</th>
                                        <th style="text-align:center">VP General Support</th>
                                        <th style="text-align:center">DIRFBS</th>
                                        <th style="text-align:center">DIRUT</th>
                                    </tr>
                                </thead>
                                <tbody style="text-align:center">
                                    @php
                                    $no =1;
                                    $data = array();
                                    @endphp
                                    @foreach ($bakn as $item => $value )
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ (int)$value->bakns->io['no_io'] }}</td>
                                        @if($value->bakns->spph['nomorsph'] == NULL )
                                        <td> - </td>
                                        @else
                                        <td>{{ $value->bakns->spph['nomorsph'] }}</td>
                                        @endif
                                        <td>{{ $value->bakns->spph['nomorspph'] }}</td>
                                        <td>{{ $value->nokontraknon }}</td>
                                        <td>{{ $value->bakns->spph['judul'] }}</td>
                                        <td>{{ $value->bakns->spph->mitras['perusahaan'] }}</td>
                                        <td>{{ date('d.F.Y', strtotime($value->bakns['tglbakn'])) }}</td>
                                        <td>{{ date('d.F.Y', strtotime($value->bakns['updated_at'])) }}</td>
                                        <td>{{ date('d.F.Y', strtotime($value->bakns['tgldisp'])) }}</td>
                                        <td>{{ $value->users['name'] }}</td>
                                        <td>{{ date('d.F.Y', strtotime($value->created_at)) }}</td>
                                        {{-- declare show harga else admin --}}
                                        @if(Auth::user()->level == 'administrator' || Auth::user()->level =='mgrlegal')
                                        <td style="white-space:nowrap">Rp
                                            {{ number_format($value->bakns['harga'],0,'.','.') }}</td>
                                        @endif
                                        <td style="white-space:nowrap">
                                            @php
                                            for ($i=0; $i < count($value->chatnons); $i++) {
                                                if ($value->chatnons[$i]['queue']== 0) {
                                                echo '<br>('.date('d.F.Y H:i',
                                                strtotime($value->chatnons[$i]['created_at'])).' -
                                                '.$value->chatnons[$i]['status'].')<br>'.$value->chatnons[$i]['name'];
                                                }
                                                }
                                                @endphp
                                        </td>
                                        <td style="white-space:nowrap">
                                            @php
                                            for ($i=0; $i < count($value->chatnons); $i++) {
                                                if ( $value->chatnons[$i]['queue']== 1) {
                                                echo '<br>('.date('d.F.Y H:i',
                                                strtotime($value->chatnons[$i]['created_at'])).' -
                                                '.$value->chatnons[$i]['status'].')<br>'.$value->chatnons[$i]['name'];
                                                }
                                                }
                                                @endphp
                                        </td>
                                        <td style="white-space:nowrap">
                                            @php
                                            for ($i=0; $i < count($value->chatnons); $i++) {
                                                if ( $value->chatnons[$i]['queue']== 2) {
                                                echo '<br>('.date('d.F.Y H:i',
                                                strtotime($value->chatnons[$i]['created_at'])).' -
                                                '.$value->chatnons[$i]['status'].')<br>'.$value->chatnons[$i]['name'];
                                                }
                                                }
                                                @endphp
                                        </td>
                                        <td style="white-space:nowrap">
                                            @php
                                            $data[$value->idnya]['id']= $value->id; //idkontrak
                                            for ($i=0; $i < count($value->chatnons); $i++) {
                                                if ( $value->chatnons[$i]['queue']== 3) {
                                                echo '<br>('.date('d.F.Y H:i',
                                                strtotime($value->chatnons[$i]['created_at'])).' -
                                                '.$value->chatnons[$i]['status'].')<br>'.$value->chatnons[$i]['name'];
                                                }
                                                }
                                                @endphp
                                        </td>
                                        <td style="white-space:nowrap">
                                            @php
                                            for ($i=0; $i < count($value->chatnons); $i++) {
                                                if ( $value->chatnons[$i]['queue']== 4) {
                                                echo '<br>('.date('d.F.Y H:i',
                                                strtotime($value->chatnons[$i]['created_at'])).' -
                                                '.$value->chatnons[$i]['status'].')<br>'.$value->chatnons[$i]['name'];
                                                }
                                                }
                                                @endphp
                                        </td>
                                        <td style="white-space:nowrap">
                                            @php
                                            for ($i=0; $i < count($value->chatnons); $i++) {
                                                if ( $value->chatnons[$i]['queue']== 5) {
                                                echo '<br>('.date('d.F.Y H:i',
                                                strtotime($value->chatnons[$i]['created_at'])).' -
                                                '.$value->chatnons[$i]['status'].')<br>'.$value->chatnons[$i]['name'];
                                                }
                                                }
                                                @endphp
                                        </td>
                                        <td>{{ $value['approval'] }}</td>
                                        @if ($value['upload'] != null)
                                        <td>{{ date('d.F.Y', strtotime($value['upload'])) }}</td>
                                        @else
                                        <td></td>
                                        @endif
                                        <td style="white-space:nowrap">
                                            <a href="{{ url('spph-preview/'.$value->bakns->spph['id']) }}">Preview
                                                SPPH</a><br>
                                            <a href="{{ url('bakn-preview/'.$value->bakns['id']) }}">Preview
                                                BAKN</a><br>
                                            <a href="{{ url('kontrak-non-preview-status/'.$value->id) }}">Preview
                                                Kontrak</a><br>
                                        </td>

                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box -->
                    <div class="modal fade" id="modal">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Upload File</h4>
                                </div>
                                <div class="modal-body">
                                    <form method="post" action="" enctype="multipart/form-data">
                                        @csrf
                                        {{-- @method('PATCH') --}}
                                        <input type="hidden" name="title" class="form-control">
                                        <br>
                                        <input type="file" name="file[]" id="" class="form-control" multiple="multiple">
                                        <br>
                                        <button type="submit" class="btn btn-success" style="width: 7em;"><i
                                                class="fa fa-check"></i>
                                            Submit</button>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->


                </div>
                <!-- /.box -->
            </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection

@section('scripts')
<script src="{{ asset('js/web/monthAndYear.js') }}"></script>
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript">
   var otable = $('.display').DataTable({
        scrollY: "50vh",
        scrollCollapse: true,
    });
    $(document).on("click", ".upload", function () {
        var idbro = $(this).data('id');
        // $(".modal-body #idnya").val( idbro );
        $('form').attr('action', "{{ url('kontrak-non-upload')}}/" + idbro);
    });

    // Setup - add a text input to each footer cell
    // $('#item tfoot .testing').each(function () {
    //     var title = $(this).text();
    //     $(this).html('<input type="text" placeholder="Search ' + title + '" />');
    // });

    // DataTable

    // Apply the search
    // otable.columns().every(function () {

    //     var that = this;
    //     $('input', this.footer()).on('keyup change', function () {
    //         if (that.search() !== this.value) {
    //             that
    //                 .search(this.value)
    //                 .draw();
    //         }
    //     });
    // });

    

</script>
@endsection
