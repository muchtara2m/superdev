@extends('layouts.master')

@section('title')
Performansi | Super Slim
@endsection

@section('stylesheets')
<link rel="stylesheet"
    href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
{{-- Icon --}}
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">

@endsection
<style type="text/css">
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }

    tfoot {
        display: table-header-group;
    }

    .dt-buttons {
        padding-bottom: 5px;
    }

    #icon {
        text-align: center;
    }

    /* table thead th tbody td{
        :
    } */
    tbody tr {
        white-space: nowrap;
    }

    thead tr th small {
        color: red;
        font-size: 10px
    }

    h3 {
        padding-bottom: inherit;
    }
</style>
<!-- Chart code -->

@section('content')

@php
$homelink = "/home";
@endphp
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            PERFORMANSI
            <!-- <small>Form PBS</small> -->
        </h1>
        <br>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">General Support</a></li>
            <li class="active"> Performansi </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        @if (\Session::has('success'))
        <div class="alert alert-success">
            <p>{{ \Session::get('success') }}</p>
        </div>
        <br />
        @endif
        <div class="row">
            <div class="col-md-12" style="padding-bottom:2%">
                <div style="text-align:-webkit-center">
                    <select name="bulan" id="bulan" class="form-group" style="width:20%;height:34px" hidden>

                    </select>
                    <select name="tahun" id="tahun" class="form-group" style="width:30%;height:34px">

                    </select>
                    <br>

                </div>
            </div>
            {{-- Dashboard Gauge AR  --}}
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border" style="text-align:center">
                        <h3 class="box-title"><b>DASHBOARD PERFORMANSI</b></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                    class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <div class="box-body table-responsive" style="background:">
                        <div class="row" style="margin:1%">
                            <div class="box box-warning">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Jumlah SPK/PKS MITRA</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                                class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                                class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12 table-responsive">
                                            <table id="jumlah_spk_pks" class="display">
                                                <thead class="text-center">
                                                    <tr style="white-space:nowrap">
                                                        <th rowspan="2">No</th>
                                                        <th rowspan="2">Unit</th>
                                                        <th rowspan="2">PBS</th>
                                                        <th colspan="5" style="text-align:center">Jumlah SPK/PKS MITRA
                                                        </th>
                                                        <th rowspan="2" style="text-align:center">Nilai COGS</th>
                                                        <th rowspan="2" style="text-align:center">Nilai SPK/PKS</th>
                                                        <th colspan="2" style="text-align:center">Efesiensi</th>

                                                        <th rowspan="2">KHS</th>
                                                    </tr>
                                                    <tr style="white-space:nowrap">
                                                        <th>Plan SPK/PKS</th>
                                                        <th>Selesai</th>
                                                        <th>Inprogress</th>
                                                        <th>Batal</th>
                                                        <th>Pending</th>
                                                        {{-- efesiensi --}}
                                                        <th>(Rp)</th>
                                                        <th>(%)</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center" style="white-space:nowrap">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /jumlah spk mitra -->
                            <div class="box box-success" style="margin-top:5%;">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Pola Pembayaran</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                                class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                                class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12 table-responsive">
                                            <table id="pola_pembayaran" class="display">
                                                <thead>
                                                    <tr style="white-space:nowrap">
                                                        <th rowspan="2">No</th>
                                                        <th rowspan="2">Unit</th>
                                                        <th colspan="5" style="text-align:center">Pola Pembayaran</th>
                                                    </tr>
                                                    <tr style="white-space:nowrap;">
                                                        {{-- pola pembayaran --}}
                                                        <th style="text-align:center">CBD/COD</th>
                                                        <th style="text-align:center">OTC</th>
                                                        <th style="text-align:center">RECURRING</th>
                                                        <th style="text-align:center">PROGRESS</th>
                                                        <th style="text-align:center">Jumlah</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center" style="white-space:nowrap">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!-- /.Pola Pembayaran -->

                            <div class="box box-danger" style="margin-top:5%;">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Klarifikasi Nilai</h3>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                                class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                                class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12 table-responsive">
                                            <table id="klarifikasi_nilai" class="display">
                                                <thead>
                                                    <tr style="white-space:nowrap">
                                                        <th rowspan="2">No</th>
                                                        <th rowspan="2">Unit</th>
                                                        <th colspan="5" style="text-align:center">Klasifikasi Nilai</th>
                                                    </tr>
                                                    <tr style="white-space:nowrap">
                                                        {{-- klas nilai --}}
                                                        <th style="text-align:center">
                                                            < 100jt</th> <th style="text-align:center">> 100 s/d <
                                                                    500jt</th> <th style="text-align:center">>500 s/d <
                                                                        2M </th> <th style="text-align:center">>2M
                                                        </th>
                                                        <th style="text-align:center">Jumlah</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center" style="white-space:nowrap">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
                @include('modules.kontrak_non.modal.detail_pbs_modal')
                @include('modules.kontrak_non.modal.detail_plan_modal')
                @include('modules.kontrak_non.modal.detail_selesai_modal')
            </div>
        </div>
    </section>
</div>

</section>
@endsection
@section('scripts')
<!-- DataTables -->
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/ajax/jumlah_spk_pks.js') }}"></script>
{{-- <script src="{{ asset('js/ajax/klarifikasi_nilai.js') }}"></script> --}}
<script src="{{ asset('js/ajax/pola_pembayaran.js') }}"></script>
<script src="{{ asset('js/ajax/detail_pbs.js') }}"></script>
<script src="{{ asset('js/ajax/detail_plan.js') }}"></script>
<script src="{{ asset('js/ajax/detail_selesai.js') }}"></script>
<script>
    var url_data_performansi_spk_pks = "{{ url('data-performansi') }}";
    var url_pola_pembayaran = "{{ url('data-pola-pembayaran') }}";
    var url_detail_data_pbs = '{{ url("data-detail-pbs") }}';
    var url_detail_data_plan = '{{ url("data-detail-plan") }}';
    var url_detail_data_selesai = '{{ url("data-detail-selesai") }}';

    dataSpkPks();
    polaPembayaran();

    $('#tahun').change(function () {
        var bln = $('#bulan').val();
        var thn = $('#tahun').val();
        // $('#jumlah_spk_pks').DataTable().destroy();
        // $('#klarifikasi_nilai').DataTable().destroy();
        // $('#pola_pembayaran').DataTable().destroy();
        dataSpkPks(thn);
        polaPembayaran(thn);
    });

    $('#jumlah_spk_pks').on('click', '#data-selesai', function () {
        var thn = $('#tahun').val();
        var unit = $(this).data('ubis');
        var ubisnya = $(this).data('ubis');
        detailDataSelesai(thn,unit);
       
    });
    $('#jumlah_spk_pks').on('click', '#data-pbs', function () {
        var thn = $('#tahun').val();
        var unit = $(this).data('ubis');
        var ubisnya = $(this).data('ubis');
        detailDataPbs(thn,unit);
    });
    $('#jumlah_spk_pks').on('click', '#data-plan', function () {
        var thn = $('#tahun').val();
        var unit = $(this).data('ubis');
        var ubisnya = $(this).data('ubis');
        detailDataPlan(thn,unit);
    });

   
    // $.ajax({
    //     method: "GET",
    //     url:url_detail_data_pbs,
    //     data: {unit:unit, thn:thn},
    //     success:function(data){
    //         console.log(data);
    //     },
    //     error:function(data){
    //         console.log(data);
    //     },
    // });

</script>
<!-- Chart code -->
{{-- <script src="{{ asset('js/chartjs/ds-ar.js') }}"></script> --}}




@endsection