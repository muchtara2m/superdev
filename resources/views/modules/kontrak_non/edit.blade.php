@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Edit Kontrak";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<script src="https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js"></script>
@endsection
@section('customstyle')

@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Kontrak
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>
    <br>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        There was a problem, please check your form carefully.
        <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    @if($chat != NULL)
    <div class="form-group col-md-12" >
        <label for="exampleInputEmail1"> {{ $chat->name }}</label>
        <textarea name="" id="chat" cols="30" rows="10">
            {{ $chat->chat }}
        </textarea>
    </div>
    @endif
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Edit Kontrak</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>
                        
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="{{ action('KontrakNonController@update', $kontraks->id) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <div class="box-body">
                                <div class="col-md-12">
                                    <div class="form-group col-md-6 igroup">
                                        <label for="exampleInputEmail1">Nomor Kontrak</label>
                                        <input type="text" class="form-control" name="nokontrak"  id="nmrkontrak" value="{{ $kontraks->nokontraknon }}">
                                    </div>
                                    <div class="form-group col-md-6 igroup">
                                        <label for="tglbakn">Tanggal Kontrak</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" data-date="" data-date-format="yyyy-mm-dd" class="form-control datejos" name="tglkontrak" id="enddelivery-date" value="{{ $kontraks->tglkontrak }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12" >
                                    <div class="form-group">
                                        <textarea name="isi"  cols="180" rows="10">
                                            {{ $kontraks->isi }}
                                        </textarea>
                                    </div>
                                    @if ($kontraks->lampiran == NULL)
                                    <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="">Lampiran</label>
                                                <br>*if you want submit this field is required
                                                <input type="file" name="lampiran[]" id="" class="form-control" multiple="multiple">
                                                
                                            </div>
                                        </div>
                                    @endif
                                    
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Comment</label>
                                            <br>*if you want submit this field is required
                                            <input type="text" name="chat" class="form-control" value="{{ old('chat') }}">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success" name="status"  value="draft_kontrak">Save</button>
                                <button type="submit" class="btn btn-primary" name="status"  value="save_kontrak">Submit</button>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        
                    </form>
                </div>
                <!-- /.box -->
            </div>
            
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<script src="{{ asset('froala/js/languages/id.js') }}"></script>
<script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- bootstrap datepicker -->
<script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>

<!-- Initialize the editor. -->
<script>
     $('#chat').froalaEditor({
            key: '{{ env("KEY_FROALA") }}',
            height: 70,
            toolbarButtons:['help'],
            charCounterCount: false,
            key: '{{ env("KEY_FROALA") }}',

            // language: 'id',

        })
    function alertData() {
        confirm("Perhatikan Semua Form, jika tidak sesuai maka data yang tertulis otomatis di reset !");
    }
    $(function () {
        $(".datejos").on("change", function () {
            this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
            .format(this.getAttribute("data-date-format"))
            )
        }).trigger("change")
    });
    $('.datejos').datepicker({
        autoclose: true,
        orientation: "bottom"
    });
    $('#nmrkontrak').change(function() {
        var nomor = $('#nmrkontrak').val();
        document.getElementById('nmrnya').innerHTML = "Nomor : "+ nomor;
        
        console.log(nomor);
        // $('#nmrnya').froalaEditor('html.set', nomor);
        
    });
    $('textarea').froalaEditor({
        documentReady: true,
        /* toolbarButtons: ['getPDF'] */
        toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo', 'getPDF'],
        language: 'id',
        key:'7D4A4F3I3cA5A4B3F2E4B2D2E3D1A3vxyA-9kB-8cH-7B-22C-16D2eC-9ykI2ytB4tz==',
    })
    
    $('#submit').click(function(e){
        var helpHtml = $('div#froala-editor').froalaEditor('html.get'); // Froala Editor Inhalt auslesen
        $.post( "{{ action('KontrakController@store') }}", { helpHtml:helpHtml });
    }); </script>
    
    <!-- Data Table -->
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <!-- date-range-picker -->
    <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <!-- clockpicker -->
    <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- CK Editor -->
    {{-- <script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script> --}}
    
    @endsection
    