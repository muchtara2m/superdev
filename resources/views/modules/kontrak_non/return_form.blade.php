@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Kontrak";
@endphp

@section('title')
{{ $crpagename." | SuperSlim" }}
@endsection

@section('stylesheets')
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="{{ asset('adminlte/plugins/iCheck/all.css') }}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
<!-- Clockpicker -->
<link rel="stylesheet" type="text/css" href="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="{{ asset('adminlte/bower_components/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<!-- Include Editor style. -->
<link href="{{ asset('froala/css/froala_editor.pkgd.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('froala/css/froala_style.min.css') }}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<script src="https://raw.githack.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.js"></script>
@endsection
@section('customstyle')

<style type="text/css">
    table tr:not(:first-child){
        cursor: pointer;transition: all .25s ease-in-out;
    }
    table tr:not(:first-child):hover{background-color: #ddd;}
    .form-horizontal .form-group {
        margin-right: unset;
        margin-left: unset;
    }
    .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
    }
    .example-modal .modal {
        background: transparent !important;
    }
    .no-bullet {
        padding-left: 0;
        list-style-type: none;
    }
    .pulse {
        width: 20%;
        --color: #ef6eae;
        --hover: #ef8f6e;
    }
    .pulse:hover,
    .pulse:focus {
        -webkit-animation: pulse 1s;
        animation: pulse 1s;
        box-shadow: 0 0 0 2em rgba(255, 255, 255, 0);
    }

    @-webkit-keyframes pulse {
        0% {
            box-shadow: 0 0 0 0 var(--hover);
        }
    }

    @keyframes pulse {
        0% {
            box-shadow: 0 0 0 0 var(--hover);
        }
    }
    .close:hover,
    .close:focus {
        box-shadow: inset -3.5em 0 0 0 var(--hover), inset 3.5em 0 0 0 var(--hover);
    }
</style>
@endsection

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Kontrak
            <!-- <small>Form PBS</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ $homelink }}"><i class="fa fa-th-large"></i> Home</a></li>
            <li><a href="#">Kontrak</a></li>
            <li class="active">{{ $crpagename }} </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- right column -->
            <div class="col-md-12">
                <!-- Horizontal Form -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-file-text"></i> Form Edit Kontrak</h3>
                        <button onclick="history.go(-1);" class="btn btn-default btn-round pull-right"><i class="fa fa-arrow-left"></i></button>

                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" action="{{ action('KontrakNonController@return_approve', $kontrak->id) }}" enctype="multipart/form-data">
                        @csrf
                        <div class="box-body">
                            <input type="hidden" name="nokontrak" value="{{ $kontrak->nokontraknon }}">
                            <div class="form-group col-md-12" >
                                <div class="form-group" >
                                    <label for="exampleInputEmail1">Comment</label>
                                    {{ $kontrak->id }}
                                    <textarea name="" id="chat" cols="30" rows="10">
                                        {{ $chat->chat }}
                                    </textarea>
                                </div>

                                <div class="form-group">
                                    <textarea name="isi" id="isi" cols="180" rows="10">
                                        {{ $kontrak->isi }}
                                    </textarea>
                                </div>
                                <input type="text" name="created_by" value="{{ Auth::id() }}" hidden>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Comment</label>
                                        <input type="text" name="chat" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-success" name="status" value="1">Save</button>
                            <button type="submit" class="btn btn-primary" name="status" id="submit" value="2">Submit</button>
                        </div>
                        <!-- /.box-body -->

                    </form>
                </div>
                <!-- /.box -->
            </div>

        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('scripts')
<!-- Include Editor JS files. -->
<script type="text/javascript" src="{{ asset('froala/js/froala_editor.pkgd.min.js') }}"></script>
<script src="{{ asset('froala/js/languages/id.js') }}"></script>

<!-- Initialize the editor. -->
<script>
    $('#isi').froalaEditor({
        // toolbarButtons: ['container'],
        // toolbarButtons: ['getPDF', 'print'],
        toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'inlineClass', 'clearFormatting', '|', 'emoticons', 'fontAwesome', 'specialCharacters', 'paragraphFormat', 'lineHeight', 'paragraphStyle', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '|', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', 'insertHR', 'selectAll', 'help', 'html', 'fullscreen', '|', 'undo', 'redo', 'getPDF', 'print'],
        placeholderText: '',
        documentReady: true,
        language: 'id',
        key: '{{ env("KEY_FROALA") }}',
    })
    $('#chat').froalaEditor({
        key: '{{ env("KEY_FROALA") }}',
        height: 70,
        toolbarButtons:['help'],
        charCounterCount: false,
        // language: 'id',

    })

    $('#submit').click(function(e){
        var helpHtml = $('div#froala-editor').froalaEditor('html.get'); // Froala Editor Inhalt auslesen
        $.post( "{{ action('KontrakController@store') }}", { helpHtml:helpHtml });
    }); </script>

    <!-- Data Table -->
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <!-- date-range-picker -->
    <script src="{{ asset('adminlte/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset('adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- iCheck 1.0.1 -->
    <script src="{{ asset('adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <!-- clockpicker -->
    <script type="text/javascript" src="{{ asset('clockpicker/dist/bootstrap-clockpicker.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('adminlte/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- CK Editor -->
    {{-- <script src="{{ asset('adminlte/bower_components/ckeditor/ckeditor.js') }}"></script> --}}

    @endsection
