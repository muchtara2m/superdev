 <!-- modal -->
 <div class="modal fade" id="modal-detail-pbs">
     <div class="modal-dialog" style="width: fit-content">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                 <h4 class="modal-title">List Detail PBS </h4><span id="ubis" >UBISNYA</span>
             </div>
             <div class="modal-body">
                 <table id="detailDataPbs" class="display table-responsive">
                     <thead>
                         <tr>
                             <th rowspan="2">No</th>
                             <th rowspan="2">IO</th>
                             <th rowspan="2">Nama Vendor</th>
                             <th rowspan="2">Nama Project</th>
                             <th rowspan="2">Nomor Kontrak</th>
                             <th rowspan="2">Tanggal</th>
                             <th colspan="2">Jangka Waktu</th>
                             <th rowspan="2">Nilai</th>
                             <th rowspan="2">Status KL</th>
                         </tr>
                         <tr>
                             <th>Mulai</th>
                             <th>Berakhir</th>
                         </tr>
                     </thead>
                     <tbody>

                     </tbody>
                 </table>
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
             </div>
         </div>
         <!-- /.modal-content -->
     </div>
     <!-- /.modal-dialog -->
 </div>
 <!-- /.modal -->