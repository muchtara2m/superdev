 <!-- modal -->
 <div class="modal fade" id="modal-detail-plan">
     <div class="modal-dialog" style="width: fit-content">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span aria-hidden="true">&times;</span></button>
                 <h4 class="modal-title">List Detail Plan</h4><span id="ubis" >UBISNYA</span>
             </div>
             <div class="modal-body">
                 <div class="box box-danger">
                     <div class="box-header with-border">
                         <h3 class="box-title">Kontrak Layanan</h3>

                         <div class="box-tools pull-right">
                             <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                     class="fa fa-minus"></i>
                             </button>
                             <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                     class="fa fa-times"></i></button>
                         </div>
                     </div>
                     <!-- /.box-header -->
                     <div class="box-body table-responsive">
                         <div class="row">
                             <div class="col-md-12 table-responsive">
                                 <table id="detailDataPlan1" class="display" style="width:100%">
                                     <thead>
                                         <tr>
                                             <th rowspan="2">No</th>
                                             <th rowspan="2">IO</th>
                                             <th rowspan="2">Nama Ubis</th>
                                             <th rowspan="2">Customer</th>
                                             <th rowspan="2">Nama Proyek</th>
                                             <th rowspan="2">Nomor Kontrak</th>
                                             <th rowspan="2">Tanggal</th>
                                             <th colspan="2" style="text-align:center">Jangka Waktu</th>
                                             <th rowspan="2">Nilai</th>
                                             <th rowspan="2">Status KL</th>
                                         </tr>
                                         <tr>
                                             <th>Mulai</th>
                                             <th>Berakhir</th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                         <!-- /.row -->
                     </div>
                     <!-- /.box-body -->
                 </div>
                 <div class="box box-primary">
                     <div class="box-header with-border">
                         <h3 class="box-title">Status Proses</h3>

                         <div class="box-tools pull-right">
                             <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                     class="fa fa-minus"></i>
                             </button>
                             <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                     class="fa fa-times"></i></button>
                         </div>
                     </div>
                     <!-- /.box-header -->
                     <div class="box-body">
                         <div class="row">
                             <div class="col-md-12 table-responsive">
                                 <table id="detailDataPlan2" class="display" style="width:100%">
                                     <thead>
                                         <tr>
                                             <th>No</th>
                                             <th>IO</th>
                                             <th>Nama Ubis</th>
                                             <th>COGS PBS ALL</th>
                                             <th>COGS PER MITRA</th>
                                             <th>PIC</th>
                                             <th>Tanggal Terima Berkas</th>
                                             <th>Nama Supplier</th>
                                             <th>Judul SPK </th>
                                             <th>Nilai SPK</th>
                                             <th>Status</th>
                                             <th>Keterangan</th>
                                         </tr>
                                        
                                     </thead>
                                     <tbody>
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                         <!-- /.row -->
                     </div>
                     <!-- /.box-body -->
                 </div>
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
             </div>
         </div>
         <!-- /.modal-content -->
     </div>
     <!-- /.modal-dialog -->
 </div>
 <!-- /.modal -->