<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Vendor extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    //
    protected $connection = 'mysql2';
    protected $table = 't_vendor';
    protected $primaryKey = 'akun_vendor';
    protected $guarded = [];

    public function vendorSpkPks(){
        return $this->hasMany('App\Models\SpkPksVendor','akun_vendor');
    }
}
