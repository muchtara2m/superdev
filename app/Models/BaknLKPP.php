<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

use App\Models\SpphLKPP;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Support\Facades\Storage;


class BaknLKPP extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;

    protected $guarded = [];

    protected static $logAttributes = [
        'spph_id','id','tglbakn','tipe_rapat','pimpinan_rapat',
        'peserta_mitra','peserta_pins','harga','io_id','agenda',
        'dasar_pembahasan','ruang_lingkup','lokasi_pekerjaan',
        'jangka_waktu','harga_terbilang','cara_bayar','lain_lain',
        'file','lampiran','approval'];
        
    protected $table = 'bakn_lkpps';

    // Relation Eloquent 
    public function spph_lkpps()
    {
        return $this->belongsTo('App\Models\SpphLKPP','spph_id');
    }
    public function sp3_lkpp()
    {
        return $this->belongsTo('App\Models\Sp3','id');
    }
    public function kontrak_lkpp()
    {
        return $this->hasOne('App\Models\Kontrak','bakn_id');
    }
    public function io_lkpp()
    {
        return $this->belongsTo('App\Models\Iodesc','io_id');
    }
    public function user_bakn_lkpp(){
        return $this->belongsTo('App\User','created_by');
    }
    public function chat_bakn_lkpp()
    {
        return $this->hasMany('App\Models\ChatBaknLkpp','id_bakn_lkpp');
    }
    public function revisi_bakn_lkpp()
    {
        return $this->hasMany(RevisiBaknLkpp::class,'bakn_id');
    }

    // Function 
    public function scopeDataIo()
    {
        $data   = Iodesc::all();
        return $data;
    }

    public function scopeDataSpphDoesntHaveBakn($id)
    {
        $data   = SpphLKPP::whereDoesntHave('bakn_lkpps', function($query) use ($id)
                    {
                        if($id != ""){
                            $query->where('id','!=', $id);
                        }
                    })
        ->orderBy('updated_at','desc');
        return $data;
    }

    public function getDetailSpph($nomorSpph)
    {
        $data   = SpphLKPP::with('mitra_lkpps','bakn_lkpps')->where('nomorspph',$nomorSpph)->first();
        return $data;
    }

    public function scopeAllData($month, $year)
    {
        if(!$month || !$year){
            $month = date('m');
            $year = date('Y');
        }

        $data = BaknLKPP::with(['spph_lkpps','user_bakn_lkpp','io_lkpp','spph_lkpps.mitra_lkpps','kontrak_lkpp'])
                ->orderBy('updated_at','des');
        // condition select all
        if($month != 13 ){
            $data->whereMonth('created_at',$month)->whereYear('created_at',$year);
        }
                return $data;
    }

    public function scopeDraftData($month, $year)
    {
        if(!$month || !$year){
            $month = date('m');
            $year = date('Y');
        }

        $query = BaknLKPP::with(['spph_lkpps','user_bakn_lkpp','io_lkpp','spph_lkpps.mitra_lkpps','kontrak_lkpp'])
                ->where('status','draft_bakn')
                ->orderBy('updated_at','des');
        
                // condition select all
        if($month != 13 ){
            $query->whereMonth('created_at',$month)->whereYear('created_at',$year);
        }

        if(Auth::user()->level == 'administrator'){
            return $query->get();
        }else{
            return $query->where('created_by',Auth::user()->id)->get();      
        }
    }

    public function scopeListData($month, $year)
    {
        if(!$month || !$year){
            $month = date('m');
            $year = date('Y');
        }
        
        $query = BaknLKPP::with(['spph_lkpps','user_bakn_lkpp','io_lkpp','spph_lkpps.mitra_lkpps','kontrak_lkpp'])
                ->where('approval','CLOSED')
                ->where('status','save_bakn')
                ->orderBy('updated_at','des');

                // condition select all
        if($month != 13 ){
            $query->whereMonth('created_at',$month)->whereYear('created_at',$year);
        }
        if(Auth::user()->level == 'administrator'){
            return $query;
        }else{
            return $query->where('created_by',Auth::user()->id);          
        }
    }

    public function scopeDoneData($month, $year)
    {
        if(!$month || !$year){
            $month = date('m');
            $year = date('Y');
        }

        $query = BaknLKPP::with(['spph_lkpps','user_bakn_lkpp','io_lkpp','spph_lkpps.mitra_lkpps'])
                ->where('status','done_bakn')
                ->orderBy('updated_at','des');

        // condition select all
        if($month != 13 ){
            $query->whereMonth('created_at',$month)->whereYear('created_at',$year);
        }

        if(Auth::user()->level == 'administrator'){
            return $query;
        }else{
            return $query->where('created_by',Auth::user()->id);
        }
    }

    public function scopeInprogress($month, $year)
    {
        if(!$month || !$year){
            $month = date('m');
            $year = date('Y');
        }
        
        $query = BaknLKPP::with(['spph_lkpps','user_bakn_lkpp','spph_lkpps.mitra_lkpps','io_lkpp','chat_bakn_lkpp'=> function($q){
            $q->select('chat_bakn_lkpp.*','users.name')
            ->join('users','users.username','chat_bakn_lkpp.username')
            ->orderBy('created_at','asc');
        }])
            ->where('approval','!=','CLOSED')
            ->orderBy('updated_at','des');

        $list = DataFlow::where('transaksi','Kontrak')
            ->where('unit','E-Commerce')
            ->get()
            ->pluck('username');
        
            // condition select all
        if($month != 13 ){
            $query->whereMonth('created_at',$month)->whereYear('created_at',$year);
        }
        
        if(Auth::user()->level == 'administrator' || in_array(Auth::user()->username, json_decode($list)) ){
            return $query;
        }else{
            return $query->where('created_by',Auth::user()->id);
        }   

    }

    public function scopeTrackingDocument($month, $year)
    {
        if(!$month || !$year){
            $month = date('m');
            $year = date('Y');
        }
        
        $query = BaknLKPP::with(['spph_lkpps','spph_lkpps.mitra_lkpps','spph_lkpps.creator_spph_lkpp','user_bakn_lkpp','io_lkpp','chat_bakn_lkpp' => function($q){
            $q->select('chat_bakn_lkpp.*','users.name')
            ->join('users','users.username','chat_bakn_lkpp.username')
            ->orderBy('created_at','asc');
        }])
            ->orderBy('updated_at','des');

        $list = DataFlow::where('transaksi','Kontrak')
                ->where('unit','E-Commerce')
                ->get()
                ->pluck('username');

        // condition select all
        if($month != 13 ){
            $query->whereMonth('created_at',$month)->whereYear('created_at',$year);
        }
        
        if(Auth::user()->level == 'administrator' || in_array(Auth::user()->username, json_decode($list)) ){
            return $query;
        }else{
            return $query->where('created_by',Auth::user()->id);
        }
    }

    public function scopeStatusTransaksi($month, $year)
    {
        if(!$month || !$year){
            $month = date('m');
            $year = date('Y');
        }
        
        $query = BaknLKPP::with(['spph_lkpps','user_bakn_lkpp','io_lkpp','spph_lkpps.mitra_lkpps','kontrak_lkpp'])
                ->where('approval','<>','CLOSED')
                ->where('hold',false)
                ->orderBy('created_at','desc'); 
             
        // condition select all
        if($month != 13 ){
            $query->whereMonth('created_at',$month)->whereYear('created_at',$year);
        }

        if(Auth::user()->level == 'administrator'){
            return $query;
        }else{
            return $query->where(function($q){
                    $q->where('approval',Auth::user()->username)
                    ->where('created_by','!=',Auth::user()->id);
                })->orWhere(function($w) use ($month, $year){
                    $w->where('approval','Return')
                ->where('created_by',Auth::user()->id);
                if($month != 13){
                    $w->whereMonth('created_at',$month)
                     ->whereYear('created_at',$year);
                 }
                });
        }   
    }

    public function insertBaknLkpp($data)
    {
        // declare Model
        $bakn = new BaknLKPP();
        // Insert Data 
        $bakn->spph_id          = $data['spph_id'];
        $bakn->tglbakn          = $data['tglbakn'];
        $bakn->harga            = str_replace(".","",$data['harga']);
        $bakn->io_id            = $data['io_id'];
        $bakn->isi              = $data['isi'];
        $bakn->cara_bayar       = implode(",",$data['caraBayar']);
        $bakn->created_by       = Auth::user()->id;
        $bakn->status           = $data['status'];
        $bakn->save();

        // Get id after insert
        $pathLampiran = "public/files/BAKN_LKPP/$bakn->id/lampiran";
        // Upload Lampiran
        if($data['lampiran'] != null){
            foreach($data['lampiran'] as $file){
                $name       = $file->getClientOriginalName();
                $file->storeAs($pathLampiran, $name);
                $namanya[]  = $name;
                $lampiran[] = $pathLampiran.'/'.$name;
            }
        }
        $bakn->lampiran        = $data['lampiran'] != null ? json_encode($lampiran) : NULL;
        $bakn->title_lampiran  = $data['lampiran'] != null ? json_encode($namanya) : NULL;
        $bakn->tgl_lampiran    = $data['lampiran'] != null ? date('Y-m-d') : NULL;
        $bakn->save();

        return $bakn;
    }

    public function updateBaknLkpp($data, $baknLKPP)
    {        
        if($baknLKPP->hold == true){
            $data['status'] = "save_bakn";
        }
        // Insert Data 
        $baknLKPP->spph_id          = $data['spph_id'];
        $baknLKPP->tglbakn          = $data['tglbakn'];
        $baknLKPP->harga            = str_replace(".","",$data['harga']);
        $baknLKPP->io_id            = $data['io_id'];
        $baknLKPP->isi              = $data['isi'];
        $baknLKPP->cara_bayar       = implode(",",$data['caraBayar']);
        $baknLKPP->status           = $data['status'];

        // Get id after insert
        $pathLampiran = "public/files/BAKN_LKPP/$baknLKPP->id/lampiran";
        // Upload Lampiran
        if($data['lampiran'] != null){
            foreach($data['lampiran'] as $file){
                $name          = $file->getClientOriginalName();
                $file->storeAs($pathLampiran, $name);
                $namanya[]     = $name;
                $lampiran[]    = $pathLampiran.'/'.$name;
            }
        }
        $baknLKPP->lampiran        = $data['lampiran'] != null ? json_encode($lampiran) : $baknLKPP->lampiran;
        $baknLKPP->title_lampiran  = $data['lampiran'] != null ? json_encode($namanya) : $baknLKPP->title_lampiran;
        $baknLKPP->tgl_lampiran    = $data['lampiran'] != null ? date('Y-m-d H:i:s') : $baknLKPP->tgl_lampiran;
        $baknLKPP->save();

        return $baknLKPP;
    }

    public function updateRevisi($data, $baknLKPP)
    {
       // Insert Data 
       $baknLKPP->spph_id          = $data['spph_id'];
       $baknLKPP->tglbakn          = $data['tglbakn'];
       $baknLKPP->harga            = str_replace(".","",$data['harga']);
       $baknLKPP->io_id            = $data['io_id'];
       $baknLKPP->isi              = $data['isi'];
       $baknLKPP->cara_bayar       = implode(",",$data['caraBayar']);
       $baknLKPP->status           = $data['status'];
       $baknLKPP->revisi           = $baknLKPP->revisi+1;

       // Get id after insert
       $pathLampiran = "public/files/BAKN_LKPP/$baknLKPP->id/lampiran";
       
       // Upload Lampiran
       if($data['lampiran'] != null){
           foreach($data['lampiran'] as $file){
               $name          = $file->getClientOriginalName();
               $file->storeAs($pathLampiran, $name);
               $namanya[]     = $name;
               $lampiran[]    = $pathLampiran.'/'.$name;
           }
       }
       $baknLKPP->lampiran        = $data['lampiran'] != null ? json_encode($lampiran) : $baknLKPP->lampiran;
       $baknLKPP->title_lampiran  = $data['lampiran'] != null ? json_encode($namanya) : $baknLKPP->title_lampiran;
       $baknLKPP->tgl_lampiran    = $data['lampiran'] != null ? date('Y-m-d H:i:s') : $baknLKPP->tgl_lampiran;
       //  dd($bakn);
        $baknLKPP->save();
        return $baknLKPP;
    }


    public function getValueApproval($bakn)
    {
        $notif = new NotificationWA();

        // get value for end approval
        $lastApproval       = DB::table('data_flows')
                            ->select('jabatan','username','queue')
                            ->where([
                                ['transaksi', 'BAKN'],
                                ['unit', 'e-Commerce']
                            ])
                            ->latest('queue')
                            ->first();
        // get first value approval
        $firstApproval      = DB::table('data_flows')
                            ->select('jabatan','username','queue')
                            ->where([
                                ['transaksi','BAKN'],
                                ['unit','e-Commerce']
                            ])
                            ->first();
         // get all data approval in json
         $jsonApproval      = DataFlow::where([
                                ['transaksi','BAKN'],
                                ['unit','e-Commerce']
                            ])
                            ->get()
                            ->pluck('username');
        // update data approval 
        $bakn->new_approval  = $jsonApproval;
        $bakn->approval      = $firstApproval->username;
        $bakn->end_approval  = $lastApproval->username;
        $bakn->save();
        
        return $notif->notificationBaknLkpp($bakn);
    }

    public function uploadFile($data, $model)
    {
        $pathLampiran = "public/files/BAKN_LKPP/$model->id/files";
        // Upload Lampiran
        if($data['file'] != null){
            foreach($data['file'] as $file){
                $name       = $file->getClientOriginalName();
                $file->storeAs($pathLampiran, $name);
                $namanya[]  = $name;
                $lampiran[] = $pathLampiran.'/'.$name;
            }
        }
        $model->file         = $data['file']        != null ? json_encode($lampiran) : $model->file;
        $model->title        = $data['file']        != null ? json_encode($namanya) : $model->title;
        $model->upload       = $data['file']        != null ? date('Y-m-d H:i:s') : $model->upload;
        $model->status       = 'done_bakn';
        $model->save();
        return $model;
    }

    public function uploadLampiran($data, $model)
    {
        $pathLampiran = "public/files/BAKN_LKPP/$model->id/lampiran";
        // Upload Lampiran
        if($data['lampiran'] != null){
            foreach($data['lampiran'] as $file){
                $name       = $file->getClientOriginalName();
                $file->storeAs($pathLampiran, $name);
                $namanya[]  = $name;
                $lampiran[] = $pathLampiran.'/'.$name;
            }
        }
        $model->lampiran        = $data['lampiran'] != null ? json_encode($lampiran) : $model->lampiran;
        $model->title_lampiran  = $data['lampiran'] != null ? json_encode($namanya) : $model->title_lampiran;
        $model->tgl_lampiran    = $data['lampiran'] != null ? date('Y-m-d H:i:s') : $model->tgl_lampiran;
        $model->save();

        return $model;
    }

    public function nextApprovalNew($model)
    {
        $notif          = new NotificationWA();

        // convert data to array
        $arrApproval    = json_decode($model->new_approval);
        // get key from array approval
        $getKey         = array_search(Auth::user()->username, $arrApproval);

        // loop and unset index has approve
        for($i = 0; $i< $getKey; $i++){
            unset($arrApproval[$i]);
        }
        // get data next after unset the data, and insert to variable 
        $next               = next($arrApproval);
        $model->approval    = $next;
        $model->save();
        return $notif->notificationBaknLkpp($model);
    }

    public function nextApprovalOld($model)
    {
        $notif              = new NotificationWA();
        $approval           = new DataFlow();

        $jsonApproval       = $approval
                                ->where([
                                    ['min','<=',$model->harga],
                                    ['transaksi', 'BAKN'],
                                    ['unit', 'e-Commerce']
                                    ])
                                ->get()
                                ->pluck('username');
        $isSubmitButton     = $approval
                                ->where('username',Auth::user()->username)
                                ->first();
        $isNextApprove      = $approval
                                ->where('unit','e-Commerce')
                                ->where('transaksi','BAKN')
                                ->where('queue', ($isSubmitButton->queue+1))
                                ->first();

        $model->approval = $isNextApprove->username;

        if($model->new_approval == null){
            $model->new_approval = $jsonApproval;
        }
        $model->save();

        return $notif->notificationBaknLkpp($model);
    }

    public function approvalReturn($model)
    {
        $notif = new NotificationWA();
        $model->approval = "Return";
        $model->save();
        return $notif->notificationBaknLkpp($model);
    }

    public function approvalClosed($model)
    {
        $notif = new NotificationWA();
        $model->approval = "CLOSED";
        $model->save();
        // dd("closed");
        return $notif->notificationBaknLkpp($model);
    }
   
}
