<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Sp3Non extends Model implements Auditable
{
    //
    use LogsActivity, \OwenIt\Auditing\Auditable;
    protected $guarded = [];

}
