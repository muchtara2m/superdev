<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class JenisPasal extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $guarded = [];
    protected $table = 'jenis_pasals';

    public function getDataAll()
    {
        $data = JenisPasal::orderBy('id','asc');
        return $data;
    }

    public function detailJenisPasal($id)
    {
        $data = JenisPasal::find($id);
        return $data;
    }
}
