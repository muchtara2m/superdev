<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;
use Symfony\Component\VarDumper\Cloner\Data;

class BakLkpp extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    protected $guarded = [];
    
    
    public function spph_bak_lkpp()
    {
        return $this->belongsTo('App\Models\SpphLKPP','spph_id');
    }
    
    public function user_bak_lkpp(){
        return $this->belongsTo('App\User','created_by');
    }

    public function chat_bak_lkpp(){
        return $this->hasMany('App\Models\ChatBakLkpp','id_bak_lkpp');
    }

    public function revisi_bak_lkpp()
    {
        return $this->hasMany(RevisiBakLkpp::class,'bak_id');
    }

    // All data for administrator only
    public function getAllData($month, $year)
    {
        if(!$month || !$year){
            $month = date('m');
            $year = date('Y');
        }
        $data = BakLkpp::with('spph_bak_lkpp','user_bak_lkpp','spph_bak_lkpp.mitra_lkpps');
        
        if($month != 13){
            $data->whereMonth('created_at',$month)
                ->whereYear('created_at',$year);
        }
        return $data;
    }

    // Draft data
    public function getDraftData($month, $year)
    {
        if(!$month || !$year){
            $month = date('m');
            $year = date('Y');
        }
        $data = BakLkpp::with('spph_bak_lkpp','user_bak_lkpp','spph_bak_lkpp.mitra_lkpps')
                ->where('status','draft_bak')
                ->orderBy('created_at','desc');

        if($month != 13){
            $data->whereMonth('created_at',$month)
                ->whereYear('created_at',$year);
        }

        if(Auth::user()->level == 'administrator'){
                return $data;
        }else{
            return $data->where('created_by',Auth::user()->id);
        }
    }

    // List Upload File Data
    public function getListData($month, $year)
    {
        if(!$month || !$year){
            $month = date('m');
            $year = date('Y');
        }
        $data = BakLkpp::with('spph_bak_lkpp','user_bak_lkpp','spph_bak_lkpp.mitra_lkpps')
                ->where('approval','CLOSED')
                ->where('status','save_bak')
                ->orderBy('created_at','desc');

        if($month != 13){
            $data->whereMonth('created_at',$month)
                ->whereYear('created_at',$year);
        }

        if(Auth::user()->level == 'administrator'){
                return $data;
        }else{
            return $data->where('created_by',Auth::user()->id);
        }
    }

    // List Data Done for all 
    public function getDoneData($month, $year)
    {
        if(!$month || !$year){
            $month = date('m');
            $year = date('Y');
        }
        $data = BakLkpp::with('spph_bak_lkpp','user_bak_lkpp','spph_bak_lkpp.mitra_lkpps')
                ->where('status','done_bak')
                ->orderBy('created_at','desc');
        
        if($month != 13){
            $data->whereMonth('created_at',$month)
                ->whereYear('created_at',$year);
        }

        if(Auth::user()->level == 'administrator'){
                return $data;
        }else{
            return $data->where('created_by',Auth::user()->id);
        }
    }

    // List Data Inprogress is Unfinished file approved
    public function getInprogressData($month, $year)
    {
        $listApproveBak = DataFlow::where('transaksi','BAK')->get()->pluck('username');

        if(!$month || !$year)
        {
            $month = date('m');
            $year = date('Y');
        }
        $data = BakLkpp::with([
                        'spph_bak_lkpp',
                        'user_bak_lkpp',
                        'spph_bak_lkpp.mitra_lkpps', 
                        'chat_bak_lkpp' => function($q){
                            $q->select('chat_bak_lkpps.*','users.name')
                            ->join('users','users.username','chat_bak_lkpps.username')
                            ->orderBy('created_at','asc');
                        }
                ])
                ->where('status','save_bak')
                ->where('approval','!=','CLOSED')
                ->orderBy('updated_at','desc');

        if($month != 13){
            $data->whereMonth('created_at',$month)
                ->whereYear('created_at',$year);
        }

        if(Auth::user()->level == 'administrator' || in_array(Auth::user()->username,json_decode($listApproveBak))){
                return $data;
        }else{
            return $data->where('created_by',Auth::user()->id);
        }
    }

    // List Data Status Transaksi for the file you want to approve and the file you return
    public function getStatusTransaksiData($month, $year)
    {
        if(!$month || !$year)
        {
            $month  = date('m');
            $year   = date('Y');
        }

        $data   = BakLkpp::with(
                    'spph_bak_lkpp',
                    'user_bak_lkpp',
                    'spph_bak_lkpp.mitra_lkpps'
                    )
                    ->where('approval','!=','CLOSED')
                    ->where('hold',false)
                    ->orderBy('created_at','desc');

        if($month != 13){
            $data->whereMonth('created_at',$month)
                ->whereYear('created_at',$year);
        }
                   
        if(Auth::user()->level == 'administrator'){
            return $data;
        }else{
            return $data->where(function($q){
                    $q->where('approval',Auth::user()->username)
                    ->where('created_by','!=',Auth::user()->id);
                })->orWhere(function($w) use ($month, $year){
                    $w->where('approval','Return')
                    ->where('created_by',Auth::user()->id);
                    if($month != 13){
                        $w->whereMonth('created_at',$month)
                         ->whereYear('created_at',$year);
                     }
                });
        }   
    }

    public function getTrackingDocumentData($month, $year)
    {
        $listApproveBak = DataFlow::where('transaksi','BAK')->get()->pluck('username');

        if(!$month || !$year)
        {
            $month  = date('m');
            $year   = date('Y');
        }
        $data = BakLkpp::with([
            'spph_bak_lkpp',
            'user_bak_lkpp',
            'spph_bak_lkpp.mitra_lkpps', 
            'chat_bak_lkpp' => function($q){
                $q->select('chat_bak_lkpps.*','users.name')
                ->join('users','users.username','chat_bak_lkpps.username')
                ->orderBy('created_at','asc');
                }
            ])->orderBy('created_at','desc');

        if($month != 13){
            $data->whereMonth('created_at',$month)
                ->whereYear('created_at',$year);
        }

        if(Auth::user()->level == 'administrator' || in_array(Auth::user()->username,json_decode($listApproveBak))){
                return $data;
        }else{
            return $data->where('created_by',Auth::user()->id);
        }
    }

    public function insertBakLkpp($data)
    {
        // dd($data);
        // declare model
        $bak                = new BakLkpp();
        // insert data 
        $bak->spph_id       = $data['spphid'];
        $bak->tgl_bak       = $data['tglbak'];
        $bak->nomor_bak     = $data['nomor_bak'];
        $bak->harga         = str_replace(".","",$data['harga']);
        $bak->isi           = $data['isi'];
        $bak->status        = $data['status'];
        $bak->created_by    = Auth::user()->id;

        // Get id after insert
        $pathLampiran = "public/files/BAK_LKPP/$bak->id/lampiran";
        // Upload Lampiran
        if($data['lampiran'] != null){
            foreach($data['lampiran'] as $file){
                $name       = $file->getClientOriginalName();
                $file->storeAs($pathLampiran, $name);
                $namanya[]  = $name;
                $lampiran[] = $pathLampiran.'/'.$name;
            }
        }
        $bak->lampiran        = $data['lampiran'] != null ? json_encode($lampiran) : NULL;
        $bak->title_lampiran  = $data['lampiran'] != null ? json_encode($namanya) : NULL;
        $bak->tgl_lampiran    = $data['lampiran'] != null ? date('Y-m-d') : NULL;
        $bak->save();

        return $bak;
    }

    public function updateBakLkpp($data, $bak)
    {
        if($bak->hold == true){
            $data['status'] = "save_bak";
        }
        // Update Data 
        $bak->spph_id       = $data['spphid'];
        $bak->tgl_bak       = $data['tglbak'];
        $bak->nomor_bak     = $data['nomor_bak'];
        $bak->harga         = str_replace(".","",$data['harga']);
        $bak->isi           = $data['isi'];
        $bak->status        = $data['status'];
        
        // Get id after insert
        $pathLampiran = "public/files/BAK_LKPP/$bak->id/lampiran";
        // Upload Lampiran
        if($data['lampiran'] != null){
            foreach($data['lampiran'] as $file){
                $name          = $file->getClientOriginalName();
                $file->storeAs($pathLampiran, $name);
                $namanya[]     = $name;
                $lampiran[]    = $pathLampiran.'/'.$name;
            }
        }
        $bak->lampiran        = $data['lampiran'] != null ? json_encode($lampiran) : $bak->lampiran;
        $bak->title_lampiran  = $data['lampiran'] != null ? json_encode($namanya) : $bak->title_lampiran;
        $bak->tgl_lampiran    = $data['lampiran'] != null ? date('Y-m-d H:i:s') : $bak->tgl_lampiran;
        $bak->save();
        return $bak;
    }

    public function updateRevisi($data, $bakLkpp)
    {
       // Insert Data 
        $bakLkpp->spph_id       = $data['spphid'];
        $bakLkpp->tgl_bak       = $data['tglbak'];
        $bakLkpp->nomor_bak     = $data['nomor_bak'];
        $bakLkpp->harga         = str_replace(".","",$data['harga']);
        $bakLkpp->isi           = $data['isi'];
        $bakLkpp->status        = $data['status'];
        $bakLkpp->status        = $data['status'];
        $bakLkpp->revisi        = $bakLkpp->revisi+1;

       // Get id after insert
       $pathLampiran = "public/files/BAK_LKPP/$bakLkpp->id/lampiran";
       
       // Upload Lampiran
       if($data['lampiran'] != null){
           foreach($data['lampiran'] as $file){
               $name          = $file->getClientOriginalName();
               $file->storeAs($pathLampiran, $name);
               $namanya[]     = $name;
               $lampiran[]    = $pathLampiran.'/'.$name;
           }
       }
       $bakLkpp->lampiran        = $data['lampiran'] != null ? json_encode($lampiran) : $bakLkpp->lampiran;
       $bakLkpp->title_lampiran  = $data['lampiran'] != null ? json_encode($namanya) : $bakLkpp->title_lampiran;
       $bakLkpp->tgl_lampiran    = $data['lampiran'] != null ? date('Y-m-d H:i:s') : $bakLkpp->tgl_lampiran;
       //  dd($bakn);
        $bakLkpp->save();
        return $bakLkpp;
    }


    public function getValueApproval($bak)
    {
        $notif = new NotificationWA();

        // get value for end approval
        $lastApproval       = DB::table('data_flows')
                            ->select('jabatan','username','queue')
                            ->where([
                                ['transaksi', 'BAK'],
                                ['unit', 'e-Commerce']
                            ])
                            ->latest('queue')
                            ->first();
        // get first value approval
        $firstApproval      = DB::table('data_flows')
                            ->select('jabatan','username','queue')
                            ->where([
                                ['transaksi','BAK'],
                                ['unit','e-Commerce']
                            ])
                            ->first();
         // get all data approval in json
         $jsonApproval      = DataFlow::where([
                                ['transaksi','BAK'],
                                ['unit','e-Commerce']
                            ])
                            ->get()
                            ->pluck('username');
        // update data approval 
        $bak->new_approval  = $jsonApproval;
        $bak->approval      = $firstApproval->username;
        $bak->end_approval  = $lastApproval->username;
        $bak->save();
        
        return $notif->notificationBakLkpp($bak);
    }

    public function uploadFile($data, $model)
    {
        $pathLampiran = "public/files/BAK_LKPP/$model->id/files";
        // Upload Lampiran
        if($data['file'] != null){
            foreach($data['file'] as $file){
                $name       = $file->getClientOriginalName();
                $file->storeAs($pathLampiran, $name);
                $namanya[]  = $name;
                $lampiran[] = $pathLampiran.'/'.$name;
            }
        }
        $model->file         = $data['file']        != null ? json_encode($lampiran) : $model->file;
        $model->title        = $data['file']        != null ? json_encode($namanya) : $model->title;
        $model->upload       = $data['file']        != null ? date('Y-m-d H:i:s') : $model->upload;
        $model->status       = 'done_bak';
        $model->save();
        return $model;
    }

    public function uploadLampiran($data, $model)
    {
        $pathLampiran = "public/files/BAK_LKPP/$model->id/lampiran";
        // Upload Lampiran
        if($data['lampiran'] != null){
            foreach($data['lampiran'] as $file){
                $name       = $file->getClientOriginalName();
                $file->storeAs($pathLampiran, $name);
                $namanya[]  = $name;
                $lampiran[] = $pathLampiran.'/'.$name;
            }
        }
        $model->lampiran        = $data['lampiran'] != null ? json_encode($lampiran) : $model->lampiran;
        $model->title_lampiran  = $data['lampiran'] != null ? json_encode($namanya) : $model->title_lampiran;
        $model->tgl_lampiran    = $data['lampiran'] != null ? date('Y-m-d H:i:s') : $model->tgl_lampiran;
        $model->save();

        return $model;
    }

    public function nextApprovalNew($model)
    {
        $notif          = new NotificationWA();

        // convert data to array
        $arrApproval    = json_decode($model->new_approval);
        // get key from array approval
        $getKey         = array_search(Auth::user()->username, $arrApproval);

        // loop and unset index has approve
        for($i = 0; $i< $getKey; $i++){
            unset($arrApproval[$i]);
        }
        // get data next after unset the data, and insert to variable 
        $next               = next($arrApproval);
        $model->approval    = $next;
        $model->save();
        return $notif->notificationBakLkpp($model);
    }

    public function nextApprovalOld($model)
    {
        $notif              = new NotificationWA();
        $approval           = new DataFlow();

        $jsonApproval       = $approval
                                ->where([
                                    ['min','<=',$model->harga],
                                    ['transaksi', 'BAK'],
                                    ['unit', 'e-Commerce']
                                    ])
                                ->get()
                                ->pluck('username');
        $isSubmitButton     = $approval
                                ->where('username',Auth::user()->username)
                                ->first();
        $isNextApprove      = $approval
                                ->where('unit','e-Commerce')
                                ->where('transaksi','BAK')
                                ->where('queue', ($isSubmitButton->queue+1))
                                ->first();

        $model->approval = $isNextApprove->username;
        if($model->new_approval == null){
            $model->new_approval = $jsonApproval;
        }
        $model->save();

        return $notif->notificationBakLkpp($model);
    }

    public function approvalReturn($model)
    {
        $notif = new NotificationWA();
        $model->approval = "Return";
        $model->save();
        return $notif->notificationBakLkpp($model);
    }

    public function approvalClosed($model)
    {
        $notif = new NotificationWA();
        $model->approval = "CLOSED";
        $model->save();
        // dd("closed");
        return $notif->notificationBakLkpp($model);
    }

}
