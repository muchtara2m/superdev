<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Role extends Model implements Auditable
{
    use LogsActivity,\OwenIt\Auditing\Auditable;

    protected $guarded = [];
    protected $fillable = [ 'name', 'display', 'menu_access', 'guard_name' ];
}
