<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Kontrak extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $guarded = [];
    protected $table = 'kontrak_lkpps';
    

    // Relation with other model
    public function sp3_lkpp()
    {
        return $this->belongsTo('App\Models\Sp3','sp3_id');
    }
    public function bakn_lkpp()
    {
        return $this->belongsTo('App\Models\BaknLKPP','bakn_id');
    }
    public function chat_kontrak_lkpp()
    {
        return $this->hasMany('App\Models\Chat','idTransaksi');
    }
    public function user_kontrak_lkpp()
    {
        return $this->belongsTo('App\User','created_by');
    }
    public function revisi_kontrak_lkpps()
    {
        return $this->hasMany(RevisiKontrakLkpp::class, 'kontrak_id');
    }

    // Function api data

    public function getNameLastApproval($id)
    {
        $dataBakn   = BaknLKPP::find($id);
        $getName        = DB::table('data_flows')
                    ->select('jabatan','data_flows.username','queue','users.name','users.position')
                    ->join('users','users.username','=','data_flows.username')
                    ->where([
                        ['min','<=',$dataBakn->harga],
                        ['max','>=',$dataBakn->harga],
                        ['transaksi', 'Kontrak'],
                        ['unit', 'e-Commerce']
                        ]);
        return $getName;
    }

    public function scopeDataSpphWhereBaknDone($id)
    {
        $data = BaknLKPP::with(['spph_lkpps' => function($q)
                {
                    $q->select('id','nomorspph','judul');
                }])->whereDoesntHave('kontrak_lkpp', function($query) use ($id)
                {
                    if($id != ""){
                        $query->where('id','!=', $id);
                    }
                })
                ->orderBy('updated_at','desc');
        return $data;
    }

    public function scopeDataAll($month, $year)
    {
        if(!$month || !$year){
            $month  = date('m');
            $year   = date('Y');
        }

        $data = Kontrak::with([
                    'bakn_lkpp',
                    'user_kontrak_lkpp',
                    'bakn_lkpp.spph_lkpps',
                    'bakn_lkpp.spph_lkpps.mitra_lkpps'
                ])
                ->orderBy('updated_at','desc');

    // condition select all
    if($month != 13 ){
        $data->whereMonth('created_at',$month)->whereYear('created_at',$year);
    }
    return $data;
   
    }

    public function scopeDataDraft($month, $year)
    {
        if(!$month || !$year){
            $month  = date('m');
            $year   = date('Y');
        }

        $data = Kontrak::with([
                    'bakn_lkpp',
                    'user_kontrak_lkpp',
                    'bakn_lkpp.spph_lkpps',
                    'bakn_lkpp.spph_lkpps.mitra_lkpps'
                ])
                ->where('status','draft_kontrak')
                ->where('approval',NULL)
                ->orderBy('updated_at','desc');

        // condition select all
        if($month != 13 ){
            $data->whereMonth('created_at',$month)->whereYear('created_at',$year);
        }
        // condition administrator or admin
        if(Auth::user()->level == 'administrator'){
            return $data;
        }else{  
            return $data->where('created_by',Auth::user()->id);
        }
    }

    public function scopeDataList($month, $year)
    {
        if(!$month || !$year){
            $month  = date('m');
            $year   = date('Y');
        }

        $data = Kontrak::with([
                    'bakn_lkpp',
                    'user_kontrak_lkpp',
                    'bakn_lkpp.spph_lkpps',
                    'bakn_lkpp.spph_lkpps.mitra_lkpps'
                ])
                ->where('approval','CLOSED')
                ->where('status','save_kontrak')
                ->orderBy('updated_at','desc');

        // condition select all
        if($month != 13 ){
            $data->whereMonth('created_at',$month)->whereYear('created_at',$year);
        }
        // condition administrator or admin
        if(Auth::user()->level == 'administrator'){
            return $data;
        }else{  
            return $data->where('created_by',Auth::user()->id);
        }
    }


    public function scopeDataDone($month, $year)
    {
        if(!$month || !$year){
            $month  = date('m');
            $year   = date('Y');
        }

        $data = Kontrak::with([
                    'bakn_lkpp',
                    'user_kontrak_lkpp',
                    'bakn_lkpp.spph_lkpps',
                    'bakn_lkpp.spph_lkpps.mitra_lkpps'
                    ])
                ->where('approval','CLOSED')
                ->where('status','done_kontrak')
                ->orderBy('updated_at','desc');

        // condition select all
        if($month != 13 ){
            $data->whereMonth('created_at',$month)->whereYear('created_at',$year);
        }
        // condition administrator or admin
        if(Auth::user()->level == 'administrator'){
            return $data;
        }else{  
            return $data->where('created_by',Auth::user()->id);
        }
    }


    public function scopeDataStatus($month, $year)
    {
        if(!$month || !$year){
            $month  = date('m');
            $year   = date('Y');
        }

        $data = Kontrak::with([
                    'bakn_lkpp',
                    'user_kontrak_lkpp',
                    'bakn_lkpp.spph_lkpps',
                    'bakn_lkpp.spph_lkpps.mitra_lkpps'
                ])
                ->where('approval','!=','CLOSED')
                ->where('hold',false)
                ->orderBy('updated_at','desc');

        // condition select all
        if($month != 13 ){
            $data->whereMonth('created_at',$month)
                ->whereYear('created_at',$year);
        }
        // condition administrator or admin
        if(Auth::user()->level == 'administrator'){
            return $data;
        }else{  
            return $data->where(function($q){
                $q->where('approval',Auth::user()->username);
            })->orWhere(function($w) use ($month, $year){
                $w->where('approval','Return')
                ->where('created_by',Auth::user()->id);
                if($month != 13){
                    $w->whereMonth('created_at',$month)
                     ->whereYear('created_at',$year);
                 }
            });
        }
    }

    public function scopeDataInprogress($month, $year)
    {
        if(!$month || !$year){
            $month  = date('m');
            $year   = date('Y');
        }

        $data = Kontrak::with([
                    'bakn_lkpp',
                    'user_kontrak_lkpp',
                    'bakn_lkpp.spph_lkpps',
                    'bakn_lkpp.spph_lkpps.mitra_lkpps',
                    'chat_kontrak_lkpp' => function($q){
                        $q->select('chats.*','users.name')
                        ->join('users','users.username','chats.username')
                        ->orderBy('created_at','asc');
                    }
                ])
                ->where('status','save_kontrak')
                ->where('approval','!=','CLOSED')
                ->orderBy('updated_at','desc');

        // condition select all
        if($month != 13 ){
            $data->whereMonth('created_at',$month)
                ->whereYear('created_at',$year);
        }
        $list = DataFlow::where('transaksi','Kontrak')
                ->where('unit','E-Commerce')
                ->get()
                ->pluck('username');

        // condition administrator or admin
        if(Auth::user()->level == 'administrator' || in_array(Auth::user()->username, json_decode($list)) ){
            return $data;
        }else{  
            return $data->where('created_by',Auth::user()->id);
        }
    }

    public function scopeDataTracking($month, $year)
    {
        if(!$month || !$year){
            $month  = date('m');
            $year   = date('Y');
        }

        $data = Kontrak::with([
                    'bakn_lkpp',
                    'user_kontrak_lkpp',
                    'bakn_lkpp.spph_lkpps',
                    'bakn_lkpp.spph_lkpps.mitra_lkpps',
                    'chat_kontrak_lkpp' => function($q){
                        $q->select('chats.*','users.name')
                        ->join('users','users.username','chats.username')
                        ->orderBy('created_at','asc');
                    }
                ])
                ->orderBy('updated_at','desc');

        // condition select all
        if($month != 13 ){
            $data->whereMonth('created_at',$month)
                ->whereYear('created_at',$year);
        }
        $list = DataFlow::where('transaksi','Kontrak')
                ->get()
                ->pluck('username');

        // condition administrator or admin
        if(Auth::user()->level == 'administrator' || in_array(Auth::user()->username, json_decode($list)) ){
            return $data;
        }else{  
            return $data->where('created_by',Auth::user()->id);
        }
    }



    // Function crud data
    public function insertKontrakLkpp($data)
    {
        dd($data);
        // declare model and insert data
        $kontrak                    = new Kontrak();
        $kontrak->bakn_id           = $data['bakn_id'];
        $kontrak->tanggal_kontrak   = $data['tanggal_kontrak'];
        $kontrak->nomor_kontrak     = $data['nomor_kontrak'];
        $kontrak->isi               = $data['isi'];
        $kontrak->jenis_kontrak     = $data['jenis_kontrak'];
        $kontrak->status            = $data['status'];
        $kontrak->created_by        = Auth::user()->id;

        // Get id after insert
        $pathLampiran = "public/files/KONTRAK_LKPP/$kontrak->id/lampiran";
        // Upload Lampiran
        if($data['lampiran'] != null){
            foreach($data['lampiran'] as $file){
                $name       = $file->getClientOriginalName();
                $file->storeAs($pathLampiran, $name);
                $namanya[]  = $name;
                $lampiran[] = $pathLampiran.'/'.$name;
            }
        }
        $kontrak->lampiran        = $data['lampiran'] != null ? json_encode($lampiran) : NULL;
        $kontrak->title_lampiran  = $data['lampiran'] != null ? json_encode($namanya) : NULL;
        $kontrak->tgl_lampiran    = $data['lampiran'] != null ? date('Y-m-d') : NULL;
        $kontrak->save();

        return $kontrak;
    }

    public function updateKontrakLkpp($data, $kontrak)
    {
        if($kontrak->hold == true){
            $data['status'] = "save_kontrak";
        }
        // Update Data 
        $kontrak->bakn_id           = $data['bakn_id'];
        $kontrak->tanggal_kontrak   = $data['tanggal_kontrak'];
        $kontrak->nomor_kontrak     = $data['nomor_kontrak'];
        $kontrak->isi               = $data['isi'];
        $kontrak->jenis_kontrak     = $data['jenis_kontrak'];
        $kontrak->status            = $data['status'];

        // Get id after insert
        $pathLampiran = "public/files/KONTRAK_LKPP/$kontrak->id/lampiran";

        // Upload Lampiran
        if($data['lampiran'] != null){
            foreach($data['lampiran'] as $file){
                $name          = $file->getClientOriginalName();
                $file->storeAs($pathLampiran, $name);
                $namanya[]     = $name;
                $lampiran[]    = $pathLampiran.'/'.$name;
            }
        }
        $kontrak->lampiran        = $data['lampiran'] != null ? json_encode($lampiran)  : $kontrak->lampiran ;
        $kontrak->title_lampiran  = $data['lampiran'] != null ? json_encode($namanya)   : $kontrak->title_lampiran ;
        $kontrak->tgl_lampiran    = $data['lampiran'] != null ? date('Y-m-d H:i:s')     : $kontrak->tgl_lampiran ;
        $kontrak->save();

        return $kontrak;
    }

    public function updateRevisi($data, $kontrak)
    {
        // Update Data 
        $kontrak->bakn_id           = $data['bakn_id'];
        $kontrak->tanggal_kontrak   = $data['tanggal_kontrak'];
        $kontrak->nomor_kontrak     = $data['nomor_kontrak'];
        $kontrak->isi               = $data['isi'];
        $kontrak->jenis_kontrak     = $data['jenis_kontrak'];
        $kontrak->status            = $data['status'];
        $kontrak->revisi            = ($kontrak->revisi+1);
        $kontrak->approval          = NULL;
        $kontrak->new_approval      = NULL;
        $kontrak->endapproval       = NULL;


        // Get id after insert
        $pathLampiran = "public/files/KONTRAK_LKPP/$kontrak->id/lampiran";

        // Upload Lampiran
        if($data['lampiran'] != null){
            foreach($data['lampiran'] as $file){
                $name          = $file->getClientOriginalName();
                $file->storeAs($pathLampiran, $name);
                $namanya[]     = $name;
                $lampiran[]    = $pathLampiran.'/'.$name;
            }
        }
        $kontrak->lampiran        = $data['lampiran'] != null ? json_encode($lampiran)  : $kontrak->lampiran ;
        $kontrak->title_lampiran  = $data['lampiran'] != null ? json_encode($namanya)   : $kontrak->title_lampiran ;
        $kontrak->tgl_lampiran    = $data['lampiran'] != null ? date('Y-m-d H:i:s')     : $kontrak->tgl_lampiran ;
        $kontrak->save();

        return $kontrak;
    }

    public function getValueApproval($model)
    {
        $notif = new NotificationWA();

        // get value for end approval
        $lastApproval       = DB::table('data_flows')
                            ->select('jabatan','username','queue')
                            ->where([
                                ['transaksi', 'Kontrak'],
                                ['unit', 'e-Commerce']
                            ])
                            ->latest('queue')
                            ->first();
        // get first value approval
        $firstApproval      = DB::table('data_flows')
                            ->select('jabatan','username','queue')
                            ->where([
                                ['transaksi','Kontrak'],
                                ['unit','e-Commerce']
                            ])
                            ->first();
         // get all data approval in json
         $jsonApproval      = DataFlow::where([
                                ['transaksi','Kontrak'],
                                ['unit','e-Commerce']
                            ])
                            ->get()
                            ->pluck('username');

        // dd($lastApproval, $firstApproval, $jsonApproval);
        // update data approval 
        $model->new_approval    = $jsonApproval;
        $model->approval        = $firstApproval->username;
        $model->endapproval     = $lastApproval->username;
        $model->save();
        
        return $notif->notificationKontrakLkpp($model);
    }
   

    // function approval use add value queue
    public function nextApprovalOld($kontrak)
    {
        $notif              = new NotificationWA();
        $approval           = new DataFlow();

        $jsonApproval       = $approval
                                ->where([
                                    ['min','<=',$kontrak->bakn_lkpp->hargas],
                                    ['transaksi', 'Kontrak'],
                                    ['unit', 'e-Commerce']
                                    ])
                                ->get()
                                ->pluck('username');
        $isSubmitButton     = $approval
                                ->where('username',Auth::user()->username)
                                ->first();
        $isNextApprove      = $approval
                                ->where('queue', ($isSubmitButton->queue+1))
                                ->first();
        $kontrak->approval = $isNextApprove->username;
        if($kontrak->new_approval == null){
            $kontrak->new_approval = $jsonApproval;
        }
        // dd("ini old approval", $id, $isNextApprove->username);
        $kontrak->save();

        return $notif->notificationKontrakLkpp($kontrak);
        
    }

    // new function approval use array
    public function nextApprovalNew($kontrak)
    {
        $notif          = new NotificationWA();

        // convert data to array
        $arrApproval    = json_decode($kontrak->new_approval);
        // get key from array approval
        $getKey         = array_search(Auth::user()->username, $arrApproval);

        // loop and unset index has approve
        for($i = 0; $i< $getKey; $i++){
            unset($arrApproval[$i]);
        }
        // get data next after unset the data, and insert to variable 
        $next               = next($arrApproval);
        $kontrak->approval  = $next;
        $kontrak->save();
        
        return $notif->notificationKontrakLkpp($kontrak);
    }

    public function approvalReturn($kontrak)
    {
        $notif = new NotificationWA();
        $kontrak->approval = "Return";
        $kontrak->save();
        return $notif->notificationKontrakLkpp($kontrak);
    }

    public function approvalClosed($kontrak)
    {
        $notif = new NotificationWA();
        $kontrak->approval = "CLOSED";
        $kontrak->save();
        // dd("closed");
        return $notif->notificationKontrakLkpp($kontrak);
    }
    
    public function uploadFile($data, $kontrak)
    {
       // Get id after insert
       $pathLampiran = "public/files/KONTRAK_LKPP/$kontrak->id/file";
       // Upload Lampiran
       if($data['file'] != null){
           foreach($data['file'] as $file){
               $name          = $file->getClientOriginalName();
               $file->storeAs($pathLampiran, $name);
              
               // insert value to array 
               $namanya[]     = $name;
               $lampiran[]    = $pathLampiran.'/'.$name;
           }
       }
       $kontrak->file          = $data['file'] != null ? json_encode($lampiran) : NULL;
       $kontrak->title         = $data['file'] != null ? json_encode($namanya) : NULL;
       $kontrak->upload        = $data['file'] != null ? date('Y-m-d H:i:s') : NULL;
       $kontrak->status        = "done_kontrak";
       $kontrak->save();

       return $kontrak;
    }

    public function uploadLampiran($data, $kontrak)
    {
        
         // Get id after insert
         $pathLampiran = "public/files/KONTRAK_LKPP/$kontrak->id/lampiran";
         // Upload Lampiran
         if($data['lampiran'] != null){
             foreach($data['lampiran'] as $file){
                 $name          = $file->getClientOriginalName();
                 $file->storeAs($pathLampiran, $name);
                
                 // insert value to array 
                 $namanya[]     = $name;
                 $lampiran[]    = $pathLampiran.'/'.$name;
             }
         }

         $kontrak->lampiran        = $data['lampiran'] != null ? json_encode($lampiran) : NULL;
         $kontrak->title_lampiran  = $data['lampiran'] != null ? json_encode($namanya) : NULL;
         $kontrak->tgl_lampiran    = $data['lampiran'] != null ? date('Y-m-d H:i:s') : NULL;
         $kontrak->save();

         return $kontrak;
    }
    
}
