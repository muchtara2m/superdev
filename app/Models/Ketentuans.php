<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Ketentuans extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $guarded = [];
    protected $table = 'ketentuans';


    public function allData()
    {
        $data = Ketentuans::all();
        return $data;
    }

    public function insertData($data)
    {
        $query = Ketentuans::create($data);
        return $query;
    }

    public function updateData($data)
    {
        // $query = Kete
    }


    public function deleteData($id)
    {
        $data = Ketentuans::find($id);
        $data->delete();
        return response()->json(null,200);
    }

}

