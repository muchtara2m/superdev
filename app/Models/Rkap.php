<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Rkap extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $connection = 'mysql2';
    protected $table = 'rkap';
    protected $guarded = [];
    
    // One to many Rkap to Ubis
    public function rkap_ubis()
    {
        return $this->belongsTo('App\Models\Ubis', 'ubis');
    }
}
