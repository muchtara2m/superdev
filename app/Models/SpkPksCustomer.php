<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class SpkPksCustomer extends Model implements Auditable
{
    //
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $connection = 'mysql2';
    protected $table = 't_spk_pks_customer';
    protected $guarded = [];

    public function customerSpkPks(){
        return $this->belongsTo('App\Models\CustomerSpkPks','akun_customer');
    }
    public function ioSpkPksCustomer(){
        return $this->belongsTo('App\Models\IoPipeline','no_io');
    }
    public function mappingIoCustomer(){
        return $this->belongsTo('App\Models\MappingIoUbis','no_io');
    }
}
