<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class ChatNon extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $guarded = [];
    //
    public function kontraknons()
    {
        return $this->belongsTo('App\Models\KontrakNon','id');
    }
    public function namabikin(){
        return $this->belongsTo('App\User','id');
    }
}
