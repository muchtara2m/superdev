<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class CaraBayar extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $table = 'cara_bayars';
    protected $guarded = [];

    public function allData()
    {
        $data = CaraBayar::all();
        return response()->json($data, 200);
    }

    public function store($data)
    {
        $response = CaraBayar::created($data);
        return response()->json($response, 201);
    }

    public function edit($data, $id)
    {
        $response = CaraBayar::where('id', $id)->update($data);
        return response()->json($response, 200);
    }

    public function deleteData($id)
    {
        $data = CaraBayar::where('id', $id)->first();
        $data->delete();
        return response()->json(null, 200);
    }


}
