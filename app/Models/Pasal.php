<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Pasal extends Model implements Auditable
{
    //
    use LogsActivity, \OwenIt\Auditing\Auditable;
    protected $guarded = [];


    public function detData()
    {
        $data = Pasal::all();
        return response()->json($data, 200);
    }

    public function storeData($data)
    {
        $data = Pasal::create($data);
        return response()->json($data, 201);
    }

    public function updateData($data, $id)
    {
        $data = Pasal::where('id', $id)->update($data);
        return response()->json($data, 200);
    }
    
}
