<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class RevisiBakLkpp extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;

    protected $guarded = [];

    public function revisi_bak_lkpps()
    {
        return $this->belongsTo(BakLkpp::class, 'bak_id');
    }

    public function insertRevisiBakLkpp($data)
    {
        // declare Model
        $revisi = new RevisiBakLkpp();
        // Insert Data 
        $revisi->bak_id               = $data->id;
        $revisi->spph_id              = $data->spph_bak_lkpp->nomorspph;
        $revisi->nomor_bak            = $data->nomor_bak;
        $revisi->tgl_bak              = $data->tgl_bak;
        $revisi->harga                = $data->harga;
        $revisi->isi                  = $data->isi;
        $revisi->status               = $data->status;

        $revisi->lampiran             = $data->lampiran;
        $revisi->title_lampiran       = $data->title_lampiran;
        $revisi->tgl_lampiran         = $data->tgl_lampiran;
        $revisi->file                 = $data->file;
        $revisi->title                = $data->title;
        $revisi->upload               = $data->upload;
        $revisi->approval             = $data->approval;
        $revisi->new_approval         = $data->new_approval;
        $revisi->end_approval         = $data->end_approval;
        $revisi->created_by           = $data->user_bak_lkpp->username;
        $revisi->hold                 = $data->hold;
        $revisi->save();
        return response()->json($revisi, 201);
    }

}
