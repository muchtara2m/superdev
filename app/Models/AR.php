<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;


class AR extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    protected $table = 'ars';
    protected $guarded = [];
    protected static $logAttributes = ['io_id', 'status'];

    public function dataio()
    {
        return $this->belongsTo('App\Models\Iodesc','io_id');
    }
    public function pembuat(){
        return $this->belongsTo('App\User','created_by');
    }
    public function customer(){
        return $this->belongsTo('App\Models\Customer', 'customer_id');
    }
    public function chatar()
    {
        return $this->hasMany('App\Models\ChatAR','idAR');
    }
}
