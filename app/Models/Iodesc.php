<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Iodesc extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $guarded = [];
    protected $table = 'mtr_io';
    protected $primaryKey = 'no_io';
    public $incrementing = false;

    public function bakns(){
        return $this->hasMany('App\Models\Bakn','io_id');
    }
    public function bakn_lkpp(){
        return $this->hasMany('App\Models\BaknLKPP','io_id');
    }
    public function ars(){
        return $this->hasMany('App\Models\AR','io_id');
    }

    public function getDataIo()
    {
        $data = Iodesc::all();
        return response()->json($data, 200);
    }
}
