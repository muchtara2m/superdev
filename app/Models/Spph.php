<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

// use Illuminate\Database\Eloquent\SoftDeletes;

class Spph extends Model implements Auditable
{
    //
    // use SoftDeletes;
    use LogsActivity, \OwenIt\Auditing\Auditable;
    protected $guarded = [];
    protected $fillable = [
        'file_title',
        'file'
    ];
    protected static $logAttributes = ['nomorspph', 'judul'];

    public function bakns(){
        return $this->hasMany("App\Models\Bakn",'spph_id');
    }
    public function creator(){
        return $this->belongsTo('App\User','created_by');
    }
    public function mitras(){
        return $this->belongsTo('App\Models\Mitra', 'mitra');
    }
}
