<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Inisiasi extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $guarded = [];
    protected $connection = 'mysql2';
    protected $table = 't_inisiasi';


    public function iopipe(){
        return $this->belongsTo('App\Models\IoPipeline','no_io');
    }


}
