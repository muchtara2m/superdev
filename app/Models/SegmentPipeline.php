<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class SegmentPipeline extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $connection = 'mysql2';
    protected $table = 't_segment';
    protected $primaryKey = 'id_segmen';
    protected $guarded = [];

}
