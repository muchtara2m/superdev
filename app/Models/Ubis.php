<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Ubis extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;

    protected $guarded = [];
    protected $connection = 'mysql2';
    protected $table = 'ubis';
    protected $primaryKey = 'id_ubis';
    // One to many Ubis -> Plan_Bast
    public function ubis_outlook()
    {
        return $this->hasMany('App\Models\PlanBast', 'ubis');
    }
    public function ubis_rkap()
    {
        return $this->hasMany('App\Models\Rkap', 'ubis');
    }
    public function ubisVendor(){
        return $this->hasMany('App\Models\MappingIoUbis','id_ubis');
    }
    public function ubisCustomer(){
        return $this->hasMany('App\Models\MappingIoUbis','id_ubis');
    }
}
