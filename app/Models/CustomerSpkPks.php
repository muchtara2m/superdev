<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class CustomerSpkPks extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $guarded = [];
    protected $connection = 'mysql2';
    protected $table = 't_customer' ;
    protected $primaryKey = 'akun_customer';

    public function SpkPksCustomer(){
        return $this->hasMany('App\Models\SpkPksCustomer','akun_customer');
    }
}
