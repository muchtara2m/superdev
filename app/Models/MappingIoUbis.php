<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MappingIoUbis extends Model
{
    use LogsActivity;
    //
    protected $connection = 'mysql2';
    protected $table = 't_mapping_io_ubis';
    protected $primaryKey = 'no_io';

    public function mappingSpkPksVendor(){
        return $this->hasMany('App\Models\SpkPksVendor','no_io');
    }
    public function ubisMappingVendor(){
        return $this->belongsTo('App\Models\Ubis','ubis_after_to');
    }
    public function mappingSpkPksCustomer(){
        return $this->hasMany('App\Models\SpkPksCustomer','no_io');
    }
    public function ubisMappingCustomer(){
        return $this->belongsTo('App\Models\Ubis','ubis_after_to');
    }
    
}
