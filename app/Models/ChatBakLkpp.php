<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class ChatBakLkpp extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    //
    protected $guarded = [];

    function bak_lkpp(){
        return $this->belongsTo('App\Models\BakLkpp','id');
    }

    public function username_chat_bak_lkpp()
    {
        return $this->belongsTo('App\User','username');
    }

    public function insertChatBak($data, $chat)
    {
        $komen                  = new ChatBakLkpp();
        $komen->chat            = $chat;
        $komen->queue           = 0;
        $komen->jabatan         = Auth::user()->position;
        $komen->username        = Auth::user()->username;
        $komen->id_bak_lkpp     = $data->id;
        $komen->transaksi       = 'BAK';
        $komen->status          = 'Approve';
        $komen->save();

        return $komen;
    }

    public function insertChatRevisi($data, $model)
    {
        $komen                  = new ChatBakLkpp();
        $komen->chat            = $data['chat'];
        $komen->queue           = 0;
        $komen->jabatan         = Auth::user()->position;
        $komen->username        = Auth::user()->username;
        $komen->idTransaksi     = $model->id;
        $komen->transaksi       = 'BAK';
        $komen->status          = 'Approve';
        $komen->save();

        return $komen;
    }

    public function isApprove($data, $model)
    {
        $queue = DataFlow::where([
            ['username',Auth::user()->username],
            ['transaksi','BAK'],
            ['unit','e-Commerce']
            ])->first();

        $komen                  = new ChatBakLkpp();
        $komen->chat            = $data['chat'];
        $komen->queue           = $queue->queue;
        $komen->jabatan         = Auth::user()->position;
        $komen->username        = Auth::user()->username;
        $komen->id_bak_lkpp    = $model->id;
        $komen->transaksi       = 'BAK';
        $komen->status          = $data['status'];
        $komen->save();
        return $komen;
    }
}
