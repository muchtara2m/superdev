<?php

namespace App\Models;

use App\User;
use App\Models\ChatBaknLkpp;

use Exception;
use Illuminate\Database\Eloquent\Model;

class NotificationWA extends Model
{
    
    
    public function notificationBaknLkpp($bakn)
    {  
        
        $lastComment    = ChatBaknLkpp::where('id_bakn_lkpp',$bakn->id)
                            ->select('chat_bakn_lkpp.*','users.name')
                            ->join('users','users.username','chat_bakn_lkpp.username')
                            ->orderBy('created_at','desc')
                            ->first();
         // if approve get data approval
         $approve = User::where('username',$bakn->approval)->first();

        if($bakn->revisi >= 1){
            $awal = " Revisi";
        }else{
            $awal ='';
        }

        if ($bakn->approval == 'CLOSED') {

            $hp         = substr(str_replace("-","",$bakn->user_bakn_lkpp->phone),6,13);
            $urls       = url('list-bakn-lkpp');
            $body       = 'Bapak/Ibu *'.$bakn->user_bakn_lkpp->name.'*'
            ."\xA".'File'.$awal.' BAKN dengan Nomor SPPH'
            ."\xA".'*'.$bakn->spph_lkpps['nomorspph'].'*'
            ."\xA".'Dengan Judul'
            ."\xA".'*'.$bakn->spph_lkpps['judul'].'*'
            ."\xA".'Sudah Selesai di Approve, silahkan askes pada link berikut['
            ."\xA".$urls;
        }else if($bakn->approval == 'Return'){

            $hp         = substr(str_replace("-","",$bakn->user_bakn_lkpp->phone),6,13);
            $urls       = url('status-transaksi-bakn-lkpp');
            $body       = 'Bapak/Ibu *'.$bakn->user_bakn_lkpp->name.'*'
            ."\xA".'File'.$awal.' BAKN dengan Nomor SPPH'
            ."\xA".'*'.$bakn->spph_lkpps['nomorspph'].'*'
            ."\xA".'Dengan Judul'
            ."\xA".'*'.$bakn->spph_lkpps['judul'].'*'
            ."\xA".'Di Return oleh '. strtoupper($lastComment->name)
            ."\xA".'Dengan Catatan '
            ."\xA".'" *_'.$lastComment->chat.'_* "'
            ."\xA".'Silahkan akses pada link berikut'
            ."\xA".$urls;
        }else{

            $hp         = substr(str_replace("-","",$approve->phone),6,13);
            $urls       = url('preview-status-bakn-lkpp/'.$bakn->id);
            $body       = 'Bapak/Ibu *'.$approve->name.'*'
            ."\xA".'File'.$awal.' BAKN dengan Nomor SPPH'
            ."\xA".'*'.$bakn->spph_lkpps['nomorspph'].'*'
            ."\xA".'Dengan Judul'
            ."\xA".'*'.$bakn->spph_lkpps['judul'].'*'
            ."\xA".'Membutuhkan Approve Anda, silahkan akses pada link berikut '
            ."\xA".$urls;
        }
        
        try{
            $notif = $this->sendNotifToWa($hp, $body);
            return response()->json(["sent" => $notif->original['message']], 200);
        }catch(Exception $e){
            return response()->json(['sent'=>'true'],200);
        }
    }
    
    public function notificationBakLkpp($bak)
    {
        // get last comment approval
        $lastnya    = ChatBakLkpp::where('id_bak_lkpp',$bak->id)
                        ->select('chat_bak_lkpps.*','users.name')
                        ->join('users','users.username','chat_bak_lkpps.username')
                        ->orderBy('created_at','desc')
                        ->first();

        // if approve get data approval
        $approve = User::where('username', $bak->approval)->first();

        if($bak->revisi >= 1){
            $awal = " Revisi";
        }else{
            $awal ='';
        }
        if ($bak->approval == 'CLOSED') {

            $hp     = substr(str_replace("-","",$bak->user_bak_lkpp['phone']),6,13);
            $urls   = url('list-bak-lkpp');
            $body   = 'Bapak/Ibu *'.$bak->user_bak_lkpp['name'].'*'
            ."\xA".'File'.$awal.' BAK dengan Nomor BAK'
            ."\xA".'*'.$bak->nomor_bak.'*'
            ."\xA".'Dengan Judul'
            ."\xA".'*'.$bak->spph_bak_lkpp['judul'].'*'
            ."\xA".'Sudah Selesai di Approve, silahkan askes pada link berikut'
            ."\xA".$urls;

        }else if($bak->approval == 'Return'){

            $hp     = substr(str_replace("-","",$bak->user_bak_lkpp['phone']),6,13);
            $urls   = url('status-transaksi-bak-lkpp');
            $body   = 'Bapak/Ibu *'.$bak->user_bak_lkpp['name'].'*'
            ."\xA".'File'.$awal.' BAK dengan Nomor BAK'
            ."\xA".'*'.$bak->nomor_bak.'*'
            ."\xA".'Dengan Judul'
            ."\xA".'*'.$bak->spph_bak_lkpp['judul'].'*'
            ."\xA".'Di Return oleh '. strtoupper($lastnya->name)
            ."\xA".'Dengan Catatan '
            ."\xA".'" *_'.$lastnya->chat.'_* "'
            ."\xA".'Silahkan akses pada link berikut'
            ."\xA".$urls;

        }else{

            $hp     = substr(str_replace("-","",$approve->phone),6,13);
            $urls   = url('preview-status-bak-lkpp/'.$bak->id);
            $body   = 'Bapak/Ibu *'.$approve->name.'*'
            ."\xA".'File'.$awal.' BAK dengan Nomor BAK'
            ."\xA".'*'.$bak->nomor_bak.'*'
            ."\xA".'Dengan Judul'
            ."\xA".'*'.$bak->spph_bak_lkpp['judul'].'*'
            ."\xA".'Membutuhkan Approve Anda, silahkan akses pada link berikut '
            ."\xA".$urls;
            
        }
        
        try{
            $notif = $this->sendNotifToWa($hp, $body);
            return response()->json(["sent" => $notif->original['message']], 200);
        }catch(Exception $e){
            return response()->json(['sent'=>'true'],200);
        }
    }

    public function notificationKontrakLkpp($kontrak)
    {

        // get last comment approval
        $lastnya    = Chat::where('idTransaksi',$kontrak->id)
        ->select('chats.*','users.name')
        ->join('users','users.username','chats.username')
        ->orderBy('created_at','desc')->first();

        // if approve get data approval
        $approve = User::where('username',$kontrak->approval)->first();

        if($kontrak->revisi >= 1){
            $awal = " Revisi";
        }else{
            $awal ='';
        }

        // function shorten url
        if ($kontrak->approval == 'CLOSED') {

            $hp     = substr(str_replace("-","",$kontrak->user_kontrak_lkpp['phone']),6,13);
            $urls   = url('list-kontrak-lkpp');
            $body   = 'Bapak/Ibu *'.$kontrak->user_kontrak_lkpp['name'].'*'
            ."\xA".'File Kontrak dengan Nomor'
            ."\xA".'*'.$kontrak->nomor_kontrak.'*'
            ."\xA".'Dengan Judul'
            ."\xA".'*'.$kontrak->bakn_lkpp->spph_lkpps['judul'].'*'
            ."\xA".'Sudah Selesai di Approve, silahkan askes pada link berikut'
            ."\xA".$urls;

        }else if($kontrak->approval == 'Return'){

            $hp     = substr(str_replace("-","",$kontrak->user_kontrak_lkpp['phone']),6,13);
            $urls   = url('status-transaksi-kontrak-lkpp');
            $body   = 'Bapak/Ibu *'.$kontrak->user_kontrak_lkpp['name'].'*'
            ."\xA".'File Kontrak dengan Nomor '
            ."\xA".'*'.$kontrak->nomor_kontrak.'*'
            ."\xA".'Dengan Judul'
            ."\xA".'*'.$kontrak->bakn_lkpp->spph_lkpps['judul'].'*'
            ."\xA".'Di Return oleh '. strtoupper($lastnya->name)
            ."\xA".'Dengan Catatan '
            ."\xA".'" *_'.$lastnya->chat.'_* "'
            ."\xA".'Silahkan akses pada link berikut'
            ."\xA".$urls;

        }else{

            $hp     = substr(str_replace("-","",$approve->phone),6,13);
            $urls   = url('preview-status-kontrak-lkpp/'.$kontrak->id);
            $body   = 'Bapak/Ibu *'.$approve->name.'*'
            ."\xA".'File Kontrak dengan Nomor '
            ."\xA".'*'.$kontrak->nomor_kontrak.'*'
            ."\xA".'Dengan Judul'
            ."\xA".'*'.$kontrak->bakn_lkpp->spph_lkpps['judul'].'*'
            ."\xA".'Membutuhkan Approve Anda, silahkan akses pada link berikut '
            ."\xA".$urls;
            
        }

        try{
            $notif = $this->sendNotifToWa($hp, $body);
            return response()->json(["sent" => $notif->original['message']], 200);
        }catch(Exception $e){
            return response()->json(['sent'=>'true'],200);
        }
    }
    
    public function sendNotifToWa($hp, $body)
    {

        $hp         = '81280295238';
        $data       = [
            'phone' => '62'.$hp, // Receivers phone
            'body' => $body,
        ];
        $json       = json_encode($data); // Encode data to JSON
        // URL for request POST /message
        $url        = env('API_MAIL');
        // Make a POST request
        $options    = stream_context_create(['http' => [
                            'method'  => 'POST',
                            'header'  => 'Content-type: application/json',
                            'content' => $json
                        ]
                    ]);
        // Send a request
        $result     = file_get_contents($url, false, $options);
        $var        = json_decode($result, true);
        
        return response()->json([
            'message'=>$var['message'], 
            'sent' => $var['sent']
        ],200);
    }
}