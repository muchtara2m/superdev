<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class RevisiBaknLkpp extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;

    protected $guarded = [];

    public function revisi_bakn_lkpps()
    {
        return $this->belongsTo(BaknLKPP::class, 'bakn_id');
    }

    public function insertRevisiBaknLkpp($data)
    {
        // declare Model
        $revisi = new RevisiBaknLkpp();
        // Insert Data 
        $revisi->bakn_id              = $data->id;
        $revisi->spph_id              = $data->spph_lkpps->nomorspph;
        $revisi->tglbakn              = $data->tglbakn;
        $revisi->isi                  = $data->isi;
        $revisi->tipe_rapat           = $data->tipe_rapat;
        $revisi->pimpinan_rapat       = $data->pimpinan_rapat;
        $revisi->peserta_mitra        = $data->peserta_mitra;
        $revisi->peserta_pins         = $data->peserta_pins;
        $revisi->harga                = $data->harga;
        $revisi->io_id                = $data->io_id;
        $revisi->agenda               = $data->agenda;
        $revisi->dasar_pembahasan     = $data->dasar_pembahasan;
        $revisi->ruang_lingkup        = $data->ruang_lingkup;
        $revisi->lokasi_pekerjaan     = $data->lokasi_pekerjaan;
        $revisi->jangka_waktu         = $data->jangka_waktu;
        $revisi->harga_terbilang      = $data->harga_terbilang;
        $revisi->cara_bayar           = $data->cara_bayar;
        $revisi->lain_lain            = $data->lain_lain;
        $revisi->created_by           = $data->user_bakn_lkpp->username;
        $revisi->status               = $data->status;
        $revisi->file                 = $data->file;
        $revisi->title                = $data->title;
        $revisi->upload               = $data->upload;
        $revisi->lampiran             = $data->lampiran;
        $revisi->title_lampiran       = $data->title_lampiran;
        $revisi->tgl_lampiran         = $data->tgl_lampiran;
        $revisi->approval             = $data->approval;
        $revisi->new_approval         = $data->new_approval;
        $revisi->end_approval         = $data->end_approval;
        $revisi->hold                 = $data->hold;
        $revisi->save();
        return response()->json($revisi, 201);
    }

}
