<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class ChatBaknLkpp extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    protected static $logAttributes = ['id','chat','queue','jabatan','username','id_bakn_lkpp','transaksi','status'];
    protected $table = 'chat_bakn_lkpp';
    protected $guarded = [];

    public function bakn_lkpp()
    {
        return $this->belongsTo('App\Models\BaknLKPP','id');
    }

    public function username_chat_bakn_lkpp()
    {
        return $this->belongsTo('App\User','username');
    }

    public function insertChat($id, $chat)
    {
        $komen = new ChatBaknLkpp();
        $komen->chat = $chat;
        $komen->queue = 0;
        $komen->jabatan = Auth::user()->position;
        $komen->username = Auth::user()->username;
        $komen->id_bakn_lkpp = $id->id;
        $komen->transaksi = 'BAKN';
        $komen->status = 'Approve';
        $komen->save();
        return $komen;
    }

    public function insertChatRevisi($data, $bakn)
    {
        // dd($data, $bakn->id);
        $komen                  = new ChatBaknLkpp();
        $komen->chat            = $data['chat'];
        $komen->queue           = 0;
        $komen->jabatan         = Auth::user()->position;
        $komen->username        = Auth::user()->username;
        $komen->idTransaksi     = $bakn->id;
        $komen->transaksi       = 'BAKN';
        $komen->status          = 'Approve';
        $komen->save();

        return $komen;
    }

    public function isApprove($data, $model)
    {
        $queue = DataFlow::where([
            ['username',Auth::user()->username],
            ['transaksi','BAKN'],
            ['unit','e-Commerce']
            ])->first();

        $komen                  = new ChatBaknLkpp();
        $komen->chat            = $data['chat'];
        $komen->queue           = $queue->queue;
        $komen->jabatan         = Auth::user()->position;
        $komen->username        = Auth::user()->username;
        $komen->id_bakn_lkpp    = $model->id;
        $komen->transaksi       = 'BAKN';
        $komen->status          = $data['status'];
        $komen->save();
        return $komen;
    }
}
