<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class DataFlow extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $guarded = [];
    protected $table = 'data_flows';

    public function listApproval($unit, $transaksi)
    {
        $data = DataFlow::where([
                ['unit', $unit],
                ['transaksi', $transaksi]
            ])
            ->orderBy('queue','asc');
        return $data;
    }

}
