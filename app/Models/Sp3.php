<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Sp3 extends Model implements Auditable
{
    //
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $table = 'sp3_lkpps';
    protected $guarded = [];
    
    public function bakn_lkpp()
    {
        return $this->belongsTo('App\Modles\BaknLKPP','bakn_id');
    }
    public function kontrak_lkpp()
    {
        return $this->hasMany('App\Modles\Kontrak','sp3_id');
    }
    public function chatsp3(){
        return $this->hasMany('App\Modles\ChatSp3','idsp3');
    }
    public function user_sp3_lkpp(){
        return $this->belongsTo('App\User','created_by');
    }
    public function kontraksnon()
    {
        return $this->hasMany('App\Modles\KontrakNon','sp3_id');
    }
}
