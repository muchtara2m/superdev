<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class ChatAR extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $table = 'chat_ars';
    protected $guarded = [];
    
    public function ars(){
        return $this->belongsTo('App\Models\AR','id');
    }
}
