<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class RevisiKontrakLkpp extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $guarded = [];

    public function revisi_kontrak_lkpps()
    {
        return $this->belongsTo(Kontrak::class, 'kontrak_id');
    }

    public function insertData($data)
    {
        $jenisPasal = JenisPasal::where('id', $data->jenis_kontrak)->first();
        $revisi     = new RevisiKontrakLkpp();

        $revisi->kontrak_id         = $data->id;
        $revisi->bakn_id            = $data->bakn_id;
        $revisi->nomor_kontrak      = $data->nomor_kontrak;
        $revisi->tanggal_kontrak    = $data->tanggal_kontrak;
        $revisi->jenis_kontrak      = $jenisPasal->jenis_pasal;
        $revisi->isi                = $data->isi;
        $revisi->new_approval       = $data->new_approval;
        $revisi->approval           = $data->approval;
        $revisi->endapproval        = $data->endapproval;
        $revisi->status             = $data->status;
        $revisi->file               = $data->file;
        $revisi->title              = $data->title;
        $revisi->upload             = $data->upload;
        $revisi->lampiran           = $data->lampiran;
        $revisi->title_lampiran     = $data->title_lampiran;
        $revisi->tgl_lampiran       = $data->tgl_lampiran;
        $revisi->hold               = $data->hold;
        $revisi->created_by         = $data->user_kontrak_lkpp->username;
        $revisi->created_at         = $data->created_at;
        $revisi->save();

        return response()->json($revisi, 201);
    }
}
