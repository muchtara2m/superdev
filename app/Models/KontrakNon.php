<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class KontrakNon extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $guarded = [];
    
    public function sp3s()
    {
        return $this->belongsTo('App\Models\Sp3','sp3_id');
    }
    public function bakns()
    {
        return $this->belongsTo('App\Models\Bakn','bakn_id');
    }
    public function chatnons()
    {
        return $this->hasMany('App\Models\ChatNon','idKontrakNon');
    }
    public function users(){
        return $this->belongsTo('App\User','created_by');
    }
}
