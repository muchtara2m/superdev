<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;



class AmPipeline extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    //
    protected $guarde = [];
    protected $connection = 'mysql2';
    protected $table = 't_am' ;
    protected $primaryKey = 'am_id';
}
