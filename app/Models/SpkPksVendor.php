<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class SpkPksVendor extends Model implements Auditable
{
    //
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $connection = 'mysql2';
    protected $table = 't_spk_pks_vendor';
    protected $guarded = [];

    public function akunVendor(){
        return $this->belongsTo('App\Models\Vendor','akun_vendor');
    }
    public function mappingIo(){
        return $this->belongsTo('App\Models\MappingIoUbis','no_io');
    }
    public function ioSpkPksVendor(){
        return $this->belongsTo('App\Models\IoPipeline','no_io');
    }
}
