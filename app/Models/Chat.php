<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Chat extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    //
    protected $table = 'chats';
    protected $guarded = [];
    
    public function kontraks()
    {
        return $this->belongsTo('App\Models\Kontrak','id');
    }

    public function insertChatKontrak($data)
    {
        $komen                  = new Chat();
        $komen->chat            = $data['chat'];
        $komen->queue           = 0;
        $komen->jabatan         = Auth::user()->position;
        $komen->username        = Auth::user()->username;
        $komen->idTransaksi     = $data['id'];
        $komen->transaksi       = 'Kontrak';
        $komen->status          = 'Approve';
        $komen->save();

        return $komen;
    }

    public function insertChatRevisi($data)
    {
        $komen                  = new Chat();
        $komen->chat            = $data['chat'];
        $komen->queue           = 0;
        $komen->jabatan         = Auth::user()->position;
        $komen->username        = Auth::user()->username;
        $komen->idTransaksi     = $data->id;
        $komen->transaksi       = 'Kontrak';
        $komen->status          = 'Approve';
        $komen->save();

        return $komen;
    }

    public function approveChatKontrak($data)
    {
        $dataApprove = DataFlow::where([
                        ['username', Auth::user()->username],
                        ['transaksi', 'Kontrak'],
                        ['unit',  'e-Commerce'],
                    ])->first();

        $komen                  = new Chat();
        $komen->chat            = $data['chat'];
        $komen->queue           = $dataApprove->queue;
        $komen->jabatan         = Auth::user()->position;
        $komen->username        = Auth::user()->username;
        $komen->idTransaksi     = $data['idTransaksi'];
        $komen->transaksi       = 'Kontrak';
        $komen->status          = $data['status'];
        $komen->save();

        return $komen;
    }
}
