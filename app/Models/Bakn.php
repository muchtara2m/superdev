<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Bakn extends Model implements Auditable
{
    use LogsActivity,\OwenIt\Auditing\Auditable;
    protected static $logAttributes = ['spph.nomorspph', 'spph.judul'];
    protected $guarded = [];

    //
    protected $fillable = [
        'title',
        'file',
        'tgl_upload'
    ];

    public function spph()
    {
        return $this->belongsTo('App\Models\Spph','spph_id');
    }
    public function io()
    {
        return $this->belongsTo('App\Models\Iodesc','io_id');
    }
    public function user(){
        return $this->belongsTo('App\User','created_by');
    }
    public function sp3s()
    {
        return $this->hasMany('App\Models\Sp3','id');
    }
    public function spks()
    {
        return $this->hasMany('App\Models\Spk','bakn_id');
    }
    public function spksnon()
    {
        return $this->hasMany('App\Models\SpkNon','id');
    }
    public function kontraks()
    {
        return $this->hasMany('App\Models\Kontrak','bakn_id');
    }
    public function kontraksnon()
    {
        return $this->hasMany('App\Models\KontrakNon','bakn_id');
    }
    public function chatbakns()
    {
        return $this->hasMany('App\Models\ChatBakn','idBakn');
    }


    // list query in menu kontrak 

    
    // list query in menu bakn
}
