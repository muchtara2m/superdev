<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class PlanBast extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    protected $connection = 'mysql2';
    protected $table = 'plan_bast';
    // One to many PlanBast to Ubis
    public function planbast_ubis()
    {
        return $this->belongsTo('App\Models\Ubis', 'ubis');
    }
}
