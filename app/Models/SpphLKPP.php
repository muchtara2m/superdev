<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use OwenIt\Auditing\Contracts\Auditable;

class SpphLKPP extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;

    protected static $logAttributes = [
        'nomorspph', 
        'judul', 
        'tglspph', 
        'nomorsph', 
        'tglsph', 
        'pic',
        'dari',
        'tembusan',
        'mitra',
        'title_lampiran',
        'title','created_by'];

    protected $table    = 'spph_lkpps';
    
    protected $guarded  = [];

    // Function Reletion
    public function mitra_lkpps()
    {
        return $this->belongsTo('App\Models\Mitra', 'mitra');
    }
    public function bakn_lkpps()
    {
        return $this->hasOne("App\Models\BaknLKPP",'spph_id');
    }
    public function bak_lkpps()
    {
        return $this->hasOne("App\Models\BakLkpp",'spph_id');
    }
    public function creator_spph_lkpp()
    {
        return $this->belongsTo('App\User','created_by');
    }

    // Function get all data just for admin
    public function scopeAllData($month, $year)
    {
        if(!$month || !$year){
            $month = date('m');
            $year = date('Y');
        }

        $data = SpphLKPP::with(
                [
                    'mitra_lkpps',
                    'creator_spph_lkpp',
                    'bakn_lkpps',
                    'bakn_lkpps.kontrak_lkpp'
                ])
                ->orderBy('updated_at','des');
       
        // condition select all
        if($month != 13 ){
            $data->whereMonth('created_at',$month)
                ->whereYear('created_at',$year);
        }

        return $data;
    }

    // Function get all data status = draft spph
    public function scopeDraftSpph($month, $year)
    {
        if(!$month || !$year){
            $month = date('m');
            $year = date('Y');
        }
      
        $data = SpphLKPP::with(['mitra_lkpps','creator_spph_lkpp'])
                ->where('status','draft_spph')
                ->orderBy('updated_at','des');

        // condition select all
        if($month != 13 ){
            $data->whereMonth('created_at',$month)->whereYear('created_at',$year);
        }

        if(Auth::user()->level == 'administrator'){
            return $data;
        }else{
            return $data->where('created_by', Auth::user()->id);
        }
    }

    // Function get all data status = save spph
    public function scopeListSpph($month, $year)
    {
        if(!$month || !$year){
            $month = date('m');
            $year = date('Y');
        }

        $data = SpphLKPP::with(['mitra_lkpps','creator_spph_lkpp'])
                ->where('status','save_spph')
                ->orderBy('updated_at','des');
        // condition select all
        if($month != 13 ){
            $data->whereMonth('created_at',$month)->whereYear('created_at',$year);
        }
        if(Auth::user()->level == 'administrator'){
            return $data;
        }else{
            return $data->where('created_by', Auth::user()->id);
        }
    }

    // Function get all data status = done spph
    public function scopeDoneSpph($month, $year)
    {
        if(!$month || !$year){
            $month = date('m');
            $year = date('Y');
        }
     
        $data = SpphLKPP::with([
                    'mitra_lkpps',
                    'creator_spph_lkpp',
                    'bakn_lkpps',
                    'bakn_lkpps.kontrak_lkpp'
                    ])
                ->where('status','done_spph')
                ->orderBy('updated_at','des');

        // condition select all
        if($month != 13 ){
            $data->whereMonth('created_at',$month)->whereYear('created_at',$year);
        }
        
        if(Auth::user()->level == 'administrator'){
            return $data;
        }else{
            return $data->where('created_by', Auth::user()->id);
        }
    
    }

    // function get data for BAK and BAKN
    public function scopeNotDraft()
    {
        $data = SpphLKPP::with(['mitra_lkpps','creator_spph_lkpp'])
                ->where('status','!=','draft_spph')
                ->orderBy('updated_at','desc');
        return $data;

    }

    // Function get data for create and edit 
    public function scopeTembusan()
    {  
        $tembusan = User::where(function($position){
                        $position->where('position','like','gm%')
                        ->orWhere('position','like','vp%')
                        ->orWhere('position','like','avp%')
                        ->orWhere('position','like','mgr%');
                    })->where(function($unit){
                        $unit->where('id_unit',44)
                        ->orWhere('id_unit',45)
                        ->orWhere('id_unit', 46)
                        ->orWhere('id_unit', 47)
                        ->orWhere('id_unit', 48)
                        ->orWhere('id_unit', 49)
                        ->orWhere('id_unit', 54)
                        ->orWhere('id_unit', 55)
                        ->orWhere('id_unit', 56)
                        ->orWhere('id_unit', 38);
                    })
                    ->get();
        return $tembusan;
    }

    public function insertSpphLkpp($data)
    {
        // declare Model
        $spph = new SpphLKPP();
        // Insert Data 
        $spph->nomorspph    = $data['nomorspph'];
        $spph->tglspph      = $data['tglspph'];
        $spph->tglsph       = $data['tglsph'];
        $spph->mitra        = $data['kepada'];      //mitra
        $spph->dari         = $data['dari'];        //penanda tangan
        $spph->pic          = Auth::user()->name;   //pic = user yang buat
        $spph->judul        = $data['judul'];   
        $spph->status       = $data['status'];      //status dokumen
        $spph->isi          = $data['isi'];         //isi text
        $spph->created_by   = Auth::user()->id;
        $spph->save();  //save for getting id 
        
        // Get id after insert
        $pathLampiran = "public/files/SPPH_LKPP/$spph->id/lampiran";
        // Upload Lampiran
        if($data['lampiran'] != null){
            foreach($data['lampiran'] as $file){
                $name       = $file->getClientOriginalName();
                $file->storeAs($pathLampiran, $name);
                $namanya[]  = $name;
                $lampiran[] = $pathLampiran.'/'.$name;
            }
        }
        $spph->lampiran        = $data['lampiran'] != null ? json_encode($lampiran) : NULL;
        $spph->title_lampiran  = $data['lampiran'] != null ? json_encode($namanya) : NULL;
        $spph->tgl_lampiran    = $data['lampiran'] != null ? date('Y-m-d H:i:s') : NULL;
        $spph->save();

        return $spph;
    }

    public function updateSpphLkpp($data, $model)
    {
         // Insert Data 
         $model->nomorspph    = $data['nomorspph'];
         $model->tglspph      = $data['tglspph'];
         $model->tglsph       = $data['tglsph'];
         $model->mitra        = $data['kepada'];      //mitra
         $model->dari         = $data['dari'];        //penanda tangan
         $model->judul        = $data['judul'];   
         $model->status       = $data['status'];      //status dokumen
         $model->isi          = $data['isi'];  
         
         // Get id after insert
         $pathLampiran = "public/files/SPPH_LKPP/$model->id/lampiran";
         // Upload Lampiran
         if($data['lampiran'] != null){
             foreach($data['lampiran'] as $file){
                 $name       = $file->getClientOriginalName();
                 $file->storeAs($pathLampiran, $name);
                 $namanya[]  = $name;
                 $lampiran[] = $pathLampiran.'/'.$name;
             }
         }
         $model->lampiran        = $data['lampiran'] != null ? json_encode($lampiran) : $model->lampiran;
         $model->title_lampiran  = $data['lampiran'] != null ? json_encode($namanya) : $model->title_lampiran;
         $model->tgl_lampiran    = $data['lampiran'] != null ? date('Y-m-d H:i:s') : $model->tgl_lampiran;
         $model->save();
         return $model;
    }

    // Function delete data and storage spph
    public function deleteSpph($id)
    {
        $data = SpphLKPP::with('creator_spph_lkpp','creator_spph_lkpp.unitnya')
                ->where('id',$id)
                ->first();
        $path = "public/files/SPPH_LKPP/".$data->creator_spph_lkpp->unitnya['nama']."/".$id;
        Storage::deleteDirectory($path);
        $data->delete();
        return response($data);
    }

    public function uploadFile($data, $model)
    {
        $pathLampiran = "public/files/SPPH_LKPP/$model->id/files";
        // Upload Lampiran
        if($data['file'] != null){
            foreach($data['file'] as $file){
                $name       = $file->getClientOriginalName();
                $file->storeAs($pathLampiran, $name);
                $namanya[]  = $name;
                $lampiran[] = $pathLampiran.'/'.$name;
            }
        }
        $model->file         = $data['file']        != null ? json_encode($lampiran) : $model->file;
        $model->title        = $data['file']        != null ? json_encode($namanya) : $model->title;
        $model->upload       = $data['file']        != null ? date('Y-m-d H:i:s') : $model->upload;
        $model->nomorsph     = $data['nomorsph']    != null ? $data['nomorsph'] : $model->nomorsph;
        $model->status       = 'done_spph';
        $model->save();
        return $model;
    }

    public function uploadLampiran($data, $model)
    {
        $pathLampiran = "public/files/SPPH_LKPP/$model->id/lampiran";
        // Upload Lampiran
        if($data['lampiran'] != null){
            foreach($data['lampiran'] as $file){
                $name       = $file->getClientOriginalName();
                $file->storeAs($pathLampiran, $name);
                $namanya[]  = $name;
                $lampiran[] = $pathLampiran.'/'.$name;
            }
        }
        $model->lampiran        = $data['lampiran'] != null ? json_encode($lampiran) : NULL;
        $model->title_lampiran  = $data['lampiran'] != null ? json_encode($namanya) : NULL;
        $model->tgl_lampiran    = $data['lampiran'] != null ? date('Y-m-d H:i:s') : NULL;

        $model->save();

        return $model;
    }



}