<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class IoPipeline extends Model
{
    use LogsActivity;
    
    //
    protected $connection = 'mysql2';
    protected $table = 't_io';
    protected $primaryKey = 'no_io';
    protected $guarded = [];


    public function inisasiPip(){
        return $this->hasMany('App\Models\Inisiasi','no_io');
    }
    public function ioSpkPksVendor(){
        return $this->hasMany('App\Models\SpkPksVendor','no_io');
    }
    public function ioSpkPksCustomer(){
        return $this->hasMany('App\Models\SpkPksCustomer','no_io');
    }
}
