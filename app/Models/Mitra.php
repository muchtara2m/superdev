<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class Mitra extends Model implements Auditable
{
    use LogsActivity, \OwenIt\Auditing\Auditable;
    
    // protected $table = 'mitras';
    protected $guarded = [];
    
    public function spph(){
        return $this->hasMany('App\Models\Spph','mitra');
    }

    public function spph_lkpps(){
        return $this->hasMany('App\Models\SpphLKPP','mitra');
    }

    public function ar(){
        return $this->hasMany('App\Models\AR','customer_id');
    }
}
