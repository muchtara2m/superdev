<?php

namespace App\Foundation;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use OwenIt\Auditing\Facades\Auditor;
trait BedoyAuthUsers
{
    use RedirectsUsers, ThrottlesLogins;
    
   
    public function showLoginForm()
    {
        return view('modules.auth.login_2');
    }
    
   
    public function login(Request $request)
    {
        $this->validateLogin($request);
        
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            
            return $this->sendLockoutResponse($request);
        }
        
        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }
        
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);
        
        return $this->sendFailedLoginResponse($request);
    }
    
  
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
            ]);
    }
        
      
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }
    
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }
    

    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        
        $this->clearLoginAttempts($request);
        $this->logsActivityLogin($request);
        return $this->authenticated($request, $this->guard()->user())
        ?: redirect()->intended($this->redirectPath());
    }
    
    public function logsActivityLogin(Request $request)
    {
        $activity = activity()
        ->causedBy(\Auth::user())
        ->withProperties(['attributes'=>$request->input('username')])
        ->log('Login');
        
        return true;
        
    }
    

    protected function authenticated(Request $request, $user)
    {
        //
    }
    

    protected function sendFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
            ]);
        }
        
        public function username()
        {
            return 'username';
        }
        
        public function logout(Request $request)
        {
            $this->guard()->logout();
            
            $request->session()->invalidate();
            
            return $this->loggedOut($request) ?: redirect('/');
        }
        
    
        protected function loggedOut(Request $request)
        {
            //
        }
        

        protected function guard()
        {
            return Auth::guard();
        }
}
        