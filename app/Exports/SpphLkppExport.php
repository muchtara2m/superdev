<?php

namespace App\Exports;

use App\Models\SpphLKPP;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class SpphLkppExport implements FromQuery, WithMapping, WithHeadings
{

    public function query()
    {
       return SpphLKPP::with(['creator_spph_lkpp','mitra_lkpps']);
    }

    public function map($data): array
    {
        return [
            $data->nomorspph,
            $data->tglspph,
            $data->judul,
            $data->mitra_lkpps['perusahaan'],
            $data->nomorsph == null ? 'Belum Diinput':$data->nomorsph,
            $data->tglsph,
            $data->pic,
            $data->tembusan,
            $data->nilai_project == null ? "Belum Diinput" : $data->nilai_projecg,
            $data->status = str_replace("_spph","",$data->status),
            $data->creator_spph_lkpp->name,
            $data->created_at,
        ];
    }

    public function headings(): array
    {
        return [
            'Nomor SPPH',
            'Tanggal SPPH',
            'Judul',
            'Mitra',
            'Nomor SPH',
            'Tanggal SPH',
            'PIC',
            'Tembusan',
            'Nilai Project',
            'Status',
            'Pembuat',
            'Tanggal Buat',
        ];
    }

}
