<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Activitylog\Traits\LogsActivity;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements Auditable
{
    use Notifiable, HasRoles, LogsActivity,  \OwenIt\Auditing\Auditable;

    protected $guarded = [];
    protected $fillable = [
        'name','username','email','password','level','employee_status','last_sign_in','current_sign_in_at',
    ];

    protected static $logAttributes = [
        'name','username','email','level','position','phone','last_sign_in','current_sign_in_at'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function creator_spph()
    {
        return $this->hasMany('App\Models\Spph', 'created_by');
    }

    public function creator_spph_lkpp()
    {
        return $this->hasMany('App\Models\SpphLKPP', 'created_by');
    }

    public function creator_bakn()
    {
        return $this->hasMany('App\Models\Bakn', 'created_by');
    }

    public function creator_bakn_lkpp()
    {
        return $this->hasMany('App\Models\BaknLKPP', 'created_by');
    }

    public function creator_spk()
    {
        return $this->hasMany('App\Models\Spk','created_by');
    }

    public function creator_kontrak()
    {
        return $this->hasMany('App\Models\Kontrak','created_by');
    }

    public function creator_kontraknon()
    {
        return $this->hasMany('App\Models\KontrakNon','created_by');
    }

    public function creator_sp3()
    {
        return $this->hasMany('App\Models\Sp3','created_by');
    }

    public function creator_ar()
    {
        return $this->hasMany('App\Models\AR','created_by');
    }

    public function unitnya()
    {
        return $this->belongsTo('App\Models\Unit', 'id_unit');
    }

    public function creator_chatnon()
    {
        return $this->belongsTo('App\Models\ChatNon','id_user');
    }

    public function creator_bak_lkpp()
    {
        return $this->hasMany('App\Models\BakLkpp','created_by');
    }

    public function creator_chat_bakn_lkpp(){
        return $this->hasMany('App\Models\ChatBaknLkpp','username');
    }
    
    public function creator_chat_bak_lkpp(){
        return $this->hasMany('App\Models\ChatBakLkpp','username');
    }

}
