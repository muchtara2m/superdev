<?php
namespace App\Http\Controllers;

use App\Models\BaknLKPP;
use App\Models\Chat;
use App\Models\Pasal;
use App\Models\JenisPasal;
use App\Models\Kontrak;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class KontrakController extends Controller
{
    public function create($id){
        $bakns = BaknLKPP::find($id);
        $pasals = Pasal::all();
        $jenis = JenisPasal::all();
        $jenispasal = DB::table('jenis_pasals')->join('bakns','bakns.jenis_kontrak','jenis_pasals.jenis_pasal')->first();
        $end = DB::table('data_flows')
        ->select('jabatan','data_flows.username','queue','users.name','users.position')
        ->join('users','users.username','=','data_flows.username')
        ->where([
            ['min','<=',$bakns->harga],
            ['max','>=',$bakns->harga],
            ['transaksi', 'Kontrak'],
            ['unit', 'e-Commerce']
            ])->first();
        return view('modules.kontrak.create',compact('bakns','end','pasals','jenispasal','jenis'));
    }

    public function jenisPasal(Request $request){
        if($request->ajax()){
            $jenis = JenisPasal::where('id', $request->id)->first();
            return response()->json(['options'=>$jenis]);
         }
    }
        
    public function list_create(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
            $var = '>=';
            $bln = $request->bulan;
            }else{
            $var = '=';
            $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        
        $bakn = BaknLKPP::where('status','!=','draft_bakn')
        ->doesntHave('kontrak_lkpp') 
        ->whereMonth('created_at',$bln)                            
        ->whereYear('created_at',$thn)
        ->orderBy('created_at','desc')->get();
       
        return view('modules.kontrak.list',compact('bakn'));
    }
        
    public function store(Request $request){
        $message = array(
            'required' => ':Attribute field is required',
            'unique' => ':Attribute sudah pernah dibuat',
        );
        $this->validate($request, [
            'nokontrak' => 'required',
        ],$message);
        DB::beginTransaction(); 
        try{
            $kontrak = new Kontrak();
            $kontrak->bakn_id = $request->input('baknid');
            $kontrak->nomor_kontrak = $request->input('nokontrak');
            $kontrak->tanggal_kontrak = $request->input('tglkontrak');
            $kontrak->jenis_kontrak = $request->input('jenis_kontrak');
            $kontrak->created_by = Auth::user()->id;
            $kontrak->status = $request->input('status');
            $kontrak->isi = $request->input('isi');
            // dd($kontrak);
            $kontrak->save();
            if($kontrak->status == 'save_kontrak'){
                $this->validate($request, [
                    'lampiran'=>'required',
                    'chat' => 'required',
                    'nokontrak' => 'required',
                    'nokontrak' => 'unique:kontrak_lkpp,nomor_kontrak',
                    'tglkontrak' => 'required',
                ],$message);
                try{
                    $path= "public/files/Kontrak_LKPP/".$kontrak->id."/lampiran";
                    if($request->file('lampiran') != NULL)
                    {
                    foreach($request->file('lampiran') as $file)
                        {
                            $name=$file->getClientOriginalName();
                            $file->storeAs($path, $name);
                            $namanya[] = $name;
                            $data[] = $path.'/'.$name;
                        }
                        $file = Kontrak::find($kontrak->id);
                        $file->lampiran = json_encode($data);
                        $file->title_lampiran = json_encode($namanya);
                        $file->tgl_lampiran = date('Y-m-d');
                        $file->save();
                    }
                    // insert chat admin
                    $komen = new Chat();
                    $komen->chat = $request->get('chat');
                    $komen->queue = 0;
                    $komen->jabatan = Auth::user()->position;
                    $komen->username = Auth::user()->username;
                    $komen->idTransaksi = $kontrak->id;
                    $komen->transaksi = 'Kontrak';
                    $komen->status = 'Approve';
                    $komen->save();
                    $this->approval($kontrak->id);

                    DB::commit();
                } catch (ValidationException $e) {
                    DB::rollback();
                    return redirect()->back()
                    ->withErrors("Something wrong with your form , please check carefully")
                    ->withInput();
                } catch (\Exception $e) {
                    DB::rollback();
                    return redirect()->back()
                    ->withErrors("Something wrong from the server, please check carefully")
                    ->withInput();
                }
                return redirect('status-transaksi-kontrak-lkpp')->with('success', 'File Kontrak with No. ('.$kontrak->nomor_kontrak.') was added successfully');
            }else{
                DB::commit();
            return redirect('draft-kontrak-lkpp')->with('success', 'File Kontrak with No. ('.$kontrak->nomor_kontrak.') was added successfully');
                
            }
        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong with your form , please check carefully")
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong from the server, please check carefully")
            ->withInput();
        }
    }
      
    public function edit($id){
        $kontraks = Kontrak::find($id);
        $pasals = Pasal::all();
        $jenis = JenisPasal::all();
        $chat = Chat::where('idTransaksi',$id)->orderBy('created_at','desc')->first();
        return view('modules.kontrak.edit',compact('kontraks','jenis','pasals','chat'));
    }

    public function update(Request $request,$id){
        $message = array(
            'required' => ':Attribute field is required'
        );
       
        DB::beginTransaction(); 
            $kontrak = Kontrak::find($id);
            $kontrak->nomor_kontrak = $request->input('nokontrak');
            $kontrak->tanggal_kontrak = $request->input('tglkontrak');
            $kontrak->jenis_kontrak = $request->input('jenis_kontrak');
            $kontrak->created_by = Auth::user()->id;
            $kontrak->status = $request->input('status');
            $kontrak->isi = $request->input('isi');
            $kontrak->save();
            if ($kontrak->status == 'save_kontrak') {
                $this->validate($request, [
                    'lampiran'=>'required',
                    'chat' => 'required',
                    'nokontrak' => 'required',
                    'tglkontrak' => 'required',
                ], $message);
                try{

                $path= "public/files/Kontrak_LKPP/".$id."/lampiran";
                if($request->file('lampiran') != NULL)
                {
                   foreach($request->file('lampiran') as $file)
                      {
                         $name=$file->getClientOriginalName();
                         $file->storeAs($path, $name);
                         $namanya[] = $name;
                         $data[] = $path.'/'.$name;
                      }
                      $file = Kontrak::find($kontrak->id);
                      $file->lampiran = json_encode($data);
                      $file->title_lampiran = json_encode($namanya);
                      $file->tgl_lampiran = date('Y-m-d');
                      $file->save();
                }
                // insert chat admin
                $komen = new Chat();
                $komen->chat = $request->input('chat');
                $komen->queue = 0;
                $komen->jabatan = Auth::user()->position;
                $komen->username = Auth::user()->username;
                $komen->idTransaksi = $kontrak->id;
                $komen->transaksi = 'Kontrak';
                $komen->status = 'Approve';
                $komen->save();
                
                $this->approval($kontrak->id);
                DB::commit();
            } catch (ValidationException $e) {
                DB::rollback();
                return redirect()->back()
                ->withErrors("Something wrong with your form , please check carefully")
                ->withInput();
             } catch (\Exception $e) {
                DB::rollback();
                return redirect()->back()
                ->withErrors("Something wrong from the server, please check carefully")
                ->withInput();
             }
             return redirect('status-transaksi-kontrak-lkpp')->with('success', 'File Kontrak with No. ('.$kontrak->nomor_kontrak.') was added successfully');

        }else{
            DB::commit();
            return redirect('draft-kontrak-lkpp')->with('success', 'File Kontrak with No. ('.$kontrak->nomor_kontrak.') was added successfully');
        }
        
    }

    public function approval($id){
        $kontrak=Kontrak::find($id);
        $bakn =BaknLKPP::find($kontrak->bakn_id);
        $end = DB::table('data_flows')
            ->select('jabatan','username','queue')
            ->where([
                ['min','<=',$bakn->harga],
                ['max','>=',$bakn->harga],
                ['transaksi', 'Kontrak'],
                ['unit', 'e-Commerce']
                ])->first();
        if(Auth::user()->level == 'adminlkpp'){
            $awal = DB::table('data_flows')
            ->select('jabatan','username','queue')
            ->where([
                ['transaksi', 'Kontrak'],
                ['unit', 'e-Commerce']
                ])
                ->orderBy('queue','asc')      
            ->first();
        }else{
            $awal = DB::table('data_flows')
            ->select('jabatan','username','queue')
            ->where([
                ['queue',2],
                ['transaksi', 'Kontrak'],
                ['unit', 'e-Commerce']
                ])   
            ->first();
        }
        $insert = DB::table('kontrak_lkpps')
        ->where('id',$id)
        ->update(
        [
            'approval' => $awal->username,
            'endapproval' =>$end->username
        ]);
        $this->notifikasi($id,$awal->username);
    }

    public function notifikasi($id, $name){
        $kontrak = Kontrak::find($id);
        $lastnya = Chat::where('idTransaksi',$id)
        ->select('chats.*','users.name')
        ->join('users','users.username','chats.username')
        ->orderBy('created_at','desc')->first();
        $usernya = User::where('username',$name)->first();
        // function shorten url
        if ($kontrak->approval == 'CLOSED') {
            $hp = substr(str_replace("-","",$usernya->phone),6,13);
            $urls = url('list-kontrak-lkpp');
            $body = 'Bapak/Ibu *'.$usernya->name.'*'
            ."\xA".'File Kontrak dengan Nomor '
            ."\xA".'*'.$kontrak->nomor_kontrak.'*'
            ."\xA".'Sudah Selesai di Approve, silahkan askes pada link berikut['
            ."\xA".$urls;
        }else if($kontrak->approval == 'Return'){
            $hp = substr(str_replace("-","",$usernya->phone),6,13);
            $urls = url('status-transaksi-kontrak-lkpp');
            $body = 'Bapak/Ibu *'.$usernya->name.'*'
            ."\xA".'File Kontrak dengan Nomor'
            ."\xA".'*'.$kontrak->nomor_kontrak.'*'
            ."\xA".'Di Return oleh *'. strtoupper($lastnya->name).'*'
            ."\xA".'Dengan Catatan '
            ."\xA".'" *_'.$lastnya->chat.'_* "'
            ."\xA".'Silahkan akses pada link berikut'
            ."\xA".$urls;
        }else{
            $hp = substr(str_replace("-","",$usernya->phone),6,13);
            $urls = url('preview-status-kontrak-lkpp/'.$id);
            $body = 'Bapak/Ibu *'.$usernya->name.'*'
            ."\xA".'File Kontrak dengan Nomor'
            ."\xA".'*'.$kontrak->nomor_kontrak.'*'
            ."\xA".'Membutuhkan Approve Anda, silahkan akses pada link berikut '
            ."\xA".$urls;
       } 
    //    $hp = '81280295238';
       $data = [
           'phone' => '62'.$hp, // Receivers phone
           'body' => $body,
       ];
       $json = json_encode($data); // Encode data to JSON
       // URL for request POST /message
       $url =env('API_MAIL');
       // Make a POST request
       $options = stream_context_create(['http' => [
               'method'  => 'POST',
               'header'  => 'Content-type: application/json',
               'content' => $json
           ]
       ]);
       // Send a request
       $result = file_get_contents($url, false, $options);
       $var = json_decode($result, true);
       if($var['sent'] == 'true'){
           return $this;                
       }else{
           return redirect()->back()->with('error', 'Notifikasi (+62'.$hp.') tidak terdaftar ');
       }
   }

    public function chatApproval(Request $request){
        $flow =DB::table('data_flows')
        ->where('username', Auth::user()->username)
        ->first();
        DB::beginTransaction();
        try{
        $komen = new Chat();
        $komen->chat = $request->get('chat');
        $komen->queue = $flow->queue;
        $komen->jabatan = Auth::user()->position;
        $komen->username = Auth::user()->username;
        $komen->idTransaksi = $request->get('idTransaksi');
        $komen->transaksi = 'Kontrak';
        $komen->status = $request->get('status');
        $komen->save();
        if ($komen->status == 'Approve') {
            $idKontrak = $request->get('idKontrak');
            $kontrak = Kontrak::find($idKontrak);
            $unit = 'e-Commerce';
            $trx = 'Kontrak';
            $skrg = $flow->queue;
            $jabatanskrg = $kontrak->approval;
            $jabatanend = $kontrak->endapproval;
            // bandingin antara approval dengan approval akhir
            if ($jabatanskrg != $jabatanend) {
            $skrg++;
            $cek = DB::table('data_flows')
            ->select('jabatan','username','queue')
            ->where([
                ['queue', $skrg],
                ['transaksi', $trx],
                ['unit', $unit]
            ])->first();
            $updateapproval = DB::table('kontrak_lkpps')
            ->where('id', $idKontrak)
            ->update(
                ['approval' => $cek->username]
            );
            $this->notifikasi($idKontrak,$cek->username);
            DB::commit();
            }else {
            // status CLOSED dengan nilai 9
            $updateapproval = DB::table('kontrak_lkpps')
            ->where('id', $idKontrak)
            ->update(
                ['approval' => 'CLOSED']
            );
            $this->notifikasi($idKontrak,$kontrak->user_kontrak_lkpp['username']);
            DB::commit();
            }
            return redirect('inprogress-kontrak-lkpp')->with('success','Kontrak with No.  ('.$kontrak->nomor_kontrak.') has been Approved by '.Auth::user()->name);
        }else {
            // jika komen return
            $idKontrak = $request->get('idKontrak');
            $kontrak = Kontrak::find($idKontrak);
            $updatestatus = DB::table('kontrak_lkpps')
            ->where('id', $idKontrak)
            ->update(['approval' =>'Return'] );
            $name = User::where('id',$kontrak->created_by)->first();
            $this->notifikasi($idKontrak,$name->username);
            DB::commit();
            return redirect('inprogress-kontrak-lkpp')->with('success','Kontrak with No.  ('.$kontrak->nomor_kontrak.') has been Return by '.Auth::user()->name);
            }
        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong with your form, please check carefully")
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong from the server, please check carefully")
            ->withInput();
        }
    }

   public function draft(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
            $var = '>=';
            $bln = $request->bulan;
            }else{
            $var = '=';
            $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator'){
            $kontrak = Kontrak::where('status','draft_kontrak') 
            ->whereMonth('created_at',$bln)                            
            ->whereYear('created_at',$thn)
            ->orderBy('updated_at','desc')
            ->get();
        }else{
            $kontrak = Kontrak::where('status','draft_kontrak')->where('created_by',Auth::user()->id)
            ->whereMonth('created_at',$bln)                            
            ->whereYear('created_at',$thn)
            ->orderBy('updated_at','desc')
            ->get();
        }
       
        return view('modules.kontrak.draft',compact('kontrak'));
   }

   public function upload_file(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
            $var = '>=';
            $bln = $request->bulan;
            }else{
            $var = '=';
            $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator'){
            $kontrak = Kontrak::where('approval','CLOSED')->where('file',NULL)->whereMonth('created_at', $bln)->whereYear('created_at',$thn)->orderBy('created_at','desc')->get();
        }else{
            $kontrak = Kontrak::where('approval','CLOSED')->where('file',NULL)->whereMonth('created_at', $bln)->whereYear('created_at',$thn)->where('created_by',Auth::user()->id)->orderBy('created_at','desc')->get();
        }
        return view('modules.kontrak.upload',compact('kontrak'));
   }

   public function status_transaksi(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
            $var = '>=';
            $bln = $request->bulan;
            }else{
            $var = '=';
            $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator'){
            $kontrak = Kontrak::where('approval','!=','CLOSED')
            ->with('bakn_lkpp','bakn_lkpp.spph_lkpps.mitra_lkpps')
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->orderBy('created_at','desc')
            ->get();
        }else{
            $kontrak = Kontrak::where(function($q){
            $q->where('approval',Auth::user()->username)
            ->where('created_by','!=',Auth::user()->id);
            })->orWhere(function($w){
            $w->where('approval','Return')
            ->where('created_by',Auth::user()->id);
            })
            ->with('bakn_lkpp','bakn_lkpp.spph_lkpps.mitra_lkpps')
            ->whereMonth('created_at',$var,$bln)                                          
            ->whereYear('created_at',$thn)
            ->orderBy('created_at','desc')              
            ->get();
        }
        return view('modules.kontrak.status_transaksi',compact('kontrak'));
   }

   public function inprogress(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
            $var = '>=';
            $bln = $request->bulan;
            }else{
            $var = '=';
            $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator' || Auth::user()->level != 'adminkpp' || Auth::user()->level == 'staff_bakn_lkpp'){
            $kontrak = Kontrak::with(['chatnya'=>function ($q){
                $q->select('chats.*','users.name')
                ->join('users','users.username','chats.username')
                ->orderBy('created_at','asc');
            },'bakn_lkpp','bakn_lkpp.spph_lkpps','user_kontrak_lkpp'])
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->orderBy('created_at','desc')
            ->get();
        }else{
            $kontrak = Kontrak::with(['chatnya'=>function ($q){
                $q->select('chats.*','users.name')
                ->join('users','users.username','chats.username')
                ->orderBy('created_at','asc');
            },'bakn_lkpp','bakn_lkpp.spph_lkpps','user_kontrak_lkpp'])
            ->where('created_by',Auth::user()->id)
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->orderBy('created_at','desc')
            ->get();
        }
        return view('modules.kontrak.inprogress',compact('kontrak'));
   }

   public function done(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
            $var = '>=';
            $bln = $request->bulan;
            }else{
            $var = '=';
            $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator'){
            $kontrak = Kontrak::where('status','done_kontrak')->orderBy('created_at','desc')->get();
        }else{
            $kontrak = Kontrak::where('status','done_kontrak')->where('created_by',Auth::user()->id)->orderBy('created_at','desc')->get();
        }
        return view('modules.kontrak.done',compact('kontrak'));
   }

   public function tracking(Request $request){
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
            $var = '>=';
            $bln = $request->bulan;
            }else{
            $var = '=';
            $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator' || Auth::user()->level != 'adminkpp' || Auth::user()->level == 'staff_bakn_lkpp'){
            $kontrak = Kontrak::with(['chatnya'=>function ($q){
                $q->select('chats.*','users.name')
                ->join('users','users.username','chats.username')
                ->orderBy('created_at','asc');
            },'bakn_lkpp','bakn_lkpp.spph_lkpps','user_kontrak_lkpp'])
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->orderBy('created_at','desc')
            ->get();
        }else{
            $kontrak = Kontrak::with(['chatnya'=>function ($q){
                $q->select('chats.*','users.name')
                ->join('users','users.username','chats.username')
                ->orderBy('created_at','asc');
            },'bakn_lkpp','bakn_lkpp.spph_lkpps','user_kontrak_lkpp'])
            ->where('created_by',Auth::user()->id)
            ->whereMonth('created_at',$var,$bln)                            
            ->whereYear('created_at',$thn)
            ->orderBy('created_at','desc')
            ->get();
        }
        return view('modules.kontrak.tracking',compact('kontrak'));
   }

   public function destroy($id){
        $kontrak = Kontrak::findOrFail($id); 
        $kontrak->delete();
        $chat = Chat::where('idTransaksi',$id)->delete();
        $lampiran= "public/files/Kontrak_LKPP/".$kontrak->id."/lampiran";
        $file= "public/files/Kontrak_LKPP/".$kontrak->id."/file";
        Storage::deleteDirectory($lampiran);
        Storage::deleteDirectory($file);
        return back()->with('success','Your Data has been successfully deleted');
   }

   public function file(Request $request,$id){
        $this->validate($request, [
            'file' => 'required',
            ]);
            $path= "public/files/Kontrak_LKPP/".$id."/file";
            if($request->file('file'))
            {
                foreach($request->file('file') as $file)
                {
                    $name=$file->getClientOriginalName();
                    $file->storeAs($path, $name);
                    $namanya[] = $name;
                    $data[] = $path.'/'.$name;
                }
            }
            $file = Kontrak::find($id);
            $file->upload = date("Y-m-d H:i:s");
            $file->status = "done_kontrak";
            $file->file=json_encode($data);
            $file->title = json_encode($namanya);
            $file->save();
            return back()->with('success', 'Your files has been successfully added');
   }

   public function preview($id){
       $kontrak = Kontrak::find($id);
       return view('modules.kontrak.preview',compact('kontrak'));
   }

   public function preview_status($id){
       $kontrak = Kontrak::find($id);
       $bakn = BaknLKPP::find($kontrak->bakn_id);
       $chats = DB::table('chats')
        ->join('users','chats.username','=','users.username')
        ->select('chat','jabatan','chats.username','users.name','chats.created_at')
        ->where('idTransaksi',$id)
        ->orderBy('chats.created_at','asc')
        ->get();
       $dirop = User::where('level','dirop')->first();
       $checkbox = explode(",", $bakn->tipe_rapat);
       $user = User::all();
    
       return view('modules.kontrak.preview_status',compact('bakn','kontrak','checkbox','chats'));
   }
   public function print_preview($id)
    {
        $kontrak = Kontrak::with('chatnya','user_kontrak_lkpp')->find($id);
        $get = DB::table('chats')
        ->where('status', 'Return')
        ->where('idTransaksi', $id)
        ->orderBy('created_at', 'desc')
        ->first();
        if($get ==NULL){
            $chat = DB::table('chats')
            ->join('users', 'users.username','chats.username')
            ->select('chats.*', 'users.name')
            ->where('idTransaksi',$id)
            ->where('transaksi', 'kontrak')
            ->where('status', 'Approve')
            ->orderBy('chats.created_at','asc')
            ->get();
        }else{
            $chat = DB::table('chats')
            ->join('users', 'users.username','chats.username')
            ->select('chats.*', 'users.name')
            ->where('idTransaksi',$id)
            ->where('transaksi', 'kontrak')
            ->where('status', 'Approve')
            ->where('chats.created_at','>',$get->created_at)
            ->orderBy('chats.created_at','asc')
            ->get();
        }
        $semua = DB::table('chats')
        ->join('users','users.username','=','chats.username')
        ->where('idTransaksi',$id)
        ->select('chats.*', 'users.name')
        ->orderBy('chats.created_at','asc')
        ->get();

        return view('modules.kontrak.print', compact('kontrak','chat','semua'));
    }
     // export to word
    public function word($id){
        $kontrak = Kontrak::find($id);
        $ygpentingbisa = $kontrak->isi;
        header("Content-Type: application/vnd.msword");
        header("Expires: 0");//no-cache
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");//no-cache
        header("content-disposition: attachment;filename=$kontrak->nomor_kontrak.doc");
        echo "<html>";
        echo "$ygpentingbisa";
        echo "</html>";
    }
}
    
