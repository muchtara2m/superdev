<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaknLkppRequest;
use App\Models\BakLkpp;
use App\Models\CaraBayar;
use App\Models\ChatBakLkpp;
use App\Models\RevisiBakLkpp;
use App\Models\SpphLKPP;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RevisiBakLkppController extends Controller
{
    protected $model;
    protected $baknLkpp;
    protected $chat;

    public function __construct()
    {
        $this->middleware('auth');
        $this->model    = new RevisiBakLkpp();
        $this->baknLkpp = new BakLkpp();
        $this->chat     = new ChatBakLkpp();
    }

    public function create(BakLkpp $bakLkpp)
    {
        $bayar = CaraBayar::all();
        return view('modules.bak_lkpp.revisi.create', compact('bakLkpp','bayar'));
    }
    
    public function store(Request $request, BakLkpp $bakLkpp)
    {
        $data = $request->all();
        $data['lampiran'] = $request->file('lampiran') != null ? $request->file('lampiran') : NULL;
        DB::beginTransaction();
         try{
 
             $insert = $this->model->insertRevisiBakLkpp($bakLkpp);
             $update = $this->baknLkpp->updateRevisi($data, $bakLkpp);
 
             if($request->status == 'save_bakn'){
                  // call fuction chat
                 $this->chat->insertChatRevisi($data, $bakLkpp);
                 // update approval 
                 $this->baknLkpp->getValueApproval($bakLkpp);
                 DB::commit();
                 return redirect('inprogress-bak-lkpp') ->with(
                    'success',
                    'Revisi BAK with No. ('.$update->nomor_bak.') was added successfully'
                 );
             }else{
                 DB::commit();
                 return redirect('draft-bak-lkpp') ->with(
                    'success',
                    'Revisi BAK with No. ('.$update->nomor_bak.') was added successfully'
                 );
             }
        }catch(Exception $e){
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
    
    public function show(RevisiBakLkpp $revisiBakLkpp)
    {
        $bakLkpp       = $revisiBakLkpp::with('revisi_bak_lkpps')->first();
        $spphLKPP   = SpphLKPP::where('nomorspph', $revisiBakLkpp->spph_id)->first();
        $user       = User::where('username',$revisiBakLkpp->created_by)->first();
        $checkbox         = explode(",", $revisiBakLkpp->tipe_rapat);
        $checkbox_mitra   = explode(",", $revisiBakLkpp->peserta_mitra);
        return view('modules.bak_lkpp.revisi.preview', compact('revisiBakLkpp','bakLkpp','spphLKPP','user','checkbox','checkbox_mitra'));
    }
}
