<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Spatie\Activitylog\Traits\LogsActivity;

class HomeController extends Controller
{
    use LogsActivity;
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        if (Auth::user()) {
        
            return redirect('home');
        }
        else
        return redirect('login');
    }
    public function notifikasi()
    {
        $notif= DB::table('kontraks')
        ->select('status_approval')
        ->where('status_approval', Auth::user()->position)
        ->count('status_approval');
        
        $list = DB::table('kontraks')
        ->select('judul','handler')
        ->where('status_approval', Auth::user()->position)
        ->get();
        
        return view('layouts.includes.header', compact('notif','list'));
    }
    public function listMenu(){
        $menu_pbs = array(
            array("Create PBS","pbs-create"),
            array("PBS","pbs"),
            array("Status Transaksi","pbs_st_trans"),
            array("Inprogress","pbs_inp"),
            array("Selesai","pbs_selesai"),
            array("Revisi Transaksi","pbs_re_trans"),
            array("Kalkulator Peminjaman","pbs_kal_pinj")
        );
        $menu_spph = array(
            array("Create SPPH","spph_create"),
            array("List SPPH","spph_list"),
            array("Draft SPPH","spph_draft"),
            array("Done SPPH","spph_done")
        );
        $menu_bakn = array(
            array("Create BAKN","bakn_create"),
            array("List BAKN","bakn_list"),
            array("Draft BAKN","bakn_draft"),
            array("Done BAKN","bakn_done")
        );
        $menu_spk = array(
            array("Create SP3/SPK","spk_create"),
            array("List SP3/SPK","spk_list"),
            array("Draft SP3/SPK","spk_draft"),
            array("Status Transaksi","spk_st_trans"),
            array("Inprogress SP3/SPK","spk_inp"),
            array("Done SP3/SPK","spk_done")
        );
        $menu_spk_non = array(
            array("Create SP3/SPK","spk_create_non"),
            array("List SP3/SPK","spk_list_non"),
            array("Draft SP3/SPK","spk_draft_non"),
            array("Status Transaksi","spk_st_trans_non"),
            array("Inprogress SP3/SPK","spk_inp_non"),
            array("Done SP3/SPK","spk_done_non")
        );
        $menu_kontrak = array(
            array("Kontrak","kontrak"),
            array("List Kontrak","kontrak_list"),
            array("Draft Kontrak","kontrak_draft"),
            array("Status Transaksi","kontrak_st_trans"),
            array("Inprogress Kontrak","kontrak_inp"),
            array("Done Kontrak","kontrak_done")
        );
        $menu_mdata = array(
            array("Data Flow","mdata_dflow"),
            array("Data Karyawan","mdata_dkaryawan"),
            array("Data Mitra","mdata_dmitra"),
            array("Data Pasal","mdata_dpasal"),
            array("Data Pimpinan Rapat","mdata_dpimrap"),
            array("Data Role","mdata_drole"),
            array("Data Unit","mdata_dunit"),
            array("Data Jenis Pasal","mdata_djenpas"),
            array("Data Cara Bayar","mdata_dcarbay")
        );
        $menu_ar = array(
            array("Dashboar AR","ar_dashboard"),
            array("Create","ar_create"),
            array("Update Nilai","ar_nilai"),
            array("Unbill","ar_unbill"),
            array("Bill","ar_bill"),
            array("Ready To Bill","ar_readytobill"),
            array("Paid","ar_paid"),
            array("Paid 100","ar_paid100"),
            array("Unbill SDV","unbill-sdv"),
            array("Unbill Operation","unbill-operation"),
            array("Unbill UBIS","unbill-ubis")
        );
        $menu_kontrak_non = array(
            array("Kontrak ","kontrak_non"),
            array("List Kontrak ","kontrak_non_list"),
            array("Draft Kontrak ","kontrak_non_draft"),
            array("Status Transaksi ","kontrak_non_st_trans"),
            array("Inprogress Kontrak ","kontrak_non_inprogress"),
            array("Upload File ","kontrak_non_upload"),
            array("Done Kontrak ","kontrak_non_done"),
            array("List Dispatch Kontrak ","kontrak_non_listdisp"),
            array("Tracking Document ","kontrak_non_track"),
        );
        return response()->json(['menu_pbs'=>$menu_pbs,'menu_bakn'=>$menu_bakn]);
        
        // return view('layouts.includes.sidebar', compact('menu_pbs', 'menu_spph', 'menu_bakn', 'menu_spk','menu_spk_non', 'menu_kontrak', 'menu_mdata','menu_ar','menu_kontrak_non'));
        // $data = array(
        //     'menu_kontrak_non' => $menu_kontrak_non,
        //     'menu_ar' => $menu_ar,
        // );
    }
}
