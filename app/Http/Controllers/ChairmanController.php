<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Chairman;
use Illuminate\Support\Facades\DB;

class ChairmanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $chairmen = Chairman::all();
        return view('modules.chairman.index', compact('chairmen'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.chairman.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $chairman = new Chairman();
        $chairman->jabatan = $request->get('jabatan');
        $chairman->nama = $request->get('nama');
        $chairman->created_by = $request->get('created_by');
        $chairman->save();

        return redirect()->route('chairman.index')->with('success', 'Data '.$chairman->nama.' telah ditambahkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $chairman = Chairman::find($id);
        return view('modules.chairman.edit', compact('chairman'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $chairman = Chairman::find($id);
        $chairman->jabatan = $request->get('jabatan');
        $chairman->nama = $request->get('nama');
        $chairman->save();

        return redirect()->route('chairman.index')->with('success', 'Data '.$chairman->nama.' berhasil diubah.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $chairman = Chairman::find($id);
        $chairman->delete();

        return redirect()->route('chairman.index')->with('success', 'Data '.$chairman->nama.' berhasil dihapus.');
    }
}
