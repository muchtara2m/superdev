<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pasal;
use DB;

class PasalController extends Controller
{
  
    public function index()
    {
        //
        $pasals=Pasal::all();
        return view('modules.pasal.index',compact('pasals'));
    }

 
    public function create()
    {
        //
        return view('modules.pasal.create');
    }

 
    public function store(Request $request)
    {
        //
        $pasal = new \App\Pasal();
        $pasal->pasal = $request->get('pasal');
        $pasal->judul = $request->get('judul');
        $pasal->isi = $request->get('isi');
        $pasal->save();

        return redirect('pasal')->with('success', 'Information has been added');
    }

  
    public function edit($id)
    {
        //
        $pasals = Pasal::find($id);
        return view('modules.pasal.edit',compact('pasals','id'));
    }

    public function update(Request $request, $id)
    {
        //
        $pasal= \App\Pasal::find($id);
        $pasal->pasal = $request->get('pasal');
        $pasal->judul = $request->get('judul');
        $pasal->isi = $request->get('isi');
        $pasal->save();

        return redirect('pasal')->with('success', 'Information has been edited');
    }

    public function destroy($id)
    {
        //
        $pasal = Pasal::find($id);
        $pasal->delete();

        return redirect('pasal')->with('success','Data has been  deleted');
    }
}
