<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;
use App\Models\Bakn;
use App\Models\Spph;
use App\Models\Iodesc;
use App\User;
use App\Models\JenisPasal;
use App\Models\CaraBayar;
use App\Models\ChatBakn;


class BaknController extends Controller
{
    function dataspph(Request $request){
        if($request->ajax()){
            $spphs = Spph::with('mitras')->where('nomorspph', $request->nomorspph)->first();
            return response()->json(['options'=>$spphs]);
        }
    }
    function caraBayar(Request $id){
        if($id->ajax()){
            $spphs = CaraBayar::where('id', $id->id)->first();
            return response()->json(['options'=>$spphs]);
        }
    }

    public function create(){
        $spph = Spph::where("status", "done_spph")->doesntHave('bakns')->latest()->get();
        $io = Iodesc::all();
        $jenispasal = JenisPasal::all();
        $bayar = CaraBayar::all();
        //query peserta pins
        $daftarPeserta = DB::select("SELECT * FROM `users` where  (position like 'mgr%' or position like 'avp%' or position like 'gm%' or position like 'vp%') AND id_unit=59 AND permanent_status='Organik' ");
        $peserta = '';
        foreach ($daftarPeserta as $daftar){
            $peserta .= $daftar->name.',';
        }
        return view('modules.bakn.create', compact('spph','io','jenispasal','daftarPeserta','peserta','bayar'));
    }

    public function store(Request $request){
        $customMessages = [
            'required' => ':Attribute field is required.',
            'unique' => ':Attribute has already taken.',
        ];
        $this->validate($request, array(
            'spph_id' => 'required',
            'spph_id' => 'unique:bakns,spph_id',
            'jenis_kontrak' => 'required',
        ),$customMessages);
        DB::beginTransaction();
        $bakn = new Bakn();
        // priority
        $bakn->spph_id = $request->input('spphid');
        $bakn->tglbakn = $request->input('tglbakn');
        $bakn->jenis_kontrak = $request->input('jenis_kontrak');
        $bakn->tipe_rapat = implode(",",$request->tipe_undangan);
        $bakn->pimpinan_rapat = $request->input('pimpinan_rapat');
        $bakn->peserta_pins = $request->input('peserta_pins');
        $bakn->peserta_mitra = $request->input('peserta_mitra');
        $bakn->harga = str_replace(".","",$request->input('harga'));
        $bakn->io_id = $request->input('io_id');
        // new fitur
        $bakn->start_date = $request->input('start_date');
        $bakn->end_date = $request->input('end_date');

        if(!empty($request->carabayar)){
            $bakn->carabayar = implode(",",$request->carabayar);
        }
        //pembahasan
        $bakn->agenda = $request->input('agenda');
        $bakn->dasar_pembahasan = $request->input('dasar_pembahasan');
        $bakn->ruang_lingkup = $request->input('ruang_lingkup');
        $bakn->lokasi_pekerjaan = $request->input('lokasi_pekerjaan');
        $bakn->jangka_waktu = $request->input('jangka_waktu');
        $bakn->harga_terbilang = $request->input('harga_terbilang');
        $bakn->cara_bayar = $request->input('cara_bayar');
        $bakn->lain_lain = $request->input('lain_lain');
        //
        $bakn->created_by = Auth::user()->id;
        $bakn->status = $request->input('status');
        $bakn->save();
        $path= "public/files/BAKN/General Support/$bakn->id/lampiran";
        if($request->file('lampiran') != null)
        {
            foreach($request->file('lampiran') as $file)
            {
                $name=$file->getClientOriginalName();
                $file->storeAs($path, $name);
                $namanya[] = $name;
                $data[] = $path.'/'.$name;
            }
            $file = Bakn::find($bakn->id);
            $file->lampiran = json_encode($data);
            $file->title_lampiran = json_encode($namanya);
            $file->tgl_lampiran = date('Y-m-d');
            $file->save();
        }
        if($bakn->status == 'save_bakn'){
            $this->validate($request, array(
                'tglbakn' => 'required',
                'peserta_mitra' => 'required',
                'harga' => 'required',
                'carabayar' => 'required',
                'agenda' => 'required',
                'dasar_pembahasan' => 'required',
                'ruang_lingkup' => 'required',
                'lokasi_pekerjaan' => 'required',
                'jangka_waktu' => 'required',
                'harga_terbilang' => 'required',
                'cara_bayar' => 'required',
                'lain_lain' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'chat' => 'required',
            ),$customMessages);
            try{
                $komen = new ChatBakn();
                $komen->chat = $request->get('chat');
                $komen->jabatan = Auth::user()->position;
                $komen->username = Auth::user()->username;
                $komen->idBakn = $bakn->id;
                $komen->transaksi = 'BAKN';
                $komen->status = 'Approve';
                $komen->save();
                $this->notifikasi($bakn->id);

                DB::commit();
                return redirect()->route('list.bakn')->with('success','Bakn has been successfully added');
            } catch (ValidationException $e) {
                DB::rollback();
                return redirect()->back()
                ->withErrors("Something wrong with your form , please check carefully")
                ->withInput();
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->back()
                ->withErrors("Something wrong from the server, please check carefully")
                ->withInput();
            }
        }else{
            DB::commit();
            return redirect()->route('draft.bakn')->with('success','Bakn has been successfully added');
        }
    }

    public function edit($id){
        $bakns = Bakn::find($id);
        $io = Iodesc::all();
        $jenispasal = JenisPasal::all();
        $bayar = CaraBayar::all();
        $chat = ChatBakn::where('idBakn', $id)->latest()->first();
        $checkbox = explode(",", $bakns->tipe_rapat);
        //query peserta pins
        $daftarPeserta = DB::select("SELECT * FROM `users` where  (position like 'mgr%' or position like 'avp%' or position like 'gm%' or position like 'vp%') AND id_unit=59" );
        $peserta = '';
        foreach ($daftarPeserta as $daftar){
            $peserta .= $daftar->name.',';
        }
        return view('modules.bakn.edit', compact('checkbox','bakns','chat','io','jenispasal','daftarPeserta','peserta','bayar'));
    }

    public function update(Request $request, $id){
        $customMessages = [
            'required' => ':Attribute field is required.',
        ];
        $bakn = Bakn::find($id);
        $bakn->tglbakn = $request->input('tglbakn');
        $bakn->jenis_kontrak = $request->input('jenis_kontrak');
        $bakn->tipe_rapat = implode(",",$request->tipe_undangan);
        $bakn->pimpinan_rapat = $request->input('pimpinan_rapat');
        $bakn->peserta_pins = $request->input('peserta_pins');
        $bakn->peserta_mitra = $request->input('peserta_mitra');
        $bakn->harga = str_replace(".","",$request->input('harga'));
        $bakn->io_id = $request->input('io_id');

         // new fitur
        $bakn->start_date = $request->input('start_date');
        $bakn->end_date = $request->input('end_date');
        
        if(!empty($request->carabayar)){
            $bakn->carabayar = implode(",",$request->carabayar);
        }
        //pembahasan
        $bakn->agenda = $request->input('agenda');
        $bakn->dasar_pembahasan = $request->input('dasar_pembahasan');
        $bakn->ruang_lingkup = $request->input('ruang_lingkup');
        $bakn->lokasi_pekerjaan = $request->input('lokasi_pekerjaan');
        $bakn->jangka_waktu = $request->input('jangka_waktu');
        $bakn->harga_terbilang = $request->input('harga_terbilang');
        $bakn->cara_bayar = $request->input('cara_bayar');
        $bakn->lain_lain = $request->input('lain_lain');
        //
        $bakn->created_by = Auth::user()->id;
        $bakn->status = $request->input('status');
        // input file
        $path= "public/files/BAKN/General Support/$id/lampiran";
        if($request->file('lampiran') != null)
        {
            foreach($request->file('lampiran') as $file)
            {
                $name=$file->getClientOriginalName();
                $file->storeAs($path, $name);
                $namanya[] = $name;
                $data[] = $path.'/'.$name;
            }
            $bakn->lampiran = json_encode($data);
            $bakn->title_lampiran = json_encode($namanya);
            $bakn->tgl_lampiran = date('Y-m-d');
        }
        $bakn->save();
        if($bakn->status == 'save_bakn'){
            $this->validate($request, array(
                'tglbakn' => 'required',
                'peserta_mitra' => 'required',
                'harga' => 'required',
                'agenda' => 'required',
                'dasar_pembahasan' => 'required',
                'ruang_lingkup' => 'required',
                'lokasi_pekerjaan' => 'required',
                'jangka_waktu' => 'required',
                'harga_terbilang' => 'required',
                'lain_lain' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'chat' => 'required',
            ),$customMessages);
            try{
                $komen = new ChatBakn();
                $komen->chat = $request->get('chat');
                $komen->jabatan = Auth::user()->position;
                $komen->username = Auth::user()->username;
                $komen->idBakn = $bakn->id;
                $komen->transaksi = 'BAKN';
                $komen->status = 'Approve';
                $komen->save();
                $this->notifikasi($bakn->id);

                DB::commit();
                return redirect()->route('list.bakn')->with('success','Bakn has been successfully added');
            } catch (ValidationException $e) {
                DB::rollback();
                return redirect()->back()
                ->withErrors("Something wrong with your form , please check carefully")
                ->withInput();
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->back()
                ->withErrors("Something wrong from the server, please check carefully")
                ->withInput();
            }
        }else{
            DB::commit();
            return redirect()->route('draft.bakn')->with('success','Bakn has been successfully added');
        }
    }

    public function preview($id){
        $chat = ChatBakn::where('idBakn',$id)->latest()->first();
        $bakn = Bakn::with(['user','io','spph.mitras'])->find($id);
        $checkbox = explode(",", $bakn->tipe_rapat);
        $handlers = User::where('id_unit', 59)->get();;
        return view('modules.bakn.preview',compact('chat','id','bakn','checkbox','handlers'));
    }

    public function upload(Request $request, $id){
        $this->validate($request, [
            'file' => 'required',
        ]);
            try{
                $path= "public/files/BAKN/General Support/$id/file";
                if($request->file('file'))
                {
                    foreach($request->file('file') as $file)
                    {
                        $name=$file->getClientOriginalName();
                        $file->storeAs($path, $name);
                        $namanya[] = $name;
                        $data[] = $path.'/'.$name;
                    }
                }
                DB::beginTransaction();
                $file = Bakn::find($id);
                $file->file=json_encode($data);
                $file->title = json_encode($namanya);
                $file->upload = date('Y-m-d H:i:s');
                $file->status = 'done_bakn';
                $file->save();
                $this->notifikasi($id);
                DB::commit();
            } catch (ValidationException $e) {
                DB::rollback();
                return redirect()->back()
                ->withErrors("Something wrong with your form , please check carefully")
                ->withInput();
            } catch (\Exception $e) {
                DB::rollback();
                return redirect()->back()
                ->withErrors("Something wrong from the server, please check carefully")
                ->withInput();
            }
        return redirect()->route('done.bakn')->with('success', 'Your files has been successfully added');
    }

    public function returnbakn(Request $request, $id){

        $bakn = Bakn::find($id);
        $customMessages = [
            'required' => ':Attribute field is required.',
        ];
        $this->validate($request, array(
            'chat' => 'required',
        ),$customMessages);

        try{
            // insert data chat
            DB::beginTransaction();
            // update data bakn
            $bakn->status = 'draft_bakn';
            $bakn->approval = $request->approval;
            $bakn->save();
            // dd($bakn);
            $chat = new ChatBakn();
            $chat->chat = $request->input('chat');
            $chat->idBakn = $id;
            $chat->jabatan = Auth::user()->position;
            $chat->username = Auth::user()->username;
            $chat->transaksi = 'BAKN';
            $chat->status = 'Return';
            $chat->save();
            
            // after insert send notifikasi to WA
            $this->notifikasi($id);
            DB::commit();
        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong with your form , please check carefully")
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong from the server, please check carefully")
            ->withInput();
        }
        return redirect('kontrak-non')->with('success','Data telah di return');
    }

    public function notifikasi($id)
    {
        $bakn = Bakn::with('user','spph')->find($id);
        $chatnya = ChatBakn::where('idBakn', $id)->latest()->first();
        $mgrlegal = User::where('level','mgrlegal')->where('permanent_status','Organik')->first();
        if($bakn->approval == 'return'){
            $lastnya = User::where('username', $chatnya->username)->first();

            $hp = substr(str_replace("-","",$lastnya->phone),6,13);
            $urls = url('draft-bakn');
            $body = 'Bapak/Ibu *'.$bakn->user['name'].'*'
            ."\xA".'File BAKN dengan Nomor SPPH'
            ."\xA".'*'.$bakn->spph['nomorspph'].'*'
            ."\xA".'Di Return oleh '. strtoupper($lastnya->name)
            ."\xA".'Dengan Catatan '
            ."\xA".'" *_'.$chatnya->chat.'_* "'
            ."\xA".'Silahkan akses pada link berikut'
            ."\xA".$urls;
        }
        if($bakn->status == 'save_bakn'){
            // $hp = substr(str_replace("-","",$mgrlegal->phone),6,13);
            $urls = url('kontrak-non');
            $body = 'Bapak/Ibu *'.$mgrlegal->name.'*'
            ."\xA".'File BAKN dengan Nomor SPPH'
            ."\xA".'*'.$bakn->spph['nomorspph'].'*'
            ."\xA".'Butuh Review Anda'
            ."\xA".'Silahkan akses pada link berikut'
            ."\xA".$urls;
        }
        //nomer ue
        // $hp = '81280295238';
         $data = [
               'phone' => '62'.$hp, // Receivers phone
               'body' => $body,
           ];
           $json = json_encode($data); // Encode data to JSON
           // URL for request POST /message
            $url =env('API_MAIL');
           // Make a POST request
           $options = stream_context_create(['http' => [
                   'method'  => 'POST',
                   'header'  => 'Content-type: application/json',
                   'content' => $json
               ]
           ]);
           // Send a request
           $result = file_get_contents($url, false, $options);
           $var = json_decode($result, true);
            if($var['sent'] == 'true'){
                // dd($var);
                return $this;
            }else{
                return redirect()->back()->with('error', 'Notifikasi (+62'.$hp.') tidak terdaftar ');
            }
        }

        public function index(Request $request){
            if($request->bulan == null){
                $var = '=';
                $bln = date('m');
                $thn = date('Y');
            }else{
                if($request->bulan == 0){
                    $var = '>=';
                    $bln = $request->bulan;
                }else{
                    $var = '=';
                    $bln = $request->bulan;
                }
                $thn = $request->tahun;
            }
            if(Auth::user()->level == 'administrator'){
                $bakns = Bakn::where('status','save_bakn')->whereMonth('created_at',$bln)->whereYear('created_at',$thn)->latest()->get();
            }else{
                $bakns = Bakn::where('status','save_bakn')->whereMonth('created_at',$bln)->whereYear('created_at',$thn)->where('created_by',Auth::user()->id)->latest()->get();
            }

            return view('modules.bakn.index',compact('bakns'));
        }

        public function draft(Request $request){
            if($request->bulan == null){
                $var = '=';
                $bln = date('m');
                $thn = date('Y');
            }else{
                if($request->bulan == 0){
                    $var = '>=';
                    $bln = $request->bulan;
                }else{
                    $var = '=';
                    $bln = $request->bulan;
                }
                $thn = $request->tahun;
            }
            if(Auth::user()->level == 'administrator'){
                $bakns = Bakn::where('status','draft_bakn')
                ->whereMonth('created_at',$var,$bln)
                ->whereYear('created_at',$thn)
                ->latest()
                ->get();
            }else{
                $bakns = Bakn::where('status','draft_bakn')
                ->whereMonth('created_at',$var,$bln)
                ->whereYear('created_at',$thn)
                ->where('created_by',Auth::user()->id)
                ->latest()
                ->get();
            }

            return view('modules.bakn.draft',compact('bakns'));
        }

        public function done(Request $request){
            if($request->bulan == null){
                $var = '=';
                $bln = date('m');
                $thn = date('Y');
            }else{
                if($request->bulan == 0){
                    $var = '>=';
                    $bln = $request->bulan;
                }else{
                    $var = '=';
                    $bln = $request->bulan;
                }
                $thn = $request->tahun;
            }
            if(Auth::user()->level == 'administrator'){
                $bakns = Bakn::where('status','done_bakn')
                ->whereMonth('created_at',$var,$bln)
                ->whereYear('created_at',$thn)
                ->latest()
                ->get();
            }else{
                $bakns = Bakn::where('status','done_bakn')
                ->whereMonth('created_at',$var,$bln)
                ->whereYear('created_at',$thn)
                ->where('created_by',Auth::user()->id)
                ->latest()
                ->get();
            }

            return view('modules.bakn.done',compact('bakns'));
        }

        public function destroy($id){
            $bakn = Bakn::findOrFail($id);
            $bakn->delete();
            $chat = ChatBakn::where('idBakn',$id)->delete();
            $lampiran= "public/files/BAKN/".$bakn->id."/lampiran";
            $file= "public/files/BAKN/".$bakn->id."/file";
            Storage::deleteDirectory($lampiran);
            Storage::deleteDirectory($file);
            return back()->with('success','Your Data has been successfully deleted');
         }

         public function updatePageIo($id){
             $io = Iodesc::all();
             $bakn = Bakn::find($id);
             return view('modules.bakn.inc.update_io', compact('io','bakn'));
         }

         public function updateIo(Request $request, $id){
             $bakn = Bakn::find($id);
             $bakn->io_id = $request->noio;
            //  dd($bakn);
             $bakn->save();

             return redirect('bakn-done')->with('success','Update IO Berhasil');

         }
    }
