<?php

namespace App\Http\Controllers;

use App\Exports\SpphLkppExport;
use App\Http\Requests\FormSpphLkppRequest;
use App\Models\Mitra;
use App\Models\SpphLKPP;
use App\Models\Unit;
use App\User;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Facades\Excel;


class SpphLKPPController extends Controller
{
    protected $model;
    protected $mitra;
    protected $user;

    public function __construct()
    {
        $this->model    = new SpphLKPP();
        $this->mitra    = new Mitra();
        $this->user     = new User();
    }

    // Function Get all data for admin only
    public function getAllData(Request $request)
    {
        $data = $this->model->scopeAllData($request->month, $request->year)->get();
        return DataTables()
                ->of($data)
                ->addIndexColumn()
                ->addColumn('status_dokumen', function($data){
                    $status = array (
                       "kontrak" =>  $data['bakn_lkpps']['kontrak_lkpp'] == null ? null : $data['bakn_lkpps']['kontrak_lkpp']['id'],
                       "bakn" => $data->bakn_lkpps == null ? null : $data->bakn_lkpps['id'],
                    );
                    return $status;
                })
                ->make(true);
    }

    // Function get data where status draft
    public function draftData(Request $request)
    {
        $data = $this->model->scopeDraftSpph($request->month, $request->year)->get();
        return DataTables()
                ->of($data)
                ->addIndexColumn()
                ->addColumn('action', function($data){
                    $edit =array(
                        "id" => $data->id,
                        "nomorspph" => $data->nomorspph,
                    );
                    return  $edit;
                })
                ->make(true);
    }

    // Function get data where status save
    public function getListData(Request $request)
    {
        $data = $this->model->scopeListSpph($request->month, $request->year)->get();
        return DataTables()
                ->of($data)
                ->addIndexColumn()
                ->make(true);
    }

    // Function get data where status done
    public function getDoneData(Request $request)
    {
        $data = $this->model->scopeDoneSpph($request->month, $request->year)->get();
        return DataTables()
                ->of($data)
                ->addIndexColumn()
                ->addColumn('action', function($data){
                    $edit =array(
                        "id" => $data->id,
                        "nomorspph" => $data->nomorspph,
                        "level" => Auth::user()->level
                    );
                    return  $edit;
                })
                ->addColumn('status_dokumen', function($data){
                    $status = array (
                       "kontrak" =>  $data['bakn_lkpps']['kontrak_lkpp'] == null ? null : $data['bakn_lkpps']['kontrak_lkpp']['id'],
                       "bakn" => $data->bakn_lkpps == null ? null : $data->bakn_lkpps['id'],
                    );
                    return $status;
                })
                ->make(true);
    }

    public function getNotDraft()
    {
        return $this->model->scopeNotDraft()->get();
    }

    // function create
    public function create()
    {
        // data mitra 
        $mitras     = $this->mitra->all();
        // data user where unit ecom
        $user       = $this->user->where('id_unit', 56)->get();
        // data tembusan
        $tembusan   = $this->model->scopeTembusan();

        return view('modules.spph_lkpp.create',compact('mitras','user','tembusan'));
      }

    // function for edit
    public function edit($id)
    {
        // get data from id
        $spph       = $this->model->with('mitra_lkpps','creator_spph_lkpp')->find($id);
        // add comma inside tembusan
        $tembus     = explode(",",$spph->tembusan);
        // Data Mitra
        $mitras     = $this->mitra->all();
        // variable user for pic
        $user       = $this->user->where('id_unit',56)->get();
        // variable for data user tembusan 
        $tembusan   = $this->model->scopeTembusan();

        return view('modules.spph_lkpp.edit', compact('spph','mitras','user','tembusan','tembus'));
    }

    // Function preview data spph
    public function preview(SpphLKPP $spphLKPP)
    {
            $pic    = DB::table('spph_lkpps')
                    ->join('users','spph_lkpps.pic','=','users.name')
                    ->select('spph_lkpps.pic', 'users.*')
                    ->where('spph_lkpps.id',$spphLKPP->id)
                    ->first();
            
            $dari   = DB::table('spph_lkpps')
                    ->join('users','spph_lkpps.dari','users.name')
                    ->select('spph_lkpps.dari', 'users.*')
                    ->where('spph_lkpps.id', $spphLKPP->id)
                    ->first();

            return view('modules.spph_lkpp.preview',compact('spphLKPP','dari','pic'));
       
    }

    // function store data
    public function store(FormSpphLkppRequest $request)
    {
        DB::beginTransaction();
        // Declare and insert data to array
        $data               = $request->all();
        $data['lampiran']   = $request->file('lampiran') != null ? $request->file('lampiran') : NULL;
        
        try{
            // Call function insert data
            $model = $this->model->insertSpphLkpp($data);

            // conditional status submit save or draft
            if($model->status == 'save_spph'){
                // insert DB 
                DB::commit();
                return redirect('/list-spph-lkpp/')->with('success', 'SPPH ('.$model->nomorspph.') was added successfully');
            }else{
                // insert DB 
                DB::commit();
                return redirect('/draft-spph-lkpp/')->with('success', 'SPPH ('.$model->nomorspph.') was added successfully');
            }
        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors($e->getMessage())
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
           ->withErrors($e->getMessage())
           ->withInput();
        }
    }

    // function update data
    public function update(FormSpphLkppRequest $request, SpphLKPP $spphLKPP)
    {
        DB::beginTransaction();
        // Declare and insert data to array
        $data               = $request->all();
        $data['lampiran']   = $request->file('lampiran') != null ? $request->file('lampiran') : NULL;
   
        try{
            // Call function insert data
            $model = $this->model->updateSpphLkpp($data, $spphLKPP);

            // conditional status submit save or draft
            if($model->status == 'save_spph'){
                // insert DB 
                DB::commit();
                return redirect('/list-spph-lkpp/')->with('success', 'SPPH ('.$model->nomorspph.') was added successfully');
            }else{
                // insert DB 
                DB::commit();
                return redirect('/draft-spph-lkpp/')->with('success', 'SPPH ('.$model->nomorspph.') was added successfully');
            }
        } catch (Exception $e) {
            DB::rollBack();
            return redirect()->back()
           ->withErrors($e->getMessage())
           ->withInput();
        }
    }

    // function upoad file where status save
    public function file(Request $request, SpphLKPP $spphLKPP)
    {
        // declare variable array 
        $data = $request->all();
        $data['file']   = $request->file('file') != null ? $request->file('file') : NULL;

        try{
            // send data to model for upload file 
            $this->model->uploadFile($data, $spphLKPP);
            DB::commit();            
            return redirect('/done-spph-lkpp/')->with('success', 'Your files has been successfully uploaded');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()
           ->withErrors($e->getMessage())
           ->withInput();
        }

    }

    public function lampiran(Request $request, SpphLKPP $spphLKPP)
    {
        // declare data to array 
        $data   = $request->all(); 
        try{
            // send data to model for upload lampiran 
            $this->model->uploadLampiran($data, $spphLKPP);    
            DB::commit();
            return redirect()->back()->with('success', 'Your lampirans has been successfully uploaded');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
           ->withErrors($e->getMessage())
           ->withInput();
        }
    }

    // Function delete data
    public function destroy($id)
    {
        // send data id to model 
        $data = $this->model->deleteSpph($id);

        return response()->json([
            'message' => 'Successfully deleted task!'
        ]);
    }

    // Export all data spph
    public function exportSpph(Request $request)
    {
        return Excel::download(new SpphLkppExport(), "spph_lkpp_".date('Y-m-d').".xlsx");
          
        // return (new SpphLkppExport)->download('spph_lkpp.xlsx');
    }
}
    