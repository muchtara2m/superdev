<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use OwenIt\Auditing\Facades\Auditor;
// use Illuminate\Foundation\Auth\AuthenticatesUsers;

// Login with LDAP
use App\Foundation\FadAuthUsers;

// Login just username
// use App\Foundation\BedoyAuthUsers;

class LoginController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;
    // Login Ldap
    use FadAuthUsers;
    
    // Login username
    // use BedoyAuthUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
