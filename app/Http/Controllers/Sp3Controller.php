<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Validator;
use DB;
use App\Models\Sp3;
use App\Models\BaknLKPP;
use App\Models\Bakn;
use App\Models\ChatSp3;
use App\User;


class Sp3Controller extends Controller
{
   public function create(){
        $bakn = BaknLKPP::where('status','!=','draft_bakn')->doesntHave('sp3_lkpp')->get();
       return view('modules.sp3_lkpp.create',compact('bakn'));
   }

   function dataBakn(Request $request){
    if($request->ajax() && $request->id != null){
        $bakn = BaknLKPP::with('spph_lkpps')->where('id', $request->id)->first();
        return response()->json(['options'=>$bakn]);
    }

   }

   public function store(Request $request){
    $message = array(
        'required' => ':Attribute field is required',
        'unique' => ':Attribute sudah pernah dibuat',

    );
    $this->validate($request, [
        'baknid' => 'required',
        'harga_sp3' => 'required',
        'nosp3' => 'required',
        'nosp3' => 'unique:sp3_lkpps,nomor_sp3',
        'tglsp3' => 'required',
    ],$message);
    DB::beginTransaction();
    try{
        $sp3 = new Sp3();
        $sp3->created_by = Auth::user()->id;
        $sp3->status = $request->input('status');
        $sp3->bakn_id = $request->input('baknid');
        $sp3->harga = $request->input('harga_sp3');
        $sp3->nomor_sp3 = $request->input('nosp3');
        $sp3->tanggal_sp3 = $request->input('tglsp3');
        $sp3->lain_lain = $request->input('lain_lain');
        $sp3->cara_bayar = $request->input('cara_bayar');
        $sp3->jangka_waktu = $request->input('jangka_waktu');
        $sp3->ruang_lingkup = $request->input('ruang_lingkup');
        $sp3->harga_terbilang = $request->input('harga_terbilang');
        $sp3->lokasi_pekerjaan = $request->input('lokasi_pekerjaan');
        $sp3->save();
        $path= "public/files/SP3_LKPP/".$sp3->id."/lampiran";
        if($request->file('lampiran') != null)
        {
            foreach($request->file('lampiran') as $file)
            {
                $name=$file->getClientOriginalName();
                $file->storeAs($path, $name);
                $namanya[] = $name;
                $data[] = $path.'/'.$name;
            }
            $file = Sp3::find($sp3->id);
            $file->lampiran = json_encode($data);
            $file->title_lampiran = json_encode($namanya);
            $file->tgl_lampiran = date('Y-m-d');
            $file->save();
        }
        // after submit 
        if($sp3->status == 'save_sp3'){
            $this->validate($request, array(
               'chat' => 'required',
            ),$message);
             // insert chat admin
             $komen = new ChatSp3();
             $komen->chat = $request->get('chat');
             $komen->queue = 0;
             $komen->jabatan = Auth::user()->position;
             $komen->username = Auth::user()->username;
             $komen->idsp3 = $sp3->id;
             $komen->transaksi = 'SP3';
             $komen->status = 'Approve';
             $komen->save();
             $this->approval($sp3->id);
          }
        DB::commit();

    } catch (ValidationException $e) {
        DB::rollback();
        return redirect()->back()
        ->withErrors("Something wrong with your form , please check carefully")
        ->withInput();
    } catch (\Exception $e) {
        DB::rollback();
        return redirect()->back()
        ->withErrors("Something wrong from the server, please check carefully")
        ->withInput();
    }
    if($sp3->status == 'draft_sp3'){
        return redirect('draft-sp3-lkpp')->with('success','File SP3 with No. ('.$sp3->nomor_sp3.') was added successfully');
    }else{
        return redirect('inprogress-sp3-lkpp')->with('success','File SP3 with No. ('.$sp3->nomor_sp3.') was added successfully');
    }

   }

   public function edit($id){
        $sp3 = Sp3::find($id);
        $chat = ChatSp3::where('idsp3',$sp3->id)->orderBy('created_at','desc')->first();
    return view('modules.sp3_lkpp.edit',compact('sp3','chat'));
   }

   public function preview($id){
       $sp3=Sp3::find($id);
       $dirop = User::where('level','dirop')->get();
       $bakn = Bakn::where('id',$sp3->bakn_id)->first();
       return view('modules.sp3_lkpp.preview',compact('sp3','bakn','dirop'));
   }

   public function update(Request $request, $id){
    $message = array(
        'required' => ':Attribute field is required'
    );
    DB::beginTransaction();
    try{
        $sp3 = Sp3::find($id);
        $sp3->bakn_id = $request->input('baknid');
        $sp3->status = $request->input('status');
        $sp3->harga = $request->input('harga_sp3');
        $sp3->nomor_sp3 = $request->input('nosp3');
        $sp3->tanggal_sp3 = $request->input('tglsp3');
        $sp3->lain_lain = $request->input('lain_lain');
        $sp3->cara_bayar = $request->input('cara_bayar');
        $sp3->jangka_waktu = $request->input('jangka_waktu');
        $sp3->ruang_lingkup = $request->input('ruang_lingkup');
        $sp3->harga_terbilang = $request->input('harga_terbilang');
        $sp3->lokasi_pekerjaan = $request->input('lokasi_pekerjaan');
        $path= "public/files/SP3_LKPP/".$sp3->id."/lampiran";
        if($request->file('lampiran') != null)
        {
            foreach($request->file('lampiran') as $file)
            {
                $name=$file->getClientOriginalName();
                $file->storeAs($path, $name);
                $namanya[] = $name;
                $data[] = $path.'/'.$name;
            }
            $sp3->lampiran = json_encode($data);
            $sp3->title_lampiran = json_encode($namanya);
            $sp3->tgl_lampiran = date('Y-m-d');
            // $sp3->save();
        }
        $sp3->save();

        // after submit 
        if($sp3->status == 'save_sp3'){
            $this->validate($request, array(
               'chat' => 'required',
            ),$message);
             // insert chat admin
             $komen = new ChatSp3();
             $komen->chat = $request->get('chat');
             $komen->queue = 0;
             $komen->jabatan = Auth::user()->position;
             $komen->username = Auth::user()->username;
             $komen->idsp3 = $sp3->id;
             $komen->transaksi = 'SP3';
             $komen->status = 'Approve';
             $komen->save();
             $this->approval($sp3->id);
          }
        DB::commit();

    } catch (ValidationException $e) {
        DB::rollback();
        return redirect()->back()
        ->withErrors("Something wrong with your form , please check carefully")
        ->withInput();
    } catch (\Exception $e) {
        DB::rollback();
        return redirect()->back()
        ->withErrors("Something wrong from the server, please check carefully")
        ->withInput();
    }
    if($sp3->status == 'draft_sp3'){
        return redirect('draft-sp3-lkpp')->with('success','File SP3 with No. ('.$sp3->nomor_sp3.') was added successfully');
    }else{
        return redirect('inprogress-sp3-lkpp')->with('success','File SP3 with No. ('.$sp3->nomor_sp3.') was added successfully');
    }

   }

   public function approval($id){
       $sp3=Sp3::find($id);
    $end = DB::table('data_flows')
        ->select('jabatan','username','queue')
        ->where([
            ['min','<=',$sp3->harga],
            ['max','>=',$sp3->harga],
            ['transaksi', 'SP3'],
            ['unit', 'e-Commerce']
            ])->first();
    if(Auth::user()->level == 'adminlkpp'){
       $awal = DB::table('data_flows')
       ->select('jabatan','username','queue')
       ->where([
           ['transaksi', 'SP3'],
           ['unit', 'e-Commerce']
           ])
        ->orderBy('queue','asc')      
       ->first();
    }else{
       $awal = DB::table('data_flows')
       ->select('jabatan','username','queue')
       ->where([
          ['queue',2],
          ['transaksi', 'SP3'],
          ['unit', 'e-Commerce']
          ])   
       ->first();
    }
    $insert = DB::table('sp3_lkpps')
    ->where('id',$id)
    ->update(
    [
        'approval' => $awal->username,
        'end_approval' =>$end->username
    ]);
    $this->notifikasi($id,$awal->username);
   }

   public function notifikasi($id, $name){
    $sp3 = Sp3::find($id);
    $lastnya = ChatSp3::where('idsp3',$id)
    ->select('chat_sp3s.*','users.name')
    ->join('users','users.username','chat_sp3s.username')
    ->orderBy('created_at','desc')->first();
    $usernya = User::where('username',$name)->first();
    // function shorten url
    if ($sp3->approval == 'CLOSED') {
        $hp = substr(str_replace("-","",$usernya->phone),6,13);
        $urls = url('list-sp3-lkpp');
        $body = 'Bapak/Ibu *'.$usernya->name.'*'
        ."\xA".'File SP3 dengan Nomor '
        ."\xA".'*'.$sp3->nomor_sp3.'*'
        ."\xA".'Sudah Selesai di Approve, silahkan askes pada link berikut['
        ."\xA".$urls;
    }else if($sp3->approval == 'Return'){
        $hp = substr(str_replace("-","",$usernya->phone),6,13);
        $urls = url('status-transaksi-sp3-lkpp');
        $body = 'Bapak/Ibu *'.$usernya->name.'*'
        ."\xA".'File SP3 dengan Nomor'
        ."\xA".'*'.$sp3->nomor_sp3.'*'
        ."\xA".'Di Return oleh '. strtoupper($lastnya->name)
        ."\xA".'Dengan Catatan '
        ."\xA".'" *_'.$lastnya->chat.'_* "'
        ."\xA".'Silahkan akses pada link berikut'
        ."\xA".$urls;
    }else{
        $hp = substr(str_replace("-","",$usernya->phone),6,13);
        $urls = url('preview-status-sp3-lkpp/'.$id);
        $body = 'Bapak/Ibu *'.$usernya->name.'*'
        ."\xA".'File SP3 dengan Nomor'
        ."\xA".'*'.$sp3->nomor_sp3.'*'
        ."\xA".'Membutuhkan Approve Anda, silahkan akses pada link berikut '
        ."\xA".$urls;
   }

    //    $hp = '81280295238';
       $data = [
           'phone' => '62'.$hp, // Receivers phone
           'body' => $body,
       ];
       $json = json_encode($data); // Encode data to JSON
       // URL for request POST /message
       $url =env('API_MAIL');
       
       // Make a POST request
       $options = stream_context_create(['http' => [
               'method'  => 'POST',
               'header'  => 'Content-type: application/json',
               'content' => $json
           ]
       ]);
       // Send a request
       $result = file_get_contents($url, false, $options);
       $var = json_decode($result, true);
       if($var['sent'] == 'true'){
           return $this;                
       }else{
           return redirect()->back()->with('error', 'Notifikasi (+62'.$hp.') tidak terdaftar ');
       }
   }

   public function chatApproval(Request $request){
    $flow =DB::table('data_flows')
    ->where('username', Auth::user()->username)
    ->first();
    DB::beginTransaction();
    try{
     $komen = new ChatSp3();
     $komen->chat = $request->get('chat');
     $komen->queue = $flow->queue;
     $komen->jabatan = Auth::user()->position;
     $komen->username = Auth::user()->username;
     $komen->idsp3 = $request->get('idTransaksi');
     $komen->transaksi = 'SP3';
     $komen->status = $request->get('status');
     $komen->save();
     if ($komen->status == 'Approve') {
        $idSp3 = $request->get('idKontrak');
        $sp3 = Sp3::find($idSp3);
        $unit = 'e-Commerce';
        $trx = 'SP3';
        $skrg = $flow->queue;
        $jabatanskrg = $sp3->approval;
        $jabatanend = $sp3->end_approval;
        // bandingin antara approval dengan approval akhir
        if ($jabatanskrg != $jabatanend) {
           $skrg++;
           $cek = DB::table('data_flows')
           ->select('jabatan','username','queue')
           ->where([
               ['queue', $skrg],
               ['transaksi', $trx],
               ['unit', $unit]
           ])->first();
           $updateapproval = DB::table('sp3_lkpps')
           ->where('id', $idSp3)
           ->update(
               ['approval' => $cek->username]
           );
           $this->notifikasi($idSp3,$cek->username);
           DB::commit();
         }else {
           // status CLOSED dengan nilai 9
           $updateapproval = DB::table('sp3_lkpps')
           ->where('id', $idSp3)
           ->update(
               ['approval' => 'CLOSED']
           );
           $this->notifikasi($idSp3,$sp3->user_sp3_lkpp['username']);
           DB::commit();
        }
        return redirect('list-sp3-lkpp')->with('success','SP3 with No.  ('.$sp3->nomor_sp3.') has been Approved by '.Auth::user()->name);
     }else {
        // jika komen return
        $idSp3 = $request->get('idKontrak');
        $sp3 = Sp3::find($idSp3);
        $updatestatus = DB::table('sp3_lkpps')
        ->where('id', $idSp3)
        ->update(['approval' =>'Return'] );
        $name = User::where('id',$sp3->created_by)->first();
        $this->notifikasi($idSp3,$name->username);
        DB::commit();
        return redirect('inprogress-sp3-lkpp')->with('success','SP3 with No.  ('.$sp3->nomor_sp3.') has been Return by '.Auth::user()->name);
        }
    } catch (ValidationException $e) {
        DB::rollback();
        return redirect()->back()
        ->withErrors("Something wrong with your form, please check carefully")
        ->withInput();
    } catch (\Exception $e) {
        DB::rollback();
        return redirect()->back()
        ->withErrors("Something wrong from the server, please check carefully")
        ->withInput();
    }
}

public function preview_status($id){
    $sp3 = Sp3::find($id);
    $chats = DB::table('chat_sp3s')
    ->join('users','chat_sp3s.username','=','users.username')
    ->select('chat','jabatan','chat_sp3s.username','users.name','chat_sp3s.created_at')
    ->where('idsp3',$id)
    ->orderBy('chat_sp3s.created_at','asc')
    ->get();
    $dirop = User::where('level','dirop')->first();
    $bakn = BaknLKPP::where('id',$sp3->bakn_id)->first();
    $checkbox = explode(",", $bakn->tipe_rapat);
    $user = User::all();
    
   return view('modules.sp3_lkpp.preview_status',compact('dirop','sp3','user','chats','bakn','checkbox'));
}

   public function list(Request $request){

    if($request->bulan == null){
        $var = '=';
        $bln = date('m');
        $thn = date('Y');
     }else{
        if($request->bulan == 0){
           $var = '>=';
           $bln = $request->bulan;
        }else{
           $var = '=';
           $bln = $request->bulan;
        }
        $thn = $request->tahun;   
     }
     if(Auth::user()->level == 'administrator'){
        $sp3 = Sp3::where('approval','CLOSED')->where('file',NULL)->orderBy('created_at','desc')->get();
     }else{
        $sp3 = Sp3::where('approval','CLOSED')->where('file',NULL)->where('created_by',Auth::user()->id)->orderBy('created_at','desc')->get();
     }
     return view('modules.sp3_lkpp.list',compact('sp3'));
   }
   
   public function draft(Request $request){
    if($request->bulan == null){
        $var = '=';
        $bln = date('m');
        $thn = date('Y');
     }else{
        if($request->bulan == 0){
           $var = '>=';
           $bln = $request->bulan;
        }else{
           $var = '=';
           $bln = $request->bulan;
        }
        $thn = $request->tahun;   
     }
     if(Auth::user()->level == 'administrator'){
        $sp3 = Sp3::where('status','draft_sp3')->orderBy('created_at','desc')->get();
     }else{
        $sp3 = Sp3::where('status','draft_sp3')->where('created_by',Auth::user()->id)->orderBy('created_at','desc')->get();
     }
    //  dd($bak);
     return view('modules.sp3_lkpp.draft',compact('sp3'));
   }

   public function inprogress(Request $request){

    if ($request->bulan == null) {
        $var = '=';
        $bln = date('m');
        $thn = date('Y');
    } else {
        if ($request->bulan == 0) {
            $var = '>=';
            $bln = $request->bulan;
        } else {
            $var = '=';
            $bln = $request->bulan;
        }
        $thn = $request->tahun;
    }
    if (Auth::user()->level == 'administrator' || Auth::user()->level != 'adminlkpp' || Auth::user()->level == 'staff_bakn_lkpp') {
        $sp3 = Sp3::with(['chatsp3'=>function ($q) {
            $q->select('chat_sp3s.*', 'users.name')
            ->join('users', 'users.username', 'chat_sp3s.username')
            ->orderBy('created_at', 'asc');
        },'bakn_lkpp','bakn_lkpp.spph_lkpps','user_sp3_lkpp','bakn_lkpp.spph_lkpps.mitra_lkpps'])
        ->where('approval', '!=', 'CLOSED')
        ->whereMonth('created_at', $var, $bln)
        ->whereYear('created_at', $thn)
        ->orderBy('created_at', 'desc')
        ->get();
    } else {
        $sp3 = Sp3::with(['chatsp3'=>function ($q) {
            $q->select('chat_sp3s.*', 'users.name')
            ->join('users', 'users.username', 'chat_sp3s.username')
            ->orderBy('created_at', 'asc');
        },'bakn_lkpp','bakn_lkpp.spph_lkpps','user_sp3_lkpp','bakn_lkpp.spph_lkpps.mitra_lkpps'])
        ->where('approval', '!=', 'CLOSED')
        ->where('created_by', Auth::user()->id)
        ->whereMonth('created_at', $var, $bln)
        ->whereYear('created_at', $thn)
        ->orderBy('created_at', 'desc')
        ->get();
    }
    // dd($sp3);
  return view('modules.sp3_lkpp.inprogress',compact('sp3'));
   }

   public function status_transaksi(Request $request){

    if($request->bulan == null){
        $var = '=';
        $bln = date('m');
        $thn = date('Y');
     }else{
        if($request->bulan == 0){
           $var = '>=';
           $bln = $request->bulan;
        }else{
           $var = '=';
           $bln = $request->bulan;
        }
        $thn = $request->tahun;   
     }
     if(Auth::user()->level == 'administrator'){
        $sp3 = Sp3::where('approval','!=','CLOSED')
        ->with('bakn_lkpp','bakn_lkpp.spph_lkpps.mitra_lkpps')
        ->whereMonth('created_at',$var,$bln)                            
        ->whereYear('created_at',$thn)
        ->orderBy('created_at','desc')
        ->get();
     }else{
        $sp3 = Sp3::where(function($q){
           $q->where('approval',Auth::user()->username)
           ->where('created_by','!=',Auth::user()->id);
        })->orWhere(function($w){
           $w->where('approval','Return')
           ->where('created_by',Auth::user()->id);
        })
        ->with('bakn_lkpp','bakn_lkpp.spph_lkpps.mitra_lkpps')
        ->whereMonth('created_at',$var,$bln)                                          
        ->whereYear('created_at',$thn)
        ->orderBy('created_at','desc')              
        ->get();
     }
     // dd($bakn);
     return view('modules.sp3_lkpp.status_transaksi',compact('sp3'));
   }

   public function tracking(Request $request){

    if($request->bulan == null){
        $var = '=';
        $bln = date('m');
        $thn = date('Y');
     }else{
        if($request->bulan == 0){
           $var = '>=';
           $bln = $request->bulan;
        }else{
           $var = '=';
           $bln = $request->bulan;
        }
        $thn = $request->tahun;   
     }
     if(Auth::user()->level == 'administrator' || Auth::user()->level != 'adminkpp' || Auth::user()->level == 'staff_bakn_lkpp'){
        $sp3 = Sp3::with(['chatsp3'=>function ($q){
            $q->select('chat_sp3s.*','users.name')
            ->join('users','users.username','chat_sp3s.username')
            ->orderBy('created_at','asc');
        },'bakn_lkpp','bakn_lkpp.spph_lkpps','user_sp3_lkpp'])
        ->whereMonth('created_at',$var,$bln)                            
        ->whereYear('created_at',$thn)
        ->orderBy('created_at','desc')
        ->get();
    }else{
        $sp3 = Sp3::with(['chatsp3'=>function ($q){
            $q->select('chat_sp3s.*','users.name')
            ->join('users','users.username','chat_sp3s.username')
            ->orderBy('created_at','asc');
        },'bakn_lkpp','bakn_lkpp.spph_lkpps','user_sp3_lkpp'])
        ->where('created_by',Auth::user()->id)
        ->whereMonth('created_at',$var,$bln)                            
        ->whereYear('created_at',$thn)
        ->orderBy('created_at','desc')
        ->get();
    }
     return view('modules.sp3_lkpp.tracking',compact('sp3'));
   }

   public function done(Request $request){
    if($request->bulan == null){
        $var = '=';
        $bln = date('m');
        $thn = date('Y');
     }else{
        if($request->bulan == 0){
           $var = '>=';
           $bln = $request->bulan;
        }else{
           $var = '=';
           $bln = $request->bulan;
        }
        $thn = $request->tahun;   
     }
     if(Auth::user()->level == 'administrator'){
        $sp3 = Sp3::where('status','done_sp3')->orderBy('created_at','desc')->get();
     }else{
        $sp3 = Sp3::where('status','done_sp3')->where('created_by',Auth::user()->id)->orderBy('created_at','desc')->get();
     }
     return view('modules.sp3_lkpp.done',compact('sp3'));
   }

   public function destroy($id){

    $sp3 = Sp3::findOrFail($id); 
    $sp3->delete();
    $chat = ChatSp3::where('idsp3',$id)->delete();
    $lampiran= "public/files/SP3_LKPP/".$sp3->id."/lampiran";
    $file= "public/files/SP3_LKPP/".$sp3->id."/file";
    Storage::deleteDirectory($lampiran);
    Storage::deleteDirectory($file);
    return back()->with('success','Your Data has been successfully deleted');
   }

   public function file(Request $request,$id){
    $this->validate($request, [
        'file' => 'required',
        ]);
        $path= "public/files/SP3_LKPP/".$id."/file";
        if($request->file('file'))
        {
            foreach($request->file('file') as $file)
            {
                $name=$file->getClientOriginalName();
                $file->storeAs($path, $name);
                $namanya[] = $name;
                $data[] = $path.'/'.$name;
            }
        }
        $file = Sp3::find($id);
        $file->upload = date("Y-m-d H:i:s");
        $file->status = "done_sp3";
        $file->file=json_encode($data);
        $file->title = json_encode($namanya);
        // dd($file);
        $file->save();
        return redirect('done-sp3-lkpp')->with('success', 'Your files has been successfully added');
   }

}
