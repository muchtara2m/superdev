<?php

namespace App\Http\Controllers;

use App\Models\Mitra;
use App\Models\Spph;
use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SpphController extends Controller
{
    public function create(){
        $mitras = Mitra::all();
        $user = DB::table('users')->where('id_unit',59)->get();
        $dari = DB::select("SELECT * FROM `users` where  (position like 'gm%' or position like 'vp%' or position like 'mgr%' or position like 'avp%') AND id_unit= 59 ");
        $tembusan = DB::select("SELECT * FROM `users` where  (position like 'mgr%' or position like 'avp%' or position like 'gm%' or position like 'vp%') AND (id_unit= 44 OR id_unit= 45 OR id_unit= 46 OR id_unit= 47 OR id_unit= 48 OR id_unit=49 OR id_unit=54 OR id_unit=55 OR id_unit=56 OR id_unit=38) ");
        return view('modules.spph.create',compact('mitras','user','tembusan'));
    }

    public function store(Request $request){
        $customMessages = [
            'required' => ':Attribute This Field is Required',
            'unique' => ' This :Attribute  have already exist',
        ];
        $this->validate($request, [
            'tglspph' => 'required',
            'nomorspph' => 'required|unique:spphs',
            'tanggal_berkas' => 'required',
        ], $customMessages);

        if($request->status == 'save_spph'){
            $this->validate($request, [
                'tglspph' => 'required',
                'tglsph' => 'required',
                'kepada' => 'required',
                'handler' => 'required',
                'judul' => 'required',
                'tembusan' => 'required',
                'pembuat' => 'required',
                'status' => 'required',
                'perihal' => 'required',
            ], $customMessages);
        }
    
        DB::beginTransaction();
        try{
            $spph = new Spph();
            $spph->nomorspph = $request->input('nomorspph');            
            $spph->tglspph = $request->input('tglspph');
            $spph->tglsph = $request->input('tglsph');
            $spph->mitra = $request->input('kepada');
            $spph->dari = $request->input('handler');
            $spph->tembusan = $request->input('tembusan');
            $spph->perihal = $request->input('perihal');
            $spph->pic = $request->input('pembuat');
            $spph->judul = $request->input('judul');
            $spph->detail = $request->input('detail');
            $spph->status = $request->input('status');
            $spph->tanggal_terima_berkas = $request->input('tanggal_berkas');
            $spph->created_by = Auth::user()->id;
            // dd($spph);
            $spph->save();
            $path= "public/files/SPPH/".$spph->id."/lampiran";
            if ($request->file('lampiran') != null) {
                foreach ($request->file('lampiran') as $file) {
                    $name=$file->getClientOriginalName();
                    $file->storeAs($path, $name);
                    $namanya[] = $name;
                    $data[] = $path.'/'.$name;
                }
                $file = Spph::find($spph->id);
                $file->lampiran = json_encode($data);
                $file->title_lampiran = json_encode($namanya);
                $file->tgl_lampiran = date('Y-m-d');
                $file->save();
            }
            if($spph->status == 'save_spph'){
                DB::commit();
                return redirect('spph-index')->with('success', "Data $spph->nomorspph has been added successfully");
            }else{
                DB::commit();
                return redirect('spph-draft')->with('success', "Data $spph->nomorspph has been added successfully");
            }
        }catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong with your form, please check carefully")
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong from the server, please check carefully")
            ->withInput();
        }
    }

    public function edit($id){
        $spph = Spph::with('mitras','creator')->find($id);
        $mitras = Mitra::all();
        $user = DB::table('users')->where('id_unit',56)->get();
        $dari = DB::select("SELECT * FROM `users` where  (position like 'gm%' or position like 'vp%' or position like 'mgr%' or position like 'avp%') AND id_unit= 56 ");
        $tembusan = DB::select("SELECT * FROM `users` where  (position like 'mgr%' or position like 'avp%' or position like 'gm%' or position like 'vp%') AND (id_unit= 44 OR id_unit= 45 OR id_unit= 46 OR id_unit= 47 OR id_unit= 48 OR id_unit=49 OR id_unit=54 OR id_unit=55 OR id_unit=56 OR id_unit=38) ");
        $tembus = explode(",",$spph->tembusan);
        $pic = DB::select("SELECT * FROM `users` where  id_unit= 59");
                // $dari = DB::select("SELECT * FROM `users` where  (position like 'gm%' or position like 'vp%' or position like 'mgr%' or position like 'avp%') AND id_unit=59");
        return view('modules.spph.edit', compact('spph','mitras','user','dari','tembusan','tembus','pic'));
    }

    public function update(Request $request, $id){
        $customMessages = [
            'required' => ':Attribute This Field is Required',
            // 'unique' => ' This :Attribute  have already exist',
        ];
        if($request->status == 'save_spph'){
            $this->validate($request, [
                'tglspph' => 'required',
                'tglsph' => 'required',
                'kepada' => 'required',
                'judul' => 'required',
                'tembusan' => 'required',
                'perihal' => 'required',
            ], $customMessages);
        }
    
        DB::beginTransaction();
        try{
            $spph = Spph::find($id);
            $spph->nomorspph = $request->input('nomorspph');
            $spph->tglspph = $request->input('tglspph');
            $spph->tglsph = $request->input('tglsph');
            $spph->mitra = $request->input('kepada');
            $spph->dari = $request->input('dari');
            $spph->tembusan = $request->input('tembusan');
            $spph->perihal = $request->input('perihal');
            $spph->pic = $request->input('pic');
            $spph->judul = $request->input('judul');
            $spph->tanggal_terima_berkas = $request->input('tanggal_berkas');
            $spph->status = $request->input('status');
            $path= "public/files/SPPH/".$id."/lampiran";
            if ($request->file('lampiran') != null) {
                foreach ($request->file('lampiran') as $file) {
                    $name=$file->getClientOriginalName();
                    $file->storeAs($path, $name);
                    $namanya[] = $name;
                    $data[] = $path.'/'.$name;
                }
                $spph->lampiran = json_encode($data);
                $spph->title_lampiran = json_encode($namanya);
                $spph->tgl_lampiran = date('Y-m-d');
            }
            $spph->save();
            if($spph->status == 'save_spph'){
                DB::commit();
                return redirect('spph-index')->with('success', "Data $spph->nomorspph has been added edited");
            }else{
                DB::commit();
                return redirect('spph-draft')->with('success', "Data $spph->nomorspph has been added edited");
            }
        }catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong with your form, please check carefully")
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors("Something wrong from the server, please check carefully")
            ->withInput();
        }
       
           
      }

    public function preview($id)
    {
        $spph = Spph::find($id);
        $pic = DB::table('spphs')
        ->join('users','spphs.pic','=','users.name')
        ->select('spphs.pic', 'users.*')
        ->where('spphs.id',$id)
        ->first();
        $dari =  DB::table('spphs')
        ->join('users','spphs.dari','users.name')
        ->select('spphs.dari', 'users.*')
        ->where('spphs.id', $id)
        ->first();
        return view('modules.spph.preview',compact('spph','dari','pic'));

    }
    public function lampiran(Request $request, $id)
    {
        $this->validate($request, [
            'file' => 'required',
        ]);
            $unit = Unit::where('id',Auth::user()->id_unit)->first();
            // dd($unit->nama);
            $path= "public/files/SPPH/".$unit->nama."/".$id;
            if($request->file('file'))
            {
                foreach($request->file('file') as $file)
                {
                    $name=$file->getClientOriginalName();
                    $file->storeAs($path, $name);
                    $namanya[] = $name;
                    $data[] = $path.'/'.$name;
                }
            }
            $file = Spph::find($id);
            $file->nomorsph = $request->input('nomorsph');
            $file->status = 'done_spph';
            $file->file=json_encode($data);
            $file->title = json_encode($namanya);
            $file->upload = date('Y-m-d H:i:s');
            $file->save();
        return redirect('spph-done')->with('success', 'Your files has been successfully added');
    }

    public function destroy($id){
        $spph = Spph::findOrFail($id);
        $spph->delete();
        $path = "public/files/SPPH/".$spph->creator->unitnya['nama']."/".$id;
        Storage::deleteDirectory($path);
        return back()->with('success', "SPPH has been deleted successfully");
    }

    public function index(Request $request)
    {
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
                $var = '>=';
                $bln = $request->bulan;
            }else{
                $var = '=';
                $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator'){
            $spphs = Spph::with(['mitras','creator'])
            ->where([
                ["status", "save_spph"]
                ])
                ->whereMonth('created_at',$bln)                                
                ->whereYear('created_at',$thn)
                ->orderBy('updated_at','desc')
                ->get();
            return view('modules.spph.index',compact('spphs'));
        }else {
            $spphs = Spph::with(['mitras','creator'])
            ->where([
                ["status", "save_spph"],
                ['created_by',Auth::user()->id]
                ])
                ->whereMonth('created_at',$bln)                                
                ->whereYear('created_at',$thn)
                ->orderBy('updated_at','desc')
                ->get();
        return view('modules.spph.index',compact('spphs'));
        }

    }
    public function draft(Request $request)
    {
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
                $var = '>=';
                $bln = $request->bulan;
            }else{
                $var = '=';
                $bln = $request->bulan;
            }
            $thn = $request->tahun;   
        }
        if(Auth::user()->level == 'administrator'){
            $draft = Spph::with(['creator','mitras'])->where([
                ["status", "draft_spph"]
                ])
                ->whereMonth('created_at',$var,$bln)                                
                ->whereYear('created_at',$thn)
                ->orderBy('updated_at','desc')
                ->get();
            return view('modules.spph.draft', compact('draft'));
        }else {
            $draft = Spph::with(['creator','mitras'])->where([
                ["status", "draft_spph"],
                ['created_by',Auth::user()->id]
                ])
                ->whereMonth('created_at',$var,$bln)                                
                ->whereYear('created_at',$thn)
                ->orderBy('updated_at','desc')
                ->get();
            return view('modules.spph.draft', compact('draft'));
        }

    }

    public function done(Request $request)
    {
        if($request->bulan == null){
            $var = '=';
            $bln = date('m');
            $thn = date('Y');
        }else{
            if($request->bulan == 0){
                $var = '>=';
                $bln = $request->bulan;
            }else{
                $var = '=';
                $bln = $request->bulan;
            }
            $thn = $request->tahun;           
        }
        if(Auth::user()->level == 'administrator'){
            $done = Spph::with(['creator','mitras'])->where([
                ["status", "done_spph"],
                ])
                ->whereMonth('created_at',$var,$bln)                
                ->whereYear('created_at',$thn)
                ->orderBy('updated_at','desc')
                ->get();
            return view('modules.spph.done', compact('done'));
        }else{
            $done = Spph::with(['creator','mitras'])->where([
                ["status", "done_spph"],
                ['created_by',Auth::user()->id],
                ])
                ->whereMonth('created_at',$var,$bln)
                ->whereYear('created_at',$thn)
                ->orderBy('updated_at','desc')
                ->get();
            return view('modules.spph.done', compact('done'));
        }
        
    }

}