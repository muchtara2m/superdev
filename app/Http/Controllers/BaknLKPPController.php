<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaknLkppRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

use App\Models\BaknLKPP;
use App\Models\Iodesc;
use App\Models\JenisPasal;
use App\Models\CaraBayar;
use App\Models\SpphLKPP;
use App\Models\ChatBaknLkpp;
use App\User;
use DateTime;
use Exception;

class BaknLKPPController extends Controller
{
   protected $model;
   protected $io;
   protected $spph;
   protected $jenispasal;
   protected $bayar;
   protected $chatBakn;
   protected $user;
   protected $chat;

   public function __construct()
   {
      $this->model      = new BaknLKPP();
      $this->io         = new Iodesc();
      $this->spph       = new SpphLKPP();
      $this->jenispasal = new JenisPasal();
      $this->bayar      = new CaraBayar();
      $this->chatBakn   = new ChatBaknLkpp();
      $this->user       = new User();

   }

   public function getDataIo()
   {
        $data = $this->model->scopeDataIo();
        return $data;
   }
   
   public function getDataWhereDoesntHaveBakn($id="")
   {
        $data = $this->model->scopeDataSpphDoesntHaveBakn($id)->get();
        return $data;
   }

   public function dataspph(Request $request)
   {   
      return $this->model->getDetailSpph($request->nomorspph);
   }

   public function getAllData(Request $request)
   {
      $data = $this->model->scopeAllData($request->month, $request->year);
      return DataTables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('key', function($data){
               $date = date("Ym", strtotime($data->created_at));
               if($data->revisi != null){

                  return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
               }else{
                  return "BAKN-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);

               }
            })
            ->addColumn('action', function($data){
               $edit =array(
                   "id" => $data->id,
                   "nomorspph" => $data->spph_lkpps->nomorspph,
               );
               return  $edit;
           })
            ->addColumn('status_dokumen', function($data){
               $status = array (
                  "kontrak" =>  $data['kontrak_lkpp'] == null ? null : $data['kontrak_lkpp']['id'],
               );
               return $status;
           })
            ->make(true);
   }
   
   public function getDraftData(Request $request)
   {
      $data = $this->model->scopeDraftData($request->month, $request->year);
      return DataTables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('key', function($data){
               $date = date("Ym", strtotime($data->created_at));
               if($data->revisi != null){

                  return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
               }else{
                  return "BAKN-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);

               }
            })
            ->addColumn('action', function($data){
               $edit =array(
                  "id" => $data->id,
                  "nomorspph" => $data->spph_lkpps->nomorspph,
               );
               return  $edit;
            })
            ->addColumn('status_dokumen', function($data){
               $status = array (
                  "kontrak" =>  $data['kontrak_lkpp'] == null ? null : $data['kontrak_lkpp']['id'],
               );
               return $status;
            })
            ->make(true);
   }
   
   public function getListData(Request $request)
   {
      $data = $this->model->scopeListData($request->month, $request->year)->get();
      return DataTables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('key', function($data){
               $date = date("Ym", strtotime($data->created_at));
               if($data->revisi != null){

                  return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
               }else{
                  return "BAKN-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);

               }
            })
            ->addColumn('action', function($data){
               $edit =array(
                  "id"     => $data->id,
                  "roles"  => Auth::user()->level
               );
               return  $edit;
            })
            ->addColumn('status_dokumen', function($data){
               $status = array (
                  "kontrak" =>  $data['kontrak_lkpp'] == null ? null : $data['kontrak_lkpp']['id'],
               );
               return $status;
            })
            ->make(true);
   }
   
   public function getDoneData(Request $request)
   {
      $data = $this->model->scopeDoneData($request->month, $request->year)->get();
      return DataTables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('key', function($data){
               $date = date("Ym", strtotime($data->created_at));
               if($data->revisi >= 1){

                  return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
               }else{
                  return "BAKN-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);

               }
            })
            ->addColumn('action', function($data){
               $edit =array(
                  "id" => $data->id,
                  "nomorspph" => $data->spph_lkpps->nomorspph,
               );
               return  $edit;
            })
            ->addColumn('status_dokumen', function($data){
               $status = array (
                  "kontrak" =>  $data['kontrak_lkpp'] == null ? null : $data['kontrak_lkpp']['id'],
               );
               return $status;
            })
            ->make(true);
   }
   
   public function getInprogressData(Request $request)
   {
      $data = $this->model->scopeInprogress($request->month, $request->year)->get();
      return DataTables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('key', function($data){
               $date = date("Ym", strtotime($data->created_at));
               if($data->revisi >= 1){

                  return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
               }else{
                  return "BAKN-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);

               }
            })
            ->addColumn('user', function($data) {
               $user = array(
                       "username"  => Auth::user()->username, 
                       "role"      => Auth::user()->level
                   );
               return $user;
           })
            ->addColumn('status_dokumen', function($data){
               $status = array (
                  "kontrak" =>  $data['kontrak_lkpp'] == null ? null : $data['kontrak_lkpp']['id'],
               );
               return $status;
            })
            ->make(true);
   }
   
   public function getStatusTransaksi(Request $request)
   {
      $data = $this->model->scopeStatusTransaksi($request->month, $request->year)->get();   
      return DataTables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('user', function($data) {
               $user = array(
                       "username"  => Auth::user()->username, 
                       "role"      => Auth::user()->level
                   );
               return $user;
           })
           ->addColumn('key', function($data){
            $date = date("Ym", strtotime($data->created_at));
            if($data->revisi >= 1){

               return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
            }else{
               return "BAKN-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);

            }
         })
            ->addColumn('status_dokumen', function($data){
               $status = array (
                  "kontrak" =>  $data['kontrak_lkpp'] == null ? null : $data['kontrak_lkpp']['id'],
               );
               return $status;
            })
            ->make(true);
   }
   
   public function getTrackingDocument(Request $request)
   {
      $data = $this->model->scopeTrackingDocument($request->month, $request->year)->get();
      return DataTables()
            ->of($data)
            ->addIndexColumn()
            ->addColumn('key', function($data){
               $date = date("Ym", strtotime($data->created_at));
               if($data->revisi >= 1){

                  return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
               }else{
                  return "BAKN-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);

               }
            })
            ->addColumn('action', function($data){
               $edit =array(
                  "id"        => $data->id,
                  "roles"     => Auth::user()->level,
                  "posisi"    => $data->approval,
                  "pembuat"   => $data->created_by,
               );
               return  $edit;
            })
            ->make(true);
   }

   public function create()
   {
      $spph          = $this->spph 
                        ->where('status','!=','draft_spph')
                        ->get();
      $io            = $this->io->all();
      $jenispasal    = $this->jenispasal->all();
      $bayar         = $this->bayar->all();
      $peserta       = $this->user
                        ->where(function($query){
                           $query->where('level', 'like','mgr%')
                           ->orWhere('level','like','avp%')
                           ->orWhere('level','like','gm%')
                           ->orWhere('level','like','vp%');
                        })->where('id_unit',56)
                        ->pluck('name');
      return view('modules.bakn_lkpp.create',compact('io','spph','jenispasal','bayar','peserta'));
   }

   public function store(BaknLkppRequest $request)
   {
      DB::beginTransaction();

      // Declare model chat
      $chat = $this->chatBakn;

      // Declare data 
      $data             = $request->except('_token');
      $data['lampiran'] = $request->file('lampiran') != null ? $request->file('lampiran') : NULL;

      try{
         // Call function insert
         $model = $this->model->insertBaknLkpp($data);

         // compare status submit
         if($model->status == 'save_bakn'){

            // insert data chat
            $chat->insertChat($model, $data['chat']);
            // Call Function approval
            $this->model->getValueApproval($model);

            DB::commit();
            return redirect('/status-transaksi-bakn-lkpp/')
            ->with(
               'success',
               'BAKN with No.SPPH ('.$model->spph_lkpps["nomorspph"].') was added successfully'
            );
         }else{
            DB::commit();
            return redirect('draft-bakn-lkpp')
            ->with(
               'success', 
               'BAKN with No.SPPH ('.$model->spph_lkpps["nomorspph"].') was added successfully'
            );
         }
      } catch (ValidationException $e) {
         DB::rollback();
         return redirect()->back()
         ->withErrors($e->getMessage())
         ->withInput();
     } catch (\Exception $e) {
         DB::rollback();
         return redirect()->back()
        ->withErrors($e->getMessage())
        ->withInput();
     }
   }

   public function preview(BaknLKPP $baknLKPP)
   {
      $bakn             = $baknLKPP;
      $checkbox         = explode(",", $baknLKPP->tipe_rapat);
      $checkbox_mitra   = explode(",", $baknLKPP->peserta_mitra);
     
      return view('modules.bakn_lkpp.preview',compact(
               'bakn',
               'baknLKPP',
               'checkbox',
               'checkbox_mitra')
            );
   }

   public function edit(BaknLKPP $baknLKPP)
   {

      $checkbox   = explode(",", $baknLKPP->tipe_rapat);
      $io         = $this->io->all();
      $jenispasal = $this->jenispasal->all();
      $bayar      = $this->bayar->all();
      $spph       = $this->spph
                     ->where('status','!=','draft_spph')
                     ->orWhereHas('bakn_lkpps', function($q){
                        $q->where('status','draft_bakn');
                     })
                     ->get();
      $chat       = $this->chatBakn
                     ->where('id_bakn_lkpp',$baknLKPP->id)
                     ->orderBy('created_at','desc')
                     ->first();

      return view('modules.bakn_lkpp.edit-new',compact(
                  'baknLKPP',
                  'spph',
                  'io',
                  'jenispasal',
                  'bayar',
                  'checkbox',
                  'chat'));
   }

   public function update(BaknLkppRequest $request, BaknLKPP $baknLKPP)
   {
      DB::beginTransaction();

      // Declare model chat
      $chat             = $this->chatBakn;

      // Declare data 
      $data             = $request->all();
      $data['lampiran'] = $request->file('lampiran') != null ? $request->file('lampiran') : NULL;

      try{
          // Call function insert
         $model = $this->model->updateBaknLkpp($data,$baknLKPP);

         // compare status submit
         if($model->status == 'save_bakn'){
            if($model->hold == true ){
               DB::commit();
               return redirect('/inprogress-bakn-lkpp/')
                        ->with(
                           'success',
                           'BAKN with No.SPPH ('.$model->spph_lkpps["nomorspph"].') was edited successfully'
                        );
            }

            // insert data chat
            $chat->insertChat($model, $data['chat']);
            $approve = $this->model->getValueApproval($model);
            // Call Function approval
            DB::commit();
            return redirect('/status-transaksi-bakn-lkpp/')
                     ->with(
                        'success',
                        'BAKN with No.SPPH ('.$model->spph_lkpps["nomorspph"].') was added successfully, and Notification has been '.$approve->original['sent']
                     );
         }else{
            DB::commit();
            return redirect('/draft-bakn-lkpp/')
                     ->with(
                        'success', 
                        'BAKN with No.SPPH ('.$model->spph_lkpps["nomorspph"].') was added successfully'
                     );
         }
      } catch (ValidationException $e) {
            DB::rollBack();
            return redirect()->back()
            ->withErrors($e->getMessage())
            ->withInput();
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()
           ->withErrors($e->getMessage())
           ->withInput();
        }
   }

   public function preview_status(BaknLKPP $baknLKPP)
   {
      $bakn       = $baknLKPP;  //declare ulang karena terdapat update field pada update bulan juli
      $checkbox   = explode(",", $baknLKPP->tipe_rapat);
      $user       = $this->user->all();
      $spphLKPP   = $this->spph->find($baknLKPP->spph_id);
      $chats      = DB::table('chat_bakn_lkpp')
                     ->join('users','chat_bakn_lkpp.username','=','users.username')
                     ->select('chat','jabatan','chat_bakn_lkpp.username','users.name','chat_bakn_lkpp.created_at')
                     ->where('id_bakn_lkpp',$baknLKPP->id)
                     ->orderBy('chat_bakn_lkpp.created_at','asc')
                     ->get();
      $pic        = DB::table('spph_lkpps')
                     ->join('users','spph_lkpps.pic','=','users.name')
                     ->select('spph_lkpps.pic', 'users.*')
                     ->where('spph_lkpps.id',$baknLKPP->spph_id)
                     ->first();
      $dari       = DB::table('spph_lkpps')
                     ->join('users','spph_lkpps.dari','users.name')
                     ->select('spph_lkpps.dari', 'users.*')
                     ->where('spph_lkpps.id', $baknLKPP->spph_id)
                     ->first();
      return view('modules.bakn_lkpp.preview_status',compact(
                  'bakn',
                  'baknLKPP',
                  'user',
                  'chats',
                  'checkbox',
                  'spphLKPP',
                  'dari',
                  'pic')
               );
   } 

     // function upoad file where status save
     public function file(Request $request, BaknLKPP $baknLKPP)
     {
         // declare variable array 
         $data = $request->all();
         $data['file']   = $request->file('file') != null ? $request->file('file') : NULL;
 
         try{
             // send data to model for upload file 
             $this->model->uploadFile($data, $baknLKPP);
             DB::commit();            
             return redirect('/done-bakn-lkpp/')->with('success', 'Your files has been successfully uploaded');
         } catch (Exception $e) {
             DB::rollback();
             return redirect()->back()
            ->withErrors($e->getMessage())
            ->withInput();
         }
 
     }
 
     public function lampiran(Request $request, BaknLKPP $baknLKPP)
     {
         // declare data to array 
         $data   = $request->all(); 
         try{
             // send data to model for upload lampiran 
             $this->model->uploadLampiran($data, $baknLKPP);    
             DB::commit();
             return redirect()->back()->with('success', 'Your lampirans has been successfully uploaded');
         } catch (\Exception $e) {
             DB::rollback();
             return redirect()->back()
            ->withErrors($e->getMessage())
            ->withInput();
         }
     }

   public function destroy($id)
   {
      $data          = BaknLKPP::find($id);
      $pathLampiran  = "public/files/BAKN_LKPP/$id/lampiran";
      $pathFile      = "public/files/BAKN_LKPP/$id/lampiran";
      Storage::deleteDirectory($pathLampiran);
      Storage::deleteDirectory($pathFile);
      $data->delete();
      return response($data);
   }

   public function holdOrRelease(BaknLKPP $baknLKPP)
   {
      if($baknLKPP->hold == true){
         $baknLKPP->hold = false;
      }else{
         $baknLKPP->hold = true;
      }
      $baknLKPP->save();
      return response($baknLKPP);
   }

   public function chatApproval(Request $request, BaknLKPP $baknLKPP)
   {

      if(Auth::user()->username != $baknLKPP->approval){
         return redirect()
               ->back()
               ->withErrors("Sorry You don't deserve to approve this document");
      }

      DB::beginTransaction();

      $data          = $request->all();
      $arrApproval   = json_decode($baknLKPP->new_approval, TRUE);

      try{
         // insert chat 
         $this->chatBakn->isApprove($data, $baknLKPP);
         // condtion approval is return 
         if($data['status'] == 'Return'){
            $approve = $this->model->approvalReturn($baknLKPP);
         }
         // condition approval is Approve
         if($data['status'] == 'Approve'){
            //condition if function approval masih cara lama
            if($baknLKPP->new_approval == NULL){
               $approve = $this->model->nextApprovalOld($baknLKPP);
               DB::commit();
               return redirect('inprogress-bakn-lkpp')
                        ->with(
                           'success', 
                           'BAKN with No.SPPH ('.$baknLKPP->spph_lkpps->nomorspph.') has been '. $data['status'].', and Notification has been '.$approve->original['sent']
                        );
            }
            //condition jika function approval menggunakan json
            if(Auth::user()->username != end($arrApproval)){
               $approve = $this->model->nextApprovalNew($baknLKPP);
            }
            // condition approval selesai
            if(Auth::user()->username == end($arrApproval) || Auth::user()->username == $baknLKPP->end_approval){
               $approve = $this->model->approvalClosed($baknLKPP);
            }  
         }
         DB::commit();
         return redirect('inprogress-bakn-lkpp')
                  ->with(
                     'success', 
                     'BAKN with No.SPPH ('.$baknLKPP->spph_lkpps->nomorspph.') has been '. $data['status'].', and Notification has been '.$approve->original['sent']
                  );
      }catch(Exception $e){
         DB::rollback();
         return redirect()
                  ->back()
                  ->withErrors($e->getMessage());
      }
   }

   
   
}