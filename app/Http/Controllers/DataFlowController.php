<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DataFlow;
use App\Models\Unit;
use App\Models\Role;
use App\User;
class DataFlowController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        //
        $dtflow = DataFlow::all();
        $users = User::all();
        return view('modules.flow.index',compact('dtflow', 'users'));
        
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
        $units = Unit::all();
        $roles = Role::all();
        $users = User::all();
        $flows = DataFlow::all();
        // $lvldesc = $this->leveldesc();
        return view('modules.flow.create', compact('roles','units','users','flows'));
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        //
        $flow = new DataFlow();
        $flow->jabatan = $request->get('jabatan');
        $flow->username = $request->get('username');
        $flow->unit = $request->get('unit');
        $flow->min = str_replace(".","",$request->input('min'));
        $flow->max = str_replace(".","",$request->input('max'));
        $flow->transaksi = $request->get('transaksi');
        $flow->save();
        
        return redirect()->route('flow.index')->with('success','Flow ('.$flow->username.' - '.$flow->jabatan.') was added Successfully');
        
    }
    
   
    public function edit($id)
    {
        //
    }
    
   
    public function update(Request $request, $id)
    {
        //
    }
    
    public function destroy($id)
    {
        //
    }
}
