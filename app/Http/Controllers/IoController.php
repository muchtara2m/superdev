<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Iodesc;

class IoController extends Controller
{

    protected $model;

    public function __construct()
    {
        $this->model = new Iodesc();    
    }

    public function dataIoAll()
    {
        return $this->model->getDataIo();
    }
}