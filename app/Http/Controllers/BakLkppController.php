<?php

namespace App\Http\Controllers;

use App\Http\Requests\BakLkppRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Models\BakLkpp;
use App\Models\SpphLKPP;
use App\User;
use App\Models\ChatBakLkpp;
use App\Models\DataFlow;
use App\Models\Ketentuans;
use Exception;
use Illuminate\Validation\ValidationException;

class BakLkppController extends Controller
{
    protected $model;
    protected $chatBak;
    protected $listApproval;
    protected $ketentuans;


    public function __construct()
    {
        $this->model        = new BakLkpp();
        $this->chatBak      = new ChatBakLkpp();
        $this->listApproval = new DataFlow();
        $this->ketentuans   = new Ketentuans();

    }

    public function getAllData(Request $request)
    {
        $data = $this->model->getAllData($request->month, $request->year)->get();
        return DataTables()->of($data)
                ->addIndexColumn()
                ->addColumn('key', function($data){
                    $date = date("Ym", strtotime($data->created_at));
                    if($data->revisi >= 1){
     
                       return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
                    }else{
                       return "BAK-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);
     
                    }
                 })
                ->make(true);
    }

    public function getDraftData(Request $request)
    {
        $data = $this->model->getDraftData($request->month, $request->year)->get();
        return DataTables()->of($data)
                ->addIndexColumn()
                ->addColumn('key', function($data){
                    $date = date("Ym", strtotime($data->created_at));
                    if($data->revisi >= 1){
     
                       return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
                    }else{
                       return "BAK-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);
     
                    }
                 })
                ->addColumn('user', function($data){
                    $user = array(
                            "username"  => Auth::user()->username, 
                            "role"      => Auth::user()->level
                        );
                    return $user;
                })
                ->make(true);
    }

    public function getListData(Request $request)
    {
        $data = $this->model->getListData($request->month, $request->year)->get();
        return DataTables()->of($data)
                ->addIndexColumn()
                ->addColumn('key', function($data){
                    $date = date("Ym", strtotime($data->created_at));
                    if($data->revisi >= 1){
     
                       return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
                    }else{
                       return "BAK-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);
     
                    }
                 })
                ->make(true);
    }

    public function getDoneData(Request $request)
    {
        $data = $this->model->getDoneData($request->month, $request->year)->get();
        return DataTables()->of($data)
                ->addIndexColumn()
                ->addColumn('key', function($data){
                    $date = date("Ym", strtotime($data->created_at));
                    if($data->revisi >= 1){
     
                       return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
                    }else{
                       return "BAK-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);
     
                    }
                 })
                ->make(true);
    }

    public function getInprogressData(Request $request)
    {
        $data = $this->model->getInprogressData($request->month, $request->year)->get();;
        return DataTables()->of($data)
                ->addIndexColumn()
                ->addColumn('key', function($data){
                    $date = date("Ym", strtotime($data->created_at));
                    if($data->revisi >= 1){
     
                       return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
                    }else{
                       return "BAK-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);
     
                    }
                 })
                 ->addColumn('user', function($data) {
                    $user = array(
                            "username"  => Auth::user()->username, 
                            "role"      => Auth::user()->level
                        );
                    return $user;
                })
                ->make(true);
    }

    public function getStatusTransaksi(Request $request)
    {
        $data = $this->model->getStatusTransaksiData($request->month, $request->year)->get();;
        return DataTables()->of($data)
                ->addIndexColumn()
                ->addColumn('key', function($data){
                    $date = date("Ym", strtotime($data->created_at));
                    if($data->revisi >= 1){
     
                       return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
                    }else{
                       return "BAK-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);
     
                    }
                 })
                ->addColumn('user', function($data) {
                    $user = array(
                            "username"  => Auth::user()->username, 
                            "role"      => Auth::user()->level
                        );
                    return $user;
                })
                ->make(true);
    }

    public function getTrackingDocument(Request $request)
    {
        $data = $this->model->getTrackingDocumentData($request->month, $request->year)->get();
        return DataTables()->of($data)
                ->addIndexColumn()
                ->addColumn('key', function($data){
                    $date = date("Ym", strtotime($data->created_at));
                    if($data->revisi >= 1){
     
                       return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
                    }else{
                       return "BAK-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);
     
                    }
                 })
                ->make(true);
    }

    public function store(BakLkppRequest $request)
    {
        DB::beginTransaction();
        // Declare data 
        $data             = $request->all();
        $data['lampiran'] = $request->file('lampiran') != null ? $request->file('lampiran') : NULL;

        try{
            // Call function insert
            $model = $this->model->insertBakLkpp($data);

            // compare status submit
            if($model->status == 'save_bak'){

                // insert data chat
                $this->chatBak->insertChatBak($model, $data['chat']);
  
                // Call Function approval
                $this->model->getValueApproval($model->id);
                 
                DB::commit();
                 
                return redirect('/inprogress-bak-lkpp')
                        ->with(
                            'success',
                            'BAK with No ('.$model->nomor_bak.') was added successfully'
                        );
            }else{
                DB::commit();
                return redirect('/draft-bak-lkpp')
                        ->with(
                            'success',
                            'BAK with No ('.$model->nomor_bak.') was added successfully'
                        );
            }
        } catch(\Exception $e){
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage())->withInput();
        } 
    }

    public function update(BakLkppRequest $request, BakLkpp $bakLkpp)
    {   
        DB::beginTransaction();
        // Declare data 
        $data             = $request->all();
        $data['lampiran'] = $request->file('lampiran') != null ? $request->file('lampiran') : NULL;

        try{
            // Call function insert
            $model = $this->model->updateBakLkpp($data,$bakLkpp);

            if($model->hold == true ){
                DB::commit();
                return redirect('/inprogress-bak-lkpp/')
                         ->with(
                            'success',
                            'BAK with No. ('.$model->nomor_bak.') was edited successfully'
                         );
             }

            // compare status submit
            if($model->status == 'save_bak')
            {
                // insert data chat
                $this->chatBak->insertChatBak($model, $data['chat']);

                // Call Function approval
                $this->model->getValueApproval($bakLkpp);
                
                DB::commit();
                return redirect('/status-transaksi-bak-lkpp/')
                    ->with(
                        'success',
                        'BAK with No ('.$model->nomor_bak.') was updated successfully'
                    );
            }else{
                DB::commit();
                return redirect('/draft-bak-lkpp/')
                    ->with(
                        'success', 
                        'BAK with No ('.$model->nomor_bak.') was update successfully'
                    );
            }
        }catch(\Exception $e){
            DB::rollBack();
            return redirect()
                    ->back()
                    ->withErrors($e->getMessage())
                    ->withInput();
        }
    }

     // function upoad file where status save
     public function file(Request $request, BakLKPP $bakLkpp)
     {
         // declare variable array 
         $data = $request->all();
         $data['file']   = $request->file('file') != null ? $request->file('file') : NULL;
 
         try{
             // send data to model for upload file 
             $this->model->uploadFile($data, $bakLkpp);
             DB::commit();            
             return redirect('/done-bak-lkpp/')->with('success', 'Your files has been successfully uploaded');
         } catch (Exception $e) {
             DB::rollback();
             return redirect()->back()
            ->withErrors($e->getMessage())
            ->withInput();
         }
 
     }
 
     public function lampiran(Request $request, BakLKPP $bakLkpp)
     {
         // declare data to array 
         $data   = $request->all(); 
         try{
             // send data to model for upload lampiran 
             $this->model->uploadLampiran($data, $bakLkpp);    
             DB::commit();
             return redirect()->back()->with('success', 'Your lampirans has been successfully uploaded');
         } catch (\Exception $e) {
             DB::rollback();
             return redirect()->back()
            ->withErrors($e->getMessage())
            ->withInput();
         }
     }

    public function destroy($id)
    {
        // declare model
        $data         = BakLKPP::find($id);
        // get path lampiran and file 
        $path = "public/files/BAK_LKPP/$id";
        // delete file & lampiran from storage
        Storage::deleteDirectory($path);
        // delete
        $data->delete();
        return response($data);
    }

    public function create()
    {    
        return view('modules.bak_lkpp.create',
        array(
            'garansi' => $this->ketentuans->allData(),
        ));
    }
 
    public function edit(BakLkpp $bakLkpp){
        return view('modules.bak_lkpp.edit',compact('bakLkpp'));
    }

    public function holdOrRelease(BakLkpp $bakLkpp)
    {
        if($bakLkpp->hold == true){
            $bakLkpp->hold = false;
        }else{
            $bakLkpp->hold = true;
        }
        $bakLkpp->save();
        return response($bakLkpp);
    }

    public function preview(BakLkpp $bakLkpp){
        return view('modules.bak_lkpp.preview', compact('bakLkpp'));
    }

    public function preview_status(BakLkpp $bakLkpp)
    {
        $spphLKPP   = $bakLkpp->spph_bak_lkpp;
        $chats  = DB::table('chat_bak_lkpps')
                ->join('users','chat_bak_lkpps.username','=','users.username')
                ->select('chat','jabatan','chat_bak_lkpps.username','users.name','chat_bak_lkpps.created_at')
                ->where('id_bak_lkpp',$bakLkpp->id)
                ->orderBy('chat_bak_lkpps.created_at','asc')
                ->get();

        $pic    = DB::table('spph_lkpps')
                ->join('users','spph_lkpps.pic','=','users.name')
                ->select('spph_lkpps.pic', 'users.*')
                ->where('spph_lkpps.id',$bakLkpp->spph_id)
                ->first();

        $dari   =  DB::table('spph_lkpps')
                ->join('users','spph_lkpps.dari','users.name')
                ->select('spph_lkpps.dari', 'users.*')
                ->where('spph_lkpps.id', $bakLkpp->spph_id)
                ->first();

       return view('modules.bak_lkpp.preview-status',compact('bakLkpp','spphLKPP','chats','dari','pic'));
    }

 
    public function chatApproval(Request $request, BakLkpp $bakLkpp)
   {

      if(Auth::user()->username != $bakLkpp->approval){
         return redirect()
               ->back()
               ->withErrors("Sorry You don't deserve to approve this document");
      }

      DB::beginTransaction();

      $data          = $request->all();
      $arrApproval   = json_decode($bakLkpp->new_approval, TRUE);

      try{
         // insert chat 
         $this->chatBak->isApprove($data, $bakLkpp);

         // condtion approval is return 
         if($data['status'] == 'Return'){
            $approve = $this->model->approvalReturn($bakLkpp);
         }

         // condition approval is Approve
         if($data['status'] == 'Approve'){
            //condition if function approval masih cara lama
            if($bakLkpp->new_approval == NULL){   
               $approve = $this->model->nextApprovalOld($bakLkpp);
               DB::commit();
               return redirect('inprogress-bak-lkpp')
                        ->with(
                           'success', 
                           'BAK with No. ('.$bakLkpp->nomor_bak.') has been '. $data['status'].', and Notification has been '.$approve->original['sent']
                        );
            }
            //condition jika function approval menggunakan json
            if(Auth::user()->username != end($arrApproval)){
               $approve = $this->model->nextApprovalNew($bakLkpp);
            }
            // condition approval selesai
            if(Auth::user()->username == end($arrApproval) || Auth::user()->username == $bakLkpp->end_approval){
               $approve = $this->model->approvalClosed($bakLkpp);
            }  
         }
         DB::commit();
         return redirect('inprogress-bak-lkpp')
                  ->with(
                     'success', 
                     'BAK with No. ('.$bakLkpp->nomor_bak.') has been '. $data['status'].', and Notification has been '.$approve->original['sent']
                  );
      }catch(Exception $e){
         DB::rollback();
         return redirect()
                  ->back()
                  ->withErrors($e->getMessage());
      }
   }


   public function chatApprovalss(Request $request, BakLkpp $bakLkpp)
   {
       $this->validate($request, [
           'chat' => 'required',
           ]
       );
       // begin 
       DB::beginTransaction();
       // add data to array
       $data           = $request->all();
       $isSubmitButton = Auth::user()->username;
       try{
           // condition if isSubmit different with approval 
           if($isSubmitButton == $bakLkpp->approval){
               
               // insert data chat
               $this->chatBak->isApprove($data, $bakLkpp);
               
               // condition if status button return or submit
               if($data['status'] == 'Return'){
                   
                   // call function approval return
                   $this->model->approvalReturn($bakLkpp);
                   DB::commit();
                   return redirect('status-transaksi-bak-lkpp')->with('success',
                       'BAK with No.BAK ('.$bakLkpp->nomor_bak.') has been Return by '.Auth::user()->name
                   );

               // condition approval equals with end approval
               }else if($bakLkpp->approval == $bakLkpp->end_approval){

                    // call function approval closed
                    $this->model->approvalClosed($bakLkpp);
                    DB::commit(); 
                    return redirect('status-transaksi-bak-lkpp')->with('success',
                        'BAK with No.BAK ('.$bakLkpp->nomor_bak.') has been Approved by '.Auth::user()->name
                    );
                   // condition flow approval
               }else{
                   
                   // condition if this document use old flow or new flow
                   if($bakLkpp->new_approval == "null"){
                       // concition old flow
                       $this->model->nextApprovalOld($bakLkpp);
                       DB::commit();
                       return redirect('status-transaksi-bak-lkpp')->with('success',
                           'BAK with No.BAK ('.$bakLkpp->nomor_bak.') has been Approved by '.Auth::user()->name
                       );
                   }else{
                       // condition new flow
                       $this->model->nextApprovalNew($bakLkpp);
                       DB::commit();
                       return redirect('status-transaksi-bak-lkpp')->with('success',
                           'BAK with No.BAK ('.$bakLkpp->nomor_bak.') has been Approved by '.Auth::user()->name
                       );
                   }
               }
           }else{
               return redirect()->back()->with('success',
                   "Sorry, you don't deserve to approve this document"
               );
           }
       }catch(\Exception $e){
           DB::rollBack();
           return redirect()
               ->back()
               ->withError($e->getMessage())
               ->withInput();
       }      
   }
}
