<?php

namespace App\Http\Controllers;

use App\Models\AR;
use App\Models\ChatAR;
use App\Models\Customer;
use App\User;
use App\Models\Iodesc;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use DataTables;

class ARController extends Controller
{
    // data dashboard
    public function dashboard(Request $request)
    {   
        if (request()->ajax()) {
            if (!empty($request->bln)) {
                $m = $request->bln;
                $y = $request->thn;
                $d = date('d');
                $dt = $y.'-'.$m.'-'.$d;
                $lastWeek = date('Y-m-d',(strtotime ( '-6 day' , strtotime ( $dt) ) ));
            }else{
                $m = date('m');
                $y = date('y');
                $d = date('d');
                $dt = date('Y-m-d');
                $lastWeek = date('Y-m-d',(strtotime ( '-6 day' , strtotime ( date('Y-m-d')) ) ));
            }
            // sum of nilai invoice by status
            $nilaiunbill = DB::table('ars')
            ->where('status','unbill')
            ->whereMonth('created_at', $m)
            ->whereYear('created_at',$y)
            ->sum('nilai_project');
            $nilaibill = DB::table('ars')
            ->where('status','bill')
            ->whereMonth('created_at', $m)
            ->whereYear('created_at',$y)
            ->sum('nilai_invoice');
            $nilaireadytobill = DB::table('ars')
            ->where('status','readytobill')
            ->whereMonth('created_at', $m)
            ->whereYear('created_at',$y)
            ->sum('nilai_project');

            // var nilai unbill = nilai revenue - (bill + paid);
            // count get now
           $spk = DB::select('SELECT COUNT(CASE WHEN spk IS NULL THEN 1 ELSE 0 END) As spk FROM ars where spk IS NULL AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $kl = DB::select('SELECT COUNT(CASE WHEN kl IS NULL THEN 1 ELSE 0 END) As kl FROM ars where kl IS NULL AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $baut = DB::select('SELECT COUNT(CASE WHEN baut IS NULL THEN 1 ELSE 0 END) As baut FROM ars where baut IS NULL AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $bast = DB::select('SELECT COUNT(CASE WHEN bast IS NULL THEN 1 ELSE 0 END) As bast FROM ars where bast IS NULL AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $baso = DB::select('SELECT COUNT(CASE WHEN baso IS NULL THEN 1 ELSE 0 END) As baso FROM ars where baso IS NULL AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $bapp = DB::select('SELECT COUNT(CASE WHEN bapp IS NULL THEN 1 ELSE 0 END) As bapp FROM ars where bapp IS NULL AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $lpp = DB::select('SELECT COUNT(CASE WHEN lpp IS NULL THEN 1 ELSE 0 END) As lpp FROM ars where lpp IS NULL AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $baperub = DB::select('SELECT COUNT(CASE WHEN baperub IS NULL THEN 1 ELSE 0 END) As baperub FROM ars where baperub IS NULL AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $barekon = DB::select('SELECT COUNT(CASE WHEN barekon IS NULL THEN 1 ELSE 0 END) As barekon FROM ars where barekon IS NULL AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $performansi = DB::select('SELECT COUNT(CASE WHEN performansi IS NULL THEN 1 ELSE 0 END) As performansi FROM ars where performansi IS NULL AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $npk = DB::select('SELECT COUNT(CASE WHEN npk IS NULL THEN 1 ELSE 0 END) As npk FROM ars where npk IS NULL AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $kb = DB::select('SELECT COUNT(CASE WHEN kb IS NULL THEN 1 ELSE 0 END) As kb FROM ars where kb IS NULL AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $basocus = DB::select('SELECT COUNT(CASE WHEN basocus IS NULL THEN 1 ELSE 0 END) As basocus FROM ars where basocus IS NULL AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           // get last week 
           $lwspk = DB::select('SELECT COUNT(CASE WHEN spk IS NULL THEN 1 ELSE 0 END) As spk FROM ars where (spk IS NOT NULL OR date(spk)= 1111-11-11) AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $lwkl = DB::select('SELECT COUNT(CASE WHEN kl IS NULL THEN 1 ELSE 0 END) As kl FROM ars where (kl IS NOT NULL OR date(kl)= 1111-11-11) AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $lwbaut = DB::select('SELECT COUNT(CASE WHEN baut IS NULL THEN 1 ELSE 0 END) As baut FROM ars where (baut IS NOT NULL OR date(baut)= 1111-11-11) AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $lwbast = DB::select('SELECT COUNT(CASE WHEN bast IS NULL THEN 1 ELSE 0 END) As bast FROM ars where (bast IS NOT NULL OR date(bast)= 1111-11-11) AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $lwbaso = DB::select('SELECT COUNT(CASE WHEN baso IS NULL THEN 1 ELSE 0 END) As baso FROM ars where (baso IS NOT NULL OR date(baso)= 1111-11-11) AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $lwbapp = DB::select('SELECT COUNT(CASE WHEN bapp IS NULL THEN 1 ELSE 0 END) As bapp FROM ars where (bapp IS NOT NULL OR date(bapp)= 1111-11-11) AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $lwlpp = DB::select('SELECT COUNT(CASE WHEN lpp IS NULL THEN 1 ELSE 0 END) As lpp FROM ars where (lpp IS NOT NULL OR date(lpp)= 1111-11-11) AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $lwbaperub = DB::select('SELECT COUNT(CASE WHEN baperub IS NULL THEN 1 ELSE 0 END) As baperub FROM ars where (baperub IS NOT NULL OR date(baperub)= 1111-11-11) AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $lwbarekon = DB::select('SELECT COUNT(CASE WHEN barekon IS NULL THEN 1 ELSE 0 END) As barekon FROM ars where (barekon IS NOT NULL OR date(barekon)= 1111-11-11) AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $lwperformansi = DB::select('SELECT COUNT(CASE WHEN performansi IS NULL THEN 1 ELSE 0 END) As performansi FROM ars where (performansi IS NOT NULL OR date(performansi)= 1111-11-11) AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $lwnpk = DB::select('SELECT COUNT(CASE WHEN npk IS NULL THEN 1 ELSE 0 END) As npk FROM ars where (npk IS NOT NULL OR date(npk)= 1111-11-11) AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $lwkb = DB::select('SELECT COUNT(CASE WHEN kb IS NULL THEN 1 ELSE 0 END) As kb FROM ars where (kb IS NOT NULL OR date(kb)= 1111-11-11) AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $lwbasocus = DB::select('SELECT COUNT(CASE WHEN basocus IS NULL THEN 1 ELSE 0 END) As basocus FROM ars where (basocus IS NOT NULL OR date(basocus)= 1111-11-11) AND MONTH(created_at)='.$m.' AND YEAR(created_at)='.$y);
           $countNull =$spk[0]->spk + $kl[0]->kl + $baut[0]->baut +$bast[0]->bast + $baso[0]->baso + $bapp[0]->bapp + $lpp[0]->lpp + $baperub[0]->baperub + $barekon[0]->barekon + $performansi[0]->performansi + $npk[0]->npk+ $kb[0]->kb+ $basocus[0]->basocus;
            $lw =$lwspk[0]->spk + $lwkl[0]->kl + $lwbaut[0]->baut +$lwbast[0]->bast + $lwbaso[0]->baso + $lwbapp[0]->bapp + $lwlpp[0]->lpp + $lwbaperub[0]->baperub + $lwbarekon[0]->barekon + $lwperformansi[0]->performansi + $lwnpk[0]->npk + $lwkb[0]->kb + $lwbasocus[0]->basocus;
            $countNotNull= $lw + $countNull;
            //count total data ar
            $total = $nilaibill+$nilaiunbill+$nilaireadytobill;
            return response()->json(
                [
                    'countLast'=>$countNotNull,
                    'now'=>$dt,
                    'lastWeek'=>$lastWeek,
                    'count'=>$countNull, 
                    'total' => $total, 
                    'nilaiunbill' => $nilaiunbill, 
                    'nilaibill' => $nilaibill, 
                    'nilaireadytobill' => $nilaireadytobill
                ]);
        }
        return view('modules.ar.dashboard');
    }
            
            
    // ready to bill
    public function readytobill(Request $request)
    {
       
        // dd($update);
        if(request()->ajax()){
            if (!empty($request->bln)) {
                $bln = $request->bln;
                $thn = $request->thn;
            } else {
                $bln = date('m');
                $thn = date('Y');
            }
            $data = AR::with('dataio','pembuat','customer','pembuat.unitnya')
            ->where('status','readytobill')
            ->whereMonth('created_at', $bln)
            ->whereYear('created_at',$thn)
            ->get();
            return DataTables()->of($data) 
            ->addColumn('action', function ($data) {
                if(Auth::user()->level == 'administrator'){
                    return 
                    '<a href="'.url('preview_ar/'.$data->id).'" title="Edit AR"><i class="fa fa-file"></i></a> 
                    <a href="'.url('edit_ar/'.$data->id).'"><i class="fa fa-edit"></i></a> 
                    <a href="#"><i class="fa fa-trash"></i></a>';
                }else{
                    return 
                    '<a href="'.url('preview_ar/'.$data->id).'" title="Edit AR"><i class="fa fa-file"></i></a>';
                }
            })->editColumn('created_at', function ($data){
                return date('d.F.Y', strtotime($data->created_at) );
            })->editColumn('tgl_invoice', function ($data){
                if($data->tgl_invoice != NULL){
                    return date('d.F.Y', strtotime($data->tgl_invoice));
                }else{
                    return "";
                }
            })
            ->addIndexColumn()
            ->make(true); 
        }
        return view('modules.ar.readytobill');
    }
    // unbill
    public function index(Request $request)
    {  
        $update = DB::update("UPDATE ars SET status='readytobill', tglreadytobill = date('Y-m-d H:i:s') WHERE
        (spk AND
        kl AND
        kb AND
        baut AND
        bast AND
        baso AND
        basocus AND
        bapp AND
        npk AND
        lpp AND
        baperub AND
        barekon AND
        performansi ) IS NOT NULL AND status='unbill' ");  
        if(request()->ajax()){
            if (!empty($request->bln)) {
                $bln = $request->bln;
                $thn = $request->thn;
            } else {
                $bln = date('m');
                $thn = date('Y');
            }
            $data = AR::with('dataio','pembuat','customer','pembuat.unitnya')
            ->where('status','unbill')
            ->whereMonth('created_at', $bln)
            ->whereYear('created_at',$thn)
            ->get();
            return datatables()->of($data) 
            ->addColumn('action', function ($data) {
                if(Auth::user()->level == 'administrator'){
                    return 
                    '<a href="'.url('preview_ar/'.$data->id).'" title="Preview AR"><i class="fa fa-file"></i></a> 
                    <a href="'.url('edit_ar/'.$data->id).'" title="Edit AR"><i class="fa fa-edit"></i></a> 
                    <a href="#"><i class="fa fa-trash"></i></a>';
                }else{
                    return 
                    '<a href="'.url('preview_ar/'.$data->id).'" title="Preview AR"><i class="fa fa-file"></i></a>
                    <a href="'.url('edit_ar/'.$data->id).'" title="Edit AR"><i class="fa fa-edit"></i></a>'; 
                    
                }
            })->editColumn('created_at', function ($data){
                return date('d.F.Y', strtotime($data->created_at) );
            })->editColumn('tgl_invoice', function ($data){
                if($data->tgl_invoice != NULL){
                    return date('d.F.Y', strtotime($data->tgl_invoice));
                }else{
                    return "";
                }
            })
            ->addIndexColumn()
            ->make(true); 
        }
        return view('modules.ar.list');
    }
            
    // list bill
    public function bill(Request $request)
    {
        if (request()->ajax()) {
            if (!empty($request->bln)) {
                $bln = $request->bln;
                $thn = $request->thn;
            } else {
                $bln = date('m');
                $thn = date('Y');
            }
            $data = AR::with('dataio','pembuat','customer','pembuat.unitnya')
                ->where('status','bill')
                ->whereMonth('created_at', $bln)
                ->whereYear('created_at',$thn)
                ->get();
            return Datatables::of($data) 
            ->addColumn('action', function ($data) {
                if(Auth::user()->level == 'administrator'){
                    return 
                    '<a href="#modal" data-id="'.$data->id.'" data-toggle="modal" title="Upload File" class="upload fa fa-fw fa-file"></a> 
                    <a href="'.url('preview_ar/'.$data->id).'" title="Preview AR"><i class="fa fa-sticky-note-o"></i></a> 
                    <a href="#"><i class="fa fa-trash"></i></a>';
                }else{
                    return 
                    '<a href="#modal" data-id="'.$data->id.'" data-toggle="modal" title="Upload File" class="upload fa fa-fw fa-file"></a> 
                    <a href="'.url('preview_ar/'.$data->id).'" title="Preview AR"><i class="fa fa-sticky-note-o"></i></a> ';
                }
                
            })->editColumn('created_at', function ($data) 
            {
                return date('d.F.Y', strtotime($data->created_at) );
            })->editColumn('tgl_invoice', function ($data) 
            {
                if($data->tgl_invoice != NULL){
                    return date('d.F.Y', strtotime($data->tgl_invoice) );
                    
                }else{
                    return "";
                }
            })
            ->addIndexColumn()
            ->make(true);
        }
        return view('modules.ar.listbill');
    }
    // list paid
    public function paid(Request $request)
    {
        if (request()->ajax()) {
            if (!empty($request->bln)) {
                $bln = $request->bln;
                $thn = $request->thn;
            } else {
                $bln = date('m');
                $thn = date('Y');
            }
            $data = AR::with('dataio','pembuat','customer','pembuat.unitnya')
            ->where('status','paid')
            ->whereMonth('created_at', $bln)
            ->whereYear('created_at',$thn)
            ->get();
            return Datatables::of($data) 
            ->addColumn('action', function ($data) {
                if(Auth::user()->level == 'administrator'){
                    return 
                    '<a href="'.url('preview_ar/'.$data->id).'" title="Preview AR"><i class="fa fa-sticky-note-o"></i></a> 
                    <a href="#"><i class="fa fa-trash"></i></a>';
                }else{
                    return 
                    '<a href="'.url('preview_ar/'.$data->id).'" title="Preview AR"><i class="fa fa-sticky-note-o"></i></a> ';
                }
            })->editColumn('created_at', function ($data) 
            {
                return date('d.F.Y', strtotime($data->created_at) );
            })->editColumn('tgl_invoice', function ($data) 
            {
                if($data->tgl_invoice != NULL){
                    return date('d.F.Y', strtotime($data->tgl_invoice) );
                    
                }else{
                    return "";
                }
            })
            ->addIndexColumn()
            ->make(true);
        }
        return view('modules.ar.listpaid');
    }
    // list paid 100%
    public function paid100(Request $request)
    {
        if (request()->ajax()) {
            if (!empty($request->bln)) {
                $bln = $request->bln;
                $thn = $request->thn;
            } else {
                $bln = date('m');
                $thn = date('Y');
            }
            $data = AR::with('dataio','pembuat','customer','pembuat.unitnya')
            ->where('status','paid100')
            ->whereMonth('created_at', $bln)
            ->whereYear('created_at',$thn)
            ->get();
            return Datatables::of($data) 
            ->addColumn('action', function ($data) {
                if(Auth::user()->level == 'administrator'){
                    return 
                    '<a href="'.url('preview_ar/'.$data->id).'" title="Preview AR"><i class="fa fa-sticky-note-o"></i></a> 
                    <a href="#"><i class="fa fa-trash"></i></a>';
                }else{
                    return 
                    '<a href="'.url('preview_ar/'.$data->id).'" title="Preview AR"><i class="fa fa-sticky-note-o"></i></a> ';
                }
            })->editColumn('created_at', function ($data) 
            {
                return date('d.F.Y', strtotime($data->created_at) );
            })->editColumn('tgl_invoice', function ($data) 
            {
                if($data->tgl_invoice != NULL){
                    return date('d.F.Y', strtotime($data->tgl_invoice) );
                    
                }else{
                    return "";
                }
            })
            ->addIndexColumn()
            ->make(true);
        }
        return view('modules.ar.listpaid100');
    }
            
    // server side sdv
    public function json_sdv(Request $request){
        if (request()->ajax()) {
            if (!empty($request->bln)) {
                $bln = $request->bln;
                $thn = $request->thn;
            } else {
                $bln = date('m');
                $thn = date('Y');
            }
            $data = AR::with('dataio', 'pembuat', 'customer', 'pembuat.unitnya')->where('status', 'unbill')->where([
            ['baut',null],
            ['bast',null],
            ['baop',null],
            ['bapp',null],
            ['baperub',null],
            ])
            ->whereMonth('created_at', $bln)
            ->whereYear('created_at', $thn)
            ->get();
            return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return
                '<a href="'.url('preview_ar/'.$data->id).'"><i class="fa fa-file"></i></a> '.
                '<a href="'.url('edit_ar/'.$data->id).'"><i class="fa fa-edit"></i></a> '.
                '<a href="#"><i class="fa fa-trash"></i></a>';
            })->editColumn('created_at', function ($data) {
                return date('d.F.Y', strtotime($data->created_at));
            })->editColumn('tgl_invoice', function ($data) {
                if ($data->tgl_invoice != null) {
                    return date('d.F.Y', strtotime($data->tgl_invoice));
                } else {
                    return "";
                }
            })
            ->addIndexColumn()
            ->make(true);
        }
        return view('modules.ar.sdv');
    }
                // server side operation
    public function json_operation(Request $request){
        if (request()->ajax()) {
            if (!empty($request->bln)) {
                $bln = $request->bln;
                $thn = $request->thn;
            } else {
                $bln = date('m');
                $thn = date('Y');
            }
            $data = AR::with('dataio', 'pembuat', 'customer', 'pembuat.unitnya')->where('status', 'unbill') ->where([
            ['barekon',null],
            ['performansi',null],
            ['lpp',null],
            ])
            ->whereMonth('created_at', $bln)
            ->whereYear('created_at', $thn)
            ->get();
            return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return
                '<a href="'.url('preview_ar/'.$data->id).'"><i class="fa fa-file"></i></a> '.
                '<a href="'.url('edit_ar/'.$data->id).'"><i class="fa fa-edit"></i></a> '.
                '<a href="#"><i class="fa fa-trash"></i></a> ';
            })->editColumn('created_at', function ($data) {
                return date('d.F.Y', strtotime($data->created_at));
            })->editColumn('tgl_invoice', function ($data) {
                if ($data->tgl_invoice != null) {
                    return date('d.F.Y', strtotime($data->tgl_invoice));
                } else {
                    return "";
                }
            })
            ->addIndexColumn()
            ->make(true);
        }
        return view('modules.ar.operation');
    }
    // server side ubis
    public function json_ubis(Request $request){
        if (request()->ajax()) {
            if (!empty($request->bln)) {
                $bln = $request->bln;
                $thn = $request->thn;
            } else {
                $bln = date('m');
                $thn = date('Y');
            }
            $data = AR::with('dataio', 'pembuat', 'customer', 'pembuat.unitnya')->where('status', 'unbill') ->where([
            ['ep',null],
            ['npk',null],
            ['spk',null],
            ['kl',null],
            ])
            ->whereMonth('created_at', $bln)
            ->whereYear('created_at', $thn)
            ->get();
            return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return
                '<a href="'.url('preview_ar/'.$data->id).'"><i class="fa fa-file"></i></a> '.
                '<a href="'.url('edit_ar/'.$data->id).'"><i class="fa fa-edit"></i></a> '.
                '<a href="#"><i class="fa fa-trash"></i></a> ';
            })->editColumn('created_at', function ($data) {
                return date('d.F.Y', strtotime($data->created_at));
            })->editColumn('tgl_invoice', function ($data) {
                if ($data->tgl_invoice != null) {
                    return date('d.F.Y', strtotime($data->tgl_invoice));
                } else {
                    return "";
                }
            })
            ->addIndexColumn()
            ->make(true);
        }
        return view('modules.ar.ubis');
    }
                        
                        
    // function create AR
    public function create()
    {
        $customer = Customer::all();
        $io = Iodesc::all();
        $user = User::all();
        return view('modules.ar.create', compact('customer','io','user'));
    }
    // function ajax get data io from create AR
    function getdataio(Request $request){
        if($request->ajax()){
            $dataio = AR::with('dataio')->where('io_id', $request->io)->first();
            if($dataio == NULL){
                $dataio = "";
                $nilai = 0;
                return response()->json([
                    'options'=>$dataio,
                    'nilai' => $nilai,
                    ]);
                }else{
                    $project = AR::where('io_id', $request->io)->first();
                    $nilai = AR::where('io_id',$request->io)->select(
                        DB::raw('sum(nilai_project) as project'),
                        DB::raw('sum(nilai_invoice) as invoice'),
                        DB::raw('('.$project->nilai_project.' - sum(nilai_invoice)) as total')
                        )->first();
                        $dataio = $dataio;
                        return response()->json([
                            'options'=>$dataio,
                            'nilai' => $nilai,
                            ]);
                }
            }
    }
                                    
    public function store(Request $request)
    {
        
        $customMessages = [
            'required' => ':Attribute field is required.',
            'unique' => ':Attribute has already be taken',
        ];
        $this->validate($request, [
            // 'customer' => 'required',
            'io' => 'required',
            'nilai_project' => 'required',
            'uraian' => 'required',
            'endproject' => 'required',
        ], $customMessages);
        DB::beginTransaction();
        $ars = new AR();
        $ars->created_by = Auth::user()->id;
        $ars->customer_id = $request->input('customer');
        $ars->io_id = $request->input('io');
        $ars->nilai_project = str_replace(".","",$request->input('nilai_project'));
        $ars->uraian = $request->input('uraian');
        $ars->endproject = $request->input('endproject');
        $ars->status = $request->input('status');
        $ars->tglunbill = date('Y-m-d H:i:s');
        // dd($ars);
        // declare date if checklist
        if($request->input('spk') == 'on'){
            $ars->spk = date('Y-m-d H:i:s');
        }else if($request->input('spk') == 'spk'){
            $ars->spk = '1111-11-11 11:11:11';
        }
        if($request->input('kl') == 'on'){
            $ars->kl = date('Y-m-d H:i:s');
        }else if($request->input('kl') == 'kl'){
            $ars->kl = '1111-11-11 11:11:11';
        }
        if($request->input('kb') == 'on'){
            $ars->kb = date('Y-m-d H:i:s');
        }else if($request->input('kb') == 'kl'){
            $ars->kb = '1111-11-11 11:11:11';
        }
        if($request->input('bast') == 'on'){
            $ars->bast = date('Y-m-d H:i:s');
        }else if($request->input('bast') == 'bast'){
            $ars->bast = '1111-11-11 11:11:11';
        }
        if($request->input('baut') == 'on'){
            $ars->baut = date('Y-m-d H:i:s');
        }else if($request->input('baut') == 'baut'){
            $ars->baut = '1111-11-11 11:11:11';
        }
        if($request->input('bapp') == 'on'){
            $ars->bapp = date('Y-m-d H:i:s');
        }else if($request->input('bapp') == 'bapp'){
            $ars->bapp = '1111-11-11 11:11:11';
        }
        if($request->input('baso') == 'on'){
            $ars->baso = date('Y-m-d H:i:s');
        }else if($request->input('baso') == 'baso'){
            $ars->baso = '1111-11-11 11:11:11';
        }
         if($request->input('basocus') == 'on'){
            $ars->basocus = date('Y-m-d H:i:s');
        }else if($request->input('basocus') == 'basocus'){
            $ars->basocus = '1111-11-11 11:11:11';
        }
        if($request->input('npk') == 'on'){
            $ars->npk = date('Y-m-d H:i:s');
        }else if($request->input('npk') == 'npk'){
            $ars->npk = '1111-11-11 11:11:11';
        }
        if($request->input('baperub') == 'on'){
            $ars->baperub = date('Y-m-d H:i:s');
        }else if($request->input('baperub') == 'baperub'){
            $ars->baperub = '1111-11-11 11:11:11';
        }
        if($request->input('barekon') == 'on'){
            $ars->barekon = date('Y-m-d H:i:s');
        }else if($request->input('barekon') == 'barekon'){
            $ars->barekon = '1111-11-11 11:11:11';
        }
        if($request->input('performansi') == 'on'){
            $ars->performansi = date('Y-m-d H:i:s');
        }else if($request->input('performansi') == 'performansi'){
            $ars->performansi = '1111-11-11 11:11:11';
        }
        if($request->input('lpp') == 'on'){
            $ars->lpp = date('Y-m-d H:i:s');
        }else if($request->input('lpp') == 'lpp'){
            $ars->lpp = '1111-11-11 11:11:11';
        }
        // dd($ars);
        
        $ars->save();
        DB::commit();
        return redirect('unbill_ar')->with('Success', 'Data telah ditambahkan');
    }
                                    
    // preview ar
    public function show($id)
    {
        $ars = AR::with('pembuat','customer','pembuat.unitnya')->find($id);
        return view('modules.ar.preview', compact('ars'));
    }
                                    
                                    
    public function edit($id)
    {
        $chat = ChatAR::where('idAR',$id)->orderBy('created_at','desc')->first();
        $customer = Customer::all();
        $io = Iodesc::all();
        $user = User::all();
        $ars = AR::with('pembuat')->find($id);
        return view('modules.ar.edit', compact('customer','io','user','ars','chat'));
    }
                                    
    public function update(Request $request, $id)
    {
        $ars = AR::find($id);
        $ars->customer_id = $request->input('customer');
        $ars->io_id = $request->input('io');
        $ars->nilai_project = str_replace('.','',$request->input('nilai_project'));
        $ars->uraian = $request->uraian;
        $ars->status = $request->status;
        // dokumen
        // // declare date if checklist
        if($request->input('spk') == 'on'){
            $ars->spk = date('Y-m-d H:i:s');
        }else if($request->input('spk') == 'spk'){
            $ars->spk = '1111-11-11 11:11:11';
        }else{
            $ars->spk = NULL;
        }
        if($request->input('kl') == 'on'){
            $ars->kl = date('Y-m-d H:i:s');
        }else if($request->input('kl') == 'kl'){
            $ars->kl = '1111-11-11 11:11:11';
        }else{
            $ars->kl = NULL;
        }
        if($request->input('kb') == 'on'){
            $ars->kb = date('Y-m-d H:i:s');
        }else if($request->input('kb') == 'kb'){
            $ars->kb = '1111-11-11 11:11:11';
        }else{
            $ars->kb = NULL;
        }
        if($request->input('bast') == 'on'){
            $ars->bast = date('Y-m-d H:i:s');
        }else if($request->input('bast') == 'bast'){
            $ars->bast = '1111-11-11 11:11:11';
        }else{
            $ars->bast = NULL;
        }
        if($request->input('baut') == 'on'){
            $ars->baut = date('Y-m-d H:i:s');
        }else if($request->input('baut') == 'baut'){
            $ars->baut = '1111-11-11 11:11:11';
        }else{
            $ars->baut = NULL;
        }
        if($request->input('bapp') == 'on'){
            $ars->bapp = date('Y-m-d H:i:s');
        }else if($request->input('bapp') == 'bapp'){
            $ars->bapp = '1111-11-11 11:11:11';
        }else{
            $ars->bapp = NULL;
        }
        if($request->input('baso') == 'on'){
            $ars->baso = date('Y-m-d H:i:s');
        }else if($request->input('baso') == 'baso'){
            $ars->baso = '1111-11-11 11:11:11';
        }else{
            $ars->baso = NULL;
        }
        if($request->input('basocus') == 'on'){
            $ars->basocus = date('Y-m-d H:i:s');
        }else if($request->input('basocus') == 'basocus'){
            $ars->basocus = '1111-11-11 11:11:11';
        }else{
            $ars->basocus = NULL;
        }
        if($request->input('npk') == 'on'){
            $ars->npk = date('Y-m-d H:i:s');
        }else if($request->input('npk') == 'npk'){
            $ars->npk = '1111-11-11 11:11:11';
        }else{
            $ars->npk = NULL;
        }
        if($request->input('baperub') == 'on'){
            $ars->baperub = date('Y-m-d H:i:s');
        }else if($request->input('baperub') == 'baperub'){
            $ars->baperub = '1111-11-11 11:11:11';
        }else{
            $ars->baperub = NULL;
        }
        if($request->input('barekon') == 'on'){
            $ars->barekon = date('Y-m-d H:i:s');
        }else if($request->input('barekon') == 'barekon'){
            $ars->barekon = '1111-11-11 11:11:11';
        }else{
            $ars->barekon = NULL;
        }
        if($request->input('performansi') == 'on'){
            $ars->performansi = date('Y-m-d H:i:s');
        }else if($request->input('performansi') == 'performansi'){
            $ars->performansi = '1111-11-11 11:11:11';
        }else{
            $ars->performansi = NULL;
        }
        if($request->input('lpp') == 'on'){
            $ars->lpp = date('Y-m-d H:i:s');
        }else if($request->input('lpp') == 'lpp'){
            $ars->lpp = '1111-11-11 11:11:11';
        }else{
            $ars->lpp = NULL;
        }
        $ars->save();
        
        if($ars->status == 'readytobill'){
            return redirect('readytobill');
        }else{
            return redirect('unbill_ar');
        }
        
    }
                                    
    public function updatenilai(Request $request, $id)
    {
        $message = [
            'required' => ':Attribute field is required',
        ];
        $this->validate($request, [
            'chat' => 'required',
        ],$message);
        DB::beginTransaction();
        
        $ars = AR::find($id);
        $ars->nilai_invoice = str_replace(".","",$request->input('nilai_invoice'));
        $ars->invoice = $request->input('invoice');
        $ars->uraian = $request->input('uraian');
        $ars->status = $request->input('status');
        $ars->tgl_invoice = date('Y-m-d H:i:s');
        $ars->tglbill = date('Y-m-d');
        // // declare date if checklist
        if($request->input('spk') == 'on'){
            $ars->spk = date('Y-m-d H:i:s');
        }else if($request->input('spk') == 'spk'){
            $ars->spk = '1111-11-11 11:11:11';
        }else{
            $ars->spk = NULL;
        }
        if($request->input('kl') == 'on'){
            $ars->kl = date('Y-m-d H:i:s');
        }else if($request->input('kl') == 'kl'){
            $ars->kl = '1111-11-11 11:11:11';
        }else{
            $ars->kl = NULL;
        }
        if($request->input('kb') == 'on'){
            $ars->kb = date('Y-m-d H:i:s');
        }else if($request->input('kb') == 'kb'){
            $ars->kb = '1111-11-11 11:11:11';
        }else{
            $ars->kb = NULL;
        }
        if($request->input('bast') == 'on'){
            $ars->bast = date('Y-m-d H:i:s');
        }else if($request->input('bast') == 'bast'){
            $ars->bast = '1111-11-11 11:11:11';
        }else{
            $ars->bast = NULL;
        }
        if($request->input('baut') == 'on'){
            $ars->baut = date('Y-m-d H:i:s');
        }else if($request->input('baut') == 'baut'){
            $ars->baut = '1111-11-11 11:11:11';
        }else{
            $ars->baut = NULL;
        }
        if($request->input('bapp') == 'on'){
            $ars->bapp = date('Y-m-d H:i:s');
        }else if($request->input('bapp') == 'bapp'){
            $ars->bapp = '1111-11-11 11:11:11';
        }else{
            $ars->bapp = NULL;
        }
        if($request->input('baso') == 'on'){
            $ars->baso = date('Y-m-d H:i:s');
        }else if($request->input('baso') == 'baso'){
            $ars->baso = '1111-11-11 11:11:11';
        }else{
            $ars->baso = NULL;
        }
        if($request->input('basocus') == 'on'){
            $ars->basocus = date('Y-m-d H:i:s');
        }else if($request->input('basocus') == 'basocus'){
            $ars->basocus = '1111-11-11 11:11:11';
        }else{
            $ars->basocus = NULL;
        }
        if($request->input('npk') == 'on'){
            $ars->npk = date('Y-m-d H:i:s');
        }else if($request->input('npk') == 'npk'){
            $ars->npk = '1111-11-11 11:11:11';
        }else{
            $ars->npk = NULL;
        }
        if($request->input('baperub') == 'on'){
            $ars->baperub = date('Y-m-d H:i:s');
        }else if($request->input('baperub') == 'baperub'){
            $ars->baperub = '1111-11-11 11:11:11';
        }else{
            $ars->baperub = NULL;
        }
        if($request->input('barekon') == 'on'){
            $ars->barekon = date('Y-m-d H:i:s');
        }else if($request->input('barekon') == 'barekon'){
            $ars->barekon = '1111-11-11 11:11:11';
        }else{
            $ars->barekon = NULL;
        }
        if($request->input('performansi') == 'on'){
            $ars->performansi = date('Y-m-d H:i:s');
        }else if($request->input('performansi') == 'performansi'){
            $ars->performansi = '1111-11-11 11:11:11';
        }else{
            $ars->performansi = NULL;
        }
        if($request->input('lpp') == 'on'){
            $ars->lpp = date('Y-m-d H:i:s');
        }else if($request->input('lpp') == 'lpp'){
            $ars->lpp = '1111-11-11 11:11:11';
        }else{
            $ars->lpp = NULL;
        }
        // dd($ars);
        $ars->save();
        DB::commit();
        
        // dd($ars);
        if($ars->status == 'unbill'){
            $chat = new ChatAR;
            $chat->idAR = $ars->id;
            $chat->status = "Return";
            $chat->chat = $request->input('chat');
            $chat->username = Auth::user()->username;
            $chat->save();
            
            return redirect('unbill_ar');
        }else{
            $chat = new ChatAR;
            $chat->idAR = $ars->id;
            $chat->status = "Return";
            $chat->chat = $request->input('chat');
            $chat->username = Auth::user()->username;
            $chat->save();
            
            return redirect('bill');
        }
    }
                                    
    public function destroy(Request $id)
    {
        //
        $ar = AR::findOrFail($id);
        $ar->delete();
        $path = "public/files/AR/".$ar->user->unitnya['nama']."/".$id;
        Storage::deleteDirectory($path);
        return back()->with('success','Data telah dihapus');
    }
                                    
    public function upload(Request $request, $id)
    {
        // $unit = DB::table('users')
        // ->join('units','units.id','users.id_unit')
        // ->select('units.nama')
        // ->where('units.id',Auth::user()->id_unit)
        // ->first();
        // // var_dump($unit);
        // $path= "public/files/AR/".$unit->nama."/".$id;
        // if($request->file('file'))
        // {
        //     foreach($request->file('file') as $file)
        //     {
        //         $name=$file->getClientOriginalName();
        //         $file->storeAs($path, $name);
        //         $namanya[] = $name;
        //         $data[] = $path.'/'.$name;
        //     }
        // }
       
        // $file->file=json_encode($data);
        // $file->title = json_encode($namanya);

        // new update = Tidak ada bukti pembayaran hanya ada tanggal pembayaran
        $file = AR::find($id);
        $file->status = 'paid';
        $file->upload = $request->input('tglupload');
        $file->tglpaid = date('Y-m-d');
        $file->save();
        
        return redirect('paid')->withSuccess('Tanggal Bayar telah diupload.');
    }
                                    
    // list nilai project
    public function nilaiproject(Request $request){
        if (request()->ajax()) {
            if (!empty($request->bln)) {
                $data = AR::with('dataio', 'pembuat', 'customer', 'pembuat.unitnya')
                ->whereMonth('created_at', $request->bln)
                ->whereYear('created_at', $request->thn)
                ->get();
            } else {
                $data = AR::with('dataio', 'pembuat', 'customer', 'pembuat.unitnya')
                ->whereMonth('created_at', date('m'))
                ->whereYear('created_at', date('Y'))
                ->get();
            }
            return Datatables::of($data) 
            ->addColumn('action', function ($data) {
                return 
                ' <a href="#modal" data-nilai="'.$data->nilai_project.'" data-id="'.$data->io_id.'" data-toggle="modal" title="Update Nilai Project" class="upload fa fa-fw fa-money"></a> '.
                ' <a href="'.url('preview_ar/'.$data->id).'" title="Preview AR"><i class="fa fa-sticky-note-o"></i></a> ';
            })->editColumn('created_at', function ($data) 
            {
                return date('d.F.Y', strtotime($data->created_at) );
            })->editColumn('tgl_invoice', function ($data) 
            {
                if($data->tgl_invoice != NULL){
                    return date('d.F.Y', strtotime($data->tgl_invoice) );
                    
                }else{
                    return "";
                }
            })
            ->addIndexColumn()
            ->make(true);
        }
        return view('modules.ar.nilai');
    }
                                    
    public function updateproject(Request $request, $id)
    {
        
        $nilai =str_replace(".","",$request->input('nilai_project'));
        $ars = DB::table('ars')
        ->where('io_id',$id)
        ->update(
            [
                'nilai_project' =>$nilai
                ]);
                return redirect('nilai');
    }
                                            
                                                
}
                                            