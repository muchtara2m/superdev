<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use DB;
use \stdClass;
use Illuminate\Support\Facades\DB;


class ArDuaController extends Controller
{
   public function index(){
      return view('modules.testing.list_data_new_ar');
   }
   
   public function updateStatus(Request $request){
      
      $dokumen = $request->input('dokumen_status');
      $nilai = $request->input('nilai_project');
      $id = $request->input('id_dokumen');
      
      switch ($dokumen) {
         case ($dokumen == 'bakn'):
            $pic = 'UBIS';
         break;    
         case ($dokumen == 'kl'):
            $pic = 'PROCUREMENT';
         break;
         case ($dokumen == 'amandemen_kl'):
            $pic = 'PROCUREMENT';
         break;
         case ($dokumen == 'spk_mitra'):
            $pic = 'PROCUREMENT';
         break;
         case ($dokumen == "paid"):
            $pic = 'Treasury';
         break;
         case ($dokumen == "billed"):
            $pic = 'Treasury';
         break;
         case ($dokumen == "invoice"):
            $pic = 'Treasury';
         break;
         case ($dokumen == "lpl"):
            $pic = 'Operation';
         break;
         case ($dokumen == "surat_segmen"):
            $pic = 'UBIS';
         break;
         case ($dokumen == "bapw"):
            $pic = 'SDV';
         break;
         case ($dokumen == "bapp"):
            $pic = 'SDV';
         break;
         case ($dokumen == 'ba_rekon'):
            $pic = 'UBIS';
         break;
         case ($dokumen == 'sid_ao'):
            $pic = 'SDV';
         break;
         case ($dokumen == 'bast_mitra'):
            $pic = 'SDV';
         break;
         case ($dokumen == 'bast_cust'):
            $pic = 'SDB';
         break;
         case ($dokumen == 'baut_mitra'):
            $pic = 'SDV';
         break;
         case ($dokumen == 'baut_cust'):
            $pic = 'SDV';
         break;
         case ($dokumen == 'baso_cust'):
            $pic = 'SDV';
         break;
         case ($dokumen == 'bakn_mitra'):
            $pic = 'PROCUREMENT';
         break;                    
      }
      
      DB::table('ars2')->where('id',$id)->update([
         $dokumen => date('Y-m-d H:i:s'),
         'amount' => $nilai,
         'pic' => $pic
         ]);
         echo 'Update successfully.';
         exit; 
      }
      
      public function dataNewAr(Request $request)
      {
         if (request()->ajax()) {
            if (!empty($request->thn) || !empty($request->bln)) {
               $m = $request->bln;
               $y = $request->thn;
            } else {
               $m = date('m');
               $y = date('Y');
            }
         }
         if(empty($y)){
            $y = date('Y');
         }
         $object = DB::table('ars2')
         ->select([
            '*',
            DB::raw('(CASE WHEN (bakn IS NOT NULL OR bakn != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BAKN'),
            DB::raw('(CASE WHEN (kl IS NOT NULL OR kl != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as KL'),
            DB::raw('(CASE WHEN (amandemen_kl IS NOT NULL OR amandemen_kl != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as AMANDEMEN_KL'),
            DB::raw('(CASE WHEN (spk_mitra IS NOT NULL OR spk_mitra != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as SPK_MITRA'),
            DB::raw('(CASE WHEN (bakn_mitra IS NOT NULL OR bakn_mitra != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BAKN_MITRA'),
            DB::raw('(CASE WHEN (baso_cust IS NOT NULL OR baso_cust != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BASO_CUSTOMER'),
            DB::raw('(CASE WHEN (baut_cust IS NOT NULL OR baut_cust != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BAUT_CUST'),
            DB::raw('(CASE WHEN (baut_mitra IS NOT NULL OR baut_mitra != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BAUT_MITRA'),
            DB::raw('(CASE WHEN (bast_cust IS NOT NULL OR bast_cust != "1111-11-11 11:11:11") THEN amount ELSE 0 END) as BAST_CUST'),
            DB::raw('(CASE WHEN (bast_mitra IS NOT NULL OR bast_mitra != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BAST_MITRA'),
            DB::raw('(CASE WHEN (sid_ao IS NOT NULL OR sid_ao != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as SID_AO'),
            DB::raw('(CASE WHEN (ba_rekon IS NOT NULL OR ba_rekon != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BA_REKON'),
            DB::raw('(CASE WHEN (bapp IS NOT NULL OR bapp != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BAPP'),
            DB::raw('(CASE WHEN (bapw IS NOT NULL OR bapw != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BAPW'),
            DB::raw('(CASE WHEN (surat_segmen IS NOT NULL OR surat_segmen != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as SURAT_SEGMEN'),
            DB::raw('(CASE WHEN (lpl IS NOT NULL OR lpl != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as LPL'),
            DB::raw('(CASE WHEN (invoice IS NOT NULL OR invoice != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as INVOICE'),
            DB::raw('(CASE WHEN (billed IS NOT NULL OR billed != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BILLED'),
            DB::raw('(CASE WHEN (paid IS NOT NULL OR paid != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as PAID'),
            ])
            // ->take(200)
            ->whereYear('posting_date',$y)
            ->whereMonth('posting_date',$m)
            ->get();
            // dd($object);
            return DataTables()->of($object)
            ->addIndexColumn()
            ->addColumn('dokumen', function($object){
               
               if($object->bakn == null && $object->kl == null && $object->amandemen_kl == null && $object->spk_mitra == null && $object->bakn_mitra == null && $object->baso_cust == null && $object->baut_cust == null && $object->baut_mitra == null && $object->sid_ao == null && $object->ba_rekon == null && $object->bapp == null && $object->bapw == null && $object->surat_segmen == null && $object->lpl == null && $object->invoice == null && $object->billed == null && $object->paid == null ){
                  
                  return array(
                     "dok" => "BELUM ADA DOKUMEN",
                     "id" => $object->id,
                     "nil" => $object->amount,
                  );
               }
               switch ($object) {
                  case ($object->PAID != 0):
                     return array("dok" => "paid",
                     "id" => $object->id,
                     "nil" => $object->amount,
                  );
               break;
               case ($object->BILLED != 0):
                  return array("dok" => "billed",
                  "nil" => $object->amount,
                  "id" => $object->id,);
               break;
               case ($object->INVOICE != 0):
                  return array("dok" => "invoice",
                  "nil" => $object->amount,
                  "id" => $object->id,);
               break;
               case ($object->LPL != 0):
                  return array("dok" => "lpl",
                  "nil" => $object->amount,
                  "id" => $object->id);
               break;
               case ($object->SURAT_SEGMEN != 0):
                  return array("dok" => "surat_segmen",
                  "nil" => $object->amount,
                  "id" => $object->id);
               break;
               case ($object->BAPW != 0):
                  return array("dok" => "bapw",
                  "nil" => $object->amount,
                  "id" => $object->id);
               break;
               case ($object->BAPP != 0):
                  return array("dok" => "bapp",
                  "nil" => $object->amount,
                  "id" => $object->id);
               break;
               case ($object->BA_REKON != 0):
                  return array("dok" => "ba_rekon",
                  "nil" => $object->amount,
                  "id" => $object->id);
               break;
               case ($object->SID_AO != 0):
                  return array("dok" => "sid_ao",
                  "nil" => $object->amount,
                  "id" => $object->id);
               break;
               case ($object->BAST_MITRA != 0):
                  return array("dok" => "bast_mitra",
                  "nil" => $object->amount,
                  "id" => $object->id);
               break;
               case ($object->BAST_CUST != 0):
                  return array("dok" => "bast_cust",
                  "nil" => $object->amount,
                  "id" => $object->id);
               break;
               case ($object->BAUT_MITRA != 0):
                  return array("dok" => "baut_mitra",
                  "nil" => $object->amount,
                  "id" => $object->id);
               break;
               case ($object->BAUT_CUST != 0):
                  return array("dok" => "baut_cust",
                  "nil" => $object->amount,
                  "id" => $object->id);
               break;
               case ($object->BASO_CUSTOMER != 0):
                  return array("dok" => "baso_cust",
                  "nil" => $object->amount,
                  "id" => $object->id);
               break;
               case ($object->BAKN_MITRA != 0):
                  return array("dok" => "bakn_mitra",
                  "nil" => $object->amount,
                  "id" => $object->id);
               break;
               case ($object->SPK_MITRA != 0):
                  return array("dok" => "spk_mitra",
                  "nil" => $object->amount,
                  "id" => $object->id);
               break;
               case ($object->AMANDEMEN_KL != 0):
                  return array("dok" => "amandemen_kl",
                  "nil" => $object->amount,
                  "id" => $object->id);
               break;
               case ($object->KL != 0):
                  return array("dok" => "kl",
                  "nil" => $object->amount,
                  "id" => $object->id);
               break;
               case ($object->BAKN != 0):
                  return array("dok" => "bakn",
                  "nil" => $object->amount,
                  "id" => $object->id);
               break;                        
            }
         })
         ->make(true);
      }
      
      public function dataNotifikasi()
      {
         $data = DB::table('ars2')->select([
            'ubis as UBIS',
            'header1 as NomorIO',
            'pic as PIC',
            DB::raw('sum(CASE WHEN (bakn IS NOT NULL OR bakn != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BAKN'),
            DB::raw('sum(CASE WHEN (kl IS NOT NULL OR kl != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as KL'),
            DB::raw('sum(CASE WHEN (amandemen_kl IS NOT NULL OR amandemen_kl != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as AMANDEMEN_KL'),
            DB::raw('sum(CASE WHEN (spk_mitra IS NOT NULL OR spk_mitra != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as SPK_MITRA'),
            DB::raw('sum(CASE WHEN (bakn_mitra IS NOT NULL OR bakn_mitra != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BAKN_MITRA'),
            DB::raw('sum(CASE WHEN (baso_cust IS NOT NULL OR baso_cust != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BASO_CUSTOMER'),
            DB::raw('sum(CASE WHEN (baut_cust IS NOT NULL OR baut_cust != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BAUT_CUST'),
            DB::raw('sum(CASE WHEN (baut_mitra IS NOT NULL OR baut_mitra != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BAUT_MITRA'),
            DB::raw('sum(CASE WHEN (bast_cust IS NOT NULL OR bast_cust != "1111-11-11 11:11:11") THEN amount ELSE 0 END) as BAST_CUST'),
            DB::raw('sum(CASE WHEN (bast_mitra IS NOT NULL OR bast_mitra != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BAST_MITRA'),
            DB::raw('sum(CASE WHEN (sid_ao IS NOT NULL OR sid_ao != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as SID_AO'),
            DB::raw('sum(CASE WHEN (ba_rekon IS NOT NULL OR ba_rekon != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BA_REKON'),
            DB::raw('sum(CASE WHEN (bapp IS NOT NULL OR bapp != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BAPP'),
            DB::raw('sum(CASE WHEN (bapw IS NOT NULL OR bapw != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BAPW'),
            DB::raw('sum(CASE WHEN (surat_segmen IS NOT NULL OR surat_segmen != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as SURAT_SEGMEN'),
            DB::raw('sum(CASE WHEN (lpl IS NOT NULL OR lpl != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as LPL'),
            DB::raw('sum(CASE WHEN (invoice IS NOT NULL OR invoice != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as INVOICE'),
            DB::raw('sum(CASE WHEN (billed IS NOT NULL OR billed != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as BILLED'),
            DB::raw('sum(CASE WHEN (paid IS NOT NULL OR paid != "1111-11-11 11:11:11") THEN amount  ELSE 0 END) as PAID'),
         ])
            ->groupBy([
               'ubis',
               'pic',
               'header1'
               ])
               // ->take(20)
               ->get();
               $grouped = array();
               $hasil = array();
               // dd($grouped);
               // Loop JSON objects
               foreach ($data as $object) {
                  if (!array_key_exists($object->UBIS, $grouped)) { // a new UBIS...
                     $newObject = new stdClass();
                     
                     // Copy the ID/UBIS, and create an ITEMS placeholder
                     $newObject->UBIS = $object->UBIS;
                     $newObject->FILE = array();
                     
                     // Save this new object
                     $grouped[$object->UBIS] = $newObject;
                  }
                  
                  $taskObject = new stdClass();
                  
                  // Copy the TASK/TASK_NAME
                  // $taskObject->PIC = $object->PIC;
                  
                  switch ($object) {
                     case ($object->PAID != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "PAID";
                        $taskObject->Nilai = $object->PAID;
                     break;
                     case ($object->BILLED != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "BILLED";
                        $taskObject->Nilai = $object->BILLED;
                     break;
                     case ($object->INVOICE != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "INVOICE";
                        $taskObject->Nilai = $object->INVOICE;
                     break;
                     case ($object->LPL != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "LPL";
                        $taskObject->Nilai = $object->LPL;
                     break;
                     case ($object->SURAT_SEGMEN != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "SURAT_SEGMEN";
                        $taskObject->Nilai = $object->SURAT_SEGMEN;
                     break;
                     case ($object->BAPW != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "BAPW";
                        $taskObject->Nilai = $object->BAPW;
                     break;
                     case ($object->BA_REKON != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "BA_REKON";
                        $taskObject->Nilai = $object->BA_REKON;
                     break;
                     case ($object->SID_AO != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "SID_AO";
                        $taskObject->Nilai = $object->SID_AO;
                     break;
                     case ($object->BAST_MITRA != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "BAST_MITRA";
                        $taskObject->Nilai = $object->BAST_MITRA;
                     break;
                     case ($object->BAST_CUST != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "BAST_CUST";
                        $taskObject->Nilai = $object->BAST_CUST;
                     break;
                     case ($object->BAUT_MITRA != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "BAUT_MITRA";
                        $taskObject->Nilai = $object->BAUT_MITRA;
                     break;
                     case ($object->BAUT_CUST != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "BAUT_CUST";
                        $taskObject->Nilai = $object->BAUT_CUST;
                     break;
                     case ($object->BASO_CUSTOMER != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "BASO_CUSTOMER";
                        $taskObject->Nilai = $object->BASO_CUSTOMER;
                     break;
                     case ($object->BAKN_MITRA != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "BAKN_MITRA";
                        $taskObject->Nilai = $object->BAKN_MITRA;
                     break;
                     case ($object->SPK_MITRA != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "SPK_MITRA";
                        $taskObject->Nilai = $object->SPK_MITRA;
                     break;
                     case ($object->AMANDEMEN_KL != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "AMANDEMEN_KL";
                        $taskObject->Nilai = $object->AMANDEMEN_KL;
                     break;
                     case ($object->KL != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "KL";
                        $taskObject->Nilai = $object->KL;
                     break;
                     case ($object->BAKN != 0):
                        $taskObject->PIC = $object->PIC;
                        $taskObject->DOKUMEN = "BAKN";
                        $taskObject->Nilai = $object->BAKN;
                     break;
                  }
                  // Append this new task to the ITEMS array
                  $grouped[$object->UBIS]->FILE[] = $taskObject;
               }
               $json = json_decode(json_encode($grouped), true);
               $sementara = "";
               $sementaraPic = "";
               $sementaraDok = "";
               $totals = [];
               $subtotals = [];
               $subsubtotals = [];
               $results = [];
               foreach ($json as $key => $val) {
                  foreach ($val['FILE'] as $key2 => $val2) {
                     if ($val2 != null) {
                        $ubis = $val['UBIS'];
                        list($pic, $dok, $nil) = array_values($val2);
                        $int = (int) $nil;
                        $subsubtotal = isset($subsubtotals[$ubis][$pic][$dok]['nilai']) ? $subsubtotals[$ubis][$pic][$dok]['nilai'] : 0;
                        $ttl = (int) $subsubtotal + (int) $nil;
                        $subsubtotals[$ubis][$pic][$dok] = array('doknya' => $dok, 'nilai' => $ttl);
                        
                        
                        $subtotal = isset($subtotals[$ubis][$pic]) ? $subtotals[$ubis][$pic] : 0;
                        $subtotals[$ubis][$pic] = $subtotal + $int;
                        
                        $total = isset($totals[$ubis]) ? $totals[$ubis] : 0;
                        $dokNya[$ubis] = (int) $total + $int;
                     }   // end condition null
                  }  // end foreach val2
               }  // end foreach
               
               // insert data table to variable and send to wa
               $dataTable = "";
               $ttlSemua = array();
               $noNo = 1;
               foreach ($subsubtotals as $item => $result) {
                  $sum = 0;
                  $no = 1;
                  $dataTable .= "\xA" . '*' . strtoupper($item) . '*' . "\xA";
                  foreach ($result as $item2 => $result2) {
                     $dataTable .= "\xA";
                     $dataTable .= '  *' . $no++ . '. PIC ' . strtoupper($item2) . '*' . "\xA";
                     $i = 1;
                     foreach ($result2 as $item3 => $result3) {
                        $dataTable .= '       - ' . $result3['doknya'] . ' = Rp ' . number_format($result3['nilai']) . "\xA";
                        $sum += $result3['nilai'];
                        $ttlSemua[] = $sum;
                     }
                  }
                  $dataTable .= "\xA" . '*Total = Rp ' . number_format($sum) . ' ,-*' . "\xA";
                  $dataTable .= '-----------------------------------------------------------------------' . "\xA";
               }
               $dataTable .= "\xA" . '*Grand Total = Rp ' . number_format(array_sum($ttlSemua)) . ' ,-*' . "\xA";
               dd($dataTable);
               // send notifikasi WA
               $this->kirimDataToWa($dataTable);
            }
            
            public function kirimDataToWa($ardua)
            {
               // $nomer = array('6281280295238', '6281221012945');
               // $nomer = '81280295238';
               // $nomer = '81319776000';
               // $nomer = '81321751973';
               $nomer = array('6281280295238', '6281281803746','6281319776000');
               foreach($nomer as $key){
                  $data = [
                     'phone' => $key, // Receivers phone
                     'body' => $ardua,
                  ];
                  $json = json_encode($data); // Encode data to JSON
                  
                  // URL for request POST /message
                  $url = 'https://eu43.chat-api.com/instance59109/sendMessage?token=bn0w07mp9572ei4t';
                  
                  // Make a POST request
                  $options = stream_context_create([
                     'http' => [
                        'method'  => 'POST',
                        'header'  => 'Content-type: application/json',
                        'content' => $json
                        ]
                        ]);
                        $result = file_get_contents($url, false, $options);
                        $var = json_decode($result, true);
                     }
                     // dd($result);
                     
                  }
               }