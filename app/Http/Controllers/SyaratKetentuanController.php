<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\CaraBayar;
use App\Models\JenisPasal;
use App\Models\Ketentuans;
use App\Models\Pasal;

class SyaratKetentuanController extends Controller
{

    // declare variable for models
    protected $caraBayars;
    protected $pasals;
    protected $jenisPasals;
    protected $ketentuans;

    public function __construct()
    {
        $this->caraBayars   = new CaraBayar();
        $this->pasals       = new Pasal();
        $this->jenisPasals  = new JenisPasal();
        $this->ketentuans   = new Ketentuans();

    }
    // TATA CARA BAYAR 

    // PASAL

    // JENIS PASAL

    // KETENTUAN / LAIN LAIN

    public function getDataKetentuan()
    {
        return $this->ketentuans->allData();
    }

    public function editKetentuan(Ketentuans $ketentuans)
    {
        return view();
    }

    public function createKetentuan()
    {
        return view();
    }

    public function storeKetentuan(Request $request)
    {
        return $this->ketentuans->store($request->all());
    }

    public function updateKetentuan(Request $request, Ketentuans $ketentuans)
    {
        return $this->ketentuans->update($request->all(), $request->id);
    }

    public function destroyKetentuan($id)
    {
        return $this->ketentuans->deleteData($id);
    }
}
