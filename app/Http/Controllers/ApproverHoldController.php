<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Approver_hold;
use Illuminate\Support\Facades\DB;

class ApproverHoldController extends Controller
{
    public function index()
    {
        $res_uid = DB::table('approver_holds')
                        ->select('approver_holds.id', 'approver_holds.min', 'approver_holds.max','roles.display', 'users.name')
                        ->join('model_has_roles', 'approver_holds.role_id', '=', 'model_has_roles.role_id')
                        ->join('roles', 'approver_holds.role_id', '=', 'roles.id')
                        ->join('users', 'model_has_roles.model_id', '=', 'users.id')
                        ->get();

        // $approvers = Approver_hold::all();
        $roles = DB::table('roles')->select('id', 'display')->get();
        return view('modules.approver_hold.index', compact('roles', 'res_uid'));
    }

    public function store(Request $request)
    {
        $aprhold = new Approver_hold();
        
        $aprhold->role_id = $request->input('role_id');
        $aprhold->min = $request->input('min');
        $aprhold->max = $request->input('max');
        $aprhold->save();

        return redirect()->route('approver_hold.index')->with('Data berhasil ditambahkan');
    }
}