<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AR;
// use DB;
use Illuminate\Support\Facades\DB;
use \stdClass;
use App\Iodesc;

class TestingController extends Controller
{

    


    public function dataPbs()
    {
        $pbs = DB::connection('mysql3')
            ->table('data_header')
            ->join('mtr_io', 'mtr_io.io_id', 'data_header.io_id')
            ->get();

        $pipeline = DB::connection('mysql2')
            ->table('t_pbs')
            ->get();
        $gabung = [];
        foreach ($pbs as $key1 => $value1) {
            foreach ($pipeline as $key2 => $value2) {
                if ($value1->no_io == $value2->no_io) {
                    $gabung[$value1->no_io] = array_merge((array) $value1, (array) $value2);
                }
            }
        }
        return response()->json([
            'data' => $gabung
        ]);
    }

    public function dataPipeline()
    {
        return view('modules.testing.join');
    }

   

    public function importIo(){
       
        $conn = @ftp_connect("10.15.179.74");
        if($conn)
        {
            echo 'server name is valid';
        }
        else
        {
            echo 'server name is invalid';
        }
    
        $ftp_server = '10.15.179.74';
        $ftp_user_name ='theo';
        $ftp_user_pass = 'p1n5_k4m1'; 

        $path = '../../ktk/NO_IO.txt';

        $connect=mysqli_connect($svr, $usr, $pass, $db);


        // path to remote file
        $remote_file = '../noc_pins/report.xlsx';
        $local_file = '../../csv/report.xlsx';

        // open some file to write to
        $handle = fopen($local_file, 'w');

        // set up basic connection
        $conn_id = ftp_connect($ftp_server);

        // login with username and password
        $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

        ftp_pasv($conn_id, true);
        // try to download $remote_file and save it to $handle
        if (ftp_fget($conn_id, $handle, $remote_file, FTP_ASCII)) {
            // fungsi setelah berhasil connect and get ftp
            // $inputFileName = 'csv/test.csv';
            $inputFileName = '../../csv/report.xlsx';

        //  Read your Excel workbook
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

        //  Get worksheet dimensions
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

        //  Loop through each row of the worksheet in turn



            echo "successfully written to $local_file\n and insert to database";

        } else {
            echo "There was a problem while downloading $remote_file to $local_file\n";
        }

        // close the connection and the file handler
        ftp_close($conn_id);
        fclose($handle);
    }

    public function updateIo(){
        // get data io from pipeline 
        $io = DB::connection('mysql2')->table('t_io')
        ->get();
        // truncate data in superslim mtr_io
        Iodesc::truncate();
        // user looping to insert data
        foreach($io as $item){
            $data  = array('no_io'=> $item->no_io, 'deskripsi'=>$item->deskripsi_project,'tahun' => "NULL");
            Iodesc::insert($data);
        }
        // view all data after success
        $dbIo = Iodesc::all();
         return DataTables()->of($dbIo)
                ->addIndexColumn()
                ->make(true);

    }
}
