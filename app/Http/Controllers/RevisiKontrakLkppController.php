<?php

namespace App\Http\Controllers;

use App\Http\Requests\FormRevisiKontrakLkpp;
use App\Models\Chat;
use App\Models\Kontrak;
use App\Models\RevisiKontrakLkpp;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\KontrakLkpp;
use App\Models\SpphLKPP;
use App\User;

class RevisiKontrakLkppController extends Controller
{
    protected $model;
    protected $kontrakLkpp;
    protected $chat;

   public function __construct()
   {
       $this->middleware('auth');

       $this->model         = new RevisiKontrakLkpp();
       $this->kontrakLkpp   = new Kontrak();
       $this->chat          = new Chat();
   }

   public function store(FormRevisiKontrakLkpp $request, Kontrak $kontrak)
   {
       

       $data = $request->all();
       $data['lampiran'] = $request->file('lampiran') != null ? $request->file('lampiran') : NULL;
    
       DB::beginTransaction();
        try{

            $insert = $this->model->insertData($kontrak);

            $update = $this->kontrakLkpp->updateRevisi($data, $kontrak);

            if($request->status == 'save_kontrak'){
                 // call fuction chat
                $this->chat->insertChatRevisi($data,$kontrak);
                // update approval 
                $this->kontrakLkpp->getValueApproval($kontrak);
                DB::commit();
                return redirect('inprogress-kontrak-lkpp')->with('success',"Revisi with No. Kontrak $update->nomor_kontrak has been added sucessfully");
            }else{
                DB::commit();
                return redirect('draft-kontrak-lkpp')->with('success',"Revisi with No. Kontrak $update->nomor_kontrak has been added sucessfully");
            }
       }catch(Exception $e){
           DB::rollBack();
           return redirect()->back()->withErrors($e->getMessage());
       }
   }

   public function show(RevisiKontrakLkpp $revisiKontrakLkpp)
   {
       $kontrak     = $revisiKontrakLkpp::with('revisi_kontrak_lkpps','revisi_kontrak_lkpps.bakn_lkpp')->first();
       $spphLKPP    = SpphLKPP::where('id',$kontrak->revisi_kontrak_lkpps->bakn_lkpp->spph_id)->first();
       $baknLKPP    = $kontrak->revisi_kontrak_lkpps->bakn_lkpp;
       $user        = User::where('username',$revisiKontrakLkpp->created_by)->first();
       return view('modules.kontrak_lkpp.revisi.preview', compact('revisiKontrakLkpp','kontrak','spphLKPP','baknLKPP','user'));
   }
}
