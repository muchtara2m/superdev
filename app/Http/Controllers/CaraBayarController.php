<?php

namespace App\Http\Controllers;

use App\Models\CaraBayar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CaraBayarController extends Controller
{
  
    public function index()
    {
        $bayar = CaraBayar::all();
        return view('modules.tatacara.index', compact('bayar'));
    }

    public function create()
    {
        return view('modules.tatacara.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'jenis' => 'required',
            'isi' => 'required'
        ]);
        DB::beginTransaction();

        $crbayar = new CaraBayar();
        $crbayar->jenis = $request->input('jenis');
        $crbayar->isi = $request->input('isi');
        $crbayar->save();
        try {

        } catch (ValidationException $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors($e)
            ->withInput();
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
            ->withErrors($e)
            ->withInput();
        }
        DB::commit();

        return redirect('tatacara')->with('Data has been added');
    }

    public function edit( $id)
    {
        $bayar = CaraBayar::find($id);
        return view('modules.tatacara.edit',compact('bayar','id'));
    }

    public function update(Request $request,  $id)
    {
        //
        $bayar          = CaraBayar::find($id);
        $bayar->jenis   = $request->get('jenis');
        $bayar->isi     = $request->get('isi');
        $bayar->save();
        return redirect('tatacara')->with('success','Data telah berhasil diubah');
    }

    public function destroy(CaraBayar $id)
    {
        //
        $bayar = CaraBayar::find($id);
        $bayar->delete();

        return redirect('tatacara')->with('success','Data telah dihapus');
    }
}
