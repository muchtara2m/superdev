<?php

namespace App\Http\Controllers;

use App\Models\Ketentuans;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class KetentuansController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Ketentuans();
    }

    public function allData()
    {
        $data = $this->model->allData();
        return DataTables()->of($data)
                ->addIndexColumn()
                ->make(true);
    }

    public function detailData(Ketentuans $ketentuans)
    {
        return $ketentuans;
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'dokumen'   => 'required',
            'jenis'     => 'required',
            'isi'       => 'required',
        ]);

        $data = $request->all();
            
        DB::transaction(function () use ($data){
            $this->model->inserData($data);
        });
    }

    public function update(Request $request, Ketentuans $ketentuans)
    {
        dd($ketentuans);
    }

    public function destroy($id)
    {
        $data = $this->model->where('id', $id)->first();
        $data->delete();
        return redirect()->back();
    }


}
