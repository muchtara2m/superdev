<?php

namespace App\Http\Controllers;

use App\Models\RolePermission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// use App\Role;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
use Validator;
use Auth;

class RolePermissionController extends Controller
{
    public function index()
    {
        $roles = DB::table('roles')
            ->select('id','name','display',DB::raw('count(model_id) as total'))
            ->leftJoin('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->groupBy('id')
            ->get();
        $permissions = DB::table('permissions')
            ->select('id', 'name', 'display', DB::raw('count(role_id) as total'))
            ->leftjoin('role_has_permissions', 'permissions.id', '=', 'role_has_permissions.permission_id')
            ->groupBy('id')
            ->get();

        return view('modules.rolepermission.index', compact('roles','permissions'));
    }
    
    public function createRole()
    {
        $menu_pbs = array(
            array("Create PBS","pbs_create"),
            array("PBS","pbs"),
            array("Status Transaksi","pbs_st_trans"),
            array("Inprogress","pbs_inp"),
            array("Selesai","pbs_selesai"),
            array("Revisi Transaksi","pbs_re_trans"),
            array("Kalkulator Peminjaman","pbs_kal_pinj")
        );
        $menu_spph = array(
            array("Create SPPH","spph_create"),
            array("List SPPH","spph_list"),
            array("Draft SPPH","spph_draft"),
            array("Done SPPH","spph_done")
        );
        $menu_bakn = array(
            array("Create BAKN","bakn_create"),
            array("List BAKN","bakn_list"),
            array("Draft BAKN","bakn_draft"),
            array("Done BAKN","bakn_done")
        );
        $menu_spk = array(
            array("Create SP3","create-sp3-lkpp"),
            array("Draft SP3","draft-sp3-lkpp"),
            array("Status Transaksi","status-transaksi-sp3-lkpp"),
            array("Inprogress SP3","inprogress-sp3-lkpp"),
            array("List SP3","list-sp3-lkpp"),
            array("Done SP3","done-sp3-lkpp"),
            array("Tracking Document",'tracking-sp3-lkpp')
            );

        $menu_spk_non = array(
            array("Create SP3/SPK","spk_create_non"),
            array("List SP3/SPK","spk_list_non"),
            array("Draft SP3/SPK","spk_draft_non"),
            array("Status Transaksi","spk_st_trans_non"),
            array("Inprogress SP3/SPK","spk_inp_non"),
            array("Done SP3/SPK","spk_done_non")
        );
        $menu_kontrak = array(
            array("Create Kontrak","create-kontrak-lkpp"),
            array("List Kontrak","list-kontrak-lkpp"),
            array("Draft Kontrak","draft-kontrak-lkpp"),
            array("Status Transaksi","status-transaksi-kontrak-lkpp"),
            array("Inprogress Kontrak","inprogress-kontrak-lkpp"),
            array("Upload File","upload-file-kontrak-lkpp"),
            array("Done Kontrak","done-kontrak-lkpp"),
            array("Tracking Document",'tracking-kontrak-lkpp')
        );
        $menu_mdata = array(
            array("Data Flow","mdata_dflow"),
            array("Data Karyawan","mdata_dkaryawan"),
            array("Data Mitra","mdata_dmitra"),
            array("Data Pasal","mdata_dpasal"),
            array("Data Pimpinan Rapat","mdata_dpimrap"),
            array("Data Role","mdata_drole"),
            array("Data Unit","mdata_dunit"),
            array("Data Jenis Pasal","mdata_djenpas"),
            array("Data Cara Bayar","mdata_dcarbay")
        );
        $menu_ar = array(
            array("Dashboar AR","ar_dashboard"),
            array("Create","ar_create"),
            array("Update Nilai","ar_nilai"),
            array("Unbill","ar_unbill"),
            array("Bill","ar_bill"),
            array("Ready To Bill","ar_readytobill"),
            array("Paid","ar_paid"),
            array("Paid 100","ar_paid100"),
            array("Unbill SDV","unbill-sdv"),
            array("Unbill Operation","unbill-operation"),
            array("Unbill UBIS","unbill-ubis")
        );
        $menu_kontrak_non = array(
            array("Kontrak ","kontrak_non"),
            array("List Kontrak ","kontrak_non_list"),
            array("Draft Kontrak ","kontrak_non_draft"),
            array("Status Transaksi ","kontrak_non_st_trans"),
            array("Inprogress Kontrak ","kontrak_non_inprogress"),
            array("Upload File ","kontrak_non_upload"),
            array("Done Kontrak ","kontrak_non_done"),
            array("List Dispatch Kontrak ","kontrak_non_listdisp"),
            array("Tracking Document ","kontrak_non_track"),
            array("Dashboard Performansi ","kontrak_non_performansi"),
          );
         // DASHBOARD MENU PIPELINE
        $menu_db = array(
            array("Target Revenue Sales & GP","target_rev_sal_gp"),
            array("Pencapaian BAST","pencapaian_bast"),
            array("Revenue SAP","revenue_sap"),
            array("Pencapaian AM","pencapaian_am"),
            array("IFRS Monitoring","ifrs_monitoring")
        );
            // PIPELINE
        $pipeline = array(
            array("CRM","crm"),
            array("Info PBS/Justifikasi","info_pbs_justi"),
            array("Legal Vendor","legal_vendor"),
            array("Legal Customer","legal_customer"),
            array("Delivery Customer","delivery_customer"),
            array("Delivery Vendor","delivery_vendor"),
            array("Report Management","report_management")
        );
        //SPPH LIST MENU LKPP
        $menu_spph_lkpp = array(
            array("Create SPPH","create-spph-lkpp"),
            array("List SPPH","list-spph-lkpp"),
            array("Draft SPPH","draft-spph-lkpp"),
            array("Done SPPH","done-spph-lkpp"),
            array("All SPPH","all-spph-lkpp")
            );
        
        //BAKN LIST MENU LKPP
        $menu_bakn_lkpp = array(
        array("Create BAKN","create-bakn-lkpp"),
        array("Draft BAKN","draft-bakn-lkpp"),
        array("List BAKN","list-bakn-lkpp"),
        array("Done BAKN","done-bakn-lkpp"),
        array("Status Transaksi","status-transaksi-bakn-lkpp"),
        array("Inprogress","inprogress-bakn-lkpp"),
        array("Tracking Document","tracking-document-bakn-lkpp"),
        );
        
        //BAK LIST MENU LKPP
        $menu_bak_lkpp = array(
        array("Create BAK","create-bak-lkpp"),
        array("Draft BAK","draft-bak-lkpp"),
        array("List BAK","list-bak-lkpp"),
        array("Done BAK","done-bak-lkpp"),
        array("Status Transaksi","status-transaksi-bak-lkpp"),
        array("Inprogress","inprogress-bak-lkpp"),
        array("Tracking Document","tracking-bak-lkpp"),
        );
        $menu_ar_dua = array(
        array("List Data","/list-ar-dua"),
        array("Notifikasi ","/ar-dua"),
        array("Dashboard","/ar-dua-dashboard"),
        );

        $menu_dashboard = array(
        array('Dashboard SPPH','/#'),
        array('Dashboard BAKN','/#'),
        array('Dashboard SPK','/#'),
        array('Dashboard SP3','/#'),
        array('Dashboard KONTRAK', '/#'),
        array("Dashboard Performansi ","kontrak_non_performansi"),
        array('Dashboard AR','ar_dashboard'),
        array('Dashboard AR 2','/ar2_dashboard'),
        );

        $rolePerm = Role::with('permission');
        return view('modules.rolepermission.create_role', compact(
            'menu_bak_lkpp',
            'menu_bakn_lkpp',
            'menu_spph_lkpp',
            'menu_db',
            'pipeline',
            'menu_pbs', 
            'menu_spph', 
            'menu_bakn', 
            'menu_spk',
            'menu_spk_non', 
            'menu_kontrak', 
            'menu_mdata',
            'menu_ar',
            'menu_kontrak_non',
            'menu_ar_dua',
            'menu_dashboard'   
        ));
    }

    public function createPermission(){
        return view('modules.rolepermission.create_permission');
    }

    public function storeRole(Request $request)
    {
        Validator::extend('without_space', function($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        });
        $this->validate($request, [
            'roleName' => 'required|without_space',
            'roleDisplay' => 'required'
        ],['without_space' => "Role name can't contain any space."]);

        $name = $request->input('roleName');
        $display = $request->input('roleDisplay');
        $arr_menu = $request->input('menuAccess');
        $menu_access = implode(",", $arr_menu);

        $assign = Role::create([
            'name' => $name,
            'display' => $display,
            'menu_access' => $menu_access,
            'guard_name' => 'web'
        ]);
        $assign->givePermissionTo([$arr_menu]);

        return redirect()->route('rpmanage.index')->with('success', 'Data was added successfully');
    }

    public function storePermission(Request $request)
    {
        Validator::extend('without_space', function($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        });
        $this->validate($request, [
            'permissionName' => 'required|without_space',
            'permissionDisplay' => 'required'
        ],['without_space' => "Permission name can't contain any space."]);

        $name = $request->input('permissionName');
        $display = $request->input('permissionDisplay');
        Permission::create([
            'name' => $name,
            'display' => $display
        ]);

        return redirect()->route('rpmanage.index')->with('success', 'Data was added successfully');
    }

 
    public function editRole($id)
    {
        $role = Role::find($id);
        $menu_pbs = array(
            array("Create PBS","pbs_create"),
            array("PBS","pbs"),
            array("Status Transaksi","pbs_st_trans"),
            array("Inprogress","pbs_inp"),
            array("Selesai","pbs_selesai"),
            array("Revisi Transaksi","pbs_re_trans"),
            array("Kalkulator Peminjaman","pbs_kal_pinj"));
        $menu_spph = array(
            array("Create SPPH","spph_create"),
            array("List SPPH","spph_list"),
            array("Draft SPPH","spph_draft"),
            array("Done SPPH","spph_done"));
        $menu_bakn = array(
            array("Create BAKN","bakn_create"),
            array("List BAKN","bakn_list"),
            array("Draft BAKN","bakn_draft"),
            array("Done BAKN","bakn_done"));
        $menu_spk = array(
            array("Create SP3","create-sp3-lkpp"),
            array("Draft SP3","draft-sp3-lkpp"),
            array("Status Transaksi","status-transaksi-sp3-lkpp"),
            array("Inprogress SP3","inprogress-sp3-lkpp"),
            array("List SP3","list-sp3-lkpp"),
            array("Done SP3","done-sp3-lkpp"),
            array("Tracking Document",'tracking-sp3-lkpp')
            );
        $menu_spk_non = array(
            array("Create SP3/SPK","spk_create_non"),
            array("List SP3/SPK","spk_list_non"),
            array("Draft SP3/SPK","spk_draft_non"),
            array("Status Transaksi","spk_st_trans_non"),
            array("Inprogress SP3/SPK","spk_inp_non"),
            array("Done SP3/SPK","spk_done_non"));
        $menu_kontrak = array(
            array("Create Kontrak","create-kontrak-lkpp"),
            array("List Kontrak","list-kontrak-lkpp"),
            array("Draft Kontrak","draft-kontrak-lkpp"),
            array("Status Transaksi","status-transaksi-kontrak-lkpp"),
            array("Inprogress Kontrak","inprogress-kontrak-lkpp"),
            array("Upload File","upload-file-kontrak-lkpp"),
            array("Done Kontrak","done-kontrak-lkpp"),
            array("Tracking Document",'tracking-kontrak-lkpp')
        );
        $menu_mdata = array(
            array("Data Flow","mdata_dflow"),
            array("Data Karyawan","mdata_dkaryawan"),
            array("Data Mitra","mdata_dmitra"),
            array("Data Pasal","mdata_dpasal"),
            array("Data Pimpinan Rapat","mdata_dpimrap"),
            array("Data Role","mdata_drole"),
            array("Data Unit","mdata_dunit"),
            array("Data Jenis Pasal","mdata_djenpas"),
            array("Data Cara Bayar","mdata_dcarbay"));
        $menu_ar = array(
            array("Dashboar AR","ar_dashboard"),
            array("Create","ar_create"),
            array("Update Nilai","ar_nilai"),
            array("Unbill","ar_unbill"),
            array("Bill","ar_bill"),
            array("Ready To Bill","ar_readytobill"),
            array("Paid","ar_paid"),
            array("Paid 100","ar_paid100"),
            array("Unbill SDV","unbill-sdv"),
            array("Unbill Operation","unbill-operation"),
            array("Unbill UBIS","unbill-ubis"));
        $menu_kontrak_non = array(
            array("Kontrak ","kontrak_non"),
            array("List Kontrak ","kontrak_non_list"),
            array("Draft Kontrak ","kontrak_non_draft"),
            array("Status Transaksi ","kontrak_non_st_trans"),
            array("Inprogress Kontrak ","kontrak_non_inprogress"),
            array("Upload File ","kontrak_non_upload"),
            array("Done Kontrak ","kontrak_non_done"),
            array("List Dispatch Kontrak ","kontrak_non_listdisp"),
            array("Tracking Document ","kontrak_non_track"),
            array("Dashboard Performansi ","kontrak_non_performansi"),
          );
        // DASHBOARD MENU PIPELINE
        $menu_db = array(
            array("Target Revenue Sales & GP","target_rev_sal_gp"),
            array("Pencapaian BAST","pencapaian_bast"),
            array("Revenue SAP","revenue_sap"),
            array("Pencapaian AM","pencapaian_am"),
            array("IFRS Monitoring","ifrs_monitoring")
        );
        // PIPELINE
        $pipeline = array(
            array("CRM","crm"),
            array("Info PBS/Justifikasi","info_pbs_justi"),
            array("Legal Vendor","legal_vendor"),
            array("Legal Customer","legal_customer"),
            array("Delivery Customer","delivery_customer"),
            array("Delivery Vendor","delivery_vendor"),
            array("Report Management","report_management")
        );
      //SPPH LIST MENU LKPP
      $menu_spph_lkpp = array(
        array("Create SPPH","create-spph-lkpp"),
        array("List SPPH","list-spph-lkpp"),
        array("Draft SPPH","draft-spph-lkpp"),
        array("Done SPPH","done-spph-lkpp"),
        array("All SPPH","all-spph-lkpp")
        );
        
        //BAKN LIST MENU LKPP
        $menu_bakn_lkpp = array(
        array("Create BAKN","create-bakn-lkpp"),
        array("Draft BAKN","draft-bakn-lkpp"),
        array("List BAKN","list-bakn-lkpp"),
        array("Done BAKN","done-bakn-lkpp"),
        array("Status Transaksi","status-transaksi-bakn-lkpp"),
        array("Inprogress","inprogress-bakn-lkpp"),
        array("Tracking Document","tracking-document-bakn-lkpp"),
        );
        
        //BAK LIST MENU LKPP
        $menu_bak_lkpp = array(
        array("Create BAK","create-bak-lkpp"),
        array("Draft BAK","draft-bak-lkpp"),
        array("List BAK","list-bak-lkpp"),
        array("Done BAK","done-bak-lkpp"),
        array("Status Transaksi","status-transaksi-bak-lkpp"),
        array("Inprogress","inprogress-bak-lkpp"),
        array("Tracking Document","tracking-bak-lkpp"),
        );
        $menu_ar_dua = array(
        array("List Data","/list-ar-dua"),
        array("Notifikasi ","/ar-dua"),
        array("Dashboard","/ar-dua-dashboard"),
        );

        $menu_dashboard = array(
        array('Dashboard SPPH','/#'),
        array('Dashboard BAKN','/#'),
        array('Dashboard SPK','/#'),
        array('Dashboard SP3','/#'),
        array('Dashboard KONTRAK', '/#'),
        array("Dashboard Performansi ","kontrak_non_performansi"),
        array('Dashboard AR','ar_dashboard'),
        array('Dashboard AR 2','/ar2_dashboard'),
        );
        return view('modules.rolepermission.edit_role', compact( 'role',
        'menu_bak_lkpp',
        'menu_bakn_lkpp',
        'menu_spph_lkpp',
        'menu_db',
        'pipeline',
        'menu_pbs', 
        'menu_spph', 
        'menu_bakn', 
        'menu_spk',
        'menu_spk_non', 
        'menu_kontrak', 
        'menu_mdata',
        'menu_ar',
        'menu_kontrak_non',
        'menu_ar_dua',
        'menu_dashboard'
    ));
    }
    public function editPermission($id)
    {
        $permission = Permission::find($id);

        return view('modules.rolepermission.edit_permission', compact('permission'));
    }

    public function updateRole(Request $request, $id)
    {
        Validator::extend('without_space', function($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        });
        $this->validate($request, [
                'roleName' => 'required|without_space',
                'roleDisplay' => 'required'
        ],['without_space' => "Role name can't contain any space."]);

        $role = Role::find($id);
        $role->name = $request->input('roleName');
				$role->display = $request->input('roleDisplay');
				$arr_menu = $request->input('menuAccess');
				$role->menu_access = implode(",", $arr_menu);
				$role->save();
				$role->syncPermissions($arr_menu	);

        return redirect()->route('rpmanage.index')->with('success', "Data has been edited");
    }
    public function updatePermission(Request $request, $id)
    {
        Validator::extend('without_space', function($attr, $value) {
            return preg_match('/^\S*$/u', $value);
        });
        $this->validate($request, [
            'permissionName' , 'required|without_space',
            'permissionDisplay' , 'required'
        ],['without_space' , "Permission name can't contain any space."]);

        $permission = Permission::find($id);
        $permission->name = $request->input('permissionName');
        $permission->display = $request->input('permissionDisplay');
        $permission->save();

        return redirect()->route('rpmanage.index')->with('success', "Data has been edited");
    }

   
    public function destroyRole($id)
    {
        $role = Role::find($id);
        $arr_permissions = explode(",", $role->menu_access);
        $role->revokePermissionTo($arr_permissions);
        $role->delete();

        return redirect()->route('rpmanage.index')->with('success', "Data Role has been deleted");
    }
    public function destroyPermission($id)
    {
        $permission = Permission::find($id);
        $permission->delete();

        return redirect()->route('rpmanage.index')->with('success', "Data Permission has been deleted");
    }
}
