<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaknLkppRequest;
use App\Models\BaknLKPP;
use App\Models\CaraBayar;
use App\Models\ChatBaknLkpp;
use App\Models\RevisiBaknLkpp;
use App\Models\SpphLKPP;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RevisiBaknLkppController extends Controller
{
    protected $model;
    protected $baknLkpp;
    protected $chat;

    public function __construct()
    {
        $this->middleware('auth');
        $this->model    = new RevisiBaknLkpp();
        $this->baknLkpp = new BaknLKPP();
        $this->chat     = new ChatBaknLkpp();
    }

    public function create(BaknLKPP $baknLKPP)
    {
        $bayar = CaraBayar::all();
        return view('modules.bakn_lkpp.revisi.create', compact('baknLKPP','bayar'));
    }
    
    public function store(BaknLkppRequest $request, BaknLKPP $baknLKPP)
    {
        $data = $request->all();
        $data['lampiran'] = $request->file('lampiran') != null ? $request->file('lampiran') : NULL;
     
        DB::beginTransaction();
         try{
 
             $insert = $this->model->insertRevisiBaknLkpp($baknLKPP);
             $update = $this->baknLkpp->updateRevisi($data, $baknLKPP);
 
             if($request->status == 'save_bakn'){
                  // call fuction chat
                 $this->chat->insertChatRevisi($data, $baknLKPP);
                 // update approval 
                 $this->baknLkpp->getValueApproval($baknLKPP);
                 DB::commit();
                 return redirect('inprogress-bakn-lkpp') ->with(
                    'success',
                    'Revisi BAKN with No.SPPH ('.$update->spph_lkpps["nomorspph"].') was added successfully'
                 );
             }else{
                 DB::commit();
                 return redirect('draft-bakn-lkpp') ->with(
                    'success',
                    'Revisi BAKN with No.SPPH ('.$update->spph_lkpps["nomorspph"].') was added successfully'
                 );
             }
        }catch(Exception $e){
            DB::rollBack();
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
    
    public function show(RevisiBaknLkpp $revisiBaknLkpp)
    {
        $baknLKPP   = $revisiBaknLkpp::with('revisi_bakn_lkpps')->first();
        $spphLKPP   = SpphLKPP::where('nomorspph', $revisiBaknLkpp->spph_id)->first();
        $user       = User::where('username',$revisiBaknLkpp->created_by)->first();
        $checkbox         = explode(",", $revisiBaknLkpp->tipe_rapat);
        $checkbox_mitra   = explode(",", $revisiBaknLkpp->peserta_mitra);
        return view('modules.bakn_lkpp.revisi.preview', compact('revisiBaknLkpp','baknLKPP','spphLKPP','user','checkbox','checkbox_mitra'));
    }
}
