<?php
namespace App\Http\Controllers;

use App\Http\Requests\KontrakLkppRequest;
use App\Models\BaknLKPP;
use App\Models\Chat;
use App\Models\DataFlow;
use App\Models\JenisPasal;
use App\Models\Kontrak;
use App\Models\Pasal;


use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class KontrakLkppController extends Controller
{

    protected $model;
    protected $pasal;
    protected $jenisPasal;
    protected $chat;

    public function __construct()
    {
        $this->model        = new Kontrak();
        $this->chat         = new Chat();
        $this->pasal        = new Pasal();
        $this->jenisPasal   = new JenisPasal();
    }

    public function dump()
    {
        $list = DataFlow::where('transaksi','BAK')
                ->get()
                ->pluck('username');

        if(in_array(Auth::user()->username, json_decode($list)) ){
            dd("ada");
        }else{
            dd("ga ada");
        }
    }

    // Function get nama approval ttd
    public function getNameForLastApproval(Request $request)
    {
        $data = $this->model->getNameLastApproval($request->id)->first();
        return ['data' => $data];
    }

    // function get list data bakn yang belum dibuat kontrak 
    public function getDataSpphWereBaknClosed($id="")
    {
        $data = $this->model->scopeDataSpphWhereBaknDone($id)->get();
        return $data;
    }

    // function get semua data khusus administrator
    public function getDataAll(Request $request)
    {
        $data = $this->model->scopeDataAll($request->month, $request->year)->get();
        return DataTables()->of($data)
                ->addIndexColumn()
                ->addColumn('key', function($data){
                    $date = date("Ym", strtotime($data->created_at));
                    if($data->revisi != null){
     
                       return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
                    }else{
                       return "KT-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);
     
                    }
                 })
                ->make(true);
    }

    // function get semua data dengan status draft
    public function getDataDraft(Request $request)
    {
        $data = $this->model->scopeDataDraft($request->month, $request->year)->get();
        return DataTables()->of($data)
                ->addIndexColumn()
                ->addColumn('key', function($data){
                    $date = date("Ym", strtotime($data->created_at));
                    if($data->revisi != null){
     
                       return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
                    }else{
                       return "KT-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);
     
                    }
                 })
                ->addColumn('user', function($data){
                    $user = array(
                            "username"  => Auth::user()->username, 
                            "role"      => Auth::user()->level
                        );
                    return $user;
                })
                ->make(true);
    }

    // function get data untuk upload files dengan status selesai approval 
    public function getDataList(Request $request)
    {
        $data = $this->model->scopeDataList($request->month, $request->year)->get();
        return DataTables()->of($data)
                ->addIndexColumn()
                ->addColumn('key', function($data){
                    $date = date("Ym", strtotime($data->created_at));
                    if($data->revisi != null){
     
                       return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
                    }else{
                       return "KT-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);
     
                    }
                 })
                ->addColumn('user', function($data){
                    $user = array(
                            "username"  => Auth::user()->username, 
                            "role"      => Auth::user()->level
                        );
                    return $user;
                })
                ->make(true);
    }

    // function get data dengan status done dengan ketentuan file sudah di upload 
    public function getDataDone(Request $request)
    {
        $data = $this->model->scopeDataDone($request->month, $request->year)->get();
        return DataTables()->of($data)
                ->addIndexColumn()
                ->addColumn('key', function($data){
                    $date = date("Ym", strtotime($data->created_at));
                    if($data->revisi != null){
     
                       return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
                    }else{
                       return "KT-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);
     
                    }
                 })
                ->make(true);
    }

    // function get data untuk kondisi approval di list yang penanda tangan, 
    // dan status return jika file di return oleh approval 
    public function getDataStatus(Request $request)
    {
        $data = $this->model->scopeDataStatus($request->month, $request->year)->get();;
        return DataTables()->of($data)
                ->addIndexColumn()
                ->addColumn('user', function($data) {
                    $user = array(
                            "username"  => Auth::user()->username, 
                            "role"      => Auth::user()->level
                        );
                    return $user;
                })
                ->make(true);
    }

    // function get data untuk dokumen yang sirkulir
    public function getDataInprogress(Request $request)
    {
        $data = $this->model->scopeDataInprogress($request->month, $request->year)->get();;
        return DataTables()->of($data)
                ->addIndexColumn()
                ->addColumn('key', function($data){
                    $date = date("Ym", strtotime($data->created_at));
                    if($data->revisi != null){
     
                       return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
                    }else{
                       return "KT-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);
     
                    }
                 })
                ->addColumn('user', function($data) {
                    $user = array(
                            "username"  => Auth::user()->username, 
                            "role"      => Auth::user()->level
                        );
                    return $user;
                })
                ->make(true);
    }

    // function get data untuk semua status dokumen 
    public function getDataTracking(Request $request)
    {
        $data = $this->model->scopeDataTracking($request->month, $request->year)->get();
        return DataTables()->of($data)
                ->addIndexColumn()
                ->addColumn('key', function($data){
                    $date = date("Ym", strtotime($data->created_at));
                    if($data->revisi != null){
     
                       return "REV-$date.".str_pad($data->revisi, 3, 0,STR_PAD_LEFT);
                    }else{
                       return "KT-$date.".str_pad($data->id, 3, 0,STR_PAD_LEFT);
     
                    }
                 })
                ->make(true);
    }


    public function store(KontrakLkppRequest $request)
    {
        DB::beginTransaction();

        // insert data to variable array
        $data = $request->all();
        $data['lampiran'] = $request->file('lampiran') != null ? $request->file('lampiran') : NULL;

        try{
            // call function insert 
            $model = $this->model->insertKontrakLkpp($data);
            // condition status save or draft
            if($model->status == 'save_kontrak'){
                    // insert data chat
                    $this->chat->insertChatKontrak($data);
                    // Call Function approval
                    $approve = $this->model->getValueApproval($model);
                    DB::commit();
                    return redirect('inprogress-kontrak-lkpp')
                            ->with(
                                'success', 
                                "Kontrak with No. $model->nomor_kontrak was added susccesfully, and Notification has been ".$approve->original['sent']
                            );
            }else{
                DB::commit();
                return redirect('draft-kontrak-lkpp')
                            ->with(
                                'success', 
                                "Kontrak with No. $model->nomor_kontrak was added susccesfully"
                            );
            }
        }catch(Exception $e){
            DB::rollBack();
            return redirect()
                    ->back()
                    ->withErrors($e->getMessage())
                    ->withInput();
        }

    }

    public function update(Request $request, Kontrak $kontrak)
    {
        DB::beginTransaction();

        $data = $request->all();
        $data['lampiran'] = $request->file('lampiran') != null ? $request->file('lampiran') : NULL;

        Validator::make($data, [
                'nomor_kontrak' => [Rule::unique('kontrak_lkpps')->ignore($kontrak->id)],
                'bakn_id'       => [Rule::unique('kontrak_lkpps')->ignore($kontrak->id)],
            ]);
            

        try{
            $model = $this->model->updateKontrakLkpp($data, $kontrak);

            if($model->status == 'save_kontrak'){

                // kondisi edit ketika di tahan oleh admin 
                if($model->hold == true ){
                    DB::commit();
                    return redirect('/inprogress-kontrak-lkpp/')
                             ->with(
                                'success',
                                "Kontrak with No. $model->nomor_kontrak was edited susccesfully"
                             );
                }

                // call fuction chat
                $this->chat->insertChatKontrak($data);

                // update approval 
                $approve = $this->model->getValueApproval($model);
                
                DB::commit();
                return redirect('inprogress-kontrak-lkpp')
                ->with(
                    'success', 
                    "Kontrak with No. $model->nomor_kontrak was added susccesfully, and Notification has been ".$approve->original['sent']
                );
            }else{
                DB::commit();
                return redirect('draft-kontrak-lkpp')
                ->with(
                    'success', 
                    "Kontrak with No. $model->nomor_kontrak was edited susccesfully"
                );
            }
        }catch(Exception $e){
            DB::rollBack();
            return redirect()
                    ->back()
                    ->withErrors($e->getMessage())
                    ->withInput();
        }
    }

    // function next approval 
    public function chatApproval(Request $request, Kontrak $kontrak)
    {
        if(Auth::user()->username != $kontrak->approval){
            return redirect()
                ->back()
                ->withErrors("Sorry You don't deserve to approve this document");
        }

      DB::beginTransaction();

      $data          = $request->all();
      $arrApproval   = json_decode($kontrak->new_approval, TRUE);

      try{
         // insert chat 
         $this->chat->approveChatKontrak($data, $kontrak);
         // condtion approval is return 
         if($data['status'] == 'Return'){
            $approve = $this->model->approvalReturn($kontrak);
         }
         // condition approval is Approve
         if($data['status'] == 'Approve'){
            //condition if function approval masih cara lama
            if($kontrak->new_approval == NULL){
               $approve = $this->model->nextApprovalOld($kontrak);
               DB::commit();
               return redirect('inprogress-kontrak-lkpp')
                        ->with(
                           'success', 
                           'Kontrak with No. ('.$kontrak->nomor_kontrak.') has been '. $data['status'].', and Notification has been '.$approve->original['sent']
                        );
            }
            //condition jika function approval menggunakan json
            if(Auth::user()->username != end($arrApproval)){
               $approve = $this->model->nextApprovalNew($kontrak);
            }
            // condition approval selesai
            if(Auth::user()->username == end($arrApproval) || Auth::user()->username == $kontrak->end_approval){
               $approve = $this->model->approvalClosed($kontrak);
            }  
         }
         DB::commit();
         return redirect('inprogress-kontrak-lkpp')
                  ->with(
                     'success', 
                     'Kontrak with No. ('.$kontrak->nomor_kontrak.') has been '. $data['status'].', and Notification has been '.$approve->original['sent']
                  );
      }catch(Exception $e){
         DB::rollback();
         return redirect()
                  ->back()
                  ->withErrors($e->getMessage());
      }
    }


    public function previewStatus(Kontrak $kontrak)
    {
        $bakn       = $kontrak->bakn_lkpp;
        $spphLKPP   = $kontrak->bakn_lkpp->spph_lkpps;
        $checkbox   = explode(",", $bakn->tipe_rapat); 
        $chats      = DB::table('chats')
                        ->join('users','chats.username','=','users.username')
                        ->select('chat','jabatan','chats.username','users.name','chats.created_at')
                        ->where('idTransaksi',$kontrak->id)
                        ->orderBy('chats.created_at','asc')
                        ->get();
        return view('modules.kontrak_lkpp.review-approval', compact('kontrak','spphLKPP','bakn','checkbox','chats'));
    }


     // function upoad file where status save
    public function file(Request $request, Kontrak $kontrak)
    {
        // declare variable array 
        $data           = $request->all();
        $data['file']   = $request->file('file') != null ? $request->file('file') : NULL;

        try{
            // send data to model for upload file 
            $this->model->uploadFile($data, $kontrak);
            DB::commit();            
            return redirect('/done-kontrak-lkpp/')->with('success', 'Your files has been successfully uploaded');
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()
           ->withErrors($e->getMessage())
           ->withInput();
        }

    }
 
    public function lampiran(Request $request, Kontrak $kontrak)
    {
        // declare data to array 
        $data   = $request->all(); 

        try{
            // send data to model for upload lampiran 
            $this->model->uploadLampiran($data, $kontrak);    
            DB::commit();
            return redirect()->back()->with('success', 'Your lampirans has been successfully uploaded');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect()->back()
           ->withErrors($e->getMessage())
           ->withInput();
        }
    }

    public function destroy(Kontrak $kontrak)
    {
        // declare model
        $path   = "public/files/KONTRAK_LKPP/$kontrak->id";
        Storage::deleteDirectory($path);
        $kontrak->delete();
        return response($kontrak);
    }

    public function holdOrRelease(Kontrak $kontrak)
    {
      if($kontrak->hold == true){
         $kontrak->hold = false;
      }else{
         $kontrak->hold = true;
      }
      $kontrak->save();
      return response($kontrak);
    }
    
}