<?php

namespace App\Http\Controllers;

use App\Models\JenisPasal;
use Illuminate\Http\Request;
use DB;


class JenisPasalController extends Controller
{

    protected $model;

    public function __construct()
    {
        $this->model = new JenisPasal();
    }

    // Function ServerSide

    public function getDataAll()
    {
        $data = $this->model->getDataAll()->get();
        return $data;
    }

    public function getDetailData(JenisPasal $jenisPasal)
    {
        return $jenisPasal;
    }


    // Function CRUD
   
    public function index()
    {
        $pasal=JenisPasal::all();
        return view('modules.jenispasal.index',compact('pasal'));
    }
  
    public function create()
    {
        return view('modules.jenispasal.create');
    }
 
    public function store(Request $request)
    {
        $jenispasal = new JenisPasal();
        $jenispasal->jenis_pasal = $request->get('jenis_pasal');
        $jenispasal->isi_pasal = $request->get('isi_pasal');
        $jenispasal->save();
        return redirect('jenispasal')->with('success', 'Information has been added');
    }

    public function edit( $id)
    {
        $pasals=JenisPasal::find($id);
        return view('modules.jenispasal.edit',compact('pasals','id'));
    }

    public function update(Request $request,  $id)
    {
        $jenispasal= JenisPasal::find($id);
        $jenispasal->jenis_pasal = $request->get('jenis_pasal');
        $jenispasal->isi_pasal = $request->get('isi_pasal');
        $jenispasal->save();
        return redirect('jenispasal')->with('success', 'Information has been edited');
    }

    public function destroy( $id)
    {
        $pasal = JenisPasal::find($id);
        $pasal->delete();
        return redirect('jenispasal')->with('success','Data has been  deleted');
    }
}
