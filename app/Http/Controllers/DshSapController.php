<?php

namespace App\Http\Controllers;

use App\DshSap;
use Illuminate\Http\Request;
use App\PlanBast;
use App\Ubis;
use App\SpkPksVendor;
use App\SpkPksCustomer;
use DB;
use DataTables;

class DshSapController extends Controller
{
    public function ifrs(){
        
        return view('modules.dashpipeline.ifrs');
    }
    public function jsonIfrsCustomer(){
        $data = SpkPksCustomer::with('customerSpkPks','ioSpkPksCustomer','mappingIoCustomer.ubisMappingCustomer');
        return Datatables::of($data) 
        ->addColumn('periode', function ($data){
            $start = strtotime($data->start_layanan);
            $end = strtotime($data->end_layanan);
            $secs = $end - $start;
            return ($secs/86400).'day';
        })
        ->addIndexColumn()
        ->make(true);
    }
    public function jsonIfrsVendor(){
        $data = SpkPksVendor::with('akunVendor','mappingIo','ioSpkPksVendor','mappingIo.ubisMappingVendor');
        // dd($data);
        return Datatables::of($data) 
        ->addColumn('periode', function ($data){
            $start = strtotime($data->start_layanan);
            $end = strtotime($data->end_layanan);
            $secs = $end - $start;
            return ($secs/86400).'day';
        })
        ->addColumn('vendornya',function($data){
            if($data->akunVendor['deskripsi_vendor'] != NULL){
                return $data->akunVendor['deskripsi_vendor'];
                
            }else{
                return "NULL";
            }
        })
        ->editColumn('start_layanan', function ($data) 
            {
                return date('d.F.Y', strtotime($data->start_layanan) );
            })
        ->editColumn('end_layanan', function ($data) 
        {
            return date('d.F.Y', strtotime($data->end_layanan) );
        })
        ->addIndexColumn()
        ->make(true);

    }
    
    public function pencapaianAM(){
        
        $data = DB::connection('mysql2')->table('table_am')
        ->leftJoin('t_inisiasi','t_inisiasi.am_id','table_am.id')
        ->join('t_ubis','t_ubis.id_ubis','t_inisiasi.id_ubis')
        ->select('table_am.*','t_inisiasi.*',DB::raw('sum(t_inisiasi.nilai_project) as nilai'),'t_ubis.after_to as ubis')
        ->whereMonth('t_inisiasi.tgl_target_win',1)
        ->whereYear('t_inisiasi.tgl_target_win',2019)
        ->groupBy('table_am.id')
        ->get();
        return view('modules.dashpipeline.am',compact('data'));
    }
    
    public function index()
    {
        $bulan = 2;
        $tahun = 2016;
        if ($bulan = 10) {
            $bulan_rkap = 'okt';
        }
        $data2 = Ubis::withCount(['ubis_outlook'=>function($q)use ($bulan, $tahun){
            $q->select(DB::raw('sum(o_l)'))->where([
                ['bulan',$bulan],
                ['tahun',$tahun],
                ])
                ->groupBy('ubis');
            }
            ])
            ->orderBy('urutan','asc')->get();
            
            return view('modules.dashpipeline.index',compact('data2'));
        }
        
    public function create()
    {
            //
    }
        
    public function store(Request $request)
        {
            //
        }
        
    public function show()
    {
        // $db = PlanBast::with('planbast_ubis')->where('bulan', 2)->limit(1)->get();
        $bulan = 10;
        $tahun = 2016;
        if ($bulan = 10) {
            $bulan_rkap = 'okt';
        }
        DB::connection("mysql2")->enableQueryLog();
        $data2 = Ubis::with(['ubis_outlook' => function($q) use ($bulan, $tahun){
            $q->select('ubis',DB::raw('sum(o_l) as total'))->where([
                ['bulan',$bulan],
                ['tahun',$tahun],
                ])->groupBy('ubis');
            }
            ])->get();
            $data = PlanBast::select('ubis',DB::raw('sum(o_l) as total'))
            ->where([
                ['bulan',$bulan],
                ['tahun',$tahun],
                ])->groupBy('ubis')->get();
                dd($data2, DB::connection("mysql2")->getQueryLog());
            }
             
    public function uploadCsv(Request $request)
    {
       $file = $request->file('filenya');
       $baca = fopen($file,'r');
       fgetcsv($baca);
       fgetcsv($baca);
       while($nilai = fgetcsv($baca)){
        $data[]=$nilai;
        print_r($nilai);
       }
    //    return Datatables::of($data) 
    //     ->addIndexColumn()
    //     ->make(true);
        // dd($data[2]);
       
    }
   
}
            