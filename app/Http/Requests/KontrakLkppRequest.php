<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class KontrakLkppRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'required'          => ':Attribute field is required.',
            'bakn_id.required'  => 'Pilih SPPH terlebih dahulu.',
            'unique'            => 'Nomor Kontrak have already exist',
            'exists'            => 'SPPH have already exist'

        ];
    }

    public function rules()
    {
        $rules      = [];
        $id         = $this->get('id') ? $this->get('id') : '';
        $bakn_id    = $this->get('bakn_id') ? $this->get('bakn_id') : '';
        
        // dd($this->request, $id);
        if($this->get('status') == 'save_kontrak'){
            $rules = [
                    'nomor_kontrak'     => [
                                            'required', 
                                            Rule::unique('kontrak_lkpps')->ignore($id)
                                        ],
                    'bakn_id'           => ['required', Rule::unique('kontrak_lkpps')->ignore($bakn_id)],
                    'chat'              => 'required',
                    'tanggal_kontrak'   => 'required',
                    'isi'               => 'required',
            ];
        }else{
            $rules = [
                    'bakn_id'       => ['required', Rule::unique('kontrak_lkpps')->ignore($bakn_id)],
                    'nomor_kontrak' => Rule::unique('kontrak_lkpps')->ignore($id),
            ];
        }
        return $rules;
    }
}
