<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class BakLkppRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'required'          => ':Attribute field is required.',
            'spphid.required'   => 'Pilih SPPH terlebih dahulu.',
            'unique'            => 'No. BAK have already exist',

        ];
    }

    public function rules()
    {
        $rules = [];
        $id = $this->get('id') ? $this->get('id') : '';
        
        // dd($this->request, $id);
        if($this->get('status') == 'save_bak'){
            $rules = [
                    'nomor_bak' => [
                                    'required', 
                                    Rule::unique('bak_lkpps')->ignore($id)
                                ],
                    'chat'      => 'required',
                    'tglbak'    => 'required',
                    'harga'     => 'required',
                    'isi'       => 'required',
            ];
        }else{
            $rules = [
                    'spphid'    => 'required',
                    'nomor_bak' => Rule::unique('bak_lkpps')->ignore($id),
            ];
        }
        return $rules;
    }
}
