<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormRevisiKontrakLkpp extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'required'          => ':Attribute field is required.',
            'bakn_id.required'  => 'Pilih SPPH terlebih dahulu.',
            'unique'            => 'Nomor Kontrak have already exist',
            'exists'            => 'SPPH have already exist'

        ];
    }

    public function rules()
    {
        $rules = [];
        
        if($this->get('status') == 'save_kontrak'){
            $rules = [ 
                'chat' => 'required',
                
            ];
        }
        return $rules;
    }
}
