<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BaknLkppRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'required'          => ':Attribute field is required.',
            'unique'            => 'Nomor SPPH have already exist',
            'tglbakn.required'  => 'Tanggal BAKN is required'
        ];
    }

    public function rules()
    {
        // dd($this->request);
        $rules = [];
        $spph_id = $this->get('spph_id') ? $this->get('spph_id') : '';

        if($this->get('status') == 'save_bakn'){
            $rules = [
                    // 'spph_id'   => ['required', Rule::unique('bakn_lkpps')->ignore($spph_id)],
                    'chat'      => 'required',
                    'tglbakn'   => 'required',
                    'harga'     => 'required',
            ];
        }else{
            $rules = [
                // 'spph_id'   => ['required', Rule::unique('bakn_lkpps')->ignore($spph_id)],
                'tglbakn'   => 'required',
            ];
        }
        return $rules;
    }
}
