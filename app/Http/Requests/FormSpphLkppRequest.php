<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FormSpphLkppRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }
    
    public function messages()
    {
        return [
            'required'              => ':Attribute field is required.',
            'nomorspph.required'    => 'Nomor SPPH field is required',
            'tglspph.required'      => 'Tanggal SPPH field is required',
            'unique'                => 'Nomor SPPH have already exist',
        ];
    }
    
    public function rules()
    {
        // dd($this->request);
        $rules = [];
        $id = $this->get('id') ? '' . $this->get('id') : '';
   
            if($this->get('status') == 'save_spph'){
                $rules = [
                    'nomorspph'     => [
                                        'required', 
                                        Rule::unique('spph_lkpps')->ignore($id)
                                    ],
                    'tglspph'   => 'required',
                    'tglsph'    => 'required',
                    'judul'     => 'required|min:10',
    
                ];
            }else{
                $rules = [
                    'nomorspph' => [
                                    'required', 
                                    Rule::unique('spph_lkpps')->ignore($id)
                                ],
                    'tglspph'   => 'required',
                ];
            }
        
        return $rules;
    }
}
