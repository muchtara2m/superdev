<?php

// -----------------------------------------------------ROUTE KETENTUANS -----------------------------------------------------
Route::group(['prefix' => '/ketentuans'], function () {
    
    // VIEW ROUTE
        Route::view('/',        'modules.ketentuans.index');
        Route::view('/create',  'modules.ketentuans.create');
        Route::view('/edit',    'modules.ketentuans.cedit');

    // METHOD ROUTE
        Route::post('/store','KetentuansController@store');
        Route::post('/update','KetentuansController@update');
        
    // API ROUTE
        Route::get('/all','KetentuansController@allData');
        Route::get('/detail/{ketentuans}','KetentuansController@detailData');
});

// ----------------------------------------------------- END ROUTE KETENTUANS -----------------------------------------------------

// ----------------------------------------------------- ROUTE KETENTUANS -----------------------------------------------------
Route::group(['prefix' => '/jenis-pasals'], function () {
    
    // VIEW ROUTE
        // Route::view('/',        'modules.ketentuans.index');
        // Route::view('/create',  'modules.ketentuans.create');
        // Route::view('/edit',    'modules.ketentuans.cedit');

    // METHOD ROUTE
        // Route::post('/store','KetentuansController@store');
        // Route::post('/update','KetentuansController@update');
        
    // API ROUTE
        Route::get('/all','JenisPasalController@getDataAll');
        Route::get('/detail/{jenisPasal}','JenisPasalController@getDetailData');
});

// ----------------------------------------------------- END ROUTE KETENTUANS -----------------------------------------------------





