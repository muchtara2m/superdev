<?php
$router->group([
    'middleware' => 'auth',
    // 'prefix'     => ''
], function () use ($router) {
    
    include __DIR__.'/ecom/index.php';
    include __DIR__.'/gs/index.php';
      // test ldap
      Route::post('test/login', 'PanelController@authLdap')->name('test-login');
      Route::get('/home', 'DashboardController@main');
  
      // -------------------------------ROUTE ALL MENU ------------------------------------------
  
      // -------------------Route PBS------------------
      Route::get('/index-pbs', function(){
          return view('modules.transaksi.index-pbs');
      })->name('index-pbs');
      Route::get('/status-transaksi', function(){
          return view('modules.transaksi.status-transaksi');
      })->name('status-transaksi');
      Route::get('/inprogress', function(){
          return view('modules.transaksi.inprogress');
      })->name('inprogress');
      Route::get('/selesai', function(){
          return view('modules.transaksi.selesai');
      })->name('selesai');
      Route::get('/revisi-transaksi', function(){
          return view('modules.transaksi.revisi-transaksi');
      })->name('revisi-transaksi');
      Route::get('/kalkulator-pinjaman', function(){
          return view('modules.transaksi.kalkulator-pinjaman');
      })->name('kalkulator-pinjaman');
      Route::get('pbs-create','PbsController@create')->name('pbs-create');
      Route::get('pbs-purchasing','PbsController@purchasing')->name('pbs-purchasing');
      // ------------------- END Route PBS------------------
  
  
      // ------------------- Route DASHBOARD ------------------
      Route::get('db_sap', 'DshSapController@index')->name('db_sap');
      Route::get('db_outlook', 'DshSapController@show');
      Route::get('db_am','DshSapController@pencapaianAM');
      Route::get('db-ifrs','DshSapController@ifrs')->name('db-ifrs');
      Route::get('ifrs-customer-json', 'DshSapController@jsonIfrsCustomer');
      Route::get('ifrs-vendor-json', 'DshSapController@jsonIfrsVendor');
      Route::post('upload-csv', 'DshSapController@uploadCsv');
      
      
      // ------------------- Route PIPELINE -------------------
      Route::get('entry-update', 'PipelineController@entryUpdate')->name('entry-update');
      Route::get('entry-outlook', 'PipelineController@entryOutlook')->name('entry-outlook');
      Route::get('create-inisiasi','PipelineController@createInisiasi')->name('create-inisiasi');
      Route::get('edit-inisiasi','PipelineController@editInisiasi')->name('edit-inisiasi');
      // ------------------- END Route PIPELINE -------------------
      
  
      //---------------- RPManage -------------------
      Route::get('create-role', 'RolePermissionController@createRole')->name('rpmanage.createRole');
      Route::get('create-permission', 'RolePermissionController@createPermission')->name('rpmanage.createPermission');
      Route::post('store-role', 'RolePermissionController@storeRole')->name('rpmanage.storeRole');
      Route::post('store-permission', 'RolePermissionController@storePermission')->name('rpmanage.storePermission');
      Route::delete('delete-permission/{id}', 'RolePermissionController@destroyPermission')->name('rpmanage.destroyPermission');
      Route::delete('delete-role/{id}', 'RolePermissionController@destroyRole')->name('rpmanage.destroyRole');
      Route::get('rpmanage/{id}/edit-role', 'RolePermissionController@editRole')->name('rpmanage.editRole');
      Route::get('rpmanage/{id}/edit-permission', 'RolePermissionController@editPermission')->name('rpmanage.editPermission');
      Route::put('rpmanage/{id}/update-role', 'RolePermissionController@updateRole')->name('rpmanage.updateRole');
      Route::put('rpmanage/{id}/update-permission', 'RolePermissionController@updatePermission')->name('rpmanage.updatePermission');
      //----------------END RPManage -------------------
  
      // ------------------Route AR------------------
      Route::post('data-io', ['as'=>'data-io','uses'=>'ARController@getdataio']);
      Route::get('create_ar', 'ARController@create');
      Route::get('unbill_ar', 'ARController@index');
      Route::get('edit_ar/{id}', 'ARController@edit')->name('edit_ar');
      Route::get('preview_ar/{id}','ARController@show')->name('preview_ar');
      Route::post('insert_ar','ARController@store')->name('insert_ar');
      Route::post('update_ar/{id}', 'ARController@update')->name('update_ar');
      Route::get('readytobill', 'ARController@readytobill')->name('readytobill');
      Route::get('bill','ARController@bill')->name('bill');
      Route::get('paid','ARController@paid')->name('paid');
      Route::get('paid100','ARController@paid100')->name('paid100');
      Route::post('update-nilai/{id}','ARController@updatenilai')->name('update-nilai');
      Route::post('upload/{id}','ARController@upload')->name('upload');
      Route::get('nilai','ARController@nilaiproject')->name('nilai');
      Route::get('db_ar','ARController@dashboard')->name('db_ar');
      Route::post('db-ar','ARController@dashboard');
      Route::get('unbill-sdv','ARController@json_sdv')->name('unbill-sdv');
      Route::get('unbill-operation','ARController@json_operation')->name('unbill-operation');
      Route::get('unbill-ubis','ARController@json_ubis')->name('unbill-ubis');
      Route::post('nilai-project/{id}','ARController@updateproject')->name('nilai-project');
      Route::get('live-search', ['as'=>'live-search','uses'=>'ARController@action']);
      // -------------------- END ROUTE AR -----------------------------------
  
      // ------------------------- Route AR DUA ------------------------------
      Route::get('/ar-dua','ArDuaController@dataNotifikasi');
      Route::get('/list-ar-dua','ArDuaController@index');
      Route::post('/update-dokumen', 'ArDuaController@updateStatus');
      Route::get('data-new-ar','ArDuaController@dataNewAr');
  
      // Route::get('send-wa-ar', 'TestingController@kirimDataToWa');
  
  
      // ------------------------- End Route AR DUA ------------------------------
  
  
       // ------------------Route AR2------------------
       Route::post('data-io', ['as'=>'data-io','uses'=>'ARController@getdataio']);
       Route::get('create_ar', 'ARController@create');
       Route::get('unbill_ar', 'ARController@index');
       Route::get('edit_ar/{id}', 'ARController@edit')->name('edit_ar');
       Route::get('preview_ar/{id}','ARController@show')->name('preview_ar');
       Route::post('insert_ar','ARController@store')->name('insert_ar');
       Route::post('update_ar/{id}', 'ARController@update')->name('update_ar');
       Route::get('readytobill', 'ARController@readytobill')->name('readytobill');
       Route::get('bill','ARController@bill')->name('bill');
       Route::get('paid','ARController@paid')->name('paid');
       Route::get('paid100','ARController@paid100')->name('paid100');
       Route::post('update-nilai/{id}','ARController@updatenilai')->name('update-nilai');
       Route::post('upload/{id}','ARController@upload')->name('upload');
       Route::get('nilai','ARController@nilaiproject')->name('nilai');
       Route::get('db_ar','ARController@dashboard')->name('db_ar');
       Route::post('db-ar','ARController@dashboard');
       Route::get('unbill-sdv','ARController@json_sdv')->name('unbill-sdv');
       Route::get('unbill-operation','ARController@json_operation')->name('unbill-operation');
       Route::get('unbill-ubis','ARController@json_ubis')->name('unbill-ubis');
       Route::post('nilai-project/{id}','ARController@updateproject')->name('nilai-project');
       Route::get('live-search', ['as'=>'live-search','uses'=>'ARController@action']);
       // -------------------- END ROUTE AR -----------------------------------
  
      /////////////// MASTER DATA ////////////////
      Route::resources([
          'user' => 'UserController',
          'chairman' => 'ChairmanController',
          'mitra' => 'MitraController',
          'pasal' => 'PasalController',
          'unit' => 'UnitController',
          'flow' => 'DataFlowController',
          'rpmanage' => 'RolePermissionController',
          'jenispasal' => 'JenisPasalController',
          'tatacara' => 'CaraBayarController',
          ]);
          /////////////// MASTER DATA ////////////////
  
          // ------------------------------- END ROUTE ALL MENU ------------------------------------------
  
  
  
          // ----------------------- ROUTE MENU GENERAL SUPPORT ------------------------------------------
  
          // -------------------Route SPPH------------------
          Route::get('spph-index', 'SpphController@index')->name('spph-index');
          Route::get('spph-done', 'SpphController@done')->name('spph-done');
          Route::get('spph-draft', 'SpphController@draft')->name('spph-draft');
          Route::get('spph-create', 'SpphController@create')->name('spph-create');
          Route::post('spph-store', 'SpphController@store')->name('spph-store');
          Route::get('spph-edit/{id}', 'SpphController@edit')->name('spph-edit');
          Route::post('spph-update/{id}', 'SpphController@update')->name('spph-update');
          Route::get('spph-delete/{id}','SpphController@destroy');
          Route::get('spph-preview/{id}', 'SpphController@preview')->name('spph-preview');
          Route::post('spph-lampiran/{id}','SpphController@lampiran');
          Route::post('select-spph', ['as'=>'select-spph','uses'=>'SpphController@selectSpph']);
          // ------------------- End Route SPPH------------------
  
  
          // Route BAKN
          Route::post('data-spph', ['as'=>'data-spph','uses'=>'BaknController@dataspph']);
          Route::get('/cara-bayar-bakn', 'BaknController@caraBayar')->name('cara_bayar');
          Route::get('bakn-spph', 'BaknController@baknspph')->name('bakn-spph');
          Route::get('bakn-create', 'BaknController@create')->name('create.bakn');
          Route::get('list-bakn', 'BaknController@index')->name('list.bakn');
          Route::post('bakn-store', 'BaknController@store')->name('store.bakn');
          Route::get('bakn-edit/{id}', 'BaknController@edit')->name('edit.bakn');
          Route::post('bakn-update/{id}', 'BaknController@update')->name('update.bakn/{id}');
          Route::get('bakn-preview/{id}', 'BaknController@preview')->name('preview.bakn');
          Route::post('bakn-approve', 'BaknController@approve')->name('approve.bakn');
          Route::get('delete/{id}','BaknController@destroy');
          Route::get('bakn-draft','BaknController@draft')->name('draft.bakn');
          Route::get('bakn-done','BaknController@done')->name('done.bakn');
          Route::get('ds-bakn', 'BaknController@dashboard');
          Route::post('bakn-upload/{id}','BaknController@upload')->name('upload.bakn');
          Route::post('return/{id}','BaknController@returnbakn')->name('return.bakn');
          // Route::get('bakn-view-upload/{id}', 'BaknController@viewUpload');
          Route::get('/page-update-io/{id}','BaknController@updatePageIo');
          Route::post('/update-io/{id}','BaknController@updateIo');
          // -----------------------END ROUTE BAKN-----------------------
  
          // SPK NON
          Route::get('create-spk-non','SpkNonController@create');
          Route::post('store-spk-non','SpkNonController@store');
          Route::get('list-sp3k-non','SpkNonController@index');
          Route::get('preview-spk-non','SpkNonController@preview');
          Route::get('edit-spk-non','SpkNonController@edit');
          Route::post('update-spk-non/{id}','SpkNonController@update');
          Route::post('delete-spk-non/{id}','SpkNonController@destroy');
          Route::get('draft-spk-non','SpkNonController@draft');
          Route::get('done-spk-non','SpkNonController@done');
          // --------------------END SPK NON-------------------------------------------
  
  
          // ------------------------Route SP3/SPK-------------------
          // // ajax sp3
          Route::get('create-sp3k','Sp3Controller@create')->name('sp3k');
          Route::get('list-sp3k', 'Sp3Controller@index');
          Route::get('draft-sp3k', 'Sp3Controller@draft');
          Route::get('done-sp3k', 'Sp3Controller@done');
          Route::get('edit-sp3/{id}', 'Sp3Controller@edit')->name('edit.sp3');
          Route::get('edit-spk/{id}', 'SpkController@edit')->name('edit.spk');
          Route::post('store-spk','SpkController@store');
          Route::post('store-sp3','Sp3Controller@store');
          Route::get('list-sp3k', 'Sp3Controller@index');
          Route::get('sp3-preview/{id}', 'Sp3Controller@preview');
          Route::get('spk-preview/{id}', 'SpkController@preview');
          Route::post('update-sp3/{id}','Sp3Controller@update')->name('update.sp3');
          Route::post('update-spk/{id}','SpkController@update')->name('update.spk');
          Route::get('delete-sp3/{id}', 'Sp3Controller@destroy');
          Route::get('delete-spk/{id}', 'SpKController@destroy');
          Route::get('inprogress-sp3k', 'Sp3Controller@inprogress');
          Route::get('approve-sp3/{id}', 'Sp3Controller@approve');
          Route::post('chat-sp3', 'Sp3Controller@chat')->name('chat-sp3');
          Route::post('sp3-upload/{id}','Sp3Controller@upload');
          Route::get('status-sp3k', 'Sp3Controller@status');
          // ----------------------End route SP3/SPK---------------------------
  
          // ----------------- Route Kontrak NON--------------------------
          Route::get('kontrak-non', 'KontrakNonController@index')->name('kontrak-non');
          Route::get('kontrak-non-list', 'KontrakNonController@list')->name('kontrak-non-list');
          Route::get('kontrak-non-create/{id}', 'KontrakNonController@create')->name('kontrak-non-create');
          Route::post('kontrak-non-store','KontrakNonController@store')->name('kontrak-non-store');
          Route::post('kontrak-non-dispbakn/{id}', 'KontrakNonController@dispbakn');
          Route::post('kontrak-non-dispsp3/{id}', 'KontrakNonController@dispsp3');
          Route::get('kontrak-non-draft', 'KontrakNonController@draft')->name('kontrak-non-draft');
          Route::get('kontrak-non-done', 'KontrakNonController@done')->name('kontrak-non-done');
          Route::get('kontrak-non-preview/{id}', 'KontrakNonController@preview')->name('kontrak-non-preview');
          Route::get('deletekontrak-non/{id}','KontrakNonController@destroy');
          Route::get('kontrak-non-edit/{id}', 'KontrakNonController@edit')->name('kontrak-non-edit');
          Route::post('kontrak-non-update/{id}', 'KontrakNonController@update')->name('kontrak-non-update');
          Route::get('ds-kontrak-non', 'KontrakNonController@dashboard');
          Route::get('kontrak-non-inprogress','KontrakNonController@inprogress')->name('kontrak-non-inprogress');
          Route::get('kontrak-non-status', 'KontrakNonController@status')->name('kontrak-non-status');
          Route::get('kontrak-non-approve/{id}', 'KontrakNonController@approve')->name('kontrak-non-approve');
          Route::post('kontrak-non-chat', 'KontrakNonController@chat')->name('kontrak-non-chat');
          Route::get('kontrak-non-return', 'KontrakNonController@return')->name('kontrak-non-return');
          Route::get('kontrak-non-return_form/{id}', 'KontrakNonController@return_form')->name('kontrak-non-return_form');
          Route::post('kontrak-non-return_approve/{id}', 'KontrakNonController@return_approve')->name('kontrak-non-return_approve');
          Route::get('kontrak-non-preview-status/{id}','KontrakNonController@preview_status')->name('kontrak-non-preview-status');
          Route::get('kontrak-non-print_preview/{id}','KontrakNonController@print_preview')->name('kontrak-non-print_preview');
          Route::post('kontrak-non-upload/{id}','KontrakNonController@upload');
          Route::get('kontrak-non-upload','KontrakNonController@uploadFile');
          Route::post('kontrak-non-print/{id}','KontrakNonController@print');
          Route::post('kontrak-non-word/{id}','KontrakNonController@word');
          Route::get('kontrak-non-listdisp','KontrakNonController@listdispatch');
          Route::get('kontrak-non-tracking','KontrakNonController@tracking')->name('kontrak-non-tracking');
          Route::get('kontrak-non-performansi', 'KontrakNonController@performansi')->name('performansi');
          
          Route::get('data-performansi','KontrakNonController@dataPerformansiSpkPks');
          Route::get('data-pola-pembayaran','KontrakNonController@polaPembayaran');
          Route::get('data-klarifikasi-nilai','KontrakNonController@klarifikasiNilai');
          Route::get('data-detail-pbs','KontrakNonController@detailDataPbs');
          Route::get('data-detail-plan','KontrakNonController@detailDataPlan');
          Route::get('data-detail-selesai','KontrakNonController@detailDataSelesai');
  
          // -----------------END ROUTE KONTRAK NON----------------------------
  
          // ----------------------- END ROUTE MENU GENERAL SUPPORT -----------------------------
  
  
  
  
          // ------------------------- ROUTE LKPP ---------------------------------
  
  
          // ----------------------Route SP3 LKPP --------------------------------
          Route::post('data-bakn', ['as'=>'data-bakn','uses'=>'Sp3Controller@databakn']);
          Route::get('create-sp3-lkpp','Sp3Controller@create');
          Route::post('store-sp3-lkpp','Sp3Controller@store');
          Route::get('preview-sp3-lkpp/{id}','Sp3Controller@preview');
          Route::get('list-sp3-lkpp','Sp3Controller@list');
          Route::get('draft-sp3-lkpp','Sp3Controller@draft');
          Route::get('done-sp3-lkpp','Sp3Controller@done');
          Route::get('status-transaksi-sp3-lkpp','Sp3Controller@status_transaksi');
          Route::get('inprogress-sp3-lkpp','Sp3Controller@inprogress');
          Route::get('tracking-sp3-lkpp','Sp3Controller@tracking');
          Route::get('edit-sp3-lkpp/{id}','Sp3Controller@edit');
          Route::post('update-sp3-lkpp/{id}','Sp3Controller@update');
          Route::post('store-sp3-lkpp','Sp3Controller@store');
          Route::post('upload-file-sp3-lkpp/{id}','Sp3Controller@file');
          Route::get('delete-sp3-lkpp/{id}','Sp3Controller@destroy');
          Route::get('preview-status-sp3-lkpp/{id}','Sp3Controller@preview_status');
          Route::post('chat-approval-sp3-lkpp','Sp3Controller@chatApproval');
  
          // ----------------------Route SP3 LKPP --------------------------------
  
        
  
          // ----------------- Route Kontrak LKPP--------------------------
  
          // ------------------------- END ROUTE LKPP ---------------------------------
  
          // ----------------testing menu-----------------------
          Route::get('telp', function () {
              return view('testing');
          });
          Route::get('test-wa','NotifikasiWAController@index')->name('test-wa');
          Route::post('notif','NotifikasiWAController@store')->name('notif');
  
          //-----command-----
          Route::get('/clear-cache', function() {
              Artisan::call('cache:clear');
              return "Cache is cleared";
          });
          ///////// playground ////////
          Route::get('bakn_old', function(){
              return view('modules.konbak.bakn_old');
          });
          Route::get('/check-auth', function(){
              return view('modules.auth.login');
          })->name('check-auth');
          //-----command-----
  
        
          // ------------------------testing menu ---------------------------
          


});