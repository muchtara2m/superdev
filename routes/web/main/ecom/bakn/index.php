<?php

    Route::get('data-spph-lkpp','BaknLKPPController@dataspph');

    // ----------------------Route BAKN LKPP --------------------------------
    Route::get('create-bakn-lkpp','BaknLKPPController@create')->name('create-bakn-lkpp');
    Route::get('preview-bakn-lkpp/{baknLKPP}','BaknLKPPController@preview');
    Route::get('edit-bakn-lkpp/{baknLKPP}','BaknLKPPController@edit');
    Route::put('update-bakn-lkpp/{baknLKPP}','BaknLKPPController@update');
    Route::post('store-bakn-lkpp','BaknLKPPController@store');
    Route::post('file-bakn-lkpp/{baknLKPP}','BaknLKPPController@file');
    Route::post('lampiran-bakn-lkpp/{baknLKPP}','BaknLKPPController@lampiran');
    Route::get('delete-bakn-lkpp/{id}','BaknLKPPController@destroy');
    Route::get('hold-bakn-lkpp/{baknLKPP}','BaknLKPPController@holdOrRelease');
    Route::get('preview-status-bakn-lkpp/{baknLKPP}','BaknLKPPController@preview_status');
    Route::post('chat-approval-bakn-lkpp/{baknLKPP}','BaknLKPPController@chatApproval')->name('lkpp.bakn.approve');
    
    
    // ----------------------Route BAKN LKPP --------------------------------

    // Route View
    Route::view('/draft-bakn-lkpp','modules.bakn_lkpp.draft');
    Route::view('/list-bakn-lkpp','modules.bakn_lkpp.list');
    Route::view('/done-bakn-lkpp','modules.bakn_lkpp.done');
    Route::view('/inprogress-bakn-lkpp','modules.bakn_lkpp.inprogress');
    Route::view('/status-transaksi-bakn-lkpp','modules.bakn_lkpp.status_transaksi');
    Route::view('/tracking-document-bakn-lkpp','modules.bakn_lkpp.tracking');
    Route::view('/all-bakn-lkpp','modules.bakn_lkpp.all')->role('administrator');

    // Route Revisi
    Route::get('revisi-bakn-lkpp/{baknLKPP}','RevisiBaknLkppController@create')->name('revisi.bakn.lkpp');
    Route::get('preview-revisi-bakn-lkpp/{revisiBaknLkpp}','RevisiBaknLkppController@show')->name('preview.revisi.bakn.lkpp');
    Route::put('/store/{baknLKPP}','RevisiBaknLkppController@store')->name('lkpp.bakn.revisi');


$router->group([
    'prefix' => '/lkpp/bakn/',
], function () use ($router) {
    
    // data spph for create edit
    Route::get('/spph/{id}','BaknLKPPController@getDataWhereDoesntHaveBakn');

    // data spph for create create
    Route::get('/spph/','BaknLKPPController@getDataWhereDoesntHaveBakn');
    Route::get('/io/','BaknLKPPController@getDataIo');

    // Route ServerSide
    Route::get('/all/','BaknLKPPController@getAllData');
    Route::get('/draft/','BaknLKPPController@getDraftData');
    Route::get('/list/','BaknLKPPController@getListData');
    Route::get('/done/','BaknLKPPController@getDoneData');
    Route::get('/status/','BaknLKPPController@getStatusTransaksi');
    Route::get('/tracking/','BaknLKPPController@getTrackingDocument');
    Route::get('/inprogress/','BaknLKPPController@getInprogressData');


});