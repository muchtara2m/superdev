<?php

$router->group([
    'middleware' => 'auth',
], function () use ($router) {

    // ----------------- Route SPPH LKPP--------------------------
    Route::get('create-spph-lkpp','SpphLKPPController@create')->name('lkpp.spph.create');
    Route::post('/store-spph-lkpp','SpphLKPPController@store')->name('lkpp.spph.store');
    Route::put('/update-spph-lkpp/{spphLKPP}','SpphLKPPController@update')->name('lkpp.spph.update');
    Route::get('/preview-spph-lkpp/{spphLKPP}','SpphLKPPController@preview')->name('lkpp.spph.preview');
    Route::get('/edit-spph-lkpp/{spphLKPP}','SpphLKPPController@edit')->name('lkpp.spph.edit');
    Route::get('/delete-spph-lkpp/{id}','SpphLKPPController@destroy')->name('lkpp.spph.delete');
    Route::post('/file-spph-lkpp/{spphLKPP}','SpphLKPPController@file')->name('lkpp.spph.upload-file');
    Route::post('/lampiran-spph-lkpp/{spphLKPP}','SpphLKPPController@lampiran')->name('lkpp.spph.upload-file');
    Route::get('/export-spph-lkpp','SpphLKPPController@exportSpph')->name('lkpp.spph.export');

    // Route View
    Route::view('/draft-spph-lkpp','modules.spph_lkpp.draft');
    Route::view('/list-spph-lkpp','modules.spph_lkpp.list');
    Route::view('/done-spph-lkpp','modules.spph_lkpp.done');
    Route::view('/all-spph-lkpp','modules.spph_lkpp.all')->role('administrator');

});
$router->group([
    'prefix' => '/lkpp/spph/',
], function () use ($router) {

    // Route ServerSide
    Route::get('/all/','SpphLKPPController@getAllData');
    Route::get('/draft/','SpphLKPPController@draftData');
    Route::get('/list/','SpphLKPPController@getListData');
    Route::get('/done/','SpphLKPPController@getDoneData');
    Route::get('/not-draft/','SpphLKPPController@getNotDraft');


});