<?php


  // ----------------- Route Kontrak LKPP--------------------------
//   Route::get('jenis-pasal','KontrakController@jenisPasal');
//   Route::get('list-kontrak-lkpp','KontrakController@list_create');
//   Route::get('create-kontrak-lkpp/{id}','KontrakController@create');
//   Route::post('store-kontrak-lkpp','KontrakController@store');
//   Route::get('preview-kontrak-lkpp/{id}','KontrakController@preview');
//   Route::get('draft-kontrak-lkpp','KontrakController@draft');
//   Route::get('done-kontrak-lkpp','KontrakController@done');
//   Route::get('status-transaksi-kontrak-lkpp','KontrakController@status_transaksi');
//   Route::get('inprogress-kontrak-lkpp','KontrakController@inprogress');
//   Route::get('tracking-kontrak-lkpp','KontrakController@tracking');
//   Route::get('edit-kontrak-lkpp/{id}','KontrakController@edit');
//   Route::post('update-kontrak-lkpp/{id}','KontrakController@update');
//   Route::post('store-kontrak-lkpp','KontrakController@store');
//   Route::get('upload-file-kontrak-lkpp','KontrakController@upload_file');
//   Route::post('file-kontrak-lkpp/{id}','KontrakController@file');
//   Route::get('delete-kontrak-lkpp/{id}','KontrakController@destroy');
//   Route::get('preview-status-kontrak-lkpp/{id}','KontrakController@preview_status');
//   Route::post('chat-approval-kontrak-lkpp','KontrakController@chatApproval');
//   Route::get('print-preview-kontrak-lkpp/{id}','KontrakController@print_preview');
//   Route::post('word-kontrak-lkpp/{id}','KontrakController@word');


    // Route Testing Data
    Route::get('kontrak-dump','KontrakLkppController@dump');

    // Route View
    Route::view('/create-kontrak-lkpp','modules.kontrak_lkpp.create');
    Route::view('/draft-kontrak-lkpp','modules.kontrak_lkpp.draft');
    Route::view('/list-kontrak-lkpp','modules.kontrak_lkpp.list');
    Route::view('/done-kontrak-lkpp','modules.kontrak_lkpp.done');
    Route::view('/inprogress-kontrak-lkpp','modules.kontrak_lkpp.inprogress');
    Route::view('/status-transaksi-kontrak-lkpp','modules.kontrak_lkpp.status-transaksi');
    Route::view('/tracking-kontrak-lkpp','modules.kontrak_lkpp.tracking');
    Route::view('/all-kontrak-lkpp','modules.kontrak_lkpp.all')->role('administrator');

     // Route Show or Preview
    Route::get('/preview-status-kontrak-lkpp/{kontrak}','KontrakLkppController@previewStatus');
    Route::get('/edit-kontrak-lkpp/{kontrak}', function(App\Models\Kontrak $kontrak){
        return view('modules.kontrak_lkpp.edit', compact('kontrak'));
    });
    Route::get('/preview-kontrak-lkpp/{kontrak}', function(App\Models\Kontrak $kontrak){
        return view('modules.kontrak_lkpp.preview', compact('kontrak'));
    });


    // Route View Revisi Kontrak
    Route::get('revisi-kontrak-lkpp/{kontrak}',function(App\Models\Kontrak $kontrak){
        if($kontrak->approval != "CLOSED" && $kontrak->status != 'done_kontrak'){
            return redirect()->back()->with('success', 'Sorry Your document still circulating');
        }else{
            return view('modules.kontrak_lkpp.revisi.create',compact('kontrak'));
        }
    })->name('revisi.kontrak.lkpp');
    Route::get('preview-revisi-kontrak-lkpp/{revisiKontrakLkpp}', 'RevisiKontrakLkppController@show');


$router->group([
    'prefix' => '/lkpp/kontrak/',
], function () use ($router) {

    // Route ServerSide

    // data spph for edit form
    Route::get('/spph-bakn/{id}','KontrakLkppController@getDataSpphWereBaknClosed');
    // data spph for create form
    Route::get('/spph-bakn/','KontrakLkppController@getDataSpphWereBaknClosed');
    // get data last signature
    Route::get('/name-approval','KontrakLkppController@getNameForLastApproval');
    

    // Route CRUD
    Route::post('/store','KontrakLkppController@store')->name('lkpp-kontrak-store');
    Route::put('/update/{kontrak}','KontrakLkppController@update')->name('lkpp.kontrak.update');
    Route::post('/lampiran/{kontrak}','KontrakLkppController@lampiran')->name('lkpp.kontrak.lampiran');
    Route::post('/file/{kontrak}','KontrakLkppController@file')->name('lkpp.kontrak.file');
    Route::get('/delete/{kontrak}','KontrakLkppController@destroy')->name('lkpp.kontrak.delete');
    Route::post('/chat-approve/{kontrak}','KontrakLkppController@chatApproval')->name('lkpp.kontrak.approve');
    Route::get('hold/{kontrak}','KontrakLkppController@holdOrRelease')->name('lkpp.kontrak.hold');


    //Route CRUD Revisi
    Route::post('store/{kontrak}','RevisiKontrakLkppController@store')->name('lkpp.kontrak.revisi');

    // Route Server Side Data
    Route::get('/all/','KontrakLkppController@getDataAll');
    Route::get('/draft/','KontrakLkppController@getDataDraft');
    Route::get('/list/','KontrakLkppController@getDataList');
    Route::get('/done/','KontrakLkppController@getDataDone');
    Route::get('/status/','KontrakLkppController@getDataStatus');
    Route::get('/tracking/','KontrakLkppController@getDataTracking');
    Route::get('/inprogress/','KontrakLkppController@getDataInprogress');


});