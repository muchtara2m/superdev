<?php

$router->group([
    'middleware' => 'auth',
], function () use ($router) {

   // ----------------------Route BAK LKPP --------------------------------
   Route::post('store-bak-lkpp','BakLkppController@store');
   Route::put('update-bak-lkpp/{bakLkpp}','BakLkppController@update');
   Route::get('delete-bak-lkpp/{id}','BakLkppController@destroy');

   
   Route::post('file-bak-lkpp/{bakLkpp}','BakLkppController@file');
   Route::post('lampiran-bak-lkpp/{bakLkpp}','BakLkppController@lampiran');
   Route::post('chat-approval-bak-lkpp/{bakLkpp}','BakLkppController@chatApproval')->name('lkpp.bak.approve');
   Route::get('preview-status-bak-lkpp/{bakLkpp}','BakLkppController@preview_status');
   Route::get('preview-bak-lkpp/{bakLkpp}','BakLkppController@preview');


    // Route View
    // Route::get('/create-bak-lkpp','BakLkppController@create');
    Route::get('edit-bak-lkpp/{bakLkpp}','BakLkppController@edit');
    Route::get('hold-bak-lkpp/{bakLkpp}','BakLkppController@holdOrRelease');

    Route::view('/create-bak-lkpp','modules.bak_lkpp.create');
    Route::view('draft-bak-lkpp','modules.bak_lkpp.draft');
    Route::view('/list-bak-lkpp','modules.bak_lkpp.list');
    Route::view('/done-bak-lkpp','modules.bak_lkpp.done');
    Route::view('/inprogress-bak-lkpp','modules.bak_lkpp.inprogress');
    Route::view('/status-transaksi-bak-lkpp','modules.bak_lkpp.status_transaksi');
    Route::view('/tracking-bak-lkpp','modules.bak_lkpp.tracking');
    Route::view('/all-bak-lkpp','modules.bak_lkpp.all')->role('administrator');

    // Route Revisi
    Route::get('revisi-bak-lkpp/{bakLkpp}','RevisiBakLkppController@create')->name('revisi.bak.lkpp');
    Route::get('preview-revisi-bak-lkpp/{revisiBakLkpp}','RevisiBakLkppController@show')->name('preview.revisi.bak.lkpp');
    Route::put('/store-bak/{bakLkpp}','RevisiBakLkppController@store')->name('lkpp.bak.revisi');

});
$router->group([
    'prefix' => '/lkpp/bak/',
], function () use ($router) {
    // Route ServerSide
    Route::get('/all/','BakLkppController@getAllData');
    Route::get('/draft/','BakLkppController@getDraftData');
    Route::get('/list/','BakLkppController@getListData');
    Route::get('/done/','BakLkppController@getDoneData');
    Route::get('/status/','BakLkppController@getStatusTransaksi');
    Route::get('/tracking/','BakLkppController@getTrackingDocument');
    Route::get('/inprogress/','BakLkppController@getInprogressData');


});

