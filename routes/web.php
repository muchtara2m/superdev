<?php
Route::get('/', 'HomeController@index');
Auth::routes();
Route::get('/register', function(){
    return abort(404);
});
include __DIR__.'/web/index.php';
// route testing without login 
Route::get('data-pbs', 'TestingController@dataPbs');
Route::get('data-pipeline', 'TestingController@dataPipeline');
Route::get('testing',function(){
    return view('modules.testing.test');
});

Route::get('get-io','TestingController@importIo');
Route::get('update-io','TestingController@updateIo');