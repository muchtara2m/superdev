
// format currency harga
function formatAngka(objek, separator) {
    a = objek.value;
    b = a.replace(/[^\d]/g, "");
    c = "";
    panjang = b.length;
    j = 0;
    for (i = panjang; i > 0; i--) {
        j = j + 1;
        if (((j % 3) == 1) && (j != 1)) {
            c = b.substr(i - 1, 1) + separator + c;
        } else {
            c = b.substr(i - 1, 1) + c;
            formatAngka
        }
    }
    objek.value = c;
}

function clearDot(number)
{
    output = number.replace(".", "");
    return output;
}
// script datepicker
$(function () {
    $(".datejos").on("change", function () {
        this.setAttribute(
        "data-date",
        moment(this.value, "YYYY-MM-DD")
        .format(this.getAttribute("data-date-format"))
        )
    }).trigger("change")
});
$('.datejos').datepicker({
    autoclose: true,
    orientation: "bottom"
});
// harga terbilang
$(document).ready(function () {
    function addCommas(nStr) {
        nStr += '';
        x = nStr.split(',');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        if (x.length == 1) {
            x[1] = "00";
        }
        return x1 + "," + x[1];
    };
    $('#harganum').change(function () {
        var hrg = $('#harganum').val();
        hapusdot = hrg.split('.').join("");
        var blg = terbilangs(hapusdot) + "Rupiah";
        let hasil_format_currency = `Total harga yang sesuai dengan ruang lingkup pekerjaan tersebut diatas adalah sebesar <strong>Rp. ${addCommas($('#harganum').val())},- (${blg})</strong> sudah termasuk PPN 10% (sepuluh persen), dengan rincian sebagai berikut:`;
        let hasil_hrg_bak = `Rp.${addCommas($('#harganum').val())},- (${blg}) sudah termasuk PPN 10% (sepuluh persen), `;
        $('#harga_bak').html(hasil_format_currency);
        $('#hrg_bak').html(hasil_hrg_bak);
        
    });
    
    function terbilangs(n) {
        k = ['', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan'];
        a = [1000000000000000, 1000000000000, 1000000000, 1000000, 1000, 100, 10, 1];
        s = ['Kuadriliun', 'Trilyun', 'Milyar', 'Juta', 'Ribu', 'ratus', 'Puluh', ''];
        var i = 0,
        x = '';
        var j = 0,
        y = '';
        var angka = n;
        //alert(angka);
        var inp_pric = String(angka).split(",");
        while (inp_pric[0] > 0) {
            b = a[i], c = Math.floor(inp_pric[0] / b), inp_pric[0] -= b * c;
            x += (c >= 10 ? terbilangs(c) + "" + s[i] + " " : ((c > 0 && c < 10) ? k[c] + " " + s[i] + " " : ""));
            i++;
            
        }
        var depan = x.replace(new RegExp(/Satu Puluh (\w+)/gi), '$1 belas').replace(new RegExp(/satu (ribu|ratus|puluh|belas)/gi), 'Se\$1');
        
        while (inp_pric[1] > 0) {
            z = a[j], d = Math.floor(inp_pric[1] / z), inp_pric[1] -= z * d;
            y += k[d] + " ";
            j++;
        }
        var belakang = y.replace(new RegExp(/satu Puluh (\w+)/gi), '$1 Belas').replace(new RegExp(/satu (Ribu|Ratus|Puluh|Belas)/gi), 'Se\$1');
        
        if (belakang != '') {
            return depan + "koma" + belakang;
        } else {
            return depan;
        }
        
    };
});
// function table spph
$(document).ready( function () {
    $('#spph').DataTable();
} )
var tablespph = document.getElementById('spph');
for(var i = 1; i < tablespph.rows.length; i++)
{
    tablespph.rows[i].onclick = function()
    {
        document.getElementById("spphid").value = this.cells[0].innerHTML;
        document.getElementById("nmrspph").value = this.cells[1].innerHTML;
        // $('#nmrspph').val(this.cells[1].innerHTML);
        
        
    };
};
// set default no_bak dan tgl_bak
let get_tgl_bak = document.getElementById('tgl_bak');
function useValue() {
    let NameValue = get_tgl_bak.value.split('-');
    // use it
    document.getElementById('no_bak').value = "/HK.810/ECOM/PIN.00.00/"+NameValue[0];        
}
get_tgl_bak.onchange = useValue;
get_tgl_bak.onblur = useValue;

// change nomor bak
$('#no_bak').change(function() {
    var nomor = $('#no_bak').val();
    let tgl_baknya = $('#tgl_bak').val();
    let split_tgl = tgl_baknya.split('-');
    const date_bak = new Date(tgl_baknya);
    const month_bak = date_bak.toLocaleString('id-ID', {month:'long'});
    $('#tanggal_bak').html(`TANGGAL ${split_tgl[2]} ${month_bak.toUpperCase()} ${split_tgl[0]}`);
    $('#nomor_bak').html(`NOMOR : ${nomor}`);
});