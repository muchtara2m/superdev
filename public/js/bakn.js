function formatAngka(objek, separator) {
    a = objek.value;
    b = a.replace(/[^\d]/g, "");
    c = "";
    panjang = b.length;
    j = 0;
    for (i = panjang; i > 0; i--) {
        j = j + 1;
        if (((j % 3) == 1) && (j != 1)) {
            c = b.substr(i - 1, 1) + separator + c;
        } else {
            c = b.substr(i - 1, 1) + c;
            formatAngka
        }
    }
    objek.value = c;
}

function clearDot(number) {
    output = number.replace(".", "");
    return output;
}
// script checkbox undangan
var checkboxes = document.getElementsByName('invittype[]');
var vals = "";
for (var i = 0, n = checkboxes.length; i < n; i++) {
    if (checkboxes[i].checked) {
        vals += "," + checkboxes[i].value;
    }
}
if (vals) vals = vals.substring(1);

// script datepicker
$(function () {
    // $('#reservationtime').daterangepicker();
    $('#reservationtime').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD'
        }
    })

    $(".datejos").on("change", function () {
        this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
            .format(this.getAttribute("data-date-format"))
        )
    }).trigger("change")
});
$('.datejos').datepicker({
    autoclose: true,
    orientation: "bottom"
});
// script tagify
$('.tagify').tagify();

// harga terbilang
$(document).ready(function () {
    function addCommas(nStr) {
        nStr += '';
        x = nStr.split(',');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        if (x.length == 1) {
            x[1] = "00";
        }
        return x1 + "," + x[1];
    };
    $('#harganum').change(function () {
        borongvariabel = "Total harga pekerjaan adalah sebesar Rp ";
        var hrg = $('#harganum').val();
        hapusdot = hrg.split('.').join("");
        console.log(hapusdot);
        var blg = terbilangs(hapusdot) + "Rupiah";
        var tes = borongvariabel + addCommas($('#harganum').val()) + " ( " + blg + " )" + " sudah termasuk PPN 10% dengan rincian sebagaimana terlampir.";
        $('#hargatext').val(tes);
        $('#nilai').froalaEditor('html.set', tes);
        $('#test_harga').val(tes);
        $('.trblng_hrg').val(tes);
        $('.dt_hrg').val(hapusdot);
    });

    function terbilangs(n) {
        k = ['', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan'];
        a = [1000000000000000, 1000000000000, 1000000000, 1000000, 1000, 100, 10, 1];
        s = ['Kuadriliun', 'Trilyun', 'Milyar', 'Juta', 'Ribu', 'Ratus', 'Puluh', ''];
        var i = 0,
            x = '';
        var j = 0,
            y = '';
        var angka = n;
        //alert(angka);
        var inp_pric = String(angka).split(",");
        while (inp_pric[0] > 0) {
            b = a[i], c = Math.floor(inp_pric[0] / b), inp_pric[0] -= b * c;
            x += (c >= 10 ? terbilangs(c) + "" + s[i] + " " : ((c > 0 && c < 10) ? k[c] + " " + s[i] + " " : ""));
            i++;

        }
        var depan = x.replace(new RegExp(/Satu Puluh (\w+)/gi), '$1 Belas').replace(new RegExp(/satu (ribu|ratus|puluh|belas)/gi), 'Se\$1');

        while (inp_pric[1] > 0) {
            z = a[j], d = Math.floor(inp_pric[1] / z), inp_pric[1] -= z * d;
            y += k[d] + " ";
            j++;
        }
        var belakang = y.replace(new RegExp(/satu Puluh (\w+)/gi), '$1 belas').replace(new RegExp(/satu (ribu|ratus|puluh|belas)/gi), 'Se\$1');

        if (belakang != '') {
            return depan + "koma" + belakang;
        } else {
            return depan;
        }

    };
});
// datatable

$(document).ready(function () {
    $('#io').DataTable();
    $('#spph').DataTable();
})
var table = document.getElementById('io');
var tablespph = document.getElementById('spph');

for (var i = 1; i < table.rows.length; i++) {
    table.rows[i].onclick = function () {
        //  rIndex = this.rowIndex;
        document.getElementById("io_id").value = this.cells[1].innerHTML;
        document.getElementById("noio").value = this.cells[1].innerHTML;
        document.getElementById("desio").value = this.cells[2].innerHTML;
    };
};
for (var i = 1; i < tablespph.rows.length; i++) {
    tablespph.rows[i].onclick = function () {
        //  rIndex = this.rowIndex;
        document.getElementById("spphid").value = this.cells[0].innerHTML;
        // document.getElementById("nmrspph").value = this.cells[1].innerHTML;
        $("#nmrspph").attr("value", this.cells[1].innerHTML);

    };
};

// check cara bayar
var interest = {
    "styles": {} // use an array to store the styles
};

function add() {
    var sbox = Array.from($(`.carabayar`));
    interest.styles = []; // empty the array before rebuilding it
    sbox.forEach(function (v) {
        if (v.checked) {
            x = v.dataset.id;
            var xData = $.caraBayar(x);
            interest.styles.push(xData);
        }
    });
    $('#tatacara').froalaEditor('html.set', interest.styles.join(" "));
    // console output for demo
    // console.log( interest.styles ); // array
    // console.log( interest.styles.join() ); // CSV
    // console.log( interest.styles.join( " " ) ); // for HTML class attributes
}
