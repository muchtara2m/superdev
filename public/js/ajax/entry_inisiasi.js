$(document).ready(function(){
    $( '#pbsTable' ).dataTable( {
        destroy: true,
        processing: true,
        serverSide: true,
        async : true,
        ajax: {
            url:url,
            data:{bln:8, thn:2019}
        },
        columns: [
            {
                "class":"details-control",
                "orderable":false,
                "data":null,
                "defaultContent": ""
            },
            { 
                data: 'DT_RowIndex', 
                name: 'DT_RowIndex' , 
                orderable: false, 
                searchable: false
            },
            { 
                data: 'dataio.no_io',
                'defaultContent': ""
            },
            { 
                data: 'dataio.no_io',
                'defaultContent': ""
            },
            { 
                data: 'pembuat.name',
                'defaultContent': ""
            },     
            { 
                data: 'pembuat.unitnya.nama',
                'defaultContent': ""
            },            
            { 
                data: 'customer.nama_customer',
                'defaultContent': ""
            },
            { 
                data: 'uraian',
                'defaultContent': ""
            },
            { 
                data: 'nilai_project', 
                render: $.fn.dataTable.render.number( '.', '.', 0, 'Rp ' )
            },
            { 
                data: null,
                className: "center",
                defaultContent: "",
                orderable:false,
            },
            { 
                data: null,
                className: "center",
                defaultContent: "",
                orderable:false,
            },
            { 
                data: null,
                className: "center",
                defaultContent: "",
                orderable:false,
            },
            { 
                data: null,
                className: "center",
                defaultContent: "",
                orderable:false,
            },
            { 
                data: null,
                className: "center",
                defaultContent: "",
                orderable:false,
            },
            { 
                data: null,
                className: "center",
                defaultContent: "",
                orderable:false,
            },
            
        ],
        
    });
});