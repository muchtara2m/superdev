function detailDataPbs(thn='', unit=''){

    $('#detailDataPbs').dataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        async: true,
        ajax: {
            url:url_detail_data_pbs,
            data: {
                    unit:unit,
                    thn:thn,
                }
        },
        columns:
            [
                {
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },{
                    data: 'no_io',
                    defaultContent : "",
                },{
                    data: 'deskripsi_vendor',
                    defaultContent:"",
                },{
                    data: 'deskripsi_project',
                    defaultContent:"",
                },{
                    data: 'nomor',
                    defaultContent:"",
                },
                {
                    data: 'entry_date',
                    defaultContent: "",
                },{
                    data: 'start_layanan',
                    defaultContent:"",
                },{
                    data: 'end_layanan',
                    defaultContent:"",
                },{
                    data:'revenue',
                    render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')
                },{
                    data: 'status_spk_pks',
                   render: function(data) { 
                        if(data == 0) {
                            return 'Menunggu Approval';
                        } else if(data == 1 ){
                            return 'Approved';
                        }else {
                            return 'Return';
                        }
                    },
                }
        ],
    })
}