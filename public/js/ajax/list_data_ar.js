function dataAr(thn,bln) {
    $('#ar').dataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        async: true,
        ajax: {
            url: url_data,
            data: {
                thn: thn,
                bln: bln,
            }
        },
        columns: [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                orderable: false,
                searchable: false
            }, {
                data: 'cuxtomer_id',
                defaultContent: "",
            }, {
                data: 'group_template_metra',
                defaultContent: "",
            }, {
                data: 'akun',
                defaultContent: "",
            }, {
                data : 'so',
                defaultContent: "",
            },{
                data :'document_no',
                defaultContent: '',
            },{
                data:'text',
                defaultContent:'',
            },{
                data : 'project',
                defaultContent: '',
            },{
                data : 'header1',
                defaultContent: '',
            },{
                data:'posting_date',
                defaultContent:'',
            },{
                data:'amount',
                defaultContent: '',
                render: $.fn.dataTable.render.number('.', '.', 0, 'Rp '),
            },{
                data: "pic",
                defaultContent: "",
                render(data){
                    return data.toUpperCase();
                }

            }, {
                data: "ubis",
                defaultContent: "",
                render(data){
                    return data.toUpperCase();
                }
            }, {
                data: "dokumen",
                defaultContent: "",
                render(data){
                    return (
                        `<a href="" data-toggle="modal" data-target="#modal-update-status" data-nilai="${data.nil}" data-dok="${data.dok}" id="update_status" data-id="${data.id}">${data.dok.toUpperCase()}</a>`
                    )
                }
            }, 
        ]
    });
}