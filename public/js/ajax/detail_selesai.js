function detailDataSelesai(thn='', unit=''){

    var myAjax = {
        destroy: true,
        processing: true,
        serverSide: true,
        async: true,
        ajax: {
                url:url_detail_data_selesai,
                data: {
                        unit:unit,
                        thn:thn,
                },
        },
    };

    var config1 = {
        columnDefs: [
          { targets: 0, data: "DT_RowIndex" },
          { targets: 1, data: "no_io" },
          { targets: 2, data: "after_to" },
          { targets: 3, data: "deskripsi_customer" },
          { targets: 4, data: "deskripsi_project" },
          { targets: 5, data: "nomor" },
          { targets: 6, data: "tanggal" },
          { targets: 7, data: "start_layanan" },
          { targets: 8, 
            data: "end_layanan" 
        },
          { targets: 9, data: "revenue",render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')},
          { targets: 10, 
            data: 'status_spk_pks',
            render: function(data) { 
                 if(data == 0) {
                     return 'Menunggu Approval';
                 } else if(data == 1 ){
                     return 'Approved';
                 }else {
                     return 'Return';
                 }
             },
        },
        ]
      };
    
      var object1 = jQuery.extend({}, myAjax, config1);
      var table1 = jQuery('#detailDataSelesai1').DataTable(object1);  

      var config2 = {
        columnDefs: [
          { targets: 0, data: "DT_RowIndex" },
          { targets: 1, data: "no_io" },
          { targets: 2, data: "after_to" },
          { targets: 3, data: "pbs_cogs" , render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')},
          { targets: 4, data: "pbs_revenue", render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')},
          { targets: 5, defaultContent:"NULL"},
          { targets: 6, data: "tanggal" },
          { targets: 7, data: "deskripsi_vendor" },
          { targets: 8, defaultContent:"JUDUL" },
          { targets: 9, data: "revenue", render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')},
          { targets: 10, defaultContent:"STATUS"},
          { targets: 11, defaultContent:"KETERANGAN"},
          { targets: 12, defaultContent:"KETERANGAN"},
          { targets: 13, defaultContent:"KETERANGAN"},
          { targets: 14, defaultContent:"KETERANGAN"},

        ]
      };
    
      var object2 = jQuery.extend({}, myAjax, config2);
      var table2 = jQuery('#detailDataSelesai2').DataTable(object2);  
  
      var config3 = {
        columnDefs: [
          { targets: 0, data:"DT_RowIndex"},
          { targets: 1, data:"no_io"},
          { targets: 2, data:"after_to"},
          { targets: 3, defaultContent:"NULL"},
          { targets: 4, defaultContent:"NULL"},
          { targets: 5, defaultContent:"NULL"},
          { targets: 6, defaultContent:"NULL"},
          { targets: 7, defaultContent:"NULL"},
          { targets: 8, defaultContent:"NULL" },
          { targets: 9, defaultContent:"NULL"},
          { targets: 10, defaultContent:"NULL"},
          { targets: 11, defaultContent:"NULL"},

        ]
      };
    
      var object3 = jQuery.extend({}, myAjax, config3);
      var table3 = jQuery('#detailDataSelesai3').DataTable(object3);  

      
    

    
}