function dataSpkPks(thn = '') {
    $('#jumlah_spk_pks').dataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        async: true,

        ajax: {
            url: url_data_performansi_spk_pks,
            data: {
                thn: thn
            }
        },
        columns: [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'UBIS',
                'defaultContent': ""
            },
            {
                data: 'detailPbs',
                'defaultContent': "",
                render(data){
                    return (
                        `<a href="" data-toggle="modal" id="data-pbs" data-target="#modal-detail-pbs" data-ubis="${data.idUbis}" data-status="pbs" >${data.nilaiPbs}</a>`
                    )
                }
            },
            {
                data: 'detailPlan',
                'defaultContent': "",
                render(data){
                    return (
                        `<a href="" data-toggle="modal" id="data-plan" data-target="#modal-detail-plan" data-ubis="${data.idUbis}" data-status="plan">${data.nilaiPlan}</a>`
                    )
                }
            },
            {
                data: 'detailSelesai',
                'defaultContent': "",
                render(data){
                    return (
                        `<a href="" data-toggle="modal" id="data-selesai" data-target="#modal-detail-selesai" data-ubis="${data.idUbis}" data-status="selesai" >${data.nilaiSelesai}</a>`
                    )
                }
            },
            {
                data: 'Inprogress',
                'defaultContent': ""
            },
            {
                data: 'Batal',
                'defaultContent': ""
            },
            {
                data: 'Pending',
                'defaultContent': ""
            },
            {
                data: 'COGS',
                render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')
            },
            {
                data: 'NilaiSPK',
                render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')
            },
            {
                data: 'referensiRp',
                render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')
            },
            {
                data: 'refPersen',
                'defaultContent': "",
                render(data) {
                    return (
                        `${Math.round(data*10)/10}%`
                    );
                },
            },
            {
                data: 'KHS',
                'defaultContent': ""
            },


        ]

    });
    
}