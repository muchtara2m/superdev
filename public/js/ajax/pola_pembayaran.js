function polaPembayaran(thn = '') {

    var myAjax = {
        destroy: true,
        processing: true,
        serverSide: true,
        async: true,
        ajax: {
            url: url_pola_pembayaran,
            data: {
                thn: thn
            }
        },
    };

    var config1 = {
        columnDefs: [
          { targets: 0, data: "DT_RowIndex" },
          { targets: 1, data: "UBIS" },
          { targets: 2, data: "cod" },
          { targets: 3, data: "otc" },
          { targets: 4, data: "recurring" },
          { targets: 5, data: "progress" },
          { targets: 6, data: "jumlah" },
        ]
      };
    
      var object1 = jQuery.extend({}, myAjax, config1);
      var table1 = jQuery('#pola_pembayaran').DataTable(object1);  

      var config2 = {
        columnDefs: [
          { targets: 0, data: "DT_RowIndex" },
          { targets: 1, data: "UBIS" },
          { targets: 2, data: "kurangSeratus" },
          { targets: 3, data: "antaraLimaRatus" },
          { targets: 4, data: "antaraDuaM" },
          { targets: 5, data: "DuaMLebih" },
          { targets: 6, data: "total" },

        ]
      };

      var object2 = jQuery.extend({}, myAjax, config2);
      var table2 = jQuery('#klarifikasi_nilai').DataTable(object2);  

}
