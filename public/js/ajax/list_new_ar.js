function dataAr() {
    $('#ar').dataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        async: true,

        ajax: {
            url: url_data,
        },
        columns: [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },{
                data : 'header1',
                name : 'header1',
                defaultContent : "",
            },{
                data : 'project',
                defaultContent : "",
            },{
                data : 'customer_id',
                defaultContent : "",

            },{
                data: "ubis",
                defaultContent : "",

            },{
                data: "pic",
                defaultContent : "",
            },{
                data: "revenue",
                render: $.fn.dataTable.render.number('.', '.', 0, 'Rp '),
                defaultContent : "",

            },{
                data: "revenue",
                render: $.fn.dataTable.render.number('.', '.', 0, 'Rp '),
                defaultContent : "",

            },
           
        ]

    });
    
}