
function klarifikasiNilai(thn = '') {
    $('#klarifikasi_nilai').dataTable({
        // destroy: true,
        processing: true,
        serverSide: true,
        // async: true,

        ajax: {
            url: url_klarifikasi_nilai,
            data: {
                thn: thn
            }
        },
        columns: [
            {
                data: 'DT_RowIndex',
                name: 'DT_RowIndex',
                orderable: false,
                searchable: false
            },
            {
                data: 'UBIS',
                'defaultContent': ""
            },
            {
                data: 'kurangSeratus',
                defaultContent: "",
            },
            {
                data:'antaraLimaRatus',
                defaultContent:"",
            },
            {
                data:'antaraDuaM',
                defaultContent:"",
            },
            {
                data:'DuaMLebih',
                defaultContent:"",
            },
            {
                data:'total',
                defaultContent:""
            },
        ]
    });
}