function detailDataPlan(thn='', unit=''){

    var myAjax = {
        destroy: true,
        processing: true,
        serverSide: true,
        async: true,
        ajax: {
                url:url_detail_data_plan,
                data: {
                        unit:unit,
                        thn:thn,
                },
        },
    };
      
      var config1 = {
        columnDefs: [
          { targets: 0, data: "DT_RowIndex" },
          { targets: 1, data: "no_io" },
          { targets: 2, data: "after_to" },
          { targets: 3, data: "deskripsi_customer" },
          { targets: 4, data: "deskripsi_project" },
          { targets: 5, data: "nomor" },
          { targets: 6, data: "tanggal" },
          { targets: 7, data: "start_layanan" },
          { targets: 8, 
            data: "end_layanan" 
        },
          { targets: 9, data: "revenue",render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')},
          { targets: 10, 
            data: 'status_spk_pks',
            render: function(data) { 
                 if(data == 0) {
                     return 'Menunggu Approval';
                 } else if(data == 1 ){
                     return 'Approved';
                 }else {
                     return 'Return';
                 }
             },
        },
        ]
      };
    
      var object1 = jQuery.extend({}, myAjax, config1);
      var table1 = jQuery('#detailDataPlan1').DataTable(object1);  
    
      var config2 = {
        columnDefs: [
          { targets: 0, data: "DT_RowIndex" },
          { targets: 1, data: "no_io" },
          { targets: 2, data: "after_to" },
          { targets: 3, data: "pbs_cogs" },
          { targets: 4, data: "pbs_revenue"},
          { targets: 5, defaultContent:"NULL"},
          { targets: 6, data: "tanggal" },
          { targets: 7, data: "deskripsi_vendor" },
          { targets: 8, defaultContent:"JUDUL" },
          { targets: 9, data: "revenue", render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')},
          { targets: 10, defaultContent:"STATUS"},
          { targets: 11, defaultContent:"KETERANGAN"},

        ]
      };
    
      var object2 = jQuery.extend({}, myAjax, config2);
      var table2 = jQuery('#detailDataPlan2').DataTable(object2);  

      
    

    
}