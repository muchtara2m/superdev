
// function select 2 & date pick
$(function () {
    $(".datepick").on("change", function () {
        this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
            .format(this.getAttribute("data-date-format"))
            )
        }).trigger("change")
    });
    $('.datepick').datepicker({
        autoclose: true,
        orientation: "bottom"
    });
    // select2
    $('.select2').select2({
        placeholder: function(){
            $(this).data('placeholder');
        }
    });
    // end function select 2 & date pick
    
    //  function add input type for searching 
    $('#pbsTable tfoot .testing').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text"/>' );
    } );
    // function search each column on keyup
    $('#pbsTable').DataTable().columns().every( function () {
        var that = this;
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                .search( this.value )
                .draw();
            }
        } );
    } );
    // function child row table 
    function format ( d ) {
        return `
        <table class="display text-center">
            <thead>
                <tr>
                    <th>Aksi</th>
                    <th>Edit Status</th>
                    <th>Mapping IO</th>
                    <th>Entry Outlook</th>
                    <th>Entry Submission SDV</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td> 
                        <button type="button" class="btn bg-navy margin" data-toggle="modal" data-target="#modal-edit-data-inisiasi" style="width:150px">Edit Inisiasi</button>
                    </td>
                    <td> 
                        <button type="button" class="btn bg-navy margin" data-toggle="modal" data-target="#modal-update-status" style="width:150px">Update Status</button>
                    </td>
                    <td> 
                        <div class="input-group">
                            <input type="text" name="desio" class="btn form-control" id="desio" title="Pilih IO" data-toggle="modal" data-target="#modal-io" readonly placeholder="Pilih IO" style="width:150px">
                            <span class="input-group-btn" id="button-io">
                            </span>
                        </div>
                    </td>
                    <td> 
                        <button type="button" class="btn bg-navy margin" data-toggle="modal" data-target="#modal-tgl-closing" style="width:150px">Entry Outlook</button>
                    </td>
                    <td> 
                        <button type="button" class="btn bg-navy margin" data-toggle="modal" data-target="#modal-submission" style="width:150px">Entry Submission SDV</button>
                    </td>
                </tr>
            </tbody>
        </table>
        `;
    }
    // Array to track the ids of the details displayed rows
    var detailRows = [];
    
    $('#pbsTable tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = $('#pbsTable').DataTable().row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );
        
        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide();
            
            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            row.child( format( row.data() ) ).show();
            
            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );
    
    // On each draw, loop over the `detailRows` array and show any child rows
    $('#pbsTable').DataTable().on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.details-control').trigger( 'click' );
        } );
    } );
    // end function table 

    // IO function 
    $('#io').DataTable();
    let tableio  = document.getElementById('io');
    for(let i = 1; i < tableio.rows.length; i++)
    {
        tableio.rows[i].onclick = function()
        {
            document.getElementById("desio").value = this.cells[1].innerHTML;
            let nilaiIo = $('#desio').val();
            if(nilaiIo !== ''){
                $('#button-io').html(`
                <button type="button" class="btn btn-info btn-flat btn-submit-io" id="submit-io">Submit</button>
                `);
                // function submit io after click
                $('#button-io').on('click','.btn-submit-io',function(){
                    alert('It Works');
                })
                // end function submit io 
            }
        };
    };
    // function tgl closing 
    $('#btn-submit-closing').on('click', function(){
      let tglclosing  = $('#tglclosing').val();
      alert(tglclosing);
    })
    // function update status inisiasi
    $('#btn-status-inisiasi').on('click', function(){
        let updateInisiasi = $('#update_status_inisiasi').val();
        alert(updateInisiasi);
    });
    // function submission
    $('#submission_to').on('change', function(){
        let val =$(this).find('option:selected').data('val');
        $('#deliveryname1').val(val);
    });
    $('#btn-submit-submission').on('click',function(){
        let file = $('#file_submission').val();
        let date = $('#tgl_submission').val();
        let delivery = $('#deliveryname1').val();
        let val = $('#submission_to').val();
        alert(file+date+delivery+val);
    });