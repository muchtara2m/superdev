var dataCanvas = document.getElementById("dataChart");

// Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 13;

var data1 = {
    label: "List PBS",
    data: [0, 59, 75, 20, 20, 55, 40],
    lineTension: 0.3,
    fill: false,
    borderColor: '#00c0ef',
    backgroundColor: 'transparent',
    pointBorderColor: '#00c0ef',
    pointBackgroundColor: 'white',
    pointRadius: 4,
    pointHoverRadius: 8,
    pointHitRadius: 30,
    pointBorderWidth: 2,
    // pointStyle: 'rect'
  };

var data2 = {
    label: "List BAKN",
    data: [20, 15, 60, 60, 65, 30, 70],
    lineTension: 0.3,
    fill: false,
    borderColor: '#00a65a',
    backgroundColor: 'transparent',
    pointBorderColor: '#00a65a',
    pointBackgroundColor: 'white',
    pointRadius: 4,
    pointHoverRadius: 8,
    pointHitRadius: 30,
    pointBorderWidth: 2
  };

var data3 = {
    label: "List Kontrak",
    data: [0, 40, 25, 40, 50, 55, 40],
    lineTension: 0.3,
    fill: false,
    borderColor: '#f39c12',
    backgroundColor: 'transparent',
    pointBorderColor: '#f39c12',
    pointBackgroundColor: 'white',
    pointRadius: 4,
    pointHoverRadius: 8,
    pointHitRadius: 30,
    pointBorderWidth: 2,
    // pointStyle: 'rect'
  };

var datas = {
  labels: ["0s", "10s", "20s", "30s", "40s", "50s", "60s"],
  datasets: [data1, data2, data3]
};

var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 40,
      fontColor: 'black'
    }
  }
};

var lineChart = new Chart(dataCanvas, {
  type: 'line',
  data: datas,
  options: chartOptions
});