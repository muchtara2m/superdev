var dataCanvas = document.getElementById("dataChart");

// Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 13;

var data1 = {
    label: "DRAFT KONTRAK",
    data: [0, 59, 75, 20, 20, 55, 40],
    lineTension: 0.3,
    fill: false,
    borderColor: '#00c0ef',
    backgroundColor: 'transparent',
    pointBorderColor: '#00c0ef',
    pointBackgroundColor: 'white',
    pointRadius: 4,
    pointHoverRadius: 8,
    pointHitRadius: 30,
    pointBorderWidth: 2,
    // pointStyle: 'rect'
  };

var data2 = {
    label: "DONE KONTRAK",
    data: [20, 15, 60, 60, 65, 30, 70],
    lineTension: 0.3,
    fill: false,
    borderColor: '#00a65a',
    backgroundColor: 'transparent',
    pointBorderColor: '#00a65a',
    pointBackgroundColor: 'white',
    pointRadius: 4,
    pointHoverRadius: 8,
    pointHitRadius: 30,
    pointBorderWidth: 2
  };

var datas = {
  labels: ["0s", "10s", "20s", "30s", "40s", "50s", "60s"],
  datasets: [data1, data2]
};

var chartOptions = {
  legend: {
    display: true,
    position: 'top',
    labels: {
      boxWidth: 40,
      fontColor: 'black'
    }
  }
};

var lineChart = new Chart(dataCanvas, {
  type: 'line',
  data: datas,
  options: chartOptions
});