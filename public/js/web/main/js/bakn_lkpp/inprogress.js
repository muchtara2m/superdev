let dateLine = ("2020-07-01");

// Call Function
$(document).ready(function () {
    getInprogress(getMonth + 1, d.getFullYear());
});

// Declare Variable
let url_list = $('#inprogress').data('url');
let bulan = $('#bulan');
let tahun = $('#tahun');
let d = new Date();
var getMonth = d.getMonth();
let minTahun = (d.getFullYear() - 5);


// Declare Array Bulan
const months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus',
    'September', 'Oktober', 'November', 'Desember','All',
];

// Insert Value Option Dropdown Bulan
months.forEach(function (i, index) {
    if (getMonth == index) {
        bulan.append("<option value=" + (index + 1) + " selected>" + i + "</option>")
    } else {
        bulan.append("<option value=" + (index + 1) + ">" + i + "</option>")
    }
})
// Insert Value Option Dropdown Tahun
for (let i = d.getFullYear(); i > minTahun; i--) {
    tahun.append('<option value="' + i + '">' + i + '</option>')
}

// OnClick Month Function
$('#bulan').change(function () {
    let m = $('#bulan').val();
    let y = $('#tahun').val();
    if (m != '' && y != '') {
        getInprogress(m, y);
    } else {
        alert('Both Date is required');
    }
});

// OnClick Year Function
$('#tahun').change(function () {
    let m = $('#bulan').val();
    let y = $('#tahun').val();
    if (m != '' && y != '') {
        getInprogress(m, y);
    } else {
        alert('Both Date is required');
    }
});

// function add class button pagination
// $.fn.dataTable.ext.classes.sPageButton = 'btn btn-primary';
// Function ServerSide DataTable
function getInprogress(m, y) {
    $('#inprogress').dataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        async: true,
        scrollY: "50vh",
        scrollX: true,
        ajax: {
            url: url_list,
            headers: {
                "Authorization": localStorage.getItem('token')
            },
            type: 'get',
            data: {
                month: m,
                year: y,
            },
        },
        columns: [{
            data: 'DT_RowIndex',
            name: 'DT_RowIndex',
            orderable: false,
            searchable: false
        },{
            data : 'key',
            defaultContent: "",
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('font-weight', 700);
            },
        },{
            data: 'io_id',
            defaultContent: "",
            fnCreatedCell: function (td, data) {
                return data == null ? $(td).css('color', 'red') : $(td).css('color', 'blue');
            },
            render: function (data) {
                return data == null ? 'Tidak Diinput' : data;
            }
        }, {
            data: 'spph_lkpps.nomorspph',
            defaultContent: "",
        }, {
            data: 'spph_lkpps.judul',
            defaultContent: "NULL",
            render : function(data){
                const judul = data;
                return judul.substr(0,80) + ' .....';
            }
        }, {
            data: 'spph_lkpps.mitra_lkpps.perusahaan',
            defaultContent: "NULL",
        }, {
            data: 'tglbakn',
            defaultContent: "",
            render : function(data){
                return moment(data).format('D MMMM Y');
            }
        }, {
            data: 'created_at',
            defaultContent: "",
            
        }, {
            data: 'harga',
            defaultContent: "NULL",
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
            },
            render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')
        }, {
            data: 'chat_bakn_lkpp',
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
            },
            defaultContent:"",
            render: function(data,type,row, meta){
                let arr=[];
                if(data != null){
                    for(let x =0; x < data.length; x++){
                        let dt = new Date(data[x].created_at);
                        if(data[x].queue == 0){
                            arr.push(`<br>( ${moment(dt).format('D.MMMM.Y HH:mm')} - ${data[x].status} )<br> ${data[x].name}`);
                        }
                    }
                  return arr;
                }
            }
        },{
            data: 'chat_bakn_lkpp',
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
            },
            defaultContent:"",
            render: function(data,type,row, meta){
                let arr=[];
                if(data != null){
                    for(let x =0; x < data.length; x++){
                        let dt = new Date(data[x].created_at);
                        if(row.tglbakn < dateLine){
                            if (data[x].username == 'lucas') {
                                arr.push(`<br>( ${moment(dt).format('D.MMMM.Y HH:mm')} - ${data[x].status} )<br> ${data[x].name}`);
                            }
                        }else{
                            if (data[x].queue == 1) {
                                arr.push(`<br>( ${moment(dt).format('D.MMMM.Y HH:mm')} - ${data[x].status} )<br> ${data[x].name}`);
                            }
                        }  
                    }
                  return arr;
                }
            }
        },{
            data: 'chat_bakn_lkpp',
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
            },
            defaultContent:"",
            render: function(data,type,row, meta){
                let arr=[];
                if(data != null){
                    for(let x =0; x < data.length; x++){
                        let dt = new Date(data[x].created_at);
                        if(row.tglbakn < dateLine){
                            if (data[x].username == 'hernadi.yoga') {
                                arr.push(`<br>( ${moment(dt).format('D.MMMM.Y HH:mm')} - ${data[x].status} )<br> ${data[x].name}`);
                            }
                        }else{
                            if (data[x].queue == 2) {
                                arr.push(`<br>( ${moment(dt).format('D.MMMM.Y HH:mm')} - ${data[x].status} )<br> ${data[x].name}`);
                            }
                        }  
                    }
                  return arr;
                }
            }
        },{
            data: 'approval',
            defaultContent: "NULL",
        }, 
        {
            data: 'tglbakn',
            orderable: false,
            searchable: false,
            'defaultContent': "",
            render: function (nTd, sData, oData, iRow, iCol) {
                let status = '';
                let action = ``;
                let hold = '';
                
                if(oData.approval == 'Return'){
                     action = `Preview`;
                }else{
                    action = `Approve`;
                }
                if(oData.hold == true){
                    hold = 'Release';
                }else{
                    hold = 'Hold';
                }
                
                if(oData.user.role == 'administrator' || (oData.user.username == oData.user_bakn_lkpp.username && oData.approval != "Return")){
                    status = `
                    <li>
                        <button id="hold-bakn" data-url="/hold-bakn-lkpp/${oData.id}" data-id="${oData.id}" data-val=${oData.hold} class="btn btn-block btn-warning">${hold}</button>
                    </li>
                    `;

                    if(oData.hold == true){
                  
                        status = `
                        <li>
                             <a href="/edit-bakn-lkpp/${oData.id}/" target="_blank" data-id="${oData.id}""><span class="icon fa fa-edit"></span>Edit</a>
                        </li>
                        ${status}`;
                    }
                } 
                
               
                return `
                    <div class="btn-group btn-hspace">
                        <button type="button" data-toggle="dropdown" class="btn bg-navy dropdown-toggle">Action <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <ul role="menu" class="dropdown-menu pull-right">
                            <li>
                                <a href="/preview-status-bakn-lkpp/${oData.id}" target="_blank" data-id="${oData.id}"><span class="icon fa fa-sticky-note-o"></span>${action}</a>
                            </li>
                            ${status}
                           
                        </ul>
                    </div>
                `
            }
        },
    ],
    });
}



// Function sweet alert hold
$('#inprogress').on('click', '#hold-bakn', function (e) {
    event.preventDefault();
    // get url and id from attribute
    const url   = $(this).data('url');
    let idnya   = $(this).data('id');
    let hold    = $(this).data('val');
    let view    = '';
    if(hold == true){
        view = "Release"
    }else{
        view = "Hold";
    }

    // declare sweet alert
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: `Yes, ${view} it!`,
        showLoaderOnConfirm: true,
        // ajax catch data
        preConfirm: (data) => {
            return fetch(url)
                .then(response => {
                    if (!response.ok) {
                        throw new Error(response.statusText)
                    }
                    return response.json();
                })
                .catch(error => {
                    Swal.showValidationMessage(
                        `Request failed: ${error}`
                    )
                })
        },
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
        if (result.value) {
            Swal.fire(
                `${view}ed!`,
                `Your file has been ${view}.`,
                'success'
            )
            // reload table after success
            setTimeout(function () {
                $('#inprogress').DataTable().ajax.reload(null, false);
            }, 800);
        }
    })
});
