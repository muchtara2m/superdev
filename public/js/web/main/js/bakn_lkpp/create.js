// Declare Variable
let spphBakn        = [];
let dataIo          = [];
let namaGmEcom      = $('#gmEcom').val();



// Set Data before document ready
$.holdReady(true);
$.getJSON("lkpp/bakn/spph", function (data) {
    data.forEach(function (item, index) {
        // console.log(item);
        spphBakn.push({
            id: `${item.nomorspph}`,
            text: `${item.nomorspph} - ${item.judul}`,

        });
    })
    $.holdReady(false);
});

// Set Data IO before document ready
$.holdReady(true);
$.getJSON("lkpp/bakn/io", function (data) {
    data.forEach(function (item, index) {
        // console.log(item);
        dataIo.push({
            id: `${item.no_io}`,
            text: `${item.no_io} - ${item.deskripsi}`,

        });
    })
    $.holdReady(false);
});


// list nomor spph with select2 ajax
$(document).ready(function () {
    $("#nospph").select2({
        placeholder     : "Pilih SPPH",
        triggerChange   : true,
        allowClear      : true,
        data            : spphBakn,
        query: function (q) {
            let pageSize    = 20;
            let results     = [];
            // for searching 
            if (q.term && q.term !== '') {
                results = _.filter(this.data, function (e) {
                    return e.text.toUpperCase().indexOf(q.term.toUpperCase()) >= 0;
                });
            } else if (q.term === '') {
                results = this.data;
            }
            q.callback({
                results: results.slice((q.page - 1) * pageSize, q.page * pageSize),
                more: results.length >= q.page * pageSize,
            });
        },
        initSelection: function (element, callback) {
            let val = $(element).val();

            if(val != ''){
                const getSpph = spphBakn.find(spph => spph.id === `${val}`);
                callback({
                    "text": getSpph.text,
                    "id": getSpph.id
                });
            }   
        }
    });

    // no_io
    $("#io_id").select2({
        placeholder     : "Pilih IO",
        triggerChange   : true,
        allowClear      : true,
        data            : dataIo,
        query: function (q) {
            let pageSize    = 20;
            let results     = [];
            // for searching 
            if (q.term && q.term !== '') {
                results = _.filter(this.data, function (e) {
                    return e.text.toUpperCase().indexOf(q.term.toUpperCase()) >= 0;
                });
            } else if (q.term === '') {
                results = this.data;
            }
            q.callback({
                results: results.slice((q.page - 1) * pageSize, q.page * pageSize),
                more: results.length >= q.page * pageSize,
            });
        },
        initSelection: function (element, callback) {
            let val = $(element).val();

            if(val != ''){
                const getDataIo = dataIo.find(io => io.id === `${val}`);
                callback({
                    "text": getDataIo.text,
                    "id": getDataIo.id
                });
            }   
        }
    });
});

// select on change no io
$('#io_id').on('change', function(){
    let valIo = $(this).val();
    $('#no_io').val(valIo);
});

// Select On Change SPPH
$('#nospph').on('change', function () {
    let nomorSpph = this.value;

    // call ajax get data spph
    $.ajax({
        headers: {
            "Authorization": localStorage.getItem('token')
        },
        type: 'get',
        url: '/data-spph-lkpp/',
        data: {
            nomorspph: nomorSpph,
        },
        dataType: 'json',
        success: function (data) {

            // variable convert date to string 
            // date tanggal spph
            const datespph  = new Date(data.tglspph);
            const monthspph = datespph.toLocaleString('id-ID', {
                                month: 'long'
                            });
            const tglspph   = datespph.getDate() + ' ' + monthspph + ' ' + datespph.getFullYear();
            // tanggal sph
            const datesph   = new Date(data.tglsph);
            const monthsph  = datesph.toLocaleString('id-ID', {
                                month: 'long'
                            });
            const tglsph    = datesph.getDate() + ' ' + monthsph + ' ' + datesph.getFullYear();

            // // on change auto fill form 
            $('#spph_id').val(data.id);
            $('#judulUtama').html(data.judul.toUpperCase());
            $('#judulSpphKedua').html(data.judul);
            $('#mitraUtama').html(data.mitra_lkpps.perusahaan.toUpperCase());
            $('.judulSpphKedua').html(data.judul);
            $('#tanggalSpph').html(`tanggal ${tglspph}`);
            $('#tanggalSph').html(tglsph);
            $('#nomorSpph').html(data.nomorspph);
            $('#nomorSPH').html(data.nomorsph);
            $('.direkturMitra').html(data.mitra_lkpps.direktur);
            $('.getNamaGmEcom').html(namaGmEcom);
            $('#namaMitra').html(data.mitra_lkpps.perusahaan);
        },
        error: function (data) {}
    });
});


// froala
$('textarea').froalaEditor({
    placeholderText: '',
    // documentReady: true,
    charCounterCount: false,
    key: '{{ env("KEY_FROALA") }}',
    toolbarButtons: [
        'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 
        'fontFamily', 'fontSize', 'color', 'inlineStyle', 'inlineClass', 'clearFormatting', '|', 
        'emoticons', 'fontAwesome', 'specialCharacters', 'paragraphFormat', 'lineHeight', '|',
        'paragraphStyle', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '|','-',
        'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 
        'insertHR', 'selectAll', 'getPDF', 'print', 'help', 'html', 'fullscreen', '|', 
        'undo', 'redo'
    ],
});
// removo alert lisensi froala
$("div > a", ".fr-wrapper").css('display', 'none');


// script datepicker
$(function () {
    $(".datejos").on("change", function () {
        this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
            .format(this.getAttribute("data-date-format"))
        )
    }).trigger("change")
});

$('.datejos').datepicker({
    autoclose: true,
    orientation: "bottom"
});

// onchange tanggal to nomer BAK
$('#tglbakn').on('change', function(){
    let tanggal = $(this).val();
    const dateBakn  = new Date(tanggal);
    const monthBakn = dateBakn.toLocaleString('id-ID', {
                        month: 'long'
                    });
    const tglBakn   = dateBakn.getDate() + ' ' + monthBakn + ' ' + dateBakn.getFullYear();
    $('#tanggalBakn').html(tglBakn);
});

// Function Harga and spelling 
// format currency harga
function formatAngka(objek, separator) {
    a = objek.value;
    b = a.replace(/[^\d]/g, "");
    c = "";
    panjang = b.length;
    j = 0;
    for (i = panjang; i > 0; i--) {
        j = j + 1;
        if (((j % 3) == 1) && (j != 1)) {
            c = b.substr(i - 1, 1) + separator + c;
        } else {
            c = b.substr(i - 1, 1) + c;
            formatAngka
        }
    }
    objek.value = c;
}

function clearDot(number) {
    output = number.replace(".", "");
    return output;
}

function addCommas(nStr) {
    nStr += '';
    x = nStr.split(',');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    if (x.length == 1) {
        x[1] = "00";
    }
    return x1 + "," + x[1];
};
// on change auto fill harga
$('#harganum').change(function () {
    var hrg = $('#harganum').val();
    hapusdot = hrg.split('.').join("");
    var blg = terbilangs(hapusdot) + "Rupiah";
    let hasil_hrg_bak = `Rp.${addCommas($('#harganum').val())},- (${blg}), `;
    $('.spellHarga').html(hasil_hrg_bak);

});

function terbilangs(n) {
    k = ['', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan'];
    a = [1000000000000000, 1000000000000, 1000000000, 1000000, 1000, 100, 10, 1];
    s = ['Kuadriliun', 'Trilyun', 'Milyar', 'Juta', 'Ribu', 'ratus', 'Puluh', ''];
    var i = 0,
        x = '';
    var j = 0,
        y = '';
    var angka = n;
    //alert(angka);
    var inp_pric = String(angka).split(",");
    while (inp_pric[0] > 0) {
        b = a[i], c = Math.floor(inp_pric[0] / b), inp_pric[0] -= b * c;
        x += (c >= 10 ? terbilangs(c) + "" + s[i] + " " : ((c > 0 && c < 10) ? k[c] + " " + s[i] + " " : ""));
        i++;

    }
    var depan = x.replace(new RegExp(/Satu Puluh (\w+)/gi), '$1 belas').replace(new RegExp(/satu (ribu|ratus|puluh|belas)/gi), 'Se\$1');

    while (inp_pric[1] > 0) {
        z = a[j], d = Math.floor(inp_pric[1] / z), inp_pric[1] -= z * d;
        y += k[d] + " ";
        j++;
    }
    var belakang = y.replace(new RegExp(/satu Puluh (\w+)/gi), '$1 Belas').replace(new RegExp(/satu (Ribu|Ratus|Puluh|Belas)/gi), 'Se\$1');

    if (belakang != '') {
        return depan + "koma" + belakang;
    } else {
        return depan;
    }

};

// function cara bayar
// check cara bayar
var interest = {
    "styles": {} // use an array to store the styles
};

function add() {
    var sbox = Array.from($('.carabayar'));
    interest.styles = []; // empty the array before rebuilding it
    sbox.forEach(function (v) {
        if (v.checked) {
            interest.styles.push(v.dataset.val);
        }
    });
    $('#isiCaraBayar').html(interest.styles.join(" "));
    // console output for demo
    // console.log( interest.styles ); // array
    // console.log( interest.styles.join() ); // CSV
    // console.log( interest.styles.join( " " ) ); // for HTML class attributes
}