// Call Function
$(document).ready(function () {
    statusBakn(getMonth + 1, d.getFullYear());
});

// Declare Variable
let url_list = $('#status').data('url');
let bulan = $('#bulan');
let tahun = $('#tahun');
let d = new Date();
var getMonth = d.getMonth();
let minTahun = (d.getFullYear() - 5);


// Declare Array Bulan
const months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus',
    'September', 'Oktober', 'November', 'Desember','All'
];

// Insert Value Option Dropdown Bulan
months.forEach(function (i, index) {
    if (getMonth == index) {
        bulan.append("<option value=" + (index + 1) + " selected>" + i + "</option>")
    } else {
        bulan.append("<option value=" + (index + 1) + ">" + i + "</option>")
    }
})
// Insert Value Option Dropdown Tahun
for (let i = d.getFullYear(); i > minTahun; i--) {
    tahun.append('<option value="' + i + '">' + i + '</option>')
}

// OnClick Month Function
$('#bulan').change(function () {
    let m = $('#bulan').val();
    let y = $('#tahun').val();
    if (m != '' && y != '') {
        statusBakn(m, y);
    } else {
        alert('Both Date is required');
    }
});

// OnClick Year Function
$('#tahun').change(function () {
    let m = $('#bulan').val();
    let y = $('#tahun').val();
    if (m != '' && y != '') {
        statusBakn(m, y);
    } else {
        alert('Both Date is required');
    }
});

// function add class button pagination
// $.fn.dataTable.ext.classes.sPageButton = 'btn btn-primary';
// Function ServerSide DataTable
function statusBakn(m, y) {
    $('#status').dataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        async: true,
        "scrollY": "58vh",
        // scrollCollapse: true,
        ajax: {
            url: url_list,
            headers: {
                "Authorization": localStorage.getItem('token')
            },
            type: 'get',
            data: {
                month: m,
                year: y,
            },
        },
        columns: [
        {
            data: 'DT_RowIndex',
            name: 'DT_RowIndex',
            orderable: false,
            searchable: false
        }, {
            data : 'key',
            defaultContent: "",
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('font-weight', 700);
            },
        },{
            data: 'io_id',
            defaultContent: "",
            fnCreatedCell: function (td, data) {
                return data == null ? $(td).css('color', 'red') : $(td).css('color', 'blue');
            },
            render: function (data) {
                return data == null ? 'Tidak Diinput' : data;
            }
        }, {
            data: 'spph_lkpps.nomorspph',
            defaultContent: "NULL",
        }, {
            data: 'tglbakn',
            defaultContent: "",
            render : function(data){
                return moment(data).format('D MMMM Y');
            }
        }, {
            data: 'spph_lkpps.judul',
            defaultContent: "NULL",
            render : function(data){
                const judul = data;
                return judul.substr(0,80) + ' .....';
            }
        }, {
            data: 'spph_lkpps.mitra_lkpps.perusahaan',
            defaultContent: "NULL",
        }, {
            data: 'harga',
            defaultContent: "NULL",
            render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')
        }, {
            data: 'user_bakn_lkpp.name',
            defaultContent: "NULL",
        }, {
            data: 'approval',
            defaultContent: "NULL",
        }, {
            data: 'tglbakn',
            orderable: false,
            searchable: false,
            'defaultContent': "",
            render: function (nTd, sData, oData, iRow, iCol) {
                let status = '';
                let action = ``;
                
                if(oData.approval == 'Return'){
                     action = `Preview`;
                }else{
                    action = `Approve`;
                }
                
                if(oData.user.role == 'administrator' || oData.user.username == oData.user_bakn_lkpp.username){
                    status = `
                        <li>
                             <a href="/edit-bakn-lkpp/${oData.id}/" target="_blank" data-id="${oData.id}""><span class="icon fa fa-edit"></span>Edit</a>
                        </li>
                        <li>
                            <button id="delete-bakn" data-url="/delete-bakn-lkpp/${oData.id}" data-id="${oData.id}" class="btn btn-block btn-danger">Delete</button>
                        </li>
                            `;
                } 
                return `
                    <div class="btn-group btn-hspace">
                        <button type="button" data-toggle="dropdown" class="btn bg-navy dropdown-toggle">Action <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <ul role="menu" class="dropdown-menu pull-right">
                            <li>
                                <a href="/preview-status-bakn-lkpp/${oData.id}" target="_blank" data-id="${oData.id}"><span class="icon fa fa-sticky-note-o"></span>${action}</a>
                            </li>
                            ${status}
                        </ul>
                    </div>
                `
            }
        }, ],
    });
}


// Function sweet alert delete
$('#status').on('click', '#delete-bakn', function (e) {
    event.preventDefault();
    // get url and id from attribute
    const url = $(this).data('url');
    let idnya = $(this).data('id');

    // declare sweet alert
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        showLoaderOnConfirm: true,
        // ajax catch data
        preConfirm: (data) => {
            return fetch(url)
                .then(response => {
                    if (!response.ok) {
                        throw new Error(response.statusText)
                    }
                    return response.json();
                })
                .catch(error => {
                    Swal.showValidationMessage(
                        `Request failed: ${error}`
                    )
                })
        },
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
        if (result.value) {
            Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
            )
            // reload table after success
            setTimeout(function () {
                $('#status').DataTable().ajax.reload(null, false);
            }, 800);
        }
    })
});