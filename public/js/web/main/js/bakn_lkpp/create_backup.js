$(document).ready(function () {
    // script froala
    $('.bakn').froalaEditor({
        placeholderText: '',
        charCounterCount: false,
        key: '{{ env("KEY_FROALA") }}',
        fontFamilySelection: 'Calibri',
        fontFamilyDefaultSelection: 'Calibri',
    });
});

function getDetailSpph() {
    let nomorSpph = $('#nmrspph').val();

    $.ajax({
        headers: {
            "Authorization": localStorage.getItem('token')
        },
        type: 'get',
        url: '/data-spph-lkpp/',
        data: {
            nomorspph: nomorSpph,
        },
        dataType: 'json',
        success: function (data) {
            $('#spphid').val(data.id);
            $('#harganum').val(data.nilai_project);
            $("#spphpreview").attr("href", "/preview-spph-lkpp/" + data.id);
            $('#direktur').attr('value', data['mitra_lkpps']['direktur']);

            const datespph = new Date(data.tglspph);
            const monthspph = datespph.toLocaleString('id-ID', {
                month: 'long'
            });
            const tglspph = datespph.getDate() + ' ' + monthspph + ' ' + datespph.getFullYear();
            const datesph = new Date(data.tglsph);
            const monthsph = datesph.toLocaleString('id-ID', {
                month: 'long'
            });
            const tglsph = datesph.getDate() + ' ' + monthsph + ' ' + datesph.getFullYear();
            var isinya2 = `
                <ol style="margin-bottom:0cm;"> 
                    <li><span>Surat Permintaan Penawaran Harga (SPPH) nomor: ${data.nomorspph} tanggal ${tglspph}perihal permintaan penawaran harga;</span></li>
                    <li><span>Surat dari SUPPLIER nomor: ${data.nomorsph} tanggal ${tglsph} perihal Penawaran Harga.</span></li>
                </ol>`;
            $('#dasarpembahasan').froalaEditor('html.set', isinya2);
            $('#agenda').froalaEditor('html.set', `Klarifikasi & Negosiasi Penawaran Harga dari SUPPLIER tentang ${data.judul}`);
            $('#ruanglingkup').froalaEditor('html.set', `${data.judul} dengan rincian sebagaimana tercantum pada point nomor 4.`);
        },
        error: function (data) {}
    });
}



// select2
$('select').select2();

function getRoles(val) {
    $('#role-cm_role_text').val('');
    var data = $('#role-cm_role').select2('data').map(function (elem) {
        return elem.text
    });
    $('#role-cm_role_text').val(data);
    $('#role-cm_role').on('select2:unselecting', function (e) {
        $('#role-cm_role_text').val('');
    });
}
// format currency harga
function formatAngka(objek, separator) {
    a = objek.value;
    b = a.replace(/[^\d]/g, "");
    c = "";
    panjang = b.length;
    j = 0;
    for (i = panjang; i > 0; i--) {
        j = j + 1;
        if (((j % 3) == 1) && (j != 1)) {
            c = b.substr(i - 1, 1) + separator + c;
        } else {
            c = b.substr(i - 1, 1) + c;
            formatAngka
        }
    }
    objek.value = c;
}

function clearDot(number) {
    output = number.replace(".", "");
    return output;
}
// script checkbox undangan
var checkboxes = document.getElementsByName('invittype[]');
var vals = "";
for (var i = 0, n = checkboxes.length; i < n; i++) {
    if (checkboxes[i].checked) {
        vals += "," + checkboxes[i].value;
    }
}
if (vals) vals = vals.substring(1);

// script datepicker
$(function () {
    $(".datejos").on("change", function () {
        this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
            .format(this.getAttribute("data-date-format"))
        )
    }).trigger("change")
});
$('.datejos').datepicker({
    autoclose: true,
    orientation: "bottom"
});
// script tagify
$('.tagify').tagify();

// harga terbilang
$(document).ready(function () {
    $('#io').DataTable();
    $('#spph').DataTable();
    // script froala
    $('.bakn').froalaEditor({
        placeholderText: '',
        charCounterCount: false,
        key: '{{ env("KEY_FROALA") }}',
        fontFamilySelection: 'Calibri',
        fontFamilyDefaultSelection: 'Calibri',
    });

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split(',');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        if (x.length == 1) {
            x[1] = "00";
        }
        return x1 + "," + x[1];
    };
    $('#harganum').change(function () {
        borongvariabel = "<b>Total harga pekerjaan adalah sebesar Rp ";
        var hrg = $('#harganum').val();
        hapusdot = hrg.split('.').join("");
        var blg = terbilangs(hapusdot) + "Rupiah";
        var tes = borongvariabel + addCommas($('#harganum').val()) + " ( " + blg + " )" + "</b> sudah termasuk PPN 10% dengan rincian sebagaimana terlampir.";
        $('#hargatext').val(tes);
        $('#nilai').froalaEditor('html.set', `${tes}`);
        $('#test_harga').val(tes);
        $('.trblng_hrg').val(tes);
        $('.dt_hrg').val(hapusdot);
    });

    function terbilangs(n) {
        k = ['', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan'];
        a = [1000000000000000, 1000000000000, 1000000000, 1000000, 1000, 100, 10, 1];
        s = ['Kuadriliun', 'Trilyun', 'Milyar', 'Juta', 'Ribu', 'ratus', 'Puluh', ''];
        var i = 0,
            x = '';
        var j = 0,
            y = '';
        var angka = n;
        //alert(angka);
        var inp_pric = String(angka).split(",");
        while (inp_pric[0] > 0) {
            b = a[i], c = Math.floor(inp_pric[0] / b), inp_pric[0] -= b * c;
            x += (c >= 10 ? terbilangs(c) + "" + s[i] + " " : ((c > 0 && c < 10) ? k[c] + " " + s[i] + " " : ""));
            i++;

        }
        var depan = x.replace(new RegExp(/Satu Puluh (\w+)/gi), '$1 belas').replace(new RegExp(/satu (ribu|ratus|puluh|belas)/gi), 'Se\$1');

        while (inp_pric[1] > 0) {
            z = a[j], d = Math.floor(inp_pric[1] / z), inp_pric[1] -= z * d;
            y += k[d] + " ";
            j++;
        }
        var belakang = y.replace(new RegExp(/satu Puluh (\w+)/gi), '$1 Belas').replace(new RegExp(/satu (Ribu|Ratus|Puluh|Belas)/gi), 'Se\$1');

        if (belakang != '') {
            return depan + "koma" + belakang;
        } else {
            return depan;
        }

    };
});
// datatable
var table = document.getElementById('io');
var tablespph = document.getElementById('spph');

for (var i = 1; i < table.rows.length; i++) {
    table.rows[i].onclick = function () {
        //  rIndex = this.rowIndex;
        document.getElementById("io_id").value = this.cells[1].innerHTML;
        document.getElementById("noio").value = this.cells[1].innerHTML;
        document.getElementById("desio").value = this.cells[2].innerHTML;
    };
};
for (var i = 1; i < tablespph.rows.length; i++) {
    tablespph.rows[i].onclick = function () {
        //  rIndex = this.rowIndex;
        document.getElementById("spphid").value = this.cells[0].innerHTML;
        $('#nmrspph').val(this.cells[1].innerHTML);

    };
};
// check cara bayar
var interest = {
    "styles": {} // use an array to store the styles
};

function add() {
    var sbox = Array.from(document.getElementsByClassName("carabayar"));
    interest.styles = []; // empty the array before rebuilding it
    sbox.forEach(function (v) {
        if (v.checked) {
            interest.styles.push(v.value);
        }
    });
    $('#tatacara').froalaEditor('html.set', interest.styles.join(" "));
    // console output for demo
    // console.log( interest.styles ); // array
    // console.log( interest.styles.join() ); // CSV
    // console.log( interest.styles.join( " " ) ); // for HTML class attributes
}