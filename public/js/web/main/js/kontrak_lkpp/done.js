// Call Function
$(document).ready(function () {
    doneKontrak(getMonth + 1, d.getFullYear());
});

// add url after click 
$(document).on("click", ".upload", function () {
    var idbro = $(this).data('id');
    $(".modal-body #idnya").val( idbro );
    $('#file').attr('action', "lkpp/kontrak/file/" + idbro);
    $('#lampiran').attr('action', "lkpp/kontrak/lampiran/" + idbro);

});

// Declare Variable
let url_list    = $('#done').data('url');
let bulan       = $('#bulan');
let tahun       = $('#tahun');
let d           = new Date();
var getMonth    = d.getMonth();
let minTahun    = (d.getFullYear() - 5);
// Declare Array Bulan
const months    = [
                    'Januari', 
                    'Februari', 
                    'Maret', 
                    'April', 
                    'Mei', 
                    'Juni', 
                    'Juli', 
                    'Agustus',
                    'September', 
                    'Oktober', 
                    'November', 
                    'Desember',
                    'All'
                ];

// Insert Value Option Dropdown Bulan
months.forEach(function (i, index) {
    if (getMonth == index) {
        bulan.append("<option value=" + (index + 1) + " selected>" + i + "</option>")
    } else {
        bulan.append("<option value=" + (index + 1) + ">" + i + "</option>")
    }
})

// Insert Value Option Dropdown Tahun
for (let i = d.getFullYear(); i > minTahun; i--) {
    tahun.append('<option value="' + i + '">' + i + '</option>')
}

// OnClick Month Function
$('#bulan').change(function () {
    let m = $('#bulan').val();
    let y = $('#tahun').val();

    if (m != '' && y != '') {
        doneKontrak(m, y);
    } else {
        alert('Both Date is required');
    }
});

// OnClick Year Function
$('#tahun').change(function () {
    let m = $('#bulan').val();
    let y = $('#tahun').val();

    if (m != '' && y != '') {
        doneKontrak(m, y);
    } else {
        alert('Both Date is required');
    }
});


// function add class button pagination
// $.fn.dataTable.ext.classes.sPageButton = 'btn btn-primary';
// Function ServerSide DataTable
function doneKontrak(m, y) {
    $('#done').dataTable({
        destroy     : true,
        processing  : true,
        serverSide  : true,
        async       : true,
        scrollY     : "50vh",
        // scrollCollapse: true,
        ajax: {
            url: url_list,
            headers: {
                "Authorization": localStorage.getItem('token')
            },
            type: 'get',
            data: {
                month: m,
                year: y,
            },
        },
        columns: [{
            data: 'DT_RowIndex',
            name: 'DT_RowIndex',
            orderable: false,
            searchable: false
        },{
            data : 'key',
            defaultContent: "",
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('font-weight', 700);
            },
        },{
            data: 'bakn_lkpp.io_id',
            defaultContent: "NULL",
        },{
            data: 'nomor_kontrak',
            defaultContent: "",
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
                $(td).css('font-weight', 'bold');
            },
        }, {
            data: 'bakn_lkpp.spph_lkpps.judul',
            defaultContent: "NULL",
            render : function(data){
                const judul = data;
                return judul.substr(0,80) + ' .....';
            }
        },{
            data: 'tanggal_kontrak',
            defaultContent:"",
            fnCreatedCell: function (td, data) {
                $(td).css('text-align', 'center');
                
            },
            render : function(data){
                return moment(data).format('D MMMM Y');
            }
        },{
            data: 'bakn_lkpp.spph_lkpps.mitra_lkpps.perusahaan',
            defaultContent: "NULL",
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
                $(td).css('font-weight', 'bold');
            },
        }, {
            data: 'bakn_lkpp.harga',
            defaultContent: "NULL",
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
                $(td).css('font-weight', 'bold');
            },
            render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')
        },{
            data: 'nomor_kontrak',
            orderable: false,
            searchable: false,
            defaultContent: "",
            render: function (nTd, sData, oData, iRow, iCol) {
                var action = ``;

                return `
                <div class="btn-group btn-hspace">
                    <button type="button" data-toggle="dropdown" class="btn bg-navy dropdown-toggle">Action <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                    <ul role="menu" class="dropdown-menu pull-right">
                        <li>
                        <a href="#modal" data-id="${oData.id}" data-toggle="modal" class="upload"  title="Upload File"><span class="icon fa fa-file-text-o"></span>Upload File</a>
                        </li>
                        <li>
                            <a href="#modal-lampiran" data-id="${oData.id}" id="upload-lampiran" data-toggle="modal" class="upload"  title="Upload Lampiran"><span class="icon fa fa-file-pdf-o"></span>Upload Lampiran</a>
                        </li>
                        <li>
                            <a href="/preview-status-kontrak-lkpp/${oData.id}/" target="_blank" data-id="${oData.id}""><span class="icon fa fa-file-code-o"></span>Preview</a>
                        </li>
                        <li>
                            <a href="/revisi-kontrak-lkpp/${oData.id}/" target="_blank" data-id="${oData.id}""><span class="icon fa fa-pencil"></span>Revisi</a>
                        </li>
                     
                        ${action}
                    </ul>
                </div>
                `
            }
        }, ],
    });
}
