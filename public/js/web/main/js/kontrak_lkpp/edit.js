// Declare Variable
let urlApi = $('#url').data('url');
let jenisKontrak = $('#jenisKontrak');
let isiJenisKontrak = $('#jenis_kontrak_sementara').data('old');
let isiSementaraJenisKontrak = $('#jenis_kontrak_sementara').val();
let urlNameApproval = $('#url_name_approval').data('url');



// Set Data before document ready
$.holdReady(true);
let spphNotDraft = [];
$.getJSON(urlApi, function (data) {
    data.forEach(function (item, index) {
        spphNotDraft.push({
            id: `${item.spph_lkpps.nomorspph}`,
            text: `${item.spph_lkpps.nomorspph} - ${item.spph_lkpps.judul}`,

        });
    })
    $.holdReady(false);
});

$(document).ready(function () {
    listDataJenisKontrak();

    $("#nospph").select2({
        placeholder: "Pilih SPPH",
        triggerChange: true,
        allowClear: true,
        data: spphNotDraft,
        query: function (q) {
            let pageSize = 20;
            let results = [];
            // for searching 
            if (q.term && q.term !== '') {
                results = _.filter(this.data, function (e) {
                    return e.text.toUpperCase().indexOf(q.term.toUpperCase()) >= 0;
                });
            } else if (q.term === '') {
                results = this.data;
            }
            q.callback({
                results: results.slice((q.page - 1) * pageSize, q.page * pageSize),
                more: results.length >= q.page * pageSize,
            });
        },
        initSelection: function (element, callback) {
            let val = $(element).val();

            if (val != '') {
                const getSpph = spphNotDraft.find(spph => spph.id === `${val}`);
                callback({
                    "text": getSpph.text,
                    "id": getSpph.id
                });
            }
        }
    });
});

function listDataJenisKontrak() {
    $.ajax({
        headers: {
            "Authorization": localStorage.getItem('token')
        },
        type: 'get',
        url: '/jenis-pasals/all',
        dataType: 'json',
        success: function (data) {
            let valIdKJenisKontrak = isiJenisKontrak == "" ? isiSementaraJenisKontrak : isiJenisKontrak;
            data.forEach(function (item, index) {
                if (item.id == valIdKJenisKontrak) {
                    jenisKontrak.append(`<option selected value="${item.id}">${item.jenis_pasal}</option>`);
                } else {
                    jenisKontrak.append(`<option value="${item.id}">${item.jenis_pasal}</option>`);
                }
            });
        },
        error: function (data) {
            console.log(data);
        }
    })
}

// on change jenis Kontrak
$('#jenisKontrak').on('change', function () {

    let idJenisKontrak = $(this).val();
    $.ajax({
        headers: {
            "Authorization": localStorage.getItem('token')
        },
        type: 'get',
        url: `/jenis-pasals/detail/${idJenisKontrak}`,
        dataType: 'json',
        success: function (data) {
            console.log(data);
            $('#isiJenisKontrak').html(data.isi_pasal);
        },
        error: function (data) {
            $('#isiJenisKontrak').html("ISI PASAL");
        }
    });
});


// Select On Change
$('#nospph').on('change', function () {
    let nomorSpph = this.value;

    // call ajax get data spph
    $.ajax({
        headers: {
            "Authorization": localStorage.getItem('token')
        },
        type: 'get',
        url: '/data-spph-lkpp/',
        data: {
            nomorspph: nomorSpph,
        },
        dataType: 'json',
        success: function (data) {

            nameApproval(data.bakn_lkpps.id);

            // variable convert date to string 
            // date tanggal spph
            const datespph = new Date(data.tglspph);
            const monthspph = datespph.toLocaleString('id-ID', {
                month: 'long'
            });
            const tglspph = datespph.getDate() + ' ' + monthspph + ' ' + datespph.getFullYear();
            // tanggal sph
            const datesph = new Date(data.tglsph);
            const monthsph = datesph.toLocaleString('id-ID', {
                month: 'long'
            });
            const tglsph = datesph.getDate() + ' ' + monthsph + ' ' + datesph.getFullYear();
            // tanggal bakn
            const datebakn = new Date(data.bakn_lkpps.tglbakn);
            const monthbakn = datebakn.toLocaleString('id-ID', {
                month: 'long'
            });
            const tglsbakn = datebakn.getDate() + ' ' + monthbakn + ' ' + datebakn.getFullYear();


            // // on change auto fill form 
            $('#bakn_id').val(data.bakn_lkpps.id);
            $('#previewBakn').attr('href',`/preview-status-bakn-lkpp/${data.bakn_lkpps.id}`);
            $('#judulKontrak').html(data.judul.toUpperCase());
            $('.judulSpph').html(data.judul);
            $('#tanggalSpph').html(tglspph);
            $('#tanggalSPH').html(tglsph);
            $('#tanggalBakn').html(tglsbakn);
            $('#nomorSpph').html(data.nomorspph);
            $('.mitraKontrak').html(data.mitra_lkpps.perusahaan.toUpperCase());
            $('#nomorSPH').html(data.nomorsph);
            $('#alamatMitra').html(data.mitra_lkpps.alamat);
            $('.namaDirektur').html(data.mitra_lkpps.direktur);
            $('#alamatPins').html(`Telkom Landmark Tower lantai 42, Jl. Gatot Subroto No.Kav. 52, Kuningan Barat, Mampang Prapatan, Kota Jakarta Selatan Daerah Khusus Ibukota Jakarta 12710`);
        },
        error: function (data) {}
    });
});

// get name approval 
function nameApproval(idBakn) {
    $.ajax({
        headers: {
            "Authorization": localStorage.getItem('token')
        },
        type: 'get',
        url: urlNameApproval,
        data: {
            id: idBakn
        },
        dataType: 'json',
        success: function (data) {
            $('.namaApproval').html(data.data.name);
            $('.jabatanApproval').html(data.data.jabatan);

        },
        error: function (data) {
            console.log(data);
        }
    })
}



// froala
$('textarea').froalaEditor({
    charCounterCount: false,
    height : 500,
    key: '{{ env("KEY_FROALA") }}',
  
});
// removo alert lisensi froala
$("div > a", ".fr-wrapper").css('display', 'none');

// script datepicker
$(function () {
    $(".datejos").on("change", function () {
        this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
            .format(this.getAttribute("data-date-format"))
        )
    }).trigger("change")
});

$('.datejos').datepicker({
    autoclose: true,
    orientation: "bottom"
});

// onchange tanggal to nomer BAK
$('#tanggal_kontrak').on('change', function () {
    let tanggal = $(this).val();
    var days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
    const date = new Date(tanggal);
    const month = date.toLocaleString('id-ID', {
        month: 'long'
    });
    const tgl = date.getDate() + ' ' + month + ' ' + date.getFullYear();
    const day = date.getDay();
    $('#hariTanggalKontrak').html(days[day]);
    $('#bulanTanggalKontrak').html(month);
    $('#tanggalTanggalKontrak').html(terbilangs(date.getDate()));
    $('#tahunTanggalKontrak').html(terbilangs(date.getFullYear()));
    $('#formatTanggalKontrak').html(` ( ${tanggal} ) `);


    let getYear = $(this).val().split('-');
    let getNoBakOnly = $('#nomor_kontrak').data('old').split('/');
    $('#nomor_kontrak').val(`${getNoBakOnly[0]}/HK.810/ECOM/PIN.00.00/${getYear[0]}`);
    // $('#hariTanggalKontrak').html(data.judul);
    // $('#tanggalTanggalKontrak').html(data.judul);
    // $('#bulanTanggalKontrak').html(data.judul);
    // $('#tahunTanggalKontrak').html(data.judul);
});

// change nomor bak
$('#nomor_kontrak').change(function () {
    var nomor = $('#nomor_kontrak').val();
    let tgl_baknya = $('#tanggal_kontrak').val();
    let split_tgl = tgl_baknya.split('-');
    const date_bak = new Date(tgl_baknya);
    const month_bak = date_bak.toLocaleString('id-ID', {
        month: 'long'
    });
    // $('#tanggal_kontrak').html(`TANGGAL ${split_tgl[2]} ${month_bak.toUpperCase()} ${split_tgl[0]}`);
    $('#nomorKontrak').html(nomor);
});


function terbilangs(angka) {
    k = ['', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan'];
    a = [1000000000000000, 1000000000000, 1000000000, 1000000, 1000, 100, 10, 1];
    s = ['Kuadriliun', 'Trilyun', 'Milyar', 'Juta', 'Ribu', 'Ratus', 'Puluh', ''];
    var i = 0,
        x = '';
    var j = 0,
        y = '';

    var inp_pric = String(angka).split(",");
    // console.log(String(angka));
    while (inp_pric[0] > 0) {
        b = a[i], c = Math.floor(inp_pric[0] / b), inp_pric[0] -= b * c;
        x += (c >= 10 ? terbilangs(c) + "" + s[i] + " " : ((c > 0 && c < 10) ? k[c] + " " + s[i] + " " : ""));
        i++;

    }
    var depan = x.replace(new RegExp(/Satu Puluh (\w+)/gi), '$1 Belas').replace(new RegExp(/satu (ribu|ratus|puluh|Belas)/gi), 'Se\$1');

    while (inp_pric[1] > 0) {
        z = a[j], d = Math.floor(inp_pric[1] / z), inp_pric[1] -= z * d;
        y += k[d] + " ";
        j++;
    }
    var belakang = y.replace(new RegExp(/Satu Puluh (\w+)/gi), '$1 Belas').replace(new RegExp(/satu (ribu|ratus|puluh|Belas)/gi), 'Se\$1');

    if (belakang != '') {
        return `${depan} koma ${belakang}`;
    } else {
        return depan;
    }

};