let dateLine = ("2020-07-01");

// Call Function
$(document).ready(function () {
    trackingKontrak(getMonth + 1, d.getFullYear());
});

// Declare Variable
let url_list    = $('#tracking').data('url');
let bulan       = $('#bulan');
let tahun       = $('#tahun');
let d           = new Date();
var getMonth    = d.getMonth();
let minTahun    = (d.getFullYear() - 5);

// Declare Array Bulan
const months    = [
                    'Januari', 
                    'Februari', 
                    'Maret', 
                    'April', 
                    'Mei', 
                    'Juni', 
                    'Juli', 
                    'Agustus',
                    'September', 
                    'Oktober', 
                    'November', 
                    'Desember',
                    'All'
                ];

// Insert Value Option Dropdown Bulan
months.forEach(function (i, index) {
    if (getMonth == index) {
        bulan.append("<option value=" + (index + 1) + " selected>" + i + "</option>")
    } else {
        bulan.append("<option value=" + (index + 1) + ">" + i + "</option>")
    }
})

// Insert Value Option Dropdown Tahun
for (let i = d.getFullYear(); i > minTahun; i--) {
    tahun.append('<option value="' + i + '">' + i + '</option>')
}

// OnClick Month Function
$('#bulan').change(function () {
    let m = $('#bulan').val();
    let y = $('#tahun').val();
    if (m != '' && y != '') {
        trackingKontrak(m, y);
    } else {
        alert('Both Date is required');
    }
});

// OnClick Year Function
$('#tahun').change(function () {
    let m = $('#bulan').val();
    let y = $('#tahun').val();
    if (m != '' && y != '') {
        trackingKontrak(m, y);
    } else {
        alert('Both Date is required');
    }
});

// function add class button pagination
// $.fn.dataTable.ext.classes.sPageButton = 'btn btn-primary';
// Function ServerSide DataTable
function trackingKontrak(m, y) {
    $('#tracking').dataTable({
        destroy     : true,
        processing  : true,
        serverSide  : true,
        async       : true,
        scrollY     : "50vh",
        scrollX     : true,
        ajax: {
            url: url_list,
            headers: {
                    "Authorization": localStorage.getItem('token')
                },
            type: 'get',
            data: {
                    month: m,
                    year: y,
                },
        },
        columns: [
        {
            data: 'DT_RowIndex',
            name: 'DT_RowIndex',
            orderable: false,
            searchable: false
        },{
            data : 'key',
            defaultContent: "",
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('font-weight', 700);
            },
        },{
            data: 'bakn_lkpp.io_id',
            defaultContent: "",
        },{
            data: 'nomor_kontrak',
            defaultContent: "",
        }, {
            data: 'bakn_lkpp.spph_lkpps.judul',
            defaultContent: "NULL",
            render : function(data){
                const judul = data;
                return judul.substr(0,80) + ' .....';
            }
        },{
            data: 'tanggal_kontrak',
            defaultContent: "",
            fnCreatedCell: function (td, data) {
                $(td).css('text-align', 'center');
            },
            render : function(data){
                return moment(data).format('D MMMM Y');
            }
        },{
            data: 'bakn_lkpp.spph_lkpps.mitra_lkpps.perusahaan',
            defaultContent: "NULL",
            fnCreatedCell: function (td, data) {
                $(td).css('text-align', 'center');
                $(td).css('font-weight', 'bold');
            },
        }, {
            data: 'bakn_lkpp.harga',
            defaultContent: "NULL",
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
            },
            render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')
        },{
            data: 'created_at',
            defaultContent: "NULL",
            render : function(data){
                return moment(data).format('D MMMM Y HH:mm');
            }
        },{
            data: 'user_kontrak_lkpp.name',
            defaultContent: "NULL",
            fnCreatedCell: function (td, data) {
                $(td).css('text-align', 'center');
                $(td).css('font-weight', 'bold');

            },
        },{
            data: 'chat_kontrak_lkpp',
            defaultContent : "",
            orderable: false,
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
            },
            render: function (data, type, row, meta) {
                let arr = [];
                if (data != null) {
                    for (let x = 0; x < data.length; x++) {
                        let dt = new Date(data[x].created_at);
                        if (data[x].queue == 0) {
                            arr.push(`<br>( ${moment(dt).format('D.MMMM.Y HH:mm')} - ${data[x].status} )<br> ${data[x].name}`);
                        }
                    }
                    return arr;
                }
            }
        },{
            data: 'chat_kontrak_lkpp',
            defaultContent: "",
            orderable: false,
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
            },
            render: function (data, type, row, meta) {
                let arr = [];
                let valQueue;
                
                if (data != null) {
                    for (let x = 0; x < data.length; x++) {
                        let dt = new Date(data[x].created_at);

                        if(row.tanggal_kontrak < dateLine){
                            if (data[x].username == 'lucas') {
                                arr.push(`<br>( ${moment(dt).format('D.MMMM.Y HH:mm')} - ${data[x].status} )<br> ${data[x].name}`);
                            }
                        }else{
                            if (data[x].queue == 1) {
                                arr.push(`<br>( ${moment(dt).format('D.MMMM.Y HH:mm')} - ${data[x].status} )<br> ${data[x].name}`);
                            }
                        }                        
                    }
                   
                    return arr;
                }
            }
        },{
            data: 'chat_kontrak_lkpp',
            defaultContent: "PROGRESS",
            orderable: false,
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
            },
            render: function (data, type, row, meta) {
                let arr = [];
                let valQueue;
                if (data != null) {
                    for (let x = 0; x < data.length; x++) {
                        let dt = new Date(data[x].created_at);

                        if(row.tanggal_kontrak < dateLine){
                            if (data[x].username == 'hernadi.yoga') {
                                arr.push(`<br>( ${moment(dt).format('D.MMMM.Y HH:mm')} - ${data[x].status} )<br> ${data[x].name}`);
                            }
                        }else{
                            if (data[x].queue == 2) {
                                arr.push(`<br>( ${moment(dt).format('D.MMMM.Y HH:mm')} - ${data[x].status} )<br> ${data[x].name}`);
                            }
                        }                        
                    }
                   
                    return arr;
                }
            }
        },{
            data: 'chat_kontrak_lkpp',
            defaultContent: "PROGRESS",
            orderable: false,
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
            },
            render: function (data, type, row, meta) {
                let arr = [];
                let valQueue;
                if (data != null) {
                    for (let x = 0; x < data.length; x++) {
                        let dt = new Date(data[x].created_at);

                        if(row.tanggal_kontrak < dateLine){
                            if (data[x].username == 'imam.santoso') {
                                arr.push(`<br>( ${moment(dt).format('D.MMMM.Y HH:mm')} - ${data[x].status} )<br> ${data[x].name}`);
                            }
                        }else{
                            if (data[x].queue == 3) {
                                arr.push(`<br>( ${moment(dt).format('D.MMMM.Y HH:mm')} - ${data[x].status} )<br> ${data[x].name}`);
                            }
                        }                        
                    }
                   
                    return arr;
                }
            }
        },{
            data: 'chat_kontrak_lkpp',
            defaultContent: "PROGRESS",
            orderable: false,
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
            },
            render: function (data, type, row, meta) {
                let arr = [];
                let valQueue;
                if (data != null) {
                    for (let x = 0; x < data.length; x++) {
                        let dt = new Date(data[x].created_at);

                        if(row.tanggal_kontrak < dateLine){
                            if (data[x].username == 'henry') {
                                arr.push(`<br>( ${moment(dt).format('D.MMMM.Y HH:mm')} - ${data[x].status} )<br> ${data[x].name}`);
                            }
                        }else{
                            if (data[x].queue == 4) {
                                arr.push(`<br>( ${moment(dt).format('D.MMMM.Y HH:mm')} - ${data[x].status} )<br> ${data[x].name}`);
                            }
                        }                        
                    }
                   
                    return arr;
                }
            }
        },{
            data: 'approval',
            defaultContent: "NULL",
            fnCreatedCell: function (td, data) {
                $(td).css('font-weight', 'bold');
            },
            render : function (nTd, sData, oData, iRow, iCol){
                if(nTd != null){
                    return nTd.toUpperCase();
                }else{
                    let status = oData.status.replace('_kontrak', '');
                    return status.toUpperCase();
                }
            }
        },{
            data: 'nomor_kontrak',
            orderable: false,
            searchable: false,
            'defaultContent': "",
            render: function (nTd, sData, oData, iRow, iCol) {
                return `
                    <div class="btn-group btn-hspace">
                        <button type="button" data-toggle="dropdown" class="btn bg-navy dropdown-toggle">Action <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <ul role="menu" class="dropdown-menu pull-right">
                            <li>
                                <a href="/preview-bak-lkpp/${oData.id}/" target="_blank" data-id="${oData.id}""><span class="icon fa fa-file-code-o"></span>Preview</a>
                            </li>
                        </ul>
                    </div>
                `
            }
        }, ],
    });
}

