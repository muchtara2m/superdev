// Call Function
$(document).ready(function () {
    statusBak(getMonth + 1, d.getFullYear());
});

// Declare Variable
let url_list    = $('#status').data('url');
let bulan       = $('#bulan');
let tahun       = $('#tahun');
let d           = new Date();
var getMonth    = d.getMonth();
let minTahun    = (d.getFullYear() - 5);

// Declare Array Bulan
const months    = [
                    'Januari', 
                    'Februari', 
                    'Maret', 
                    'April', 
                    'Mei', 
                    'Juni', 
                    'Juli', 
                    'Agustus',
                    'September', 
                    'Oktober', 
                    'November', 
                    'Desember',
                    'All'
                ];

// Insert Value Option Dropdown Bulan
months.forEach(function (i, index) {
    if (getMonth == index) {
        bulan.append("<option value=" + (index + 1) + " selected>" + i + "</option>")
    } else {
        bulan.append("<option value=" + (index + 1) + ">" + i + "</option>")
    }
})

// Insert Value Option Dropdown Tahun
for (let i = d.getFullYear(); i > minTahun; i--) {
    tahun.append('<option value="' + i + '">' + i + '</option>')
}

// OnClick Month Function
$('#bulan').change(function () {
    let m = $('#bulan').val();
    let y = $('#tahun').val();

    if (m != '' && y != '') {
        statusBak(m, y);
    } else {
        alert('Both Date is required');
    }
});

// OnClick Year Function
$('#tahun').change(function () {
    let m = $('#bulan').val();
    let y = $('#tahun').val();

    if (m != '' && y != '') {
        statusBak(m, y);
    } else {
        alert('Both Date is required');
    }
});


// function add class button pagination
// $.fn.dataTable.ext.classes.sPageButton = 'btn btn-primary';
// Function ServerSide DataTable
function statusBak(m, y) {
    $('#status').dataTable({
        destroy     : true,
        processing  : true,
        serverSide  : true,
        async       : true,
        scrollY     : "50vh",
        scrollX     : true,
        // scrollCollapse: true,
        ajax: {
            url: url_list,
            headers: {
                "Authorization": localStorage.getItem('token')
            },
            type: 'get',
            data: {
                month: m,
                year: y,
            },
        },
        columns: [{
            data: 'DT_RowIndex',
            name: 'DT_RowIndex',
            orderable: false,
            searchable: false
        },{
            data : 'key',
            defaultContent: "",
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('font-weight', 700);
            },
        },{
            data: 'nomor_bak',
            defaultContent: "",
            fnCreatedCell: function (td, data) {
                $(td).css('text-align', 'center');
                $(td).css('font-weight', 'bold');
            },
        }, {
            data: 'spph_bak_lkpp.judul',
            defaultContent: "NULL",
            render : function(data){
                const judul = data;
                return judul.substr(0,80) + ' .....';
            }
        },{
            data: 'tgl_bak',
            defaultContent:"",
            fnCreatedCell: function (td, data) {
                $(td).css('text-align', 'center');
            },
            render : function(data){
                return moment(data).format('D MMMM Y');
            }
        },{
            data: 'spph_bak_lkpp.mitra_lkpps.perusahaan',
            defaultContent: "NULL",
            fnCreatedCell: function (td, data) {
                $(td).css('text-align', 'center');
                $(td).css('font-weight', 'bold');
            },
        }, {
            data: 'harga',
            defaultContent: "NULL",
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
                $(td).css('font-weight', 'bold');
            },
            render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')
        },{
            data: 'created_at',
            defaultContent: "NULL",
        },{
            data: 'user_bak_lkpp.name',
            defaultContent: "NULL",
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
                $(td).css('font-weight', 'bold');
            },
        },{
            data: 'approval',
            defaultContent: "NULL",
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
                $(td).css('font-weight', 'bold');
            },
        },{
            data: 'nomor_bak',
            orderable: false,
            searchable: false,
            'defaultContent': "",
            render: function (nTd, sData, oData, iRow, iCol) {
                let status = '';
                let action = ``;
                if(oData.status == 'Return'){
                     action = `Preview`;
                }else{
                    action = `Approve`;
                }
                
                if(oData.user.role == 'administrator' || oData.user.username == oData.user_bak_lkpp.username){
                    status = `
                        <li>
                             <a href="/edit-bak-lkpp/${oData.id}/" target="_blank" data-id="${oData.id}""><span class="icon fa fa-edit"></span>Edit</a>
                        </li>
                        <li>
                            <button id="delete-bak" data-url="/delete-bak-lkpp/${oData.id}" data-id="${oData.id}" class="btn btn-block btn-danger">Delete</button>
                        </li>
                            `;
                } 
                return `
                    <div class="btn-group btn-hspace">
                        <button type="button" data-toggle="dropdown" class="btn bg-navy dropdown-toggle">Action <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                        <ul role="menu" class="dropdown-menu pull-right">
                            <li>
                                <a href="/preview-status-bak-lkpp/${oData.id}" target="_blank" data-id="${oData.id}"><span class="icon fa fa-sticky-note-o"></span>${action}</a>
                            </li>
                            ${status}
                        </ul>
                    </div>
                `
            }
        }, ],
    });
}
