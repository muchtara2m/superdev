// Declare Variable
let old = $('#no_bak').data('old');
let urlApi = $('#url').data('url');

// Set Data before document ready
$.holdReady(true);
let spphNotDraft = [];
$.getJSON(urlApi, function (data) {
    data.forEach(function (item, index) {
        spphNotDraft.push({
            id: `${item.nomorspph}`,
            text: `${item.nomorspph} - ${item.judul}`,

        });
    })
    $.holdReady(false);
});

$(document).ready(function () {
    $("#nospph").select2({
        placeholder     : "Pilih SPPH",
        triggerChange   : true,
        allowClear      : true,
        data            : spphNotDraft,
        query: function (q) {
            let pageSize    = 20;
            let results     = [];
            // for searching 
            if (q.term && q.term !== '') {
                results = _.filter(this.data, function (e) {
                    return e.text.toUpperCase().indexOf(q.term.toUpperCase()) >= 0;
                });
            } else if (q.term === '') {
                results = this.data;
            }
            q.callback({
                results: results.slice((q.page - 1) * pageSize, q.page * pageSize),
                more: results.length >= q.page * pageSize,
            });
        },
        initSelection: function (element, callback) {
            let val = $(element).val();

            if(val != ''){
                const getSpph = spphNotDraft.find(spph => spph.id === `${val}`);
                callback({
                    "text": getSpph.text,
                    "id": getSpph.id
                });
            }   
        }
    });
});


// Select On Change
$('#nospph').on('change', function () {
    let nomorSpph = this.value;

    // call ajax get data spph
    $.ajax({
        headers: {
            "Authorization": localStorage.getItem('token')
        },
        type: 'get',
        url: '/data-spph-lkpp/',
        data: {
            nomorspph: nomorSpph,
        },
        dataType: 'json',
        success: function (data) {
            // variable convert date to string 
            const datespph = new Date(data.tglspph);
            const monthspph = datespph.toLocaleString('id-ID', {
                month: 'long'
            });
            const tglspph = datespph.getDate() + ' ' + monthspph + ' ' + datespph.getFullYear();
            const datesph = new Date(data.tglsph);
            const monthsph = datesph.toLocaleString('id-ID', {
                month: 'long'
            });
            const tglsph = datesph.getDate() + ' ' + monthsph + ' ' + datesph.getFullYear();

            $('#spphpreview').attr('href', `/preview-spph-lkpp/${data.id}`);
            $('#spphid').val(data.id);
            // on change auto fill form 
            $('#judul').html(data.judul.toUpperCase());
            $('#surat_spph').html(`Surat Permintaan Penawaran Harga (SPPH) nomor: ${data.nomorspph} tanggal ${tglspph} permintaan penawaran harga.`);
            $('#surat_sph').html(`Surat dari Supplier tanggal ${tglsph} perihal Penawaran Harga.`);
            $('#ruang_lingkup').html(data.judul);
            $('#mitra_perusahaan').html(data.mitra_lkpps.perusahaan);
            $('#mitra').html(data.mitra_lkpps.perusahaan.toUpperCase());
            $('#direktur').html(data.mitra_lkpps.direktur);

        },
        error: function (data) {}
    });

});



// froala
$('.bak').froalaEditor({
    placeholderText: '',
    charCounterCount: false,
    key: '{{ env("KEY_FROALA") }}',
});


// format currency harga
function formatAngka(objek, separator) {
    a = objek.value;
    b = a.replace(/[^\d]/g, "");
    c = "";
    panjang = b.length;
    j = 0;
    for (i = panjang; i > 0; i--) {
        j = j + 1;
        if (((j % 3) == 1) && (j != 1)) {
            c = b.substr(i - 1, 1) + separator + c;
        } else {
            c = b.substr(i - 1, 1) + c;
            formatAngka
        }
    }
    objek.value = c;
}

function clearDot(number) {
    output = number.replace(".", "");
    return output;
}


// script datepicker
$(function () {
    $(".datejos").on("change", function () {
        this.setAttribute(
            "data-date",
            moment(this.value, "YYYY-MM-DD")
            .format(this.getAttribute("data-date-format"))
        )
    }).trigger("change")
});

$('.datejos').datepicker({
    autoclose: true,
    orientation: "bottom"
});


// function type with format currency
function addCommas(nStr) {
    nStr += '';
    x = nStr.split(',');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    if (x.length == 1) {
        x[1] = "00";
    }
    return x1 + "," + x[1];
};

// function conver number to string
function terbilangs(n) {
    k = ['', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan'];
    a = [1000000000000000, 1000000000000, 1000000000, 1000000, 1000, 100, 10, 1];
    s = ['Kuadriliun', 'Trilyun', 'Milyar', 'Juta', 'Ribu', 'ratus', 'Puluh', ''];
    var i = 0,
        x = '';
    var j = 0,
        y = '';
    var angka = n;
    //alert(angka);
    var inp_pric = String(angka).split(",");
    while (inp_pric[0] > 0) {
        b = a[i], c = Math.floor(inp_pric[0] / b), inp_pric[0] -= b * c;
        x += (c >= 10 ? terbilangs(c) + "" + s[i] + " " : ((c > 0 && c < 10) ? k[c] + " " + s[i] + " " : ""));
        i++;

    }
    var depan = x.replace(new RegExp(/Satu Puluh (\w+)/gi), '$1 belas').replace(new RegExp(/satu (ribu|ratus|puluh|belas)/gi), 'Se\$1');

    while (inp_pric[1] > 0) {
        z = a[j], d = Math.floor(inp_pric[1] / z), inp_pric[1] -= z * d;
        y += k[d] + " ";
        j++;
    }
    var belakang = y.replace(new RegExp(/satu Puluh (\w+)/gi), '$1 Belas').replace(new RegExp(/satu (Ribu|Ratus|Puluh|Belas)/gi), 'Se\$1');

    if (belakang != '') {
        return depan + "koma" + belakang;
    } else {
        return depan;
    }

};

// onchange tanggal to nomer BAK
$('#tgl_bak').on('change', function(){
    let getYear = $(this).val().split('-');
    let getNoBakOnly = $('#no_bak').data('old').split('/');
    let tgl_baknya = $('#tgl_bak').val();
    let split_tgl = tgl_baknya.split('-');
    const date_bak = new Date(tgl_baknya);
    const month_bak = date_bak.toLocaleString('id-ID', {
        month: 'long'
    });
    console.log(tgl_baknya);
    $('#tanggal_bak').html(`TANGGAL ${split_tgl[2]} ${month_bak.toUpperCase()} ${split_tgl[0]}`);
    $('#no_bak').val(`${getNoBakOnly[0]}/HK.810/ECOM/PIN.00.00/${getYear[0]}`);
});


// change nomor bak
$('#no_bak').change(function () {
    var nomor = $('#no_bak').val();
    
    $('#nomor_bak').html(`NOMOR : ${nomor}`);
});

// on change auto fill harga
$('#harganum').change(function () {
    var hrg = $('#harganum').val();
    hapusdot = hrg.split('.').join("");
    var blg = terbilangs(hapusdot) + "Rupiah";
    let hasil_format_currency = `Total harga yang sesuai dengan ruang lingkup pekerjaan tersebut diatas adalah sebesar <strong>Rp. ${addCommas($('#harganum').val())},- (${blg})</strong> sudah termasuk PPN 10% (sepuluh persen), dengan rincian sebagai berikut:`;
    let hasil_hrg_bak = `Rp.${addCommas($('#harganum').val())},- (${blg}) sudah termasuk PPN 10% (sepuluh persen), `;
    $('#harga_bak').html(hasil_format_currency);
    $('#hrg_bak').html(hasil_hrg_bak);

});
