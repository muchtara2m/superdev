// Call Function
$(document).ready(function () {
    allBakn(getMonth + 1, d.getFullYear());
});

// Declare Variable
let url_list = $('#all').data('url');
let bulan = $('#bulan');
let tahun = $('#tahun');
let d = new Date();
var getMonth = d.getMonth();
let minTahun = (d.getFullYear() - 5);


// Declare Array Bulan
const months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus',
    'September', 'Oktober', 'November', 'Desember', 'All'
];

// Insert Value Option Dropdown Bulan
months.forEach(function (i, index) {
    if (getMonth == index) {
        bulan.append("<option value=" + (index + 1) + " selected>" + i + "</option>")
    } else {
        bulan.append("<option value=" + (index + 1) + ">" + i + "</option>")
    }
})
// Insert Value Option Dropdown Tahun
for (let i = d.getFullYear(); i > minTahun; i--) {
    tahun.append('<option value="' + i + '">' + i + '</option>')
}

// OnClick Month Function
$('#bulan').change(function () {
    let m = $('#bulan').val();
    let y = $('#tahun').val();
    if (m != '' && y != '') {
        allBakn(m, y);
    } else {
        alert('Both Date is required');
    }
});

// OnClick Year Function
$('#tahun').change(function () {
    let m = $('#bulan').val();
    let y = $('#tahun').val();
    if (m != '' && y != '') {
        allBakn(m, y);
    } else {
        alert('Both Date is required');
    }
});

// function add class button pagination
// $.fn.dataTable.ext.classes.sPageButton = 'btn btn-primary';
// Function ServerSide DataTable
function allBakn(m, y) {
    $('#all').dataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        async: true,
        "scrollY": "58vh",
        // scrollCollapse: true,
        ajax: {
            url: url_list,
            headers: {
                "Authorization": localStorage.getItem('token')
            },
            type: 'get',
            data: {
                month: m,
                year: y,
            },
        },
        columns: [
        {
            data: 'DT_RowIndex',
            name: 'DT_RowIndex',
            orderable: false,
            searchable: false
        },{
            data : 'key',
            defaultContent: "",
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('font-weight', 700);
            },
        },{
            data: 'spph_bak_lkpp.judul',
            defaultContent: "",
            render : function(data){
                const judul = data;
                return judul.substr(0,80) + ' .....';
            }
        },{
            data: 'tgl_bak',
            defaultContent: "",
            render : function(data){
                return moment(data).format('D MMMM Y');
            }
           
        },{
            data: 'spph_bak_lkpp.nomorspph',
            defaultContent: "",
            fnCreatedCell: function(td)
            {
                $(td).css('color', '#605ca8')
                $(td).css('font-weight', 'bold')
            }
        },{
            data: 'nomor_bak',
            defaultContent: "",
            fnCreatedCell: function(td)
            {
                $(td).css('color', '#D81B60')
                $(td).css('font-weight', 'bold')
            }
        },{
            data: 'spph_bak_lkpp.mitra_lkpps.perusahaan',
            defaultContent: "",
            fnCreatedCell: function(td)
            {
                $(td).css('color', '#001F3F')
                $(td).css('font-weight', 'bold')
            }
        },{
            data: 'harga',
            defaultContent: "",
            render: $.fn.dataTable.render.number('.', '.', 0, 'Rp ')

        },{
            data: 'status',
            defaultContent: "",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                let status = sData.replace('_bakn', '');
                if (status == "draft") {
                    $(nTd).css('color', '#D81B60')
                    $(nTd).css('font-weight', 'bold')
                } else if (status == "save") {
                    $(nTd).css('color', '#605ca8')
                    $(nTd).css('font-weight', 'bold')
                } else {
                    $(nTd).css('color', '#39CCCC')
                    $(nTd).css('font-weight', 'bold')
                }
            },
            render: function (data) {
                return data.replace('_bak', '').toUpperCase();
            },
        },{
            data: 'created_at',
            defaultContent: "",
        },{
            data: 'approval',
            defaultContent: "",
            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                if(sData != null){
                    let status = sData.replace('_bak', '');
                    if (status == "CLOSED") {
                        $(nTd).css('color', '#39CCCC')
                        $(nTd).css('font-weight', 'bold')
                    } else {
                        $(nTd).css('color', '#ff851b')
                        $(nTd).css('font-weight', 'bold')
                    }
                }
              
            },
            render: function (data) {
                if(data != null){
                    return data.toUpperCase();
                }else{
                    return "Draft";
                }
            },
        },{
            data: 'user_bak_lkpp.name',
            defaultContent: "",
            render: function (data) {
                return data.toUpperCase();
            },
        },{
            data: 'user_bak_lkpp',
            fnCreatedCell: function (td, data) {
                $(td).css('white-space', 'nowrap');
                $(td).css('text-align', 'center');
            },
            defaultContent: "",
            render: function (data, type, row, meta) {
            //    console.log(row);
               return `
               <div class="btn-group btn-hspace">
                   <button type="button" data-toggle="dropdown" class="btn bg-navy dropdown-toggle">Action <span class="icon-dropdown mdi mdi-chevron-down"></span></button>
                   <ul role="menu" class="dropdown-menu pull-right">
                       <li>
                           <a href="/preview-bak-lkpp/${row.id}/" target="_blank" data-id="${row.id}""><span class="icon fa fa-file-code-o"></span>Preview</a>
                       </li>
                      <li>
                           <a href="/edit-bakn-lkpp/${row.id}/" target="_blank" data-id="${row.id}""><span class="icon fa fa-edit"></span>Edit</a>
                       </li>
                       <li>
                           <button id="delete-spph" data-url="/delete-bakn-lkpp/${row.id}" data-id="${row.id}" class="btn btn-block btn-danger" ><span class="icon fa fa-trash"></span>Delete</button>
                       </li>
                   </ul>
               </div>
               `
            }
        },],
    });
}


// Function sweet alert delete
$('#all').on('click', '#delete-spph', function (e) {
    event.preventDefault();
    // get url and id from attribute
    const url = $(this).data('url');
    let idnya = $(this).data('id');

    // declare sweet alert
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!',
        showLoaderOnConfirm: true,
        // ajax catch data
        preConfirm: (data) => {
            return fetch(url)
                .then(response => {
                    if (!response.ok) {
                        throw new Error(response.statusText)
                    }
                    return response.json();
                })
                .catch(error => {
                    Swal.showValidationMessage(
                        `Request failed: ${error}`
                    )
                })
        },
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
        if (result.value) {
            Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
            )
            // reload table after success
            setTimeout(function () {
                $('#all').DataTable().ajax.reload(null, false);
            }, 800);
        }
    })
});